VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Form1"
   ClientHeight    =   5040
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   6840
   LinkTopic       =   "Form1"
   ScaleHeight     =   5040
   ScaleWidth      =   6840
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdConverter 
      Caption         =   "Converter"
      Height          =   336
      Left            =   36
      TabIndex        =   0
      Top             =   1560
      Width           =   1200
   End
   Begin VB.Label Label1 
      Caption         =   "The converter will trasfer all infomation stored in the %folderId%.ini files into the preferences (system folders)  xml settings."
      Height          =   540
      Left            =   60
      TabIndex        =   1
      Top             =   936
      Width           =   4896
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'
' Run the converter form
'
Private Sub cmdConverter_Click()
  Dim fConverter As New frmConverter
  Load fConverter
  fConverter.Show
End Sub
