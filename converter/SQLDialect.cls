VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SQLDialect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit


Public Function Column(Args As Variant) As String
  If TypeOf Args Is ColumnHeaders Then
    Dim List As String
    Dim Header As Variant
    For Each Header In Args
      If Header.Key <> "" And Left(Header.Key, 1) <> "~" Then
        List = List & IIf(List = "", "", ", ") & """" & Header.Key & """"
      End If
    Next Header
    Column = List
    Exit Function
  End If
  MsgBox "Column does not know how to handle: " & Args
End Function


Public Function Sort(Args As Variant) As String
  If TypeOf Args Is ListView Then
    If Args.ColumnHeaders.Count > 0 Then
      Sort = """" & Args.ColumnHeaders(Args.SortKey + 1).Key & _
          """" & IIf(Args.SortOrder = 1, " DESC", "")
    End If
    Exit Function
  End If
  MsgBox "Sort does not know how to handle: " & Args
End Function


'
' Return two digit month of field in select clause
'
Public Function Month(Column As String) As String
  Month = "{fn month(""" & Column & """)}"
End Function


Public Function Concat(Param1 As String, Param2 As String) As String
  Concat = "{fn concat(" & Param1 & ", " & Param2 & ")}"
End Function


Public Function UCase(Column As String) As String
  UCase = "{fn ucase(""" & Column & """)}"
End Function


Public Function DateFormat(DateVal As Date) As String
  DateFormat = "{d '" & Format(DateVal, "yyyy-mm-dd") & "'}"
End Function
