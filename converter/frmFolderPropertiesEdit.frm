VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFolderPropertiesEdit 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Folder Properties Edit"
   ClientHeight    =   7548
   ClientLeft      =   36
   ClientTop       =   324
   ClientWidth     =   8700
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7548
   ScaleWidth      =   8700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picbody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   7452
      Left            =   0
      ScaleHeight     =   7452
      ScaleWidth      =   8700
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   0
      Width           =   8700
      Begin VB.CommandButton cmdPreferences 
         Caption         =   "&Preferences"
         Height          =   336
         Left            =   4272
         TabIndex        =   31
         Top             =   7032
         Width           =   1200
      End
      Begin VB.CommandButton cmdOrganize 
         Caption         =   "&Organize"
         Height          =   336
         Left            =   2964
         TabIndex        =   11
         Top             =   7032
         Width           =   1200
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "Apply"
         Default         =   -1  'True
         Height          =   336
         Left            =   6012
         TabIndex        =   12
         Top             =   7032
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Cancel          =   -1  'True
         Caption         =   "&Close"
         Height          =   336
         Left            =   7332
         TabIndex        =   13
         Top             =   7032
         Width           =   1200
      End
      Begin VB.Frame fraNote 
         Caption         =   "Note"
         Height          =   2364
         Left            =   2976
         TabIndex        =   29
         Top             =   4440
         Width           =   5544
         Begin VB.TextBox txtDataField 
            DataField       =   "note"
            Height          =   1932
            Index           =   7
            Left            =   120
            MultiLine       =   -1  'True
            TabIndex        =   10
            Top             =   264
            Width           =   5292
         End
      End
      Begin VB.Frame fraFolder 
         Caption         =   "Folder"
         Height          =   804
         Left            =   2964
         TabIndex        =   25
         Top             =   84
         Width           =   5544
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "Id"
            DataField       =   "folderId"
            Height          =   192
            Index           =   0
            Left            =   984
            TabIndex        =   30
            Top             =   228
            Width           =   4176
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Name:"
            Height          =   192
            Index           =   1
            Left            =   144
            TabIndex        =   28
            Top             =   456
            Width           =   480
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Id:"
            Height          =   192
            Index           =   5
            Left            =   132
            TabIndex        =   27
            Top             =   228
            Width           =   168
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "Name"
            DataField       =   "name"
            Height          =   192
            Index           =   1
            Left            =   996
            TabIndex        =   26
            Top             =   456
            Width           =   4176
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame fraData 
         Caption         =   "Data"
         Height          =   1656
         Left            =   2976
         TabIndex        =   20
         Top             =   960
         Width           =   5544
         Begin VB.TextBox txtDataField 
            DataField       =   "datatype"
            Height          =   288
            Index           =   8
            Left            =   984
            TabIndex        =   1
            Text            =   "Text1"
            Top             =   228
            Width           =   2628
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "new"
            Height          =   288
            Index           =   2
            Left            =   984
            TabIndex        =   4
            Text            =   "Text1"
            Top             =   1236
            Width           =   2628
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "source"
            Height          =   288
            Index           =   0
            Left            =   984
            TabIndex        =   2
            Text            =   "Text1"
            Top             =   564
            Width           =   2628
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "filter"
            Height          =   288
            Index           =   1
            Left            =   984
            TabIndex        =   3
            Text            =   "Text1"
            Top             =   900
            Width           =   4440
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Type:"
            Height          =   192
            Left            =   132
            TabIndex        =   24
            Top             =   276
            Width           =   420
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "New"
            Height          =   192
            Index           =   6
            Left            =   132
            TabIndex        =   23
            Top             =   1284
            Width           =   324
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Filter"
            Height          =   192
            Index           =   3
            Left            =   132
            TabIndex        =   22
            Top             =   960
            Width           =   348
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Source"
            Height          =   192
            Index           =   0
            Left            =   132
            TabIndex        =   21
            Top             =   612
            Width           =   516
         End
      End
      Begin VB.Frame fraLine 
         Caption         =   "Popup Menu"
         Height          =   1680
         Left            =   2976
         TabIndex        =   15
         Top             =   2712
         Width           =   5544
         Begin VB.TextBox txtDataField 
            DataField       =   "popup.default"
            Height          =   288
            Index           =   4
            Left            =   996
            TabIndex        =   7
            Text            =   "Text1"
            Top             =   576
            Width           =   2628
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "popup.explore"
            Height          =   288
            Index           =   5
            Left            =   984
            TabIndex        =   8
            Text            =   "Text1"
            Top             =   900
            Width           =   2628
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "popup.open"
            Height          =   288
            Index           =   6
            Left            =   996
            TabIndex        =   9
            Text            =   "Text1"
            Top             =   1224
            Width           =   2628
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "popup"
            Height          =   288
            Index           =   3
            Left            =   984
            TabIndex        =   5
            Text            =   "Text1"
            Top             =   216
            Width           =   2628
         End
         Begin VB.CheckBox chkDatafield 
            Caption         =   "Search"
            DataField       =   "search"
            Height          =   216
            Index           =   0
            Left            =   3888
            TabIndex        =   6
            Top             =   264
            Width           =   1416
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Default"
            Height          =   192
            Index           =   4
            Left            =   132
            TabIndex        =   19
            Top             =   612
            Width           =   504
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Open"
            Height          =   192
            Index           =   7
            Left            =   132
            TabIndex        =   18
            Top             =   1284
            Width           =   396
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Explore"
            Height          =   192
            Index           =   8
            Left            =   132
            TabIndex        =   17
            Top             =   948
            Width           =   552
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Popup"
            Height          =   192
            Index           =   9
            Left            =   132
            TabIndex        =   16
            Top             =   276
            Width           =   480
         End
      End
      Begin MSComctlLib.TreeView tvFolders 
         Height          =   7272
         Left            =   108
         TabIndex        =   0
         Top             =   132
         Width           =   2748
         _ExtentX        =   4847
         _ExtentY        =   12827
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   127
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList imlTVFolders 
         Left            =   5508
         Top             =   6948
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   255
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFolderPropertiesEdit.frx":0000
               Key             =   "tvopenfolder"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFolderPropertiesEdit.frx":05E2
               Key             =   "tvfolder"
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmFolderPropertiesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Compontents: Microsoft Windows Common Controls 6.0 (SP 6)
'References: Micrsoft XML, v4.0

Private mDirty As Boolean
Private mNodeKey As String

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    cmdApply.Enabled = mDirty
    'FormUtil.Dirty Me, mDirty
  End If
End Property

'
' Gets / sets the node key. This also loads the node's information
'
Private Property Get NodeKey() As String
  NodeKey = mNodeKey
End Property

Private Property Let NodeKey(vData As String)
  'check to insure that the node key is valid
  If tvFolders.Nodes(vData) Is Nothing Then
    Exit Property
  End If

  'set current node
  mNodeKey = vData
  tvFolders.SelectedItem = tvFolders.Nodes(NodeKey)
    
  NodeRefresh
End Property


Private Sub FormRefresh()
  TreeRefresh
  NodeRefresh
  Dirty = False
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  If Not mDirty And Verify Then
    Save = True
    Exit Function
  End If
  
  
  ' If last control doesn't validate.
'  If Not FormUtil.ValidateControls(Me) Then
'    Save = False
'    Exit Function
'  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  '
  'Save Stuff
  '
  
  ' Load items into an array
  Dim AttributeNames() As String
  Dim AttributeValues() As String
  Dim mDataControls As New Collection
      
  AddTextControl txtDataField, mDataControls
  AddValueControl chkDatafield, mDataControls
  
  Dim mDataEntry As DataField
  For Each mDataEntry In mDataControls
    If mDataEntry.Value <> vbNullString Then
      AddParameter mDataEntry.Name, mDataEntry.Value, AttributeNames, AttributeValues
    End If
  Next mDataEntry
  
  If NodeKey = SystemFolders_RootKey Then
    PreferenceUtil.writeIniNodeEntries SystemFolders_TreeName, NodeKey, tvFolders.SelectedItem.Text, AttributeNames, AttributeValues
    Save = True
  Else
    Save = (mPreferenceService.WriteFolder(SystemFolders_TreeName, NodeKey, AttributeNames, AttributeValues) <> vbNullString)
  End If
  
  If Save Then
    Save = True
  End If
  Dirty = Not Save

End Function

Private Sub NodeRefresh()
  If NodeKey = vbNullString Then
    Exit Sub
  End If
  
  Dim Datafields As New Collection
  Set Datafields = PreferenceUtil.getMapEntryCollection(SystemFolders_TreeName, NodeKey)

'  Set Datafields = IIf(NodeKey = SystemFolders_RootKey, _
'        PreferenceUtil.getIniNodeCollection(SystemFolders_TreeName), _
'        PreferenceUtil.getMapEntryCollection(SystemFolders_TreeName, NodeKey))
  
  lblDataField(0).Caption = NodeKey
  lblDataField(1).Caption = tvFolders.Nodes(NodeKey).Text
  SetFields txtDataField, Datafields
  SetFields chkDatafield, Datafields
  
  Dirty = False
 
End Sub

Public Sub ShowFolder(FolderKey As String)
  If Contains(tvFolders.Nodes, FolderKey) Then
    NodeKey = FolderKey
  End If
  Show
End Sub

Private Sub TreeRefresh()
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  'ver 2.30.0826-B (jjm)
  If Contains(tvFolders.Nodes, NodeKey) Then
    tvFolders.SelectedItem = tvFolders.Nodes(NodeKey)
  End If
End Sub

Private Sub cboDataType_Change()
  Dirty = True
End Sub

Private Sub chkSearch_Click()
  Dirty = True
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdApply_Click()
  Save
End Sub

Private Sub cmdClose_Click()
  Unload Me
End Sub

Private Sub cmdOrganize_Click()
  Dim fForm As New frmFolderOrganizeDialog
  fForm.ShowFolder NodeKey
  Unload fForm
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  'FolderUtil.TreeViewLoadFolder tvFolders, systemfolders_treename
  If Contains(tvFolders.Nodes, NodeKey) Then
    tvFolders.Nodes(NodeKey).Selected = True
  End If
  TreeRefresh
End Sub


Private Sub cmdPreferences_Click()
  Dim fPref As New frmPreferencesEdit
  Load fPref
  fPref.Show vbModal
End Sub

Private Sub Form_Load()
  'load images for Tree View
'  imlTVFolders.ListImages.Add 1, "TVFolderImage", PicDisplay.GetPicture(UrlName.TVFOLDER_IMAGE)
'  imlTVFolders.ListImages.Add 2, "TVOpenFolderImage", PicDisplay.GetPicture(UrlName.TVOPENFOLDER_IMAGE)
  tvFolders.ImageList = imlTVFolders
  
  TreeRefresh
  'Set a nodekey if there is none
  If Not tvFolders.Nodes.item(1) Is Nothing And NodeKey = vbNullString Then
    NodeKey = tvFolders.Nodes.item(1).Key
    tvFolders.Nodes.item(1).Expanded = True
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If Save(True) Then Unload Me
End Sub

Private Sub tvFolders_NodeClick(ByVal Node As MSComctlLib.Node)
  If Node.Key = NodeKey Then
    Exit Sub
  End If

  If Not Save(True) Then
    'make no changes, leave as is
    tvFolders.Nodes(NodeKey).Selected = True
    Exit Sub
  End If

  NodeKey = Node.Key
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtFolderName_Change()
  Dirty = True
End Sub
