VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "XMLDataProvider"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Session"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
' ver 2.00.0303 (jjm)

Option Explicit
Implements DataProvider

Private mRecordset As MSXML2.DOMDocument

Private mConnection As String
Private mSession As String
Private mBaseFilter As String
Private mColumn As String
Private mSource As String
Private mFilter As String
Private mSort As String


Public Property Let Connection(ByVal vData As String)
  mConnection = vData
End Property


Public Property Let Session(ByVal vData As String)
  mSession = vData
End Property



'
' Base filter is used to create a standard filter to be used with
' each search. This is mainly used with the filter created in folder
' properties edit.
'
Private Property Let DataProvider_BaseFilter(ByVal vData As String)
  mBaseFilter = vData
End Property

Private Property Get DataProvider_BaseFilter() As String
  DataProvider_BaseFilter = mBaseFilter
End Property

Private Property Get DataProvider_IsEmpty() As Boolean
  If mRecordset Is Nothing Then
    DataProvider_IsEmpty = True
  Else
    DataProvider_IsEmpty = mRecordset.documentElement.selectSingleNode("data/" & LCase(mSource) & "/row") Is Nothing
  End If
End Property

Private Property Get DataProvider_IsOpen() As Boolean
  DataProvider_IsOpen = Not mRecordset Is Nothing
End Property


Private Property Let DataProvider_Source(ByVal vData As String)
  mSource = vData
  Set mRecordset = Nothing
End Property


Private Property Get DataProvider_Source() As String
  DataProvider_Source = mSource
End Property


Private Property Let DataProvider_Column(ByVal vData As String)
  mColumn = vData
  Set mRecordset = Nothing
End Property


Private Property Get DataProvider_Column() As String
  DataProvider_Column = mColumn
End Property


Private Property Let DataProvider_Filter(ByVal vData As String)
  'mFilter = vData
  'Change the Oracle sql function sysdate into a generic sql date using the
  'date provided by vb. This function is being used in the filters set by the
  'users found in law. Mainly used in filtering claim diaries
  mFilter = Replace(vData, "sysdate", LawSQL.DateFormat(Today), , , vbTextCompare)
  Set mRecordset = Nothing
End Property


Private Property Get DataProvider_Filter() As String
  DataProvider_Filter = mFilter
End Property


Private Property Let DataProvider_Sort(ByVal vData As String)
  mSort = vData
  Set mRecordset = Nothing
End Property


Private Property Get DataProvider_Sort() As String
  DataProvider_Sort = mSort
End Property


Private Property Get DataProvider_Params() As String
  DataProvider_Params = "source=text/xml"
  If mColumn = "" Then
    DataProvider_Params = DataProvider_Params & "&column=" & EncodeURL("*")
  Else
    DataProvider_Params = DataProvider_Params & "&column=" & EncodeURL(mColumn)
  End If
  
  'ver 2.00.0303 (jjm)
  Dim BaseNotNull, FilterNotNull As Boolean
  BaseNotNull = mBaseFilter <> vbNullString
  FilterNotNull = mFilter <> vbNullString
  If BaseNotNull Or FilterNotNull Then
    Dim tempFilter As String
    tempFilter = IIf(BaseNotNull, "(", vbNullString) & mBaseFilter & IIf(BaseNotNull, ") ", vbNullString) & _
      IIf(FilterNotNull, IIf(BaseNotNull, " AND ", vbNullString) & mFilter, vbNullString)
    DataProvider_Params = DataProvider_Params & "&filter=" & EncodeURL(tempFilter)
  End If
  If Not mSort = "" Then
    DataProvider_Params = DataProvider_Params & "&sort= " & EncodeURL(mSort)
  End If
End Property


Private Sub DataProvider_OpenRecordset(Optional Count As Integer)
  'MsgBox "DataProvider_OpenRecordset: Select " & mColumn & " By " & mSort
  
  'Load Custom Setting for number of Items in a list view
  If Not Count > 0 Then
    Count = LocaleItemsPerView
  End If
  
  Dim xmlHttp As New xmlHttp
  Set xmlHttp = XMLUtil.HttpPost(mConnection & "search/" & mSource, _
      DataProvider_Params & IIf(Count > 0, "&count=" & Count, ""))
  
  'MsgBox DataProvider_Params
  'MsgBox xmlHttp.responseText
  
  If xmlHttp.responseXML.documentElement Is Nothing Then
    MsgBox "Invalid response from server: " & xmlHttp.responseText
    Set mRecordset = Nothing
    Exit Sub
  End If
  
  If Not xmlHttp.responseXML.documentElement.selectSingleNode("error") Is Nothing Then
    MsgBox "Unable to open recordset. Response: " & xmlHttp.responseText
    Set mRecordset = Nothing
    Exit Sub
  End If
  
  Set mRecordset = xmlHttp.responseXML
  If mRecordset.parseError.srcText <> "" Then
    MsgBox mRecordset.parseError.srcText & " at line " & mRecordset.parseError.Line
    Set mRecordset = Nothing
  End If

End Sub

Private Property Set DataProvider_Recordset(ByVal vData As Variant)
  Set mRecordset = vData
End Property


Private Property Get DataProvider_Recordset() As Variant
  Set DataProvider_Recordset = mRecordset
End Property


Private Property Get DataProvider_Collection() As Collection
  If mRecordset Is Nothing Then Exit Property
  Set DataProvider_Collection = XMLUtil.Rowset(mRecordset.documentElement, mSource)
End Property


Public Sub DataProvider_SetListView(View As Object, Optional isIcon As Boolean = True)
  Dim xmlNode, xmlColumnNode, RowsetNode As MSXML2.IXMLDOMNode
  Dim xItem As ListItem
      
  ' If tag is not set, then we create column headers below.
  If View.Tag = "" Then
    View.ColumnHeaders.Clear
  Else
'    If View.ColumnHeaders.Count = 0 Then
'      ListViewModule.DeserializeColumnHeaders View, View.Tag
'    End If
  End If
  View.ListItems.Clear
    
  ' Usually happens when the middle tier is down or a servlet has not been created for the source.
  If mRecordset Is Nothing Then
    MsgBox "Unable to view data.  DataProvider " & mSource & " was not opened successfully."
    Exit Sub
  End If
  
  Set RowsetNode = mRecordset.documentElement.selectSingleNode("data/" & LCase(mSource))
  If RowsetNode Is Nothing Then
    MsgBox "Failed to find /xml/data/" & LCase(mSource)
    Exit Sub
  End If
  
  If Not RowsetNode.hasChildNodes Then
    Exit Sub
  End If
  
  ' Default Header Setup
  If View.ColumnHeaders.Count = 0 Then
    For Each xmlNode In RowsetNode.childNodes(0).childNodes
      View.ColumnHeaders.Add , xmlNode.NodeName, xmlNode.NodeName
    Next xmlNode
  End If
    
  ' Scan through nodes
  For Each xmlNode In RowsetNode.childNodes
    Dim Column As ColumnHeader
    Dim First As Boolean
    First = True
    For Each Column In View.ColumnHeaders
      Select Case Left(Column.Key, 1)
        Case ""
        Case "~"
          xItem.ListSubItems.Add , Column.Key
        Case Else
          Set xmlColumnNode = xmlNode.selectSingleNode(Column.Key)
          If First Then
            Set xItem = View.ListItems.Add(, , DisplayFormat(xmlColumnNode.NodeName, DataValue(xmlColumnNode.NodeName, xmlColumnNode.Text), Column.Tag, LocaleShortDate))
            xItem.Tag = xmlColumnNode.NodeName
            If isIcon Then
              xItem.Icon = "book"
              xItem.SmallIcon = "closed"
            End If
            First = False
          Else
            ' This can happen if the column key does not exist in the underlying dataset.
            If xmlColumnNode Is Nothing Then
              xItem.ListSubItems.Add
            Else
              xItem.ListSubItems.Add , xmlColumnNode.NodeName, DisplayFormat(xmlColumnNode.NodeName, DataValue(xmlColumnNode.NodeName, xmlColumnNode.Text), Column.Tag, LocaleShortDate)
            End If
          End If
      End Select
    Next Column
  Next xmlNode
End Sub


Public Function DataProvider_Find(HandleName As String, Handle, Optional Column As String) As Collection
  Dim Rowset As Collection
  mSort = ""
  mColumn = IIf(Column = vbNullString, "*", Column)
  mFilter = """" & HandleName & """ = '" & Handle & "'"
  DataProvider_OpenRecordset 1
  Set Rowset = DataProvider_Collection
  If Not Rowset Is Nothing Then
    Set DataProvider_Find = Rowset(1)
  End If
End Function

Public Function DataProvider_FindMulti(HandleNames() As String, Handles, Optional Column As String) As Collection
  mSort = ""
  mColumn = IIf(mColumn = vbNullString, "*", mColumn)
  
  Dim newFilter As New Filter, I As Integer, Handle As Variant
  For I = 0 To SafeUBound(HandleNames)
    Handle = Handles(I)
    If VarType(Handle) = vbDate Then
      Handle = LawSQL.DateFormat(CDate(Handle))
    Else
      Handle = "'" & Handle & "'"
    End If
    newFilter.Add """" & HandleNames(I) & """ = " & Handle
  Next I
  
  mFilter = newFilter
  DataProvider_OpenRecordset 1
  
  Dim Rowset As Collection
  Set Rowset = DataProvider_Collection
  If Not Rowset Is Nothing Then
    Set DataProvider_FindMulti = Rowset(1)
  End If
End Function
