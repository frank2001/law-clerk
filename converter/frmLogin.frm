VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmLoginDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Login"
   ClientHeight    =   3180
   ClientLeft      =   36
   ClientTop       =   336
   ClientWidth     =   4428
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   4428
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "Login"
   Begin VB.PictureBox picAdvanced 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   1800
      Left            =   0
      ScaleHeight     =   1800
      ScaleWidth      =   4428
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   888
      Visible         =   0   'False
      Width           =   4428
      Begin TabDlg.SSTab ssTab 
         Height          =   1668
         Left            =   60
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   36
         Width           =   4296
         _ExtentX        =   7578
         _ExtentY        =   2942
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Advanced"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabels(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cboServer"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtServerAddress"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cmdNew"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cmdRemove"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         Begin VB.CommandButton cmdRemove 
            Caption         =   "&Remove"
            Height          =   288
            Left            =   3180
            TabIndex        =   3
            Top             =   600
            Width           =   1028
         End
         Begin VB.CommandButton cmdNew 
            Caption         =   "&New"
            Height          =   288
            Left            =   2088
            TabIndex        =   2
            Top             =   600
            Width           =   1028
         End
         Begin VB.TextBox txtServerAddress 
            Height          =   288
            Left            =   108
            TabIndex        =   4
            Top             =   1200
            Width           =   4092
         End
         Begin VB.ComboBox cboServer 
            Height          =   288
            Left            =   108
            TabIndex        =   1
            Text            =   "cboServer"
            Top             =   612
            Width           =   1860
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "I am &connecting to:"
            Height          =   192
            Left            =   132
            TabIndex        =   16
            Top             =   360
            Width           =   1320
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            Caption         =   "&Server:"
            Height          =   192
            Index           =   2
            Left            =   132
            TabIndex        =   10
            Tag             =   "&Password:"
            Top             =   960
            Width           =   516
         End
      End
   End
   Begin VB.PictureBox picLogin 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   888
      Left            =   0
      ScaleHeight     =   888
      ScaleWidth      =   4428
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   0
      Width           =   4428
      Begin VB.TextBox txtUserName 
         Height          =   288
         Left            =   1224
         TabIndex        =   8
         Top             =   72
         Width           =   3060
      End
      Begin VB.TextBox txtPassword 
         Height          =   288
         IMEMode         =   3  'DISABLE
         Left            =   1224
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   432
         Width           =   3060
      End
      Begin VB.Label lblLabels 
         Caption         =   "&User Name:"
         Height          =   252
         Index           =   0
         Left            =   96
         TabIndex        =   11
         Tag             =   "&User Name:"
         Top             =   96
         Width           =   1080
      End
      Begin VB.Label lblLabels 
         Caption         =   "&Password:"
         Height          =   252
         Index           =   1
         Left            =   96
         TabIndex        =   9
         Tag             =   "&Password:"
         Top             =   456
         Width           =   1080
      End
   End
   Begin VB.PictureBox picButtons 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   468
      Left            =   0
      ScaleHeight     =   468
      ScaleWidth      =   4428
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   2688
      Width           =   4428
      Begin VB.CommandButton cmdAdvanced 
         Caption         =   "&Advanced >>"
         Height          =   336
         Left            =   3144
         TabIndex        =   7
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   1896
         TabIndex        =   6
         Tag             =   "Cancel"
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   648
         TabIndex        =   5
         Tag             =   "OK"
         Top             =   60
         Width           =   1200
      End
   End
End
Attribute VB_Name = "frmLoginDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.00.0312 Core

Option Explicit

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public OK As Boolean

Private ServerList(99, 1) As String
Private Const ProfileName = 1
Private Const ServerName = 0
Private mIndex As Integer

Private Sub LoadProfileList()
  'ver 2.00.0312
  cboServer.Clear
  Dim I As Integer
  For I = 0 To SafeUBound(ServerList)
    If ServerList(I, ProfileName) <> vbNullString Or _
       ServerList(I, ServerName) <> vbNullString Then
      cboServer.AddItem ServerList(I, ProfileName)
    End If
  Next I
  
  'Ensure that there is at least one item in the box
  If cboServer.ListCount = 0 Then
    cmdNew_Click
    cmdRemove.Enabled = False
  Else
    cboServer.ListIndex = mIndex
    cmdRemove.Enabled = True
  End If
  
End Sub

Public Sub SerializeProfileArray()
'  'code to ensure that all address have the tailing "/"
'  Dim c As Integer
'  For c = 0 To cboServer.ListCount
'    If Right(ServerList(c, ServerName), 1) <> "/" And ServerList(c, ServerName) <> vbNullString Then
'      ServerList(c, ServerName) = ServerList(c, ServerName) & "/"
'    End If
'  Next c
'
'  'Resort Array
'  'Move current selection to top of list
'  Dim TempProfile, TempServer As String
'
'  'Make temp copy of index
'  TempProfile = ServerList(mIndex, ProfileName)
'  TempServer = ServerList(mIndex, ServerName)
'
'  'Move evey item under the index up one to fill the hole and
'  Dim I As Integer
'  For I = mIndex To 1 Step -1
'    ServerList(I, ProfileName) = ServerList(I - 1, ProfileName)
'    ServerList(I, ServerName) = ServerList(I - 1, ServerName)
'  Next I
'
'  'replace the index at the begining of the list
'  ServerList(0, ProfileName) = TempProfile
'  ServerList(0, ServerName) = TempServer
'
'  'Serialize ServerList to INI
'  'IniMain.RemoveSection UrlName.PROFILELIST_INI
'  'IniMain.RemoveSection UrlName.SERVERLIST_INI
'  Dim n As Integer
'  For n = 0 To cboServer.ListCount - 1
'  '  IniMain.WriteFile UrlName.PROFILELIST_INI, "list." & n & ".item", ServerList(n, ProfileName)
'  '  IniMain.WriteFile UrlName.SERVERLIST_INI, "list." & n & ".item", ServerList(n, ServerName)
'  Next n
'  'IniMain.WriteFile UrlName.SERVERLIST_INI, "listindex", 0
  mIndex = 0
  LoadProfileList
  
End Sub


Private Sub cboServer_Change()
  'Make changes to working list
  Dim Pos As Integer
  'ver 2.00.0312
  ServerList(mIndex, ProfileName) = IIf(cboServer.Text = vbNullString, " ", cboServer.Text)
  Pos = cboServer.SelStart
  LoadProfileList
  cboServer.SelStart = Pos 'Hide select
End Sub

Private Sub cboServer_Click()
  'Set list index and load server address data.
  mIndex = cboServer.ListIndex
  txtServerAddress.Text = ServerList(cboServer.ListIndex, ServerName)
End Sub

Private Sub cmdAdvanced_Click()
  '
  ' Hide and show the advanced tab
  '
  If picAdvanced.Visible Then
    picAdvanced.Visible = False
    cmdAdvanced.Caption = "&Advanced >>"
  Else
    picAdvanced.Visible = True
    cboServer.SetFocus
    cmdAdvanced.Caption = "&Advanced <<"
  End If
  
  Form_Resize
End Sub

Private Sub cmdNew_Click()
  'Add item to the combo box... must be first in list and then update collection
  
  'Add space at the begining
  Dim I As Integer
  For I = SafeUBound(ServerList) To 1 Step -1
    ServerList(I, ProfileName) = ServerList(I - 1, ProfileName)
    ServerList(I, ServerName) = ServerList(I - 1, ServerName)
  Next I
  
  'Insert new item into the space
  ServerList(0, ProfileName) = "New Name"
  ServerList(0, ServerName) = "http://New Server"
  mIndex = 0
  
  LoadProfileList
  
  'ver 2.00.0228
  If picAdvanced.Visible Then
    cboServer.SetFocus
  End If
End Sub

Private Sub cmdRemove_Click()
  'Remove item from list
  Dim I As Integer
  For I = cboServer.ListIndex To SafeUBound(ServerList) - 1
    ServerList(I, ProfileName) = ServerList(I + 1, ProfileName)
    ServerList(I, ServerName) = ServerList(I + 1, ServerName)
  Next I
  'Clear last entry that was moved up
  ServerList(I, ProfileName) = vbNullString
  ServerList(I, ServerName) = vbNullString
  mIndex = 0
  
  LoadProfileList
End Sub

Private Sub Form_Activate()
  Height = picButtons.Top + picButtons.Height + Height - ScaleHeight
End Sub

Private Sub Form_Initialize()
  ServerList(0, ProfileName) = "localhost"
  ServerList(0, ServerName) = "http://127.0.0.1:8080/"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    'FormUtil.ShowHelp HelpName.LOGIN_TOPIC, Me.hWnd
  End If
End Sub

Private Sub Form_Load()
  Dim sBuffer As String
  Dim lSize As Long

  sBuffer = Space$(255)
  lSize = Len(sBuffer)
  Call GetUserName(sBuffer, lSize)
  If lSize > 0 Then
      txtUserName.Text = Left$(sBuffer, lSize)
  Else
      txtUserName.Text = vbNullString
  End If
    
  LoadProfileList
  
  WindowState = 0
  Width = 4560
  Height = 1700
End Sub

Private Sub cmdCancel_Click()
    OK = False
    Me.Hide
End Sub

Private Sub cmdOk_Click()
  If txtServerAddress.Text <> vbNullString Then
    OK = True
    
    SerializeProfileArray
    
    Me.Hide
  Else
    MsgBox "You must select a server to connect to.", vbExclamation
    
    picAdvanced.Visible = True
    cboServer.SetFocus
    cmdAdvanced.Caption = "&Advanced <<"
    
    Form_Resize
  End If
End Sub

Private Sub Form_Resize()
  Height = picButtons.Top + picButtons.Height + Height - ScaleHeight
End Sub

Private Sub txtPassword_GotFocus()
  txtPassword.SelStart = 0
  txtPassword.SelLength = Len(txtPassword)
End Sub

Private Sub txtServerAddress_Change()
  If ActiveControl Is txtServerAddress Then
    ServerList(mIndex, ServerName) = txtServerAddress.Text
  End If
End Sub

Private Sub txtUserName_GotFocus()
  txtUserName.SelStart = 0
  txtUserName.SelLength = Len(txtUserName)
End Sub
