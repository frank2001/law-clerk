VERSION 5.00
Begin VB.Form frmConverter 
   Caption         =   "Converter"
   ClientHeight    =   7575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6435
   LinkTopic       =   "Form1"
   ScaleHeight     =   7575
   ScaleWidth      =   6435
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   336
      Left            =   5100
      TabIndex        =   5
      Top             =   7050
      Width           =   1200
   End
   Begin VB.TextBox txtFolderPath 
      Height          =   300
      Left            =   84
      TabIndex        =   3
      Text            =   "O:\LAWSystem\conf\folder.ini"
      Top             =   372
      Width           =   5964
   End
   Begin VB.TextBox txtOutPut 
      Height          =   5244
      Left            =   84
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   1512
      Width           =   6168
   End
   Begin VB.CommandButton cmdConvert 
      Caption         =   "&Convert"
      Height          =   336
      Left            =   84
      TabIndex        =   0
      Top             =   852
      Width           =   1200
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Enter the path of the folder.ini"
      Height          =   192
      Left            =   84
      TabIndex        =   4
      Top             =   132
      Width           =   2028
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Output:"
      Height          =   192
      Left            =   84
      TabIndex        =   1
      Top             =   1272
      Width           =   492
   End
End
Attribute VB_Name = "frmConverter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private FolderINI As New IniFile


Private Sub cmdClose_Click()
  Unload Me
End Sub

'-----------------------------------------'
' This is the xml format for the map node '
'-----------------------------------------'
'
'  <node name="folderId">
'    <map>
'      <entry key="datatype" value=""/>
'      <entry key="source" value=""/>
'      <entry key="filter" value=""/>
'      <entry key="search" value=""/>
'      <entry key="new" value=""/>
'      <entry key="note" value=""/>
'      <entry key="popup" value=""/>
'      <entry key="popup.default" value=""/>
'      <entry key="popup.explore" value=""/>
'      <entry key="popup.open" value=""/>
'    </map>
'  </node>


' Get new tree
' walk new tree and get old info
' set old info into new tree
Private Sub cmdConvert_Click()
  cmdConvert.Enabled = False
  
  FolderINI.FileName = txtFolderPath.Text
   
  Dim FolderNode As MSXML2.IXMLDOMNode
  Set FolderNode = GetFolderNode(SystemFolders_TreeName)
  If FolderNode Is Nothing Then
    MsgBox "Unable to retrieve folders from middle tier"
    Exit Sub
  End If
    
  'Take care of root data
  'doesn't need to be saved on server side, it is saved in INI
  
  'load the children
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  ConvertNodes FolderNode.selectNodes("node")
  
  MsgBox "Conversion Complete"
  
  cmdConvert.Enabled = True
End Sub

'
' Walk node and children.
'
Public Sub ConvertNode(Node As IXMLDOMNode)
  ' If no node, nothing more to load
  If Node Is Nothing Then
    Exit Sub
  End If

  'ver 2.30.0821-B (jjm)
  Dim TreeChild As MSXML2.IXMLDOMNode
  Dim Key As String
  Dim mText As String
  Key = Node.Attributes.getNamedItem("handle").Text
  mText = Node.Attributes.getNamedItem("name").Text

  '
  ' Convert Code goes here
  '
  Dim save As Boolean
  Dim AttributeNames() As String
  Dim AttributeValues() As String
  
  'Hack over the settings
  Dim temp As String
  
  'newitem = Claim
  temp = FolderINI.ReadFile(Key, "newitem")
  If temp <> vbNullString Then
    AddParameter "new", temp, AttributeNames, AttributeValues
  End If
  
  'Popup = Popup.mnuPopupClaim
  temp = FolderINI.ReadFile(Key, "Popup")
  If temp <> vbNullString Then
    AddParameter "popup", temp, AttributeNames, AttributeValues
  End If
  
  'Popup.Default = mnuClaimOpen(1)
  temp = FolderINI.ReadFile(Key, "popup.default")
  If temp <> vbNullString Then
    AddParameter "popup.default", temp, AttributeNames, AttributeValues
  End If

  'Popup.explore = mnuClaimExplore
  temp = FolderINI.ReadFile(Key, "popup.explore")
  If temp <> vbNullString Then
    AddParameter "popup.explore", temp, AttributeNames, AttributeValues
  End If
  
  'Popup.open = mnuClaimOpen
  temp = FolderINI.ReadFile(Key, "popup.open")
  If temp <> vbNullString Then
    AddParameter "popup.open", temp, AttributeNames, AttributeValues
  End If
  
  'search = 1
  temp = FolderINI.ReadFile(Key, "search")
  If temp <> vbNullString Then
    AddParameter "search", temp, AttributeNames, AttributeValues
  End If
  
  'Data.Type = Claim
  temp = FolderINI.ReadFile(Key, "data.type")
  If temp <> vbNullString Then
    AddParameter "datatype", temp, AttributeNames, AttributeValues
  End If
  
  'Data.Source = ClaimView
  temp = FolderINI.ReadFile(Key, "data.source")
  If temp <> vbNullString Then
    AddParameter "source", temp, AttributeNames, AttributeValues
  End If
  
  'filter="completeDate" is null and "voidDate" is null
  temp = FolderINI.ReadFile(Key, "filter")
  If temp <> vbNullString Then
    AddParameter "filter", temp, AttributeNames, AttributeValues
  End If
  
  'Make the call
  save = (Factory.PreferenceService.WriteFolder("systemfolders", Key, AttributeNames, AttributeValues) <> vbNullString)
  If save Then
    temp = "Save Successfull" & vbCrLf & "Key" _
          & " = " & Key _
          & vbCrLf & "Name" _
          & " = " & mText
    Dim I As Integer
    For I = 0 To SafeUBound(AttributeNames)
      temp = temp & vbCrLf & AttributeNames(I) _
          & " = " & AttributeValues(I)
    Next I
    txtOutPut.Text = txtOutPut.Text & vbCrLf & temp
  Else
    txtOutPut.Text = txtOutPut.Text & vbCrLf & _
      "ERROR ON SAVE: for key " & Key
  End If
  txtOutPut.Refresh
    
  ' Convert Child Nodes
  ConvertNodes Node.selectNodes("node")
End Sub

'
' Walk nodes
'
Public Sub ConvertNodes(NodeList As IXMLDOMNodeList)
  ' Walk Children.
  Dim Child As IXMLDOMNode
  For Each Child In NodeList
    ConvertNode Child
  Next
End Sub

'-------------------------'
' folder.ini folder entry '
'-------------------------'
'
'[Claim]
'newitem = Claim
'Popup = Popup.mnuPopupClaim
'Popup.Default = mnuClaimOpen(1)
'Popup.explore = mnuClaimExplore
'Popup.open = mnuClaimOpen
'search = 1
'Data.Type = Claim
'Data.Source = ClaimView
'filter="completeDate" is null and "voidDate" is null
'version=1.10 (9/24/2002 4:34:11 PM)

