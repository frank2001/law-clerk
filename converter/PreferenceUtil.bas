Attribute VB_Name = "PreferenceUtil"
Option Explicit

'Util Module to help the PreferenceService

'------------------------------------------'
'  This is the xml format for the map node '
'------------------------------------------'
'
'  <node name="folderId">
'    <map>
'      <entry key="datatype" value=""/>
'      <entry key="source" value=""/>
'      <entry key="filter" value=""/>
'      <entry key="search" value=""/>
'      <entry key="new" value=""/>
'      <entry key="note" value=""/>
'      <entry key="popup" value=""/>
'      <entry key="popup.default" value=""/>
'      <entry key="popup.explore" value=""/>
'      <entry key="popup.open" value=""/>
'    </map>
'  </node>


'
' Service Helpers
'
'
'
' Add a new workflow folder
'
Public Function AddFolder(Tree As TreeView) As Node
  Dim FolderHandle As String
  Dim AttributeNames(0 To 5) As String
  Dim AttributeValues(0 To 5) As String
    
  AttributeNames(0) = "datatype"
  AttributeValues(0) = "Event"
  AttributeNames(1) = "source"
  AttributeValues(1) = "EventView"
  AttributeNames(2) = "filter"
  AttributeValues(2) = """completeDate"" is null and ""voidDate"" is null"
  AttributeNames(3) = "popup"
  AttributeValues(3) = "Popup2.mnuEvent"
  AttributeNames(4) = "popup.default"
  AttributeValues(4) = "mnuEventOpen(1)"
  AttributeNames(5) = "popup.open"
  AttributeValues(5) = "mnuEventOpen"
    
  
  FolderHandle = mPreferenceService.CreateFolder(SystemFolders_TreeName, _
        IIf(Tree.SelectedItem.Key = SystemFolders_RootKey, SystemFolders_TreeName, Tree.SelectedItem.Key), _
        "New Folder", AttributeNames, AttributeValues)
  If FolderHandle = vbNullString Then
    Exit Function
  End If

  'ListViewModule.CopyColumnHeaders "EventViewDefault", FolderIniFileName("EventViewDefault"), FolderHandle, FolderIniFileName(FolderHandle)


  Dim nodeX As Node

  Set nodeX = Tree.Nodes.Add(Tree.SelectedItem, tvwChild, FolderHandle, "New Folder", _
      "tvfolder", "tvopenfolder")
  nodeX.ExpandedImage = "tvopenfolder"

  nodeX.Selected = True
  Set AddFolder = nodeX
End Function

'
' Remove the selected folder.
'
Public Sub RemoveFolder(Tree As TreeView)
  If Not MsgBox("Are you sure you want to remove the folder '" & Tree.SelectedItem.FullPath & "'?", vbYesNo + vbQuestion, "Confirm Folder Delete") = vbYes Then
    Exit Sub
  End If
  If mPreferenceService.DeleteFolder(SystemFolders_TreeName, Tree.SelectedItem.Key) Then
    Tree.Nodes.Remove Tree.SelectedItem.Index
  End If
End Sub


Public Function GetNodeMap(TreeName As String, NodeKey As String) As MSXML2.IXMLDOMNode
  Set GetNodeMap = HttpGet(HttpServer & WebApplication & WebService & "folderopen?key=" & TreeName & "&node=" & NodeKey & "&source=text/xml" & "&" & NoCache).responseXML.selectSingleNode("map")
  'Browse HttpServer & WebApplication & WebService & "folderopen?key=" & TreeName & "&node=" & NodeKey & "&source=text/xml"
End Function


'Plan to change to fill in a data type collection, then can use the set fields
Public Function getMapEntryCollection(TreeName As String, FolderKey As String) As Collection

  Dim map As MSXML2.IXMLDOMNode
  Dim Entries As MSXML2.IXMLDOMNodeList
  Dim EntriesCollection As New Collection
  
  Set map = GetNodeMap(TreeName, FolderKey)
  If map Is Nothing Then
    Exit Function
  End If
  Set Entries = map.selectNodes("entry")

  Dim Entry As MSXML2.IXMLDOMNode
  For Each Entry In Entries
    EntriesCollection.Add CreateDataField("entry", Entry.Attributes.getNamedItem("key").Text, Entry.Attributes.getNamedItem("value").Text), Entry.Attributes.getNamedItem("key").Text
  Next Entry

  Set getMapEntryCollection = EntriesCollection
End Function

Public Function getMapEntryValue(Node As IXMLDOMNode, EntryKey As String) As String
  If Node Is Nothing Then
    Exit Function
  End If
  
  Dim EntryList As IXMLDOMNodeList
  Dim Entry As IXMLDOMNode
  Set EntryList = Node.selectNodes("entry")
  For Each Entry In EntryList
    If Entry.Attributes.getNamedItem("key").Text = EntryKey Then
      getMapEntryValue = Entry.Attributes.getNamedItem("value").Text
      Exit Function
    End If
  Next
End Function

'
' These functions deal with saving and retrieving data for the root
' folders that are saved locally
'

'---------------------------------------'
' LAW.ini  (Move to folder.ini)         '
'---------------------------------------'
'[main]
'SystemFoldersTreeName = systemtreename
'[systemtreename]
'handle = "something"
'name = "somethingelse"
'entry.0.key="name"
'entry.0.value="value"
'entry.1.key="name"
'entry.1.value="value"

Public Function getIniNodeCollection(TreeName As String) As Collection
  Dim Col As New Collection
  Dim I As Integer
  Dim Key As String
  For I = 0 To 100
    Key = FolderINI.ReadFile(TreeName, "entry." & I & ".key")
    If Key = vbNullString Then
      Exit For
    End If
    Col.Add CreateDataField("entry", Key, FolderINI.ReadFile(TreeName, "entry." & I & ".value")), Key
  Next I
  Set getIniNodeCollection = Col
End Function

Public Function writeIniNodeEntries(TreeName As String, NodeHandle As String, NodeName As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String) As Boolean
  FolderINI.RemoveSection TreeName
  FolderINI.WriteFile TreeName, "handle", NodeHandle
  FolderINI.WriteFile TreeName, "name", NodeName
  Dim I As Integer
  For I = 0 To SafeUBound(AttributeValues)
    FolderINI.WriteFile TreeName, "entry." & I & ".key", AttributeNames(I)
    FolderINI.WriteFile TreeName, "entry." & I & ".value", AttributeValues(I)
  Next I
  writeIniNodeEntries = True
End Function

Public Sub renameIniNode(TreeName As String, NodeName As String)
  FolderINI.WriteFile TreeName, "name", NodeName
End Sub
