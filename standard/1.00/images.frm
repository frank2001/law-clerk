VERSION 5.00
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmImages 
   Caption         =   "Form1"
   ClientHeight    =   7956
   ClientLeft      =   60
   ClientTop       =   348
   ClientWidth     =   4476
   LinkTopic       =   "Form1"
   ScaleHeight     =   7956
   ScaleWidth      =   4476
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List1 
      Height          =   7344
      Left            =   72
      TabIndex        =   4
      Top             =   396
      Width           =   4272
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4896
      Top             =   1272
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   24
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "images.frx":0000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4476
      _ExtentX        =   7895
      _ExtentY        =   593
      BandCount       =   1
      _CBWidth        =   4476
      _CBHeight       =   336
      _Version        =   "6.7.8988"
      Child1          =   "picNavigation"
      MinHeight1      =   288
      Width1          =   2352
      NewRow1         =   0   'False
      Begin VB.PictureBox picNavigation 
         BorderStyle     =   0  'None
         Height          =   288
         Left            =   24
         ScaleHeight     =   288
         ScaleWidth      =   4380
         TabIndex        =   1
         Top             =   24
         Width           =   4380
         Begin MSComctlLib.Toolbar Toolbar2 
            Height          =   324
            Left            =   2160
            TabIndex        =   2
            Top             =   0
            Width           =   1452
            _ExtentX        =   2561
            _ExtentY        =   572
            ButtonWidth     =   1122
            ButtonHeight    =   487
            Appearance      =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Two"
                  Style           =   5
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   324
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Width           =   1464
            _ExtentX        =   2582
            _ExtentY        =   572
            ButtonWidth     =   1143
            ButtonHeight    =   487
            Appearance      =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "One"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "one"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
End
Attribute VB_Name = "frmImages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

'This is the standard way to use bitmap images in LAW. The bitmaps must be 256
'color bitmaps. Anything higher will result in a conflicting mask color. Bitmaps
'are to be captured using the 256 color display settings on the computer.

ImageList1.ListImages.Add 1, "forward", LoadPicture(App.Path & "\forward.bmp")
ImageList1.ListImages.Add 2, "back", LoadPicture(App.Path & "\back.bmp")
ImageList1.UseMaskColor = True

Toolbar1.ImageList = ImageList1
Toolbar1.Buttons(1).Image = "back"
Toolbar2.ImageList = ImageList1
Toolbar2.Buttons(1).Image = "forward"

  
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  List1.AddItem "One_buttonClick"
End Sub

Private Sub Toolbar1_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  List1.AddItem "One_buttonDropDown"
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  List1.AddItem "One_buttonMenuClick"
End Sub

Private Sub Toolbar1_Click()
  'List1.AddItem "ToolbarOne_buttonClick"
End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
  List1.AddItem "two_buttonClick"
End Sub

Private Sub Toolbar2_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  List1.AddItem "two_buttonDropDown"
End Sub

Private Sub Toolbar2_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  List1.AddItem "two_buttonMenuClick"
End Sub

Private Sub Toolbar2_Click()
  'List1.AddItem "Toolbartwo_buttonClick"
End Sub


