VERSION 5.00
Begin VB.Form frmComboSearch 
   Caption         =   "Combo Search"
   ClientHeight    =   6000
   ClientLeft      =   48
   ClientTop       =   288
   ClientWidth     =   9840
   LinkTopic       =   "Form1"
   ScaleHeight     =   6000
   ScaleWidth      =   9840
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtName 
      Height          =   288
      Left            =   288
      TabIndex        =   0
      Top             =   264
      Width           =   2520
   End
   Begin VB.ListBox lstAll 
      Height          =   5040
      Left            =   5256
      TabIndex        =   3
      Top             =   264
      Width           =   3672
   End
   Begin VB.ListBox lstName 
      Appearance      =   0  'Flat
      Height          =   792
      Left            =   288
      TabIndex        =   2
      Top             =   552
      Visible         =   0   'False
      Width           =   3708
   End
   Begin VB.ComboBox cboSearch 
      Height          =   288
      ItemData        =   "frmComboSearch.frx":0000
      Left            =   5280
      List            =   "frmComboSearch.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   1
      Text            =   "cboSearch"
      Top             =   5388
      Width           =   3384
   End
End
Attribute VB_Name = "frmComboSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mNames As Collection
Private mWords As String

Private Sub FilterListBox(Phrase As String)
  Dim I As Integer
  Dim Param As String
  Param = "*" & Phrase & "*"
  mWords = Phrase
  lstName.Clear
  
  For I = 1 To mNames.Count
    If UCase(mNames(I)) Like UCase(Param) Then
      lstName.AddItem mNames.Item(I)
    End If
  Next I
  
  If lstName.ListCount = 1 And UCase(lstName.List(1)) = UCase(Phrase) Then
    lstName.Visible = False
    Exit Sub
  End If
    
  lstName.Height = 288 * lstName.ListCount
  lstName.Visible = (lstName.ListCount > 0)
  lstName.ListIndex = -1
  txtName.SetFocus
End Sub

'Private Sub cboSearch_KeyDown(KeyCode As Integer, Shift As Integer)
'  If lstName.Visible Then
'    Select Case KeyCode
'      Case vbKeyUp
'         lstName.ListIndex = lstName.ListCount - 1
'      Case vbKeyDown
'         lstName.ListIndex = 0
'    End Select
'  End If
'End Sub

Private Sub cboSearch_KeyPress(KeyAscii As Integer)
'  FilterListBox cboSearch.Text
End Sub

Private Sub Form_Load()
  
  ' Set up a collection
  Set mNames = New Collection
  mNames.Add "Richard"
  mNames.Add "Jack"
  mNames.Add "Jill"
  mNames.Add "Brad"
  mNames.Add "Jeremy"
  mNames.Add "Jacob"
  mNames.Add "Jesse"
  mNames.Add "Libbi"
  mNames.Add "Sean"
  mNames.Add "Dennis"
  mNames.Add "Mel"
  mNames.Add "Connie"
  mNames.Add "Jane"
  mNames.Add "Sally"
  mNames.Add "Katja"
  
  LoadList
End Sub
Private Sub LoadList()
  '
  ' Load the collection into a list box
  '
  
  Dim I As Integer
  
  cboSearch.Clear
  For I = 1 To mNames.Count
    cboSearch.AddItem mNames.Item(I)
    lstAll.AddItem mNames.Item(I)
  Next I
End Sub
Private Sub ResetText(RestoreText As Boolean)
  '
  'Code to hide the list view and set focus back to text field
  '
  lstName.Visible = False
  txtName.SetFocus
  If RestoreText Then
    txtName.Text = mWords
  End If
End Sub
Private Sub ScrollToText()
  lstName.ListIndex = -1
  txtName.SetFocus
  txtName.Text = mWords
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set mNames = Nothing
End Sub

Private Sub lstName_Click()
  '
  ' This is called anytime the list index is changed.
  ' This code updates the text box when appropriate
  '
  
  If Not (lstName.ListIndex = -1) Then
    txtName.Text = lstName.List(lstName.ListIndex)
  '  cboSearch.Text = lstName.List(lstName.ListIndex)
  End If
  
End Sub

Private Sub lstName_KeyDown(KeyCode As Integer, Shift As Integer)
'  Select Case KeyCode
'    Case vbKeyUp
'      ' Trap to bring focus back to text
'      If lstName.ListIndex = 0 Then
'        ScrollToText
'        ' Prevent the keycode from being sent to the list control
'        KeyCode = 0
'      End If
'    Case vbKeyDown
'      If lstName.ListIndex = lstName.ListCount - 1 Then
'        ScrollToText
'        KeyCode = 0
'      End If
'    Case vbEnter
'      ResetText True
'    Case vbKeyEscape
'      ResetText True
'  End Select
'
End Sub

Private Sub txtName_Change()
  If Not ActiveControl = lstName Then
    FilterListBox txtName.Text
  End If
End Sub

Private Sub txtName_KeyDown(KeyCode As Integer, Shift As Integer)
  If lstName.Visible Then
    lstName.SetFocus
    Select Case KeyCode
      Case vbKeyUp
        Select Case lstName.ListIndex
          Case 0
            ScrollToText
          Case Else
            lstName.ListIndex = IIf(lstName.ListIndex < 0, lstName.ListCount, lstName.ListIndex) - 1
            txtName.SetFocus
         End Select
         
      Case vbKeyDown
        Select Case lstName.ListIndex
          Case lstName.ListCount - 1
            ScrollToText
          Case Else
            lstName.ListIndex = IIf(lstName.ListIndex = lstName.ListCount - 1, 0, lstName.ListIndex + 1)
            txtName.SetFocus
        End Select

      Case vbEnter
        ResetText True
      Case vbKeyEscape
        ResetText True
        
    End Select
  End If
End Sub

