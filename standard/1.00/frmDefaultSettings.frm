VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmDefaultSettings 
   Caption         =   "Default Settings"
   ClientHeight    =   3768
   ClientLeft      =   48
   ClientTop       =   288
   ClientWidth     =   6264
   LinkTopic       =   "Form1"
   ScaleHeight     =   3768
   ScaleWidth      =   6264
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Default         =   -1  'True
      Height          =   336
      Left            =   3588
      TabIndex        =   4
      Top             =   3312
      Width           =   1200
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   4920
      TabIndex        =   3
      Top             =   3300
      Width           =   1200
   End
   Begin VB.ComboBox cboItem 
      Height          =   288
      Left            =   108
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   252
      Width           =   2304
   End
   Begin VB.TextBox txtItemValue 
      Height          =   288
      Left            =   2664
      TabIndex        =   0
      Top             =   240
      Width           =   3348
   End
   Begin MSComctlLib.ListView lvItems 
      Height          =   2316
      Left            =   108
      TabIndex        =   2
      Top             =   768
      Width           =   6024
      _ExtentX        =   10626
      _ExtentY        =   4085
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Name"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Value"
         Object.Width           =   5292
      EndProperty
   End
End
Attribute VB_Name = "frmDefaultSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOpenItemHandle As String
Private mItem As Collection
Private mItemValue As Collection

Private Sub ItemRefresh()
'  lvItem.ListItems.Clear
'  If mItemValue Is Nothing Then
'    Exit Sub
'  End If
'  Dim Row As Collection, Attr As String
'  For Each Row In mItem
'    Attr = Row("ItemId")
'    'Will need replacement code for the contains method
'    If Contains(mItemValue, Attr) Then
'      If mItemValue(Attr) <> vbNullString Then
'        With lvItem.ListItems.Add(, Attr, Row("ItemName"))
'          .ListSubItems.Add , , mItemValue(Attr)
'        End With
'      End If
'    End If
'  Next Row
End Sub

Private Sub FormRefresh()
  'Need to load Item data into a collection
  ItemRefresh
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
'Add save code


End Function


Private Sub cboItem_Click()
  txtDatafield(7).Text = mItem(cboItem.ListIndex + 1)("ItemId")
  If Contains(mItemValue, txtDatafield(7).Text) Then
    txtitem.Text = mItemValue(txtDatafield(7).Text)
  Else
    txtitem.Text = vbNullString
  End If
End Sub
'
' Controls
'
Private Sub cmdCancel_Click()
  Unload Me
End Sub


Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub


Private Sub Form_Load()
  
  'Load the combo box Items
  'Set mItem = Factory.ItemService.GetItemsCollection(DatabaseName.PERSON_TABLE)
  'FormUtil.ComboBoxLoad mItem, cboItem, "ItemName"
  
  FormRefresh
End Sub

Private Sub lvItem_Click()
  txtDatafield(7).Text = lvItem.SelectedItem.Key
End Sub


Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub


Private Sub txtItem_LostFocus()
  If txtDatafield(7).Text = vbNullString Then
    If txtitem.Text <> vbNullString Then
      MsgBox "Must select an Item first."
      txtitem.Text = vbNullString
    End If
    Exit Sub
  End If
  
  'Removes the value if it already exists in the collection
  'so it can be re-added
  If Contains(mItemValue, txtDatafield(7).Text) Then
    If mItemValue(txtDatafield(7).Text) = txtitem.Text Then
      Exit Sub
    End If
    mItemValue.Remove txtDatafield(7).Text
  End If
  
  'If (txtItem.Text <> vbNullString) Then
    If mItemValue Is Nothing Then
      Set mItemValue = New Collection
    End If
    mItemValue.Add txtItemValue.Text, txtDatafield(7).Text
  'End If
  
  ItemRefresh
  
End Sub


'
' See if a collection contains the index given
'
Public Function Contains(Collection As Variant, Index As Variant) As Boolean
On Error GoTo Exception
  IsObject Collection(Index)
  Contains = True
  Exit Function
Exception:
  Err.Clear
End Function

