VERSION 5.00
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmApplicationOpen 
   Caption         =   "Application"
   ClientHeight    =   6192
   ClientLeft      =   132
   ClientTop       =   612
   ClientWidth     =   7716
   Icon            =   "frmApplicationOpen.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6192
   ScaleWidth      =   7716
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5364
      Left            =   0
      ScaleHeight     =   5364
      ScaleWidth      =   7716
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   384
      Width           =   7716
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   276
         Left            =   3684
         TabIndex        =   49
         Top             =   1848
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   487
         ButtonWidth     =   1143
         ButtonHeight    =   487
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "One"
               Object.ToolTipText     =   "This is only a test"
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   3
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Text            =   "MiniOne"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Text            =   "MiniTwo"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Text            =   "MiniThree"
                  EndProperty
               EndProperty
            EndProperty
         EndProperty
      End
      Begin VB.Frame fraNote 
         Caption         =   "Application Note"
         Height          =   912
         Left            =   84
         TabIndex        =   46
         Top             =   1272
         Width           =   3660
         Begin VB.TextBox txtNote 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   600
            Left            =   108
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   47
            TabStop         =   0   'False
            Top             =   204
            Width           =   3516
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Contact"
         Height          =   1200
         Left            =   84
         TabIndex        =   18
         Top             =   24
         Width           =   4200
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "county"
            DataField       =   "county"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   17
            Left            =   2460
            TabIndex        =   39
            Top             =   924
            UseMnemonic     =   0   'False
            Width           =   468
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "fax"
            DataField       =   "fax"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   18
            Left            =   2460
            TabIndex        =   38
            Top             =   696
            UseMnemonic     =   0   'False
            Width           =   204
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmName"
            DataField       =   "firmName"
            DataMember      =   "ContactFirm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   192
            Index           =   1
            Left            =   144
            TabIndex        =   26
            Top             =   192
            UseMnemonic     =   0   'False
            Width           =   804
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "telephone"
            DataField       =   "telephone"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   3
            Left            =   2460
            TabIndex        =   25
            Top             =   444
            UseMnemonic     =   0   'False
            Width           =   720
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "displayName"
            DataField       =   "displayName"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   2
            Left            =   144
            TabIndex        =   24
            Top             =   444
            UseMnemonic     =   0   'False
            Width           =   972
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmId"
            DataField       =   "firmId"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   0
            Left            =   2244
            TabIndex        =   23
            Top             =   144
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   384
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "zip"
            DataField       =   "zip"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   9
            Left            =   2820
            TabIndex        =   22
            Top             =   120
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   204
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "state"
            DataField       =   "state"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   8
            Left            =   3456
            TabIndex        =   21
            Top             =   120
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   348
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "city"
            DataField       =   "city"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   7
            Left            =   144
            TabIndex        =   20
            Top             =   924
            UseMnemonic     =   0   'False
            Width           =   240
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "addressLine1"
            DataField       =   "addressLine1"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   4
            Left            =   144
            TabIndex        =   19
            Top             =   696
            UseMnemonic     =   0   'False
            Width           =   984
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Application"
         Height          =   2004
         Left            =   4344
         TabIndex        =   5
         Top             =   0
         Width           =   3252
         Begin VB.Label lblLawyerCount 
            Caption         =   "Label3"
            Height          =   180
            Left            =   1740
            TabIndex        =   45
            Top             =   1722
            Width           =   708
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerOtherCount"
            DataField       =   "lawyerOtherCount"
            DataMember      =   "Application"
            Height          =   192
            Index           =   21
            Left            =   2796
            TabIndex        =   44
            Top             =   504
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   312
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerOfCounselCount"
            DataField       =   "lawyerOfCounselCount"
            DataMember      =   "Application"
            Height          =   192
            Index           =   20
            Left            =   2796
            TabIndex        =   43
            Top             =   300
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   672
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerPrincipalCount"
            DataField       =   "lawyerPrincipalCount"
            DataMember      =   "Application"
            Height          =   192
            Index           =   13
            Left            =   2796
            TabIndex        =   42
            Top             =   96
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   552
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Lawyers"
            Height          =   192
            Left            =   132
            TabIndex        =   41
            Top             =   1704
            Width           =   600
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Policy Term:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   17
            Top             =   972
            UseMnemonic     =   0   'False
            Width           =   900
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "policyTerm"
            DataField       =   "policyTerm"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Application"
            Height          =   192
            Index           =   15
            Left            =   1740
            TabIndex        =   16
            Top             =   972
            UseMnemonic     =   0   'False
            Width           =   816
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "receiveDate"
            DataField       =   "receiveDate"
            DataMember      =   "Application"
            Height          =   192
            Index           =   14
            Left            =   1740
            TabIndex        =   15
            Top             =   732
            UseMnemonic     =   0   'False
            Width           =   888
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Date Rec'd:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   14
            Top             =   732
            UseMnemonic     =   0   'False
            Width           =   852
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            DataMember      =   "Application"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   192
            Index           =   11
            Left            =   1740
            TabIndex        =   13
            Top             =   252
            UseMnemonic     =   0   'False
            Width           =   1116
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Left            =   144
            TabIndex        =   12
            Top             =   240
            UseMnemonic     =   0   'False
            Width           =   1032
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            Caption         =   "Application Status:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   11
            Top             =   1212
            UseMnemonic     =   0   'False
            Width           =   1320
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "applicationStatus"
            DataField       =   "applicationStatus"
            DataMember      =   "Application"
            Height          =   192
            Index           =   16
            Left            =   1740
            TabIndex        =   10
            Top             =   1212
            UseMnemonic     =   0   'False
            Width           =   1212
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Staff:"
            Height          =   192
            Left            =   144
            TabIndex        =   9
            Top             =   492
            UseMnemonic     =   0   'False
            Width           =   348
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "staffCount"
            DataField       =   "staffCount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Application"
            Height          =   192
            Index           =   12
            Left            =   1740
            TabIndex        =   8
            Top             =   492
            UseMnemonic     =   0   'False
            Width           =   696
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "applicationTypeId"
            DataField       =   "applicationTypeId"
            DataMember      =   "Application"
            Height          =   192
            Index           =   10
            Left            =   1740
            TabIndex        =   7
            Top             =   1452
            UseMnemonic     =   0   'False
            Width           =   1308
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Application Type:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   6
            Top             =   1452
            UseMnemonic     =   0   'False
            Width           =   1260
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   4560
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   1368
         Width           =   1416
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3132
         Left            =   24
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   2232
         Width           =   7608
         _ExtentX        =   13420
         _ExtentY        =   5525
         _Version        =   393216
         Style           =   1
         Tabs            =   5
         TabsPerRow      =   10
         TabHeight       =   420
         TabCaption(0)   =   "Lawyer"
         TabPicture(0)   =   "frmApplicationOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Law Area"
         TabPicture(1)   =   "frmApplicationOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Limit"
         TabPicture(2)   =   "frmApplicationOpen.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Insurer"
         TabPicture(3)   =   "frmApplicationOpen.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Question"
         TabPicture(4)   =   "frmApplicationOpen.frx":007C
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "lvView(4)"
         Tab(4).ControlCount=   1
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   3
            Left            =   -74904
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "ApplicationFirmInsurer"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   2
            Left            =   -74904
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationLimit"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   1
            Left            =   -74904
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationLawArea"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   0
            Left            =   96
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationLawyer"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   4
            Left            =   -74904
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationQuestion"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   7716
      _ExtentX        =   13610
      _ExtentY        =   677
      _CBWidth        =   7716
      _CBHeight       =   384
      _Version        =   "6.7.8988"
      Child1          =   "picMenu"
      MinWidth1       =   792
      MinHeight1      =   336
      Width1          =   3504
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   996
      MinHeight2      =   336
      Width2          =   996
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   324
      Width3          =   948
      NewRow3         =   0   'False
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   324
         Left            =   6852
         ScaleHeight     =   324
         ScaleWidth      =   792
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   24
         Width           =   792
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   40
            Top             =   12
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   402
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   132
         ScaleHeight     =   336
         ScaleWidth      =   3348
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   24
         Width           =   3348
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   552
            Left            =   1836
            TabIndex        =   34
            Top             =   12
            Width           =   1056
            _ExtentX        =   1863
            _ExtentY        =   974
            ButtonWidth     =   1545
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  ImageIndex      =   1
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Application"
                        Text            =   "Application"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   552
            Left            =   996
            TabIndex        =   33
            Top             =   12
            Width           =   744
            _ExtentX        =   1312
            _ExtentY        =   974
            ButtonWidth     =   1080
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Application"
                        Text            =   "Application"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Contact"
                        Text            =   "Contact"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   276
            Left            =   0
            TabIndex        =   48
            Top             =   0
            Width           =   888
            _ExtentX        =   1566
            _ExtentY        =   487
            ButtonWidth     =   1143
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   5
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Lawyer"
                        Text            =   "Lawyer"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "LawArea"
                        Text            =   "Law Area"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Limit"
                        Text            =   "Limit"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Insurer"
                        Text            =   "Insurer"
                     EndProperty
                     BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Question"
                        Text            =   "Question"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   3660
         ScaleHeight     =   336
         ScaleWidth      =   3012
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   24
         Width           =   3012
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   276
            Left            =   0
            TabIndex        =   31
            Top             =   12
            Width           =   1044
            _ExtentX        =   1842
            _ExtentY        =   487
            ButtonWidth     =   1397
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   264
      Left            =   0
      TabIndex        =   35
      Top             =   5928
      Width           =   7716
      _ExtentX        =   13610
      _ExtentY        =   466
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5292
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "5/24/2002"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "3:10 PM"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   576
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   60
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Application"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator999 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewLawyer 
            Caption         =   "&Lawyer"
         End
         Begin VB.Menu mnuFileNewLawArea 
            Caption         =   "Law &Area"
         End
         Begin VB.Menu mnuFileNewLimit 
            Caption         =   "Li&mit"
         End
         Begin VB.Menu mnuFileNewInsurer 
            Caption         =   "I&nsurer"
         End
         Begin VB.Menu mnuFileNewQuestion 
            Caption         =   "&Question"
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Diary"
            Index           =   0
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   1
         End
      End
      Begin VB.Menu mnuSeparator847 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To "
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator3412 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Application"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact"
         Index           =   1
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   2
      End
      Begin VB.Menu mnuSeparator388 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator3832 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "T&ab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Lawyer"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "L&aw Area"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "Li&mit"
            Index           =   2
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "In&surer"
            Index           =   3
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Question"
            Index           =   4
         End
      End
      Begin VB.Menu mnuSeparator123 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings"
      End
      Begin VB.Menu mnuSeparator2384 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuViewBrowseApplication 
         Caption         =   "&Browse Application"
         Enabled         =   0   'False
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuViewBrowseFirm 
         Caption         =   "Browse &Firm"
      End
      Begin VB.Menu mnuSeparator5673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeparator8343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&Workflow..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About LAW Explorer..."
      End
   End
End
Attribute VB_Name = "frmApplicationOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
' ApplicationOpen
'
'
' Any framework changes to ApplicationOpen should be propogated to the following
' forms:
' -ClaimOpen
' -FirmDiaryOpen
' -FirmOpen
' -LawyerOpen
' -PolicyOpen
' -CLEEventOpen
'

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
  
Private mApplicationHandle As String
Private mFirmHandle As String
Private mEventHandle As String

'Public FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

' Detail Tabs
'Private ApplicationLawyer As Dataset
'Private mApplicationLawArea As Dataset
'Private ApplicationLimit As Dataset
'Private mApplicationQuestion As Dataset
'Private mReference As Dataset
'Private FirmInsurer As Dataset
'Private mFirmViewData As DataProvider
'Private Person As Dataset
'Private mContact As Dataset



'
' FormInterface Interface
'

'
' Called when a child is unloaded
''
'Public Sub FormChildUnload(ByRef fInterface As FormInterface)
'  Refresh
'  FormRefresh
'  Show
'End Sub
'
''
'' Called when the form is loaded from the FormInterface
''
'Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
'  Set FormMgr.ParentInterface = ParentInterface
'End Sub
'
''
'' Called when a parent is unloaded and the interface is still valid
''
'Public Sub FormParentUnload()
'  Set mListView = Nothing
'  FormRefresh
'End Sub
'
'Public Sub FormRefresh()
'  Dim Pointer As New WaitPointer
'  Pointer.Pointer = vbHourglass
'
'  Dim DataFields As Collection
'  Set DataFields = Factory.ApplicationService.Find(mApplicationHandle)
'  SetFields lblDataField, DataFields, "Application"
'
'  mFirmHandle = DataFields("firmId")
'  mEventHandle = DataFields(DatabaseName.EVENT_KEY)
'  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
'  If (mEventHandle = vbNullString) Then
'    txtNote.Text = vbNullString
'  Else
'    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
'  End If
'
'  ' Set from FirmView
'  SetFields lblDataField, mFirmViewData.Find(DatabaseName.FIRM_KEY, mFirmHandle), "ContactFirm"
'
'  Caption = "Application (" & mApplicationHandle & ")"
'  lblDataField(17).Caption = lblDataField(17).Caption & " County" 'somecounty County
'  lblDataField(7).Caption = lblDataField(7).Caption & _
'     ", " & lblDataField(8).Caption & "  " _
'     & lblDataField(9).Caption 'city, state  zip in city field
'  lblDataField(1).Caption = lblDataField(1).Caption & _
'     " (" & lblDataField(0).Caption & ")" 'adds firmId to end of firmName
'  lblDataField(3).Caption = lblDataField(3).Caption & " (phone)" 'add phone
'  lblDataField(18).Caption = lblDataField(18).Caption & " (fax)"
'
'  'Open applicationLawyer Dataset
'  ApplicationLawyer.DataProvider.Filter = """applicationId"" = '" & mApplicationHandle & "'"
'
'  'Open mApplicationLawArea dataset
'  mApplicationLawArea.DataProvider.Filter = """applicationId"" = '" & mApplicationHandle & "'"
'
'  'Open applicationLimit dataset
'  ApplicationLimit.DataProvider.Filter = """applicationId"" = '" & mApplicationHandle & "'"
'
'  'Open insurer dataset
'  FirmInsurer.DataProvider.Filter = """firmId"" = '" & mFirmHandle & "'"
'
'  'Open mApplicationQuestion dataset
'  mApplicationQuestion.DataProvider.Filter = """applicationId"" = '" & mApplicationHandle & "'"
'
'  ' Standard Navigation Update.
'  FormUtil.NavigationRefresh Me, mListView, mListIndex
'
'  SSTabRefresh
'
'  lblLawyerCount = GetDouble(lblDataField(13)) + GetDouble(lblDataField(20)) + GetDouble(lblDataField(21))
'
'  Refresh
'End Sub

'
' Set ListIndex from underlying view.  How we pull the field is
' dependent upon the list item type.  In this example, if we are
' opening from an event view, there is a different method for
' pulling the appliction id.
''
'Private Property Let ListIndex(vData As Integer)
'  mListIndex = vData
'  mListView.SelectedItem.Selected = False
'  mListView.ListItems(vData).Selected = True
'  Select Case mListItemType
'    Case "Application"
'      mApplicationHandle = ListItemText(mListView.SelectedItem, "applicationId")
'    Case "Event"
'      Dim Handle As String
'      Handle = Factory.ReferenceService.ReferenceId1(ListItemText(mListView.SelectedItem, "eventId"), DatabaseName.APPLICATION_TABLE)
'      If Handle = vbNullString Then
'        MsgBox "Underlying event does not reference an Application."
'        Exit Property
'      End If
'      mApplicationHandle = Handle
'  End Select
'End Property
'
'Private Function ListViewDataProvider(Index As Integer) As DataProvider
'  Select Case Index
'  Case 0
'    Set ListViewDataProvider = ApplicationLawyer.DataProvider
'  Case 1
'    Set ListViewDataProvider = mApplicationLawArea.DataProvider
'  Case 2
'    Set ListViewDataProvider = ApplicationLimit.DataProvider
'  Case 3
'    Set ListViewDataProvider = FirmInsurer.DataProvider
'  Case 4
'    Set ListViewDataProvider = mApplicationQuestion.DataProvider
'  End Select
'End Function
'
'
'Public Sub ShowByListView(View As ListView, Optional ListItemType As String = "Application")
'  If View.ListItems.Count = 0 Then
'    MsgBox "There are no applications to view or edit.  To insert, press [Insert]."
'    Exit Sub
'  End If
'  Set mListView = View
'  mListItemType = ListItemType
'  ListIndex = View.SelectedItem.Index
'  Show
'End Sub
'
'Public Sub ShowByPrimaryKey(ApplicationID As String, Optional ShowTab As Integer = 0)
'  mApplicationHandle = ApplicationID
'  mTab = ShowTab
'  Show
'End Sub
'
'Public Sub SSTabRefresh()
'  Dim Source As DataProvider, View As ListView, Index As Variant
'  Set View = lvView(mTab)
'  Set Source = ListViewDataProvider(mTab)
'
'  With View
'    'Ensure first tab is open.
'    If SSTab.Tab = mTab Then
'      If .ListItems.Count > 0 Then
'        Index = IIf(.SelectedItem.Key = "", .SelectedItem.Index, .SelectedItem.Key)
'        .Visible = False
'        .SelectedItem.Selected = False
'        ListViewLoad View, Source
'        On Error Resume Next
'        .ListItems(Index).Selected = True
'        .Visible = True
'        .SetFocus
'      Else
'        ListViewLoad View, Source
'      End If
'    Else
'      SSTab.Tab = mTab
'    End If
'
'    sbStatusBar.Panels(1).Text = Str(.ListItems.Count) + " object(s)"
'    sbStatusBar.Panels(2).Text = Source.Source
'
'    ' Custom extension for calculating law area total.
'    If mTab = 1 Then
'      Dim Total As Integer, Item As Variant
'      For Each Item In .ListItems
'        Total = Item.ListSubItems("lawAreaPercent") + Total
'      Next
'      sbStatusBar.Panels(1).Text = sbStatusBar.Panels(1).Text & " - " & Total & "%"
'    End If
'  End With
'End Sub
'
'Public Sub SSTabResize()
'  Dim Index As Integer
'  Index = SSTab.Tab
'  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
'  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
'End Sub
''
' End General Methods
'
'
'
'
' Begin Controls
'

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Initialize()
  'Initialize the ContactFirm Dataset
 ' Set mFirmViewData = LawSession.NewDataProvider(DatabaseName.FIRM_VIEW)
  
  'Initialize the ApplicationLawyer Dataset
  'Set ApplicationLawyer = New ApplicationLawyerDataset
  'Set ApplicationLawyer.Session = LawSession
   
  'Initialize ApplicationLawArea
  'Set mApplicationLawArea = NewGenericDataset(DatabaseName.APPLICATIONLAWAREA_VIEW)
   
  'Initialize the Limit Dataset
  'Set ApplicationLimit = New ApplicationLimitDataset
  'Set ApplicationLimit.Session = LawSession
   
  'Initialize the Person Dataset
 ' Set Person = New PersonDataset
 ' Set Person.Session = LawSession
   
  'Initialize the Insurer Dataset
  'Set FirmInsurer = New FirmInsurerDataset
  'Set FirmInsurer.Session = LawSession
   
  'Initialize the mApplicationQuestion Dataset
  'Set mApplicationQuestion = NewGenericDataset(DatabaseName.APPLICATIONQUESTION_VIEW)
  
  'Set mContact = NewGenericDataset(DatabaseName.CONTACT_TABLE)
   
  'Event for scanning through event records.
  'Set mReference = New ReferenceDataset
  'Set mReference.Session = LawSession
   
End Sub

'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyL
        mnuFileNewLawyer_Click
      Case vbKeyA
        mnuFileNewLawArea_Click
      Case vbKeyM
        mnuFileNewLimit_Click
      Case vbKeyI
        mnuFileNewInsurer_Click
      Case vbKeyQ
        mnuFileNewQuestion_Click
    End Select
    Exit Sub
  End If
  
  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      Select Case SSTab.Tab
        Case 0:   'Lawyer
          mnuFileNewLawyer_Click
        Case 1:   'Law Area
          mnuFileNewLawArea_Click
        Case 2:    'Limit
          mnuFileNewLimit_Click
        Case 3:    'Insurer
          mnuFileNewInsurer_Click
        Case 4:    'Question
          mnuFileNewQuestion_Click
      End Select
  End Select
End Sub


Private Sub Form_Load()
  'Load to from to previous position
  'FormUtil.DeserializePosition Me
  
  ' Setup Toolbar.
'  FormUtil.SizeSingleToolbar tbrInsert, 0
'  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
'  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
'
'  cbrCoolBar.Bands(1).MinHeight = tbrInsert.Height + 24
'
'  FormUtil.EditLoadImages Me
'  FormUtil.MenuLoad Me
  
  ' Custom Menu Shortcut Adjustments
  mnuFileNewLawyer.Caption = mnuFileNewLawyer.Caption + Chr(9) + "Ctrl+Shift+L"
  mnuFileNewLawArea.Caption = mnuFileNewLawArea.Caption + Chr(9) + "Ctrl+Shift+A"
  mnuFileNewLimit.Caption = mnuFileNewLimit.Caption + Chr(9) + "Ctrl+Shift+M"
  mnuFileNewInsurer.Caption = mnuFileNewInsurer.Caption + Chr(9) + "Ctrl+Shift+I"
  mnuFileNewQuestion.Caption = mnuFileNewQuestion.Caption + Chr(9) + "Ctrl+Shift+Q"
  
'  mnuFileNewQuestion.Caption = "&Question" + Chr(9) + "Ctrl+Shift+Q"
  mnuEditEditRow.Caption = mnuEditEditRow.Caption + Chr(9) + "Enter"
'
'  ' Setup Form Manager
'  FormMgr.Load Me
'
'  FormRefresh
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub

' Expand body
Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)
  mnuEdit_Click 1
End Sub

Private Sub Frame4_DblClick()
  mnuEdit_Click 0
End Sub

Private Sub fraNote_DblClick()
  If mnuToolsNotepad.Enabled Then
    mnuToolsNotepad_Click
  Else
    MsgBox "Please add initial note using the Edit, Firm menu."
  End If
End Sub


Private Sub lblLaywerCount_Click()

End Sub

Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvView_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0 'Application
      FormUtil.ShowByPrimaryKey New frmApplicationEdit, mApplicationHandle, FormMgr
    
    Case 1 'Contact
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowByFirm mFirmHandle, "Firm"

    Case 2 'Firm
      FormUtil.ShowByPrimaryKey New frmFirmEdit, mFirmHandle, FormMgr
  End Select
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If
  
  If lvView(SSTab.Tab).ListItems.Count = 0 Then
    MsgBox "There are no items to delete."
    Exit Sub
  End If
  
  Select Case SSTab.Tab
    Case 0: 'Lawyer
      With lvView(0)
        Call Factory.ApplicationLawyerService.DeleteApplicationLawyer( _
            ListItemText(.SelectedItem, "applicationId"), ListItemText(.SelectedItem, "lawyerId"), _
            vbNullStringArray, vbNullStringArray)
      End With
    Case 1: 'LawArea
      With lvView(1)
        Call Factory.ApplicationLawAreaService.DeleteApplicationLawArea( _
            ListItemText(.SelectedItem, "applicationId"), ListItemText(.SelectedItem, "lawAreaId"), _
            vbNullStringArray, vbNullStringArray)
      End With
    Case 2: 'Limit
      With lvView(2)
        Call Factory.ApplicationLimitService.DeleteApplicationLimit( _
            ListItemText(.SelectedItem, "applicationLimitId"), _
            vbNullStringArray, vbNullStringArray)
      End With
    Case 3: 'Insurer
      With lvView(3)
        Call Factory.FirmInsurerService.DeleteFirmInsurer( _
            ListItemText(.SelectedItem, "firmId"), _
            ListItemText(.SelectedItem, "insurerId"), _
            ListItemText(.SelectedItem, "effectiveDate"), _
            vbNullStringArray, vbNullStringArray)
      End With
    Case 4: 'Question
      With lvView(4)
        Call Factory.ApplicationQuestionService.DeleteApplicationQuestion( _
            ListItemText(.SelectedItem, "applicationId"), ListItemText(.SelectedItem, "questionId"), _
            vbNullStringArray, vbNullStringArray)
      End With
  End Select
  FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case 0:   'Lawyer
      Dim fLawyer As New frmApplicationLawyerEdit
      FormMgr.Add fLawyer
      fLawyer.ShowByListView lvView(0)
      
    Case 1:   'Law Area
      Dim fLawArea As New frmApplicationLawAreaEdit
      FormMgr.Add fLawArea
      fLawArea.ShowByListView lvView(1)
    
    Case 2:    'Limit
      Dim fLimit As New frmApplicationLimitEdit
      FormMgr.Add fLimit
      fLimit.ShowByListView lvView(2)
    
    Case 3:    'Insurer
      Dim fInsurer As New frmFirmInsurerEdit
      FormMgr.Add fInsurer
      fInsurer.ShowByListView lvView(3)
    
    Case 4:    'Question
      Dim fQuestion As New frmApplicationQuestionEdit
      FormMgr.Add fQuestion
      fQuestion.ShowByListView lvView(4)
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmFirmDiaryOpen, mFirmHandle, FormMgr
    Case 1
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.APPLICATION_REF, DatabaseName.APPLICATION_TABLE, mApplicationHandle, mApplicationHandle
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog DatabaseName.APPLICATION_TABLE, mApplicationHandle, Index
End Sub


Private Sub mnuFileNew_Click()
  FormUtil.ShowNew New frmApplicationEdit, FormMgr, mFirmHandle
End Sub

'
' Edit/Insert Routines
'
Private Sub mnuFileNewInsurer_Click()
  Dim fForm As New frmFirmInsurerEdit
  FormMgr.Add fForm
  fForm.ShowNew mFirmHandle
End Sub

Private Sub mnuFileNewLawArea_Click()
  Dim fForm As New frmApplicationLawAreaEdit
  FormMgr.Add fForm
  fForm.ShowNew mApplicationHandle
End Sub

Private Sub mnuFileNewLawyer_Click()
  Dim fForm As New frmApplicationLawyerEdit
  FormMgr.Add fForm
  fForm.ShowNew mApplicationHandle
End Sub

Private Sub mnuFileNewLimit_Click()
  Dim fForm As New frmApplicationLimitEdit
  FormMgr.Add fForm
  fForm.ShowNew mApplicationHandle
End Sub

Private Sub mnuFileNewQuestion_Click()
  Dim fForm As New frmApplicationQuestionEdit
  FormMgr.Add fForm
  fForm.ShowNew mApplicationHandle
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.APPLICATION_TABLE, mApplicationHandle
  End If
End Sub

Private Sub mnuHelpAbout_Click()
  Dim fAbout As New frmAbout
  fAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
    Dim nRet As Integer

    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hWnd, App.HelpFile, 3, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If
End Sub


Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.APPLICATION_TABLE, FormMgr
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.APPLICATION_TABLE, lawActionNew
End Sub

Private Sub mnuViewBrowseApplication_Click()
  ShellExecute 0&, vbNullString, HttpServer & WebApplication & WebService & "applicationopen?key=" + mApplicationHandle, vbNullString, _
      vbNullString, SW_SHOWNORMAL
End Sub

Private Sub mnuViewBrowseFirm_Click()
  ShellExecute 0&, vbNullString, HttpServer & WebApplication & WebService & "firmopen?key=" + mFirmHandle, vbNullString, _
      vbNullString, SW_SHOWNORMAL
End Sub

'
' Standard choose columns for the current tab's list view.
'
Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'
' Standard view next menu
'
Private Sub mnuViewNext_Click()
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

'
' Standard view previous menu
'
Private Sub mnuViewPrevious_Click()
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

'
' Standard view refresh menu
'
Public Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

'
' Standard view a specific tab
'
Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

'
' Standard save settings
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

'
' Standard view toolbar.
'
Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  SSTabResize
End Sub


Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  'SSTabResize
End Sub

Private Sub picMenu_Resize()
  tbrInsert.Top = (picMenu.Height - tbrInsert.Height) / 2
  tbrEdit.Top = (picMenu.Height - tbrEdit.Height) / 2
  tbrBrowse.Top = (picMenu.Height - tbrBrowse.Height) / 2
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  mTab = SSTab.Tab
  'SSTabRefresh

  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
   ' SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub




Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1 ' lawyer
      mnuFileNewLawyer_Click
    Case 2 ' Law Area
      mnuFileNewLawArea_Click
    Case 3 ' Limit
      mnuFileNewLimit_Click
    Case 4 ' Insurer
      mnuFileNewInsurer_Click
    Case 5 ' Question
      mnuFileNewQuestion_Click
  End Select
End Sub

Private Sub tbrInsert_Click()
  Select Case SSTab.Caption
    Case "Lawyer"
      mnuFileNewLawyer_Click
    Case "Law Area"
      mnuFileNewLawArea_Click
    Case "Limit"
      mnuFileNewLimit_Click
    Case "Insurer"
      mnuFileNewInsurer_Click
    Case "Question"
      mnuFileNewQuestion_Click
  End Select
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuEdit_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrEdit_Click()
  mnuEdit_Click 0
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1
      ' application browse code here
    Case 2
      mnuViewBrowseFirm_Click
  End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  MsgBox "Toolbar1_ButtonClick"
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1
        MsgBox "Toolbar1_ButtonMenuClick 1"
    Case 2
        MsgBox "Toolbar1_ButtonMenuClick 2"
    Case 3
        MsgBox "Toolbar1_ButtonMenuClick 3"
  End Select
  End Sub

Private Sub Toolbar1_Click()
 ' MsgBox "Toolbar1_Click"
End Sub
