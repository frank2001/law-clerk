VERSION 5.00
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSequence 
   Caption         =   "Sequence"
   ClientHeight    =   4128
   ClientLeft      =   72
   ClientTop       =   612
   ClientWidth     =   3888
   LinkTopic       =   "Form1"
   ScaleHeight     =   4128
   ScaleWidth      =   3888
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3708
      Left            =   0
      ScaleHeight     =   3708
      ScaleWidth      =   3888
      TabIndex        =   6
      Top             =   336
      Width           =   3888
      Begin VB.TextBox txtSize 
         Height          =   288
         Left            =   552
         TabIndex        =   14
         Text            =   "5"
         Top             =   72
         Width           =   432
      End
      Begin VB.ListBox lboList 
         Height          =   1584
         Left            =   48
         TabIndex        =   13
         Top             =   1308
         Width           =   3588
      End
      Begin VB.TextBox txtAdd 
         Height          =   288
         Left            =   516
         TabIndex        =   12
         Top             =   528
         Width           =   3084
      End
      Begin VB.CommandButton cmdNew 
         Caption         =   "&New"
         Default         =   -1  'True
         Height          =   336
         Left            =   1320
         TabIndex        =   11
         Top             =   48
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Cancel          =   -1  'True
         Caption         =   "&Close"
         Height          =   336
         Left            =   2412
         TabIndex        =   10
         Top             =   3324
         Width           =   1200
      End
      Begin VB.CommandButton cmdForward 
         Caption         =   "&Forward >>"
         Enabled         =   0   'False
         Height          =   288
         Left            =   1344
         TabIndex        =   9
         Top             =   984
         Width           =   1028
      End
      Begin VB.CommandButton cmdBack 
         Caption         =   "<< &Back"
         Enabled         =   0   'False
         Height          =   288
         Left            =   108
         TabIndex        =   8
         Top             =   984
         Width           =   1028
      End
      Begin VB.CommandButton cmdHome 
         Caption         =   "&Home"
         Height          =   288
         Left            =   2520
         TabIndex        =   7
         Top             =   996
         Width           =   1028
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Add:"
         Height          =   192
         Index           =   1
         Left            =   36
         TabIndex        =   15
         Top             =   564
         Width           =   336
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   3888
      _ExtentX        =   6858
      _ExtentY        =   593
      BandCount       =   1
      _CBWidth        =   3888
      _CBHeight       =   336
      _Version        =   "6.7.8988"
      Child1          =   "picNavigation"
      MinHeight1      =   288
      Width1          =   2352
      NewRow1         =   0   'False
      Begin VB.PictureBox picNavigation 
         BorderStyle     =   0  'None
         Height          =   288
         Left            =   24
         ScaleHeight     =   288
         ScaleWidth      =   3792
         TabIndex        =   3
         Top             =   24
         Width           =   3792
         Begin MSComctlLib.Toolbar tbrForward 
            Height          =   264
            Left            =   1152
            TabIndex        =   4
            Top             =   0
            Width           =   624
            _ExtentX        =   1101
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            ToolTips        =   0   'False
            AllowCustomize  =   0   'False
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  ImageKey        =   "right"
                  Style           =   5
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrBack 
            Height          =   276
            Left            =   0
            TabIndex        =   5
            Top             =   0
            Width           =   1128
            _ExtentX        =   1990
            _ExtentY        =   487
            ButtonWidth     =   1228
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Back"
                  ImageKey        =   "left"
                  Style           =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Hidden Add"
      Enabled         =   0   'False
      Height          =   336
      Left            =   2556
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   2088
      Width           =   1200
   End
   Begin MSComctlLib.ImageList imlArrows 
      Left            =   600
      Top             =   3756
      _ExtentX        =   826
      _ExtentY        =   826
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlArrowsDisabled 
      Left            =   0
      Top             =   3756
      _ExtentX        =   826
      _ExtentY        =   826
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Size:"
      Height          =   192
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   180
      Width           =   348
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileExit 
         Caption         =   "&Exit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewGoTo 
         Caption         =   "G&o To"
         Begin VB.Menu mnuViewGoToBack 
            Caption         =   "&Back"
         End
         Begin VB.Menu mnuViewGoToForward 
            Caption         =   "&Forward"
         End
         Begin VB.Menu mnuSeparator833 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewGoToHome 
            Caption         =   "&Home"
         End
      End
   End
End
Attribute VB_Name = "frmSequence"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mSequence As Sequence

Private Sub ListRefresh()
  lboList.Clear
  Dim I As Integer
  For I = 0 To mSequence.ListCount - 1
    lboList.AddItem I & vbTab & mSequence.List(I)
  Next I
  If I > 0 Then
    lboList.ListIndex = 0
  End If
  SequenceRefresh
End Sub


Private Sub SequenceRefresh()
  Dim Back, Forward As Boolean
  If Not mSequence Is Nothing Then
    With mSequence
      Back = .ListIndex < .ListCount - 1
      Forward = .ListIndex > 0
    End With
  End If
  
  cmdBack.Enabled = Back
  tbrBack.Buttons(1).Enabled = Back
  mnuViewGoToBack.Enabled = Back
  cmdForward.Enabled = Forward
  tbrForward.Buttons(1).Enabled = Forward
  mnuViewGoToForward.Enabled = Forward
End Sub


Private Sub cmdAdd_Click()
  mSequence.AddItem txtAdd.Text
  ListRefresh
End Sub

Private Sub cmdBack_Click()
  mnuViewGoToBack_Click
End Sub

Private Sub cmdClose_Click()
  Unload Me
End Sub


Private Sub cmdForward_Click()
  mnuViewGoToForward_Click
End Sub


Private Sub cmdHome_Click()
  mnuViewGoToHome_Click
End Sub


Private Sub cmdNew_Click()
  Set mSequence = New Sequence
  mSequence.Size = txtSize.Text
  lboList.Clear
  txtSize.Enabled = False
  cmdNew.Enabled = False
  cmdAdd.Enabled = True
  cmdAdd.Default = True
End Sub

'
' Depends on KeyPreview True in Form_Load
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Ensure only the Alt key is down.
  If (vbAltMask And Shift) And Not (vbShiftMask And Shift) And Not (vbCtrlMask And Shift) Then
    Select Case KeyCode
      Case vbKeyLeft
        If mnuViewGoToBack.Enabled Then
          mnuViewGoToBack_Click
        End If
        KeyCode = 0
        Shift = 0
      Case vbKeyRight
        If mnuViewGoToForward.Enabled Then
          mnuViewGoToForward_Click
        End If
        KeyCode = 0
        Shift = 0
      Case vbKeyHome
        mnuViewGoToHome_Click
        Shift = 0
        KeyCode = 0
    End Select
  End If
End Sub


' Important form settings that code relies on
' for proper functionality should be placed here.
' Form properties that affect visibility are OK to use.
Private Sub Form_Load()
  ' Menu shortcuts done in code.  Ensure
  ' KeyPreview and adjust captions for short cuts.
  KeyPreview = True
  mnuViewGoToBack.Caption = mnuViewGoToBack.Caption + vbTab + "Alt+Left Arrow"
  mnuViewGoToForward.Caption = mnuViewGoToForward.Caption + vbTab + "Alt+Right Arrow"
  mnuViewGoToHome.Caption = mnuViewGoToHome.Caption + vbTab + "Alt+Home"
  
  ' Initialize Controls
  SequenceRefresh
  
  
  'Load the images
  imlArrows.ListImages.Add 1, "Back", LoadPicture(App.Path & "\Back.bmp")
  imlArrows.ListImages.Add 2, "Forward", LoadPicture(App.Path & "\Forward.bmp")
  imlArrows.UseMaskColor = True

  imlArrowsDisabled.ListImages.Add 1, "DisBack", LoadPicture(App.Path & "\DisabledBack.bmp")
  imlArrowsDisabled.ListImages.Add 2, "DisForward", LoadPicture(App.Path & "\DisabledForward.bmp")
  imlArrows.UseMaskColor = True
  
  ' set the image list to the toolbar
  tbrBack.ImageList = imlArrows
  tbrForward.ImageList = imlArrows
  tbrBack.DisabledImageList = imlArrowsDisabled
  tbrForward.DisabledImageList = imlArrowsDisabled
  tbrBack.Buttons(1).Image = "Back"
  tbrForward.Buttons(1).Image = "Forward"


  ' Adjust height of coolbar bands for toolbars.  Toolbars are
  ' expandable depending on windows font settings
  cbrCoolBar.Bands(1).MinHeight = tbrForward.ButtonHeight + (tbrForward.Top * 2)
End Sub


Private Sub lboList_Click()
  mSequence.ListIndex = lboList.ListIndex
  SequenceRefresh
End Sub


'
' Menus contain the master functionality.  Everything
' should be in a menu somewhere.  The only exception
' is default button behavior.
'

Private Sub mnuFileExit_Click()
  Unload Me
End Sub


Private Sub mnuViewGoToBack_Click()
  'mSequence.ListIndex = mSequence.ListIndex + 1
  lboList.ListIndex = lboList.ListIndex + 1
End Sub


Private Sub mnuViewGoToForward_Click()
  'mSequence.ListIndex = mSequence.ListIndex - 1
  lboList.ListIndex = lboList.ListIndex - 1
End Sub

Private Sub mnuViewGoToHome_Click()
  If mSequence Is Nothing Then
    Exit Sub
  End If
    
  Dim Count As Integer
  Count = mSequence.ListCount
  If Count > 0 Then
    'mSequence.ListIndex = mSequence.ListCount - 1
    lboList.ListIndex = lboList.ListCount - 1
  End If
End Sub

Private Sub tbrBack_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuViewGoToBack_Click
End Sub

Private Sub tbrBack_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  tbrBack.Buttons(1).ButtonMenus.Clear
  Dim I As Integer
  For I = mSequence.ListIndex + 1 To mSequence.ListCount - 1
    tbrBack.Buttons(1).ButtonMenus.Add , , mSequence.List(I)
  Next I
End Sub


Private Sub tbrBack_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  lboList.ListIndex = ButtonMenu.Index + lboList.ListIndex
End Sub


Private Sub tbrForward_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuViewGoToForward_Click
End Sub


Private Sub tbrForward_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  tbrForward.Buttons(1).ButtonMenus.Clear
  Dim I As Integer
  For I = 0 To mSequence.ListIndex - 1
    tbrForward.Buttons(1).ButtonMenus.Add , , mSequence.List(I)
  Next I
End Sub


Private Sub tbrForward_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  lboList.ListIndex = ButtonMenu.Index - 1
End Sub
