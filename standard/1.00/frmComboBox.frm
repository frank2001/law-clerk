VERSION 5.00
Begin VB.Form frmComboBox 
   Caption         =   "Combo Box"
   ClientHeight    =   2472
   ClientLeft      =   72
   ClientTop       =   336
   ClientWidth     =   3696
   LinkTopic       =   "Form1"
   ScaleHeight     =   2472
   ScaleWidth      =   3696
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      Default         =   -1  'True
      Height          =   288
      Left            =   2580
      TabIndex        =   7
      Top             =   456
      Width           =   1008
   End
   Begin VB.TextBox txtSize 
      Height          =   288
      Left            =   1428
      TabIndex        =   5
      Text            =   "15"
      Top             =   888
      Width           =   720
   End
   Begin VB.CommandButton cmdSize 
      Caption         =   "Size"
      Height          =   288
      Left            =   276
      TabIndex        =   4
      Top             =   876
      Width           =   1028
   End
   Begin VB.TextBox txtIniFile 
      Height          =   288
      Left            =   288
      TabIndex        =   3
      Text            =   "combo.ini"
      Top             =   1608
      Width           =   1416
   End
   Begin VB.CommandButton cmdDeserialize 
      Caption         =   "Deserialize"
      Height          =   288
      Left            =   1368
      TabIndex        =   2
      Top             =   1248
      Width           =   1028
   End
   Begin VB.CommandButton cmdSerialize 
      Caption         =   "Serialize"
      Height          =   288
      Left            =   264
      TabIndex        =   1
      Top             =   1248
      Width           =   1028
   End
   Begin VB.ComboBox cboCombo 
      Height          =   288
      Left            =   240
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   456
      Width           =   2208
   End
   Begin VB.Label Label1 
      Caption         =   "items"
      Height          =   216
      Left            =   2256
      TabIndex        =   6
      Top             =   924
      Width           =   672
   End
End
Attribute VB_Name = "frmComboBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboCombo_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF3 Then
    ComboBoxUtil.AddText cboCombo, True, txtSize.Text
  End If
End Sub

Private Sub cmdAdd_Click()
  ComboBoxUtil.AddText cboCombo, True, txtSize.Text
  cboCombo.SetFocus
End Sub

Private Sub cmdSize_Click()
  ComboBoxUtil.SetDropDownHeight Me, cboCombo, txtSize
End Sub
