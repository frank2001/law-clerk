VERSION 5.00
Begin VB.Form MenuLoad 
   Caption         =   "Form1"
   ClientHeight    =   4224
   ClientLeft      =   132
   ClientTop       =   708
   ClientWidth     =   1584
   LinkTopic       =   "Form1"
   ScaleHeight     =   4224
   ScaleWidth      =   1584
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdUnloadMenus 
      Caption         =   "Unload"
      Height          =   336
      Left            =   168
      TabIndex        =   1
      Top             =   3780
      Width           =   1200
   End
   Begin VB.CommandButton cmdLoadMenus 
      Caption         =   "Load"
      Height          =   336
      Left            =   180
      TabIndex        =   0
      Top             =   3276
      Width           =   1200
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "Menu Zero Alpha Root"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
End
Attribute VB_Name = "MenuLoad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdLoadMenus_Click()
  Dim I As Integer
  For I = 1 To 5
    Load mnuFileNew(I)
    mnuFileNew(I).Caption = "New Menu " & I
  Next I
  'Hide the first menu
  mnuFileNew(0).Visible = False
  
  mnuFileNewMenu.Visible = True
  cmdUnloadMenus.Enabled = True
  cmdLoadMenus.Enabled = False
  
End Sub

Private Sub cmdUnloadMenus_Click()
  'Show First menu item
  mnuFileNew(0).Visible = True
  'Unload all other items
  Dim I As Integer
  For I = 1 To mnuFileNew.UBound
    Unload mnuFileNew(I)
  Next I
  
  cmdUnloadMenus.Enabled = False
  cmdLoadMenus.Enabled = True
  mnuFileNewMenu.Visible = False
End Sub

Private Sub Form_Load()
  mnuFileNewMenu.Visible = False
  cmdUnloadMenus.Enabled = False
End Sub
