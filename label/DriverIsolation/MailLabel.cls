VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestMailLabel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private mPostNet As PostNet

Private mDeliveryColor As OLE_COLOR
Private mDeliveryFont As StdFont

Private mXMargin As Double
Private mYMargin As Double

Private mFontSize As Long

Private mPaperSize As Integer

Private mPaperHeight As Double
Private mPaperWidth As Double

Private Function GetPaperHeight(vPaperSize As Integer) As Double
  Dim Size As Double
  
  Select Case vPaperSize
    Case PrinterObjectConstants.vbPRPSEnv9 '(3 7/8 x 8 7/8 in)
      Size = 3.875 * 1440
    Case PrinterObjectConstants.vbPRPSEnv10 '(4 1/8 x 9 1/2 in)
      Size = 4.125 * 1440
    Case PrinterObjectConstants.vbPRPSEnv11 '(4 1/2 x 10 3/8 in)
      Size = 4.5 * 1440
    Case PrinterObjectConstants.vbPRPSEnv12 '(4 1/2 x 11 in)
      Size = 4.5 * 1440
    Case PrinterObjectConstants.vbPRPSEnv14 '(5 x 11 in)
      Size = 5 * 1440
    Case 250 ' (9 x 12 in)
      Size = 9 * 1440
    Case 251 ' (10 x 13in)
      Size = 10 * 1440
    Case Else
      Size = 0
  End Select

  GetPaperHeight = Size
End Function

Private Function GetPaperWidth(vPaperSize) As Double
  Dim Size As Double
  Select Case vPaperSize
    Case PrinterObjectConstants.vbPRPSEnv9 '(3 7/8 x 8 7/8 in)
      Size = 8.875 * 1440
    Case PrinterObjectConstants.vbPRPSEnv10 '(4 1/8 x 9 1/2 in)
      Size = 9.5 * 1440
    Case PrinterObjectConstants.vbPRPSEnv11 '(4 1/2 x 10 3/8 in)
      Size = 10.375 * 1440
    Case PrinterObjectConstants.vbPRPSEnv12 '(4 1/2 x 11 in)
      Size = 11 * 1440
    Case PrinterObjectConstants.vbPRPSEnv14 '(5 x 11 in)
      Size = 11 * 1440
    Case 250 ' (9 x 12 in)
      Size = 12 * 1440
    Case 251 ' (10 x 13in)
      Size = 13 * 1440
    Case Else
      Size = 0
  End Select
  GetPaperWidth = Size
End Function

Private Function SetCustomSize() As Boolean
On Error GoTo Exception
  
  Printer.Height = GetPaperHeight(mPaperSize)
  Printer.Width = GetPaperWidth(mPaperSize)
  
  MsgBox "The Printer.Height and Printer.Width have been customly set" & _
         Chr(13) & "The current PaperSize is " & Printer.PaperSize
  
  SetPaperBinToManual
  'Printer.PaperBin = PrinterObjectConstants.vbPRBNManual 'PrinterObjectConstants.vbPRBNAuto '
  'Printer.PaperSize = PrinterObjectConstants.vbPRPSUser
  SetCustomSize = True
  Exit Function
  
Exception:
  ' The custom settings failed
    MsgBox Err.Number & ": " & Err.Description & "  Could not set printer page height and width."
  SetCustomSize = False
End Function
Private Function SetPaperBinToManual() As Boolean
On Error GoTo Exception
  Printer.PaperBin = PrinterObjectConstants.vbPRBNManual 'PrinterObjectConstants.vbPRBNAuto '
  MsgBox "PaperBin set to manual"
  SetPaperBinToManual = True
  Exit Function
  
Exception:
  MsgBox Err.Number & ": " & Err.Description & "  Could not set printer to manual feed"
  SetPaperBinToManual = False
End Function

Private Function SetPaperSize() As Boolean
On Error GoTo Exception
  
  ' Should work if printer supports the selected size
  Printer.PaperSize = IIf(mPaperSize = 0, PrinterObjectConstants.vbPRPSEnv10, mPaperSize)
  SetPaperSize = True
  
  Exit Function
  
Exception:

  SetPaperSizeToUserDefined
  
  SetPaperSize = SetCustomSize
  
End Function
Private Function SetPaperSizeToUserDefined() As Boolean
On Error GoTo Exception
  
  ' Should work if printer supports the selected size
  Printer.PaperSize = PrinterObjectConstants.vbPRPSUser
  SetPaperSizeToUserDefined = True
  
  Exit Function
  
Exception:

  ' Setting the height and width automatically sets the
  ' pagesize to userdefined / custom, printers dont always
  ' accept the user defined pagesize when setting directly to
  ' the page size property
  MsgBox "Attempt to set PaperSize to UserDefined has failed."
  Printer.PaperSize = PrinterObjectConstants.vbPRPSLetter
  
End Function
Public Function PrintEnvelope(LabelText As String, Optional PrintBarCode As Boolean, Optional DeliveryPoint As String) As Boolean
  Dim oldPrinter As Integer
  Dim I As Integer
    
  'Save Current Printer
 ' For I = 0 To Printers.Count - 1
 '   If Printer.DeviceName = Printers(I).DeviceName Then
 '     oldPrinter = I: Exit For
 '   End If
 ' Next
  
  'Load Envelope Printer
'  For I = 0 To Printers.Count - 1
'    If Printers(I).DeviceName = DefaultEnvelopePrinter Then
'      Set Printer = Printers(I)
'      Exit For
'    End If
'  Next
  
  If LabelText = vbNullString Then Exit Function
  
  Dim addressArray() As String
  
  Dim PaperSizeOrig, OrientationOrig, PaperBinOrig As Long 'Stores default entry
  Dim ScaleModeOrig As Integer
    
  'Debug
  MsgBox "Saving Existing Printer infromation" & Chr(13) & _
      "PaperSize = " & Printer.PaperSize & Chr(13) & _
      "PaperBin = " & Printer.PaperBin & Chr(13) & _
      "Orientation = " & Printer.Orientation & Chr(13) & _
      "ScaleMode = " & Printer.ScaleMode
      
  PaperSizeOrig = Printer.PaperSize
  OrientationOrig = Printer.Orientation
  PaperBinOrig = Printer.PaperBin
  ScaleModeOrig = Printer.ScaleMode

On Error GoTo Exception
   
  ' Use functions for error handleing
  If Not SetPaperSize Then
    Err.Clear
    Err.Description = "Unable to set paper size settings to printer."
    If Not MsgBox(Err.Description & " Would you like to continue printing anyway?", vbYesNo, "Print Error") = vbYes Then
      Err.Raise 513
    End If
  End If
  
  'Set Orientation
  Printer.Orientation = vbPRORLandscape

  'Load font settings
  If Not mDeliveryFont Is Nothing Then
    Set Printer.Font = mDeliveryFont
  End If
  Printer.ForeColor = mDeliveryColor

  'Set the label position
  mXMargin = IIf(mXMargin = 0, Printer.ScaleWidth / 2, mXMargin)
  mYMargin = IIf(mYMargin = 0, Printer.ScaleHeight / 2, mYMargin)
  Printer.CurrentY = mYMargin
  Printer.CurrentX = mXMargin
  
  'Load the label's text
  addressArray = Split(LabelText, vbCrLf)
  
  If MsgBox("Envelope Printer Configuration: " & Chr(13) & _
      "PaperSize = " & Printer.PaperSize & Chr(13) & _
      "Height = " & Printer.Height & Chr(13) & _
      "Width = " & Printer.Width & Chr(13) & _
      "PaperBin = " & Printer.PaperBin & Chr(13) & _
      "Orientation = " & Printer.Orientation & Chr(13) & _
      "ScaleMode = " & Printer.ScaleMode & Chr(13) & _
      "CurrentX = " & Printer.CurrentX & Chr(13) & _
      "CurrentY = " & Printer.CurrentY & Chr(13) & _
      "ScaleHeight = " & Printer.ScaleHeight & Chr(13) & _
      "ScaleWidth = " & Printer.ScaleWidth & Chr(13) & _
      "DeviceName = " & Printer.DeviceName & Chr(13) & _
      "ScaleTop = " & Printer.ScaleTop & Chr(13) & _
      "ScaleLeft = " & Printer.ScaleLeft & Chr(13) & _
      Chr(13) & "Continue to print?", vbYesNo) = vbNo Then
    Err.Clear
    Err.Description = "User canceled print job. "
    Err.Raise 1
  End If

  'Print the label
  If PrintBarCode Then
    mPostNet.DrawText addressArray(UBound(addressArray)) & DeliveryPoint
  End If
  
  Printer.Print " "
 
  For I = LBound(addressArray) To UBound(addressArray)
    If Not addressArray(I) = vbNullString Then
      Printer.CurrentX = mXMargin
      Printer.Print addressArray(I)
    End If
  Next I
  
  Printer.EndDoc
  
  'Debug
  MsgBox "About to restore Existing Printer infromation" & Chr(13) & _
      "PaperSize = " & Printer.PaperSize & Chr(13) & _
      "PaperBin = " & Printer.PaperBin & Chr(13) & _
      "Orientation = " & Printer.Orientation & Chr(13) & _
      "ScaleMode = " & Printer.ScaleMode
  
  'Reset Settings
  Printer.PaperSize = PaperSizeOrig
  Printer.Orientation = OrientationOrig
  Printer.PaperBin = PaperBinOrig
  Printer.ScaleMode = ScaleModeOrig
  
  'Restore old printer
  'Set Printer = Printers(oldPrinter)
  PrintEnvelope = True
  Exit Function
  
Exception:
  
  MsgBox Err.Number & ": " & Err.Description & " Clerk was unable to print the envelope."
  
  
  MsgBox "An error has occurred. Attempting to restore printer settings" & Chr(13) & _
      "PaperSize = " & Printer.PaperSize & Chr(13) & _
      "PaperBin = " & Printer.PaperBin & Chr(13) & _
      "Orientation = " & Printer.Orientation & Chr(13) & _
      "ScaleMode = " & Printer.ScaleMode
  
  
  'Reset Settings
  MsgBox "Setting Paper size to " & PaperSizeOrig & "..."
  Printer.PaperSize = PaperSizeOrig
  MsgBox "Paper size set. Setting Orientation to " & OrientationOrig & "..."
  Printer.Orientation = OrientationOrig
  MsgBox "Orientation set. Setting PaperBin to " & PaperBinOrig & "..."
  Printer.PaperBin = PaperBinOrig
  MsgBox "PaperBin set. Setting ScaleMode to " & ScaleModeOrig & "..."
  Printer.ScaleMode = ScaleModeOrig
  MsgBox "ScaleMode set."
  
  'Restore old printer
  'Set Printer = Printers(oldPrinter)
  PrintEnvelope = False
  
End Function

Public Sub Class_Initialize()
  Set mPostNet = New PostNet
  Set mPostNet.DrawObject = Printer
  
  'Load default settings
'  mDeliveryColor = LocaleEnvelopeForeColor
'  mXMargin = LocaleEnvelopeCurrentX
'  mYMargin = LocaleEnvelopeCurrentY
'  Set mDeliveryFont = LocaleEnvelopeFont
'  mPaperSize = LocaleEnvelopePaperSize
  
  
End Sub

'
' Explicitly clean up
'
Public Sub Class_Terminate()
  Set mPostNet = Nothing
End Sub

Public Property Get DeliveryLeftMargin() As Long
  DeliveryLeftMargin = mXMargin
End Property

Public Property Let DeliveryLeftMargin(vData As Long)
  mXMargin = vData
End Property

Public Property Get DeliveryTopMargin() As Long
  DeliveryTopMargin = mYMargin
End Property

Public Property Let DeliveryTopMargin(vData As Long)
  mYMargin = vData
End Property

Public Property Let PaperSize(vData As PrinterObjectConstants)
  mPaperSize = vData
End Property

Public Property Get PaperSize() As Long
  PaperSize = mPaperSize
End Property

Public Property Get DeliveryFont() As StdFont
  Set DeliveryFont = mDeliveryFont
End Property

Public Property Set DeliveryFont(vData As StdFont)
  Set mDeliveryFont = vData
End Property

Public Property Get DeliveryColor() As OLE_COLOR
  DeliveryColor = mDeliveryColor
End Property

Public Property Let DeliveryColor(vData As OLE_COLOR)
  mDeliveryColor = vData
End Property



