VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmTest 
   Caption         =   "Test"
   ClientHeight    =   2652
   ClientLeft      =   48
   ClientTop       =   324
   ClientWidth     =   6276
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2652
   ScaleWidth      =   6276
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab SSTab1 
      Height          =   2376
      Left            =   24
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   72
      Width           =   6108
      _ExtentX        =   10774
      _ExtentY        =   4191
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   420
      TabCaption(0)   =   "Envelope"
      TabPicture(0)   =   "frmTest.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdOptions"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdCancel"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtDeliveryAddress"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdPrint"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      Begin VB.CommandButton cmdPrint 
         Caption         =   "&Print"
         Height          =   336
         Left            =   4752
         TabIndex        =   1
         Top             =   672
         Width           =   1200
      End
      Begin VB.TextBox txtDeliveryAddress 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1548
         Left            =   108
         MultiLine       =   -1  'True
         TabIndex        =   0
         Top             =   684
         Width           =   4560
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   4764
         TabIndex        =   2
         Top             =   1068
         Width           =   1200
      End
      Begin VB.CommandButton cmdOptions 
         Caption         =   "&Options"
         Height          =   336
         Left            =   4764
         TabIndex        =   3
         Top             =   1488
         Width           =   1200
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Delivery Address:"
         Height          =   192
         Left            =   120
         TabIndex        =   5
         Top             =   384
         Width           =   1284
      End
   End
End
Attribute VB_Name = "frmTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Delivery Address
Private mDeliveryAddress As String

' USPS Delivery Point Code
Private mDeliveryPoint As String

' Should the bar code be printed
Private mPrintBarcode As Integer

' The envelope size
Private mPaperSize As Integer

Private mDeliveryTop As Double
Private mDeliveryLeft As Double

Public OK As Boolean

Public Property Let DeliveryAddress(vData As String)
  mDeliveryAddress = vData
End Property

Public Property Get DeliveryPoint() As String
  DeliveryPoint = mDeliveryPoint
End Property

Public Property Let DeliveryPoint(vData As String)
  mDeliveryPoint = vData
End Property

Public Property Get PrintBarCode() As Integer
  PrintBarCode = mPrintBarcode
End Property

Public Property Let PrintBarCode(vData As Integer)
  mPrintBarcode = vData
End Property

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOptions_Click()
  Dim fOptions As New frmTestOptions
  
  ' Load current settings into the option form
  'CommonDialogUtil.SetFont fOptions.dlgDeliveryFont, txtDeliveryAddress.Font
  fOptions.dlgDeliveryFont.Color = txtDeliveryAddress.ForeColor
  fOptions.txtDeliveryPoint.Text = mDeliveryPoint
  fOptions.chkPrintBarcode.Value = mPrintBarcode
  fOptions.PaperSize = mPaperSize
  
  ' Convert X and y positions from pixels to inches
  If Not mDeliveryTop = 0 Then
    fOptions.txtDeliveryTop = FormatNumber((mDeliveryTop / 1440), 1)
  End If
  If Not mDeliveryLeft = 0 Then
    fOptions.txtDeliveryLeft = FormatNumber((mDeliveryLeft / 1440), 1)
  End If
  
  fOptions.Show vbModal
  If Not fOptions.OK Then
    Exit Sub
  End If

  ' Retrieve Settings
  'Set txtDeliveryAddress.Font = CommonDialogUtil.GetFont(fOptions.dlgDeliveryFont)
  txtDeliveryAddress.ForeColor = fOptions.dlgDeliveryFont.Color
  mDeliveryPoint = fOptions.txtDeliveryPoint.Text
  mPrintBarcode = fOptions.chkPrintBarcode.Value
  mPaperSize = fOptions.PaperSize
  mDeliveryTop = fOptions.GetDeliveryTopMargin
  mDeliveryLeft = fOptions.GetDeliveryLeftMargin
  
  Unload fOptions
End Sub

Private Sub cmdPrint_Click()
  'Dim Pointer As New WaitPointer
  'Pointer.Default
  
  'Load current settings and Print the envelope
  Dim Envelope As New TestMailLabel
  Envelope.DeliveryColor = txtDeliveryAddress.ForeColor
  Set Envelope.DeliveryFont = txtDeliveryAddress.Font
  Envelope.DeliveryTopMargin = mDeliveryTop '/ 1440
  Envelope.DeliveryLeftMargin = mDeliveryLeft ' / 1440
  Envelope.PaperSize = mPaperSize
  If Envelope.PrintEnvelope(txtDeliveryAddress.Text, mPrintBarcode = 1, mDeliveryPoint) Then
    MsgBox "Print Ok"
  Else
    MsgBox "Did not print"
  End If
End Sub

Private Sub mnuFileExit_Click()
  OK = True
  Hide
End Sub


Private Sub Form_Load()
  'mPrintBarcode = 1
  txtDeliveryAddress.Text = mDeliveryAddress
  
  'Load default settings
  
'  txtDeliveryAddress.ForeColor = LocaleEnvelopeForeColor
'  FormUtil.SetTextControlFont txtDeliveryAddress, LocaleEnvelopeFont
'  mPrintBarcode = LocaleEnvelopeBarCode
'  mPaperSize = LocaleEnvelopePaperSize
'  mDeliveryTop = LocaleEnvelopeCurrentY
'  mDeliveryLeft = LocaleEnvelopeCurrentX
  
End Sub

