VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmTestOptions 
   Caption         =   "Form1"
   ClientHeight    =   4032
   ClientLeft      =   48
   ClientTop       =   324
   ClientWidth     =   4308
   LinkTopic       =   "Form1"
   ScaleHeight     =   4032
   ScaleWidth      =   4308
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3984
      Left            =   0
      ScaleHeight     =   3984
      ScaleWidth      =   4308
      TabIndex        =   0
      Top             =   0
      Width           =   4308
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Default         =   -1  'True
         Height          =   336
         Left            =   1668
         TabIndex        =   2
         Top             =   3564
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2940
         TabIndex        =   1
         Top             =   3564
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3348
         Left            =   84
         TabIndex        =   3
         Top             =   132
         Width           =   4044
         _ExtentX        =   7133
         _ExtentY        =   5906
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Envelope"
         TabPicture(0)   =   "frmTestOptions.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraUSA"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraDelivery"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "cboEnvelopeSize"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dlgDeliveryFont"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         Begin MSComDlg.CommonDialog dlgDeliveryFont 
            Left            =   2688
            Top             =   36
            _ExtentX        =   677
            _ExtentY        =   677
            _Version        =   393216
         End
         Begin VB.ComboBox cboEnvelopeSize 
            Height          =   288
            ItemData        =   "frmTestOptions.frx":001C
            Left            =   108
            List            =   "frmTestOptions.frx":0035
            Style           =   2  'Dropdown List
            TabIndex        =   16
            Top             =   684
            Width           =   3792
         End
         Begin VB.Frame fraDelivery 
            Caption         =   "Delivery"
            Height          =   1128
            Left            =   108
            TabIndex        =   8
            Top             =   1968
            Width           =   3804
            Begin VB.CommandButton cmdDeliveryFont 
               Caption         =   "&Font..."
               Height          =   288
               Left            =   144
               TabIndex        =   11
               Top             =   312
               Width           =   1028
            End
            Begin VB.TextBox txtDeliveryLeft 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0.0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   1
               EndProperty
               Height          =   288
               Left            =   2340
               TabIndex        =   10
               Text            =   "Auto"
               Top             =   300
               Width           =   768
            End
            Begin VB.TextBox txtDeliveryTop 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0.0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   1
               EndProperty
               Height          =   288
               Left            =   2340
               TabIndex        =   9
               Text            =   "Auto"
               Top             =   648
               Width           =   768
            End
            Begin MSComCtl2.UpDown updTop 
               Height          =   288
               Left            =   3109
               TabIndex        =   12
               TabStop         =   0   'False
               Top             =   648
               Width           =   192
               _ExtentX        =   339
               _ExtentY        =   508
               _Version        =   393216
               BuddyControl    =   "fraDelivery"
               BuddyDispid     =   196614
               OrigLeft        =   2400
               OrigTop         =   3120
               OrigRight       =   2640
               OrigBottom      =   3495
               Increment       =   0
               Max             =   52
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin MSComCtl2.UpDown updLeft 
               Height          =   288
               Left            =   3109
               TabIndex        =   13
               TabStop         =   0   'False
               Top             =   300
               Width           =   192
               _ExtentX        =   339
               _ExtentY        =   508
               _Version        =   393216
               BuddyControl    =   "txtDeliveryTop"
               BuddyDispid     =   196617
               OrigLeft        =   2400
               OrigTop         =   3120
               OrigRight       =   2640
               OrigBottom      =   3495
               Increment       =   0
               Max             =   52
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin VB.Label Label2 
               Caption         =   "From &left:"
               Height          =   216
               Left            =   1560
               TabIndex        =   15
               Top             =   336
               Width           =   672
            End
            Begin VB.Label Label3 
               Caption         =   "From &top:"
               Height          =   240
               Left            =   1560
               TabIndex        =   14
               Top             =   672
               Width           =   852
            End
         End
         Begin VB.Frame fraUSA 
            Caption         =   "If mailed in the USA"
            Height          =   744
            Left            =   108
            TabIndex        =   4
            Top             =   1068
            Width           =   3792
            Begin VB.CheckBox chkPrintBarcode 
               Caption         =   "Print &barcode"
               Height          =   192
               Left            =   132
               TabIndex        =   6
               Top             =   312
               Width           =   1380
            End
            Begin VB.TextBox txtDeliveryPoint 
               Height          =   288
               Left            =   2928
               TabIndex        =   5
               Top             =   264
               Width           =   600
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Delivery point:"
               Height          =   192
               Index           =   1
               Left            =   1776
               TabIndex        =   7
               Top             =   312
               Width           =   1020
            End
         End
         Begin VB.Label lblLabel 
            Caption         =   "Envelope &size:"
            Height          =   228
            Index           =   0
            Left            =   108
            TabIndex        =   17
            Top             =   408
            Width           =   1524
         End
      End
   End
End
Attribute VB_Name = "frmTestOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOk As Boolean
Private mPaperSize As Integer

' Used for the auto text boxes
Private mAutoLeft As Double
Private mAutoTop As Double

Public Function GetDeliveryLeftMargin() As Long
  If txtDeliveryLeft = "Auto" Then
    GetDeliveryLeftMargin = mAutoLeft * 1440
    Exit Function
  End If
  GetDeliveryLeftMargin = CDbl(txtDeliveryLeft.Text) * 1440
End Function

Public Function GetDeliveryTopMargin() As Long
  If txtDeliveryTop.Text = "Auto" Then
    GetDeliveryTopMargin = mAutoTop * 1440
    Exit Function
  End If
  GetDeliveryTopMargin = CDbl(txtDeliveryTop.Text) * 1440
End Function

Public Property Get OK() As Boolean
  OK = mOk
End Property

Public Property Let PaperSize(vData As Integer)
  '
  ' Set the papersize and the combo box
  '
  mPaperSize = vData
  Select Case vData
    Case PrinterObjectConstants.vbPRPSEnv9
      cboEnvelopeSize.ListIndex = 0
    Case PrinterObjectConstants.vbPRPSEnv10
      cboEnvelopeSize.ListIndex = 1
    Case PrinterObjectConstants.vbPRPSEnv11
      cboEnvelopeSize.ListIndex = 2
    Case PrinterObjectConstants.vbPRPSEnv12
      cboEnvelopeSize.ListIndex = 3
    Case PrinterObjectConstants.vbPRPSEnv14
      cboEnvelopeSize.ListIndex = 4
    Case 250
      cboEnvelopeSize.ListIndex = 5
    Case 251
      cboEnvelopeSize.ListIndex = 6
  End Select

End Property

Public Property Get PaperSize() As Integer
  PaperSize = mPaperSize
End Property

Private Sub cboEnvelopeSize_Click()
  'Set default settings for each envelope
  Select Case cboEnvelopeSize.ListIndex
    Case 0 'Size 9
      mPaperSize = PrinterObjectConstants.vbPRPSEnv9
      mAutoTop = 2
      mAutoLeft = 4
    Case 1 'Size 10
      mPaperSize = PrinterObjectConstants.vbPRPSEnv10
      mAutoTop = 2
      mAutoLeft = 4
    Case 2 'Size 11
      mPaperSize = PrinterObjectConstants.vbPRPSEnv11
      mAutoTop = 2
      mAutoLeft = 4.5
    Case 3 'Size 12 (9 x 12 in)
      mPaperSize = PrinterObjectConstants.vbPRPSEnv12
      mAutoTop = 2
      mAutoLeft = 4.5
    Case 4 'Size 14 (10 x 13 in)
      mPaperSize = PrinterObjectConstants.vbPRPSEnv14
      mAutoTop = 2.5
      mAutoLeft = 6
  
  '
  ' The WILIMIC HP  8000 does not support any other page sizes than
  ' letter and Size 10 envelope. This is currently sending the information
  ' to be printed on size 10 envelopes. Any setting must be in the range
  ' to fit on a size 10 envelope.
  '
  ' Any changes made here need to be followed by updating the code in:
  '  * frmEnvelopeOptions (This Form) - Property Let Papersize
  '  * MailLabel.GetPaperHeight
  '  * MailLabel.GetPaperWidth
  '
    
    Case 5 'Size (9 x 12in)
      mPaperSize = 250 'PrinterObjectConstants.vbPRPSUser
      mAutoTop = 3  '4.5  '7200  ' 5"
      mAutoLeft = 5     '7200  ' 5"
    Case 6 'Size (10 x 13in)
      mPaperSize = 251 'PrinterObjectConstants.vbPRPSUser
      mAutoTop = 3    '5  '7920  ' 5.5"
      mAutoLeft = 5   '    7920  ' 5.5"
      
  End Select

  txtDeliveryLeft.Text = "Auto"
  txtDeliveryTop.Text = "Auto"

End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdDeliveryFont_Click()
  With dlgDeliveryFont
    .Flags = cdlCFScreenFonts Or cdlCFForceFontExist Or cdlCFEffects
    .CancelError = False
    .ShowFont
  End With
End Sub


Private Sub cmdOK_Click()
  mOk = True
  
  Hide
End Sub


Private Sub Form_Load()

  ' If there is not a papersize set the combo box defaults to Size 10
  '- the most common envelope size
  If mPaperSize = 0 Then
    cboEnvelopeSize.ListIndex = 1
  End If
End Sub

Private Sub txtDeliveryLeft_Change()
  ' Insures a positive number
  If CDbl(txtDeliveryLeft.Text) < 0 Then
    txtDeliveryLeft.Text = "0"
  End If
  
End Sub

'
'The up down controls only increment values by integers.
'This customizes it to be a double
'
Private Sub updLeft_DownClick()
  If txtDeliveryLeft.Text = "Auto" Then
    txtDeliveryLeft.Text = mAutoLeft
  End If
  If Not (CDbl(txtDeliveryLeft.Text) <= 0) Then
    txtDeliveryLeft.Text = CDbl(txtDeliveryLeft.Text) - 0.1
  End If
End Sub

Private Sub updLeft_UpClick()
  If txtDeliveryLeft.Text = "Auto" Then
    txtDeliveryLeft.Text = mAutoLeft
  End If
  txtDeliveryLeft.Text = CDbl(txtDeliveryLeft.Text) + 0.1
End Sub

Private Sub updTop_DownClick()
  If txtDeliveryTop.Text = "Auto" Then
    txtDeliveryTop.Text = mAutoTop
  End If
  If Not (CDbl(txtDeliveryTop.Text) <= 0) Then
    txtDeliveryTop.Text = CDbl(txtDeliveryTop.Text) - 0.1
  End If
End Sub

Private Sub updTop_UpClick()
  If txtDeliveryTop.Text = "Auto" Then
    txtDeliveryTop.Text = mAutoTop
  End If
  txtDeliveryTop.Text = CDbl(txtDeliveryTop.Text) + 0.1
End Sub


