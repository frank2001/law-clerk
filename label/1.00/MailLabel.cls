VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MailLabel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mPostNet As PostNet

Private mDeliveryColor As OLE_COLOR
Private mDeliveryFont As StdFont

Private mXMargin As Long
Private mYMargin As Long

Private mFont As String
Private mFontSize As Long

Private mPaperSize As Long


Public Sub Class_Initialize()
  Set mPostNet = New PostNet
  Set mPostNet.DrawObject = Printer
End Sub

'
' Explicitly clean up
'
Public Sub Class_Terminate()
  Set mPostNet = Nothing
End Sub


Public Sub PrintEnvelope(LabelText As String, Optional PrintBarCode As Boolean, Optional DeliveryPoint As String)
  Dim oldPrinter As Integer
  Dim I As Integer
    
  'Save Current Printer
  For I = 0 To Printers.Count - 1
    If Printer.DeviceName = Printers(I).DeviceName Then
      oldPrinter = I: Exit For
    End If
  Next
  
  'Load Envelope Printer
  For I = 0 To Printers.Count - 1
    If Printers(I).DeviceName = DefaultEnvelopePrinter Then
      Set Printer = Printers(I)
      Exit For
    End If
  Next


  
  If LabelText = vbNullString Then Exit Sub
  
  Dim addressArray() As String
  
  Dim PaperSizeOrig, OrientationOrig As Long 'Stores default entry
  
  PaperSizeOrig = Printer.PaperSize
  Printer.PaperSize = IIf(mPaperSize = 0, PrinterObjectConstants.vbPRPSEnv10, mPaperSize)
  
  OrientationOrig = Printer.Orientation
  Printer.Orientation = vbPRORLandscape
  
  If Not mDeliveryFont Is Nothing Then
    Set Printer.Font = mDeliveryFont
  End If
  Printer.ForeColor = mDeliveryColor
  
  mXMargin = IIf(mXMargin = 0, Printer.ScaleWidth / 2, mXMargin)
  mYMargin = IIf(mYMargin = 0, Printer.ScaleHeight / 2, mYMargin)
  
  Printer.CurrentY = mYMargin
  Printer.CurrentX = mXMargin
  
  addressArray = Split(LabelText, vbCrLf)
  
  If PrintBarCode Then
    mPostNet.DrawText addressArray(UBound(addressArray)) & DeliveryPoint
  End If
  
  Printer.Print " "
  
  For I = LBound(addressArray) To UBound(addressArray)
    If Not addressArray(I) = vbNullString Then
      Printer.CurrentX = mXMargin
      Printer.Print addressArray(I)
    End If
  Next I
  
  Printer.EndDoc
  
  Printer.PaperSize = PaperSizeOrig
  Printer.Orientation = OrientationOrig
  
  'Restore old printer
  Set Printer = Printers(oldPrinter)
End Sub


Public Property Get DeliveryLeftMargin() As Long
  DeliveryLeftMargin = mXMargin
End Property

Public Property Let DeliveryLeftMargin(vData As Long)
  mXMargin = vData
End Property

Public Property Get DeliveryTopMargin() As Long
  DeliveryTopMargin = mYMargin
End Property

Public Property Let DeliveryTopMargin(vData As Long)
  mYMargin = vData
End Property

Public Property Let PaperSize(vData As PrinterObjectConstants)
  mPaperSize = vData
End Property

Public Property Get PaperSize() As Long
  PaperSize = mPaperSize
End Property


Public Property Get DeliveryFont() As StdFont
  Set DeliveryFont = mDeliveryFont
End Property

Public Property Set DeliveryFont(vData As StdFont)
  Set mDeliveryFont = vData
End Property


Public Property Get DeliveryColor() As OLE_COLOR
  DeliveryColor = mDeliveryColor
End Property

Public Property Let DeliveryColor(vData As OLE_COLOR)
  mDeliveryColor = vData
End Property

