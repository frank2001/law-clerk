VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PostNet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"PostNet"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'
'
'

'
' See http://www.cedar.buffalo.edu/Adserv/postcode.html for a
' good description of PostNet bar codes.
'


' PostNet Character Set.
Private mCharSet(9) As String

' Draw object and properties
Private mObject As Object


' USPS Specs for POSTNET

' Must be 0.020" +- 0.005"  -- Minimum seems to be common usage.
Private Const LINE_WIDTH = 0.015

' Must Be 0.125" +- 0.010"
Private Const FULL_HEIGHT = 0.125

' Must be 0.050" +- 0.010"
Private Const HALF_HEIGHT = 0.05

' Should be 22 +- 2 Bars/Inch
Private Const PITCH = 1 / 22


'
' Initialize PostNet character set.
'
Private Sub Class_Initialize()
  mCharSet(0) = "11000"
  mCharSet(1) = "00011"
  mCharSet(2) = "00101"
  mCharSet(3) = "00110"
  mCharSet(4) = "01001"
  mCharSet(5) = "01010"
  mCharSet(6) = "01100"
  mCharSet(7) = "10001"
  mCharSet(8) = "10010"
  mCharSet(9) = "10100"
End Sub


'
' Release DrawObject
'
Private Sub Class_Terminate()
  Set mObject = Nothing
End Sub


'
' Set Object.  Object must support ScaleMode, CurrentX and CurrentY.
'
Public Property Set DrawObject(vData As Object)
  Set mObject = vData
End Property


'
' Draw text to PostNet.  Converts text to a numeric
' string using the NumericText function.  Then, creates the
' PostNet string from the NumericText.  Finally, draws the
' bar codes from the Postnet string.
'
Public Sub DrawText(Text As String, Optional Object As Object)
  DrawPostNet PostNet(NumericText(Text)), Object
End Sub


'
' Convert text to a sequence of numeric text.  Discard any non-numeric
' digits.
'
Public Function NumericText(Text As String) As String
  Dim Parsed As String, Digit As String, I, Size As Integer
  Size = Len(Text)
  For I = 1 To Size
    Digit = Mid(Text, I, 1)
    If IsNumeric(Digit) Then
      Parsed = Parsed & Digit
    End If
  Next I
  NumericText = Parsed
End Function


'
' Create a PostNet string as a series of 0's and 1's.
' 1 represents a tall bar, 0 represents a short bar.
' The PostNet String always starts and
' ends with a tall bar and has a checksum digit added at the end (before
' the ending tall bar).
'
Public Function PostNet(NumericText As String) As String
  Dim I, Digit, Checksum As Integer, Result As String
  For I = 1 To Len(NumericText)
    Digit = Mid(NumericText, I, 1)
    Result = Result & mCharSet(Digit)
    Checksum = Checksum + Digit
  Next I
    
  'add the start/stop characters and checksum.
  PostNet = "1" & Result & mCharSet((10 - (Checksum Mod 10)) Mod 10) & "1"
End Function


'
' Draw a PostNet String to the DrawObject or the Object given.
'
Public Sub DrawPostNet(PostNet As String, Optional Object As Object)
  If Object Is Nothing Then
    If mObject Is Nothing Then
      Err.Raise 1, , "Either the DrawObject must be set or an Object must be given."
    End If
    Set Object = mObject
  End If
  
  Dim I As Integer
  For I = 1 To Len(PostNet)
    DrawBarCode Mid(PostNet, I, 1) = 1, Object
  Next I
End Sub


'
' Draw a single bar code.
'
Public Sub DrawBarCode(FullBar As Boolean, Object As Object)
  Dim X As Single, Y As Single, ScaleMode As Integer
  
  'Save original scalemode and set to inches
  ScaleMode = Object.ScaleMode
  Object.ScaleMode = vbInches
  
  X = Object.CurrentX
  Y = Object.CurrentY
  If FullBar Then
    Object.Line (X, Y)-(X + LINE_WIDTH, Y + FULL_HEIGHT), vbBlack, BF
  Else
    ' Y draws from top to bottom
    Object.Line (X, Y + FULL_HEIGHT - HALF_HEIGHT)-(X + LINE_WIDTH, Y + FULL_HEIGHT), vbBlack, BF
  End If
  Object.CurrentX = X + PITCH
  Object.CurrentY = Y
  
  'Restore original scalemode
  Object.ScaleMode = ScaleMode
End Sub
