VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEnvelopeOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Envelope Options"
   ClientHeight    =   4068
   ClientLeft      =   36
   ClientTop       =   312
   ClientWidth     =   4332
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4068
   ScaleWidth      =   4332
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog dlgDeliveryFont 
      Left            =   384
      Top             =   3204
      _ExtentX        =   677
      _ExtentY        =   677
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3984
      Left            =   0
      ScaleHeight     =   3984
      ScaleWidth      =   4332
      TabIndex        =   0
      Top             =   0
      Width           =   4332
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2928
         TabIndex        =   14
         Top             =   3564
         Width           =   1200
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Default         =   -1  'True
         Height          =   336
         Left            =   1656
         TabIndex        =   13
         Top             =   3552
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3348
         Left            =   84
         TabIndex        =   1
         Top             =   132
         Width           =   4044
         _ExtentX        =   7133
         _ExtentY        =   5906
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Envelope"
         TabPicture(0)   =   "frmEnvelopeOptions.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "cboEnvelopeSize"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraDelivery"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "fraUSA"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         Begin VB.Frame fraUSA 
            Caption         =   "If mailed in the USA"
            Height          =   744
            Left            =   108
            TabIndex        =   11
            Top             =   1068
            Width           =   3804
            Begin VB.TextBox txtDeliveryPoint 
               Height          =   288
               Left            =   2928
               TabIndex        =   5
               Top             =   264
               Width           =   600
            End
            Begin VB.CheckBox chkPrintBarcode 
               Caption         =   "Print &barcode"
               Height          =   192
               Left            =   132
               TabIndex        =   4
               Top             =   312
               Width           =   1380
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Delivery point:"
               Height          =   192
               Index           =   1
               Left            =   1776
               TabIndex        =   15
               Top             =   312
               Width           =   1020
            End
         End
         Begin VB.Frame fraDelivery 
            Caption         =   "Delivery"
            Height          =   1128
            Left            =   108
            TabIndex        =   3
            Top             =   1968
            Width           =   3804
            Begin VB.TextBox txtDeliveryTop 
               Height          =   288
               Left            =   2340
               TabIndex        =   8
               Text            =   "Auto"
               Top             =   648
               Width           =   960
            End
            Begin VB.TextBox txtDeliveryLeft 
               Height          =   288
               Left            =   2340
               TabIndex        =   7
               Text            =   "Auto"
               Top             =   300
               Width           =   960
            End
            Begin VB.CommandButton cmdDeliveryFont 
               Caption         =   "&Font..."
               Height          =   288
               Left            =   144
               TabIndex        =   6
               Top             =   312
               Width           =   1028
            End
            Begin VB.Label Label3 
               Caption         =   "From &top:"
               Height          =   240
               Left            =   1560
               TabIndex        =   10
               Top             =   672
               Width           =   852
            End
            Begin VB.Label Label2 
               Caption         =   "From &left:"
               Height          =   216
               Left            =   1560
               TabIndex        =   9
               Top             =   336
               Width           =   672
            End
         End
         Begin VB.ComboBox cboEnvelopeSize 
            Height          =   288
            ItemData        =   "frmEnvelopeOptions.frx":001C
            Left            =   108
            List            =   "frmEnvelopeOptions.frx":0023
            TabIndex        =   2
            Text            =   "Size 10"
            Top             =   684
            Width           =   3792
         End
         Begin VB.Label lblLabel 
            Caption         =   "Envelope &size:"
            Height          =   228
            Index           =   0
            Left            =   108
            TabIndex        =   12
            Top             =   408
            Width           =   1524
         End
      End
   End
End
Attribute VB_Name = "frmEnvelopeOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOk As Boolean

Public Property Get OK() As Boolean
  OK = mOk
End Property



Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdDeliveryFont_Click()
  With dlgDeliveryFont
    .flags = cdlCFScreenFonts Or cdlCFForceFontExist Or cdlCFEffects
    .CancelError = False
    .ShowFont
  End With
End Sub


Private Sub cmdOK_Click()
  mOk = True
  Hide
End Sub

