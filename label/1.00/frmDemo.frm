VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDemo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Envelope Generator"
   ClientHeight    =   2808
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   6120
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2808
   ScaleWidth      =   6120
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   324
      Left            =   4824
      TabIndex        =   5
      ToolTipText     =   "Print PostNet Barcode to Printer"
      Top             =   1008
      Width           =   984
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2400
      Left            =   48
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   48
      Width           =   5844
      _ExtentX        =   10308
      _ExtentY        =   4233
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   420
      TabCaption(0)   =   "Envelopes"
      TabPicture(0)   =   "frmDemo.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdShow"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "txtAddress"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "picPostnet"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      Begin VB.PictureBox picPostnet 
         BackColor       =   &H80000005&
         Height          =   444
         Left            =   120
         ScaleHeight     =   396
         ScaleWidth      =   4836
         TabIndex        =   4
         Top             =   1452
         Width           =   4884
      End
      Begin VB.TextBox txtAddress 
         Height          =   312
         Left            =   108
         MultiLine       =   -1  'True
         TabIndex        =   0
         Top             =   516
         Width           =   4560
      End
      Begin VB.CommandButton cmdShow 
         Caption         =   "Show"
         Height          =   324
         Left            =   4776
         TabIndex        =   1
         ToolTipText     =   "Print PostNet Barcode to Printer"
         Top             =   516
         Width           =   984
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Zip Code"
         Height          =   192
         Left            =   120
         TabIndex        =   3
         Top             =   300
         Width           =   660
      End
   End
End
Attribute VB_Name = "frmDemo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mPostNet As PostNet


Private Sub cmdPrint_Click()
  mPostNet.DrawText txtAddress, Printer
  Printer.EndDoc
End Sub

Private Sub cmdShow_Click()
  picPostnet.Cls
  picPostnet.CurrentX = 50
  picPostnet.CurrentY = 50
  mPostNet.DrawText txtAddress, picPostnet
End Sub

Private Sub Form_Load()
  Set mPostNet = New PostNet
End Sub
