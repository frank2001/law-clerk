VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEnvelopeOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Envelope Options"
   ClientHeight    =   4068
   ClientLeft      =   36
   ClientTop       =   312
   ClientWidth     =   4260
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4068
   ScaleWidth      =   4260
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog dlgDeliveryFont 
      Left            =   384
      Top             =   3204
      _ExtentX        =   677
      _ExtentY        =   677
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3984
      Left            =   0
      ScaleHeight     =   3984
      ScaleWidth      =   4260
      TabIndex        =   0
      Top             =   0
      Width           =   4260
      Begin VB.CommandButton cmdHelp 
         Caption         =   "&Help"
         Height          =   336
         Left            =   2928
         TabIndex        =   18
         Top             =   3564
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   1644
         TabIndex        =   14
         Top             =   3564
         Width           =   1200
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Default         =   -1  'True
         Height          =   336
         Left            =   372
         TabIndex        =   13
         Top             =   3564
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3348
         Left            =   84
         TabIndex        =   1
         Top             =   132
         Width           =   4044
         _ExtentX        =   7133
         _ExtentY        =   5906
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Envelope"
         TabPicture(0)   =   "frmEnvelopeOptions.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "cboEnvelopeSize"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraDelivery"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "fraUSA"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         Begin VB.Frame fraUSA 
            Caption         =   "If mailed in the USA"
            Height          =   744
            Left            =   108
            TabIndex        =   11
            Top             =   1068
            Width           =   3792
            Begin VB.TextBox txtDeliveryPoint 
               Height          =   288
               Left            =   2928
               TabIndex        =   5
               Top             =   264
               Width           =   600
            End
            Begin VB.CheckBox chkPrintBarcode 
               Caption         =   "Print &barcode"
               Height          =   192
               Left            =   132
               TabIndex        =   4
               Top             =   312
               Width           =   1380
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Delivery point:"
               Height          =   192
               Index           =   1
               Left            =   1776
               TabIndex        =   15
               Top             =   312
               Width           =   1020
            End
         End
         Begin VB.Frame fraDelivery 
            Caption         =   "Delivery"
            Height          =   1128
            Left            =   108
            TabIndex        =   3
            Top             =   1968
            Width           =   3804
            Begin VB.TextBox txtDeliveryTop 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0.0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   1
               EndProperty
               Height          =   288
               Left            =   2340
               TabIndex        =   8
               Text            =   "Auto"
               Top             =   648
               Width           =   768
            End
            Begin VB.TextBox txtDeliveryLeft 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0.0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   1
               EndProperty
               Height          =   288
               Left            =   2340
               TabIndex        =   7
               Text            =   "Auto"
               Top             =   300
               Width           =   768
            End
            Begin VB.CommandButton cmdDeliveryFont 
               Caption         =   "&Font..."
               Height          =   288
               Left            =   144
               TabIndex        =   6
               Top             =   312
               Width           =   1028
            End
            Begin MSComCtl2.UpDown updTop 
               Height          =   288
               Left            =   3109
               TabIndex        =   16
               TabStop         =   0   'False
               Top             =   648
               Width           =   192
               _ExtentX        =   339
               _ExtentY        =   508
               _Version        =   393216
               BuddyControl    =   "txtDeliveryTop"
               BuddyDispid     =   196618
               OrigLeft        =   2400
               OrigTop         =   3120
               OrigRight       =   2640
               OrigBottom      =   3495
               Increment       =   0
               Max             =   52
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin MSComCtl2.UpDown updLeft 
               Height          =   288
               Left            =   3109
               TabIndex        =   17
               TabStop         =   0   'False
               Top             =   300
               Width           =   192
               _ExtentX        =   339
               _ExtentY        =   508
               _Version        =   393216
               BuddyControl    =   "txtDeliveryLeft"
               BuddyDispid     =   196619
               OrigLeft        =   2400
               OrigTop         =   3120
               OrigRight       =   2640
               OrigBottom      =   3495
               Increment       =   0
               Max             =   52
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin VB.Label Label3 
               Caption         =   "From &top:"
               Height          =   240
               Left            =   1560
               TabIndex        =   10
               Top             =   672
               Width           =   852
            End
            Begin VB.Label Label2 
               Caption         =   "From &left:"
               Height          =   216
               Left            =   1560
               TabIndex        =   9
               Top             =   336
               Width           =   672
            End
         End
         Begin VB.ComboBox cboEnvelopeSize 
            Height          =   288
            ItemData        =   "frmEnvelopeOptions.frx":001C
            Left            =   108
            List            =   "frmEnvelopeOptions.frx":0035
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   684
            Width           =   3792
         End
         Begin VB.Label lblLabel 
            Caption         =   "Envelope &size:"
            Height          =   228
            Index           =   0
            Left            =   108
            TabIndex        =   12
            Top             =   408
            Width           =   1524
         End
      End
   End
End
Attribute VB_Name = "frmEnvelopeOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOk As Boolean
Private mPaperSize As Integer

' Used for the auto text boxes
Private mAutoLeft As Double
Private mAutoTop As Double

Public Function GetDeliveryLeftMargin() As Long
  If txtDeliveryLeft = "Auto" Then
    GetDeliveryLeftMargin = mAutoLeft * TwipsPerInch
    Exit Function
  End If
  GetDeliveryLeftMargin = GetDouble(txtDeliveryLeft.Text) * TwipsPerInch
End Function

Public Function GetDeliveryTopMargin() As Long
  If txtDeliveryTop.Text = "Auto" Then
    GetDeliveryTopMargin = mAutoTop * TwipsPerInch
    Exit Function
  End If
  GetDeliveryTopMargin = GetDouble(txtDeliveryTop.Text) * TwipsPerInch
End Function

Public Property Get OK() As Boolean
  OK = mOk
End Property

Public Property Let PaperSize(vData As Integer)
  '
  ' Set the papersize and the combo box
  '
  mPaperSize = vData
  Select Case vData
    Case PrinterObjectConstants.vbPRPSEnv9
      cboEnvelopeSize.ListIndex = 0
    Case PrinterObjectConstants.vbPRPSEnv10
      cboEnvelopeSize.ListIndex = 1
    Case PrinterObjectConstants.vbPRPSEnv11
      cboEnvelopeSize.ListIndex = 2
    Case PrinterObjectConstants.vbPRPSEnv12
      cboEnvelopeSize.ListIndex = 3
    Case PrinterObjectConstants.vbPRPSEnv14
      cboEnvelopeSize.ListIndex = 4
    Case 250
      cboEnvelopeSize.ListIndex = 5
    Case 251
      cboEnvelopeSize.ListIndex = 6
  End Select

End Property

Public Property Get PaperSize() As Integer
  PaperSize = mPaperSize
End Property

Private Sub cboEnvelopeSize_Click()
  'Set default settings for each envelope
  Select Case cboEnvelopeSize.ListIndex
    Case 0 'Size 9
      mPaperSize = PrinterObjectConstants.vbPRPSEnv9
      mAutoTop = 2
      mAutoLeft = 4
    Case 1 'Size 10
      mPaperSize = PrinterObjectConstants.vbPRPSEnv10
      mAutoTop = 2
      mAutoLeft = 4
    Case 2 'Size 11
      mPaperSize = PrinterObjectConstants.vbPRPSEnv11
      mAutoTop = 2
      mAutoLeft = 4.5
    Case 3 'Size 12 (9 x 12 in)
      mPaperSize = PrinterObjectConstants.vbPRPSEnv12
      mAutoTop = 2
      mAutoLeft = 4.5
    Case 4 'Size 14 (10 x 13 in)
      mPaperSize = PrinterObjectConstants.vbPRPSEnv14
      mAutoTop = 2.5
      mAutoLeft = 6
  
  '
  ' The WILIMIC HP  8000 does not support any other page sizes than
  ' letter and Size 10 envelope. This is currently sending the information
  ' to be printed on size 10 envelopes. Any setting must be in the range
  ' to fit on a size 10 envelope.
  '
  ' Any changes made here need to be followed by updating the code in:
  '  * frmEnvelopeOptions (This Form) - Property Let Papersize
  '  * MailLabel.GetPaperHeight
  '  * MailLabel.GetPaperWidth
  '
    
    Case 5 'Size (9 x 12in)
      mPaperSize = 250 'PrinterObjectConstants.vbPRPSUser
      mAutoTop = 3  '4.5  '7200  ' 5"
      mAutoLeft = 5     '7200  ' 5"
    Case 6 'Size (10 x 13in)
      mPaperSize = 251 'PrinterObjectConstants.vbPRPSUser
      mAutoTop = 3    '5  '7920  ' 5.5"
      mAutoLeft = 5   '    7920  ' 5.5"
      
  End Select

  txtDeliveryLeft.Text = "Auto"
  txtDeliveryTop.Text = "Auto"

End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdDeliveryFont_Click()
  With dlgDeliveryFont
    .flags = cdlCFScreenFonts Or cdlCFForceFontExist Or cdlCFEffects
    .CancelError = False
    .ShowFont
  End With
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub cmdOK_Click()
  mOk = True
  
  Hide
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()

  ' If there is not a papersize set the combo box defaults to Size 10
  '- the most common envelope size
  If mPaperSize = 0 Then
    cboEnvelopeSize.ListIndex = 1
  End If
End Sub

Private Sub txtDeliveryLeft_Change()
  ' Insures a positive number
  If GetDouble(txtDeliveryLeft.Text) < 0 Then
    txtDeliveryLeft.Text = "0"
  End If
  
End Sub

'
'The up down controls only increment values by integers.
'This customizes it to be a double
'
Private Sub updLeft_DownClick()
  If txtDeliveryLeft.Text = "Auto" Then
    txtDeliveryLeft.Text = mAutoLeft
  End If
  If Not (GetDouble(txtDeliveryLeft.Text) <= 0) Then
    txtDeliveryLeft.Text = GetDouble(txtDeliveryLeft.Text) - 0.1
  End If
End Sub

Private Sub updLeft_UpClick()
  If txtDeliveryLeft.Text = "Auto" Then
    txtDeliveryLeft.Text = mAutoLeft
  End If
  txtDeliveryLeft.Text = GetDouble(txtDeliveryLeft.Text) + 0.1
End Sub

Private Sub updTop_DownClick()
  If txtDeliveryTop.Text = "Auto" Then
    txtDeliveryTop.Text = mAutoTop
  End If
  If Not (GetDouble(txtDeliveryTop.Text) <= 0) Then
    txtDeliveryTop.Text = GetDouble(txtDeliveryTop.Text) - 0.1
  End If
End Sub

Private Sub updTop_UpClick()
  If txtDeliveryTop.Text = "Auto" Then
    txtDeliveryTop.Text = mAutoTop
  End If
  txtDeliveryTop.Text = GetDouble(txtDeliveryTop.Text) + 0.1
End Sub

