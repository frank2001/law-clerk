VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEnvelopeLabel 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Envelopes"
   ClientHeight    =   2580
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   6300
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2580
   ScaleWidth      =   6300
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   2376
      Left            =   96
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   6108
      _ExtentX        =   10774
      _ExtentY        =   4191
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   420
      TabCaption(0)   =   "Envelope"
      TabPicture(0)   =   "frmEnvelopeLabel.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdPrint"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "txtDeliveryAddress"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdCancel"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdOptions"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdHelp"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      Begin VB.CommandButton cmdHelp 
         Caption         =   "&Help"
         Height          =   336
         Left            =   4764
         TabIndex        =   6
         Top             =   1896
         Width           =   1200
      End
      Begin VB.CommandButton cmdOptions 
         Caption         =   "&Options"
         Height          =   336
         Left            =   4764
         TabIndex        =   5
         Top             =   1488
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   4764
         TabIndex        =   4
         Top             =   1068
         Width           =   1200
      End
      Begin VB.TextBox txtDeliveryAddress 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1548
         Left            =   108
         MultiLine       =   -1  'True
         TabIndex        =   0
         Top             =   684
         Width           =   4560
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "&Print"
         Height          =   336
         Left            =   4752
         TabIndex        =   1
         Top             =   672
         Width           =   1200
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Delivery Address:"
         Height          =   192
         Left            =   120
         TabIndex        =   3
         Top             =   384
         Width           =   1284
      End
   End
End
Attribute VB_Name = "frmEnvelopeLabel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Delivery Address
Private mDeliveryAddress As String

' USPS Delivery Point Code
Private mDeliveryPoint As String

' Should the bar code be printed
Private mPrintBarcode As Integer

' The envelope size
Private mPaperSize As Integer

Private mDeliveryTop As Double
Private mDeliveryLeft As Double

Public OK As Boolean

Public Property Let DeliveryAddress(vData As String)
  mDeliveryAddress = vData
End Property

Public Property Get DeliveryPoint() As String
  DeliveryPoint = mDeliveryPoint
End Property

Public Property Let DeliveryPoint(vData As String)
  mDeliveryPoint = vData
End Property

Public Property Get PrintBarCode() As Integer
  PrintBarCode = mPrintBarcode
End Property

Public Property Let PrintBarCode(vData As Integer)
  mPrintBarcode = vData
End Property

Private Sub cmdCancel_Click()
  Hide
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub cmdOptions_Click()
  Dim fOptions As New frmEnvelopeOptions
  
  ' Load current settings into the option form
  CommonDialogUtil.SetFont fOptions.dlgDeliveryFont, txtDeliveryAddress.Font
  fOptions.dlgDeliveryFont.Color = txtDeliveryAddress.ForeColor
  fOptions.txtDeliveryPoint.Text = mDeliveryPoint
  fOptions.chkPrintBarcode.Value = mPrintBarcode
  fOptions.PaperSize = mPaperSize
  
  ' Convert X and y positions from pixels to inches
  If Not mDeliveryTop = 0 Then
    fOptions.txtDeliveryTop = FormatNumber((mDeliveryTop / TwipsPerInch), 1)
  End If
  If Not mDeliveryLeft = 0 Then
    fOptions.txtDeliveryLeft = FormatNumber((mDeliveryLeft / TwipsPerInch), 1)
  End If
  
  fOptions.Show vbModal
  If Not fOptions.OK Then
    Exit Sub
  End If

  ' Retrieve Settings
  Set txtDeliveryAddress.Font = CommonDialogUtil.GetFont(fOptions.dlgDeliveryFont)
  txtDeliveryAddress.ForeColor = fOptions.dlgDeliveryFont.Color
  mDeliveryPoint = fOptions.txtDeliveryPoint.Text
  mPrintBarcode = fOptions.chkPrintBarcode.Value
  mPaperSize = fOptions.PaperSize
  mDeliveryTop = fOptions.GetDeliveryTopMargin
  mDeliveryLeft = fOptions.GetDeliveryLeftMargin
  
  Unload fOptions
End Sub

Private Sub cmdPrint_Click()
  Dim Pointer As New WaitPointer
  Pointer.Default
  
  'Load current settings and Print the envelope
  Dim Envelope As New MailLabel
  Envelope.DeliveryColor = txtDeliveryAddress.ForeColor
  Set Envelope.DeliveryFont = txtDeliveryAddress.Font
  Envelope.DeliveryTopMargin = mDeliveryTop '/ TwipsPerInch
  Envelope.DeliveryLeftMargin = mDeliveryLeft ' / TwipsPerInch
  Envelope.PaperSize = mPaperSize
  If Envelope.PrintEnvelope(txtDeliveryAddress.Text, mPrintBarcode = 1, mDeliveryPoint) Then
    ' Exit after printing
    mnuFileExit_Click
  End If
End Sub

Private Sub cmdPrintSetup_Click()
  CommonDialogUtil.PrintSetupDialog Me
End Sub

Private Sub mnuFileExit_Click()
  OK = True
  Hide
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  'mPrintBarcode = 1
  txtDeliveryAddress.Text = mDeliveryAddress
  
  'Load default settings
  
  txtDeliveryAddress.ForeColor = LocaleEnvelopeForeColor
  FormUtil.SetTextControlFont txtDeliveryAddress, LocaleEnvelopeFont
  mPrintBarcode = LocaleEnvelopeBarCode
  mPaperSize = LocaleEnvelopePaperSize
  mDeliveryTop = LocaleEnvelopeCurrentY
  mDeliveryLeft = LocaleEnvelopeCurrentX
  
End Sub
