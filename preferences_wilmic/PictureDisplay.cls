VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PictureDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.30.0620

'
' This class stores all images used in law clerk
' through a central image collection.
'

Option Explicit

Private PhotoAlbum As New Collection

Public Function GetPicture(PictureName As String) As IPictureDisp
  On Error GoTo Exception
  Set GetPicture = PhotoAlbum(PictureName)
  Exit Function
  
Exception:
  MsgBox "Error: Picture not found. " & PictureName
End Function

Private Sub Class_Initialize()
  Dim TmpName As String
  Dim TmpPic As IPictureDisp
  
  'Get first file to start the loop
  TmpName = Dir(ImageFolderPath)
  Do While Len(TmpName)
    On Error Resume Next
    Set TmpPic = LoadPicture(ImageFolderPath & TmpName)
    PhotoAlbum.Add TmpPic, TmpName
    TmpName = Dir
  Loop
End Sub


