Attribute VB_Name = "FolderUtil"
'ver 2.70.0129-B (jjm)
'ver 2.30.0911-B (jjm)
'ver 2.30.0825-B (jjm)
'ver 1.93.0221 (jjm)
Option Explicit

'
' The View Module controls access to a global set of data views.
'

' Views for data.

Public Sub TreeViewLoadSystemFolders(Tree As TreeView)
  TreeViewLoad Tree, SystemFolders_TreeName, SystemFolders_RootKey, SystemFolders_RootName
End Sub

Public Sub TreeViewLoadSystemPreferences(Tree As TreeView)
  TreeViewLoad Tree, SystemPreferences_TreeName, SystemPreferences_RootKey, SystemPreferences_RootName
End Sub

Public Sub TreeViewLoad(Tree As TreeView, Treename As String, RootKey As String, RootName As String)
  ' Clear tree and folders.
  Tree.Nodes.Clear
  
  Dim FolderNode As MSXML2.IXMLDOMNode
  Set FolderNode = GetFolderNode(Treename)
  If FolderNode Is Nothing Then
    MsgBox "Unable to retrieve folders from middle tier"
    Exit Sub
  End If
  'Add the hack root
  Tree.Nodes.Add , , RootKey, RootName, "LAWFolder"
  
  'load the children
  TreeViewLoadNodes Tree, Tree.Nodes(RootKey), FolderNode.selectNodes("node")
  Tree.Nodes.item(1).Expanded = True
End Sub

'
' Walk node and children.
'
Public Sub TreeViewLoadNode(Tree As TreeView, Parent As Node, Node As IXMLDOMNode)
  ' If no node, nothing more to load
  If Node Is Nothing Then
    Exit Sub
  End If
  
  'ver 2.30.0821-B (jjm)
  Dim TreeChild As Node
  Dim Key As String
  Dim Text As String
  Key = Node.Attributes.getNamedItem("handle").Text
  Text = Node.Attributes.getNamedItem("name").Text

  If (Parent Is Nothing) Then
    Set TreeChild = Tree.Nodes.Add(, , Key, Text, _
      "TVFolderImage", "TVOpenFolderImage")
  Else
    Set TreeChild = Tree.Nodes.Add(Parent, tvwChild, Key, Text, _
      "TVFolderImage", "TVOpenFolderImage")
  End If
  TreeChild.ExpandedImage = "TVOpenFolderImage"
  
  ' ver 1.93.0221 (jjm)
  'Child.Tag = IniFolder.ReadFile(Node.Attributes.getNamedItem("id").Text, "data.type")
  'Child.Tag = Node.Attributes.getNamedItem("type").Text
  
 
  ' Load Child Nodes
  TreeViewLoadNodes Tree, TreeChild, Node.selectNodes("node")
End Sub

'
' Walk nodes
'
Public Sub TreeViewLoadNodes(Tree As TreeView, Parent As Node, NodeList As IXMLDOMNodeList)
  
  ' Walk Children.
  Dim Child As IXMLDOMNode
  For Each Child In NodeList
    TreeViewLoadNode Tree, Parent, Child
  Next
End Sub
'Public Function WorkflowShow(Folder As String, Optional ActionType As Integer, _
'    Optional ParameterNames As Variant, Optional ParameterValues As Variant, _
'    Optional OkCancel As Boolean) As Boolean
'
'  'ver 2.30.0825-B (jjm)
'  Dim WorkflowDialog As New frmWorkflowDialog
'  If Not WorkflowDialog.ShowByFolder(Folder, ActionType, OkCancel) Then
'    Unload WorkflowDialog
'    Exit Function
'  End If
'
'  'WorkflowDialog.ShowByFolder Folder, ActionType, OkCancel
'  Unload WorkflowDialog
'  WorkflowShow = True
'End Function

'ver 2.70.0129-B (jjm)
Public Function GetFolderNode(Treename As String) As MSXML2.IXMLDOMNode
  Dim OpenURL As String
  Select Case Treename
    Case SystemFolders_TreeName
      OpenURL = "folderopen"
    Case SystemPreferences_TreeName
      OpenURL = "preferenceopen"
    Case Else
      MsgBox "Error:Unknown tree name: " & Treename
      Exit Function
  End Select
  Set GetFolderNode = XMLUtil.HttpGet(HttpServer & WebApplication & WebUtil & OpenURL & "?" & "source=text/xml" & "&" & NoCache).responseXML.selectSingleNode("root")
  
  'Set GetFolderNode = XMLUtil.HttpGet(HttpServer & WebApplication & WebUtil & "folderopen?key=" & Treename & "&source=text/xml" & "&" & NoCache).responseXML.selectSingleNode("root")
  'Browse HttpServer & WebApplication & WebUtil & "folderopen?key=" & Treename & "&source=text/xml" & "&" & NoCache
End Function

Public Function FolderNodes(ParentHandle As String) As MSXML2.IXMLDOMNodeList
  Set FolderNodes = XMLUtil.HttpGet(HttpServer & WebApplication & WebUtil & "folderopen?key=" & EncodeURL(ParentHandle) & "&source=text/xml & " & " & NoCache").responseXML.selectNodes("xml/data/folder")
End Function


Private Function GetControlName(Name As String) As String
  GetControlName = Mid(Name, InStr(Name, ".") + 1)
End Function


Private Function GetForm(Name As String, Popup() As Form) As Form
  Dim Size, I As Integer
  Size = UBound(Popup)
  For I = 0 To Size
    If Popup(I).Name = Name Then
      Set GetForm = Popup(I)
      Exit Function
    End If
  Next I
End Function


Private Function GetFormName(Name As String) As String
  GetFormName = "frm" & Left(Name, InStr(Name, ".") - 1)
End Function
'
'Public Sub ViewLoad(View As View, Key As String, Popup() As Form, Source As String)
'  Dim PopupMenuName As String
'
'  'If the information is stored in an ini get from ini instead of xml
'  If Key = SystemFolders_RootKey Then
'    Dim Col As New Collection
'    Set Col = PreferenceUtil.getIniNodeCollection(SystemFolders_TreeName)
'    With View
'      .Key = Key
'      Set .Dataset = DataUtil.NewDataset(IIf(Contains(Col, "datatype"), Col("datatype"), vbNullString), Source)
'
'      PopupMenuName = IIf(Contains(Col, "popup"), Col("popup"), vbNullString)
'      If PopupMenuName = vbNullString Then Exit Sub
'
'      Set .PopupMenuForm = GetForm(GetFormName(PopupMenuName), Popup)
'      .PopupMenuName = GetControlName(PopupMenuName)
'      .DefaultMenuName = IIf(Contains(Col, "popup.default"), Col("popup.default"), vbNullString)
'      .OpenMenuName = IIf(Contains(Col, "popup.open"), Col("popup.open"), vbNullString)
'      .ExploreMenuName = IIf(Contains(Col, "popup.explore"), Col("popup.explore"), vbNullString)
'      .NewItemName = IIf(Contains(Col, "new"), Col("new"), vbNullString)
'    End With
'    Exit Sub
'  End If
'
'
'  Dim map As MSXML2.IXMLDOMNode
'  Set map = PreferenceUtil.GetNodeMap(SystemFolders_TreeName, Key)
'  If map Is Nothing Then
'    Exit Sub
'  End If
'
'  With View
'    .Key = Key
'    Set .Dataset = DataUtil.NewDataset(PreferenceUtil.getMapEntryValue(map, "datatype"), Source)
'    PopupMenuName = PreferenceUtil.getMapEntryValue(map, "popup")
'    If PopupMenuName = vbNullString Then Exit Sub
'
'    Set .PopupMenuForm = GetForm(GetFormName(PopupMenuName), Popup)
'    .PopupMenuName = GetControlName(PopupMenuName)
'    .DefaultMenuName = PreferenceUtil.getMapEntryValue(map, "popup.default")
'    .OpenMenuName = PreferenceUtil.getMapEntryValue(map, "popup.open")
'    .ExploreMenuName = PreferenceUtil.getMapEntryValue(map, "popup.explore")
'    .NewItemName = PreferenceUtil.getMapEntryValue(map, "new")
'  End With
'End Sub
