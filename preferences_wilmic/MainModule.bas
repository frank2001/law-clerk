Attribute VB_Name = "MainModule"
'
' This module is used to simulate the LAW Clerk Environment
'

Option Explicit

Public Factory As New ServiceFactory

' Constants for Null's
Public vbNullDate As Date
Public vbNullStringArray() As String

'Session information set by login.
Public HttpServer As String
Public WebApplication As String
Public WebService As String

Public LawSession As Session
Public LawSQL As SQLDialect

'Tree variables
Public SystemPreferences_TreeName As String
Public SystemPreferences_RootName As String
Public SystemPreferences_RootKey As String
Public SystemFolders_TreeName As String
Public SystemFolders_RootName As String
Public SystemFolders_RootKey As String

Public mCache As Long

Public PicDisplay As New PictureDisplay
Public ImageFolderPath As String



Sub Main()
  
  ImageFolderPath = App.Path & "\"
  
  SystemPreferences_TreeName = "systempreferences"
  SystemPreferences_RootName = "Preferences"
  SystemPreferences_RootKey = "preferencesroot"
  SystemFolders_TreeName = "systemfolders"
  SystemFolders_RootName = "LAW Explorer"
  SystemFolders_RootKey = "foldersroot"
  
  Randomize
  mCache = Rnd() * 1000000000
    
  'XMLSession connects to middle tier.
  Set LawSession = New XMLSession
  Set LawSQL = New SQLDialect
    
  Login
  
  Set Factory = New ServiceFactory
    
  Dim fFolderProperties As New frmFolderPropertiesEdit
  Load fFolderProperties
  fFolderProperties.Show
  
End Sub


Public Sub Login()
  Dim fLogin As New frmLoginDialog
  Dim Authenticated As Boolean
   
  While Not Authenticated
    fLogin.Show vbModal
    If Not fLogin.OK Then
        'Login Failed so exit app
       End
    End If
    Authenticated = LawSession.Authenticate(fLogin)
  Wend
   
  ' Generic now works with {fn xxx()} calls.
  'Set LawSQL = New SQLDialect
  
  ' Set web info
  HttpServer = fLogin.txtServerAddress.Text
  WebApplication = "malcolm/"
  WebService = "servlet/"
   
  Unload fLogin
End Sub

Public Function NoCache() As String
  mCache = mCache + 1
  NoCache = "noCache=" & mCache
End Function


Public Sub AddParameter(Name As String, Value As String, ParameterNames() As String, ParameterValues() As String)
  Dim Count As Integer
  Count = SafeUBound(ParameterNames) + 1
  ReDim Preserve ParameterNames(Count)
  ReDim Preserve ParameterValues(Count)
  ParameterNames(Count) = Name
  ParameterValues(Count) = Value
End Sub

Public Function CreateDataField(Source As String, Name As String, Value As Variant) As DataField
  Set CreateDataField = New DataField
  CreateDataField.Source = Source
  CreateDataField.Name = Name
  CreateDataField.Value = Value
End Function

Public Sub ClearDataField(Datafields As Variant, Optional DataMember As String)
  Dim Control As Variant
  For Each Control In Datafields
    If DataMember = vbNullString Or Control.DataMember = DataMember Then
      If TypeOf Control Is TextBox Then
        Control.Text = vbNullString
      ElseIf TypeOf Control Is Label Then
        Control.Caption = vbNullString
'      ElseIf TypeOf Control Is MaskEdBox Then
'        Dim msk As MaskEdBox
'        Set msk = Control
'        MaskEditClear msk
      ElseIf TypeOf Control Is ComboBox Then
        Dim cbx As ComboBox
        Set cbx = Control
        With cbx
          If .ListCount > 0 Then
            .ListIndex = 0
          End If
        End With
'      ElseIf TypeOf Control Is DTPicker Then
'        Dim dtpDate As DTPicker
'        Set dtpDate = Control
'        If dtpDate.CheckBox Then
'          dtpDate.Value = Today
'          dtpDate.Value = vbNull
'        Else
'          dtpDate.Value = vbNull
'          dtpDate.Value = Today
'        End If
      End If
    End If
  Next
End Sub


Public Function CollectionItem(Collection As Variant, Index As String) As Variant
  On Error Resume Next
  If VarType(Collection) = vbEmpty Then
  ElseIf TypeOf Collection Is ListItem Then
    Dim ListItem As ListItem
    Set ListItem = Collection
    CollectionItem = ListItemText(ListItem, Index)
  Else
    CollectionItem = Collection(Index)
  End If
  Err.Clear
End Function

Public Sub SetFields(Controls As Variant, Optional Items As Variant, Optional DataMember As String)
  Dim item As Variant
  Dim Control As Variant
  For Each Control In Controls
    If ((DataMember <> "" And Control.DataMember <> DataMember) Or Control.DataField = "") Then
      GoTo Continue
    End If
    
    item = CollectionItem(Items, Control.DataField)
      
    If TypeOf Control Is Label Then
      'Control.Caption = 'DisplayFormat(Control.DataField, CStr(item), Control.DataFormat.Format, LocaleShortDate)
    
    ElseIf TypeOf Control Is TextBox Then
      Control.Text = Format(item, Control.DataFormat.Format)
    
'    ElseIf TypeOf Control Is DTPicker Then
'      If item = vbNullString Then
'        Control.CheckBox = True
'      End If
'      Control.Value = Format(item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is ComboBox Then
      Dim cbx As ComboBox
      Set cbx = Control
      With cbx
        If item = vbNullString Then
          If .ListCount > 0 Then
            .ListIndex = 0
          End If
        Else
          Dim Text As String
          Text = Format(item, .DataFormat.Format)
          If .Style = ComboBoxConstants.vbComboDropdownList Then
            Dim I As Integer
            For I = .ListCount - 1 To 0 Step -1
              If .List(I) = Text Then
                .ListIndex = I
                Exit For
              End If
            Next I
          Else
            .Text = Text
          End If
        End If
      End With
    
'    ElseIf TypeOf Control Is MaskEdBox Then
'      Control.SelStart = 0
'      Control.SelLength = 9999
'      Control.SelText = Format(item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is CheckBox Then 'Hack for old data
      Control.Value = IIf(item = "Yes" Or item = "Y" Or item = "1", 1, 0)
    End If
    
Continue:
  Next
End Sub

Public Sub AddTextControl(TextControl As Variant, Items As Collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In TextControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Text), Control.DataField
    End If
  Next
End Sub

Public Sub AddValueControl(ValueControl As Variant, Items As Collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In ValueControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Value), Control.DataField
    End If
  Next
End Sub

Public Sub AddCaptionControl(CaptionControl As Variant, Items As Collection)
  Dim Control As Variant
  For Each Control In CaptionControl
    If (Not Control.DataField = "") Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Caption), Control.DataField
    End If
  Next
End Sub

Public Sub ReadValueControl(TextControl As Variant, Items As Collection)
  Dim Control As Variant
  For Each Control In TextControl
    If (Not Control.DataField = "") Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Value), Control.DataField
    End If
  Next
End Sub

'--------------------------------------'
' Code Based on LAW Clerk FolderUtil   '
'--------------------------------------'

'
' Walk node and children.
'
Public Sub TreeViewLoadSystemFolders(Tree As TreeView)
  TreeViewLoad Tree, SystemFolders_TreeName, SystemFolders_RootKey, SystemFolders_RootName
End Sub

Public Sub TreeViewLoadSystemPreferences(Tree As TreeView)
  TreeViewLoad Tree, SystemPreferences_TreeName, SystemPreferences_RootKey, SystemPreferences_RootName
End Sub

Public Sub TreeViewLoad(Tree As TreeView, TreeName As String, RootKey As String, RootName As String)
  ' Clear tree and folders.
  Tree.Nodes.Clear
  
  Dim FolderNode As MSXML2.IXMLDOMNode
  Set FolderNode = GetFolderNode(TreeName)
  If FolderNode Is Nothing Then
    MsgBox "Unable to retrieve folders from middle tier"
    Exit Sub
  End If
  'Add the hack root
  Tree.Nodes.Add , , RootKey, RootName
  'load the children
  TreeViewLoadNodes Tree, Tree.Nodes(RootKey), FolderNode.selectNodes("node")
  Tree.Nodes.item(1).Expanded = True
End Sub

'
' Walk node and children.
'
Public Sub TreeViewLoadNode(Tree As TreeView, Parent As Node, Node As IXMLDOMNode)
  ' If no node, nothing more to load
  If Node Is Nothing Then
    Exit Sub
  End If
  
  'ver 2.30.0821-B (jjm)
  Dim TreeChild As Node
  Dim Key As String
  Dim Text As String
  Key = Node.Attributes.getNamedItem("handle").Text
  Text = Node.Attributes.getNamedItem("name").Text

  If (Parent Is Nothing) Then
    Set TreeChild = Tree.Nodes.Add(, , Key, Text)
  Else
    Set TreeChild = Tree.Nodes.Add(Parent, tvwChild, Key, Text)
  End If
   
  ' Load Child Nodes
  TreeViewLoadNodes Tree, TreeChild, Node.selectNodes("node")
End Sub

'
' Walk nodes
'
Public Sub TreeViewLoadNodes(Tree As TreeView, Parent As Node, NodeList As IXMLDOMNodeList)
  ' Walk Children.
  Dim Child As IXMLDOMNode
  For Each Child In NodeList
    TreeViewLoadNode Tree, Parent, Child
  Next
End Sub



'--------------------------------------'
' Code Borrowed from Law Clerk xmlUtil '
'--------------------------------------'
'Public Function HttpGet(URL As String) As xmlHttp
'  Dim xmlHttp As New xmlHttp
'  xmlHttp.open "GET", URL, False
'  xmlHttp.send
'  Set HttpGet = xmlHttp
'End Function
