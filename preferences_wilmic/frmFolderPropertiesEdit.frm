VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmFolderPropertiesEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Folder Properties Edit"
   ClientHeight    =   5955
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   9405
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5955
   ScaleWidth      =   9405
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picbody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5940
      Left            =   0
      ScaleHeight     =   5940
      ScaleWidth      =   9405
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   0
      Width           =   9405
      Begin TabDlg.SSTab SSTab1 
         Height          =   5160
         Left            =   3540
         TabIndex        =   1
         Top             =   105
         Width           =   5790
         _ExtentX        =   10213
         _ExtentY        =   9102
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmFolderPropertiesEdit.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFolder"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraData"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraLine"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "C&omplete"
         TabPicture(1)   =   "frmFolderPropertiesEdit.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraNote"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "&Void"
         TabPicture(2)   =   "frmFolderPropertiesEdit.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame1"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "&Advanced"
         TabPicture(3)   =   "frmFolderPropertiesEdit.frx":0054
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView"
         Tab(3).ControlCount=   1
         Begin VB.Frame Frame1 
            Caption         =   "Void Note"
            Height          =   4635
            Left            =   -74880
            TabIndex        =   34
            Top             =   312
            Width           =   5544
            Begin VB.TextBox txtDataField 
               DataField       =   "voidnote"
               Height          =   4260
               Index           =   9
               Left            =   120
               MultiLine       =   -1  'True
               TabIndex        =   35
               Top             =   264
               Width           =   5292
            End
         End
         Begin VB.Frame fraNote 
            Caption         =   "Complete Note"
            Height          =   4635
            Left            =   -74880
            TabIndex        =   32
            Top             =   312
            Width           =   5544
            Begin VB.TextBox txtDataField 
               DataField       =   "note"
               Height          =   4260
               Index           =   7
               Left            =   120
               MultiLine       =   -1  'True
               TabIndex        =   33
               Top             =   264
               Width           =   5292
            End
         End
         Begin VB.Frame fraLine 
            Caption         =   "Popup Menu"
            Height          =   1680
            Left            =   132
            TabIndex        =   26
            Top             =   3300
            Width           =   5544
            Begin VB.TextBox txtDataField 
               DataField       =   "popup.default"
               Height          =   288
               Index           =   4
               Left            =   996
               TabIndex        =   8
               Text            =   "Text1"
               Top             =   576
               Width           =   2628
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "popup.explore"
               Height          =   288
               Index           =   5
               Left            =   984
               TabIndex        =   9
               Text            =   "Text1"
               Top             =   900
               Width           =   2628
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "popup.open"
               Height          =   288
               Index           =   6
               Left            =   996
               TabIndex        =   10
               Text            =   "Text1"
               Top             =   1224
               Width           =   2628
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "popup"
               Height          =   288
               Index           =   3
               Left            =   984
               TabIndex        =   6
               Text            =   "Text1"
               Top             =   216
               Width           =   2628
            End
            Begin VB.CheckBox chkDatafield 
               Caption         =   "Search"
               DataField       =   "search"
               Height          =   216
               Index           =   0
               Left            =   3888
               TabIndex        =   7
               Top             =   264
               Width           =   1416
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Default"
               Height          =   192
               Index           =   4
               Left            =   132
               TabIndex        =   30
               Top             =   612
               Width           =   504
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Open"
               Height          =   192
               Index           =   7
               Left            =   132
               TabIndex        =   29
               Top             =   1284
               Width           =   396
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Explore"
               Height          =   192
               Index           =   8
               Left            =   132
               TabIndex        =   28
               Top             =   948
               Width           =   552
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Popup"
               Height          =   192
               Index           =   9
               Left            =   132
               TabIndex        =   27
               Top             =   276
               Width           =   480
            End
         End
         Begin VB.Frame fraData 
            Caption         =   "Data"
            Height          =   1656
            Left            =   132
            TabIndex        =   21
            Top             =   1560
            Width           =   5544
            Begin VB.TextBox txtDataField 
               DataField       =   "datatype"
               Height          =   288
               Index           =   8
               Left            =   984
               TabIndex        =   2
               Text            =   "Text1"
               Top             =   228
               Width           =   2628
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "new"
               Height          =   288
               Index           =   2
               Left            =   984
               TabIndex        =   5
               Text            =   "Text1"
               Top             =   1236
               Width           =   2628
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "source"
               Height          =   288
               Index           =   0
               Left            =   984
               TabIndex        =   3
               Text            =   "Text1"
               Top             =   564
               Width           =   2628
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "filter"
               Height          =   288
               Index           =   1
               Left            =   984
               TabIndex        =   4
               Text            =   "Text1"
               Top             =   900
               Width           =   4460
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Type:"
               Height          =   192
               Left            =   132
               TabIndex        =   25
               Top             =   276
               Width           =   420
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "New"
               Height          =   192
               Index           =   6
               Left            =   132
               TabIndex        =   24
               Top             =   1284
               Width           =   324
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Filter"
               Height          =   192
               Index           =   3
               Left            =   132
               TabIndex        =   23
               Top             =   960
               Width           =   348
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Source"
               Height          =   192
               Index           =   0
               Left            =   132
               TabIndex        =   22
               Top             =   612
               Width           =   516
            End
         End
         Begin VB.Frame fraFolder 
            Caption         =   "Folder"
            Height          =   1188
            Left            =   120
            TabIndex        =   14
            Top             =   312
            Width           =   5544
            Begin VB.TextBox txtLocation 
               BorderStyle     =   0  'None
               Height          =   390
               Left            =   984
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   15
               Text            =   "frmFolderPropertiesEdit.frx":0070
               Top             =   696
               Width           =   4464
            End
            Begin VB.Label lblLocation 
               AutoSize        =   -1  'True
               Caption         =   "Location:"
               Height          =   192
               Left            =   144
               TabIndex        =   20
               Top             =   696
               Width           =   648
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "Id"
               DataField       =   "folderId"
               Height          =   192
               Index           =   0
               Left            =   984
               TabIndex        =   19
               Top             =   228
               Width           =   4176
               WordWrap        =   -1  'True
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               Height          =   192
               Index           =   1
               Left            =   144
               TabIndex        =   18
               Top             =   456
               Width           =   480
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Id:"
               Height          =   192
               Index           =   5
               Left            =   132
               TabIndex        =   17
               Top             =   228
               Width           =   168
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "Name"
               DataField       =   "name"
               Height          =   192
               Index           =   1
               Left            =   996
               TabIndex        =   16
               Top             =   456
               Width           =   4176
               WordWrap        =   -1  'True
            End
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   4680
            Left            =   -74880
            TabIndex        =   31
            Top             =   312
            Width           =   5595
            _ExtentX        =   9869
            _ExtentY        =   8255
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "Apply"
         Height          =   336
         Left            =   6870
         TabIndex        =   11
         Top             =   5460
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "&Close"
         Height          =   336
         Left            =   8130
         TabIndex        =   12
         Top             =   5460
         Width           =   1200
      End
      Begin MSComctlLib.TreeView tvFolders 
         Height          =   5235
         Left            =   105
         TabIndex        =   0
         Top             =   135
         Width           =   3345
         _ExtentX        =   5900
         _ExtentY        =   9234
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   127
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList imlTVFolders 
         Left            =   1830
         Top             =   5550
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   255
         _Version        =   393216
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save Values"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeperator001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuEditNewKey 
            Caption         =   "&Folder"
            Shortcut        =   ^Y
         End
         Begin VB.Menu mnuSeperator109 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEditNewValue 
            Caption         =   "&Value"
            Shortcut        =   ^U
         End
      End
      Begin VB.Menu mnuSeperator100 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditDelete 
         Caption         =   "&Delete "
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuEditRename 
         Caption         =   "&Rename"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuSeperator101 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditModifyValueData 
         Caption         =   "&Modify Value Data"
      End
      Begin VB.Menu mnuSeperator102 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditFind 
         Caption         =   "&Find"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuEditFindNext 
         Caption         =   "Find Ne&xt"
         Enabled         =   0   'False
         Shortcut        =   {F3}
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsOrganizeFolders 
         Caption         =   "&Organize Folders..."
      End
      Begin VB.Menu mnuSeperator069 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSynchronize 
         Caption         =   "&Synchronize"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator201 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupNewMenu 
         Caption         =   "New"
         Begin VB.Menu mnuPopupNewKey 
            Caption         =   "&Folder"
         End
         Begin VB.Menu mnuSeperator300 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupNewValue 
            Caption         =   "&Value"
         End
      End
      Begin VB.Menu mnuPopupFind 
         Caption         =   "&Find"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeperator301 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupModify 
         Caption         =   "&Modify"
      End
      Begin VB.Menu mnuSeperator302 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuPopupRename 
         Caption         =   "&Rename"
      End
      Begin VB.Menu mnuSeperator070 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupOrganizeFolders 
         Caption         =   "&Organize Folders"
      End
   End
End
Attribute VB_Name = "frmFolderPropertiesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Compontents: Microsoft Windows Common Controls 6.0 (SP 6)
'References: Micrsoft XML, v4.0

Private mDirty As Boolean
Private mNodeKey As String

Private Function GetListIndex(DataField As String, Items As ListItems) As Integer
  GetListIndex = -1
  Dim item As ListItem
  For Each item In Items
    If item.Text = DataField Then
      GetListIndex = item.Index
      Exit Function
    End If
  Next item
End Function

Private Function AddKey() As Node
  Dim FolderHandle As String
  'Get the new folder from the server
  FolderHandle = Factory.PreferenceService.CreateFolder(SystemFolders_TreeName, _
        IIf(tvFolders.SelectedItem.Key = SystemFolders_RootKey, SystemFolders_TreeName, tvFolders.SelectedItem.Key), _
        "New Folder", vbNullStringArray, vbNullStringArray)
  If FolderHandle = vbNullString Then
    Exit Function
  End If
  
  TreeRefresh
  Set AddKey = tvFolders.Nodes(FolderHandle)
End Function

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    cmdApply.Enabled = mDirty
    FormUtil.Dirty Me, mDirty
  End If
End Property

Private Sub FormRefresh()
  TreeRefresh
  NodeRefresh
  Dirty = False
End Sub

'
' Enables and disables the menus
'
Private Sub MenuRefresh()
  Dim ItemsExist, IsLV As Boolean
  Select Case ActiveControl.Name
    Case tvFolders.Name
      ItemsExist = tvFolders.Nodes.Count > 0
    Case lvView.Name
      ItemsExist = lvView.ListItems.Count > 0
      IsLV = True
  End Select

  mnuEditModifyValueData.Enabled = ItemsExist
  mnuPopupModify.Enabled = ItemsExist And IsLV
  mnuEditRename.Enabled = ItemsExist
  mnuPopupRename.Enabled = ItemsExist
  mnuEditDelete.Enabled = ItemsExist
  mnuPopupDelete.Enabled = ItemsExist
End Sub


'
' Gets / sets the node key. This also loads the node's information
'
Private Property Get NodeKey() As String
  NodeKey = mNodeKey
End Property

Private Property Let NodeKey(vData As String)
  'check to insure that the node key is valid
  If tvFolders.Nodes(vData) Is Nothing Then
    Exit Property
  End If

  'set current node
  mNodeKey = vData
  tvFolders.SelectedItem = tvFolders.Nodes(NodeKey)
    
  NodeRefresh
End Property

Private Function save(Optional Verify As Boolean) As Boolean
  If Not mDirty And Verify Then
    save = True
    Exit Function
  End If
    
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If

  '
  'Save Stuff
  '
  ' Load items into an array
  Dim AttributeNames() As String
  Dim AttributeValues() As String

  Dim mItem As ListItem
  For Each mItem In lvView.ListItems
    AddParameter mItem.Text, mItem.ListSubItems("data"), AttributeNames, AttributeValues
  Next mItem

  ' call write
'''  If NodeKey = SystemFolders_RootKey Then
'''    PreferenceUtil.writeIniNodeEntries SystemFolders_TreeName, NodeKey, tvFolders.Nodes(NodeKey).Text, AttributeNames, AttributeValues
'''    save = True
'''  Else
    save = (Factory.PreferenceService.WriteFolder(SystemFolders_TreeName, NodeKey, AttributeNames, AttributeValues) <> vbNullString)
'''  End If
  
  Dirty = Not save

End Function

Private Sub NodeRefresh()
  If NodeKey = vbNullString Then
    Exit Sub
  End If

  'Clear list
  lvView.ListItems.Clear
 
  txtLocation.Text = Factory.PreferenceService.GetPath(SystemFolders_TreeName, NodeKey)
  'Get values
  Dim map As MSXML2.IXMLDOMNode
  Set map = PreferenceUtil.GetNodeMap(SystemFolders_TreeName, NodeKey)
  If map Is Nothing Then
    Exit Sub
  End If
  'Load list and populate data collection for SetFields
  Dim EntryList As IXMLDOMNodeList
  Dim Entry As IXMLDOMNode
  Set EntryList = map.selectNodes("entry")
  
  Dim Datafields As New Collection
  Dim tempKey As String
  Dim tempValue As String
  For Each Entry In EntryList
    tempKey = Entry.Attributes.getNamedItem("key").Text
    'Hack for missing line returns. normally handled in XMLUtil.DataValue()
    tempValue = Replace(Entry.Attributes.getNamedItem("value").Text, Chr(10), vbNewLine)
    
    With lvView.ListItems.Add(, , tempKey)
      .ListSubItems.Add , "data", tempValue
    End With
    
  Next

  'Build a collection of values from the list view
  Dim mItem As ListItem
  For Each mItem In lvView.ListItems
    Datafields.Add CreateDataField("entry", mItem.Text, mItem.ListSubItems("data")), mItem.Text
  Next mItem
  
  'Load the values
  lbldatafield(0).Caption = NodeKey
  lbldatafield(1).Caption = tvFolders.Nodes(NodeKey).Text
  SetFields txtDataField, Datafields
  SetFields chkDatafield, Datafields
  
  Dirty = False
End Sub

Public Sub ShowFolder(FolderKey As String)
  If Contains(tvFolders.Nodes, FolderKey) Then
    NodeKey = FolderKey
  End If
  Show
End Sub

Private Sub TreeRefresh()
  
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  'ver 2.30.0826-B (jjm)
  If Contains(tvFolders.Nodes, NodeKey) Then
    tvFolders.SelectedItem = tvFolders.Nodes(NodeKey)
  End If

End Sub

Private Sub cboDataType_Change()
  Dirty = True
End Sub

Private Sub chkSearch_Click()
  Dirty = True
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dim strDatafield As String
  Dim ListIndex As Integer
  
  strDatafield = chkDatafield(Index).DataField
  ListIndex = GetListIndex(strDatafield, lvView.ListItems)
  
  'remove from list if null
  If chkDatafield(Index).Value = 0 Then
    If ListIndex > -1 Then
      lvView.ListItems.Remove ListIndex
    End If
    Dirty = True
    Exit Sub
  End If
    
  If ListIndex > -1 Then
    lvView.ListItems(ListIndex).ListSubItems("data").Text = chkDatafield(Index).Value
  Else
    With lvView.ListItems.Add(, , strDatafield)
      .ListSubItems.Add , "data", chkDatafield(Index).Value
    End With
  End If
  Dirty = True
End Sub

Private Sub cmdApply_Click()
  save
End Sub

Private Sub cmdClose_Click()
  mnuFileExit_Click
End Sub


Private Sub Form_Load()
  
  lvView.ColumnHeaders.Clear
  lvView.ColumnHeaders.Add , "name", "Name"
  lvView.ColumnHeaders.Add , "data", "Data"
  lvView.ColumnHeaders("data").Width = lvView.Width - lvView.ColumnHeaders("name").Width

  'load images for Tree View
  imlTVFolders.ListImages.Add 1, "TVFolderImage", PicDisplay.GetPicture(UrlName.TVFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 2, "TVOpenFolderImage", PicDisplay.GetPicture(UrlName.TVOPENFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 3, "LAWFolder", PicDisplay.GetPicture(UrlName.LAWFOLDER_IMAGE)
  tvFolders.ImageList = imlTVFolders
    
  txtLocation.BackColor = lblLocation.BackColor
  FormUtil.TabControlEnable SSTab1
    
  TreeRefresh
  
  'Set a nodekey if there is none
  If Not tvFolders.Nodes.item(1) Is Nothing And NodeKey = vbNullString Then
    NodeKey = tvFolders.Nodes.item(1).Key
    tvFolders.Nodes.item(1).Expanded = True
  End If

End Sub

Private Sub lvview_DblClick()
  mnuEditModifyValueData_Click
End Sub

Private Sub lvView_GotFocus()
  MenuRefresh
End Sub

Private Sub lvView_KeyDown(KeyCode As Integer, Shift As Integer)
  'ver 1.01 (jjm) 2004-04-30
  If KeyCode = vbKeyReturn Then
    mnuEditModifyValueData_Click
  End If
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    PopupMenu mnuPopup, , , , mnuPopupModify
  End If
End Sub

Private Sub mnuEditDelete_Click()
  If ActiveControl Is Nothing Then Exit Sub
  Select Case ActiveControl.Name
    Case lvView.Name
      If Not lvView.SelectedItem Is Nothing Then
        lvView.ListItems.Remove lvView.SelectedItem.Index
        Dirty = True
        If lvView.ListItems.Count > 0 Then
          lvView.SelectedItem.Selected = True
        End If
      End If

    Case tvFolders.Name
      If MsgBox("Are you sure you want to delete this key?", vbYesNo) = vbYes Then
        If Factory.PreferenceService.DeleteFolder(SystemFolders_TreeName, tvFolders.SelectedItem.Key) Then
          tvFolders.Nodes.Remove tvFolders.SelectedItem.Key
          NodeKey = tvFolders.SelectedItem.Key
        End If
      End If
  End Select

End Sub

Private Sub mnuEditModifyValueData_Click()
  If Not lvView.ListItems.Count > 0 Then
    Exit Sub
  End If

  Dim temp As String
  temp = InputBox("Value Name:  " & lvView.SelectedItem.Text & vbCrLf & vbCrLf & _
      "Value Data:", "Modify Value Data", lvView.SelectedItem.ListSubItems("data"))
  
  If temp = "" Then
    Exit Sub
  End If
  
  lvView.SelectedItem.ListSubItems("data") = temp
  Dirty = True
End Sub

Private Sub mnuEditNewKey_Click()
  'ver 1.01 (jjm) 2004-04-30
  Dim NewNode As Node
  Set NewNode = AddKey
  MenuRefresh

  tvFolders.SetFocus
  If Not NewNode Is Nothing Then
    NewNode.Selected = True
    mnuEditRename_Click
  End If
  
End Sub

Private Sub mnuEditNewValue_Click()
  With lvView.ListItems.Add(, , "New Value")
    .ListSubItems.Add , "data", vbNullString
    .Selected = True 'ver 1.01 (jjm) 2004-04-30
  End With
  
  Dirty = True
  MenuRefresh
  
  'ver 1.01 (jjm) 2004-04-30
  'Set the focus to the recently added item along with the rename
  SSTab1.Tab = 1
  lvView.SetFocus
  mnuEditRename_Click
  
End Sub

Private Sub mnuEditRename_Click()
  If ActiveControl Is Nothing Then Exit Sub
  Select Case ActiveControl.Name
    Case tvFolders.Name
      tvFolders.SetFocus
      tvFolders.StartLabelEdit
    Case lvView.Name
      lvView.SetFocus
      lvView.StartLabelEdit
  End Select
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then
    MsgBox "Clerk must be restarted before changes can take effect."
    Unload Me
  End If
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDelete_Click
End Sub

Private Sub mnuPopupModify_Click()
  mnuEditModifyValueData_Click
End Sub

Private Sub mnuPopupNewKey_Click()
  mnuEditNewKey_Click
End Sub

Private Sub mnuPopupNewValue_Click()
  mnuEditNewValue_Click
End Sub

Private Sub mnuPopupOrganizeFolders_Click()
  mnuToolsOrganizeFolders_Click
End Sub

Private Sub mnuPopupRename_Click()
  mnuEditRename_Click
End Sub

Private Sub mnuSynchronize_Click()
  save True
  If Factory.PreferenceService.Sync(SystemFolders_TreeName) Then
    FormRefresh
  End If
End Sub

Private Sub mnuToolsOrganizeFolders_Click()
  Dim fForm As New frmFolderOrganizeDialog
  fForm.ShowFolder NodeKey
  Unload fForm
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  'FolderUtil.TreeViewLoadFolder tvFolders, systemfolders_treename
  If Contains(tvFolders.Nodes, NodeKey) Then
    tvFolders.Nodes(NodeKey).Selected = True
  End If
  TreeRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub tvFolders_AfterLabelEdit(Cancel As Integer, NewString As String)
  'If lvView.SelectedItem.Text <> NewString Then
  If Not DirNameSafe(NewString) Then
    Cancel = True
    Exit Sub
  End If
  
'''  If tvFolders.SelectedItem.Key = SystemFolders_RootKey Then
'''    SystemFolders_RootName = NewString
'''    PreferenceUtil.renameIniNode SystemFolders_TreeName, SystemFolders_RootName
'''  Else
    Cancel = Not Factory.PreferenceService.RenameFolder(SystemFolders_TreeName, tvFolders.SelectedItem.Key, NewString)
'''  End If

End Sub

Private Sub tvFolders_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    PopupMenu mnuPopup, , , , mnuPopupRename
  End If
End Sub

Private Sub tvFolders_NodeClick(ByVal Node As MSComctlLib.Node)
  If Node.Key = NodeKey Then
    Exit Sub
  End If

  If Not save(True) Then
    'make no changes, leave as is
    tvFolders.Nodes(NodeKey).Selected = True
    Exit Sub
  End If

  NodeKey = Node.Key
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dim strDatafield As String
  Dim ListIndex As Integer
  
  strDatafield = txtDataField(Index).DataField
  ListIndex = GetListIndex(strDatafield, lvView.ListItems)
  
  'remove from list if null
  If txtDataField(Index).Text = vbNullString Then
    If ListIndex > -1 Then
      lvView.ListItems.Remove ListIndex
    End If
    Dirty = True
    Exit Sub
  End If
    
  'write
  If ListIndex > -1 Then
    lvView.ListItems(ListIndex).ListSubItems("data").Text = txtDataField(Index).Text
  Else
    With lvView.ListItems.Add(, , strDatafield)
      .ListSubItems.Add , "data", txtDataField(Index).Text
    End With
  End If
  
  Dirty = True
End Sub

