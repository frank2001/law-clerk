Attribute VB_Name = "ListViewModule"
Option Explicit


Global Const LVM_FIRST As Long = &H1000
Global Const LVM_HITTEST As Long = (LVM_FIRST + 18)
Public Const LVM_SETCOLUMNWIDTH As Long = (LVM_FIRST + 30)
Global Const LVM_SETITEMSTATE = (LVM_FIRST + 43)
Global Const LVM_GETITEMSTATE As Long = (LVM_FIRST + 44)
Global Const LVM_GETSELECTEDCOUNT = (LVM_FIRST + 50)
Global Const LVM_SUBITEMHITTEST As Long = (LVM_FIRST + 57)
Global Const LVM_SETEXTENDEDLISTVIEWSTYLE = (LVM_FIRST + 54)
Global Const LVM_GETEXTENDEDLISTVIEWSTYLE = (LVM_FIRST + 55)
Global Const LVM_GETCOLUMN = (LVM_FIRST + 25)
Global Const LVM_SETCOLUMNPOSITIONARRAY = (LVM_FIRST + 58)
Global Const LVM_GETCOLUMNPOSITIONARRAY = (LVM_FIRST + 59)

Global Const LVCF_TEXT = &H4

Global Const LVIS_SELECTED = &H2
Global Const LVIS_STATEIMAGEMASK As Long = &HF000

Global Const LVIF_STATE = &H8

Global Const LVHT_NOWHERE As Long = &H1
Global Const LVHT_ONITEMICON As Long = &H2
Global Const LVHT_ONITEMLABEL As Long = &H4
Global Const LVHT_ONITEMSTATEICON As Long = &H8
Global Const LVHT_ONITEM As Long = (LVHT_ONITEMICON Or _
                                     LVHT_ONITEMLABEL Or _
                                     LVHT_ONITEMSTATEICON)

Global Const LVS_EX_FULLROWSELECT = &H20
Global Const LVS_EX_GRIDLINES = &H1
Global Const LVS_EX_CHECKBOXES = &H4
Global Const LVS_EX_HEADERDRAGDROP = &H10

Global Const LVSCW_AUTOSIZE As Long = -1
Global Const LVSCW_AUTOSIZE_USEHEADER As Long = -2


Type LVHITTESTINFO
    pt As POINTAPI
    flags As Long
    iItem As Long
    iSubItem  As Long  'ie3+ only .. was NOT in win95.
                       'Valid only for LVM_SUBITEMHITTEST
End Type

Type LV_ITEM
   Mask As Long
   iItem As Long
   iSubItem As Long
   State As Long
   stateMask As Long
   pszText As String
   cchTextMax As Long
   iImage As Long
   lParam As Long
   iIndent As Long
End Type

Type lvColumn
    Mask As Long
    fmt As Long
    cx As Long
    pszText  As String
    cchTextMax As Long
    iSubItem As Long
    iImage As Long
    iPosition As Long
End Type


Public Sub AutosizeWidth(View As ListView)

  'Size each column based on the width
  'of the widest list item in the column.
  'If the items are shorter than the column
  'header text, the header text is truncated.

  'You may need to lengthen column header
  'captions to see this effect.
  Dim col2adjust As Long

  With View
    For col2adjust = 0 To .ColumnHeaders.Count - 1
      If .ColumnHeaders(col2adjust + 1).Width > 0 Then
        Call SendMessage(.hWnd, _
                         LVM_SETCOLUMNWIDTH, _
                         col2adjust, _
                         ByVal LVSCW_AUTOSIZE)
      End If
    Next
  End With
   
End Sub


Public Sub SelectAll(hWnd As Long)
  Dim LV As LV_ITEM
  Dim numSelected As Long
   
  With LV
    .Mask = LVIF_STATE
    .State = True
    .stateMask = LVIS_SELECTED
  End With
      
  'by setting wParam to -1, the call affects all
  'listitems. To just change a particular item,
  'pass its index as wParam.
   Call SendMessage(hWnd, _
                    LVM_SETITEMSTATE, _
                     -1, _
                     LV)
End Sub


'
' Return the number of selected items in list view.
'
Public Function SelectedCount(hWnd As Long) As Long
  SelectedCount = SendMessage(hWnd, LVM_GETSELECTEDCOUNT, 0&, ByVal 0&)
End Function

'
' Determine if a click on a subitem or white space.
'
Public Function SubItemClick(hWnd As Long, X As Single, Y As Single) As Boolean
  Dim HTI As LVHITTESTINFO, itmX As ListItem
     
  '----------------------------------------
  'this gets the hittest info using
  'the mouse co-ordinates
  With HTI
    .pt.X = (X \ Screen.TwipsPerPixelX)
    .pt.Y = (Y \ Screen.TwipsPerPixelY)
    .flags = LVHT_ONITEM
  End With
      
  Call SendMessage(hWnd, LVM_SUBITEMHITTEST, 0, HTI)
     
  ' Trap for click on subitem or click on main item whitespace.
  SubItemClick = (HTI.iSubItem <> 0 Or HTI.iItem < 0)
End Function


'
' Save ListView To Ini.
'
Public Sub Serialize(View As ListView, Ini As IniFile, Section As String)
  Dim Item, SubItem As Variant
  Dim I, j As Integer, IniKey As String
  IniKey = View.Name
  I = 1
  
  Ini.WriteFile Section, IniKey & ".count", View.ListItems.Count
  For Each Item In View.ListItems
    With Item
      Ini.WriteFile Section, IniKey & "." & I & ".key", .Key
      Ini.WriteFile Section, IniKey & "." & I & ".text", .Text
      Ini.WriteFile Section, IniKey & "." & I & ".count", .ListSubItems.Count
      j = 1
      For Each SubItem In .ListSubItems
        Ini.WriteFile Section, IniKey & "." & I & ".subitem." & j & ".text", SubItem.Text
        j = j + 1
      Next SubItem
      I = I + 1
    End With
  Next Item
End Sub

'
' Read List View To Ini.
'
Public Sub Deserialize(View As ListView, Ini As IniFile, Section As String)
  Dim I, j As Integer
  Dim IniKey, Count, Key, Text, SubCount, SubKey, SubText As String
  IniKey = View.Name
  For I = 1 To 1000
    Key = Ini.ReadFile(Section, IniKey & "." & I & ".key")
    If (Key = "") Then
      Exit For
    End If
    Text = Ini.ReadFile(Section, IniKey & "." & I & ".text")
    If Text = "" Then Text = Key
    
    With View.ListItems.Add(, Key, Text)
      For j = 1 To 10
        SubKey = Ini.ReadFile(Section, IniKey & "." & I & ".subitem." & j & ".key")
        SubText = Ini.ReadFile(Section, IniKey & "." & I & ".subitem." & j & ".text")
        If (SubKey <> "" Or SubText <> "") Then
          .ListSubItems.Add , SubKey, SubText
        End If
      Next j
    End With
  Next I
End Sub

Public Function DeserializeArray(IniKey As String, Ini As IniFile, Section As String, _
    TextArray() As String, Element As String, Optional SubItemIndex As Integer) As Integer
    
  Dim I As Integer, Count As String
  
  Count = Ini.ReadFile(Section, IniKey & ".count")
  If Count = vbNullString Then Exit Function
  If Count = 0 Then Exit Function
  
  ReDim TextArray(Count - 1)
  For I = 1 To Count
    If SubItemIndex = 0 Then
      TextArray(I - 1) = Ini.ReadFile(Section, IniKey & "." & I & "." & LCase(Element))
    Else
      TextArray(I - 1) = Ini.ReadFile(Section, IniKey & "." & I & ".subitem." & SubItemIndex & "." & LCase(Element))
    End If
  Next I
  DeserializeArray = Count
End Function


'
' Serialize column headers.  Do not rewrite section since only visible columns are
' serialized.
'
Public Sub SerializeColumnHeaders(View As ListView, Section As String, Optional FileName As String, Optional Ini As IniFile)
  If FileName <> vbNullString Then
    Set Ini = New IniFile
    Ini.FileName = FileName
  End If
    
  ' Do not rewrite since we are only writing out visible.
  Ini.WriteFile Section, "Ver", "1.53 (" & Now & ")"
  Ini.WriteFile Section, "View", View.View
  Ini.WriteFile Section, "SortKey", View.SortKey
  Ini.WriteFile Section, "SortOrder", View.SortOrder
  
  ' Only serialize count if one has not been set.  This is necessary since the
  ' settings may include columns that are not displayed in the ColumnHeaders
  If Ini.ReadFile(Section, "Count") = "" Then
    Ini.WriteFile Section, "Count", View.ColumnHeaders.Count
  End If
  
  Dim Header As Variant, HeaderKey As String
  For Each Header In View.ColumnHeaders
    With Header
      HeaderKey = .Index
      Ini.WriteFile Section, HeaderKey & ".Key", .Key
      Ini.WriteFile Section, HeaderKey & ".Text", .Text
      Ini.WriteFile Section, HeaderKey & ".Position", .Position
      
      If Round(.Width, 0) <> 1440 Then
        Ini.WriteFile Section, HeaderKey & ".Width", .Width
      Else
        Ini.RemoveKey Section, HeaderKey & ".Width"
      End If
      If .Alignment > 0 Then
        Ini.WriteFile Section, HeaderKey & ".Alignment", .Alignment
      Else
        Ini.RemoveKey Section, HeaderKey & ".Alignment"
      End If
      If .Tag = "" Then
        Ini.RemoveKey Section, HeaderKey & ".Tag"
      Else
        Ini.WriteFile Section, HeaderKey & ".Tag", .Tag
      End If
      If .Icon = 0 Then
        Ini.RemoveKey Section, HeaderKey & ".Icon"
      Else
        Ini.WriteFile Section, HeaderKey & ".Icon", .Icon
      End If
    End With
  Next Header
End Sub


Public Sub DeserializeColumnHeaders(View As ListView, Section As String, Optional Ini As IniFile)
  If Ini Is Nothing Then
    Set Ini = New IniFile
    Ini.FileName = FolderIniFileActive(Section)
  End If
    
  View.ColumnHeaders.Clear
  
  ' Set view and ensure that section is available.
  Dim Val As String
  Val = Ini.ReadFile(Section, "View")
  If Val = "" Then
    Exit Sub
  End If
  View.View = Val

  ' Set headers
  Dim PosArray(254) As Long, j As Long, I As Integer, HeaderKey As String
  
  ' Go through ini file
  For I = 1 To 255
    HeaderKey = I
    
    Dim Position As String
    Position = Ini.ReadFile(Section, HeaderKey & ".Position")
    If Position = "" Then
      Exit For
    End If
    If Position < 0 Then
      Exit For
    End If
    
    Dim Header As ColumnHeader
    Set Header = View.ColumnHeaders.Add(, _
      Ini.ReadFile(Section, HeaderKey & ".Key"), _
      Ini.ReadFile(Section, HeaderKey & ".Text"), _
      GetInteger(Ini.ReadFile(Section, HeaderKey & ".Width"), 1440), _
      GetInteger(Ini.ReadFile(Section, HeaderKey & ".Alignment")))
      
    ' Set tag
    Header.Tag = Ini.ReadFile(Section, HeaderKey & ".Tag")
    
    ' Only set icon if it is non-zero
    Dim Icon As Integer
    Icon = GetInteger(Ini.ReadFile(Section, HeaderKey & ".Icon"))
    If Icon > 0 Then
      Header.Icon = Icon
    End If
          
    PosArray(Position - 1) = j
    j = j + 1
  Next I
  
  Call SendMessage(View.hWnd, _
                   LVM_SETCOLUMNPOSITIONARRAY, _
                   j, PosArray(0))
  
  ' Ensure that sort key does not go under 0, or over the column count - 1.
  View.SortKey = Min(GetInteger(Ini.ReadFile(Section, "SortKey")), Max(View.ColumnHeaders.Count - 1, 0))
  View.SortOrder = GetInteger(Ini.ReadFile(Section, "SortOrder"))
  View.Refresh
End Sub


Private Sub CopyFile(Ini As IniFile, Section As String, Key As String, Value As String)
  If Value <> vbNullString Then
    Ini.WriteFile Section, Key, Value
  End If
End Sub

Public Sub CopyColumnHeaders(Section As String, FileName As String, ToSection As String, ToFileName As String)
  Dim Ini As New IniFile, IniTo As New IniFile, I As Integer, Count As Integer, HeaderKey As String
  
  Ini.FileName = FileName
  IniTo.FileName = ToFileName
  
  IniTo.RemoveSection ToSection
  CopyFile IniTo, ToSection, "View", Ini.ReadFile(Section, "View")
  CopyFile IniTo, ToSection, "SortKey", Ini.ReadFile(Section, "SortKey")
  CopyFile IniTo, ToSection, "SortOrder", Ini.ReadFile(Section, "SortOrder")
  
  Count = GetInteger(Ini.ReadFile(Section, "Count"))
  If Count = 0 Then Exit Sub
  
  CopyFile IniTo, ToSection, "Count", CStr(Count)
  
  ' Go through ini file
  For I = 1 To Count
    HeaderKey = I
    CopyFile IniTo, ToSection, HeaderKey & ".Key", Ini.ReadFile(Section, HeaderKey & ".Key")
    CopyFile IniTo, ToSection, HeaderKey & ".Text", Ini.ReadFile(Section, HeaderKey & ".Text")
    CopyFile IniTo, ToSection, HeaderKey & ".Width", Ini.ReadFile(Section, HeaderKey & ".Width")
    CopyFile IniTo, ToSection, HeaderKey & ".Alignment", Ini.ReadFile(Section, HeaderKey & ".Alignment")
    CopyFile IniTo, ToSection, HeaderKey & ".Position", Ini.ReadFile(Section, HeaderKey & ".Position")
    CopyFile IniTo, ToSection, HeaderKey & ".Tag", Ini.ReadFile(Section, HeaderKey & ".Tag")
    CopyFile IniTo, ToSection, HeaderKey & ".Required", Ini.ReadFile(Section, HeaderKey & ".Required")
  Next I
End Sub


Public Sub SerializeColumnSettings(frm As frmColumnSettingsDialog, Section As String, Ini As IniFile)
  'Save settings
  Dim ViewSetting, SortKey, SortOrder As String
  ViewSetting = Ini.ReadFile(Section, "View")
  SortKey = Ini.ReadFile(Section, "SortKey")
  SortOrder = Ini.ReadFile(Section, "SortOrder")
  
  'Rewrite
  Ini.RemoveSection Section
  Ini.WriteFile Section, "Ver", "1.53 (" & Now & ")"
  Ini.WriteFile Section, "View", CStr(ViewSetting)
  Ini.WriteFile Section, "SortKey", CStr(SortKey)
  Ini.WriteFile Section, "SortOrder", CStr(SortOrder)

  ' Save count
  Ini.WriteFile Section, "Count", frm.lvColumn.ListItems.Count
  
  Dim ItemIndex As Integer
  ItemIndex = frm.cboItem.ListIndex + 1
  
  ' Pass through and write out ItemIndex
  Dim Position As Integer
  Position = 1
  Dim Item As Variant
  For Each Item In frm.lvColumn.ListItems
    If Item.ListSubItems("Index") = ItemIndex Then
      Item.Checked = True
      Item.ListSubItems("Alignment") = lvwColumnLeft
      SerializeItem Item, 1, Position, Ini, Section
      Exit For
    Else
      Position = Position + 1
    End If
  Next Item
  
  ' Pass through and do checked items, except for ItemIndex
  Dim Index As Integer
  Index = 2
  Position = 1
  For Each Item In frm.lvColumn.ListItems
    If Item.Checked Then
      If Item.ListSubItems("Index") = ItemIndex Then
        Position = Position + 1
      Else
        SerializeItem Item, Index, Position, Ini, Section
        Index = Index + 1
      End If
    End If
  Next Item
  
  ' Pass through and write non-checked items.
  For Each Item In frm.lvColumn.ListItems
    If Not Item.Checked Then
      SerializeItem Item, Index, Position, Ini, Section
      Index = Index + 1
    End If
  Next Item
  
End Sub


Public Sub DeserializeColumnSettings(frm As frmColumnSettingsDialog, Ini As IniFile, Section As String)
  Dim I As Integer, Count As Integer, Ver As String
  Ver = Ini.ReadFile(Section, "Ver")
  If Ver <> vbNullString Then
    Count = Ini.ReadFile(Section, "Count")
  Else
    Count = 1000
  End If
  
  For I = 1 To Count
    Dim HeaderKey As String
    HeaderKey = I
    
    ' Use Width as required key for older versions.
    If Count = 1000 Then
      If Ini.ReadFile(Section, HeaderKey & ".Width") = "" Then
        Exit For
      End If
    End If
    
    frm.AddItem _
        I, _
        Ini.ReadFile(Section, HeaderKey & ".Key"), _
        Ini.ReadFile(Section, HeaderKey & ".Text"), _
        GetInteger(Ini.ReadFile(Section, HeaderKey & ".Alignment")), _
        GetInteger(Ini.ReadFile(Section, HeaderKey & ".Width"), 1440), _
        GetInteger(Ini.ReadFile(Section, HeaderKey & ".Position")), _
        Ini.ReadFile(Section, HeaderKey & ".Tag"), _
        Ini.ReadFile(Section, HeaderKey & ".Required") = "1"
  Next I
End Sub

Private Sub SerializeItem(Item As Variant, Index As Integer, Position As Integer, Ini As IniFile, Section As String)
  With Item
    Dim HeaderKey As String
    HeaderKey = Index
    Ini.WriteFile Section, HeaderKey & ".Key", .Key
    Ini.WriteFile Section, HeaderKey & ".Text", .Text
    
    If .ListSubItems("Alignment") > 0 Then
      Ini.WriteFile Section, HeaderKey & ".Alignment", .ListSubItems("Alignment")
    End If
    If .Tag <> "" Then
      Ini.WriteFile Section, HeaderKey & ".Tag", .Tag
    End If
    If .Checked Then
      Ini.WriteFile Section, HeaderKey & ".Position", CStr(Position)
      Position = Position + 1
      If .ListSubItems("Width") <> 1440 Then
        Ini.WriteFile Section, HeaderKey & ".Width", .ListSubItems("Width")
      End If
    End If
    If .ListSubItems("Required") Then
      Ini.WriteFile Section, HeaderKey & ".Required", 1
    End If
  End With
End Sub


Public Function ColumnSettings(View As ListView, Source As String, Section As String) As Boolean
  Dim Ini As New IniFile
  Ini.FileName = FolderIniFileActive(Section)
  
  ' If there are no settings, serialize current settings.
  If Ini.ReadFile(Section, "Count") = "" Then
    SerializeColumnHeaders View, Section, , Ini
    ' If still no settings, then exit
    If Ini.ReadFile(Section, "Count") = "" Then
      MsgBox "There are no columns to set."
      ColumnSettings = False
      Exit Function
    End If
  End If
  
  Dim fForm As New frmColumnSettingsDialog
  fForm.Source = Source
  fForm.FolderName = Section
  fForm.LoadItems Ini
  fForm.Caption = Source + " Column Settings (" & Section & ")"
  fForm.Show vbModal
  If Not fForm.OK Then
    Unload fForm
    ColumnSettings = False
    Exit Function
  End If
  
  If fForm.SaveAsDefault Then
    fForm.KillPersonal
  End If
  
  Dim SaveFile As String
  SaveFile = FolderIniFile(Section, Not fForm.SaveAsDefault)
  If Ini.FileName <> SaveFile Then
    If Not IsFile(SaveFile) Then
      CopyColumnHeaders Section, Ini.FileName, Section, SaveFile
    End If
    Ini.FileName = SaveFile
  End If
      
  SerializeColumnSettings fForm, Section, Ini
  Unload fForm
  
  View.Visible = False
  View.ListItems.Clear
  DeserializeColumnHeaders View, Section, Ini
  View.Visible = True
  
  ColumnSettings = True
End Function


Public Function ListViewSelected(View As ListView) As Collection
  Dim I, Count, Selected As Integer
  Dim Result As New Collection
  
  Count = View.ListItems.Count
  ' Set List Count
  If Count = 0 Or SelectedCount(View.hWnd) = 0 Then
    MsgBox "There are no items available or none selected."
    Exit Function
  End If
  
  For I = 1 To Count
    If View.ListItems(I).Selected Then
      Result.Add View.ListItems(I)
    End If
  Next I
  
  Set ListViewSelected = Result
End Function

'
' Return an array of text items.
'
Public Function ListViewText(View As ListView, Index As String) As String()
  Dim Item As ListItem, Count, j, I As Integer
  ReDim Text(ListViewModule.SelectedCount(View.hWnd) - 1) As String
  j = 0
  Count = View.ListItems.Count
  For I = 1 To Count
    Set Item = View.ListItems(I)
    If Item.Selected Then
      Text(j) = ListItemText(Item, Index)
      j = j + 1
    End If
  Next I
  ListViewText = Text
End Function

'
' Load list view from DataProvider
'
Public Sub ListViewLoad(View As ListView, Source As DataProvider, Optional Refresh As Boolean, Optional Icon As Boolean)
  If Refresh Or Not Source.isOpen Then
    ' See if headers need initialization
    If View.Tag <> "" And View.ColumnHeaders.Count = 0 Then
      DeserializeColumnHeaders View, View.Tag
    End If
    Source.Column = LawSQL.Column(View.ColumnHeaders)
    Source.Sort = LawSQL.Sort(View)
    Source.OpenRecordset
    Source.SetListView View, Icon
  End If
End Sub


Public Sub ListViewSort(View As ListView, ColumnHeader As MSComctlLib.ColumnHeader, Optional Source As DataProvider, Optional Icon As Boolean)
  If View.SortKey = ColumnHeader.Index - 1 Then
    View.SortOrder = IIf(View.SortOrder = lvwAscending, lvwDescending, lvwAscending)
  Else
    View.SortOrder = lvwAscending
  End If
  View.SortKey = ColumnHeader.Index - 1
  If Not Source Is Nothing Then
    ListViewLoad View, Source, True, Icon
    View.Refresh
  End If
End Sub


Public Sub ChooseColumns(View As ListView, Source As DataProvider, Optional Icon As Boolean)
  If ColumnSettings(View, Source.Source, View.Tag) Then
    ListViewLoad View, Source, True, Icon
  End If
End Sub

Public Sub SaveSettings(View As ListView, Section As String)
  SerializeColumnHeaders View, Section, FolderIniFile(Section, True)
End Sub


Public Function ListItemArray(View As ListView, Index As String) As String()
  Dim Item As ListItem
  Dim ListCount, Count As Integer
  
  ListCount = View.ListItems.Count
  Count = ListViewModule.SelectedCount(View.hWnd)
  If ListCount = 0 Or Count = 0 Then
    MsgBox "No items are available or none have been selected."
    Exit Function
  End If
  
  Dim I, j As Integer
  ReDim Handles(Count - 1) As String
  j = 0
  For I = 1 To ListCount
    Set Item = View.ListItems(I)
    If Item.Selected Then
      Handles(j) = ListItemText(Item, Index)
      j = j + 1
    End If
  Next I
  ListItemArray = Handles
End Function


'
' Return text from a list item by Index.  Looks in both the Item and SubItems
'
Public Function ListItemText(Item As ListItem, Index As String) As String
On Error GoTo Exception
  
  If Item.Tag = Index Then
    ListItemText = Item
  Else
    ListItemText = Item.ListSubItems(Index)
  End If
  
Exception:
  Err.Clear
End Function


Public Function ListItemCollection(Headers As ColumnHeaders, Item As ListItem, Optional Source As String) As Collection
  Dim Collection As New Collection, Header As ColumnHeader
  For Each Header In Headers
    Collection.Add CreateDataField(Source, Header.Key, ListItemText(Item, Header.Key)), Header.Key
  Next Header
  Set ListItemCollection = Collection
End Function

Public Function AddListItem(View As ListView) As ListItem
  Dim Item As ListItem, I As Integer
  
  Set Item = View.ListItems.Add
  Item.Tag = View.ColumnHeaders(1).Key
  For I = 2 To View.ColumnHeaders.Count
    Item.ListSubItems.Add , View.ColumnHeaders(I).Key
  Next I
  
  Set AddListItem = Item
End Function

'
' Set text.
'
Public Sub SetItemText(Item As ListItem, Index As String, Text As String)
On Error GoTo Exception
  
  If Item.Tag = Index Then
    Item.Text = Text
  Else
    Item.ListSubItems(Index).Text = Text
  End If
  
Exception:
  Err.Clear
End Sub


Public Sub SetItemCollection(Item As ListItem, Items As Collection)
  Dim Field As DataField
  For Each Field In Items
    SetItemText Item, Field.Name, Field.Value
  Next Field
End Sub

