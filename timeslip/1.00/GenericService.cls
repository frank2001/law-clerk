VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GenericService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mClient As SoapClient
Private mDataProvider As DataProvider


'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mClient = New SoapClient
  mClient.mssoapinit WSDLFile
  mClient.ConnectorProperty("EndPointURL") = EndPointURL
  
  Set mDataProvider = Session.NewDataProvider
End Sub


'
' Data Access
'
Public Function Find(Source As String, HandleName As String, Handle As String, Optional Column As String) As Collection
  mDataProvider.Source = Source
  Set Find = mDataProvider.Find(HandleName, Handle, Column)
End Function


'
' Create an application.
'
Public Function Update(Source As String, _
    HandleNames() As String, HandleValues() As String, _
    AttributeNames() As String, AttributeValues() As String, _
    ParameterNames() As String, ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
  
  Update = mClient.Update( _
      Source, _
      HandleNames, HandleValues, _
      AttributeNames, AttributeValues, _
      ParameterNames, ParameterValues)
  Exit Function
  
ErrorHandler:
  MsgBox "Generic Service Update Error: " & Err.Description, vbExclamation
  Err.Clear
End Function
