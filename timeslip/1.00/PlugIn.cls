VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TimeSlipPlugIn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const GenericServiceWSDL = "GenericService.wsdl"
Private Const AutoKeyServiceWSDL = "AutoKeyService.wsdl"

Private mSession As Session
Private mGenericService As GenericService
Private mAutoKeyService As AutoKeyService


Public Property Set Session(vData As Session)
  Set mSession = vData
  Set mGenericService = Nothing
  Set mAutoKeyService = Nothing
End Property


Public Function NewFinder() As DataFinder
  Set NewFinder = New TimeSlipFinder
  Set NewFinder.Session = mSession
End Function


Public Function NewPopupMenuForm() As Object
  Set NewPopupMenuForm = New frmTimeSlipPopup
  Set NewPopupMenuForm.PlugIn = Me
End Function


Public Sub ShowByListView(FormMgr As FormManager, View As Object)
  FormUtil.ShowByListView Me, New frmTimeSlipEdit, FormMgr, View
End Sub

Public Sub ShowNew(Optional FormMgr As FormManager)
  FormUtil.ShowNew Me, New frmTimeSlipEdit, FormMgr
End Sub


Public Property Get AutoKeyService() As AutoKeyService
  If mAutoKeyService Is Nothing Then
    Set mAutoKeyService = New AutoKeyService
    mAutoKeyService.Init _
        mSession.Connection & ComponentService & AutoKeyServiceWSDL, _
        mSession.Connection & WebService & mSession.Session, _
        mSession
  End If
  Set AutoKeyService = mAutoKeyService
End Property


Public Property Get GenericService() As GenericService
  If mGenericService Is Nothing Then
    Set mGenericService = New GenericService
    mGenericService.Init _
        mSession.Connection & ComponentService & GenericServiceWSDL, _
        mSession.Connection & WebService & mSession.Session, _
        mSession
  End If
  Set GenericService = mGenericService
End Property
