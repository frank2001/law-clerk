VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMainOptionsDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Options"
   ClientHeight    =   4272
   ClientLeft      =   36
   ClientTop       =   324
   ClientWidth     =   4680
   Icon            =   "frmOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4272
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "Options"
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   3360
      TabIndex        =   4
      Tag             =   "Cancel"
      Top             =   3828
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   2064
      TabIndex        =   3
      Tag             =   "OK"
      Top             =   3840
      Width           =   1200
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3828.617
      ScaleMode       =   0  'User
      ScaleWidth      =   5733.383
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         Height          =   2029
         Left            =   506
         TabIndex        =   9
         Tag             =   "Sample 4"
         Top             =   504
         Width           =   2039
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   2
      Left            =   -20000
      ScaleHeight     =   3828.617
      ScaleMode       =   0  'User
      ScaleWidth      =   5733.383
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Sample 3"
         Height          =   2029
         Left            =   407
         TabIndex        =   8
         Tag             =   "Sample 3"
         Top             =   405
         Width           =   2039
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3828.617
      ScaleMode       =   0  'User
      ScaleWidth      =   5733.383
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame Frame1 
         Caption         =   "Editing Options"
         Height          =   852
         Left            =   360
         TabIndex        =   10
         Top             =   240
         Width           =   2772
         Begin VB.CheckBox Check1 
            Caption         =   "Tab on Enter keystroke"
            Height          =   252
            Left            =   120
            TabIndex        =   11
            Top             =   360
            Value           =   1  'Checked
            Width           =   2052
         End
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3612
      Left            =   132
      TabIndex        =   0
      Top             =   120
      Width           =   4416
      _ExtentX        =   7789
      _ExtentY        =   6371
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   420
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmOptions.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSample1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraSample1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Edit"
      TabPicture(1)   =   "frmOptions.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "chkEnterAsTab"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraSample1 
         Caption         =   "Home Page"
         Height          =   1092
         Index           =   1
         Left            =   192
         TabIndex        =   14
         Tag             =   "Sample 1"
         Top             =   456
         Width           =   4008
         Begin VB.CommandButton cmdFolderModify 
            Caption         =   "Modify..."
            Height          =   288
            Left            =   3156
            TabIndex        =   18
            Top             =   720
            Width           =   780
         End
         Begin VB.TextBox txtFolder 
            Height          =   288
            Left            =   348
            TabIndex        =   17
            Top             =   732
            Width           =   2664
         End
         Begin VB.OptionButton optHome 
            Caption         =   "Use Folder:"
            Height          =   252
            Index           =   1
            Left            =   120
            TabIndex        =   16
            Top             =   492
            Width           =   1392
         End
         Begin VB.OptionButton optHome 
            Caption         =   "Remember Last Page"
            Height          =   252
            Index           =   0
            Left            =   120
            TabIndex        =   15
            Top             =   252
            Value           =   -1  'True
            Width           =   2652
         End
      End
      Begin VB.Frame fraSample1 
         Caption         =   "Web View"
         Height          =   996
         Index           =   0
         Left            =   204
         TabIndex        =   12
         Tag             =   "Sample 1"
         Top             =   1668
         Width           =   3972
         Begin VB.OptionButton optFolders 
            Caption         =   "Enable Web content in &folders"
            Height          =   252
            Index           =   0
            Left            =   120
            TabIndex        =   1
            Top             =   252
            Value           =   -1  'True
            Width           =   2652
         End
         Begin VB.OptionButton optFolders 
            Caption         =   "Use Windows &classic folders"
            Height          =   252
            Index           =   1
            Left            =   120
            TabIndex        =   13
            Top             =   492
            Width           =   2652
         End
      End
      Begin VB.CheckBox chkEnterAsTab 
         Caption         =   "Enter key moves to next field"
         Height          =   252
         Left            =   -74760
         TabIndex        =   2
         Top             =   492
         Width           =   3252
      End
   End
End
Attribute VB_Name = "frmMainOptionsDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmdCancel_Click()
    Unload Me
End Sub


Private Sub cmdFolderModify_Click()
  Dim fFolder As New frmFolderBrowseDialog, Path As String
  optHome(1).Value = True
  
  Path = txtFolder.Text
  If fFolder.ShowSelect(, Path) Then
    txtFolder.Text = fFolder.FolderPath
  End If
  Unload fFolder
End Sub

Private Sub cmdOK_Click()
  LocaleEnterAsTab = chkEnterAsTab
  LocaleEnableWebContent = optFolders(0).Value
  
  IniMain.WriteFile "Main", "EnterAsTab", IIf(LocaleEnterAsTab, "True", "False")
  IniMain.WriteFile "Main", "EnableWebContent", IIf(LocaleEnableWebContent, "True", "False")
  
  IniForm.WriteFile "frmMain", "home", IIf(optHome(0).Value, 0, 1)
  If optHome(1) Then
    IniForm.WriteFile "frmMain", "folder", txtFolder.Text
  End If
  
  Unload Me
End Sub


Private Sub Form_Load()
  chkEnterAsTab.Value = IIf(LocaleEnterAsTab, 1, 0)
  If LocaleEnableWebContent = True Then
    optFolders(0).Value = True
  Else
    optFolders(1).Value = True
  End If
  
  optHome(GetInteger(IniForm.ReadFile("frmMain", "home"))).Value = True
  txtFolder.Text = IniForm.ReadFile("frmMain", "folder")
End Sub

