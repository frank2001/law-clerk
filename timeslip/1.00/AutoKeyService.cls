VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AutoKeyService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mClient As SoapClient


'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mClient = New SoapClient
  mClient.mssoapinit WSDLFile
  mClient.ConnectorProperty("EndPointURL") = EndPointURL
End Sub


'
' Create an application.
'
Public Function GetDocumentKey() As String
On Error GoTo ErrorHandler
  
  GetDocumentKey = mClient.GetDocumentKey()
  Exit Function
  
ErrorHandler:
  MsgBox "AutoKey Get Document Key Service Error: " & Err.Description, vbExclamation
  Err.Clear
End Function
