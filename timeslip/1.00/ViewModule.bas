Attribute VB_Name = "FolderUtil"
Option Explicit

'
' The View Module controls access to a global set of data views.
'
'

' Views for data.
Public Views As ViewItems


Public Sub TreeViewLoadFolder(Tree As TreeView)
  ' Clear tree and folders.
  Tree.Nodes.Clear
  
  Dim FolderNode As MSXML2.IXMLDOMNode
  Set FolderNode = GetFolderNode
  If FolderNode Is Nothing Then
    MsgBox "Unable to retrieve folders from middle tier"
    Exit Sub
  End If
  
  TreeViewLoadNode Tree, Nothing, FolderNode
  Tree.Nodes.Item(1).Expanded = True
End Sub

'
' Walk node and children.
'
Public Sub TreeViewLoadNode(Tree As TreeView, Parent As Node, Node As IXMLDOMNode)
  ' If no node, nothing more to load
  If Node Is Nothing Then Exit Sub
  
  ' Add child as root if first call or add to the parent.
  Dim Child As Node
  If (Parent Is Nothing) Then
    Set Child = Tree.Nodes.Add(, , _
        Node.Attributes.getNamedItem("id").Text, Node.Attributes.getNamedItem("name").Text)
  Else
    Set Child = Tree.Nodes.Add(Parent, tvwChild, _
        Node.Attributes.getNamedItem("id").Text, Node.Attributes.getNamedItem("name").Text)
  End If
  
  Child.Tag = Node.Attributes.getNamedItem("type").Text
  
  ' Load Child Nodes
  TreeViewLoadNodes Tree, Child, Node.selectNodes("folder")
End Sub

'
' Walk nodes
'
Public Sub TreeViewLoadNodes(Tree As TreeView, Parent As Node, NodeList As IXMLDOMNodeList)
  ' Walk Children.
  Dim Child As IXMLDOMNode
  For Each Child In NodeList
    TreeViewLoadNode Tree, Parent, Child
  Next
End Sub


Public Function WorkflowShow(Folder As String, ActionType As Integer, _
    Optional ParameterNames As Variant, Optional ParameterValues As Variant, _
    Optional OkCancel As Boolean) As Boolean
'  Dim WorkflowDialog As New frmWorkflowDialog
'  If Not WorkflowDialog.ShowByFolder(Folder, ActionType, OkCancel) Then
'    Unload WorkflowDialog
'    Exit Function
'  End If
'  If Not IsMissing(ParameterNames) Then
'    ParameterNames = WorkflowDialog.ParameterNames
'  End If
'  If Not IsMissing(ParameterValues) Then
'    ParameterValues = WorkflowDialog.ParameterValues
'  End If
'  Unload WorkflowDialog
'  WorkflowShow = True
End Function


Public Function GetFolderNode() As MSXML2.IXMLDOMNode
  Set GetFolderNode = XMLUtil.HttpGet(HttpServer & WebApplication & WebService & "folderopen?key=DOCUMENT&source=text/xml").responseXML.selectSingleNode("xml/data/folder")
End Function


Public Function FolderNodes(ParentHandle As String) As MSXML2.IXMLDOMNodeList
  Set FolderNodes = XMLUtil.HttpGet(HttpServer & WebApplication & WebService & "folderopen?key=" & EncodeURL(ParentHandle) & "&source=text/xml").responseXML.selectNodes("xml/data/folder")
End Function


Private Function GetControlName(Name As String) As String
  GetControlName = Mid(Name, InStr(Name, ".") + 1)
End Function


Private Function GetForm(Name As String, Popup() As Form) As Form
  Dim Size, I As Integer
  Size = UBound(Popup)
  For I = 0 To Size
    If Popup(I).Name = Name Then
      Set GetForm = Popup(I)
      Exit Function
    End If
  Next I
End Function


Private Function GetFormName(Name As String) As String
  GetFormName = "frm" & Left(Name, InStr(Name, ".") - 1)
End Function


Public Sub ViewLoad(View As View, Key As String, Popup() As Form, Source As String)
  With View
    .Key = Key
    Set .DataFinder = PlugInUtil.NewFinder(Source)
    
    Dim PopupMenuName As String
    PopupMenuName = IniFolder.ReadFile(Key, "popup")
    If PopupMenuName = vbNullString Then Exit Sub
    
    Set .PopupMenuForm = GetForm(GetFormName(PopupMenuName), Popup)
    .PopupMenuName = GetControlName(PopupMenuName)
    .DefaultMenuName = IniFolder.ReadFile(Key, "popup.default")
    .OpenMenuName = IniFolder.ReadFile(Key, "popup.open")
    .ExploreMenuName = IniFolder.ReadFile(Key, "popup.explore")
    .NewItemName = IniFolder.ReadFile(Key, "newitem")
  End With
End Sub
