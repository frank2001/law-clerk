Attribute VB_Name = "PlugInUtil"
Option Explicit

Private PlugIns As New Collection


Private Function GetPlugIn(PlugInName As String) As Object
On Error GoTo Exception
  If Contains(PlugIns, PlugInName) Then
    Set GetPlugIn = PlugIns(PlugInName)
    Exit Function
  End If
    
  Dim PlugIn As Object
  Set PlugIn = CreateObject(PlugInName)
  Set PlugIn.Session = LawSession
  PlugIns.Add PlugIn, PlugInName
  Set GetPlugIn = PlugIn
  Exit Function
  
Exception:
  Err.Clear
End Function


Public Function NewGenericFinder(Source As String) As DataFinder
  Dim mFinder As DataFinder
  Set mFinder = New GenericFinder
  Set mFinder.Session = LawSession
  mFinder.DataProvider.Source = Source
  Set NewGenericFinder = mFinder
End Function


Public Function NewFinder(PlugInName As String) As DataFinder
On Error GoTo Exception
  Dim PlugIn As Object
  Set PlugIn = GetPlugIn(PlugInName)
  If PlugIn Is Nothing Then
    Set NewFinder = NewGenericFinder(PlugInName)
    Exit Function
  End If
  
  Set NewFinder = PlugIn.NewFinder
  Exit Function
  
Exception:
  Err.Clear
End Function


Public Function NewPopupMenuForm(PlugInName As String, FormMgr As FormManager, ListView As ListView) As Form
  Dim PlugIn As Object
  Set PlugIn = GetPlugIn(PlugInName)
  If Not PlugIn Is Nothing Then
    Set NewPopupMenuForm = PlugIn.NewPopupMenuForm
    NewPopupMenuForm.Init FormMgr, ListView
  End If
End Function


Public Sub ShowNew(PlugInName As String, FormMgr As FormManager)
  Dim PlugIn As Object
  Set PlugIn = GetPlugIn(PlugInName)
  If Not PlugIn Is Nothing Then
    PlugIn.ShowNew FormMgr
  End If
  
End Sub
