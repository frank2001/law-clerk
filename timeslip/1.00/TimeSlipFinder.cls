VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TimeSlipFinder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Implements DataFinder

Private mSession As Session
Private mSQL As SQLDialect
Private mDataProvider As DataProvider
Private mWords As String
Private mListIndex As Integer


Private Property Set DataFinder_Session(ByVal vData As Session)
  Set mSession = vData
  Set mDataProvider = mSession.NewDataProvider(TimeSlipTable)
  Set mSQL = mSession.SQLDialect
End Property


Private Property Get DataFinder_Session() As Session
  Set DataFinder_Session = mSession
End Property


Private Property Get DataFinder_DataProvider() As DataProvider
  Set DataFinder_DataProvider = mDataProvider
End Property


Private Sub DataFinder_LoadSearch(Form As Object)
  Form.txtWords = mWords
  Form.cboIn.Clear
  Form.cboIn.AddItem "in project identifier field only"
  Form.cboIn.AddItem "in resource identifier field only"
  Form.cboIn.ListIndex = mListIndex
End Sub


Private Function DataFinder_FindByForm(Form As Object) As Collection
  mWords = Form.txtWords
  mListIndex = Form.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not mWords = "" Then
    Select Case mListIndex
      Case 0
        newFilter.Add mSQL.UCase("projectId") & " like '" + "%" + UCase(Replace(mWords, " ", "%")) + "%'"
      Case 1
        newFilter.Add mSQL.UCase("resourceId") & " like '" + "%" + UCase(mWords) + "%'"
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView Form.lvView
End Function
