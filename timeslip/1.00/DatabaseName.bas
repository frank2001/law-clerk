Attribute VB_Name = "DatabaseName"
Option Explicit


' Jesse - Add _TABLE to all constants.  When updating the
' DataFinder, list the FindByForm methods that have no active internal code.
' For example, RatingBookMetricFinder has no internal code.

Public Const TIMESLIP_KEY = "timeSlipId"
Public Const TIMESLIP_TABLE = "TimeSlip"

Public Const APPLICATIONLAWAREA_TABLE = "ApplicationLawArea"
Public Const APPLICATIONLAWAREA_VIEW = "ApplicationLawAreaView"

Public Const APPLICATIONLAWYER_TABLE = "ApplicationLawyer"
Public Const APPLICATIONLAWYER_VIEW = "ApplicationLawyerView"

Public Const APPLICATIONLIMIT_KEY = "applicationLimitId"
Public Const APPLICATIONLIMIT_TABLE = "ApplicationLimit"

Public Const APPLICATIONRATING_KEY = "applicationRatingId"
Public Const APPLICATIONRATING_TABLE = "ApplicationRating"
Public Const APPLICATIONRATING_REF = "ApplicationRatingRef"
Public Const APPLICATIONRATINGVIEW_TABLE = "ApplicationRatingView"

Public Const APPLICATIONTYPE_TABLE = "ApplicationType"
Public Const APPLICATIONTYPE_KEY = "applicationTypeId"

Public Const APPLICATIONQUESTION_TABLE = "ApplicationQuestion"
Public Const APPLICATIONQUESTION_VIEW = "ApplicationQuestionView"

Public Const ATTRIBUTE_TABLE = "SysAttribute"

Public Const CLAIM_KEY = "claimId"
Public Const CLAIM_REF = "ClaimReference"
Public Const CLAIM_TABLE = "Claim"
Public Const CLAIM_DOCUMENT = "ClaimDocument"

Public Const CLAIMENDORSEMENT_KEY = "claimEndorsementId"
Public Const CLAIMENDORSEMENT_TABLE = "ClaimEndorsement"
Public Const CLAIMLAWYER_TABLE = "ClaimLawyer"
Public Const CLAIMPAYMENT_KEY = "claimPaymentId"
Public Const CLAIMPAYMENT_TABLE = "ClaimPayment"

Public Const CLAIMLAWAREA_TABLE = "ClaimLawArea"
Public Const CLAIMLAWAREA_VIEW = "ClaimLawAreaView"

Public Const CLAIMLAWERROR_TABLE = "ClaimLawError"
Public Const CLAIMLAWERROR_VIEW = "ClaimLawErrorView"

Public Const CLAIMRESERVE_TABLE = "ClaimReserve"
Public Const CLAIMRESERVE_KEY = "claimReserveId"
Public Const CLEEVENT_KEY = "cleEventId"
Public Const CLEEVENT_TABLE = "CLEEvent"
Public Const CLEEVENTLAWYER_TABLE = "CLEEventLawyer"

Public Const COMMUNICATIONTYPE_KEY = "communicationTypeId"
Public Const COMMUNICATIONTYPE_TABLE = "CommunicationType"
Public Const CONTACT_KEY = "contactId"
Public Const CONTACT_TABLE = "Contact"
Public Const CONTACT_VIEW = "ContactView"
Public Const CONTACTTYPE_KEY = "contactTypeId"
Public Const CONTACTTYPE_TABLE = "ContactType"

Public Const COVERAGE_KEY = "coverageId"
Public Const COVERAGE_TABLE = "Coverage"
Public Const COVERAGE_VIEW = "CoverageView"
Public Const COVERAGE_REF = "CoverageReference"
Public Const COVERAGELAWYER_TABLE = "CoverageLawyer"
Public Const COVERAGELAWYER_VIEW = "CoverageLawyerView"

Public Const DEDUCTIBLEREIMBURSE_KEY = "deductibleReimburseId"
Public Const DEDUCTIBLEREIMBURSE_TABLE = "DeductibleReimburse"
Public Const DEDUCTIBLEREIMBURSE_REF = "DeductibleReimburseRef"

Public Const DOCUMENT_GROUP = "Document"

Public Const EFTDEPOSITFINANCIALINST_KEY = "eftDepositFinancialInstId"
Public Const EFTDEPOSITFINANCIALINST_TABLE = "EFTDepositFinancialInst"
Public Const EFTTRANSACTIONCODE_KEY = "eftTransactionCodeId"
Public Const EFTTRANSACTIONCODE_TABLE = "EFTTransactionCode"
Public Const ENDORSEMENT_REF = "EndorsementReference"
Public Const ENDORSEMENT_TABLE = "Endorsement"
Public Const ENDORSEMENT_KEY = "endorsementId"
Public Const ENDORSEMENTTYPE_KEY = "endorsementTypeId"
Public Const ENDORSEMENTTYPE_TABLE = "EndorsementType"

Public Const EVENT_KEY = "eventId"
Public Const EVENT_TABLE = "Event"
Public Const EVENTATTRIBUTE_TABLE = "EventAttribute"
Public Const EVENTVIEW_TABLE = "EventView"
Public Const EVENTTYPE_KEY = "eventTypeId"
Public Const EVENTTYPE_TABLE = "EventType"
Public Const EVENTTYPEGROUP_TABLE = "EventTypeGroup"

Public Const EVENTRECIPIENT_KEY = "eventRecipientId"
Public Const EVENTRECIPIENT_TABLE = "EventRecipient"

Public Const EVENTGROUP_KEY = "eventGroupId"

Public Const FIRM_KEY = "firmId"
Public Const FIRM_TABLE = "Firm"
Public Const FIRM_REF = "FirmReference"
Public Const FIRM_DOCUMENT = "FirmDocument"

Public Const FIRMINSURER_TABLE = "FirmInsurer"
Public Const FIRMLAWYERVIEW_TABLE = "FirmLawyerView"
Public Const FIRMLAWYER_TABLE = "FirmLawyer"
Public Const FIRM_VIEW = "FirmView"

Public Const FISCALCALENDAR_TABLE = "FiscalCalendar"

Public Const GLACCOUNT_KEY = "glAccountId"
Public Const GLACCOUNT_TABLE = "GLAccount"
Public Const GLACCOUNTGROUP_TABLE = "GLAccountGroup"

Public Const GLBATCH_KEY = "glBatchId"
Public Const GLBATCH_TABLE = "GLBatch"
Public Const GLENTRY_TABLE = "GLEntry"
Public Const GLTRANSACTION_TABLE = "GLTransaction"

Public Const INCIDENT_TABLE = "Incident"
Public Const INSTALLMENT_TABLE = "Installment"
Public Const INSTALLMENTCHILD_VIEW = "InstallmentChildView"

Public Const INSTALLMENTPOLICYPAYMENT_TABLE = "InstallmentPolicyPayment"
Public Const INSTALLMENTPOLICYPAYMENT_VIEW = "InstallmentPolicyPaymentView"



Public Const INSURER_KEY = "insurerId"
Public Const INSURER_TABLE = "Insurer"


Public Const LAWAREA_KEY = "lawAreaId"
Public Const LAWAREA_TABLE = "LawArea"
Public Const LAWDISPOSITION_KEY = "lawDispositionId"
Public Const LAWDISPOSITION_TABLE = "LawDisposition"
Public Const LAWERROR_KEY = "lawErrorId"
Public Const LAWERROR_TABLE = "LawError"
Public Const LAWYER_TABLE = "Lawyer"
Public Const LAWYER_REF = "LawyerReference"
Public Const LAWYER_KEY = "lawyerId"
Public Const LISTENER_KEY = "listenerId"
Public Const LISTENER_TABLE = "SysListener"

Public Const LOCATION_KEY = "locationId"
Public Const LOCATION_TABLE = "Location"
Public Const LOCATION_REF = "LocationReference"
Public Const LOCATIONTYPE_KEY = "locationTypeId"
Public Const LOCATIONTYPE_TABLE = "LocationType"

Public Const PARAMETER_KEY = "parameterId"

Public Const PERSON_KEY = "personId"
Public Const PERSON_TABLE = "Person"
Public Const PERSON_REF = "PersonReference"
Public Const PERSONATTRIBUTE_TABLE = "PersonAttribute"
Public Const PERSONATTRIBUTE_VIEW = "PersonAttributeView"

Public Const POLICY_KEY = "policyId"
Public Const POLICY_TABLE = "Policy"
Public Const POLICY_REF = "PolicyReference"
Public Const POLICYLAWYER_TABLE = "PolicyLawyer"
Public Const POLICYLAWYERCOVERAGEVIEW_TABLE = "PolicyLawyerCoverageView"
Public Const POLICYACCOUNTING_KEY = "policyAccountingId"
Public Const POLICYACCOUNTING_TABLE = "PolicyAccounting"
Public Const PUBLISHER_KEY = "publisherId"

Public Const RATING_TABLE = "Rating"
Public Const RATINGBOOK_TABLE = "RatingBook"
Public Const RATINGBOOK_KEY = "ratingBookId"
Public Const RATINGBOOKMETRIC_TABLE = "RatingBookMetric"
Public Const RATINGBOOKMETRICCALC_TABLE = "RatingBookMetricCalc"
Public Const RATINGBOOKRATE_TABLE = "RatingBookRate"
Public Const RATINGLAWYER_TABLE = "RatingLawyer"
Public Const RATINGLIMIT_TABLE = "RatingLimit"
Public Const RATINGMETRIC_TABLE = "RatingMetric"
Public Const RATINGMETRIC_KEY = "ratingMetricId"
Public Const RATINGMETRICIDCALC_KEY = "ratingMetricIdCalc"
Public Const RATINGRATE_TABLE = "RatingRate"

Public Const REFERENCE_KEY = "referenceId"
Public Const REFERENCE_TABLE = "Reference"
Public Const REFERENCEVIEW_TABLE = "ReferenceView"
Public Const REINSURANCETREATY_KEY = "reinsuranceTreatyId"
Public Const REINSURANCETREATY_TABLE = "ReinsuranceTreaty"

Public Const FOLDER_KEY = "folderId"
Public Const FOLDER_TABLE = "SysFolder"

Public Const SYSFOLDERACTIONPARAM_TABLE = "SysFolderActionParam"

Public Const SYSGROUPACTIONPARAM_TABLE = "SysGroupActionParam"

Public Const SYSLISTENER_TABLE = "SysListener"
Public Const SYSLISTENERACTIONPARAM_TABLE = "SysListenerActionParam"
Public Const SYSLISTENERPARAMETER_TABLE = "SysListenerParameter"

Public Const SYSPARAMETER_TABLE = "SysParameter"
Public Const SYSPUBLISHER_TABLE = "SysPublisher"
Public Const SYSPUBLISHERLISTENER_TABLE = "SysPublisherListener"

Public Const QUESTION_KEY = "questionId"
Public Const QUESTION_TABLE = "Question"


