VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PrintListView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'==============================================================================
'Prints a ListView.
'Code has been inspired by Marco Losavio, but has been modified and improved a
'lot.
'
'Scope:     DLL-Public creatable.
'==============================================================================

Option Explicit

Event BeforeNewPage()

Private m_Control As ListView
Private m_PageLayout As PrintPageLayout
Private m_Title As PrintText
Private m_PrintOnlySelectedItems As Boolean
Private m_PrintedItems As Long
Private m_HeaderFont As StdFont
Private m_FontItem As StdFont

Public Property Get HeaderFont() As StdFont
    Set HeaderFont = m_HeaderFont
End Property

Public Property Get PrintedItems() As Long
    PrintedItems = m_PrintedItems
End Property

Public Property Get PrintOnlySelectedItems() As Boolean
    PrintOnlySelectedItems = m_PrintOnlySelectedItems
End Property
Public Property Let PrintOnlySelectedItems(ByVal nv As Boolean)
    m_PrintOnlySelectedItems = nv
End Property

Public Property Get Title() As PrintText
    Set Title = m_Title
End Property

Public Property Set Title(Title As PrintText)
    Set m_Title = Title
End Property

Public Property Get PageLayout() As PrintPageLayout
    Set PageLayout = m_PageLayout
End Property

Public Property Set PageLayout(PageLayout As PrintPageLayout)
    Set m_PageLayout = PageLayout
End Property

Public Property Get Control() As ListView
    Set Control = m_Control
End Property

Public Property Set Control(nv As ListView)
    Set m_Control = nv
    With m_FontItem
        .Name = nv.Font.Name
        .Size = nv.Font.Size
        .Bold = nv.Font.Bold
        .Italic = nv.Font.Italic
        .Strikethrough = nv.Font.Strikethrough
        .Underline = nv.Font.Underline
    End With
    With m_HeaderFont
        .Name = nv.Font.Name
        .Size = nv.Font.Size
        .Bold = nv.Font.Bold
        .Italic = nv.Font.Italic
        .Strikethrough = nv.Font.Strikethrough
        .Underline = nv.Font.Underline
    End With
    
End Property


Private Sub Class_Initialize()
    Set m_Title = New PrintText
    Set m_HeaderFont = New StdFont
    Set m_FontItem = New StdFont
End Sub

Private Sub Class_Terminate()
    Set m_Title = Nothing
    Set m_Control = Nothing
    Set m_HeaderFont = Nothing
    Set m_FontItem = Nothing
End Sub


' The FontItem property

Public Property Get FontItem() As StdFont
    Set FontItem = m_FontItem
End Property

Public Property Set FontItem(Font As StdFont)
    Set m_FontItem = Font
End Property


Public Function PrintControl(ByVal InColor As Boolean, ByVal Mark As Boolean) As Boolean
Dim itmX As ListItem, I As Long

'    On Error GoTo ErrPrintLV
    SetPage m_PageLayout
    
    Printer.Print " "
    
    RaiseEvent BeforeNewPage
    Printer.CurrentY = m_PageLayout.TopMargin * vbCM - Printer.TextHeight(m_PageLayout.Header.Text)
    
    PrintPrintText m_PageLayout.Header, _
               m_PageLayout.LeftMargin * vbCM, _
               m_PageLayout.RightMargin * vbCM
                
    Printer.CurrentX = m_PageLayout.LeftMargin * vbCM
    Printer.CurrentY = m_PageLayout.TopMargin * vbCM + IIf(m_PageLayout.Header.Text <> "", 20 * Screen.TwipsPerPixelY, 0)
    
        
    
    If m_Title.Text <> "" Then
        PrintPrintText m_Title, m_PageLayout.LeftMargin * vbCM, m_PageLayout.RightMargin * vbCM
        
        Printer.CurrentY = Printer.CurrentY + 20 * Screen.TwipsPerPixelY
    End If
    
    SetPrinterFont m_HeaderFont
    PrintHeaderLV m_Control, m_PageLayout.LeftMargin * vbCM  ', m_PageLayout.RightMargin * vbCM
    SetPrinterFont m_FontItem
    m_PrintedItems = 0
    For I = 1 To m_Control.ListItems.Count
        Set itmX = m_Control.ListItems(I)
        If (m_PrintOnlySelectedItems And itmX.Selected) Or _
            (Not m_PrintOnlySelectedItems) Then
            If Not CanPrintItem(Printer.TextHeight(itmX.Text) + 2 * Printer.TwipsPerPixelY, _
                                m_PageLayout.BottomMargin * vbCM + IIf(m_PageLayout.Footer.Text <> "", 20 * Screen.TwipsPerPixelY, 0)) Then
                
                Printer.CurrentY = Printer.ScaleHeight - m_PageLayout.BottomMargin * vbCM
                
                PrintPrintText m_PageLayout.Footer, _
                            m_PageLayout.LeftMargin * vbCM, _
                            m_PageLayout.RightMargin * vbCM
                            
                RaiseEvent BeforeNewPage
                Printer.NewPage
                
                Printer.CurrentY = m_PageLayout.TopMargin * vbCM - Printer.TextHeight(m_PageLayout.Header.Text)
                
                PrintPrintText m_PageLayout.Header, _
                            m_PageLayout.LeftMargin * vbCM, _
                            m_PageLayout.RightMargin * vbCM
                
                SetPrinterFont m_HeaderFont
                
                Printer.CurrentY = m_PageLayout.TopMargin * vbCM + IIf(m_PageLayout.Header.Text <> "", 20 * Screen.TwipsPerPixelY, 0)
                
                PrintHeaderLV m_Control, _
                                     m_PageLayout.LeftMargin * vbCM ', _
                                     m_PageLayout.RightMargin * vbCM
                                     
                SetPrinterFont m_FontItem
                        
            End If
            
            PrintItemLV itmX, m_Control, _
                         m_PageLayout.LeftMargin * vbCM, InColor, Mark
                         
            m_PrintedItems = m_PrintedItems + 1
        End If
    Next I
    
    Printer.CurrentY = Printer.ScaleHeight - m_PageLayout.BottomMargin * vbCM
    
    PrintPrintText m_PageLayout.Footer, _
                m_PageLayout.LeftMargin * vbCM, _
                m_PageLayout.RightMargin * vbCM
                
    Printer.EndDoc
    PrintControl = True
    
    Exit Function
    
ErrPrintLV:
    Printer.KillDoc
    Beep
    MsgBox Err.Number & " " & Err.Description, vbExclamation
End Function

