Attribute VB_Name = "MainModule"
'
'Claim Law Area

' Main Module
'

' The main module controls the loading of the project.  The
' main form is frmMain.  This form is one of three major types.
' See the frmMain comments for more information.

' See:  Sub Main() for more information on the loading of the project and
' configuration of project for testing.


' References

' (Ref:Add Detail)
' Search the project for this reference to see all the places
' code was changed to add a table in a tab (Detail Table) to
' an "Open" form.


'
' Misc. VB Questions (Anyone know?)
'
' -How do you pass a control array to a method.
' -How do you tell if a form has been shown modally.


'
' Add back to main project --
'
' InitialContext
' form.ini - Save full path for ini.
'
'

Option Explicit

'Factory for loading middle tier services.
Public Factory As ServiceFactory

'Session information set by login.
Public HttpServer As String
Public WebApplication As String
Public WebService As String
Public LawSession As Session
Public LawSQL As SQLDialect


'//Store value of ini file
Public IniMain As New IniFile
Public IniForm As New IniFile
Public IniFolder As New IniFile
Public IniSearch As New IniFile

'Private IniViewPath As String
Private ConfDefaultPath As String
Private ConfPersonalPath As String


Public Const GW_HWNDPREV = 3

Declare Function OpenIcon Lib "user32" (ByVal hWnd As Long) As Long
Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
        (ByVal lpClassName As String, ByVal lpWindowName As String) _
         As Long
Declare Function GetWindow Lib "user32" _
        (ByVal hWnd As Long, ByVal wCmd As Long) As Long
Declare Function SetForegroundWindow Lib "user32" _
        (ByVal hWnd As Long) As Long

Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

' Global constants for the frmMain form types.
Global Const ftView = 1
Global Const ftAdvancedSearch = 2
Global Const ftSelect = 3

' Constants for form interface messages
Global Const fimNone = 0
Global Const fimSelect = 1
Global Const fimNew = 2
Global Const fimSave = 3

' Constants for Null's
Public vbNullDate As Date
Public vbNullStringArray() As String

' Support cut/paste throughout system
Public EventBufferFolder As String
Public EventBuffer As Collection

Public LocaleShortDate As String
Public LocaleLongDate As String
Public LocaleTimeFormat As String

Public LocaleEnterAsTab As Boolean
Public LocaleEnableWebContent As Boolean

Public LocaleShortOrganization As String
Public LocaleLongOrganization As String

'Public picDisplay As New PictureDisplay
Public fSplash As frmSplash

Public InitialContext As String
Public RootFolderName As String


'***************************
' Purpose:  Load the project
'***************************

Sub Main()
  If App.PrevInstance Then
    ActivatePrevInstance
  End If
  
  IniMain.FileName = App.Path & "\" & "law.ini"
  
  'IniViewPath = GetRealPath(IniMain.ReadFile("Main", "IniViewPath"))
  ConfDefaultPath = GetRealPath(IniMain.ReadFile("Main", "ConfDefaultPath"))
  ConfPersonalPath = GetRealPath(IniMain.ReadFile("Main", "ConfPersonalPath"))
  
  ' Setup ini files.
  IniForm.FileName = GetRealPath(IniMain.ReadFile("Main", "IniForm"))
  IniFolder.FileName = GetRealPath(IniMain.ReadFile("Main", "IniFolder"))
  IniSearch.FileName = GetRealPath(ConfPersonalPath + "\search.ini")
  
  LocaleShortDate = GetUserLocaleInfo(GetSystemDefaultLCID, LOCALE_SSHORTDATE)
  LocaleLongDate = GetUserLocaleInfo(GetSystemDefaultLCID, LOCALE_SLONGDATE)
  LocaleTimeFormat = GetUserLocaleInfo(GetSystemDefaultLCID, LOCALE_STIMEFORMAT)
  
  ' Convert time format to vb spec.
  If InStr(LocaleTimeFormat, "tt") Then
    LocaleTimeFormat = Left(LocaleTimeFormat, Len(LocaleTimeFormat) - 2) & "AMPM"
  End If
  
  LocaleEnterAsTab = IniMain.ReadFile("Main", "EnterAsTab") = "True"
  LocaleEnableWebContent = IniMain.ReadFile("Main", "EnableWebContent")
  LocaleShortOrganization = IniMain.ReadFile("Main", "OrganizationShort")
  LocaleLongOrganization = IniMain.ReadFile("Main", "OrganizationLong")
  
  Set Factory = New ServiceFactory
  
   ' ***********************************************
   ' Uncomment the appropriate session.
   
   'MemorySession is used for testing.
   'Set LawSession = New MemorySession
   
   'ADOSession connects directly to Oracle via ADO/ODBC
   'Set LawSession = New ADOSession
   
   'XMLSession connects to middle tier.
   Set LawSession = New XMLSession
   
   
   InitialContext = "Timeslip"
   RootFolderName = "Desktop"
   
      ' ***********************************************
   
   
  ' **********************************************
  ' Uncomment a form to load
   
  Set fSplash = New frmSplash
  fSplash.Show
  Login
  fSplash.Refresh
  
  ' Load up main form
  Dim fMain As New frmMain, FolderPath As String
  Load fMain
  FolderPath = IniForm.ReadFile(fMain.Name, "folder")
  fMain.SelectByPath FolderPath
'  fMain.FolderKey = FolderId
'  If Not fMain.tvFolders.SelectedItem Is Nothing Then
'    fMain.tvFolders.SelectedItem.Expanded = True
'  End If
  fMain.Show
  
  'PersonServiceTest
  'ApplicationServiceTest
  
'   Dim fForm As New frmEventDialog
'   fForm.ReferenceLoad DatabaseName.APPLICATION_TABLE, "AP10003826", "Complete"
'   fForm.Show vbModal
'   Unload fForm

'    Dim fForm As New frmClaimPaymentEdit
'    fForm.ShowNew "00-0003"


'   CreateSearchForm().Show

   
'   Dim fForm As New frmApplicationLimitEdit
'   fForm.ShowNew "TEST"
'   fForm.Show
   
   'Dim Complete As New frmCompleteEvent
   'Complete.FolderId = "Underwriting_Applications"
   'Complete.Show
   
   'frmFolderDialog.Show
   
   'frmApplicationCreate.Show
   'frmFront.Show
   'frmXMLListView.Show
   'frmColumnSelect.Show
   'frmGLTransactionEdit.Show
   'CreateMainForm(ftSelect, "Lawyer").Show
   'frmLawyer.Show
   'CreateMainForm(ftAdvancedSearch, "Firm").Show
   'frmUnderwritingContact.Show
   'frmFirmInsurerEdit.Show
   
   'frmApplicationOpen.Show
   'frmApplicationLimitEdit.Show
   'frmApplicationLawAreaEdit.Show
End Sub


' Return the remote path for the full path specified.
Public Function RemotePath(FullPath As String) As String
  RemotePath = Replace(FullPath, RootFolderName, InitialContext)
End Function

' Return the remote path for the full path specified.
Public Function LocalPath(FullPath As String) As String
  LocalPath = Replace(FullPath, RootFolderName & "/", "")
End Function

Public Sub AddParameter(Name As String, Value As String, ParameterNames() As String, ParameterValues() As String)
  Dim Count As Integer
  Count = SafeUBound(ParameterNames) + 1
  ReDim Preserve ParameterNames(Count)
  ReDim Preserve ParameterValues(Count)
  ParameterNames(Count) = Name
  ParameterValues(Count) = Value
End Sub

Public Sub MaskEditClear(Control As MaskEdBox)
   Control.SelStart = 0
   Control.SelLength = 9999
   Control.SelText = ""
End Sub

Public Sub ClearDataField(DataFields As Variant, Optional DataMember As String)
  Dim Control As Variant
  For Each Control In DataFields
    If DataMember = vbNullString Or Control.DataMember = DataMember Then
      If TypeOf Control Is TextBox Then
        Control.Text = vbNullString
      ElseIf TypeOf Control Is Label Then
        Control.Caption = vbNullString
      ElseIf TypeOf Control Is MaskEdBox Then
        Dim msk As MaskEdBox
        Set msk = Control
        MaskEditClear msk
      ElseIf TypeOf Control Is ComboBox Then
        Dim cbx As ComboBox
        Set cbx = Control
        With cbx
          If .ListCount > 0 Then
            .ListIndex = 0
          End If
        End With
      ElseIf TypeOf Control Is DTPicker Then
        Dim dtpDate As DTPicker
        Set dtpDate = Control
        If dtpDate.CheckBox Then
          dtpDate.Value = Now()
          dtpDate.Value = vbNull
        Else
          dtpDate.Value = vbNull
          dtpDate.Value = Now()
        End If
      End If
    End If
  Next
End Sub


Public Function CollectionItem(Collection As Variant, Index As String) As Variant
  On Error Resume Next
  If VarType(Collection) = vbEmpty Then
  ElseIf TypeOf Collection Is ListItem Then
    Dim ListItem As ListItem
    Set ListItem = Collection
    CollectionItem = ListItemText(ListItem, Index)
  Else
    CollectionItem = Collection(Index)
  End If
  Err.Clear
End Function

Public Sub SetFields(Controls As Variant, Optional Items As Variant, Optional DataMember As String)
  Dim Item As Variant
  Dim Control As Variant
  For Each Control In Controls
    If ((DataMember <> "" And Control.DataMember <> DataMember) Or Control.DataField = "") Then
      GoTo Continue
    End If
    
    Item = CollectionItem(Items, Control.DataField)
      
    If TypeOf Control Is Label Then
      Control.Caption = DisplayFormat(Control.DataField, CStr(Item), Control.DataFormat.Format, LocaleShortDate)
    
    ElseIf TypeOf Control Is TextBox Then
      Control.Text = Format(Item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is DTPicker Then
      If Item = vbNullString Then
        Control.CheckBox = True
      End If
      Control.Value = Format(Item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is ComboBox Then
      Dim cbx As ComboBox
      Set cbx = Control
      With cbx
        If Item = vbNullString Then
          If .ListCount > 0 Then
            .ListIndex = 0
          End If
        Else
          Dim Text As String
          Text = Format(Item, .DataFormat.Format)
          If .Style = ComboBoxConstants.vbComboDropdownList Then
            Dim I As Integer
            For I = .ListCount - 1 To 0 Step -1
              If .List(I) = Text Then
                .ListIndex = I
                Exit For
              End If
            Next I
          Else
            .Text = Text
          End If
        End If
      End With
    
    ElseIf TypeOf Control Is MaskEdBox Then
      Control.SelStart = 0
      Control.SelLength = 9999
      Control.SelText = Format(Item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is CheckBox Then
      Control.Value = IIf(Item = "Yes" Or Item = 1, 1, 0)
  
    End If
    
Continue:
  Next
End Sub

Public Sub AddMaskControl(MaskControl As Object, Items As Collection)
  Dim Control As MaskEdBox, Text As String
  For Each Control In MaskControl
    If (Not Control.DataField = "") Then
      If Control.ClipMode = mskIncludeLiterals Then
        Text = Control.Text
      Else
        Text = Control.ClipText
      End If
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Text), Control.DataField
    End If
  Next
End Sub

Public Sub AddTextControl(TextControl As Variant, Items As Collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In TextControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Text), Control.DataField
    End If
  Next
End Sub

Public Sub AddValueControl(ValueControl As Variant, Items As Collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In ValueControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Value), Control.DataField
    End If
  Next
End Sub

Public Sub AddCaptionControl(CaptionControl As Variant, Items As Collection)
  Dim Control As Variant
  For Each Control In CaptionControl
    If (Not Control.DataField = "") Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Caption), Control.DataField
    End If
  Next
End Sub

Public Sub ReadValueControl(TextControl As Variant, Items As Collection)
  Dim Control As Variant
  For Each Control In TextControl
    If (Not Control.DataField = "") Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Value), Control.DataField
    End If
  Next
End Sub


Public Function ListKey(cData As Collection) As String
  Dim List As String
  Dim Item As Variant
  For Each Item In cData
    If (List = "") Then
      List = Item
    Else
      List = List + ", " + Item
    End If
  Next
End Function

Public Function CreateDataField(Source As String, Name As String, Value As Variant) As DataField
  Set CreateDataField = New DataField
  CreateDataField.Source = Source
  CreateDataField.Name = Name
  CreateDataField.Value = Value
End Function

Private Sub ActivatePrevInstance()
  Dim OldTitle As String
  Dim PrevHndl As Long
  Dim Result As Long

  'Save the title of the application.
  OldTitle = App.Title

  'Rename the title of this application so FindWindow
  'will not find this application instance.
  App.Title = "unwanted instance"

  'Attempt to get window handle using VB4 class name.
  PrevHndl = FindWindow("ThunderRTMain", OldTitle)

  'Check for no success.
  If PrevHndl = 0 Then
     'Attempt to get window handle using VB5 class name.
     PrevHndl = FindWindow("ThunderRT5Main", OldTitle)
  End If

  'Check if found
  If PrevHndl = 0 Then
    'Attempt to get window handle using VB6 class name
    PrevHndl = FindWindow("ThunderRT6Main", OldTitle)
  End If

  'Check if found
  If PrevHndl = 0 Then
     'No previous instance found.
     Exit Sub
  End If

  'Get handle to previous window.
  PrevHndl = GetWindow(PrevHndl, GW_HWNDPREV)

  'Restore the program.
  Result = OpenIcon(PrevHndl)

  'Activate the application.
  Result = SetForegroundWindow(PrevHndl)

  'End the application.
  End
  
End Sub

 
Public Function HttpBody(Item As ListItem, Columns As Collection) As String
  Dim Column As Variant
  HttpBody = ""
  For Each Column In Columns
    HttpBody = IIf(HttpBody = "", "", "&") & Column.Name & "=" & EncodeURL(Item.ListSubItems(Column.Value))
  Next Column
End Function


Public Sub Login()
  Dim fLogin As New frmLoginDialog
  Dim Authenticated As Boolean
   
  While Not Authenticated
    fLogin.Show vbModal
    If Not fLogin.OK Then
        'Login Failed so exit app
       End
    End If
    Authenticated = LawSession.Authenticate(fLogin)
  Wend
   
  ' Generic now works with {fn xxx()} calls.
  Set LawSQL = New SQLDialect
  
  ' Set web info
  HttpServer = fLogin.cboServer
  WebApplication = "malcolm/"
  WebService = "servlet/"

  Unload fLogin
   
   'MsgBox "Authenticated with session: " & LawSession.Session
End Sub


' Will be obsolete
Public Function IniViewFileNameOld(Source As String) As String
  Dim Ini As New IniFile
  
  Ini.FileName = ConfDefaultPath & "\view.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "view"
    Exit Function
  End If
  
  Ini.FileName = ConfDefaultPath & "\viewclaim.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewclaim"
    Exit Function
  End If
  
  Ini.FileName = ConfDefaultPath & "\viewevent.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewevent"
    Exit Function
  End If
  
  Ini.FileName = ConfDefaultPath & "\viewcontact.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewcontact"
    Exit Function
  End If
  
  Ini.FileName = ConfDefaultPath & "\viewrating.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewrating"
    Exit Function
  End If
  
  Ini.FileName = ConfDefaultPath & "\viewsys.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewsys"
    Exit Function
  End If
  
  IniViewFileNameOld = "viewevent"
End Function


'
' Map a source to a file name.  Need to partition because of 64K limit.
'
Public Function FolderIniFileActive(FolderName As String) As String
  Dim FileName As String
  
  FileName = ConfPersonalPath & "\" & FolderName & ".ini"
  If FileUtil.IsFile(FileName) Then
    FolderIniFileActive = FileName
    Exit Function
  End If
  
  FileName = ConfDefaultPath & "\" & FolderName & ".ini"
  If FileUtil.IsFile(FileName) Then
    FolderIniFileActive = FileName
    Exit Function
  End If
  
  ' Retrieve old file name setting.
  FolderIniFileActive = ConfDefaultPath & "\" & IniViewFileNameOld(FolderName) & ".ini"
End Function


Public Function FolderIniFile(FolderName As String, Optional Personal As Boolean) As String
  If Personal Then
    FolderIniFile = ConfPersonalPath & "\" & FolderName & ".ini"
    Exit Function
  End If
  
  FolderIniFile = ConfDefaultPath & "\" & FolderName & ".ini"
End Function
