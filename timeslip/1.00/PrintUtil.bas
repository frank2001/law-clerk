Attribute VB_Name = "PrintUtil"
Option Explicit

Public Sub PrintDialog(formOwner As Form, LV As ListView)
  Dim PrintFlags As Long
  PrintFlags = IIf(SelectedCount(LV.hWnd) = 0, cdlPDNoSelection, 0)
  If ShowPrinter(formOwner, PrintFlags) Then
    PrintListView LV, PrintFlags And cdlPDSelection
  End If
End Sub
  
  
Public Sub PrintSetupDialog(formOwner As Form)
  Dim PrintFlags As Long
  PrintFlags = cdlPDPrintSetup
  ShowPrinter formOwner, PrintFlags
End Sub


Public Sub PrintListView(View As ListView, OnlySelected As Boolean)
  Dim Pointer As New WaitPointer
  Pointer.Pointer = vbHourglass
    
  Dim PrintEx As New PrintExportEngine
  PrintEx.ListView = View
  PrintEx.OnlySelected = OnlySelected
  PrintEx.PrintListView
  Set PrintEx = Nothing
End Sub
