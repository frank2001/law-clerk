VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "NamingService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mClient As SoapClient
Private Const ServiceName = "NamingService"
Private mDataProvider As DataProvider

'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mClient = New SoapClient
  mClient.mssoapinit WSDLFile
  mClient.ConnectorProperty("EndPointURL") = EndPointURL
  
  Set mDataProvider = Session.NewDataProvider(DatabaseName.FOLDER_TABLE)
End Sub


'
' Default finder for single key tables.
'

Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataProvider.Find(DatabaseName.FOLDER_KEY, Handle, Column)
End Function

'
' Interface to server
'



'
' CreateSubcontext.
'
Public Function CreateSubcontext(Context As String, Name As String) As Boolean
    
On Error GoTo Exception
  
  Call mClient.CreateSubcontext(Context, Name)
  CreateSubcontext = True
  Exit Function
  
Exception:
  MsgBox "Naming Service Error: " & Err.Description, vbExclamation
End Function



'
' Move Folder
'
Public Function Rename(Context As String, OldName As String, NewName As String) As Boolean
    
On Error GoTo Exception
  
  Call mClient.Rename(Context, OldName, NewName)
  Rename = True
  Exit Function
  
Exception:
  MsgBox "Naming Service Error: " & Err.Description, vbExclamation
End Function



'
' List contexts.
'
Public Function ListContexts(Context As String, Name As String) As String()
    
On Error GoTo Exception
  
  ListContexts = mClient.ListContexts(Context, Name)
  Exit Function
  
Exception:
  MsgBox "Naming Service List Contexts Error: " & Err.Description, vbExclamation
End Function


'
' Delete an Folder
'
Public Function DestroySubcontext(Context As String, Name As String) As Boolean
    
On Error GoTo Exception

  Call mClient.DestroySubcontext(Context, Name)
  DestroySubcontext = True
  Exit Function
  
Exception:
  MsgBox "Naming Service Destroy Subcontext Error: " & Err.Description, vbExclamation
End Function


'
' Service Helpers
'

'
' Add a new workflow folder
'
Public Function Add(Tree As TreeView) As Node
  
  If Not CreateSubcontext(Tree.SelectedItem.FullPath, "New Folder") Then
    Exit Function
  End If
  
  Dim nodeX As Node
  Set nodeX = Tree.Nodes.Add(Tree.SelectedItem, tvwChild, , "New Folder")
  nodeX.Selected = True
  Set Add = nodeX
End Function

'
' Remove the selected folder.
'
Public Sub Remove(Tree As TreeView)
  If Not MsgBox("Are you sure you want to remove the folder '" & Tree.SelectedItem.FullPath & "'?", vbYesNo + vbQuestion, "Confirm Folder Delete") = vbYes Then
    Exit Sub
  End If
  If DestroySubcontext("", Tree.SelectedItem.FullPath) Then
    Tree.Nodes.Remove Tree.SelectedItem.Index
  End If
End Sub


