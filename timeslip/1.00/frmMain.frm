VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Begin VB.Form frmMain 
   Caption         =   "LAW Explorer"
   ClientHeight    =   7092
   ClientLeft      =   156
   ClientTop       =   696
   ClientWidth     =   9492
   Icon            =   "frmMain.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7092
   ScaleWidth      =   9492
   StartUpPosition =   3  'Windows Default
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   9492
      _ExtentX        =   16743
      _ExtentY        =   593
      _CBWidth        =   9492
      _CBHeight       =   336
      _Version        =   "6.7.8988"
      Child1          =   "picStart"
      MinHeight1      =   288
      Width1          =   1500
      NewRow1         =   0   'False
      Child2          =   "picToolBar"
      MinWidth2       =   984
      MinHeight2      =   264
      Width2          =   7176
      NewRow2         =   0   'False
      MinHeight3      =   288
      NewRow3         =   0   'False
      Begin VB.PictureBox picStart 
         BorderStyle     =   0  'None
         Height          =   288
         Left            =   132
         ScaleHeight     =   288
         ScaleWidth      =   1344
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   24
         Width           =   1344
         Begin MSComctlLib.Toolbar tbrStart 
            Height          =   264
            Left            =   0
            TabIndex        =   30
            Top             =   24
            Width           =   1128
            _ExtentX        =   1990
            _ExtentY        =   466
            ButtonWidth     =   1143
            ButtonHeight    =   466
            Style           =   1
            TextAlignment   =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Start"
                  Object.ToolTipText     =   "Click here to begin"
                  ImageIndex      =   10
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picToolBar 
         BorderStyle     =   0  'None
         Height          =   264
         Left            =   1656
         ScaleHeight     =   264
         ScaleWidth      =   7632
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   36
         Width           =   7632
         Begin MSComctlLib.Toolbar tbToolBar 
            Height          =   264
            Left            =   0
            TabIndex        =   31
            Top             =   0
            Width           =   8676
            _ExtentX        =   15304
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Wrappable       =   0   'False
            Style           =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   13
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "New"
                  Object.ToolTipText     =   "New"
                  ImageKey        =   "New"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Open"
                  Object.ToolTipText     =   "Open"
                  ImageKey        =   "Open"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Print"
                  Object.ToolTipText     =   "Print"
                  ImageKey        =   "Print"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Move"
                  Object.ToolTipText     =   "Move to Folder"
                  ImageKey        =   "Copy"
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Delete"
                  Object.ToolTipText     =   "Delete"
                  ImageKey        =   "Delete"
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode0"
                  Object.ToolTipText     =   "View Large Icons"
                  ImageKey        =   "View Large Icons"
                  Style           =   2
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode1"
                  Object.ToolTipText     =   "View Small Icons"
                  ImageKey        =   "View Small Icons"
                  Style           =   2
               EndProperty
               BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode2"
                  Object.ToolTipText     =   "View List"
                  ImageKey        =   "View List"
                  Style           =   2
               EndProperty
               BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode3"
                  Object.ToolTipText     =   "View Details"
                  ImageKey        =   "View Details"
                  Style           =   2
               EndProperty
               BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdDefault 
      Caption         =   "Default"
      Height          =   336
      Left            =   1800
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   4236
      Width           =   1560
   End
   Begin VB.CommandButton cmdMenu 
      Caption         =   "Alt &M"
      CausesValidation=   0   'False
      Height          =   336
      Left            =   216
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   4236
      Width           =   1500
   End
   Begin VB.PictureBox picSplitter 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      FillColor       =   &H00808080&
      Height          =   4800
      Left            =   10872
      ScaleHeight     =   2084.849
      ScaleMode       =   0  'User
      ScaleWidth      =   312
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   156
      Visible         =   0   'False
      Width           =   48
   End
   Begin VB.PictureBox picListView 
      BackColor       =   &H80000005&
      Enabled         =   0   'False
      HasDC           =   0   'False
      Height          =   1392
      Left            =   12
      ScaleHeight     =   1344
      ScaleWidth      =   9456
      TabIndex        =   16
      TabStop         =   0   'False
      Tag             =   "Firm"
      Top             =   2280
      Visible         =   0   'False
      Width           =   9504
      Begin VB.PictureBox picSearchStatus 
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         Height          =   240
         Left            =   1920
         ScaleHeight     =   240
         ScaleWidth      =   4992
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   420
         Visible         =   0   'False
         Width           =   4992
         Begin VB.Label lblSearchStatus 
            AutoSize        =   -1  'True
            BackColor       =   &H80000009&
            BackStyle       =   0  'Transparent
            Caption         =   "Search Status"
            Height          =   192
            Left            =   0
            TabIndex        =   22
            Top             =   0
            Width           =   996
         End
      End
      Begin MSComctlLib.ListView lvView 
         Height          =   4152
         Left            =   1416
         TabIndex        =   17
         Top             =   0
         Width           =   5856
         _ExtentX        =   10329
         _ExtentY        =   7324
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         _Version        =   393217
         Icons           =   "imlIcons"
         SmallIcons      =   "imlSmallIcons"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.Label lblListView 
         BackColor       =   &H80000005&
         Caption         =   "Double click or press Enter on an item to view or edit the information. "
         Height          =   1572
         Index           =   1
         Left            =   84
         TabIndex        =   19
         Top             =   720
         Width           =   1212
      End
      Begin VB.Label lblListView 
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "Table Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   84
         TabIndex        =   18
         Top             =   0
         Width           =   1524
         WordWrap        =   -1  'True
      End
   End
   Begin VB.PictureBox picView 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2004
      Left            =   0
      ScaleHeight     =   2004
      ScaleWidth      =   9492
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   336
      Width           =   9495
      Begin VB.PictureBox picMain 
         BackColor       =   &H80000005&
         Height          =   4560
         Left            =   3144
         ScaleHeight     =   4512
         ScaleWidth      =   7524
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "WILMIC"
         Top             =   1140
         Visible         =   0   'False
         Width           =   7572
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Press Alt-M for the Start Menu or click the start button in the upper left hand corner."
            Height          =   384
            Index           =   3
            Left            =   504
            TabIndex        =   23
            Top             =   1260
            Width           =   4428
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblWelcolme 
            BackColor       =   &H80000005&
            Caption         =   "Welcome to the WILMIC implementation of the LAW System."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   852
            Left            =   216
            TabIndex        =   15
            Top             =   276
            Width           =   2892
         End
         Begin VB.Label lblVersion 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Version: 1.75.0318"
            Height          =   192
            Left            =   5316
            TabIndex        =   14
            Top             =   276
            Width           =   1284
         End
      End
      Begin VB.Frame fraSearch 
         Enabled         =   0   'False
         Height          =   1260
         Left            =   2172
         TabIndex        =   10
         Top             =   0
         Visible         =   0   'False
         Width           =   7812
         Begin VB.PictureBox picWords 
            BackColor       =   &H80000005&
            Height          =   900
            Left            =   0
            ScaleHeight     =   852
            ScaleWidth      =   7524
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   360
            Width           =   7572
            Begin VB.Frame fraLookFor 
               BackColor       =   &H80000005&
               BorderStyle     =   0  'None
               Height          =   684
               Left            =   0
               TabIndex        =   33
               Top             =   0
               Visible         =   0   'False
               Width           =   4116
               Begin VB.ComboBox txtWords 
                  Height          =   288
                  Left            =   1500
                  TabIndex        =   1
                  Text            =   "Combo1"
                  Top             =   0
                  Width           =   2604
               End
               Begin VB.ComboBox cboIn 
                  Height          =   288
                  ItemData        =   "frmMain.frx":000C
                  Left            =   1500
                  List            =   "frmMain.frx":000E
                  Style           =   2  'Dropdown List
                  TabIndex        =   2
                  Tag             =   "Firm"
                  Top             =   348
                  Width           =   2604
               End
               Begin VB.Label Label1 
                  Alignment       =   1  'Right Justify
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "Search f&or:"
                  Height          =   192
                  Left            =   120
                  TabIndex        =   34
                  Top             =   108
                  Width           =   768
               End
            End
            Begin VB.CommandButton cmdSearchNow 
               Caption         =   "Search Now"
               Height          =   288
               Left            =   4200
               TabIndex        =   3
               Top             =   144
               Width           =   1080
            End
            Begin VB.CommandButton cmdAdvancedSearch 
               Caption         =   "&Advanced"
               Height          =   288
               Index           =   0
               Left            =   4200
               TabIndex        =   4
               Top             =   492
               Width           =   1092
            End
         End
         Begin VB.Label lblLabel 
            Caption         =   "Search"
            Height          =   252
            Index           =   5
            Left            =   120
            TabIndex        =   12
            Top             =   120
            Width           =   1692
         End
      End
      Begin VB.Frame fraFolders 
         Height          =   1836
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   2340
         Begin VB.PictureBox picRemoveFolders 
            BorderStyle     =   0  'None
            Height          =   192
            Left            =   1968
            Picture         =   "frmMain.frx":0010
            ScaleHeight     =   192
            ScaleWidth      =   192
            TabIndex        =   24
            Top             =   144
            Visible         =   0   'False
            Width           =   192
         End
         Begin VB.PictureBox picFolders 
            Height          =   1284
            Left            =   0
            ScaleHeight     =   1236
            ScaleWidth      =   2304
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   360
            Width           =   2352
            Begin MSComctlLib.TreeView tvFolders 
               Height          =   1068
               Left            =   0
               TabIndex        =   0
               Top             =   0
               Width           =   1836
               _ExtentX        =   3239
               _ExtentY        =   1884
               _Version        =   393217
               HideSelection   =   0   'False
               Indentation     =   127
               LabelEdit       =   1
               LineStyle       =   1
               PathSeparator   =   "/"
               Style           =   7
               Appearance      =   0
            End
         End
         Begin VB.Label lblLabel 
            Caption         =   "Folders"
            Height          =   192
            Index           =   0
            Left            =   120
            TabIndex        =   9
            Top             =   120
            Width           =   744
         End
      End
      Begin VB.Image imgSplitter 
         Height          =   5748
         Left            =   2076
         MousePointer    =   9  'Size W E
         Top             =   108
         Width           =   48
      End
   End
   Begin VB.Timer tmrSearch 
      Enabled         =   0   'False
      Interval        =   750
      Left            =   8760
      Top             =   5676
   End
   Begin VB.Timer tmrFolderList 
      Enabled         =   0   'False
      Interval        =   750
      Left            =   8088
      Top             =   5256
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   276
      Left            =   0
      TabIndex        =   5
      Top             =   6816
      Width           =   9492
      _ExtentX        =   16743
      _ExtentY        =   487
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9186
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "4/18/2002"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "6:56 PM"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox dlgCommonDialog 
      Height          =   384
      Left            =   9660
      ScaleHeight     =   336
      ScaleWidth      =   912
      TabIndex        =   35
      Top             =   8472
      Width           =   960
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   7488
      Top             =   5292
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":028E
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":03A0
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":04B2
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":05C4
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":06D6
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":07E8
            Key             =   "View Large Icons"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":08FA
            Key             =   "View Small Icons"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0A0C
            Key             =   "View List"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0B1E
            Key             =   "View Details"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0C30
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlIcons 
      Left            =   10152
      Top             =   8256
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0F8C
            Key             =   "book"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlSmallIcons 
      Left            =   10836
      Top             =   8364
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   13
      ImageHeight     =   13
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":12A6
            Key             =   "closed"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1418
            Key             =   "cylinder"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":158A
            Key             =   "leaf"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":16FC
            Key             =   "open"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":186E
            Key             =   "smlBook"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1B20
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblStart 
      AutoSize        =   -1  'True
      BackColor       =   &H80000018&
      Caption         =   "Click here to begin..."
      Height          =   192
      Left            =   4080
      TabIndex        =   32
      Top             =   4188
      Visible         =   0   'False
      Width           =   1428
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNewFolder 
            Caption         =   "Folder"
         End
         Begin VB.Menu mnuFileNewItem 
            Caption         =   "mnuFileNewItem"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator9832 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "mnuFileNew"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "mnuFileOpen"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileExploreMenu 
         Caption         =   "Exp&lore"
         Begin VB.Menu mnuFileExplore 
            Caption         =   "mnuFileExplore"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator8347 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "&Print..."
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFilePrintSetup 
         Caption         =   "Print Set&up..."
      End
      Begin VB.Menu mnuSeparator8723 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuFileRename 
         Caption         =   "Rena&me"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d to"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuFileBar4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileImport 
         Caption         =   "&Import..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuFileExport 
         Caption         =   "Expor&t..."
      End
      Begin VB.Menu mnuFileBar6 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileLogOff 
         Caption         =   "&Log Off..."
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditCut 
         Caption         =   "Cu&t"
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "&Paste"
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator3343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditRotate 
         Caption         =   "Rotate"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSeparator2343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditSelectAll 
         Caption         =   "Select &All"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuEditInvertSelection 
         Caption         =   "&Invert Selection"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewFolders 
         Caption         =   "&Folders"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "Status &Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewGoToMenu 
         Caption         =   "G&o To"
         Begin VB.Menu mnuViewGoToBack 
            Caption         =   "&Back"
         End
         Begin VB.Menu mnuViewGoToForward 
            Caption         =   "&Forward"
         End
         Begin VB.Menu mnuSeparator34211 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewGoToHome 
            Caption         =   "&Home Folder"
         End
         Begin VB.Menu mnuSeparator82112 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewGoToStartMenu 
            Caption         =   "Start &Menu"
         End
         Begin VB.Menu mnuGoToOpen 
            Caption         =   "&Open Menu"
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuGoToExplore 
            Caption         =   "Exp&lore Menu"
            Shortcut        =   ^L
         End
         Begin VB.Menu mnuSeparator3843 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewGoToFolder 
            Caption         =   "Choose &Folder..."
            Shortcut        =   {F7}
         End
      End
      Begin VB.Menu mnuViewBar33 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "Lar&ge Icons"
         Index           =   0
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "S&mall Icons"
         Index           =   1
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&List"
         Index           =   2
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&Details"
         Index           =   3
      End
      Begin VB.Menu mnuViewBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewArrangeIcons 
         Caption         =   "Arrange &Icons"
         Begin VB.Menu mnuArrangeIcon 
            Caption         =   "mnuArrangeIcon"
            Index           =   0
         End
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuViewBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeparator3467 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewWebBrowser 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeparator8343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsEnvelope 
         Caption         =   "&Envelope..."
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge..."
      End
      Begin VB.Menu mnuSeparator3833 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsOrganizeFolders 
         Caption         =   "Or&ganize Folders..."
      End
      Begin VB.Menu mnuToolsWorkflowOptionsMenu 
         Caption         =   "Wor&kflow"
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&Complete..."
            Index           =   0
         End
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&Void..."
            Index           =   1
         End
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&Reopen"
            Enabled         =   0   'False
            Index           =   2
         End
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&New"
            Index           =   3
         End
      End
      Begin VB.Menu mnuSeparator3432 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsOptions 
         Caption         =   "&Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
      End
      Begin VB.Menu mnuControl 
         Caption         =   "&Control"
         Shortcut        =   ^{F12}
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "&Search For Help On..."
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Explorer..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Begin VB.Menu mnuFolder 
         Caption         =   "Folder"
         Begin VB.Menu mnuFolderNew 
            Caption         =   "&New Folder"
         End
         Begin VB.Menu mnuSeparator3433 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFolderProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuListView 
         Caption         =   "ListView"
         Begin VB.Menu mnuListViewNew 
            Caption         =   "Ne&w"
         End
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
' Main Form
'
'
' The main form can be one of 3 form types set by the the following
' global constants from MainModule:
'
' ftView - Folder list and main window
' ftAdvancedSearch - Look in list, advanced search and a list view
' ftSelect - Advanced search and list view pinned to a folder
'
'
'

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

' Form Arrangement for ftView
Const faMain = 1
Const faListView = 2
  
' Classes for accessing data
Private mView As View
  
'local variable(s) to hold property value(s)
Private mFolderKey As String
Private mArrangement As Integer

Private lastSearch As String
Public TxtWordsIndex As Integer
Private lastCount As Integer

Private mbMoving As Boolean
Const sglSplitLimit = 1000

Public FormMgr As New FormManager

Private mShowPopup As Boolean
Private mLastControl As Control
Private mUnloaded As Boolean

Private mWordLimit As Integer
Private mWordCache As New Collection

Private mSequence As Sequence



' Just return the form type
Public Property Get Arrangement() As Integer
  Arrangement = mArrangement
End Property

' Only ftView has different arrangements.
Public Property Let Arrangement(ByVal vData As Integer)
  If mArrangement = vData Then
    Exit Property
  End If
  mArrangement = vData
    
  '  If we are not on a View form type, then there can be no arrangement change
  Select Case mArrangement
    Case faMain
      picMain.Enabled = True
      picMain.Visible = True
      fraSearch.Visible = False
      fraSearch.Enabled = False
      picListView.Visible = False
      picListView.Enabled = False
      lvView.ListItems.Clear
      lvView.ColumnHeaders.Clear
    
    Case faListView
      fraSearch.Visible = True
      fraSearch.Enabled = True
      picListView.Visible = True
      picListView.Enabled = True
      picMain.Visible = False
      cmdSearchNow.Default = True
   End Select
  
  picView_Resize
   
End Property
  

Public Function Export(ExportIndex As Integer, Optional FileName As String) As Boolean
  ' ToDo Need an export DLL
End Function


' Folder configures for the current folder
Public Property Get FolderKey() As String
  FolderKey = mFolderKey
End Property


' Folder configures for the current folder
Public Property Let FolderKey(Key As String)
  ' Turn off any timer.
  tmrFolderList.Enabled = False
  
  If mFolderKey = Key Then Exit Property
  mFolderKey = Key
  Set mView = Views(Key)
  
  ' Ensure folder is selected.
  If Not Contains(tvFolders.Nodes, Key) Then
    MsgBox "Folder " + Key + " no longer exists."
    Exit Property
  End If
  
  ' Hook in sequence
  If mSequence.Selected <> mFolderKey Then
    mSequence.AddItem FolderKey
    SequenceRefresh
  End If
  
  tvFolders.Nodes(Key).Selected = True
  Caption = "LAW Explorer - " + tvFolders.SelectedItem.Text + "  (" + HttpServer + ")"
  
  
  ' Reset folder dependent menus
  sbStatusBar.Panels(2).Text = ""
  
  If mView Is Nothing Then
    MenuRefresh False
    Arrangement = faMain
    Exit Property
  End If
  
  Arrangement = faListView
  
  lblListView(0) = tvFolders.SelectedItem.Text
  sbStatusBar.Panels(2).Text = mView.DataFinder.DataProvider.Source
    
  ' Setup listview.
  lvView.Tag = Key
  lvView.Visible = False
  lvView.ColumnHeaders.Clear
  lvView.ListItems.Clear
  ListViewModule.DeserializeColumnHeaders lvView, Key
    
  ' Set toolbar
  tbToolBar.Buttons("lvMode" & lvView.View).Value = tbrPressed
         
  mView.DataFinder.LoadSearch Me
  LoadView
    
  lvView.Visible = True
  lvView.Refresh
End Property

'
' Load folder.
'
Public Sub FolderLoad(FolderHandle As String, Optional DefaultMenuName, Optional ListIndex, Optional SearchText)
  If Not IsMissing(DefaultMenuName) Then Views(FolderHandle).DefaultMenuName = DefaultMenuName
  FolderKey = FolderHandle
  If Not IsMissing(ListIndex) Then cboIn.ListIndex = ListIndex
  If txtWords.Visible Then
    If Not IsMissing(SearchText) Then
      txtWords = SearchText
      SearchNow
      If lvView.ListItems.Count = 0 Then
        txtWords.SetFocus
      Else
        lvView.SetFocus
      End If
    Else
      txtWords.SetFocus
    End If
  ElseIf lvView.Visible Then
    lvView.SetFocus
  End If
End Sub

'
' If a child is unloaded, find last form of same name.  Load that first, then
' load the main form.
'
Public Sub FormChildUnload(ByRef vData As FormInterface)
  Dim LastForm As Form
  Set LastForm = FindLastForm(vData.Child)
  If LastForm Is Nothing Then
    Show
    Refresh
  Else
    LastForm.Show
  End If
End Sub

'
' An interface is set when using frmMain as a select
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Do nothing if a parent form is unloaded
'
Public Sub FormParentUnload()
End Sub

Public Sub FileNew(Source As String)
  
  MsgBox "New " & Source
  ' ToDo - Need new module.
End Sub


Private Sub FileNewLoad(Caption As String, Index As Integer, Optional Shortcut As String, Optional Enabled As Boolean)
  Load mnuFileNew(Index)
  mnuFileNew(Index).Caption = Caption & IIf(Shortcut = vbNullString, "", vbTab & Shortcut)
  mnuFileNew(Index).Enabled = Enabled
  mnuFileNew(Index).Tag = Replace(Caption, " ", "")
End Sub


Private Sub FileNewMenuLoad()
  FileNewLoad "Application", 100, "Ctrl+Shift+A", True
  FileNewLoad "Application Law Area", 110, , True
  FileNewLoad "Application Lawyer", 120, , True
  FileNewLoad "Application Limit", 130, , True
  FileNewLoad "Application Question", 140, , True
  FileNewLoad "Application Type", 160, , True
  FileNewLoad "Claim", 170, , True
  FileNewLoad "Claim Payment", 190, , True
  FileNewLoad "Claim Reserve", 200, , True
  FileNewLoad "Contact", 235, , True
  FileNewLoad "Contact Type", 240, , True
  FileNewLoad "Deductible Reimburse", 250, , True
  FileNewLoad "Endorsement Type", 270, , True
  FileNewLoad "Event Type", 280, , True
  FileNewLoad "Firm", 290, "Ctrl+Shift+F", True
  FileNewLoad "Firm Insurer", 300, , True
  FileNewLoad "Firm Lawyer", 310, , True
  FileNewLoad "Fiscal Calendar", 320, , True
  FileNewLoad "GL Account", 330, , True
  FileNewLoad "GL Account Group", 335, , True
  FileNewLoad "GL Batch", 340, , True
  FileNewLoad "Incident", 350, , True
  FileNewLoad "Insurer", 360, , True
  FileNewLoad "Law Area", 380, , True
  FileNewLoad "Law Disposition", 390, , True
  FileNewLoad "Law Error", 400, , True
  FileNewLoad "Lawyer", 410, "Ctrl+Shift+L", True
  FileNewLoad "Location", 420, "Ctrl+Shift+O", True
  FileNewLoad "Location Type", 430, , True
  FileNewLoad "Person", 440, , True
  FileNewLoad "Policy Accounting", 460, , True
  FileNewLoad "Question", 470, , True
  FileNewLoad "Rating Book", 490, , True
  FileNewLoad "Rating Book Metric", 500, , True
  FileNewLoad "Rating Book Metric Calc", 510, , True
  FileNewLoad "Rating Metric", 540, , True
  FileNewLoad "Rating Rate", 550, , True
  mnuFileNew(0).Visible = False
End Sub


'
' Update ListView after a search.
'
Public Sub ListViewResult()
  Dim IsCount As Boolean
  IsCount = (lvView.ListItems.Count > 0)
  picSearchStatus.Visible = Not IsCount
  
  ' Can not just check picSerachStatus.Visible since it may not
  ' be visible yet, even though it was set.
  If IsCount Then
    ' Undo any selected item
    lvView.SelectedItem.Selected = False
  Else
    If fraSearch.Visible Then
      lblSearchStatus.Caption = "Search is complete.  There are no results to display."
    Else
      lblSearchStatus.Caption = "There is nothing to display."
    End If
  End If
     
  MenuViewRefresh
End Sub


Public Sub LoadView()
  MenuArrangeIconLoad
  MenuViewRefresh
  
  Dim SearchOnly As Boolean
  SearchOnly = mView.IsSearchOnly
  
  fraSearch.Visible = SearchOnly
  mnuEditRotate.Enabled = SearchOnly
  
  If SearchOnly Then
    lastSearch = ""
    If mView.DataFinder.DataProvider.isOpen Then
      ' DataFinder is already open
      picSearchStatus.Visible = False
      mView.DataFinder.DataProvider.SetListView lvView
      ListViewResult
    Else
      ' Setup list view for searching.
      lvView.ListItems.Clear
      picListView_Resize
      lblSearchStatus.Caption = "Enter your search criteria to begin."
      picSearchStatus.Visible = True
    End If
  Else
    picSearchStatus.Visible = False
    If mView.DataFinder.DataProvider.isOpen Then
      mView.DataFinder.DataProvider.SetListView lvView
    Else
      Dim Pointer As New WaitPointer
      Pointer.Pointer = vbHourglass
      ListViewLoad lvView, mView.DataFinder.DataProvider, False, True
    End If
    ListViewResult
  End If
  
  picView_Resize
End Sub


Public Sub MenuSubItem(Optional X, Optional Y)
  PopupMenu mnuListView, , X, Y
End Sub


Public Sub MenuListItem(Optional X, Optional Y)
  MenuViewRefresh
    
  Set mView.PopupMenuForm = PlugInUtil.NewPopupMenuForm(mView.PlugInName, FormMgr, lvView)
  If mView.PopupMenuForm Is Nothing Then
    Exit Sub
  End If
  
  On Error Resume Next
  PopupMenu mView.PopupMenu, , X, Y, mView.DefaultMenu
  ' Either default menu doesn't exist or control array doesn't exist, or the default
  ' menu is a sub menu.
  If Err = 427 Or Err = 490 Then
    PopupMenu mView.PopupMenu, , X, Y
  ElseIf Err > 0 Then
    'MsgBox Err.Description
  End If
  Err.Clear
  
  On Error GoTo 0
  
  Unload mView.PopupMenuForm
  Set mView.PopupMenuForm = Nothing
  
  MenuViewRefresh
End Sub


Public Sub MenuRefresh(IsView As Boolean)
  mnuFileNewItem.Enabled = IsView
  mnuViewRefresh.Enabled = IsView
  mnuViewArrangeIcons.Enabled = IsView
  mnuViewChooseColumns.Enabled = IsView
  mnuViewSaveSettings.Enabled = IsView
  mnuToolsWorkflowOptionsMenu.Enabled = IsView
  mnuListViewMode(0).Enabled = IsView
  mnuListViewMode(1).Enabled = IsView
  mnuListViewMode(2).Enabled = IsView
  mnuListViewMode(3).Enabled = IsView
  
  If IsView Then
    MenuViewRefresh
  Else
    MenuViewDisable
  End If
End Sub


Public Sub MenuViewDisable()
  mnuFileOpenMenu.Enabled = False
  mnuFileExploreMenu.Enabled = False
  mnuFileNewItem.Visible = False
  mnuFilePrint.Enabled = False
  mnuFileExport.Enabled = False
  mnuFileSendTo.Enabled = False
  mnuFileWorkflow(0).Enabled = False
  mnuFileWorkflow(1).Enabled = False
  
  mnuEditCut.Enabled = False
  mnuEditPaste.Enabled = False
  mnuEditMove.Enabled = False
  mnuEditInvertSelection.Enabled = False
  mnuEditSelectAll.Enabled = False
End Sub


Public Sub MenuViewRefresh()
  Dim Count As Integer, IsCount, Selected As Integer, IsSelected, Workflow As Boolean
  Count = lvView.ListItems.Count
  IsCount = Count > 0
  Selected = ListViewModule.SelectedCount(lvView.hWnd)
  IsSelected = Selected > 0
  Workflow = mView.IsWorkflow
  
  
  ' set arrange check
  If (lvView.ColumnHeaders.Count > 0) Then
'    mnuArrangeIcon(lvView.SortKey + 1).Checked = True
'    Popup.mnuListViewArrangeIcon(lvView.SortKey + 1).Checked = True
  End If
  
  ' If an item selected, copy over popup open and explore.
  If IsSelected Then
    sbStatusBar.Panels(1).Text = Selected & " of " & Count & " object(s) selected"
        
    ' Copy over Open and Explore
    FormUtil.MenuCopy mView.OpenMenu, mnuFileOpen
    FormUtil.MenuCopy mView.ExploreMenu, mnuFileExplore
  Else
    sbStatusBar.Panels(1).Text = Count & " object(s)"
  End If
  
  mnuFileOpenMenu.Enabled = IsSelected And Not mnuFileOpen(0).Visible
  mnuFileExploreMenu.Enabled = IsSelected And Not mnuFileExplore(0).Visible
  mnuFileNewItem.Visible = (mView.NewItemName <> vbNullString)
  If mnuFileNewItem.Visible Then
    mnuFileNewItem.Caption = mView.NewItemName
  End If
  mnuFilePrint.Enabled = IsCount
  mnuFileExport.Enabled = IsSelected
  mnuFileSendTo.Enabled = IsSelected And Not Workflow
    
  mnuFileWorkflow(0).Enabled = Workflow
  mnuFileWorkflow(1).Enabled = Workflow
    
  mnuEditCut.Enabled = IsSelected And Workflow
  mnuEditPaste.Enabled = Not EventBuffer Is Nothing And Workflow
  mnuEditMove.Enabled = IsSelected And Workflow
  
  mnuEditInvertSelection.Enabled = IsCount
  mnuEditSelectAll.Enabled = IsCount
End Sub


'
' Helper function for adding a new view.
'
Public Sub NewView(Key As String, PlugInName As String, _
      Optional FolderType As String, Optional mIsSearchOnly As Boolean)
    
  Dim SourceFinder As DataFinder, Filter As New Filter, IsWorkflow As Boolean
  Set SourceFinder = NewFinder(PlugInName)
  IsWorkflow = FolderType = "Workflow"
  
  Filter.Add IniFolder.ReadFile(Key, "filter")
  If IsWorkflow Then
    Filter.Add """folderId"" = '" & Key & "'"
  End If
  SourceFinder.DataProvider.Filter = Filter.Filter
  
  Dim MenuPopup As String, PopupDefault As String, OpenMenu As String
  MenuPopup = IniFolder.ReadFile(Key, "popup")
  If MenuPopup = vbNullString Then
    Views.Add Key, PlugInName, SourceFinder, IsWorkflow, mIsSearchOnly
    Exit Sub
  End If
  
  Views.Add Key, PlugInName, SourceFinder, IsWorkflow, mIsSearchOnly, MenuPopup, IniFolder.ReadFile(Key, "popup.default"), IniFolder.ReadFile(Key, "popup.open"), IniFolder.ReadFile(Key, "popup.explore"), IniFolder.ReadFile(Key, "newitem")
End Sub


Private Sub PopulateTree(Tree As TreeView)
  ' Clear tree and folders.
  Tree.Nodes.Clear
  
  Dim Parent As Node
  Set Parent = Tree.Nodes.Add(, , , RootFolderName)
  
  PopulateChildren Tree, Parent, InitialContext
  Exit Sub
  
  ' Original Code.
  
  Dim FolderNode As MSXML2.IXMLDOMNode
  Set FolderNode = GetFolderNode
  If FolderNode Is Nothing Then
    MsgBox "Unable to retrieve folders from middle tier"
    Exit Sub
  End If
  
  Set Views = New ViewItems
  PopulateTreeWithChildren Tree, Nothing, FolderNode
End Sub


Private Sub PopulateChildren(Tree As TreeView, Parent As Node, Context As String, Optional Name As String)
  Dim I As Integer, Children() As String, Child As Node
  Children = Factory.NamingService.ListContexts(Context, Name)
  For I = 0 To SafeUBound(Children)
    Set Child = Tree.Nodes.Add(Parent, tvwChild, , Children(I))
    Tree.Nodes.Add Child, tvwChild, , ""
  Next I
End Sub

Public Sub SelectByPath(Path As String)
  Dim Contexts() As String, Size As Integer
  Contexts = Split(Path, tvFolders.PathSeparator)
  Size = SafeUBound(Contexts)
  If Size < 0 Then
    tvFolders.Nodes(1).Selected = True
    Exit Sub
  End If
     
  Dim I As Integer, j As Integer, Last As Integer, Node As Node
  Set Node = tvFolders.Nodes(1)
  j = 1
  For I = 0 To Size
    If Node.Children = 0 Then
      Exit Sub
    End If
    
    j = Node.Child.Index
    Last = tvFolders.Nodes(j).LastSibling.Index
    
    ' While N is not the index of the child node's
    ' last sibling, get next sibling's text.
    While j <> Last And tvFolders.Nodes(j).Text <> Contexts(I)
      j = tvFolders.Nodes(j).Next.Index
    Wend
    If tvFolders.Nodes(j).Text <> Contexts(I) Then
      Exit Sub
    End If
    tvFolders.Nodes(j).Expanded = True
    tvFolders.SelectedItem = tvFolders.Nodes(j)
    Set Node = tvFolders.Nodes(j)
  Next I
End Sub

'Walk tree
Private Sub PopulateTreeWithChildren(Tree As TreeView, ParentNode As Node, xmlNode As IXMLDOMNode)
  Dim TreeChild As Node
  Dim Key As String
  Dim Text As String
  
  Key = xmlNode.Attributes.getNamedItem("id").Text
  Text = xmlNode.Attributes.getNamedItem("name").Text
  
  If (ParentNode Is Nothing) Then
    Set TreeChild = Tree.Nodes.Add(, , Key, Text)
  Else
    Set TreeChild = Tree.Nodes.Add(ParentNode, tvwChild, Key, Text)
  End If
  
  IniFolder.RemoveKey Key, "arrangement"
  
  Dim Source As String
  Source = xmlNode.Attributes.getNamedItem("source").Text
  If Not Source = vbNullString Then
    NewView Key, Source, _
        xmlNode.Attributes.getNamedItem("type").Text, _
        (IniFolder.ReadFile(Key, "search") = "1")
  End If
  
  ' Walk Children.
  Dim xmlChild As IXMLDOMNode
  For Each xmlChild In xmlNode.childNodes
    If xmlChild.NodeName = "folder" Then
      PopulateTreeWithChildren Tree, TreeChild, xmlChild
    End If
  Next
End Sub


Private Sub MenuArrangeIconLoad()
'  Dim ColumnCount, I As Integer
'  ColumnCount = lvView.ColumnHeaders.Count
'
'  mnuViewArrangeIcons.Enabled = ColumnCount > 0
'  Popup.mnuListViewArrangeIcons.Enabled = ColumnCount > 0
'
'  MenuUnload mnuArrangeIcon
'  For I = 1 To ColumnCount
'    Load mnuArrangeIcon(I)
'    mnuArrangeIcon(I).Caption = lvView.ColumnHeaders(I).Text
'  Next I
'
'  If mnuViewArrangeIcons.Enabled Then
'    mnuArrangeIcon(0).Visible = False
'  End If
'
'  MenuCopy mnuArrangeIcon, Popup.mnuListViewArrangeIcon
End Sub

Public Property Get View() As View
  Set View = mView
End Property

'
' Refresh underlying data for a folder.  Called when we know the underlying data
' changes and we may not be on the foler
'
Public Sub RefreshFolder(Key As String)
  Views(Key).DataFinder.DataProvider.OpenRecordset
End Sub


Public Sub SearchNow()
  ' Show hourglass
  Dim Pointer As New WaitPointer
  Pointer.Pointer = vbHourglass
  
  If txtWords <> vbNullString Then
    ComboBoxUtil.AddText txtWords, True, mWordLimit
    ComboBoxUtil.Serialize txtWords, cboIn.Text, IniSearch, mWordCache
  End If
  
  With lvView
    .ListItems.Clear
    lblSearchStatus.Caption = "Searching..."
    picSearchStatus.Visible = True
    lblSearchStatus.Refresh
  
    ' lastSearch is used for tmrSearch
    lastSearch = txtWords.Text
  
    ' Setup column, sort and find
    If .ColumnHeaders.Count > 0 Then
      mView.DataFinder.DataProvider.Column = LawSQL.Column(lvView.ColumnHeaders)
    End If
    mView.DataFinder.DataProvider.Sort = LawSQL.Sort(lvView)
    
    mView.DataFinder.FindByForm Me
  
    ' if results.
    If Not .SelectedItem Is Nothing Then
      SetDefault
      .SelectedItem.Selected = False
    End If
  End With
  
  ListViewResult
End Sub

'
' Set default command button
'
Private Sub SetDefault()
  cmdDefault.Default = True
End Sub


Private Sub SequenceRefresh()
  Dim Back, Forward As Boolean
  If Not mSequence Is Nothing Then
    With mSequence
      Back = .ListIndex < .ListCount - 1
      Forward = .ListIndex > 0
    End With
  End If
  
  'cmdBack.Enabled = Back
  mnuViewGoToBack.Enabled = Back
  'cmdForward.Enabled = Forward
  mnuViewGoToForward.Enabled = Forward
End Sub
'
' End General Methods
'
'
'
'
' Control Methods
'


Private Sub cboIn_Click()
  txtWords_GotFocus
End Sub

Private Sub cboIn_KeyDown(KeyCode As Integer, Shift As Integer)
  'Trap F6
  If Me.View.DataFinder Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdAdvancedSearch_Click(Index As Integer)
  mnuAdvancedFind_Click
End Sub

Private Sub cmdAdvancedSearch_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  'Trap F6
  If Me.View.DataFinder Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub

Private Sub cmdDefault_Click()
  If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
    Exit Sub
  End If

  ' Call default menu if available.
  If mView.DefaultMenuName = vbNullString Then
    Exit Sub
  End If
  
  Set mView.PopupMenuForm = PlugInUtil.NewPopupMenuForm(mView.PlugInName, FormMgr, lvView)
  If mView.PopupMenuForm Is Nothing Then
    Exit Sub
  End If

  If mView.DefaultMenuIndex < 0 Then
    CallByName mView.PopupMenuForm, mView.DefaultMenuName & "_Click", VbMethod
  Else
    CallByName mView.PopupMenuForm, mView.DefaultMenuName & "_Click", VbMethod, mView.DefaultMenuIndex
  End If
  
  Unload mView.PopupMenuForm
  Set mView.PopupMenuForm = Nothing
End Sub

Private Sub cmdMenu_Click()
  mnuViewGoToStartMenu_Click
'  If ActiveControl Is cmdMenu And Not mLastControl Is Nothing And mLastControl.Visible Then
'    mLastControl.SetFocus
'  End If
End Sub


Private Sub cmdSearchNow_Click()
  mnuViewRefresh_Click
  If lvView.ListItems.Count > 0 Then
    lvView.ListItems(1).Selected = True
    MenuViewRefresh
  End If
End Sub


Private Sub cmdSearchNow_KeyDown(KeyCode As Integer, Shift As Integer)
  'Trap F6
  If Me.View.DataFinder Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub



'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If (Shift And vbCtrlMask) And (Shift And vbShiftMask) Then
    Select Case KeyCode
      Case vbKeyA
        FileNew "Application"
        KeyCode = 0
        Shift = 0
      Case vbKeyF
        FileNew "Firm"
        KeyCode = 0
        Shift = 0
      Case vbKeyL
        FileNew "Lawyer"
        KeyCode = 0
        Shift = 0
      Case vbKeyO
        FileNew "Location"
        KeyCode = 0
        Shift = 0
      Case vbKeyP
        FileNew "Person"
        KeyCode = 0
        Shift = 0
    End Select
    Exit Sub
  End If
  If (Shift And vbAltMask) And Not (Shift And vbShiftMask) And Not (Shift And vbCtrlMask) Then
    Select Case KeyCode
      Case vbKeyLeft
        If mnuViewGoToBack.Enabled Then
          mnuViewGoToBack_Click
        End If
        Shift = 0
        KeyCode = 0
      Case vbKeyRight
        If mnuViewGoToForward.Enabled Then
          mnuViewGoToForward_Click
        End If
        Shift = 0
        KeyCode = 0
      Case vbKeyHome
        mnuViewGoToHome_Click
        Shift = 0
        KeyCode = 0
      Case vbKeyM
        Shift = 0
        KeyCode = 0
        mShowPopup = True
    End Select
    Exit Sub
  End If
  If ((Shift And vbAltMask) = 0 And (Shift And vbShiftMask) = 0) Then
    Select Case KeyCode
      Case vbKeyDelete
        mnuFile_Click
        If mnuFileDelete.Visible And mnuFileDelete.Enabled Then
          mnuFileDelete_Click
        End If
    End Select
  End If
End Sub

Private Sub Form_Load()
  If mUnloaded Then
    MsgBox Name & ": Illegal Load Attempt.  Please report."
    Unload Me
    Exit Sub
  End If
  
  ' Load form manager.
  FormMgr.Load Me
   
  ' Populuate tree and load folders.
  PopulateTree tvFolders
  
  'Get view settings from ini file.
  mnuViewFolders.Checked = IniMain.ReadFile("Main", "ViewFolders")
  mnuViewToolbar.Checked = IniMain.ReadFile("Main", "ViewToolbar")
  mnuViewStatusBar.Checked = IniMain.ReadFile("Main", "ViewStatusBar")
  mnuViewGoToStartMenu.Caption = mnuViewGoToStartMenu.Caption + vbTab + "Alt+M"
   
  ' position settings
  ' set defaults first
  Top = 0
  Left = 0
  Height = 7200
  Width = 9600
  ' get saved settings, if any.
  FormUtil.DeserializePosition Me
   
  ' Custom Menu Shortcut Adjustments
  KeyPreview = True
  mnuFileExit.Caption = mnuFileExit.Caption + vbTab + "Alt+F4"
  mnuViewGoToBack.Caption = mnuViewGoToBack.Caption + vbTab + "Alt+Left Arrow"
  mnuViewGoToForward.Caption = mnuViewGoToForward.Caption + vbTab + "Alt+Right Arrow"
  mnuViewGoToHome.Caption = mnuViewGoToHome.Caption + vbTab + "Alt+Home"
  
  ' Initialize Controls
  Set mSequence = New Sequence
  mSequence.Size = 10
  SequenceRefresh
  
  
  FileNewMenuLoad
  
  ' Hide alt key shortcuts.
  cmdMenu.Left = -20000
  cmdDefault.Left = -20000
  
  
  ' Setup screen.
  tbToolBar.Height = tbToolBar.ButtonHeight
  tbrStart.Height = tbrStart.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrStart.Height + 24
  cbrCoolBar.Bands(1).MinWidth = tbrStart.ButtonWidth
  cbrCoolBar.Bands(1).Width = tbrStart.ButtonWidth
  cbrCoolBar.Bands(2).MinHeight = tbToolBar.Height + 24
  
  ' Unload splash screen last
  If Not fSplash Is Nothing Then
    Unload fSplash
  End If
      
  ' Get any limit to the drop downs.
  ComboBoxUtil.SetDropDownHeight Me, txtWords, mWordLimit
  mWordLimit = Max(GetInteger(IniMain.ReadFile("WordList", "limit")), 20)
      
  lblWelcolme.Caption = "Welcome to the " & LocaleShortOrganization & " implementation of the LAW System."
      
'  lblStart.Move cbrCoolBar.Left + picStart.Left + 24, _
'        cbrCoolBar.Top + picStart.Top + cbrCoolBar.Bands(2).Height + 12
'  lblStart.Visible = True
End Sub

'
' Ensure any outstanding timer is disabled when leaving form.
'
Private Sub Form_LostFocus()
  tmrFolderList.Enabled = False
  tmrSearch.Enabled = False
End Sub

Private Sub Form_Resize()
  ' Do not resize when minimized.
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  fraFolders.Visible = mnuViewFolders.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  sbStatusBar.Visible = mnuViewStatusBar.Checked
  
  Dim StatusBar As Integer
  StatusBar = IIf(sbStatusBar.Visible, sbStatusBar.Height, 0)
  picView.Height = Max(Me.ScaleHeight - StatusBar - picView.Top, 0)
End Sub

'
' Uncomment MsgBox to test for proper form termination.  Terminate
' should NOT reference any controls or the form is reloaded.
'
Private Sub Form_Terminate()
  'MsgBox "Main_Terminate"
End Sub


Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  
  ComboBoxUtil.Flush IniSearch, mWordCache
  
  ' Close down form manager
  FormMgr.Unload
    
  ' See if we are saving previous folder.
  If GetInteger(IniForm.ReadFile(Name, "home")) = 0 Then
    IniForm.WriteFile Name, "folder", LocalPath(tvFolders.SelectedItem.FullPath)
  End If
  
  mUnloaded = True
End Sub





Private Sub imgSplitter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  With imgSplitter
    picSplitter.Move .Left, .Top + picView.Top, .Width, .Height - 20
  End With
  picSplitter.Visible = True
  mbMoving = True
End Sub


Private Sub imgSplitter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim sglPos As Single
  
  If mbMoving Then
    sglPos = X + imgSplitter.Left
    If sglPos < sglSplitLimit Then
      picSplitter.Left = sglSplitLimit
    ElseIf sglPos > Me.Width - sglSplitLimit Then
      picSplitter.Left = Me.Width - sglSplitLimit
    Else
      picSplitter.Left = sglPos
    End If
  End If
End Sub

Private Sub imgSplitter_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  fraFolders.Width = picSplitter.Left - fraFolders.Left - picView.Left
  picFolders.Width = fraFolders.Width
  picView_Resize
  picSplitter.Visible = False
  mbMoving = False
End Sub

Private Sub lblListView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  picListView_MouseUp Button, Shift, X, Y
End Sub



Public Sub lvView_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'  mnuArrangeIcon(lvView.SortKey + 1).Checked = False
'  Popup.mnuListViewArrangeIcon(lvView.SortKey + 1).Checked = False
  
  If lvView.ListItems.Count = 0 Then
    ListViewSort lvView, ColumnHeader
  Else
    Dim Pointer As New WaitPointer
    Pointer.Pointer = vbHourglass
    ListViewSort lvView, ColumnHeader, mView.DataFinder.DataProvider, True
  End If
  MenuViewRefresh
End Sub

Private Sub lvView_DblClick()
  cmdDefault_Click
End Sub

Private Sub lvView_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF10 And (Shift And vbShiftMask) > 0 And Not mView.PopupMenu Is Nothing Then
    If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
      MenuSubItem picListView.Left + 24, picListView.Top + 24
    Else
      MenuListItem picListView.Left + 24, picListView.Top + 24
    End If
    KeyCode = 0
    Exit Sub
  End If

  'Trap F6
  If KeyCode = vbKeyF6 Then
    If (Shift And vbShiftMask) > 0 Then
      If txtWords.Visible Then txtWords.SetFocus
    Else
      If tvFolders.Visible Then tvFolders.SetFocus
    End If
  End If
End Sub

Private Sub lvView_KeyUp(KeyCode As Integer, Shift As Integer)
  MenuViewRefresh
End Sub

Private Sub lvView_LostFocus()
  tvFolders_GotFocus
  tvFolders_LostFocus
  Set mLastControl = lvView
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView.hWnd, X, Y) And ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    lvView.SelectedItem.Selected = False
  End If
  
  
  If Button = vbRightButton Then ' And (Not mView.PopupMenu Is Nothing) Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
       MenuSubItem
     Else
       MenuListItem
     End If
  End If
  MenuViewRefresh
End Sub

Private Sub lvView_GotFocus()
  If Not lvView.SelectedItem Is Nothing Then
    SetDefault
  End If
   
  mnuFileOpenMenu.Visible = True
  mnuFileExploreMenu.Visible = True
  mnuFileBar1.Visible = True
  mnuFileSendTo.Visible = True
  
  mnuFileWorkflow(0).Visible = True
  mnuFileWorkflow(1).Visible = True
    
  ' Refresh full menu.  Don't do MenuViewRefresh as we
  ' need to enable the view set.
  MenuRefresh True
End Sub


Private Sub mnuAdvancedFind_Click()
  If Not mView.IsWorkflow Then
     Dispatcher.ShowSearch mView.Key, FormMgr
  End If
End Sub

Private Sub mnuArrangeIcon_Click(Index As Integer)
  Call lvView_ColumnClick(lvView.ColumnHeaders(Index))
End Sub

Private Sub mnuControl_Click()
  MsgBox Me.ActiveControl.Name & " TabIndex: " & Me.ActiveControl.TabIndex
End Sub


'
' Called from event popup.
'
Public Sub mnuEditCut_Click()
  Dim I, Count As Integer
  Set EventBuffer = New Collection
  EventBufferFolder = mFolderKey
  
  With lvView
    Count = .ListItems.Count
    For I = 1 To Count
      If .ListItems(I).Selected Then
        EventBuffer.Add ListItemText(.ListItems(I), "eventId")
      End If
    Next I
  End With
  
  MenuViewRefresh
End Sub

Private Sub mnuEditInvertSelection_Click()
  Dim I, Count As Integer
  Count = lvView.ListItems.Count
  
  With lvView
    Count = .ListItems.Count
    For I = 1 To Count
      .ListItems(I).Selected = (Not .ListItems(I).Selected)
    Next I
  End With
  
  MenuViewRefresh
End Sub



Public Sub mnuEditMove_Click()
' ToDo PlugIn

'  Dim fForm As New frmFolderBrowseDialog
'  If fForm.ShowMove(tvFolders.SelectedItem.Key) Then
'    Factory.EventService.MoveToFolder lvView, fForm.FolderKey
'    mnuViewRefresh_Click
'  End If
'  Unload fForm
End Sub

'
' Called from popups.
'
Public Sub mnuEditPaste_Click()
' ToDo PlugIn

'  Dim Hourglass As New WaitPointer
'  Hourglass.Default
'
'  Factory.EventService.PasteToFolder mFolderKey
'
'  ' Mark old folder as dirty and Refresh current
'  RefreshFolder EventBufferFolder
'  mnuViewRefresh_Click
'
'  EventBufferFolder = ""
'  Set EventBuffer = Nothing
'
'  MenuViewRefresh
End Sub


Private Sub mnuEditRotate_Click()
  cboIn.ListIndex = (cboIn.ListIndex + 1) Mod cboIn.ListCount
  txtWords.SetFocus
End Sub

Private Sub mnuEditSelectAll_Click()
  ListViewModule.SelectAll lvView.hWnd
  MenuViewRefresh
End Sub


'
' Set Open/Explore menus
'
Public Sub mnuFile_Click()
  If ActiveControl Is lvView Then
    mnuFileProperties.Tag = vbNullString
    If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
      mnuFileProperties.Tag = ListViewModule.ListItemText(lvView.SelectedItem, "eventId")
    End If
    mnuFileProperties.Enabled = mnuFileProperties.Tag <> vbNullString
  End If
  MenuRefresh Not mView Is Nothing
End Sub

'
' Called from popups.
'
Public Sub mnuFileDelete_Click()
  Factory.FolderService.RemoveFolder tvFolders
  FolderKey = tvFolders.SelectedItem.Key
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileExplore_Click(Index As Integer)
  CallByName mView.PopupMenuForm, mView.ExploreMenuName & "_Click", VbMethod, Index
End Sub

Private Sub mnuFileExport_Click()
'  Dim ExportDlg As New frmExportDialog
'  Dim Index As Integer
'  Index = ExportDlg.ShowExport(Me)
'  If Index > -1 Then
'    Export Index
'  End If
End Sub

Private Sub mnuFileLogOff_Click()
  Dim fLogin As New frmLoginDialog
  fLogin.Show vbModal
  If Not fLogin.OK Then
    End
  End If
  HttpServer = fLogin.cboServer.Text
  Unload fLogin
  PopulateTree tvFolders
End Sub


Private Sub mnuFileNew_Click(Index As Integer)
  FileNew mnuFileNew(Index).Tag
End Sub

Public Sub mnuFileNewFolder_Click()
  Dim Node As Node, ViewFile As String
  Set Node = Factory.FolderService.AddFolder(tvFolders)
  If Node Is Nothing Then Exit Sub
  
  NewView Node.Key, "EventView", "Workflow"
  FolderKey = Node.Key
  tvFolders.StartLabelEdit
End Sub

Public Sub mnuFileNewItem_Click()
  PlugInUtil.ShowNew mView.PlugInName, FormMgr
End Sub

Public Sub mnuFileOpen_Click(Index As Integer)
  CallByName mView.PopupMenuForm, mView.OpenMenuName & "_Click", VbMethod, Index
End Sub


Private Sub mnuFilePrint_Click()
  PrintUtil.PrintDialog Me, lvView
End Sub

Private Sub mnuFilePrintSetup_Click()
  PrintUtil.PrintSetupDialog Me
End Sub

Public Sub mnuFileProperties_Click()
  ' trap for list view.
  If ActiveControl Is lvView Then
    If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
      FormUtil.ShowEventProperties mnuFileProperties.Tag
      Exit Sub
    End If
  End If
  
  ' If no list view trap, then just show folder properties.
  Dim fProperties As New frmFolderPropertiesEdit
  With tvFolders.SelectedItem
    fProperties.ShowFolder .Text, .FullPath
  End With
End Sub


Public Sub mnuFileRename_Click()
  If ActiveControl Is Nothing Then Exit Sub
  If ActiveControl Is tvFolders Then
    If Not tvFolders.SelectedItem Is Nothing Then
      tvFolders.StartLabelEdit
    End If
  End If
End Sub

'
' Called from various popups.
'
Public Sub mnuFileSendToFolder_Click()
'  Dim Source As String, Group As String, ReferenceId As String, EventName As String
'  Select Case mFolderKey
'    Case DatabaseName.APPLICATION_TABLE
'      Source = DatabaseName.APPLICATION_TABLE
'      Group = DatabaseName.APPLICATION_REF
'      ReferenceId = DatabaseName.APPLICATION_KEY
'      EventName = "firmName"
'    Case DatabaseName.APPLICATIONRATING_TABLE
'      Source = DatabaseName.APPLICATIONRATING_TABLE
'      Group = DatabaseName.APPLICATIONRATING_REF
'      ReferenceId = DatabaseName.APPLICATIONRATING_KEY
'      EventName = "firmName"
'    Case DatabaseName.FIRM_TABLE
'      Source = DatabaseName.FIRM_TABLE
'      Group = DatabaseName.FIRM_TABLE & "Reference"
'      ReferenceId = "firmId"
'      EventName = "firmName"
'    Case DatabaseName.CLAIM_TABLE
'      Source = DatabaseName.CLAIM_TABLE
'      Group = DatabaseName.CLAIM_REF
'      ReferenceId = DatabaseName.CLAIM_KEY
'      EventName = "claimName"
'  End Select
'
'  ' If not a valid source for reference.
'  If Source = vbNullString Then
'    MsgBox "Send To Folder is not supported for this record type."
'    Exit Sub
'  End If
'
'  ' Create form and show.
'  Dim Form As New frmFolderBrowseDialog
'  If Not Form.ShowSendTo(Group) Then
'    Unload Form
'    Exit Sub
'  End If
'
'  '
'  ' Form is OK
'  '
'  Refresh
'  Dim Pointer As New WaitPointer
'  Pointer.Default
'
'  Call Factory.EventService.Create( _
'      Form.EventTypeId, _
'      Group, _
'      Form.FolderKey, _
'      Source, _
'      ListViewText(lvView, ReferenceId), _
'      ListViewText(lvView, EventName))
'
'  Unload Form
End Sub

Public Sub mnuFileWorkflow_Click(Index As Integer)
'  Dim ParameterNames() As String
'  Dim ParameterValues() As String
'  If Not FolderUtil.WorkflowShow(mFolderKey, Index, ParameterNames, ParameterValues, True) Then
'    Exit Sub
'  End If
'
'  '  Call Service
'  Dim Pointer As New WaitPointer
'  Pointer.Default
'  Factory.EventService.SetViewWorkflow Index, lvView, DatabaseName.EVENT_KEY, ParameterNames, ParameterValues
'  SearchNow
End Sub

Private Sub mnuFolderNew_Click()
  mnuFileNewFolder_Click
End Sub

Private Sub mnuFolderProperties_Click()
  mnuFileProperties_Click
End Sub

Private Sub mnuGoToExplore_Click()
  If mnuFileExploreMenu.Enabled Then
    PopupMenu mnuFileExploreMenu, , picListView.Left + 24, picListView.Top + 24
  End If
End Sub

Private Sub mnuGoToOpen_Click()
  If mnuFileOpenMenu.Enabled Then
    PopupMenu mnuFileOpenMenu, , picListView.Left + 24, picListView.Top + 24
  End If
End Sub

Private Sub mnuHelpAbout_Click()
    frmAbout.Show vbModal, Me
End Sub


Private Sub mnuHelpSearchForHelpOn_Click()
    Dim nRet As Integer

    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hWnd, App.HelpFile, 261, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If

End Sub

Private Sub mnuHelpContents_Click()
    Dim nRet As Integer


    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hWnd, App.HelpFile, 3, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If
End Sub

Private Sub mnuListViewMode_Click(Index As Integer)
  lvView.View = Index
  tbToolBar.Buttons("lvMode" & Index) = tbrPressed
End Sub

Private Sub mnuTEST_Click()

End Sub

Private Sub mnuListViewNew_Click()
  mnuFileNewItem_Click
End Sub

Private Sub mnuToolsEnvelope_Click()
  FormUtil.PrintEnvelope
End Sub

Private Sub mnuToolsMerge_Click()
'  Dim Pointer As New WaitPointer
'  Pointer.Default
'
'  Dim Handles() As String, I As Integer
'  Handles = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
'
'  ' Print Envelopes
'  For I = 0 To SafeUBound(Handles)
'    Factory.EventRecipientService.PrintEnvelope Handles(I)
'  Next I
'
'  ' Merge Documents
'  Factory.EventService.DocumentMerge Handles
End Sub

Private Sub mnuToolsOptions_Click()
  Dim fOptions As New frmMainOptionsDialog
  fOptions.Show vbModal, Me
  Unload fOptions
  picListView_Resize
End Sub

Public Sub mnuToolsOrganizeFolders_Click()
  Dim fForm As New frmFolderOrganizeDialog
  fForm.ShowFolder mFolderKey
  Unload fForm
  FolderUtil.TreeViewLoadFolder tvFolders
  If Contains(tvFolders.Nodes, mFolderKey) Then
    tvFolders.Nodes(mFolderKey).Selected = True
  End If
End Sub

Private Sub mnuToolsWorkflowOptions_Click(Index As Integer)
  FolderUtil.WorkflowShow mFolderKey, Index
End Sub

'
' Choose Columns.  Public because it is called from popup menu.
'
Public Sub mnuViewChooseColumns_Click()
  If ListViewModule.ColumnSettings(lvView, mView.DataFinder.DataProvider.Source, lvView.Tag) Then
    LoadView
    lvView.Visible = True
  End If
End Sub

Private Sub mnuViewFolders_Click()
  mnuViewFolders.Checked = Not mnuViewFolders.Checked
  Form_Resize
  picView_Resize
  IniMain.WriteFile "Main", "ViewFolders", mnuViewFolders.Checked
End Sub

Private Sub mnuViewGoToBack_Click()
  mSequence.ListIndex = mSequence.ListIndex + 1
  FolderKey = mSequence.Selected
  SequenceRefresh
End Sub

Private Sub mnuViewGoToForward_Click()
  mSequence.ListIndex = mSequence.ListIndex - 1
  FolderKey = mSequence.Selected
  SequenceRefresh
End Sub

Private Sub mnuViewGoToFolder_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowGoTo(mFolderKey) Then
    FolderLoad fForm.FolderKey
  End If
  Unload fForm
End Sub

Private Sub mnuViewGoToHome_Click()
  If mSequence Is Nothing Then
    Exit Sub
  End If
    
  Dim Count As Integer
  Count = mSequence.ListCount
  If Count > 0 Then
    mSequence.ListIndex = mSequence.ListCount - 1
    FolderKey = mSequence.Selected
    SequenceRefresh
  End If
End Sub

Private Sub mnuViewGoToStartMenu_Click()
  tbrStart.Buttons(1).Value = tbrPressed
'  PopupMenu GotoMenu.mnuGoto, , cbrCoolBar.Left + picStart.Left + 24, _
'        cbrCoolBar.Top + picStart.Top + cbrCoolBar.Bands(2).Height + 12
  tbrStart.Buttons(1).Value = tbrUnpressed
End Sub

'
' Refresh - Public because called from popup
'
Public Sub mnuViewRefresh_Click()
  If Not mView Is Nothing And ActiveControl Is tvFolders Then
    With mView
      .PopupMenuName = IniFolder.ReadFile(.Key, "popup")
      .DefaultMenuName = IniFolder.ReadFile(.Key, "popup.default")
      .OpenMenuName = IniFolder.ReadFile(.Key, "popup.open")
      .ExploreMenuName = IniFolder.ReadFile(.Key, "popup.explore")
      .NewItemName = IniFolder.ReadFile(.Key, "newitem")
    End With
  End If
  SearchNow
End Sub

'
' Serialize column headers
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView, lvView.Tag
End Sub

Private Sub mnuViewStatusBar_Click()
  mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
  Form_Resize
  IniMain.WriteFile "Main", "ViewStatusbar", mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  Form_Resize
  IniMain.WriteFile "Main", "ViewToolbar", mnuViewToolbar.Checked
End Sub

Private Sub mnuViewWebBrowser_Click()
  Browse HttpServer
End Sub


Private Sub picListView_KeyDown(KeyCode As Integer, Shift As Integer)
  lvView_KeyDown KeyCode, Shift
End Sub


Private Sub picListView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    MenuSubItem picListView.Left + X, picListView.Top + Y
  End If
End Sub

Private Sub picFolders_Resize()
  ' Do not resize when minimized
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  tvFolders.Height = picFolders.ScaleHeight
  tvFolders.Width = picFolders.ScaleWidth
End Sub


Private Sub picListView_Resize()
  ' Do not resize when minimized
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  lvView.Left = IIf(LocaleEnableWebContent = True, 1500, 0)
  
  lvView.Height = Max(picListView.ScaleHeight - lvView.Top, 0)
  lvView.Width = Max(picListView.ScaleWidth - lvView.Left, 0)
  
  picSearchStatus.Top = lvView.Top + IIf(lvView.View = lvwReport, 252, 0)
  picSearchStatus.Left = lvView.Left
  picSearchStatus.Width = lvView.Width
End Sub

Private Sub picRemoveFolders_Click()
  mnuViewFolders_Click
End Sub

Private Sub picStart_Resize()
  tbrStart.Top = (picStart.Height - tbrStart.Height) / 2
End Sub

Private Sub picToolBar_Resize()
  tbToolBar.Top = (picToolBar.Height - tbToolBar.Height) / 2
End Sub

Private Sub picView_Resize()
  ' Do not resize on minimize
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If
  
  Dim BodyLeft As Integer
  If fraFolders.Visible Then
    fraFolders.Height = picView.Height
    picFolders.Height = Max(picView.Height - picFolders.Top, 0)
    imgSplitter.Left = fraFolders.Width
    imgSplitter.Height = picView.Height
    BodyLeft = imgSplitter.Left + imgSplitter.Width
  Else
    BodyLeft = 0
  End If
  
  Select Case mArrangement
  Case faMain
      picMain.Top = 96
      picMain.Left = BodyLeft
      picMain.Height = Max(picView.Height - picMain.Top, 0)
      picMain.Width = Max(picView.Width - picMain.Left, 0)
      fraLookFor.Visible = False
    Case faListView
      If fraSearch.Visible Then
        fraSearch.Left = BodyLeft
        fraSearch.Width = Max(picView.Width - fraSearch.Left, 0)
        picWords.Width = fraSearch.Width
        fraLookFor.Left = 24
        fraLookFor.Top = cmdSearchNow.Top
        fraLookFor.Visible = True
      Else
        fraLookFor.Visible = False
      End If
      
      picListView.Top = picView.Top + IIf(fraSearch.Visible, fraSearch.Height + 24, 96)
      picListView.Left = picView.Left + BodyLeft
      picListView.Width = Max(picView.Width - BodyLeft, 0)
      picListView.Height = Max(picView.Top + picView.Height - picListView.Top, 0)
  End Select

End Sub



Private Sub tbrStart_ButtonClick(ByVal Button As MSComctlLib.Button)
  cmdMenu_Click
End Sub

'
' This is a rare case where the ToolBar is the master for functionality.
' Usually, menus contain the functionality
'
Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "New"
            'ToDo: Add 'New' button code.
            MsgBox "Add 'New' button code."
        Case "Open"
            
        Case "Print"
            PrintUtil.PrintListView lvView, SelectedCount(lvView.hWnd) > 0
        Case "Move"
            
        Case "Delete"
            
        Case "lvMode0"
            lvView.View = lvwIcon
            picListView_Resize
        Case "lvMode1"
            lvView.View = lvwSmallIcon
            picListView_Resize
        Case "lvMode2"
            lvView.View = lvwList
            picListView_Resize
        Case "lvMode3"
            lvView.View = lvwReport
            picListView_Resize
    End Select
End Sub


Private Sub tmrFolderList_Timer()
  tmrFolderList.Enabled = False
  If Not tvFolders.SelectedItem Is Nothing Then
    FolderKey = tvFolders.SelectedItem.Key
  End If
End Sub

Private Sub tmrSearch_Timer()
  tmrSearch.Enabled = False
  If Len(txtWords.Text) > 2 And lastSearch <> txtWords.Text Then
    cmdSearchNow_Click
  End If
End Sub

'
' Called after label edit AND the label was changed.
'
Private Sub tvFolders_AfterLabelEdit(Cancel As Integer, NewString As String)
  If Not DirNameSafe(NewString) Then
    Cancel = True
    Exit Sub
  End If
  Cancel = Not Factory.FolderService.RenameFolder(tvFolders.SelectedItem.Key, NewString)
End Sub

Private Sub tvFolders_Expand(ByVal Node As MSComctlLib.Node)
  If Node.Child.Text <> vbNullString Then
    Exit Sub
  End If
  
  tvFolders.Nodes.Remove Node.Child.Index
  PopulateChildren tvFolders, Node, RemotePath(Node.FullPath)
End Sub

Private Sub tvFolders_GotFocus()
  mnuFileOpenMenu.Visible = False
  mnuFileExploreMenu.Visible = False
  mnuFileBar1.Visible = False
  mnuFileSendTo.Visible = False
  
  mnuFileDelete.Visible = True
  mnuFileRename.Visible = True
  mnuFileWorkflow(0).Visible = False
  mnuFileWorkflow(1).Visible = False
  mnuFileProperties.Enabled = True
End Sub

Private Sub tvFolders_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF10 And (Shift And vbShiftMask) > 0 Then
    tmrFolderList_Timer
'    PopupMenu Popup.mnuPopupFolderList, , picFolders.Left + picView.Left + 24, picFolders.Top + picView.Top + 24
    KeyCode = 0
  End If
  
  'Trap F6 for folders w/DataFinder.
  If KeyCode = vbKeyF6 Then
    tmrFolderList_Timer
    If (Shift And vbShiftMask) > 0 Then
      If picListView.Visible Then
        lvView.SetFocus
      End If
    Else
      If fraSearch.Visible Then
        txtWords.SetFocus
      ElseIf picListView.Visible Then
        lvView.SetFocus
      End If
    End If
  End If
End Sub

Private Sub tvFolders_LostFocus()
  Set mLastControl = tvFolders
  mnuFileDelete.Visible = False
  mnuFileRename.Visible = False
  mnuFileProperties.Enabled = False
  
  'MsgBox ActiveControl.Name
End Sub

Private Sub tvFolders_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  tmrFolderList_Timer
  If Button = vbRightButton Then
    PopupMenu mnuFolder
  End If
End Sub
  
'
' Reset timer on any node click
'
Private Sub tvFolders_NodeClick(ByVal Node As MSComctlLib.Node)
  tmrFolderList.Enabled = False
  tmrFolderList.Enabled = True
End Sub


Private Sub txtWords_Click()
  cmdSearchNow.Default = True
End Sub

Private Sub txtWords_DropDown()
  tmrSearch.Enabled = False
End Sub

Private Sub txtWords_GotFocus()
  ComboBoxUtil.Deserialize txtWords, cboIn.Text, IniSearch, mWordCache
  cmdSearchNow.Default = True
End Sub

Private Sub txtWords_KeyDown(KeyCode As Integer, Shift As Integer)
  If ComboBoxUtil.IsDropped(txtWords) Then
    Exit Sub
  End If
  
  If (KeyCode = vbKeyDown Or KeyCode = vbKeyUp) And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    SendMessage lvView.hWnd, WM_KEYDOWN, KeyCode, 0
    KeyCode = 0
    Exit Sub
  End If

  'Trap F6
  If View.DataFinder Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub

Private Sub txtWords_KeyPress(KeyAscii As Integer)
  If ComboBoxUtil.IsDropped(txtWords) Then
    Exit Sub
  End If
  
  tmrSearch.Enabled = False
  tmrSearch.Enabled = True
  cmdSearchNow.Default = True
End Sub
