VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IniFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"IniFile"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'// Private member that holds a reference to
'// the path of our ini file
Private mFileName As String

'// Win API Declares
Private Declare Function WritePrivateProfileString _
  Lib "kernel32" Alias "WritePrivateProfileStringA" _
 (ByVal lpApplicationName As String, _
  ByVal lpKeyName As Any, _
  ByVal lpString As Any, _
  ByVal lpFileName As String) As Long

Private Declare Function GetPrivateProfileString _
  Lib "kernel32" Alias "GetPrivateProfileStringA" _
 (ByVal lpApplicationName As String, _
  ByVal lpKeyName As Any, _
  ByVal lpDefault As String, _
  ByVal lpReturnedString As String, _
  ByVal nSize As Long, _
  ByVal lpFileName As String) As Long


Private Function MakePath(ByVal strDrv As String, _
  ByVal strDir As String) As String
 
 '// Makes an INI file: Guarantees a sub dir
 Do While Right$(strDrv, 1) = "\"
   strDrv = Left$(strDrv, Len(strDrv) - 1)
 Loop

 Do While Left$(strDir, 1) = "\"
   strDir = Mid$(strDir, 2)
 Loop

 '// Return the path
 MakePath = strDrv & "\" & strDir
End Function


Public Sub CreateIni(strDrv As String, strDir As String)
 '// Make a new ini file
 mFileName = MakePath(strDrv, strDir)
End Sub


Public Sub WriteFile(strSection As String, _
 strKey As String, _
 strValue As String)
 
 '// Write to mFileName
 WritePrivateProfileString strSection, _
   strKey, strValue, mFileName
End Sub


Public Sub RemoveSection(strSection As String)
  WritePrivateProfileString strSection, 0&, 0&, mFileName
End Sub


Public Sub RemoveKey(strSection As String, strKey As String)
  WritePrivateProfileString strSection, strKey, 0&, mFileName
End Sub


Public Function ReadFile(strSection As String, _
  strKey As String) As String

  Dim strTmp As String
  Dim lngRet As String

  strTmp = Space(1024)
  lngRet = GetPrivateProfileString(strSection, _
    strKey, "", strTmp, _
    Len(strTmp), mFileName)

  ReadFile = Left(strTmp, lngRet)
End Function


Public Property Let FileName(ByVal vData As String)
  mFileName = vData
End Property


Public Property Get FileName() As String
  FileName = mFileName
End Property



