VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmGenericSearch 
   ClientHeight    =   7032
   ClientLeft      =   156
   ClientTop       =   696
   ClientWidth     =   8412
   Icon            =   "frmGenericSearch.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7032
   ScaleWidth      =   8412
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picButtons 
      BorderStyle     =   0  'None
      Height          =   456
      Left            =   2148
      ScaleHeight     =   456
      ScaleWidth      =   4068
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   5112
      Width           =   4068
      Begin VB.CommandButton cmdNew 
         Caption         =   "&New"
         Height          =   336
         Left            =   1380
         TabIndex        =   19
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2640
         TabIndex        =   18
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Enabled         =   0   'False
         Height          =   336
         Left            =   96
         TabIndex        =   17
         Top             =   60
         Width           =   1200
      End
   End
   Begin VB.PictureBox picSelect 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   492
      Left            =   0
      ScaleHeight     =   492
      ScaleMode       =   0  'User
      ScaleWidth      =   6764.775
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   4680
      Width           =   8412
   End
   Begin VB.Timer tmrSearch 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   6732
      Top             =   5292
   End
   Begin VB.PictureBox picListView 
      Align           =   1  'Align Top
      BackColor       =   &H80000005&
      Height          =   1392
      Left            =   0
      ScaleHeight     =   1344
      ScaleWidth      =   8364
      TabIndex        =   9
      TabStop         =   0   'False
      Tag             =   "Firm"
      Top             =   3288
      Width           =   8412
      Begin VB.PictureBox picSearchStatus 
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         Height          =   240
         Left            =   1704
         ScaleHeight     =   240
         ScaleWidth      =   4992
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   4992
         Begin VB.Label lblSearchStatus 
            AutoSize        =   -1  'True
            BackColor       =   &H80000009&
            BackStyle       =   0  'Transparent
            Caption         =   "Search Status"
            Height          =   192
            Left            =   0
            TabIndex        =   13
            Top             =   0
            Width           =   996
         End
      End
      Begin MSComctlLib.ListView lvView 
         Height          =   4152
         Left            =   1704
         TabIndex        =   4
         Top             =   12
         Width           =   5856
         _ExtentX        =   10329
         _ExtentY        =   7324
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         _Version        =   393217
         Icons           =   "imlIcons"
         SmallIcons      =   "imlSmallIcons"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.Label lblListView 
         BackColor       =   &H80000005&
         Caption         =   "Double click or press Enter on an item to view or edit the information. "
         Height          =   1572
         Index           =   1
         Left            =   84
         TabIndex        =   11
         Top             =   732
         Width           =   1212
      End
      Begin VB.Label lblListView 
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "Table Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   84
         TabIndex        =   10
         Top             =   0
         Width           =   1524
         WordWrap        =   -1  'True
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   264
      Left            =   0
      TabIndex        =   6
      Top             =   6768
      Width           =   8412
      _ExtentX        =   14838
      _ExtentY        =   466
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7281
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "4/9/2002"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "1:01 AM"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   7344
      Top             =   5100
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":000C
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":011E
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0230
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0342
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0454
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0566
            Key             =   "View Large Icons"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0678
            Key             =   "View Small Icons"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":078A
            Key             =   "View List"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":089C
            Key             =   "View Details"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":09AE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlIcons 
      Left            =   7176
      Top             =   5700
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0D0A
            Key             =   "book"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlSmallIcons 
      Left            =   10836
      Top             =   8364
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   13
      ImageHeight     =   13
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":1024
            Key             =   "closed"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":1196
            Key             =   "cylinder"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":1308
            Key             =   "leaf"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":147A
            Key             =   "open"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":15EC
            Key             =   "smlBook"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":189E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picSearchTab 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3000
      Left            =   0
      ScaleHeight     =   3000
      ScaleWidth      =   8412
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   288
      Width           =   8412
      Begin VB.PictureBox picSearchButtons 
         BorderStyle     =   0  'None
         Height          =   2244
         Left            =   6336
         ScaleHeight     =   2244
         ScaleWidth      =   1380
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   312
         Width           =   1380
         Begin VB.CommandButton cmdSearchNow 
            Caption         =   "&Search Now"
            Default         =   -1  'True
            Height          =   336
            Left            =   96
            TabIndex        =   2
            Top             =   36
            Width           =   1080
         End
         Begin VB.CommandButton cmdNewSearch 
            Caption         =   "New Search"
            Height          =   336
            Left            =   96
            TabIndex        =   3
            Top             =   444
            Width           =   1080
         End
      End
      Begin TabDlg.SSTab sstabSearch 
         Height          =   2856
         Left            =   60
         TabIndex        =   5
         Top             =   84
         Width           =   6276
         _ExtentX        =   11070
         _ExtentY        =   5038
         _Version        =   393216
         Style           =   1
         Tab             =   1
         TabsPerRow      =   5
         TabHeight       =   420
         TabCaption(0)   =   "Generic"
         TabPicture(0)   =   "frmGenericSearch.frx":1B50
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Label3"
         Tab(0).Control(1)=   "Label1"
         Tab(0).Control(2)=   "txtWords"
         Tab(0).Control(3)=   "cboIn"
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Advanced"
         TabPicture(1)   =   "frmGenericSearch.frx":1B6C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "lblTortfeasor"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "chkClaimName"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "chkClaimant"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "chkTortfeasor"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "txtClaimName"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "txtClaimant"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).Control(6)=   "cmdTortfeasor"
         Tab(1).Control(6).Enabled=   0   'False
         Tab(1).Control(7)=   "Frame1"
         Tab(1).Control(7).Enabled=   0   'False
         Tab(1).Control(8)=   "Frame2"
         Tab(1).Control(8).Enabled=   0   'False
         Tab(1).Control(9)=   "Frame3"
         Tab(1).Control(9).Enabled=   0   'False
         Tab(1).ControlCount=   10
         TabCaption(2)   =   "Advanced"
         TabPicture(2)   =   "frmGenericSearch.frx":1B88
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "chkPolicyIncludeVoid"
         Tab(2).ControlCount=   1
         Begin VB.CheckBox chkPolicyIncludeVoid 
            Caption         =   "Include Void"
            Height          =   192
            Left            =   -74796
            TabIndex        =   44
            Top             =   444
            Value           =   1  'Checked
            Width           =   1308
         End
         Begin VB.Frame Frame3 
            Height          =   1248
            Left            =   1488
            TabIndex        =   38
            Top             =   1416
            Width           =   1920
            Begin VB.CheckBox chkDate 
               Caption         =   "Date Range:"
               Height          =   264
               Left            =   96
               TabIndex        =   43
               Top             =   144
               Width           =   1524
            End
            Begin MSComCtl2.DTPicker dtpFrom 
               Height          =   288
               Left            =   600
               TabIndex        =   39
               Top             =   420
               Width           =   1164
               _ExtentX        =   2053
               _ExtentY        =   508
               _Version        =   393216
               Enabled         =   0   'False
               Format          =   24576001
               CurrentDate     =   37272
            End
            Begin MSComCtl2.DTPicker dtpTo 
               Height          =   288
               Left            =   600
               TabIndex        =   40
               Top             =   744
               Width           =   1164
               _ExtentX        =   2053
               _ExtentY        =   508
               _Version        =   393216
               Enabled         =   0   'False
               Format          =   24576001
               CurrentDate     =   37272
            End
            Begin VB.Label Label4 
               Caption         =   "To:"
               Height          =   192
               Left            =   96
               TabIndex        =   42
               Top             =   792
               Width           =   348
            End
            Begin VB.Label Label2 
               Caption         =   "From:"
               Height          =   192
               Left            =   96
               TabIndex        =   41
               Top             =   468
               Width           =   432
            End
         End
         Begin VB.Frame Frame2 
            Height          =   972
            Left            =   3540
            TabIndex        =   30
            Top             =   1416
            Width           =   1392
            Begin VB.CheckBox chkACA 
               Caption         =   "Adjuster:"
               Height          =   240
               Left            =   72
               TabIndex        =   31
               Top             =   144
               Width           =   1116
            End
            Begin VB.OptionButton optAdjuster 
               Caption         =   "Sally"
               Enabled         =   0   'False
               Height          =   240
               Index           =   0
               Left            =   72
               TabIndex        =   32
               Top             =   408
               Width           =   936
            End
            Begin VB.OptionButton optAdjuster 
               Caption         =   "Katja"
               Enabled         =   0   'False
               Height          =   240
               Index           =   1
               Left            =   72
               TabIndex        =   34
               Top             =   672
               Width           =   936
            End
         End
         Begin VB.Frame Frame1 
            Height          =   1248
            Left            =   72
            TabIndex        =   29
            Top             =   1416
            Width           =   1260
            Begin VB.CheckBox chkStatus 
               Caption         =   "Status:"
               Height          =   264
               Left            =   108
               TabIndex        =   33
               Top             =   144
               Width           =   960
            End
            Begin VB.OptionButton optStatus 
               Caption         =   "&Open"
               Enabled         =   0   'False
               Height          =   276
               Index           =   0
               Left            =   108
               TabIndex        =   35
               Top             =   408
               Width           =   1092
            End
            Begin VB.OptionButton optStatus 
               Caption         =   "&Close"
               Enabled         =   0   'False
               Height          =   276
               Index           =   1
               Left            =   108
               TabIndex        =   36
               Top             =   672
               Width           =   1092
            End
            Begin VB.OptionButton optStatus 
               Caption         =   "&Re-Open"
               Enabled         =   0   'False
               Height          =   276
               Index           =   2
               Left            =   108
               TabIndex        =   37
               Top             =   924
               Width           =   1092
            End
         End
         Begin VB.CommandButton cmdTortfeasor 
            Caption         =   "&Tortfeasor"
            Height          =   300
            Left            =   1380
            TabIndex        =   27
            Top             =   1044
            Width           =   1164
         End
         Begin VB.TextBox txtClaimant 
            Height          =   288
            Left            =   1392
            TabIndex        =   26
            Top             =   672
            Width           =   4764
         End
         Begin VB.TextBox txtClaimName 
            Height          =   288
            Left            =   1392
            TabIndex        =   25
            Top             =   324
            Width           =   4764
         End
         Begin VB.CheckBox chkTortfeasor 
            Caption         =   "Tortfeasor:"
            Height          =   192
            Left            =   84
            TabIndex        =   24
            Top             =   1098
            Width           =   1272
         End
         Begin VB.CheckBox chkClaimant 
            Caption         =   "Claimant:"
            Height          =   192
            Left            =   84
            TabIndex        =   23
            Top             =   720
            Width           =   1272
         End
         Begin VB.CheckBox chkClaimName 
            Caption         =   "Claim Name:"
            Height          =   192
            Left            =   84
            TabIndex        =   22
            Top             =   372
            Width           =   1272
         End
         Begin VB.ComboBox cboIn 
            Height          =   288
            ItemData        =   "frmGenericSearch.frx":1BA4
            Left            =   -73428
            List            =   "frmGenericSearch.frx":1BA6
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Tag             =   "Firm"
            Top             =   720
            Width           =   2160
         End
         Begin VB.TextBox txtWords 
            Height          =   300
            Left            =   -73428
            TabIndex        =   0
            Top             =   372
            Width           =   2148
         End
         Begin VB.Label lblTortfeasor 
            Height          =   204
            Left            =   2688
            TabIndex        =   28
            Top             =   1092
            Width           =   3420
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Search f&or:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   21
            Top             =   468
            Width           =   768
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Search in:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   20
            Top             =   768
            Width           =   708
         End
      End
   End
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   288
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   8412
      _ExtentX        =   14838
      _ExtentY        =   508
      ButtonWidth     =   487
      ButtonHeight    =   466
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            Object.ToolTipText     =   "New"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Open"
            Object.ToolTipText     =   "Open"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Print"
            Object.ToolTipText     =   "Print"
            ImageKey        =   "Print"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Move"
            Object.ToolTipText     =   "Move to Folder"
            ImageKey        =   "Copy"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Delete"
            Object.ToolTipText     =   "Delete"
            ImageKey        =   "Delete"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode0"
            Object.ToolTipText     =   "View Large Icons"
            ImageKey        =   "View Large Icons"
            Style           =   2
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode1"
            Object.ToolTipText     =   "View Small Icons"
            ImageKey        =   "View Small Icons"
            Style           =   2
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode2"
            Object.ToolTipText     =   "View List"
            ImageKey        =   "View List"
            Style           =   2
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode3"
            Object.ToolTipText     =   "View Details"
            ImageKey        =   "View Details"
            Style           =   2
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "mnuFileOpen"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFilePrintSetup 
         Caption         =   "Print Set&up"
      End
      Begin VB.Menu mnuFileBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "Propert&ies..."
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditCut 
         Caption         =   "Cu&t"
         Enabled         =   0   'False
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "&Paste"
         Enabled         =   0   'False
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move..."
         Enabled         =   0   'False
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuEditBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditRotate 
         Caption         =   "Rotate"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSeparator8261829 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditSelectAll 
         Caption         =   "Select &All"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuEditInvertSelection 
         Caption         =   "&Invert Selection"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "Status &Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "Lar&ge Icons"
         Index           =   0
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "S&mall Icons"
         Index           =   1
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&List"
         Index           =   2
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&Details"
         Index           =   3
      End
      Begin VB.Menu mnuViewBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewArrangeIcons 
         Caption         =   "Arrange &Icons"
         Begin VB.Menu mnuArrangeIcon 
            Caption         =   "Columns"
            Index           =   0
         End
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuViewBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsOptions 
         Caption         =   "&Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
      End
      Begin VB.Menu mnuControl 
         Caption         =   "&Control"
         Shortcut        =   ^{F12}
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "&Search For Help On..."
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Explorer..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "&Popups"
      Begin VB.Menu mnuListItem 
         Caption         =   "mnuListItem"
         Begin VB.Menu mnuListItemSelect 
            Caption         =   "Select"
         End
         Begin VB.Menu mnuListItemOpen 
            Caption         =   "mnuListItemOpen"
            Index           =   0
         End
         Begin VB.Menu mnuSeparator3421 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuListItemSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuSeparator783 
            Caption         =   "-"
         End
         Begin VB.Menu mnuListItemProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuSubItem 
         Caption         =   "mnuSubItem"
         Begin VB.Menu mnuSubItemView 
            Caption         =   "&View"
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "Lar&ge Icons"
               Index           =   0
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "S&mall Icons"
               Index           =   1
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "&List"
               Index           =   2
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "&Details"
               Index           =   3
            End
         End
         Begin VB.Menu mnuSeparator33 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemArrangeIcons 
            Caption         =   "Arrange &Icons"
            Begin VB.Menu mnuSubItemArrangeIcon 
               Caption         =   "mnuListViewArrangeIcon"
               Index           =   0
            End
         End
         Begin VB.Menu mnuSubItemRefresh 
            Caption         =   "R&efresh"
         End
         Begin VB.Menu mnuSeparator95 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemChooseColumns 
            Caption         =   "Choose Colum&ns..."
         End
         Begin VB.Menu mnuSeparator592 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemNew 
            Caption         =   "Ne&w"
         End
         Begin VB.Menu mnuSeparator343 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemProperties 
            Caption         =   "P&roperties"
         End
      End
   End
End
Attribute VB_Name = "frmGenericSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
' Main Form
'
'
' The main form can be one of 3 form types set by the the following
' global constants from MainModule:
'
' ftView - Folder list and main window
' ftAdvancedSearch - Look in list, advanced search and a list view
' ftSelect - Advanced search and list view pinned to a folder
'
'
'

Option Explicit

' Public manager is needed by popup menus.
Public FormMgr As New FormManager

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

'local variable(s) to hold property value(s)
Private mFolderKey As String
Private mView As View
Private mIsSearchOnly As Boolean

Private lastSearch As String
Private lastCount As Integer

Private mbMoving As Boolean
Const sglSplitLimit = 1000
Private isLoaded As Boolean

' ToDo: Why is this Public?
Public LawArea As Collection
Public TxtWordsIndex As Integer

Private mPopup(1) As Form
Private mDefaultMenu As Menu
Private mUnloaded As Boolean


'
' Start Generic Section
'
' Generic code is copied from frmGenericSearch.  In an object
' oriented world, this would be included in an ancestor.
'
'

'
' If a child is unloaded, find last form of same name.  Load that first, then
' load the main form.
'
Public Sub FormChildUnload(ByRef vData As FormInterface)
  Select Case vData.Message
    Case fimSelect
      lblTortfeasor.Caption = ListItemText(vData.Child.lvView.SelectedItem, "lawyerId")
    Case fimNew
      MsgBox "NEW"
    Case fimNone
      MsgBox "NONE"
  End Select
      
  Dim LastForm As Form
  Set LastForm = FindLastForm(vData.Child)
  If LastForm Is Nothing Then
    Show
    Refresh
  Else
    LastForm.Show
  End If
End Sub

'
' Called when this form is loaded from a Form Manager
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub


'
' Do nothing if a parent form is unloaded
'
Public Sub FormParentUnload()
End Sub


' Refresh folder

Public Sub MenuListItem(Optional X, Optional Y)
  MenuViewRefresh
  
  On Error Resume Next
  PopupMenu mnuListItem, , X, Y, mDefaultMenu
  On Error GoTo 0
  
  ' Either default menu doesn't exist or control array doesn't exist, or the default
  ' menu is a sub menu.
  If Err = 340 Or Err = 427 Or Err = 490 Then
    PopupMenu mnuListItem, , X, Y
  End If
End Sub

Public Sub MenuSubItem(Optional X, Optional Y)
  ' Eventually add Paste functionality here.
  PopupMenu mnuSubItem, , X, Y
End Sub


Public Sub MenuViewRefresh()
  Dim Count As Integer, IsCount, Selected As Integer, IsSelected, Workflow As Boolean
  Count = lvView.ListItems.Count
  IsCount = Count > 0
  Selected = ListViewModule.SelectedCount(lvView.hWnd)
  IsSelected = Selected > 0
  Workflow = mView.IsWorkflow
  
  
  ' set arrange check
  If (lvView.ColumnHeaders.Count > 0) Then
'    mnuArrangeIcon(lvView.SortKey + 1).Checked = True
'    Popup.mnuListViewArrangeIcon(lvView.SortKey + 1).Checked = True
  End If
  
  ' If an item selected, copy over popup open and explore.
  If IsSelected Then
    sbStatusBar.Panels(1).Text = Selected & " of " & Count & " object(s) selected"
    
    ' Copy over Open and Explore
    FormUtil.MenuCopy mView.OpenMenu, mnuFileOpen
    FormUtil.MenuCopy mView.OpenMenu, mnuListItemOpen
  Else
    sbStatusBar.Panels(1).Text = Count & " object(s)"
  End If
  
  mnuFileOpenMenu.Enabled = IsSelected And Not mnuFileOpen(0).Visible
'  mnuFilePrint.Enabled = IsCount
'  mnuFileExport.Enabled = IsSelected
  mnuFileSendToFolder.Enabled = IsSelected And Not Workflow
    
  mnuEditCut.Enabled = IsSelected And Workflow
  mnuEditPaste.Enabled = Not EventBuffer Is Nothing And Workflow
  mnuEditMove.Enabled = IsSelected And Workflow
  
  mnuEditInvertSelection.Enabled = IsCount
  mnuEditSelectAll.Enabled = IsCount
  
  cmdOK.Enabled = IsSelected
  If IsSelected Then
    cmdOK.Default = True
  Else
    cmdSearchNow.Default = True
  End If
  
  If (mIsSearchOnly) Then
    Set mDefaultMenu = mnuListItemOpen(mView.DefaultMenuIndex)
  Else
    Set mDefaultMenu = mnuListItemSelect
  End If
End Sub


Private Sub SetArrangeMenu()
End Sub


Public Sub LoadView()
  ' Setup list view for searching.
  lvView.ListItems.Clear
  picListView_Resize
  lblSearchStatus.Caption = "Enter your search criteria to begin."
  picSearchStatus.Visible = True
End Sub


Public Sub SearchNow()
  ' Show hourglass
  Dim Pointer As New WaitPointer
  Pointer.Pointer = vbHourglass
  
  lvView.ListItems.Clear
  lblSearchStatus.Caption = "Searching..."
  picSearchStatus.Visible = True
  lblSearchStatus.Refresh
  
  'lastSearch is used for tmrSearch
  lastSearch = txtWords.Text
  
  'Setup column, sort and find
  If lvView.ColumnHeaders.Count > 0 Then
    mView.DataFinder.DataProvider.Column = LawSQL.Column(lvView.ColumnHeaders)
    mView.DataFinder.DataProvider.Sort = LawSQL.Sort(lvView)
  End If
  
  mView.DataFinder.FindByForm Me
  
  SearchResult
  MenuViewRefresh
End Sub

Public Sub SearchResult()
  If lvView.ListItems.Count = 0 Then
    lblSearchStatus.Caption = "Search is complete.  There are no results to display."
    picSearchStatus.Visible = True
  Else
    picSearchStatus.Visible = False
  End If
  MenuViewRefresh
  RefreshClaim
  SetArrangeMenu
End Sub

Public Sub ShowSearch(FolderKey As String, Optional IsSelect As Boolean, Optional TabIndex As Integer, Optional SearchInIndex As Integer, Optional SearchText As String)
  mFolderKey = FolderKey
  mIsSearchOnly = Not IsSelect
  Show
  sstabSearch.Tab = TabIndex
  
  If Not SearchInIndex = -1 Then
    If Not cboIn.ListCount < SearchInIndex - 1 Then
      cboIn.ListIndex = SearchInIndex
    End If
  End If
  txtWords = SearchText
End Sub

Public Sub ShowAdvancedSearch(FolderKey As String)
  mFolderKey = FolderKey
  mIsSearchOnly = True
  Show
End Sub

Public Sub ShowSelect(FolderKey As String)
  mFolderKey = FolderKey
  Show
End Sub
'
' End General Methods
'
'
'
'
' Control Methods
'

Private Sub chkACA_Click()
  Dim Enabled As Boolean
  Enabled = chkACA.Value
  optAdjuster(0).Enabled = Enabled
  optAdjuster(1).Enabled = Enabled
  
  RefreshClaim
End Sub

Private Sub chkPolicyExcludeVoid_Click()

End Sub

Private Sub chkStatus_Click()
  RefreshClaim
End Sub

Private Sub chkDate_Click()
  RefreshClaim
End Sub

Private Sub chkTortfeasor_Click()
  RefreshClaim
End Sub

Private Sub chkClaimant_Click()
  RefreshClaim
End Sub

Private Sub chkClaimName_Click()
  RefreshClaim
End Sub

Private Sub cmdClose_Click()
  mnuFileExit_Click
End Sub

Private Sub cmdNew_Click()
  mnuFileNew_Click
End Sub

Private Sub cmdNewSearch_Click()
  txtWords.Text = ""
  lvView.ListItems.Clear
  lblSearchStatus.Caption = "Enter your search criteria to begin."
End Sub

Private Sub cmdOK_Click()
  If mIsSearchOnly Then
    MenuViewRefresh
    CallByName Me, mDefaultMenu.Name & "_Click", VbMethod, mDefaultMenu.Index
  Else
    FormMgr.Message = fimSelect
    Unload Me
  End If
End Sub

Public Sub cmdSearchNow_Click()
  mnuViewRefresh_Click
End Sub

Private Sub cmdTortfeasor_Click()
  Dispatcher.ShowSearch DatabaseName.LAWYER_TABLE, FormMgr, True
End Sub

Private Sub Form_Load()
  If mUnloaded Then
    MsgBox Name & ": Illegal Load Attempt.  Please report."
    Unload Me
    Exit Sub
  End If
  
  ' Load form manager.
  FormMgr.Load Me
   
  sstabSearch.TabVisible(1) = (mFolderKey = "Claim")
  sstabSearch.TabVisible(2) = (mFolderKey = "Policy")
  If sstabSearch.TabVisible(1) Then
    dtpFrom.Value = Now() - 30
    dtpTo.Value = Now()
  End If
   
  ' Setup child forms.
'  Set mPopup(0) = New frmPopup
'  Load mPopup(0)
'  FormMgr.Add mPopup(0)
'
'  Set mPopup(1) = New frmPopup2
'  Load mPopup(1)
'  FormMgr.Add mPopup(1)
  
  ' Setup view.
  Set mView = New View
  If mFolderKey = DatabaseName.APPLICATIONRATING_TABLE Then
    FolderUtil.ViewLoad mView, mFolderKey, mPopup, mFolderKey & "View"
  Else
    FolderUtil.ViewLoad mView, mFolderKey, mPopup, mFolderKey
  End If
    
  Caption = IIf(mIsSearchOnly, "Search for a ", "Select a ") & mFolderKey
  sstabSearch.Caption = mFolderKey
  
  ' Open DataFinder if necessary.
  sbStatusBar.Panels(2).Text = mView.DataFinder.DataProvider.Source
    
  lvView.Tag = mFolderKey & IIf(mIsSearchOnly, "Search", "Select")
  lvView.ColumnHeaders.Clear
  lvView.ListItems.Clear
  ListViewModule.DeserializeColumnHeaders lvView, lvView.Tag
  mView.DataFinder.LoadSearch Me
  
  LoadView

  ' Set toolbar
  tbToolBar.Buttons("lvMode" & lvView.View).Value = tbrPressed
    
  ' Setup form
  If mIsSearchOnly Then
    picSelect.Visible = False
    picSelect.Enabled = False
    mnuListItemSelect.Visible = False
    cmdClose.Enabled = False
    picButtons.Left = -20000
  End If
  
  'Get view settings from ini file.
  mnuViewToolbar.Checked = IniMain.ReadFile("Main", "ViewToolbar")
  mnuViewStatusBar.Checked = IniMain.ReadFile("Main", "ViewStatusBar")
  
  ' Hide popup.
  mnuPopup.Visible = False
  
  ' SSTab Fix
  FormUtil.TabControlEnable sstabSearch
  
  chkStatus_Click
  
  ' position settings
  ' set defaults first
  Top = 0
  Left = 0
  Height = 7200
  Width = 9600
  FormUtil.DeserializePosition Me
  
  FormLoad
End Sub

Private Sub Form_Resize()
  ' Do not resize when minimized.
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  tbToolBar.Visible = mnuViewToolbar.Checked
  sbStatusBar.Visible = mnuViewStatusBar.Checked
  
  Dim BottomHeight As Integer
  BottomHeight = IIf(sbStatusBar.Visible, sbStatusBar.Height, 0)
  BottomHeight = BottomHeight + IIf(picSelect.Visible, picSelect.Height, 0)
  
  picListView.Height = Max(Me.ScaleHeight - BottomHeight - picListView.Top, 0)
  If Not mIsSearchOnly Then picButtons.Top = picSelect.Top
End Sub

'
' Uncomment MsgBox to test for proper form termination.
'
Private Sub Form_Terminate()
  'MsgBox "frmMain Terminate"
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  
  ' Close down form manager
  FormMgr.Unload
  
  ' Unload child forms
  Unload mPopup(0)
  Unload mPopup(1)
  
  mUnloaded = True
End Sub

Private Sub lblListView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  picListView_MouseUp Button, Shift, X, Y
End Sub

Public Sub lvView_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  Dim Pointer As New WaitPointer
  Pointer.Pointer = vbHourglass
  ListViewSort lvView, ColumnHeader, mView.DataFinder.DataProvider, True
End Sub

Private Sub lvView_DblClick()
  If cmdOK.Enabled Then cmdOK_Click
End Sub

Private Sub lvView_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF10 And (Shift And vbShiftMask) > 0 Then
    If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
      MenuSubItem picListView.Left + 24, picListView.Top + 24
    Else
      MenuListItem picListView.Left + 24, picListView.Top + 24
    End If
    KeyCode = 0
    Exit Sub
  End If
End Sub

Private Sub lvView_KeyUp(KeyCode As Integer, Shift As Integer)
  MenuViewRefresh
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView.hWnd, X, Y) And ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    lvView.SelectedItem.Selected = False
  End If
   If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
       MenuSubItem
     Else
       MenuListItem
     End If
  End If
  ' Don't call anything here in case form was unloaded in MenuSubItem.
End Sub

Private Sub lvView_GotFocus()
  If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    cmdOK.Default = True
  End If
End Sub

Private Sub mnuArrangeIcon_Click(Index As Integer)
  Call lvView_ColumnClick(lvView.ColumnHeaders(Index))
End Sub

Private Sub mnuControl_Click()
  MsgBox Me.ActiveControl.Name & " TabIndex: " & Me.ActiveControl.TabIndex
End Sub

Private Sub mnuEditInvertSelection_Click()
  Dim n, lvCount As Integer
  lvCount = lvView.ListItems.Count
  
  For n = 1 To lvCount
    If lvView.ListItems(n).Selected = True Then
      lvView.ListItems(n).Selected = False
    Else
      lvView.ListItems(n).Selected = True
    End If
  Next n
  MenuViewRefresh
End Sub

Private Sub mnuEditRotate_Click()
  cboIn.ListIndex = (cboIn.ListIndex + 1) Mod cboIn.ListCount
  txtWords.SetFocus
End Sub

Private Sub mnuEditSelectAll_Click()
  ListViewModule.SelectAll lvView.hWnd
  MenuViewRefresh
End Sub

Private Sub mnuFile_Click()
  mnuFilePrint.Enabled = (lvView.ListItems.Count > 0)
  If ActiveControl Is lvView Then
    mnuFileProperties.Tag = vbNullString
    If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
      mnuFileProperties.Tag = ListViewModule.ListItemText(lvView.SelectedItem, "eventId")
    End If
    mnuFileProperties.Enabled = mnuFileProperties.Tag <> vbNullString
  End If
  MenuViewRefresh
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNew_Click()
  ' The parent form should use the FormInterface.Message property
  ' to see if there is a request for a new record (fimNew).  See
  ' frmUnderwritingComment.FormChildUnload method.
  If mIsSearchOnly Then
    Dispatcher.ShowNew DatabaseName.FIRM_TABLE, , FormMgr
    Exit Sub
  End If
  
  FormMgr.Message = fimNew
  Unload Me
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  CallByName mView.PopupMenuForm, mView.OpenMenuName & "_Click", VbMethod, Index
End Sub

Private Sub mnuFilePrint_Click()
  PrintUtil.PrintDialog Me, lvView
End Sub

Private Sub mnuFilePrintSetup_Click()
  PrintUtil.PrintSetupDialog Me
End Sub

Private Sub mnuFileSendToFolder_Click()
'  Dim Source As String, Group As String, ReferenceId As String, EventName As String
'  Source = DatabaseName.FIRM_TABLE
'  Group = DatabaseName.FIRM_REF
'  ReferenceId = "firmId"
'  EventName = "firmName"
'
'  ' Create form and show.
'  Dim Form As New frmFolderBrowseDialog
'  If Not Form.ShowSendTo(Group) Then
'    Unload Form
'    Exit Sub
'  End If
'
'  '
'  ' Form is OK
'  '
'  Refresh
'  Dim Pointer As New WaitPointer
'  Pointer.Default
'
'  Call Factory.EventService.Create( _
'      Form.EventTypeId, _
'      Group, _
'      Form.FolderKey, _
'      Source, _
'      ListViewText(lvView, ReferenceId), _
'      ListViewText(lvView, EventName))
'
'  Unload Form
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Public Sub mnuListItemOpen_Click(Index As Integer)
  mnuFileOpen_Click Index
End Sub

Private Sub mnuListItemSelect_Click()
  cmdOK_Click
End Sub

Private Sub mnuListItemSendToFolder_Click()
  mnuFileSendToFolder_Click
End Sub

Private Sub mnuListViewMode_Click(Index As Integer)
  lvView.View = Index
  tbToolBar.Buttons("lvMode" & Index).Value = tbrPressed
End Sub

Private Sub mnuToolsOptions_Click()
  Dim fOptions As New frmMainOptionsDialog
  fOptions.Show vbModal, Me
  Unload fOptions
  picListView_Resize
End Sub

'
' Choose Columns.  Public because it is called from mPopup menu.
'
Public Sub mnuViewChooseColumns_Click()
  If ListViewModule.ColumnSettings(lvView, mView.DataFinder.DataProvider.Source, lvView.Tag) Then
    LoadView
    lvView.Visible = True
  End If
End Sub

'
' Refresh - Public because called from mPopup
'
Public Sub mnuViewRefresh_Click()
  SearchNow
End Sub

'
' Serialize column headers
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView, lvView.Tag
End Sub

Private Sub mnuViewStatusBar_Click()
  mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
  Form_Resize
  IniMain.WriteFile "Main", "ViewStatusbar", mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  Form_Resize
  IniMain.WriteFile "Main", "ViewToolbar", mnuViewToolbar.Checked
End Sub

Private Sub picListView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  lvView_MouseUp Button, Shift, X, Y
End Sub

Private Sub picListView_Resize()
  ' Do not resize when minimized
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  lvView.Left = IIf(LocaleEnableWebContent = True, 1716, 0)
  lvView.Height = Max(picListView.ScaleHeight - lvView.Top, 0)
  lvView.Width = Max(picListView.ScaleWidth - lvView.Left, 0)
  
  picSearchStatus.Top = lvView.Top + IIf(lvView.View = lvwReport, 240, 0)
  picSearchStatus.Left = lvView.Left
  picSearchStatus.Width = lvView.Width
  
  If Me.Width < 7180 Then
    Me.Width = 7180
    sstabSearch.Width = Me.Width - picSearchButtons.Width
    Exit Sub
   End If
    
   sstabSearch.Width = Me.Width - picSearchButtons.Width
   picSearchButtons.Left = sstabSearch.Left + sstabSearch.Width
   txtWords.Width = sstabSearch.Width - 1850
   cboIn.Width = txtWords.Width
End Sub

Private Sub picSelect_Resize()
  If Not mIsSearchOnly Then
    picButtons.Left = Max(ScaleWidth - picButtons.Width, 0)
  End If
End Sub

Private Sub sstabSearch_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable sstabSearch
  If sstabSearch.TabVisible(1) Then RefreshClaim
End Sub

'
' This is a rare case where the ToolBar is the master for functionality.
' Usually, menus contain the functionality
'
Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "New"
            'ToDo: Add 'New' button code.
            MsgBox "Add 'New' button code."
        Case "Open"
            
        Case "Print"
            'ToDo: Add 'Print' button code.
            MsgBox "Add 'Print' button code."
        Case "Move"
            
        Case "Delete"
            
        Case "View Large Icons"
            lvView.View = lvwIcon
            picListView_Resize
        Case "View Small Icons"
            lvView.View = lvwSmallIcon
            picListView_Resize
        Case "View List"
            lvView.View = lvwList
            picListView_Resize
        Case "View Details"
            lvView.View = lvwReport
            picListView_Resize
    End Select
End Sub

Private Sub tmrSearch_Timer()
  tmrSearch.Enabled = False
  If Len(txtWords.Text) > 2 And lastSearch <> txtWords.Text Then
    cmdSearchNow_Click
  End If
End Sub

Private Sub txtWords_GotFocus()
  FormUtil.SelAll txtWords
End Sub

Private Sub txtWords_KeyDown(KeyCode As Integer, Shift As Integer)
  If (KeyCode = vbKeyDown Or KeyCode = vbKeyUp) And (Shift And vbShiftMask) = 0 Then
    SendMessage lvView.hWnd, WM_KEYDOWN, KeyCode, 0
    KeyCode = 0
    cmdOK.Default = True
    Exit Sub
  End If
  If (KeyCode = vbKeyL) And (Shift And vbCtrlMask) > 0 Then
    cboIn.ListIndex = IIf(cboIn.ListIndex = cboIn.ListCount - 1, _
      0, cboIn.ListIndex + 1)
    txtWords_KeyPress 0
    KeyCode = 0
  End If
End Sub

Private Sub txtWords_KeyPress(KeyAscii As Integer)
  tmrSearch.Enabled = False
  cmdSearchNow.Default = True
  tmrSearch.Enabled = True
End Sub
'
'
'
' End Generic Section
'
'
'
'
' Start Custom Section
'
'


'
' Hook for inherited searches (frmFirmSearch, )
'
Public Sub FormLoad()
End Sub


Private Sub RefreshClaim()
  If Not sstabSearch.TabVisible(1) Then Exit Sub
  
  Dim Enabled As Boolean
  
  Enabled = IIf(chkStatus.Value = 1, True, False)
  optStatus(0).Enabled = Enabled
  optStatus(1).Enabled = Enabled
  optStatus(2).Enabled = Enabled
  chkDate.Enabled = Enabled
  
  dtpFrom.Enabled = ((chkDate.Value = 1) And chkDate.Enabled)
  dtpTo.Enabled = ((chkDate.Value = 1) And chkDate.Enabled)
  
  If Not (optStatus(0).Value Or optStatus(1).Value Or optStatus(2).Value) Then
    optStatus(1).Value = True
  End If
End Sub
