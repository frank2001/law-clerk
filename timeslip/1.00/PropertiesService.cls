VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PropertiesService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mClient As SoapClient
Private Const ServiceName = "PropertiesService"

'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mClient = New SoapClient
  mClient.mssoapinit WSDLFile
  mClient.ConnectorProperty("EndPointURL") = EndPointURL
End Sub


'
' Interface to Web Service
'


Public Function GetProperty(Context As String, Name As String, Key As String) As String
    
On Error GoTo Exception
  
  GetProperty = mClient.GetProperty(Context, Name, Key)
  Exit Function
  
Exception:
  Err.Clear
  MsgBox "Properties Service Get Error: " & Err.Description, vbExclamation
End Function


Public Function SetProperty(Context As String, Name As String, Key As String, Value As String) As Boolean
    
'On Error GoTo Exception
  
  Call mClient.SetProperty(Context, Name, Key, Value)
  SetProperty = True
  Exit Function
  
Exception:
  Err.Clear
  MsgBox "Properties Service Set Error: " & Err.Description, vbExclamation
End Function



Public Function GetProperties(Context As String, Name As String) As String()
    
On Error GoTo Exception
  
  GetProperties = mClient.GetProperties(Context, Name)
  Exit Function
  
Exception:
  Err.Clear
  MsgBox "Naming Service List Contexts Error: " & Err.Description, vbExclamation
End Function


Public Function SetProperties(Context As String, Name As String, Keys() As String, Values() As String) As Boolean
    
'On Error GoTo Exception
  
  Call mClient.SetProperties(Context, Name, Keys, Values)
  SetProperties = True
  Exit Function
  
Exception:
  Err.Clear
  MsgBox "Properties Service Set Error: " & Err.Description, vbExclamation
End Function


Public Function PropertiesExist(Context As String, Name As String) As Boolean
    
On Error GoTo Exception
  
  PropertiesExist = mClient.PropertiesExist(Context, Name)
  Exit Function
  
Exception:
  Err.Clear
  MsgBox "Naming Service Properties Exist Error: " & Err.Description, vbExclamation
End Function


Public Function Find(Context As String, Name As String, Optional Source As String) As Collection
  Dim Properties() As String, I As Integer, Fields As New Collection
  Properties = GetProperties(Context, Name)
  For I = 0 To SafeUBound(Properties) Step 2
    Fields.Add CreateDataField(Source, Properties(I), Properties(I + 1)), Properties(I)
  Next I
  Set Find = Fields
End Function

Public Function WriteProperties(Context As String, Name As String, DataFields As Collection) As Boolean
  Dim Size As Integer, I As Integer
  Size = DataFields.Count
  ReDim Keys(Size - 1) As String
  ReDim Values(Size - 1) As String
  For I = 1 To Size
    Keys(I - 1) = DataFields(I).Name
    Values(I - 1) = DataFields(I).Value
  Next I
  WriteProperties = SetProperties(Context, Name, Keys, Values)
End Function

