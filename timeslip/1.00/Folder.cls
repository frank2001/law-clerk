VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "View"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Default information about a folder."
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Dataset"
Option Explicit

Private mPlugInName As String

Private mPopupMenuName As String
Private mPopupMenuForm As Form

Private mDefaultMenuName As String
Private mDefaultMenuIndex As Integer

Private mOpenMenuName As String
Private mPropertiesMenuName As String
Private mExploreMenuName As String

Private mNewItemName As String

Private mFinder As DataFinder
Private mFinderName As String

Private mIsWorkflow As Boolean
Private mIsSearchOnly As Boolean
Private mIsMaintenance As Boolean

'local variable(s) to hold property value(s)
Private mKey As String 'local copy

Private mColumn As String


Public Property Let Key(ByVal vData As String)
  mKey = vData
End Property

Public Property Get Key() As String
Attribute Key.VB_UserMemId = 0
  Key = mKey
End Property


Public Property Let PlugInName(ByVal vData As String)
  mPlugInName = vData
End Property

Public Property Get PlugInName() As String
  PlugInName = mPlugInName
End Property


Public Property Let IsSearchOnly(ByVal vData As Boolean)
    mIsSearchOnly = vData
End Property

Public Property Get IsSearchOnly() As Boolean
    IsSearchOnly = mIsSearchOnly
End Property


Public Property Let IsWorkflow(ByVal vData As Boolean)
    mIsWorkflow = vData
End Property

Public Property Get IsWorkflow() As Boolean
    IsWorkflow = mIsWorkflow
End Property


'
' DataFinder
'

Public Property Set DataFinder(ByVal vData As DataFinder)
    Set mFinder = vData
End Property


Public Property Get DataFinder() As DataFinder
    Set DataFinder = mFinder
End Property

Public Property Let FinderName(ByVal vData As String)
  mFinderName = vData
End Property


Public Property Get FinderName() As String
  FinderName = mFinderName
End Property


'
' Popup Menu Form
'

Public Property Set PopupMenuForm(vData As Form)
  Set mPopupMenuForm = vData
End Property

Public Property Get PopupMenuForm() As Form
  Set PopupMenuForm = mPopupMenuForm
End Property


'
' Popup Menu
'

Public Property Let PopupMenuName(vData As String)
  mPopupMenuName = vData
End Property


Public Property Get PopupMenuName() As String
  PopupMenuName = mPopupMenuName
End Property


Public Property Get PopupMenu() As Menu
  If (mPopupMenuForm Is Nothing Or mPopupMenuName = "") Then
    Exit Property
  End If
  Set PopupMenu = mPopupMenuForm.Controls(mPopupMenuName)
End Property


'
' Default Menu
'

Public Property Let DefaultMenuName(vData As String)
  Dim Paren As Integer
  Paren = InStr(vData, "(")
  If Paren = 0 Then
    mDefaultMenuName = vData
    mDefaultMenuIndex = -1
  Else
    mDefaultMenuName = Mid(vData, 1, Paren - 1)
    mDefaultMenuIndex = CInt(Mid(vData, Paren + 1, 1))
  End If
End Property


Public Property Get DefaultMenuName() As String
  DefaultMenuName = mDefaultMenuName
End Property

Public Property Get DefaultMenuIndex() As Integer
  DefaultMenuIndex = mDefaultMenuIndex
End Property


Public Property Get DefaultMenu() As Menu
  If (mPopupMenuForm Is Nothing Or mDefaultMenuName = "") Then
    Exit Property
  End If
  ' VB does not short circut IIf function.  Must do if then else to
  ' keep from evaluating both.
  If mDefaultMenuIndex < 0 Then
    Set DefaultMenu = mPopupMenuForm.Controls(mDefaultMenuName)
  Else
    Set DefaultMenu = mPopupMenuForm.Controls(mDefaultMenuName)(mDefaultMenuIndex)
  End If
End Property


'
' Open Menu Control Array
'

Public Property Let OpenMenuName(vData As String)
  mOpenMenuName = vData
End Property


Public Property Get OpenMenuName() As String
  OpenMenuName = mOpenMenuName
End Property

Public Property Get OpenMenu() As Object
  If (mPopupMenuForm Is Nothing Or mOpenMenuName = "") Then
    Exit Property
  End If
  Set OpenMenu = mPopupMenuForm.Controls(mOpenMenuName)
End Property


'
' Explore Menu Control Array
'

Public Property Let ExploreMenuName(vData As String)
  mExploreMenuName = vData
End Property

Public Property Get ExploreMenuName() As String
  ExploreMenuName = mExploreMenuName
End Property

Public Property Get ExploreMenu() As Object
  If (mPopupMenuForm Is Nothing Or mExploreMenuName = "") Then
    Exit Property
  End If
  Set ExploreMenu = mPopupMenuForm.Controls(mExploreMenuName)
End Property


'
' Item for Ctrl-N or Insert
'

Public Property Let NewItemName(vData As String)
  mNewItemName = vData
End Property

Public Property Get NewItemName() As String
  NewItemName = mNewItemName
End Property



'
' Properties Menu
'

Public Property Let PropertiesMenuName(vData As String)
  mPropertiesMenuName = vData
End Property


Public Property Get PropertiesMenuName() As String
  PropertiesMenuName = mPropertiesMenuName
End Property

Public Property Get PropertiesMenu() As Object
  If (mPopupMenuForm Is Nothing Or mPropertiesMenuName = "") Then
    Exit Property
  End If
  Set PropertiesMenu = mPopupMenuForm.Controls(mPropertiesMenuName)
End Property


