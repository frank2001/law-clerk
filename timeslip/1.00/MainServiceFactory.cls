VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ServiceFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"ApplicationLawAreaService"
Attribute VB_Ext_KEY = "Member1" ,"ApplicationLawyerService"
Attribute VB_Ext_KEY = "Member2" ,"ApplicationLimitService"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member3" ,"ApplicationQuestionService"
Attribute VB_Ext_KEY = "Member4" ,"ApplicationRatingService"
Attribute VB_Ext_KEY = "Member5" ,"ApplicationService"
Attribute VB_Ext_KEY = "Member6" ,"AttributeService"
Attribute VB_Ext_KEY = "Member7" ,"AutoKeyService"
Attribute VB_Ext_KEY = "Member8" ,"ClaimPaymentService"
Attribute VB_Ext_KEY = "Member9" ,"ClaimService"
Attribute VB_Ext_KEY = "Member10" ,"ContactService"
Attribute VB_Ext_KEY = "Member11" ,"ContactTypeService"
Attribute VB_Ext_KEY = "Member12" ,"CoverageService"
Attribute VB_Ext_KEY = "Member13" ,"DeductibleReimburseService"
Attribute VB_Ext_KEY = "Member14" ,"EventRecipientService"
Attribute VB_Ext_KEY = "Member15" ,"EventService"
Attribute VB_Ext_KEY = "Member16" ,"EventTypeService"
Attribute VB_Ext_KEY = "Member17" ,"FirmInsurerService"
Attribute VB_Ext_KEY = "Member18" ,"FirmService"
Attribute VB_Ext_KEY = "Member19" ,"FolderParameterService"
Attribute VB_Ext_KEY = "Member20" ,"FolderService"
Attribute VB_Ext_KEY = "Member21" ,"GLAccountService"
Attribute VB_Ext_KEY = "Member22" ,"LawAreaService"
Attribute VB_Ext_KEY = "Member23" ,"LawErrorService"
Attribute VB_Ext_KEY = "Member24" ,"LawyerService"
Attribute VB_Ext_KEY = "Member25" ,"LocationService"
Attribute VB_Ext_KEY = "Member26" ,"LocationTypeService"
Attribute VB_Ext_KEY = "Member27" ,"PersonService"
Attribute VB_Ext_KEY = "Member28" ,"PolicyAccountingService"
Attribute VB_Ext_KEY = "Member29" ,"PolicyService"
Attribute VB_Ext_KEY = "Member30" ,"ReferenceService"
Option Explicit

Private Const GenericServiceWSDL = "component/GenericService.wsdl"
Private Const FolderServiceWSDL = "component/FolderService.wsdl"
Private Const NamingServiceWSDL = "component/NamingService.wsdl"
Private Const PropertiesServiceWSDL = "component/PropertiesService.wsdl"

Private mGenericService As GenericService
Private mFolderService As FolderService
Private mNamingService As NamingService
Private mPropertiesService As PropertiesService


Public Property Get FolderService() As FolderService
  If mFolderService Is Nothing Then
    Set mFolderService = New FolderService
    mFolderService.Init _
        HttpServer & WebApplication & FolderServiceWSDL, _
        HttpServer & WebApplication & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set FolderService = mFolderService
End Property

Public Property Get GenericService() As GenericService
  If mGenericService Is Nothing Then
    Set mGenericService = New GenericService
    mGenericService.Init _
        HttpServer & WebApplication & GenericServiceWSDL, _
        HttpServer & WebApplication & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set GenericService = mGenericService
End Property

Public Property Get NamingService() As NamingService
  If mNamingService Is Nothing Then
    Set mNamingService = New NamingService
    mNamingService.Init _
        HttpServer & WebApplication & NamingServiceWSDL, _
        HttpServer & WebApplication & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set NamingService = mNamingService
End Property

Public Property Get PropertiesService() As PropertiesService
  If mPropertiesService Is Nothing Then
    Set mPropertiesService = New PropertiesService
    mPropertiesService.Init _
        HttpServer & WebApplication & PropertiesServiceWSDL, _
        HttpServer & WebApplication & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set PropertiesService = mPropertiesService
End Property


