Attribute VB_Name = "FormUtil"
Option Explicit

Public Const lawActionComplete = 0
Public Const lawActionVoid = 1
Public Const lawActionReopen = 2
Public Const lawActionNew = 3

Public picDisplay As New PictureDisplay


Public Function ActionText(ActionType As Integer, Optional IsDatabaseName As Boolean) As String
  Select Case ActionType
    Case 0
      ActionText = "Complete"
    Case 1
      ActionText = "Void"
    Case 2
      ActionText = "Reopen"
    Case 3
      ActionText = IIf(IsDatabaseName, "Create", "New")
  End Select

End Function


Public Sub SelAll(Text As Control)
  Text.SelStart = 0
  Text.SelLength = Len(Text)
End Sub


Public Sub PostTab(hWnd As Integer)
  PostMessage hWnd, WM_KEYDOWN, vbKeyTab, 0
End Sub


'
' Load up open menu.
'
Public Sub MenuUnload(Menu As Object)
  Dim Item As Menu, Index As Integer
  
  ' Ensure first is visible before removing everything.
  Menu(0).Visible = True
  
  ' Clear destination.
  For Each Item In Menu
    If Item.Index <> 0 Then Unload Item
  Next Item
End Sub

'
' Load up open menu.
'
Public Sub MenuCopy(Source As Object, Destination As Object)
  Dim Item As Menu, Index As Integer
  
  ' Unload any destination.
  MenuUnload Destination
  
  ' If no source, then just exit
  If Source Is Nothing Then Exit Sub
  
  ' Copy source to destination.
  For Each Item In Source
    Index = Item.Index
    If Index <> 0 Then
      Load Destination(Index)
      With Destination(Index)
        .Caption = Item.Caption
        .Enabled = Item.Enabled
        .Visible = Item.Visible
      End With
    End If
  Next Item
  
  ' Hide holding menu if something has been copied.
  If Destination.Count > 1 Then
    Destination(0).Visible = False
  End If
End Sub


Public Sub MenuLoad(Frm As Form)
  ' Custom Menu Shortcut Adjustments
  Frm.KeyPreview = True
  
  Frm.mnuViewPrevious.Caption = Frm.mnuViewPrevious.Caption + vbTab + "Ctrl+<"
  Frm.mnuViewNext.Caption = Frm.mnuViewNext.Caption + vbTab + "Ctrl+>"
End Sub

'
' Requires standard "Edit" menus and command buttons.
'
Public Sub EditLoad(Frm As Form)
  On Error Resume Next
  
  MenuLoad Frm
  
  Frm.cmdEnter.Default = False
  Frm.cmdEnter.TabStop = False
  
  Frm.cmdCancel.Cancel = True
  Frm.cmdCancel.CausesValidation = False
End Sub


Public Sub EditLoadImages(Frm As Form)
  'add the images
  ' fill image list
  Frm.imlUpDown.ListImages.Add 1, "Next", picDisplay.DownEnabled
  Frm.imlUpDown.ListImages.Add 2, "Previous", picDisplay.UpEnabled
  Frm.imlUpDown.MaskColor = &HC0C0C0
  Frm.imlUpDown.UseMaskColor = True
  Frm.imlUpDownDisabled.ListImages.Add 1, "DisNext", picDisplay.DownDisabled
  Frm.imlUpDownDisabled.ListImages.Add 2, "DisPrevious", picDisplay.UpDisabled
  Frm.imlUpDownDisabled.MaskColor = &HC0C0C0
  Frm.imlUpDownDisabled.UseMaskColor = True
  
  ' set the image list to the toolbar
  Frm.tbrUpDown.ImageList = Frm.imlUpDown
  Frm.tbrUpDown.DisabledImageList = Frm.imlUpDownDisabled
  Frm.tbrUpDown.Buttons(1).Image = "Next"
  Frm.tbrUpDown.Buttons(2).Image = "Previous"
End Sub


Public Function ValidateControls(Form As Form) As Boolean
  On Error Resume Next
  Form.ValidateControls
  ValidateControls = Not Err
End Function


'
' Navigation Refresh
'
Public Sub NavigationRefresh(Form As Form, View As ListView, Index As Integer)
  '
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  '
  If (View Is Nothing) Then
    Form.mnuViewPrevious.Enabled = False
    Form.mnuViewNext.Enabled = False
    Form.tbrUpDown.Buttons(1).Enabled = False
    Form.tbrUpDown.Buttons(2).Enabled = False
  Else
    Form.mnuViewNext.Enabled = View.Enabled And Index < View.ListItems.Count
    Form.mnuViewPrevious.Enabled = View.Enabled And Index > 1
    Form.tbrUpDown.Buttons(1).Enabled = Form.mnuViewNext.Enabled
    Form.tbrUpDown.Buttons(2).Enabled = Form.mnuViewPrevious.Enabled
  End If
End Sub


'
' Standardize call to write to form's ini file.
'
Public Sub WriteIniFile(FormName As String, Name As String, Value As String)
  IniForm.WriteFile FormName, Name, Value
End Sub

'
' Save form position.
'
Public Sub SerializePosition(Form As Form)
  If Form.WindowState = vbNormal Then
    WriteIniFile Form.Name, "Left", Form.Left
    WriteIniFile Form.Name, "Top", Form.Top
    If Form.BorderStyle = vbSizable Then
      WriteIniFile Form.Name, "Height", Form.Height
      WriteIniFile Form.Name, "Width", Form.Width
    End If
  End If
  WriteIniFile Form.Name, "WindowState", Form.WindowState
End Sub

'
' Standardize call to read from forms ini file.
'
Public Function ReadIniFile(FormName As String, Name As String) As String
  ReadIniFile = IniForm.ReadFile(FormName, Name)
End Function

'
' Load form position.  See if other forms of the same name are
' open and offset from those.
'
Public Sub DeserializePosition(Form As Form)
  Dim mLeft, mTop As String
  mLeft = ReadIniFile(Form.Name, "Left")
  mTop = ReadIniFile(Form.Name, "Top")
  
  Dim Window As Form
  For Each Window In Forms
    If Window.Name = Form.Name And Window.hWnd <> Form.hWnd Then
      If Window.WindowState = vbMinimized Then
        Window.WindowState = vbNormal
      End If
      Window.Show
      Window.Refresh
      mLeft = Window.Left + 288
      mTop = Window.Top + 336
    End If
  Next Window
  
  If mLeft <> vbNullString Then Form.Left = mLeft
  If mTop <> vbNullString Then Form.Top = mTop
  
  Dim Value As String
  Value = ReadIniFile(Form.Name, "WindowState")
  If Value <> vbNullString Then
    If Value = vbMaximized Then
      Form.WindowState = vbMaximized
      Exit Sub
    End If
  End If
  Form.WindowState = vbNormal
  
  If Form.BorderStyle = vbSizable Then
    Value = ReadIniFile(Form.Name, "Height")
    If Value <> vbNullString Then Form.Height = Value
    Value = ReadIniFile(Form.Name, "Width")
    If Value <> vbNullString Then Form.Width = Value
  End If
End Sub

'
' Used by form main when a child is unloaded.
'
Public Function FindLastForm(Form As Form) As Form
  Dim Window As Variant
  For Each Window In Forms
    If Window.Name = Form.Name And Window.hWnd <> Form.hWnd Then
      Set FindLastForm = Window
    End If
  Next Window
End Function

'
' Used by SStabs to fix the tab order by enabling the control.
' Just call this code form the tab click routine and in Form_Load
'
Public Sub TabControlEnable(mSSTab As SSTab)
  Dim c As Control
  For Each c In mSSTab.Parent.Controls
    If Not (TypeOf c Is ImageList Or _
            TypeOf c Is Menu Or _
            TypeOf c Is CommonDialog Or _
            TypeOf c Is Timer Or _
            TypeOf c Is PictureBox) Then
      If c.Container Is mSSTab Then
        c.Enabled = c.Left > 0
      End If
    End If
  Next
End Sub

Public Sub SizeSingleToolbar(Bar As Toolbar, Left As Integer)
  Bar.Height = Bar.ButtonHeight
  Bar.Width = Bar.Buttons(1).Width
  Bar.Left = Left
End Sub


Public Sub ShowByListView(PlugIn As Object, Form As Form, FormMgr As FormManager, View As ListView)
  Set Form.PlugIn = PlugIn
  FormMgr.Add Form
  Form.ShowByListView View
End Sub

Public Sub ShowByPrimaryKey(Form As Form, Handle As String, Optional FormMgr As FormManager, Optional Index, Optional FormMgrKey)
  If Not FormMgr Is Nothing Then
    If IsMissing(FormMgrKey) Then
      FormMgr.Add Form
    Else
      FormMgr.Add Form, CStr(FormMgrKey)
    End If
  End If
  If IsMissing(Index) Then
    Form.ShowByPrimaryKey Handle
  Else
    Form.ShowByPrimaryKey Handle, CInt(Index)
  End If
End Sub

Public Sub ShowByPrimaryKeys(Form As Form, FormMgr As FormManager, Handle1 As String, Optional Handle2, Optional Index)
  FormMgr.Add Form
  If IsMissing(Index) Then
    Form.ShowByPrimaryKey Handle1, Handle2
  Else
    Form.ShowByPrimaryKey Handle1, Handle2, Index
  End If
End Sub

Public Sub ShowEventDialog(Table As String, Handle As String, ActionType As Integer)
'  Dim fForm As New frmEventDialog
'  fForm.ReferenceLoad Table, Handle, ActionType
'  fForm.Show vbModal
'  Unload fForm
End Sub

Public Function ShowEventProperties(EventHandle As String) As Boolean
'  If EventHandle = vbNullString Then
'    Exit Function
'  End If
'
'  Dim DataField As Collection
'  Set DataField = Factory.EventService.Find(EventHandle)
'
'  If DataField Is Nothing Then
'    Exit Function
'  End If
'
'  Dim fEvent As New frmEventPropertiesEdit
'  fEvent.ShowEvent DataField
'  ShowEventProperties = True
End Function

Public Sub ShowNew(PlugIn As Object, Form As Form, FormMgr As FormManager, Optional Handle, Optional FormMgrKey)
  Set Form.PlugIn = PlugIn
  If IsMissing(FormMgrKey) Then
    FormMgr.Add Form
  Else
    FormMgr.Add Form, CStr(FormMgrKey)
  End If
  If IsMissing(Handle) Then
    Form.ShowNew
  Else
    Form.ShowNew Handle
  End If
End Sub



Public Sub SendToFolder(Group As String, Source As String, Handle As String, Name As String)
  ' Create form and show.
'  Dim Form As New frmFolderBrowseDialog
'  If Not Form.ShowSendTo(Group) Then
'    Unload Form
'    Exit Sub
'  End If
'
'  Dim Handles(0) As String, Names(0) As String
'  Handles(0) = Handle
'  Names(0) = Name
'
'  Call Factory.EventService.Create( _
'      Form.EventTypeId, _
'      Group, _
'      Form.FolderKey, _
'      Source, _
'      Handles, _
'      Names)
'
'  Unload Form
End Sub


Public Sub GLTransactionSummary(Items As ListItems, _
    Optional lblDebit As Label, Optional lblCredit As Label, Optional lblBalance As Label)
  
  Dim Item As ListItem
  Dim Debits As Double, Credits As Double
  For Each Item In Items
    If Item.ListSubItems("transactionAmount") < 0 Then
      Item.ListSubItems("~debit") = ""
      Item.ListSubItems("~credit") = Format(0 - Item.ListSubItems("transactionAmount"), "#,##0.00;(#,##0.00)")
      Credits = Credits + Item.ListSubItems("~credit")
    Else
      Item.ListSubItems("~debit") = Format(Item.ListSubItems("transactionAmount"), "#,##0.00;(#,##0.00)")
      Item.ListSubItems("~credit") = ""
      Debits = Debits + Item.ListSubItems("~debit")
    End If
  Next Item
  
  If Not lblDebit Is Nothing Then
    lblDebit.Caption = Format(Debits, "#,##0.00;(#,##0.00)")
  End If
  
  If Not lblCredit Is Nothing Then
    lblCredit.Caption = Format(Credits, "#,##0.00;(#,##0.00)")
  End If
  
  If Not lblBalance Is Nothing Then
    lblBalance.Caption = Format(Debits - Credits, "#,##0.00;(#,##0.00)")
  End If

End Sub


Public Sub MenuEventLoad(Popup As Form, Handle As String)
'  With Popup
'    ' Unload current menu.
'    FormUtil.MenuUnload .mnuEventOpen
'
'    Dim Refs As Collection, I, j As Integer, Source As String
'    Set Refs = Factory.ReferenceService.References(Handle)
'    If Refs Is Nothing Then
'      .mnuEventOpen(0).Visible = False
'      Exit Sub
'    End If
'
'    For I = 1 To Refs.Count
'      Source = Refs.Item(I).Item("referenceTable")
'      Load .mnuEventOpen(I)
'      .mnuEventOpen(I).Tag = Source
'      .mnuEventOpen(I).Caption = "Open " & Source
'    Next I
'
'    Load .mnuEventOpen(I)
'    .mnuEventOpen(I).Tag = "Notepad"
'    .mnuEventOpen(I).Caption = "Open Notepad"
'
'    Dim Row As Collection
'    Set Row = Factory.EventService.Find(Handle, """sourceGroupId""")
'    If Not Row Is Nothing Then
'      If Row("sourceGroupId") = DatabaseName.DOCUMENT_GROUP Then
'        Load .mnuEventOpen(I + 1)
'        .mnuEventOpen(I + 1).Tag = Row("sourceGroupId")
'        .mnuEventOpen(I + 1).Caption = "Edit Document"
'      End If
'    End If
'    .mnuEventOpen(0).Visible = False
'    .mnuEventPaste.Enabled = (Not EventBuffer Is Nothing)
'  End With
End Sub

Public Sub Dirty(Form As Form, IsDirty As Boolean, Optional IsNew As Boolean)
  On Error Resume Next
  
  Dim isOpen As Boolean
  isOpen = IsDirty Or IsNew
  
  Form.mnuFileSave.Enabled = isOpen
  Form.cmdSave.Enabled = isOpen
  Form.cmdSaveNew.Enabled = isOpen
  Form.cmdCancel.Caption = IIf(IsDirty, "Cancel", "Close")
  
  Form.mnuFileWorkflow(0).Enabled = Not isOpen
  Form.mnuFileWorkflow(1).Enabled = Not isOpen
  
  ' Clear any error.
  Err.Clear
End Sub


Public Sub GLTransactionLoad(View As ListView, _
    GLAccountIds() As String, GLTransactionAmounts() As Double)
    
  Dim Count As Integer, I As Integer
  Count = View.ListItems.Count
  
  If Count = 0 Then Exit Sub
  
  ReDim GLAccountIds(Count - 1)
  ReDim GLTransactionAmounts(Count - 1)
  For I = 1 To Count
    GLAccountIds(I - 1) = View.ListItems(I)
    GLTransactionAmounts(I - 1) = View.ListItems(I).ListSubItems("transactionAmount")
  Next I
    
End Sub

'Public Sub LoadListViewCollection(LV As ListView, mCollection As Collection)
'  Dim n As Integer
'  Dim xItem As ListItem
'
'  Set xItem = LV.ListItems.Add(, , mCollection(LV.ColumnHeaders(1).Key))
'  For n = 2 To LV.ColumnHeaders.Count
'    xItem.ListSubItems.Add , LV.ColumnHeaders(n).Key, mCollection(LV.ColumnHeaders(n).Key)
'  Next n
'End Sub

Public Sub ComboBoxLoad(Items As Collection, Combo As ComboBox, Index As Variant)
  If Items Is Nothing Then Exit Sub
  Dim I, Count As Integer
  Count = Items.Count
  For I = 1 To Count
    Combo.AddItem Items(I)(Index)
  Next I
End Sub

Public Sub ComboBoxRegistryLoad(Combo As ComboBox, Key As String)
  Dim List As String, Items() As String
  Combo.Clear
  List = IniMain.ReadFile("Dropdown", Key)
  If List <> vbNullString Then
    ComboBoxArrayLoad Combo, Split(List, "|")
  End If
End Sub

Public Sub ComboBoxArrayLoad(Combo As ComboBox, List As Variant)
  Dim I, Count As Integer
  Count = UBound(List)
  For I = 0 To Count
    Combo.AddItem List(I)
  Next I
End Sub


Private Sub SetFormMgr(Form As Form, FormMgr As FormManager, Optional FormMgrKey)
  If IsMissing(FormMgrKey) Then
    FormMgr.Add Form
  Else
    FormMgr.Add Form, CStr(FormMgrKey)
  End If
End Sub

Public Sub PrintEnvelope(Optional DeliveryAddress As String, Optional DeliveryPoint As String)
'  Dim fEnvelope As New frmEnvelopeLabel
'  fEnvelope.DeliveryAddress = DeliveryAddress
'  fEnvelope.DeliveryPoint = DeliveryPoint
'  fEnvelope.PrintBarCode = 1
'  fEnvelope.Show vbModal
End Sub




'
'  Added from other modules.
'
'

Public Sub MaskEditClear(Control As MaskEdBox)
   Control.SelStart = 0
   Control.SelLength = 9999
   Control.SelText = ""
End Sub


Public Sub ClearDataField(DataFields As Variant, Optional DataMember As String)
  Dim Control As Variant
  For Each Control In DataFields
    If DataMember = vbNullString Or Control.DataMember = DataMember Then
      If TypeOf Control Is TextBox Then
        Control.Text = vbNullString
      ElseIf TypeOf Control Is Label Then
        Control.Caption = vbNullString
      ElseIf TypeOf Control Is MaskEdBox Then
        Dim msk As MaskEdBox
        Set msk = Control
        MaskEditClear msk
      ElseIf TypeOf Control Is ComboBox Then
        Dim cbx As ComboBox
        Set cbx = Control
        With cbx
          If .ListCount > 0 Then
            .ListIndex = 0
          End If
        End With
      ElseIf TypeOf Control Is DTPicker Then
        Dim dtpDate As DTPicker
        Set dtpDate = Control
        If dtpDate.CheckBox Then
          dtpDate.Value = Now()
          dtpDate.Value = vbNull
        Else
          dtpDate.Value = vbNull
          dtpDate.Value = Now()
        End If
      End If
    End If
  Next
End Sub


Public Function CollectionItem(Collection As Variant, Index As String) As Variant
  On Error Resume Next
  If VarType(Collection) = vbEmpty Then
  ElseIf TypeOf Collection Is ListItem Then
    Dim ListItem As ListItem
    Set ListItem = Collection
    CollectionItem = ListItemText(ListItem, Index)
  Else
    CollectionItem = Collection(Index)
  End If
  Err.Clear
End Function


Public Sub SetFields(Controls As Variant, Optional Items As Variant, Optional DataMember As String)
  Dim Item As Variant
  Dim Control As Variant
  For Each Control In Controls
    If ((DataMember <> "" And Control.DataMember <> DataMember) Or Control.DataField = "") Then
      GoTo Continue
    End If
    
    Item = CollectionItem(Items, Control.DataField)
      
    If TypeOf Control Is Label Then
      Control.Caption = DisplayFormat(Control.DataField, CStr(Item), Control.DataFormat.Format, LocaleShortDate)
    
    ElseIf TypeOf Control Is TextBox Then
      Control.Text = Format(Item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is DTPicker Then
      If Item = vbNullString Then
        Control.CheckBox = True
      End If
      Control.Value = Format(Item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is ComboBox Then
      Dim cbx As ComboBox
      Set cbx = Control
      With cbx
        If Item = vbNullString Then
          If .ListCount > 0 Then
            .ListIndex = 0
          End If
        Else
          Dim Text As String
          Text = Format(Item, .DataFormat.Format)
          If .Style = ComboBoxConstants.vbComboDropdownList Then
            Dim I As Integer
            For I = .ListCount - 1 To 0 Step -1
              If .List(I) = Text Then
                .ListIndex = I
                Exit For
              End If
            Next I
          Else
            .Text = Text
          End If
        End If
      End With
    
    ElseIf TypeOf Control Is MaskEdBox Then
      Control.SelStart = 0
      Control.SelLength = 9999
      Control.SelText = Format(Item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is CheckBox Then
      Control.Value = IIf(Item = "Yes", 1, 0)
  
    End If
    
Continue:
  Next
End Sub



'
' Return text from a list item by Index.  Looks in both the Item and SubItems
'
Public Function ListItemText(Item As ListItem, Index As String) As String
On Error GoTo Exception
  
  If Item.Tag = Index Then
    ListItemText = Item
  Else
    ListItemText = Item.ListSubItems(Index)
  End If
  
Exception:
  Err.Clear
End Function




' From XMLUTil

Public Function DisplayFormat(DataField As String, DataValue As String, Optional DataFormat As String, Optional DateFormat As String) As String
  If DataFormat <> vbNullString Then
    DisplayFormat = Format(DataValue, DataFormat)
    Exit Function
  End If
  If (Right(DataField, 4) <> "Date" And Right(DataField, 4) <> "Time") Or Len(DataValue) < 10 Or DateFormat = vbNullString Then
    DisplayFormat = DataValue
    Exit Function
  End If
  DisplayFormat = Format(DataValue, DateFormat)
End Function



' From MainModule


Public Function CreateDataField(Source As String, Name As String, Value As Variant) As DataField
  Set CreateDataField = New DataField
  CreateDataField.Source = Source
  CreateDataField.Name = Name
  CreateDataField.Value = Value
End Function


Public Sub AddMaskControl(MaskControl As Object, Items As Collection)
  Dim Control As MaskEdBox, Text As String
  For Each Control In MaskControl
    If (Not Control.DataField = "") Then
      If Control.ClipMode = mskIncludeLiterals Then
        Text = Control.Text
      Else
        Text = Control.ClipText
      End If
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Text), Control.DataField
    End If
  Next
End Sub

Public Sub AddTextControl(TextControl As Variant, Items As Collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In TextControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Text), Control.DataField
    End If
  Next
End Sub

Public Sub AddValueControl(ValueControl As Variant, Items As Collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In ValueControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Value), Control.DataField
    End If
  Next
End Sub

Public Sub AddCaptionControl(CaptionControl As Variant, Items As Collection)
  Dim Control As Variant
  For Each Control In CaptionControl
    If (Not Control.DataField = "") Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Caption), Control.DataField
    End If
  Next
End Sub

