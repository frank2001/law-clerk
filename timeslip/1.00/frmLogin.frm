VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLoginDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Login"
   ClientHeight    =   2604
   ClientLeft      =   36
   ClientTop       =   336
   ClientWidth     =   4500
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2604
   ScaleWidth      =   4500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "Login"
   Begin VB.PictureBox picAdvanced 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   1224
      Left            =   0
      ScaleHeight     =   1224
      ScaleWidth      =   4500
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   888
      Visible         =   0   'False
      Width           =   4500
      Begin TabDlg.SSTab ssTab 
         Height          =   1116
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   0
         Width           =   4296
         _ExtentX        =   7557
         _ExtentY        =   1969
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Advanced"
         TabPicture(0)   =   "frmLogin.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabels(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "cboServer"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         Begin VB.ComboBox cboServer 
            Height          =   288
            ItemData        =   "frmLogin.frx":0028
            Left            =   1212
            List            =   "frmLogin.frx":002A
            TabIndex        =   3
            Top             =   396
            Width           =   2784
         End
         Begin VB.Label lblLabels 
            Caption         =   "&Server:"
            Height          =   252
            Index           =   2
            Left            =   132
            TabIndex        =   2
            Tag             =   "&Password:"
            Top             =   420
            Width           =   1068
         End
      End
   End
   Begin VB.PictureBox picLogin 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   888
      Left            =   0
      ScaleHeight     =   888
      ScaleWidth      =   4500
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   0
      Width           =   4500
      Begin VB.TextBox txtUserName 
         Height          =   288
         Left            =   1296
         TabIndex        =   8
         Top             =   72
         Width           =   3072
      End
      Begin VB.TextBox txtPassword 
         Height          =   288
         IMEMode         =   3  'DISABLE
         Left            =   1296
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   432
         Width           =   3072
      End
      Begin VB.Label lblLabels 
         Caption         =   "&User Name:"
         Height          =   252
         Index           =   0
         Left            =   96
         TabIndex        =   7
         Tag             =   "&User Name:"
         Top             =   96
         Width           =   1080
      End
      Begin VB.Label lblLabels 
         Caption         =   "&Password:"
         Height          =   252
         Index           =   1
         Left            =   96
         TabIndex        =   0
         Tag             =   "&Password:"
         Top             =   456
         Width           =   1080
      End
   End
   Begin VB.PictureBox picButtons 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   468
      Left            =   0
      ScaleHeight     =   468
      ScaleWidth      =   4500
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   2112
      Width           =   4500
      Begin VB.CommandButton cmdAdvanced 
         Caption         =   "&Advanced >>"
         Height          =   336
         Left            =   3180
         TabIndex        =   6
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   1932
         TabIndex        =   5
         Tag             =   "Cancel"
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   684
         TabIndex        =   4
         Tag             =   "OK"
         Top             =   60
         Width           =   1200
      End
   End
End
Attribute VB_Name = "frmLoginDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpbuffer As String, nSize As Long) As Long

Public OK As Boolean




Private Sub cmdAdvanced_Click()
  If picAdvanced.Visible Then
    picAdvanced.Visible = False
    cmdAdvanced.Caption = "&Advanced >>"
  Else
    picAdvanced.Visible = True
    cboServer.SetFocus
    cmdAdvanced.Caption = "&Advanced <<"
  End If
  
  Height = picButtons.Top + picButtons.Height + Height - ScaleHeight
End Sub

Private Sub Form_Activate()
  Height = picButtons.Top + picButtons.Height + Height - ScaleHeight
End Sub

Private Sub Form_Load()
    Dim sBuffer As String
    Dim lSize As Long

    sBuffer = Space$(255)
    lSize = Len(sBuffer)
    Call GetUserName(sBuffer, lSize)
    If lSize > 0 Then
        txtUserName.Text = Left$(sBuffer, lSize)
    Else
        txtUserName.Text = vbNullString
    End If
    
    ComboBoxUtil.Deserialize cboServer, "ServerList", IniMain
    WindowState = 0
    Width = 4560
    Height = 1700
End Sub

Private Sub cmdCancel_Click()
    OK = False
    Me.Hide
End Sub


Private Sub cmdOK_Click()
    OK = True
    ComboBoxUtil.AddText cboServer, True
    ComboBoxUtil.Serialize cboServer, "ServerList", IniMain
    Me.Hide
End Sub



Private Sub txtPassword_GotFocus()
  txtPassword.SelStart = 0
  txtPassword.SelLength = Len(txtPassword)
End Sub

Private Sub txtUserName_GotFocus()
  txtUserName.SelStart = 0
  txtUserName.SelLength = Len(txtUserName)
End Sub
