VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ViewItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Collection of folder"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"Folder"
Attribute VB_Ext_KEY = "Member0" ,"Folder"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable to hold collection
Private mCol As Collection

Public Function Add(Key As String, PlugInName As String, _
        Optional Finder As DataFinder, _
        Optional IsWorkflow As Boolean, Optional mIsSearchOnly As Boolean, _
        Optional PopupMenuName As String, _
        Optional DefaultMenuName As String, _
        Optional OpenMenuName, _
        Optional ExploreMenuName, _
        Optional NewItemName, _
        Optional PropertiesMenuName) As View
    
  'create a new object
  Dim objNewMember As New View

  With objNewMember
    .Key = Key
    .PlugInName = PlugInName
    Set .DataFinder = Finder
    .IsWorkflow = IsWorkflow
    .IsSearchOnly = mIsSearchOnly
    
    If PopupMenuName <> vbNullString Then
      .PopupMenuName = PopupMenuName
      .DefaultMenuName = DefaultMenuName
      .OpenMenuName = OpenMenuName
      .ExploreMenuName = ExploreMenuName
      .NewItemName = NewItemName
      If Not IsMissing(PropertiesMenuName) Then
        .PropertiesMenuName = PropertiesMenuName
      End If
    End If
  End With
  
  'set the properties passed into the method
  mCol.Add objNewMember, Key

  'return the object created
  Set Add = objNewMember
  Set objNewMember = Nothing
End Function


Public Property Get Item(Index As Variant) As View
Attribute Item.VB_UserMemId = 0
On Error Resume Next
  Set Item = mCol(Index)
  Err.Clear
End Property


Public Property Get Count() As Long
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property

Public Property Get ContainsKey(Key As String) As Boolean
  ContainsKey = Not Item(Key) Is Nothing
End Property


Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub

