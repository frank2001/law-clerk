VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPropertiesEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Properties"
   ClientHeight    =   4992
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   5568
   Icon            =   "frmPropertiesEdit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4992
   ScaleWidth      =   5568
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   4296
      Left            =   84
      TabIndex        =   0
      Top             =   144
      Width           =   5388
      _ExtentX        =   9504
      _ExtentY        =   7578
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   420
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmPropertiesEdit.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabel(5)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "txtSource"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraLine(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Advanced"
      TabPicture(1)   =   "frmPropertiesEdit.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lvAdvanced"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   0
         Left            =   156
         TabIndex        =   2
         Top             =   1020
         Width           =   5076
      End
      Begin VB.TextBox txtSource 
         Height          =   288
         Left            =   1032
         Locked          =   -1  'True
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   564
         Width           =   3924
      End
      Begin MSComctlLib.ListView lvAdvanced 
         Height          =   3792
         Left            =   -74856
         TabIndex        =   6
         Top             =   372
         Width           =   4968
         _ExtentX        =   8763
         _ExtentY        =   6689
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Name"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Value"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Source:"
         Height          =   192
         Index           =   5
         Left            =   240
         TabIndex        =   3
         Top             =   612
         Width           =   552
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   4248
      TabIndex        =   4
      Top             =   4560
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   336
      Left            =   2988
      TabIndex        =   5
      Top             =   4560
      Width           =   1200
   End
End
Attribute VB_Name = "frmPropertiesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mDataField As Collection
Private mSource As String



Public Property Set DataField(vData As Collection)
  Set mDataField = vData
End Property


Public Sub ShowProperties(Source As String, DataField As Collection)
  mSource = Source
  Set mDataField = DataField
  Show
End Sub

Public Property Let Source(vData As String)
  mSource = vData
End Property



Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOK_Click()
  Unload Me
End Sub


Private Sub Form_Load()
  Caption = mSource & " Properties"
  txtSource = mSource
  
  lvAdvanced.ListItems.Clear
    
  lvAdvanced.ListItems.Add , , "--Column--"
  Dim Field As DataField
  For Each Field In mDataField
    With lvAdvanced.ListItems.Add(, Field.Name, Field.Name)
      .ListSubItems.Add , , Field.Value
    End With
  Next Field
       
  ListViewModule.AutosizeWidth lvAdvanced
  TabControlEnable SSTab1
End Sub


Private Sub SSTab1_Click(PreviousTab As Integer)
  TabControlEnable SSTab1
End Sub

Private Sub txtSource_LostFocus()
  If cmdOK.Visible Then cmdOK.SetFocus
End Sub
