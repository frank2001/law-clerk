VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GenericFinder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
' ClaimFinder

Option Explicit
Implements DataFinder

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer


Private Property Set DataFinder_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
End Property


Private Property Get DataFinder_Session() As Session
  Set DataFinder_Session = mvarSession
End Property


Private Property Get DataFinder_DataProvider() As DataProvider
  Set DataFinder_DataProvider = mDataProvider
End Property


Private Sub DataFinder_LoadSearch(frm As Object)
  frm.txtWords = Words
  frm.cboIn.Clear
End Sub

' Sets lvView.  Returns a collection of key fields.
Private Function DataFinder_FindByForm(frm As Object) As Collection
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView, True
End Function
