VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFolderPropertiesEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Properties"
   ClientHeight    =   5760
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   5964
   Icon            =   "frmFolderProperties.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   5964
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   5112
      Left            =   84
      TabIndex        =   32
      Top             =   60
      Width           =   5748
      _ExtentX        =   10139
      _ExtentY        =   9017
      _Version        =   393216
      Style           =   1
      TabHeight       =   420
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmFolderProperties.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabel(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblLabel(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLabel(2)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblLabel(3)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblLabel(4)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblLabel(5)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblLocation"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblLabel(6)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblLabel(7)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblLabel(8)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblLabel(9)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtName"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cboDataField(0)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "fraLine(0)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtDataField(1)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "fraLine(1)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "txtDataField(0)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "txtDataField(11)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "txtDataField(3)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "fraLine(2)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "txtDataField(12)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "txtDataField(13)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "txtDataField(10)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "chkDataField(0)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).ControlCount=   24
      TabCaption(1)   =   "Note"
      TabPicture(1)   =   "frmFolderProperties.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtDataField(2)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Workflow"
      TabPicture(2)   =   "frmFolderProperties.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraWorkflow(3)"
      Tab(2).Control(1)=   "fraWorkflow(2)"
      Tab(2).Control(2)=   "fraWorkflow(1)"
      Tab(2).Control(3)=   "fraWorkflow(0)"
      Tab(2).ControlCount=   4
      Begin VB.CheckBox chkDataField 
         Caption         =   "Search"
         DataField       =   "search"
         Height          =   216
         Index           =   0
         Left            =   3936
         TabIndex        =   29
         Top             =   3576
         Width           =   1416
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "popup"
         Height          =   288
         Index           =   10
         Left            =   1032
         TabIndex        =   12
         Text            =   "Text1"
         Top             =   3540
         Width           =   2628
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "popup.open"
         Height          =   288
         Index           =   13
         Left            =   1032
         TabIndex        =   15
         Text            =   "Text1"
         Top             =   4596
         Width           =   2628
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "popup.explore"
         Height          =   288
         Index           =   12
         Left            =   1032
         TabIndex        =   14
         Text            =   "Text1"
         Top             =   4236
         Width           =   2628
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   2
         Left            =   204
         TabIndex        =   36
         Top             =   3420
         Width           =   5076
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "newitem"
         Height          =   288
         Index           =   3
         Left            =   1032
         TabIndex        =   11
         Text            =   "Text1"
         Top             =   2988
         Width           =   2628
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "New"
         Height          =   1116
         Index           =   3
         Left            =   -74820
         TabIndex        =   26
         Top             =   3852
         Width           =   5364
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&New"
            Height          =   336
            Index           =   3
            Left            =   4068
            TabIndex        =   27
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "New settings are used to provide folder and parameter information when creating a new item in this folder."
            Height          =   564
            Index           =   3
            Left            =   156
            TabIndex        =   28
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "Reopen"
         Height          =   1116
         Index           =   2
         Left            =   -74820
         TabIndex        =   23
         Top             =   2688
         Width           =   5364
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&Reopen"
            Height          =   336
            Index           =   2
            Left            =   4068
            TabIndex        =   24
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "Reopen settings are used to provide folder and parameter information when a competed or voided event is reopened in this folder."
            Height          =   564
            Index           =   2
            Left            =   156
            TabIndex        =   25
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "Void"
         Height          =   1116
         Index           =   1
         Left            =   -74820
         TabIndex        =   20
         Top             =   1512
         Width           =   5364
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&Void"
            Height          =   336
            Index           =   1
            Left            =   4068
            TabIndex        =   21
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "Void settings are used to provide folder and parameter information when voiding an event in this folder."
            Height          =   564
            Index           =   1
            Left            =   156
            TabIndex        =   22
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "Complete"
         Height          =   1116
         Index           =   0
         Left            =   -74820
         TabIndex        =   17
         Top             =   348
         Width           =   5364
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&Complete"
            Height          =   336
            Index           =   0
            Left            =   4068
            TabIndex        =   18
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "Complete settings are used to provide folder and parameter information when completing an event in this folder."
            Height          =   564
            Index           =   0
            Left            =   156
            TabIndex        =   19
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "popup.default"
         Height          =   288
         Index           =   11
         Left            =   1032
         TabIndex        =   13
         Text            =   "Text1"
         Top             =   3888
         Width           =   2628
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "filter"
         Height          =   288
         Index           =   0
         Left            =   1032
         TabIndex        =   10
         Text            =   "Text1"
         Top             =   2640
         Width           =   4260
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   1
         Left            =   204
         TabIndex        =   8
         Top             =   2148
         Width           =   5076
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "folderNote"
         Height          =   3564
         Index           =   2
         Left            =   -74820
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Text            =   "frmFolderProperties.frx":0060
         Top             =   648
         Width           =   5112
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "plugin"
         Height          =   288
         Index           =   1
         Left            =   1032
         TabIndex        =   3
         Text            =   "Text1"
         Top             =   2316
         Width           =   2628
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   0
         Left            =   204
         TabIndex        =   2
         Top             =   1008
         Width           =   5076
      End
      Begin VB.ComboBox cboDataField 
         DataField       =   "type"
         Height          =   288
         Index           =   0
         ItemData        =   "frmFolderProperties.frx":0066
         Left            =   1032
         List            =   "frmFolderProperties.frx":0073
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1704
         Width           =   2460
      End
      Begin VB.TextBox txtName 
         DataField       =   "folderName"
         Height          =   288
         Left            =   1032
         TabIndex        =   0
         Text            =   "Text1"
         Top             =   564
         Width           =   3348
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Popup"
         Height          =   192
         Index           =   9
         Left            =   180
         TabIndex        =   39
         Top             =   3588
         Width           =   480
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Explore"
         Height          =   192
         Index           =   8
         Left            =   180
         TabIndex        =   38
         Top             =   4296
         Width           =   552
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Open"
         Height          =   192
         Index           =   7
         Left            =   180
         TabIndex        =   37
         Top             =   4668
         Width           =   396
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "New"
         Height          =   192
         Index           =   6
         Left            =   180
         TabIndex        =   35
         Top             =   3024
         Width           =   324
      End
      Begin VB.Label lblLocation 
         AutoSize        =   -1  'True
         Caption         =   "location"
         Height          =   192
         Left            =   1056
         TabIndex        =   34
         Top             =   1200
         Width           =   4176
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Name:"
         Height          =   192
         Index           =   5
         Left            =   180
         TabIndex        =   33
         Top             =   600
         Width           =   480
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Default"
         Height          =   192
         Index           =   4
         Left            =   180
         TabIndex        =   16
         Top             =   3936
         Width           =   504
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Filter"
         Height          =   192
         Index           =   3
         Left            =   180
         TabIndex        =   9
         Top             =   2724
         Width           =   348
      End
      Begin VB.Label lblLabel 
         Caption         =   "Type:"
         Height          =   288
         Index           =   2
         Left            =   180
         TabIndex        =   7
         Top             =   1716
         Width           =   744
      End
      Begin VB.Label lblLabel 
         Caption         =   "Location:"
         Height          =   288
         Index           =   1
         Left            =   180
         TabIndex        =   6
         Top             =   1176
         Width           =   864
      End
      Begin VB.Label lblLabel 
         Caption         =   "Plug In"
         Height          =   288
         Index           =   0
         Left            =   180
         TabIndex        =   4
         Top             =   2340
         Width           =   792
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   336
      Left            =   4608
      TabIndex        =   31
      Top             =   5292
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   3300
      TabIndex        =   30
      Top             =   5292
      Width           =   1200
   End
End
Attribute VB_Name = "frmFolderPropertiesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private mFullPath As String
Private mRemotePath As String
Private mFolderName As String
Private mDirty As Boolean
Private mData As DataProvider
Private mOK As Boolean
Private mDataControls As Collection

Private Property Let Dirty(vData As Boolean)
  If mDirty = vData Then Exit Property
  mDirty = vData
End Property

Public Property Get FolderFilter() As String
  If Not mDataControls Is Nothing Then
    FolderFilter = mDataControls("filter")
  End If
End Property

Public Property Get FolderPlugIn() As String
  If Not mDataControls Is Nothing Then
    FolderPlugIn = mDataControls("plugin")
  End If
End Property

Public Sub FormRefresh()
  Caption = mFolderName & " Properties"
  txtName.Text = mFolderName
  lblLocation.Caption = mRemotePath
  
  Dim DataFields As Collection
  Set DataFields = Factory.PropertiesService.Find(mRemotePath, "vb.properties")
  
  SetFields txtDataField, DataFields
  SetFields cboDataField, DataFields
  SetFields chkDataField, DataFields
 
  Dirty = False
End Sub


Private Function Save() As Boolean
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  Set mDataControls = New Collection
  AddTextControl txtDataField, mDataControls
  AddTextControl cboDataField, mDataControls
  AddValueControl chkDataField, mDataControls
  
'  If mFolderName <> mDataControls("folderName") Then
'    Save = Factory.FolderService.RenameFolder(mFullPath, mDataControls("folderName"))
'    If Not Save Then Exit Function
'  End If
  
  
  Save = Factory.PropertiesService.WriteProperties(mRemotePath, "vb.properties", mDataControls)
  
  If Save Then
    Dirty = False
  End If
End Function


Public Sub ShowFolder(FolderName As String, FullPath As String)
  mFolderName = FolderName
  mFullPath = FullPath
  mRemotePath = RemotePath(FullPath)
  Show
End Sub


Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
  
  'To Do: Store previous values in case a mistake is made.
  If cboDataField(Index).Text = "General" Then
    ClearDataField txtDataField
  End If
End Sub

Private Sub chkSearch_Click()
  Dirty = True
End Sub

Private Sub chkDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOK_Click()
  If mDirty Then
    If Not Save Then Exit Sub
    mOK = True
    MsgBox "You need to restart the program for changes to take effect.", vbOKOnly
  End If
  Unload Me
End Sub

Private Sub cmdWorkflow_Click(Index As Integer)
  FolderUtil.WorkflowShow mFullPath, Index
End Sub

Private Sub Form_Initialize()
  Set mData = LawSession.NewDataProvider(DatabaseName.FOLDER_TABLE)
End Sub


Private Sub Form_Load()
  If (mFullPath = "") Then
    MsgBox "Must use ShowFolder to call form"
    Unload Me
    Exit Sub
  End If
  
  TabControlEnable SSTab1
  
  FormRefresh
End Sub


Private Sub SSTab1_Click(PreviousTab As Integer)
  TabControlEnable SSTab1
End Sub

Private Sub txtDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtFilter_Change()
  Dirty = True
End Sub

Private Sub txtNewItem_Change()
  Dirty = True
End Sub

Private Sub txtPopup_Change(Index As Integer)
  Dirty = True
End Sub
