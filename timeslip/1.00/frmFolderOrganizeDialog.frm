VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFolderOrganizeDialog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Organize Folders"
   ClientHeight    =   4068
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   5160
   Icon            =   "frmFolderOrganizeDialog.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4068
   ScaleWidth      =   5160
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picFolder 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3540
      Left            =   0
      ScaleHeight     =   3540
      ScaleWidth      =   5160
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   0
      Width           =   5160
      Begin VB.CommandButton cmdMoveUp 
         Caption         =   "Move &Up"
         Height          =   336
         Left            =   3876
         TabIndex        =   1
         Top             =   84
         Width           =   1200
      End
      Begin VB.CommandButton cmdMoveDown 
         Caption         =   "Move &Down"
         Height          =   336
         Left            =   3864
         TabIndex        =   2
         Top             =   480
         Width           =   1200
      End
      Begin VB.CommandButton cmdCreateFolder 
         Caption         =   "&New Folder"
         Height          =   336
         Left            =   3876
         TabIndex        =   3
         Top             =   1044
         Width           =   1200
      End
      Begin VB.CommandButton cmdMoveToFolder 
         Caption         =   "Mo&ve to Folder"
         Height          =   336
         Left            =   3876
         TabIndex        =   4
         Top             =   1428
         Width           =   1200
      End
      Begin VB.CommandButton cmdRename 
         Caption         =   "&Rename"
         Height          =   336
         Left            =   3876
         TabIndex        =   5
         Top             =   2016
         Width           =   1200
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "D&elete"
         Height          =   336
         Left            =   3876
         TabIndex        =   6
         Top             =   2388
         Width           =   1200
      End
      Begin MSComctlLib.TreeView tvFolder 
         Height          =   3384
         Left            =   60
         TabIndex        =   0
         Top             =   48
         Width           =   3708
         _ExtentX        =   6541
         _ExtentY        =   5969
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   494
         LabelEdit       =   1
         PathSeparator   =   "/"
         Style           =   7
         BorderStyle     =   1
         Appearance      =   1
      End
   End
   Begin VB.PictureBox picOkCancel 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   528
      Left            =   0
      ScaleHeight     =   528
      ScaleWidth      =   5160
      TabIndex        =   7
      Top             =   3540
      Width           =   5160
      Begin VB.Frame fraLine 
         Height          =   36
         Left            =   12
         TabIndex        =   10
         Top             =   0
         Width           =   5076
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Close"
         Default         =   -1  'True
         Height          =   336
         Left            =   3840
         TabIndex        =   8
         Top             =   120
         Width           =   1200
      End
   End
End
Attribute VB_Name = "frmFolderOrganizeDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'
' General Methods
'

Public Sub ButtonRefresh()
  With tvFolder.SelectedItem
    cmdMoveUp.Enabled = Not .Previous Is Nothing
    cmdMoveDown.Enabled = Not .Next Is Nothing
  End With
  tvFolder.SetFocus
End Sub

Public Sub ShowFolder(Key As String)
  Load Me
  tvFolder.Nodes(Key).Selected = True
  Show vbModal
End Sub

Private Sub Swap(Node As Node, Relative As Node, Relationship As Integer)
  If Relationship = tvwChild Then
    If Not Factory.FolderService.MoveFolder(Node.Key, Relative.Key) Then
      Exit Sub
    End If
  ElseIf Not Factory.FolderService.SwapFolder(Node.Key, Relative.Key) Then
    Exit Sub
  End If
  
  ' Remove node and add back in and then add children.
  tvFolder.Nodes.Remove Node.Key
  Set Node = tvFolder.Nodes.Add(Relative, Relationship, Node.Key, Node.Text, Node.Image, Node.SelectedImage)
  FolderUtil.TreeViewLoadNodes tvFolder, Node, FolderUtil.FolderNodes(Node.Key)
  
  Node.Selected = True
  ButtonRefresh
End Sub
'
' End General Methods
'
'
' Control Methods
'

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Sub cmdCreateFolder_Click()
  Factory.FolderService.AddFolder tvFolder
  ButtonRefresh
End Sub

Private Sub cmdDelete_Click()
  Factory.FolderService.RemoveFolder tvFolder
  ButtonRefresh
End Sub

Private Sub cmdMoveDown_Click()
  Swap tvFolder.SelectedItem, tvFolder.SelectedItem.Next, tvwNext
End Sub

Private Sub cmdMoveToFolder_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowMove(tvFolder.SelectedItem.Key) Then
    Swap tvFolder.SelectedItem, tvFolder.Nodes(fForm.FolderKey), tvwChild
  End If
  Unload fForm
  ButtonRefresh
End Sub

Private Sub cmdMoveUp_Click()
  Swap tvFolder.SelectedItem, tvFolder.SelectedItem.Previous, tvwPrevious
End Sub

Private Sub cmdRename_Click()
  tvFolder.StartLabelEdit
End Sub

Private Sub Form_Load()
  FolderUtil.TreeViewLoadFolder tvFolder
End Sub

Private Sub tvFolder_AfterLabelEdit(Cancel As Integer, NewString As String)
  If Not DirNameSafe(NewString) Then
    Cancel = True
    Exit Sub
  End If
  Cancel = Not Factory.FolderService.RenameFolder(tvFolder.SelectedItem.Key, NewString)
End Sub

Private Sub tvFolder_NodeClick(ByVal Node As MSComctlLib.Node)
  ButtonRefresh
End Sub
