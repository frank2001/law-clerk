VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmTimeSlipEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Title"
   ClientHeight    =   4644
   ClientLeft      =   120
   ClientTop       =   660
   ClientWidth     =   5496
   Icon            =   "frmTimeSlipEdit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4644
   ScaleWidth      =   5496
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4080
      Left            =   0
      ScaleHeight     =   4080
      ScaleWidth      =   5496
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   384
      Width           =   5496
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1536
         TabIndex        =   10
         Top             =   3648
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   2796
         TabIndex        =   11
         Top             =   3648
         Width           =   1188
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   4032
         TabIndex        =   12
         Top             =   3648
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3528
         Left            =   84
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   84
         Width           =   5136
         _ExtentX        =   9059
         _ExtentY        =   6223
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "frmTimeSlipEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel(6)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel(7)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtpEndTime"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtpDataField(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtpStartTime"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboDataField(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboDataField(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtpDataField(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtDataField(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtDataField(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtDataField(2)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtDataField(3)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).ControlCount=   17
         Begin VB.TextBox txtDataField 
            DataField       =   "serviceDesc"
            Height          =   636
            Index           =   3
            Left            =   180
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   9
            Text            =   "frmTimeSlipEdit.frx":0028
            Top             =   2784
            Width           =   4692
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            DataField       =   "serviceAmount"
            Height          =   288
            Index           =   2
            Left            =   2508
            TabIndex        =   8
            Text            =   "serviceAmount"
            Top             =   2388
            Width           =   1056
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            DataField       =   "serviceRate"
            Height          =   288
            Index           =   1
            Left            =   1356
            TabIndex        =   7
            Text            =   "serviceRate"
            Top             =   2388
            Width           =   1056
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            DataField       =   "serviceHours"
            Height          =   288
            Index           =   0
            Left            =   180
            TabIndex        =   6
            Text            =   "serviceHours"
            Top             =   2388
            Width           =   1056
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "startTime"
            Height          =   288
            Index           =   0
            Left            =   1488
            TabIndex        =   3
            Top             =   1140
            Width           =   1848
            _ExtentX        =   3260
            _ExtentY        =   508
            _Version        =   393216
            Format          =   24576001
            CurrentDate     =   37354
         End
         Begin VB.ComboBox cboDataField 
            DataField       =   "resourceId"
            Height          =   288
            Index           =   1
            ItemData        =   "frmTimeSlipEdit.frx":002E
            Left            =   3468
            List            =   "frmTimeSlipEdit.frx":003B
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   420
            Width           =   1368
         End
         Begin VB.ComboBox cboDataField 
            DataField       =   "projectId"
            Height          =   288
            Index           =   0
            ItemData        =   "frmTimeSlipEdit.frx":0057
            Left            =   972
            List            =   "frmTimeSlipEdit.frx":0061
            Style           =   2  'Dropdown List
            TabIndex        =   0
            Top             =   420
            Width           =   1368
         End
         Begin MSComCtl2.DTPicker dtpStartTime 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "h:nn AM/PM"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   288
            Left            =   168
            TabIndex        =   2
            Top             =   1128
            Width           =   1212
            _ExtentX        =   2138
            _ExtentY        =   508
            _Version        =   393216
            Format          =   24576002
            CurrentDate     =   37354
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "endTime"
            Height          =   288
            Index           =   1
            Left            =   1476
            TabIndex        =   5
            Top             =   1776
            Width           =   1848
            _ExtentX        =   3260
            _ExtentY        =   508
            _Version        =   393216
            Format          =   24576001
            CurrentDate     =   37354
         End
         Begin MSComCtl2.DTPicker dtpEndTime 
            Height          =   288
            Left            =   180
            TabIndex        =   4
            Top             =   1776
            Width           =   1212
            _ExtentX        =   2138
            _ExtentY        =   508
            _Version        =   393216
            Format          =   24576002
            CurrentDate     =   37354
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Amount"
            Height          =   192
            Index           =   7
            Left            =   3000
            TabIndex        =   25
            Top             =   2160
            Width           =   540
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Rate"
            Height          =   192
            Index           =   6
            Left            =   1920
            TabIndex        =   24
            Top             =   2160
            Width           =   348
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Hours:"
            Height          =   192
            Index           =   5
            Left            =   732
            TabIndex        =   23
            Top             =   2160
            Width           =   468
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Ending time and date:"
            Height          =   192
            Index           =   3
            Left            =   180
            TabIndex        =   22
            Top             =   1536
            Width           =   1548
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Starting time and date:"
            Height          =   192
            Index           =   2
            Left            =   156
            TabIndex        =   21
            Top             =   852
            Width           =   1584
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Resource"
            Height          =   192
            Index           =   1
            Left            =   2628
            TabIndex        =   20
            Top             =   468
            Width           =   708
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Project"
            Height          =   192
            Index           =   0
            Left            =   144
            TabIndex        =   19
            Top             =   468
            Width           =   504
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   5496
      _ExtentX        =   9694
      _ExtentY        =   677
      BandCount       =   1
      _CBWidth        =   5496
      _CBHeight       =   384
      _Version        =   "6.7.8988"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   336
         ScaleWidth      =   5400
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   24
         Width           =   5400
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   24
            TabIndex        =   17
            Top             =   24
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   402
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   1572
      TabIndex        =   18
      Top             =   456
      Width           =   1056
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   16777215
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   624
      Top             =   96
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator998 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About..."
      End
   End
End
Attribute VB_Name = "frmTimeSlipEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mTimeSlipHandle As String

Private mNew As Boolean
Private mListView As ListView
Private mListIndex As Integer

Private mDirty As Boolean
Private mPlugIn As TimeSlipPlugIn

Private mHandleNames(0) As String
Private mAttributeNames(8) As String

Public FormMgr As New FormManager


Private Sub AmountRefresh()
  txtDataField(0).Text = Round((dtpDataField(1).Value - dtpDataField(0).Value) * 24, 2)
  txtDataField(2).Text = Round(GetDouble(txtDataField(0).Text) * GetDouble(txtDataField(1).Text), 2)
End Sub

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Public Property Set PlugIn(vData As Object)
  Set mPlugIn = vData
End Property

'
' FormInterface Interface
'

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  Dim Pointer As New WaitPointer
  Pointer.Pointer = vbHourglass
  
  If mNew Then
    Caption = "New Time Slip"
    TimeRefresh
    AmountRefresh
    mTimeSlipHandle = mPlugIn.AutoKeyService.GetDocumentKey
  Else
    Caption = "Edit Time Slip (" & mTimeSlipHandle & ")"
    Dim DataFields As Collection
    Set DataFields = mPlugIn.GenericService.Find(TimeSlipTable, TimeSlipKey, mTimeSlipHandle)
    SetFields cboDataField, DataFields
    SetFields dtpDataField, DataFields
    SetFields txtDataField, DataFields
    TimeRefresh
  End If
  
  mnuFileProperties.Enabled = Not mNew
  
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' Record is no longer dirty.
  Dirty = False
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mTimeSlipHandle = ListItemText(mListView.SelectedItem, TimeSlipKey)
End Property

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  Dim DataControls As New Collection
  AddValueControl dtpDataField, DataControls
  AddTextControl txtDataField, DataControls
  AddTextControl cboDataField, DataControls
  
  Dim AttributeValues(8) As String, I As Integer
  AttributeValues(0) = mTimeSlipHandle
  For I = 1 To 8
    AttributeValues(I) = DataControls(mAttributeNames(I))
  Next I
  AttributeValues(3) = Format(AttributeValues(3), "yyyy-mm-dd hh:mm:ss")
  AttributeValues(4) = Format(AttributeValues(4), "yyyy-mm-dd hh:mm:ss")
  
  Dim Handle As String
  If mNew Then
    Handle = mPlugIn.GenericService.Update( _
      TimeSlipTable, _
      vbNullStringArray, vbNullStringArray, _
      mAttributeNames, AttributeValues, _
      vbNullStringArray, vbNullStringArray)
  Else
    Dim HandleValues(0) As String
    HandleValues(0) = mTimeSlipHandle
    Handle = mPlugIn.GenericService.Update( _
      TimeSlipTable, _
      mHandleNames, HandleValues, _
      mAttributeNames, AttributeValues, _
      vbNullStringArray, vbNullStringArray)
  End If
    
  Save = Not VarType(Handle) = vbEmpty
  If Save Then
    mNew = False
  End If
  
  If Save Then
    FormRefresh
  Else
    Dirty = False
  End If
End Function
          
Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no law areas to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub


Public Sub ShowByPrimaryKey(TimeSlipHandle As String)
  mTimeSlipHandle = TimeSlipHandle
  Show
End Sub


Public Sub ShowNew()
  mNew = True
  Show
End Sub


Public Sub TimeRefresh()
  dtpStartTime.Value = dtpDataField(0).Value
  dtpEndTime.Value = dtpDataField(1).Value
End Sub
'
'
' End General
'
'
'
'
' Control Methods
'

Private Sub cboDataField_Scroll(Index As Integer)
  Dirty = True
  Select Case cboDataField(1).Text
    Case "BDS001"
      txtDataField(1).Text = "35"
    Case "JES001"
      txtDataField(1).Text = "30"
    Case "SRD001"
      txtDataField(1).Text = IIf(cboDataField(1).Text = "IMPL", "95", "100")
  End Select

End Sub

Private Sub cboDataField_Validate(Index As Integer, Cancel As Boolean)
  cboDataField_Scroll Index
End Sub

'
' Standard Cancel Command Button.
'
Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

Private Sub cmdSaveNew_Click()
  If Save Then mnuFileNew_Click
End Sub


Private Sub dtpDataField_Change(Index As Integer)
  Select Case Index
    Case 0
      dtpDataField(1).Value = dtpDataField(0).Value
  End Select
  TimeRefresh
  AmountRefresh
End Sub


Private Sub dtpEndTime_Change()
  dtpDataField(1).Value = dtpEndTime.Value
  AmountRefresh
End Sub


Private Sub dtpStartTime_Change()
  dtpDataField(0).Value = dtpStartTime.Value
  If dtpDataField(1).Value < dtpDataField(0).Value Then
    dtpDataField(1).Value = dtpDataField(0).Value
  End If
  AmountRefresh
End Sub


Private Sub Form_Initialize()
  mHandleNames(0) = TimeSlipKey
  mAttributeNames(0) = TimeSlipKey
  mAttributeNames(1) = "projectId"
  mAttributeNames(2) = "resourceId"
  mAttributeNames(3) = "startTime"
  mAttributeNames(4) = "endTime"
  mAttributeNames(5) = "serviceHours"
  mAttributeNames(6) = "serviceRate"
  mAttributeNames(7) = "serviceAmount"
  mAttributeNames(8) = "serviceDesc"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
    
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
    
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub


Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  FormRefresh
  dtpStartTime.SetFocus
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub


Private Sub txtDataField_Change(Index As Integer)
  Dirty = True
End Sub

