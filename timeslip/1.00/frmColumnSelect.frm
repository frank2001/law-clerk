VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmColumnSettingsDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Column Settings"
   ClientHeight    =   5112
   ClientLeft      =   36
   ClientTop       =   312
   ClientWidth     =   5064
   Icon            =   "frmColumnSelect.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5112
   ScaleWidth      =   5064
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAdvanced 
      Caption         =   "&Advanced"
      Height          =   336
      Left            =   3696
      TabIndex        =   14
      Top             =   4668
      Width           =   1200
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select &All"
      Height          =   336
      Index           =   0
      Left            =   3672
      TabIndex        =   5
      Top             =   2408
      Width           =   1200
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select &None"
      Height          =   336
      Index           =   1
      Left            =   3672
      TabIndex        =   6
      Top             =   2812
      Width           =   1200
   End
   Begin VB.Frame fraLine 
      Height          =   36
      Left            =   120
      TabIndex        =   13
      Top             =   4464
      Width           =   4788
   End
   Begin VB.ComboBox cboItem 
      Height          =   288
      Left            =   156
      TabIndex        =   9
      Text            =   "cboItem"
      Top             =   3960
      Width           =   2124
   End
   Begin VB.CommandButton cmdProperties 
      Caption         =   "&Properties"
      Height          =   336
      Left            =   3672
      TabIndex        =   7
      Top             =   3216
      Width           =   1200
   End
   Begin VB.CommandButton cmdHide 
      Caption         =   "&Hide"
      Height          =   336
      Left            =   3672
      TabIndex        =   4
      Top             =   2004
      Width           =   1200
   End
   Begin VB.CommandButton cmdShow 
      Caption         =   "&Show"
      Height          =   336
      Left            =   3672
      TabIndex        =   3
      Top             =   1600
      Width           =   1200
   End
   Begin MSComctlLib.ListView lvColumn 
      Height          =   2772
      Left            =   168
      TabIndex        =   0
      Top             =   792
      Width           =   3348
      _ExtentX        =   5906
      _ExtentY        =   4890
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   336
      Left            =   2388
      TabIndex        =   11
      Top             =   4668
      Width           =   1200
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   1068
      TabIndex        =   10
      Top             =   4668
      Width           =   1200
   End
   Begin VB.CommandButton cmdItemDown 
      Caption         =   "Move &Down"
      Height          =   336
      Left            =   3672
      TabIndex        =   2
      Top             =   1196
      Width           =   1200
   End
   Begin VB.CommandButton cmdItemUp 
      Caption         =   "Move &Up"
      Height          =   336
      Left            =   3672
      TabIndex        =   1
      Top             =   792
      Width           =   1200
   End
   Begin VB.Label lblInstructions 
      AutoSize        =   -1  'True
      Caption         =   "Select the column you would like highlighted:"
      Height          =   192
      Index           =   1
      Left            =   144
      TabIndex        =   8
      Top             =   3696
      Width           =   3156
   End
   Begin VB.Label lblInstructions 
      Caption         =   $"frmColumnSelect.frx":000C
      Height          =   588
      Index           =   0
      Left            =   204
      TabIndex        =   12
      Top             =   108
      Width           =   4680
   End
End
Attribute VB_Name = "frmColumnSettingsDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOK As Boolean
Private mDirty As Boolean
Private mFolderName As String
Private mSource As String
Private mSaveAsDefault As Boolean


Public Sub AddItem(Index As Integer, Key As String, Text As String, Optional Alignment As Integer, Optional Width As Integer = 1440, Optional Position As Integer, Optional Tag As String, Optional Required As Boolean)
  cboItem.AddItem Text
  
  With lvColumn.ListItems.Add(, Key, Text)
    .Checked = (Position > 0)
    .Tag = Tag
  
    Dim SortKey As String
    SortKey = IIf(Position = 0, "B" & Text, "A" & Format(Position, "000"))
    .ListSubItems.Add , "SortKey", SortKey
    .ListSubItems.Add , "Index", Index
    .ListSubItems.Add , "Alignment", Alignment
    .ListSubItems.Add , "Width", Width
    .ListSubItems.Add , "Required", Required Or Right(Key, 2) = "Id"
  End With
End Sub


'
' Refresh which buttons are enabled.
'
Public Sub ButtonRefresh()
  With lvColumn.SelectedItem
    cmdItemUp.Enabled = (.Index > 1)
    cmdItemDown.Enabled = (.Index < lvColumn.ListItems.Count)
    cmdShow.Enabled = Not .Checked
    cmdHide.Enabled = .Checked And Not .ListSubItems("Required")
  End With
End Sub


'
' Refresh list from DataFinder
'
Public Sub ColumnRefresh()
  Dim mFinder As DataFinder
  Set mFinder = NewGenericFinder(mSource)
  mFinder.DataProvider.OpenRecordset 1
  
  Dim Collection As Collection
  Set Collection = mFinder.DataProvider.Collection
  
  ' Scan through collection.
  Dim Item As Variant
  For Each Item In Collection(1)
    If Not Contains(lvColumn.ListItems, Item.Name) Then
      If MsgBox("Add column " & Item.Name, vbOKCancel, "Add Column") = vbOK Then
        AddItem lvColumn.ListItems.Count + 1, Item.Name, Item.Name
      End If
    End If
  Next Item
  
  ' Scan through list items to see if any should be removed.
  ' Must store keys to remove because we can't do it within a for each.
  Dim Remove As New Collection
  For Each Item In lvColumn.ListItems
    If Not Contains(Collection(1), Item.Key) Then
      If MsgBox("Remove column " & Item.Key, vbOKCancel, "Remove Column") = vbOK Then
        Remove.Add Item
      End If
    End If
  Next Item
  For Each Item In Remove
    lvColumn.ListItems.Remove Item.Index
  Next Item
  mDirty = True
End Sub


Public Property Get Dirty() As Boolean
  Dirty = mDirty
End Property

Public Property Let FolderName(vData As String)
  mFolderName = vData
End Property


Public Sub KillPersonal()
  Dim FileName As String
  FileName = FolderIniFile(mFolderName, True)
  If FileUtil.IsFile(FileName) Then
    FileSystem.Kill FileName
  End If
End Sub


Public Property Get OK() As Boolean
  OK = mOK
End Property


Public Sub RestoreDefault()
  KillPersonal
  
  lvColumn.ListItems.Clear
  Dim Ini As New IniFile
  Ini.FileName = FolderIniFileActive(mFolderName)
  LoadItems Ini
  mDirty = False
End Sub


'
' Method requires that the section have at least one column.
'
Public Sub LoadItems(Ini As IniFile)
  ListViewModule.DeserializeColumnSettings Me, Ini, mFolderName
  
  lvColumn.SortKey = 1
  lvColumn.Sorted = True
  lvColumn.ListItems(1).Selected = True

  ButtonRefresh
  lvColumn_ItemClick Nothing
  cboItem.ListIndex = 0
End Sub

Public Property Get SaveAsDefault() As Boolean
  SaveAsDefault = mSaveAsDefault
End Property

Public Property Let Source(vData As String)
  mSource = vData
End Property


'
' Swap simply swaps the SortKey of the selected item with the
' desired index
'
Private Sub Swap(Index As Integer)
  lvColumn.Sorted = False
  
  Dim Selected As ListItem
  Set Selected = lvColumn.SelectedItem
  
  Dim Item As ListItem
  Set Item = lvColumn.ListItems(Index)
  
  Dim SortKey As String
  SortKey = Selected.ListSubItems("SortKey")
  Selected.ListSubItems("SortKey") = Item.ListSubItems("SortKey")
  Item.ListSubItems("SortKey") = SortKey
  
  lvColumn.Sorted = True
  lvColumn.ListItems(Index).Selected = True
End Sub

Private Sub cmdAdvanced_Click()
  Dim Advanced As New frmColumnSettingsAdvancedDialog
  Set Advanced.ColumnSettingsDialog = Me
  Advanced.Show vbModal
  If Advanced.OK Then
    mSaveAsDefault = Advanced.chkDefault
  End If
  Unload Advanced
End Sub

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Sub cmdHide_Click()
  lvColumn.SelectedItem.Checked = False
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdItemDown_Click()
  Swap lvColumn.SelectedItem.Index + 1
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdItemUp_Click()
  Swap lvColumn.SelectedItem.Index - 1
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdOK_Click()
  mOK = True
  Hide
End Sub

Private Sub cmdProperties_Click()
  Dim fProperties As New frmColumnPropertiesDialog
  fProperties.ShowListItem lvColumn.SelectedItem
  fProperties.Show vbModal
  If fProperties.Dirty Then
    mDirty = True
  End If
  Unload fProperties
End Sub

Private Sub cmdSelect_Click(Index As Integer)
  Dim Item As ListItem
  For Each Item In lvColumn.ListItems
    If Not Item.ListSubItems("Required") Then
      Item.Checked = Index = 0
    End If
  Next Item
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdShow_Click()
  lvColumn.SelectedItem.Checked = True
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub


Private Sub Form_Load()
  mDirty = False
End Sub

Private Sub lvColumn_DblClick()
  cmdProperties_Click
End Sub

Private Sub lvColumn_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  If Not Item.Checked And Item.ListSubItems("Required") Then
    Beep
    MsgBox "The column you have selected is required. To hide the column set the column width to 0 under properties.", vbOKOnly, "Required Column"
    Item.Checked = True
  End If
  ButtonRefresh
End Sub

Private Sub lvColumn_ItemClick(ByVal Item As MSComctlLib.ListItem)
  ButtonRefresh
End Sub

Public Property Get Column() As ListView
  Set Column = lvColumn
End Property
