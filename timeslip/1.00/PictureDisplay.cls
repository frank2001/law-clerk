VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PictureDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mUp As IPictureDisp
Private mDown As IPictureDisp
Private mUpDisabled As IPictureDisp
Private mDownDisabled As IPictureDisp

Public Property Get DownDisabled() As IPictureDisp
  Set DownDisabled = mDownDisabled
End Property

Public Property Get DownEnabled() As IPictureDisp
  Set DownEnabled = mDown
End Property

Public Property Get UpEnabled() As IPictureDisp
  Set UpEnabled = mUp
End Property

Public Property Get UpDisabled() As IPictureDisp
  Set UpDisabled = mUpDisabled
End Property

Private Sub Class_Initialize()
  Set mUp = LoadPicture(App.Path + "\" + IniMain.ReadFile("Images", "Up"))
  Set mDown = LoadPicture(App.Path + "\" + IniMain.ReadFile("Images", "Down"))
  Set mUpDisabled = LoadPicture(App.Path + "\" + IniMain.ReadFile("Images", "UpDisabled"))
  Set mDownDisabled = LoadPicture(App.Path + "\" + IniMain.ReadFile("Images", "DownDisabled"))
End Sub
