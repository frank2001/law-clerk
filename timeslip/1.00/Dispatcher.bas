Attribute VB_Name = "Dispatcher"
Option Explicit

Public Sub ShowNewForm(Form As Form, Optional Handle, Optional FormMgr)
  If Not IsMissing(FormMgr) Then FormMgr.Add Form
  If IsMissing(Handle) Then
    Form.ShowNew
  Else
    Form.ShowNew CStr(Handle)
  End If
End Sub

Public Sub ShowNew(Source As String, Optional Handle, Optional FormMgr)
'  Select Case Source
'    Case DatabaseName.APPLICATION_TABLE
'      ShowNewForm New frmApplicationEdit, Handle, FormMgr
'
'    Case DatabaseName.APPLICATIONLAWAREA_TABLE
'      ShowNewForm New frmApplicationLawAreaEdit, Handle, FormMgr
'
'    Case DatabaseName.APPLICATIONLAWYER_TABLE
'      ShowNewForm New frmApplicationLawyerEdit, Handle, FormMgr
'
'    Case DatabaseName.APPLICATIONLIMIT_TABLE
'      ShowNewForm New frmApplicationLimitEdit, Handle, FormMgr
'
'    Case DatabaseName.APPLICATIONQUESTION_TABLE
'      ShowNewForm New frmApplicationQuestionEdit, Handle, FormMgr
'
'    Case DatabaseName.CLAIMPAYMENT_TABLE
'      ShowNewForm New frmClaimPaymentEdit, Handle, FormMgr
'
'    Case DatabaseName.CLAIMRESERVE_TABLE
'      Browse HttpServer & WebApplication & WebService & "claimreservenew?key=&key=" + Handle + "&action=create"
'
'    Case DatabaseName.CONTACT_TABLE
'      ShowNewForm New frmContactEdit, Handle, FormMgr
'
'    Case DatabaseName.DEDUCTIBLEREIMBURSE_TABLE
'      ShowNewForm New frmDeductibleReimburseEdit, Handle, FormMgr
'
'    Case DatabaseName.FIRM_TABLE
'      ShowNewForm New frmFirmEdit, Handle, FormMgr
'
'    Case DatabaseName.FIRMINSURER_TABLE
'      ShowNewForm New frmFirmInsurerEdit, Handle, FormMgr
'
'    Case DatabaseName.FIRMLAWYER_TABLE
'      ShowNewForm New frmFirmLawyerEdit, Handle, FormMgr
'
'    Case DatabaseName.GLBATCH_TABLE
'      ShellExecute 0&, vbNullString, HttpServer & WebApplication & WebService & "glbatchnew", vbNullString, _
'        vbNullString, SW_SHOWNORMAL
'
'    Case DatabaseName.LAWYER_TABLE
'      ShowNewForm New frmPersonEdit, True, FormMgr
'
'    Case DatabaseName.LOCATION_TABLE
'      ShowNewForm New frmLocationEdit, , FormMgr
'
'    Case DatabaseName.PERSON_TABLE
'      ShowNewForm New frmPersonEdit, , FormMgr
'
'    Case DatabaseName.POLICYACCOUNTING_TABLE
'      ShowNewForm New frmPolicyAccountingEdit, Handle, FormMgr
'
'  End Select
End Sub


Private Function ShowSearchForm(Form As Form, FolderKey As String, FormMgr As FormManager, Optional IsSelect As Boolean, Optional TabIndex As Integer, Optional SearchInIndex As Integer, Optional SearchText As String) As Form
  If Not FormMgr Is Nothing Then
    FormMgr.Add Form, FolderKey
  End If
  Form.ShowSearch FolderKey, IsSelect, TabIndex, SearchInIndex, SearchText
  Set ShowSearchForm = Form
End Function


Public Function ShowSearch(FolderKey As String, Optional FormMgr As FormManager, Optional IsSelect As Boolean, Optional TabIndex As Integer, Optional SearchInIndex As Integer = -1, Optional SearchText As String) As Form
  Select Case FolderKey
'    Case DatabaseName.FIRM_TABLE
'      ShowSearchForm New frmFirmSearch, DatabaseName.FIRM_TABLE, FormMgr, IsSelect, TabIndex, SearchInIndex, SearchText
    Case Else
      Set ShowSearch = ShowSearchForm(New frmGenericSearch, FolderKey, FormMgr, IsSelect, TabIndex, SearchInIndex, SearchText)
  End Select
  If Not SearchText = vbNullString Then
    ShowSearch.Refresh
    ShowSearch.cmdSearchNow_Click
  End If
End Function
