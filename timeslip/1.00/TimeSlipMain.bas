Attribute VB_Name = "TimeSlipMain"
Option Explicit

Public IniMain As New IniFile
Public IniForm As New IniFile

Public WebService As String
Public ComponentService As String

Public TimeSlipTable As String
Public TimeSlipKey As String

Public vbNullStringArray() As String


Public Sub Main()
  IniMain.FileName = App.Path & "\" & "law.ini"
  
  ' Setup ini files.
  IniForm.FileName = GetRealPath(IniMain.ReadFile("Main", "IniForm"))
  
  WebService = "servlet/rpcrouter"
  ComponentService = "component/"
  
  TimeSlipTable = "TimeSlip"
  TimeSlipKey = "timeSlipId"
End Sub
