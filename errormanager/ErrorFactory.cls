VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ErrorFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'The function of this utility is to create a single
'source to export errors to the user.
'
'This class uses frmErrorDialog
'
Option Explicit

Public Sub DisplaySoapError(Error As ErrObject, Client As SoapClient30)
  Dim fError As New frmErrorDialog
  Load fError
  fError.lblError.Caption = "Fault: " & Error.Description
  fError.txtDescription.Text = Client.Detail
  Beep
  fError.Show vbModal
  Unload fError
End Sub

