VERSION 5.00
Begin VB.Form frmErrorDialog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Error"
   ClientHeight    =   5670
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   7575
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5670
   ScaleWidth      =   7575
   Begin VB.TextBox txtError 
      BackColor       =   &H8000000F&
      Height          =   1155
      Left            =   90
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   105
      Width           =   7365
   End
   Begin VB.PictureBox picDetail 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3648
      Left            =   0
      ScaleHeight     =   3645
      ScaleWidth      =   7575
      TabIndex        =   3
      Top             =   1785
      Width           =   7575
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   336
         Left            =   6270
         TabIndex        =   5
         Top             =   3240
         Width           =   1200
      End
      Begin VB.TextBox txtDescription 
         BackColor       =   &H8000000F&
         Height          =   3132
         Left            =   84
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   4
         Top             =   24
         Width           =   7380
      End
   End
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   1788
      Left            =   0
      ScaleHeight     =   1785
      ScaleWidth      =   7575
      TabIndex        =   0
      Top             =   0
      Width           =   7575
      Begin VB.CommandButton cmdOk 
         Cancel          =   -1  'True
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   4755
         TabIndex        =   2
         Top             =   1380
         Width           =   1296
      End
      Begin VB.CommandButton cmdDetail 
         Caption         =   "Show &Details >>"
         Height          =   336
         Left            =   6150
         TabIndex        =   1
         Top             =   1380
         Width           =   1320
      End
   End
End
Attribute VB_Name = "frmErrorDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'This form is to be used with the error manager for displaying errors
Option Explicit

Private Sub cmdDetail_Click()
  picDetail.Visible = Not picDetail.Visible
  Form_Resize
End Sub

Private Sub cmdOk_Click()
  Hide
End Sub

Private Sub cmdPrint_Click()
   Printer.CurrentY = 1440
   Printer.Print txtDescription.Text
   Printer.EndDoc
End Sub

Private Sub Form_Load()
  picDetail.Visible = False
  Form_Resize
End Sub

Private Sub Form_Resize()
  cmdDetail.Caption = IIf(picDetail.Visible, "Hide &Details <<", "Show &Details >>")
  Height = picBody.Height + IIf(picDetail.Visible, picDetail.Height, 0) + Height - ScaleHeight
End Sub
