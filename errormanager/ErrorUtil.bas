Attribute VB_Name = "ErrorUtil"
'The function of this utility is to create a single
'source to export errors to the user.
'
'This class uses frmErrorDialog
'
Option Explicit

Public Sub DisplayMessage(Message As String, Detail As String, Optional Title As String, Optional isBeep As Boolean = False)
  Dim fError As New frmErrorDialog
  Load fError
  fError.txtError.Text = Message
  fError.txtDescription.Text = Detail
  fError.Caption = IIf(Title <> vbNullString, Title, "Clerk")
  If isBeep Then
    Beep
  End If
  fError.Show vbModal
  Unload fError
End Sub


Public Sub DisplaySoapError(Error As ErrObject, Client As SoapClient30, Optional ServiceName As String)
  If Client Is Nothing Then
    MsgBox "SOAP Client is null. Unable to build error display.", vbCritical
    Exit Sub
  End If

  Dim fError As New frmErrorDialog
  Load fError
  fError.txtError.Text = ServiceName & " Fault: " & Client.FaultString
  fError.txtDescription.Text = "[DESCRIPTION] " & vbCrLf & Error.Description & vbCrLf & vbCrLf & _
                               "[SOURCE] " & vbCrLf & Error.Source & vbCrLf & vbCrLf & _
                               "[SOAP FAULTCODE] " & vbCrLf & Client.FaultCode & vbCrLf & vbCrLf & _
                               "[SOAP DETAIL] " & vbCrLf & Client.Detail
  Beep
  fError.Show vbModal
  Unload fError
End Sub


Public Sub DisplayXMLHttpError(xmlHttpResponse As xmlHttp, url As String)
  
  Dim fError As New frmErrorDialog
  Load fError
  
  'Variables used to store the error message.
  Dim eMessage As String
  Dim eDescription As String
  
  'Test for error node
  Dim errorNode As MSXML2.IXMLDOMNode
  Set errorNode = xmlHttpResponse.responseXML.selectSingleNode("xml/error")
  
  If errorNode Is Nothing Then
    eMessage = "Error durring http communications. Server did not respond to an http request."
    eDescription = "StatusText: " & xmlHttpResponse.StatusText & vbCrLf & _
                   "URI: " & url

  Else
    eMessage = errorNode.selectSingleNode("message").Text
    eDescription = errorNode.selectSingleNode("stack").Text
  End If
  
  fError.txtError.Text = eMessage
  fError.txtDescription.Text = eDescription
  Beep
  fError.Show vbModal
  Unload fError
End Sub

