VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TemplateDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

' DatasetTemplate
' See ToDo: sections for changes when implementing.

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer


Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  
  ' ToDo:  Change to the name of the data source (usually table name)
  mDataProvider.Source = ""
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  'ToDo:  Uncomment below and add appropriate search fields.
  'frm.cboIn.AddItem "in lawyerId field only"
  'frm.cboIn.AddItem "in firm locator field only"
  'frm.cboIn.ListIndex = 0
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection

'  Words = frm.txtWords
'  ListIndex = frm.cboIn.ListIndex
'  Dim newFilter As String
'  If Not Words = "" Then
'    Select Case ListIndex
'      Case 0
'        newFilter = "UPPER(""lawyerId"") like '" + UCase(Words) + "%'"
'      Case 1
'        newFilter = "UPPER(""firmId"") like '" + UCase(Words) + "%'"
'    End Select
'  End If
'  mDataProvider.Filter = newFilter
'  mDataProvider.OpenRecordset
'  mDataProvider.SetListView frm.lvView
End Function
