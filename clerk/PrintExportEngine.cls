VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PrintExportEngine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"PrintListView"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'==============================================================================
'Encapsulates the printing of a ListView.
'
'Scope:     DLL-Public creatable.
'==============================================================================


Option Explicit

Private mListView As ListView 'ListView that should be printed
Private mPageLayout As New PrintPageLayout 'Page setup properties

'Title, header and footer
Dim mTitle As New PrintText, mHeader As New PrintText, mFooter As New PrintText

'Font of the entries
Dim mFont As New StdFont

'print in color, highlight selected entries, only print selected entries
Dim mInColor As Boolean, mMark As Boolean, mOnlySelected As Boolean

Dim Pages As Long 'current count of printed pages

'Object that performs the print of the ListView
Dim WithEvents LVPrinter As PrintListView
Attribute LVPrinter.VB_VarHelpID = -1

'Returns the printer object of that DLL, because other DLLs (can) have a different
'object with other properties
Property Get MyPrinter() As Object
    Set MyPrinter = Printer
End Property

Public Property Get PageLayout() As PrintPageLayout
  Set PageLayout = mPageLayout
End Property

'==============================================================================
'Reason:    Sets the ListView that should be printed. The font that is used to
'           print the ListView-ListItems is automatically retrieved out of the
'           control.
'
'Arguments: - ListView      ListView that should be printed.
'==============================================================================
Property Let ListView(vData As ListView)
  Set mListView = vData
    
  Set mFont = New StdFont
  With mFont
      .Name = mListView.Font.Name
      .Size = mListView.Font.Size
      .Bold = mListView.Font.Bold
      .Italic = mListView.Font.Italic
      .Strikethrough = mListView.Font.Strikethrough
      .Underline = mListView.Font.Underline
  End With
End Property

'The following properties are used to get or set the export-attributes.

Property Get ListView() As ListView
    Set ListView = mListView
End Property

Property Get Title() As PrintText
    Set Title = mTitle
End Property

Property Get Header() As PrintText
    Set Header = mHeader
End Property

Property Get Footer() As PrintText
    Set Footer = mFooter
End Property

Property Get Font() As StdFont
    Set Font = mFont
End Property

Property Get InColor() As Boolean
    InColor = mInColor
End Property

Property Let InColor(ByVal vData As Boolean)
    mInColor = vData
End Property

Property Get Mark() As Boolean
    Mark = mMark
End Property

Property Let Mark(ByVal vData As Boolean)
    mMark = vData
End Property

Property Get OnlySelected() As Boolean
    OnlySelected = mOnlySelected
End Property

Property Let OnlySelected(ByVal vData As Boolean)
    mOnlySelected = vData
End Property

'==============================================================================
'Reason:    Prints the given ListView with the given attributes on the given
'           printer.
'
'Returns:   True if the operation was successful, otherwise False.
'==============================================================================
Function PrintListView() As Boolean
    Set LVPrinter = New PrintListView
    With LVPrinter
        Set .Control = mListView
        Set .FontItem = mFont
        Set .Title = mTitle
        
        Set .PageLayout = mPageLayout
        .PageLayout.Header.Alignment = mHeader.Alignment
        Set .PageLayout.Header.Font = mHeader.Font
        
        .PageLayout.Footer.Alignment = mFooter.Alignment
        Set .PageLayout.Footer.Font = mFooter.Font
        
        Pages = 0
        
        .PrintOnlySelectedItems = mOnlySelected
        
        PrintListView = .PrintControl(mInColor, mMark)
    End With
End Function

Private Sub Class_Initialize()
    InColor = True
    Header.Text = "printed on %datetime%"
    Header.Alignment = vbCenter
    Header.Font.Size = 8
    
    Title.Alignment = vbCenter
    Title.Font.Size = 22
    Title.Font.Bold = True
    
    Footer.Text = "Page %page%"
    Footer.Alignment = vbRightJustify
    Footer.Font.Size = 8
End Sub

'Occures if a new page begins
Private Sub LVPrinter_BeforeNewPage()
    'here headers and footers are updated
    Pages = Pages + 1
    LVPrinter.PageLayout.Header.Text = Replace(mHeader.Text, "%page%", Pages)
    LVPrinter.PageLayout.Footer.Text = Replace(mFooter.Text, "%page%", Pages)
    LVPrinter.PageLayout.Header.Text = Replace(LVPrinter.PageLayout.Header.Text, "%datetime%", Format(Now, "dddd, d. mmmm yyyy, hh:nn"))
    LVPrinter.PageLayout.Footer.Text = Replace(LVPrinter.PageLayout.Footer.Text, "%datetime%%", Format(Now, "dddd, d. mmmm yyyy, hh:nn"))
End Sub
