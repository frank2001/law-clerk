VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmQuoteEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Application Rating Edit"
   ClientHeight    =   4785
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   5070
   Icon            =   "frmQuoteEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   5070
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4305
      Left            =   0
      ScaleHeight     =   4305
      ScaleWidth      =   5070
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   384
      Width           =   5076
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1320
         TabIndex        =   5
         Top             =   3828
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   2556
         TabIndex        =   6
         Top             =   3828
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         CausesValidation=   0   'False
         Height          =   336
         Left            =   3792
         TabIndex        =   7
         Top             =   3828
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3636
         Left            =   96
         TabIndex        =   19
         Top             =   96
         Width           =   4884
         _ExtentX        =   8625
         _ExtentY        =   6403
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmQuoteEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtDataField(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "cmdWorksheet"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cmdCalculate"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtDataField(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmQuoteEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "cmdClearNotepad"
         Tab(1).Control(1)=   "txtNote"
         Tab(1).ControlCount=   2
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74928
            TabIndex        =   4
            Top             =   3252
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   2856
            Left            =   -74940
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   312
            Width           =   4764
         End
         Begin VB.TextBox txtDataField 
            BackColor       =   &H8000000F&
            DataField       =   "ratingBookId"
            DataMember      =   "QuoteBook"
            Height          =   288
            Index           =   1
            Left            =   1224
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Text            =   "ratingBookId"
            Top             =   1068
            Width           =   1164
         End
         Begin VB.CommandButton cmdCalculate 
            Caption         =   "&Calculate"
            Height          =   288
            Left            =   3564
            TabIndex        =   2
            Top             =   1584
            Width           =   1000
         End
         Begin VB.CommandButton cmdWorksheet 
            Caption         =   "&Worksheet"
            Height          =   288
            Left            =   3564
            TabIndex        =   1
            Top             =   1080
            Width           =   1000
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "quoteName"
            DataMember      =   "Quote"
            Height          =   288
            Index           =   0
            Left            =   1212
            TabIndex        =   0
            Text            =   "quoteName"
            Top             =   552
            Width           =   3420
         End
         Begin VB.Label lblLabel 
            Caption         =   "Book:"
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   11
            Top             =   1116
            Width           =   768
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Rating Name:"
            Height          =   192
            Index           =   2
            Left            =   96
            TabIndex        =   10
            Top             =   588
            Width           =   984
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   492
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   456
      Top             =   480
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   396
      Left            =   108
      TabIndex        =   9
      Top             =   4176
      Visible         =   0   'False
      Width           =   972
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   5076
      _ExtentX        =   8943
      _ExtentY        =   688
      BandCount       =   2
      BandBorders     =   0   'False
      _CBWidth        =   5070
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picUpDown"
      MinWidth2       =   600
      MinHeight2      =   315
      Width2          =   945
      NewRow2         =   0   'False
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrApplication 
            Height          =   276
            Left            =   0
            TabIndex        =   17
            Top             =   12
            Width           =   1284
            _ExtentX        =   2275
            _ExtentY        =   476
            ButtonWidth     =   2328
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Application"
                  Object.ToolTipText     =   "Opens the Application"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrOther 
            Height          =   660
            Left            =   1380
            TabIndex        =   18
            Top             =   15
            Width           =   1035
            _ExtentX        =   1826
            _ExtentY        =   1164
            ButtonWidth     =   1508
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Other"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   3675
         ScaleHeight     =   315
         ScaleWidth      =   1305
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   30
         Width           =   1305
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   0
            TabIndex        =   15
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   450
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Application"
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Firm"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator342 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator876 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator986 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
      End
      Begin VB.Menu mnuSeparator3512 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator632 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmQuoteEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mQuoteHandle As String
Private mQuoteBookHandle As String
Private mQuoteBookEventHandle As String


Private mApplicationHandle As String
Private mQuoteCoverageHandle As String
Private mEventHandle As String
Private mPolicyAccountingHandle As String

Private mDataSet As Dataset
Private mListView As ListView
Private mListIndex As Integer
Private mListItemType As String

Private mApplicationLimit As Dataset
Private mRatingLimit As Dataset

Private mQuoteView As Dataset

Private mEventTypes As Collection

Private mParameterNames() As String
Private mParameterValues() As String

Private mDirty As Boolean
Public FormMgr As New FormManager

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, vData, False
  End If
End Property

Public Sub FormChildUnload(ByRef fInterface As FormInterface)
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  ClearDataField txtDataField
    
  Dim Row As Collection
  Set Row = Factory.QuoteService.Find(mQuoteHandle)
  
  If Row Is Nothing Then
    MsgBox "Quote (" & mQuoteHandle & ") no longer exists."
    Exit Sub
  End If

  mApplicationHandle = GetDataFromCollection(Row, DatabaseName.APPLICATION_KEY)
  mQuoteBookHandle = GetDataFromCollection(Row, DatabaseName.MAINQUOTEBOOKID_FIELD)
  
  'get QuoteBook. WILMIC will only have a single QuoteBook per Quote
  Dim quoteBookDataset As Dataset, quoteBookCol As Collection
  Set quoteBookDataset = NewGenericDataset(DatabaseName.QUOTEBOOK_TABLE)
  Set quoteBookCol = quoteBookDataset.DataProvider.Find(DatabaseName.QUOTEBOOK_KEY, mQuoteBookHandle)
    
  mQuoteCoverageHandle = GetDataFromCollection(quoteBookCol, DatabaseName.MAINQUOTECOVERAGEID_FIELD)
  mQuoteBookEventHandle = GetDataFromCollection(quoteBookCol, DatabaseName.EVENT_KEY)
  
  SetFields txtDataField, Row, "Quote"
  SetFields txtDataField, quoteBookCol, "QuoteBook"
  
  mEventHandle = GetDataFromCollection(Row, DatabaseName.EVENT_KEY)
  If (mEventHandle <> vbNullString) Then
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  End If
  
  
  ' Get stored binding attributes.
'  mPolicyAccountingHandle = Factory.EventService.GetAttribute(mEventHandle, DatabaseName.POLICYACCOUNTING_KEY)
  
  Caption = "Edit Quote (" & mQuoteHandle & ")"
  
  Set Row = mQuoteView.DataProvider.Find(DatabaseName.QUOTE_KEY, mQuoteHandle)
 
 ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex

  Dirty = False
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.QUOTE_TABLE
      mQuoteHandle = ListItemText(mListView.SelectedItem, DatabaseName.QUOTE_KEY)
    Case DatabaseName.EVENT_TABLE
      Dim eventId As String
      Dim SourceTypeId As String
      eventId = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
      SourceTypeId = ListItemText(mListView.SelectedItem, DatabaseName.SOURCETYPE_KEY)
      
      If SourceTypeId = vbNullString Then
        MsgBox "Field: sourceTypeId not found. Please verify that the EventType.sourceTypeId is included in the data source being used. A choose columns may need to be done after updating a data source.", vbCritical
      End If
      
      'More likely to be a quote book reference than a quote refernce. Test for quotebook event first.
      'Test for QuoteBook Event
      If SourceTypeId = DatabaseName.QUOTEBOOK_TABLE Then
        mQuoteHandle = DataUtil.FindHandle(DatabaseName.QUOTEBOOK_TABLE, DatabaseName.EVENT_KEY, eventId, DatabaseName.QUOTE_KEY)
      End If
      
      'Test for Quote Event
      If SourceTypeId = DatabaseName.QUOTE_TABLE Then
        mQuoteHandle = DataUtil.FindHandle(DatabaseName.QUOTE_TABLE, DatabaseName.EVENT_KEY, eventId, DatabaseName.QUOTE_KEY)
      End If
      
      'Test for quote book reference
      If mQuoteHandle = vbNullString Then
        Dim quoteBookEventHandle As String
        quoteBookEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(eventId, DatabaseName.QUOTEBOOK_TABLE)
        If quoteBookEventHandle <> vbNullString Then
          mQuoteHandle = DataUtil.FindHandle(DatabaseName.QUOTEBOOK_TABLE, DatabaseName.EVENT_KEY, quoteBookEventHandle, DatabaseName.QUOTE_KEY)
        End If
      End If
      
      'Test for quote reference
      If mQuoteHandle = vbNullString Then
        Dim quoteEventHandle As String
        quoteEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(eventId, DatabaseName.QUOTE_TABLE)
        If quoteEventHandle <> vbNullString Then
          mQuoteHandle = DataUtil.FindHandle(DatabaseName.QUOTE_TABLE, DatabaseName.EVENT_KEY, quoteEventHandle, DatabaseName.QUOTE_KEY)
        End If
      End If
    
  End Select
End Property

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And Verify Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
  
  ' Custom Code
  
  Dim mDataControl As New Collection
  AddTextControl txtDataField, mDataControl
  
  
  save = Factory.QuoteService.WriteQuote( _
                    mQuoteHandle, _
                    mDataControl(DatabaseName.QUOTENAME_FIELD), _
                    txtNote.Text)

  If save Then
    FormRefresh
  Else
    Dirty = True
  End If
End Function
          
Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.QUOTE_TABLE)
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(QuoteHandle As String)
  mQuoteHandle = QuoteHandle
  Show
End Sub


'
'
' End General
'
'
'
'
' Control Methods
'

Private Sub cbrCoolBar_Resize()
  FormResize
End Sub

Private Sub cmdCalculate_Click()
  Browse HttpServer & WebUtil & "rating" & _
      "?key=" & EncodeURL(mQuoteBookHandle)
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSave_Click()
  If save Then Unload Me
End Sub


Private Sub cmdWorksheet_Click()
  Dispatcher.BrowseUnderwritingWorksheet mQuoteHandle
End Sub

Private Sub Form_Initialize()
  Set mDataSet = New GenericDataset
  Set mDataSet.Session = LawSession
  mDataSet.DataProvider.Source = DatabaseName.QUOTE_TABLE
  
  Set mApplicationLimit = New GenericDataset
  Set mApplicationLimit.Session = LawSession
  mApplicationLimit.DataProvider.Source = DatabaseName.APPLICATIONLIMIT_TABLE
  
  Set mRatingLimit = New GenericDataset
  Set mRatingLimit.Session = LawSession
  mRatingLimit.DataProvider.Source = DatabaseName.RatingLIMIT_TABLE
  
  Set mQuoteView = NewGenericDataset(DatabaseName.QUOTE_VIEW)
  'Factory.FolderParameterService.ParameterLoad DatabaseName.POLICY_TABLE, lawActionNew, mParameterNames, mParameterValues
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
 ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrApplication.ButtonHeight + 24
  cbrCoolBar.Bands(2).MinHeight = tbrUpDown.ButtonHeight + 24
  FormUtil.SizeSingleToolbar tbrApplication, 0
  FormUtil.SizeSingleToolbar tbrOther, tbrApplication.Left + tbrApplication.Width
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  

'  '
'  ' Populate EventTypeName Dropdown
'  '
'  Set mEventTypes = Factory.EventTypeService.SourceCollection(DatabaseName.DEDUCTIBLEREIMBURSE_TABLE)
'  Factory.EventTypeService.ComboBoxLoad mEventTypes, cboEventTypeName, "eventName"
  
  FormResize
  FormRefresh
  
  FormUtil.TabControlEnable SSTab
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuEditMove_Click()
  'Move the QuoteBook event instead of the Quote event
  Factory.EventService.ShowMove mQuoteBookEventHandle
End Sub

Private Sub mnuFileOpen_Click()
  FormUtil.ShowByPrimaryKey frmApplicationOpen, mApplicationHandle, FormMgr
End Sub

Private Sub mnuFileOpenOther_Click(Index As Integer)
  Select Case Index
    Case 0 ' Firm
      Dim firmHandle As String
      firmHandle = GetDataFromCollection(Factory.ApplicationService.Find(mApplicationHandle, DatabaseName.FIRM_KEY), DatabaseName.FIRM_KEY)
      FormUtil.ShowByPrimaryKey frmFirmOpen, firmHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.APPLICATIONRATING_REF, DatabaseName.QUOTE_TABLE, mQuoteHandle, txtDataField(0)
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.QUOTE_TABLE, mQuoteHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsNotepad_Click()
  Dim fNotepad As New frmNotepad
'  fNotepad.ShowEvent mEventHandle
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
'  FolderUtil.WorkflowShow DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, lawActionNew, _
'      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub



Private Sub SSTab_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab
End Sub

Private Sub tbrApplication_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileOpen_Click
End Sub

Private Sub tbrOther_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileOpenOther_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
