VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LocationDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.70 (jjm) 2004 09 14
'ver 2.10.0421 (jjm)
'ver 2.00.0114 (jjm)

' LocationDataset

Option Explicit
Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private mEntityLocationDP As DataProvider
Private Words As String
Private ListIndex As Integer
Private mvarPrimaryKey As Collection

Private Sub Class_Initialize()
  ListIndex = UrlName.LOCATIONSEARCH_ENTITYLOCATOR_INDEX
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.LOCATION_TABLE
  
  Set mEntityLocationDP = vData.NewDataProvider
  mEntityLocationDP.Source = DatabaseName.ENTITYLOCATION_VIEW
End Property

Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property

Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property

Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in city field only"
  frm.cboIn.AddItem "in entity name field only"
  frm.cboIn.AddItem "in entity identifier field only"
  frm.cboIn.AddItem "in entity locator field only"
  frm.cboIn.AddItem "in location identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, UrlName.LOCATIONSEARCH_ENTITYLOCATOR_INDEX)
End Sub

Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  Dim isEntityLocationSearch As Boolean          'flag for EntityLocation
  If Not Words = vbNullString Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case UrlName.LOCATIONSEARCH_CITY_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.CITY_FIELD, Words)
      Case UrlName.LOCATIONSEARCH_LOCATIONID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LOCATION_KEY, Words)
      Case UrlName.LOCATIONSEARCH_ENTITYID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ENTITY_KEY, Words)
        isEntityLocationSearch = True
      Case UrlName.LOCATIONSEARCH_COMPANYNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.ENTITYNAME_FIELD, Words)
        isEntityLocationSearch = True
      Case UrlName.LOCATIONSEARCH_ENTITYLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ENTITYLOCATOR_FIELD, Words)
        isEntityLocationSearch = True
      End Select
  End If
'  If frm.FormType <> ftView Then
'    If frm.txtByFirm <> "" Then
'      newFilter.Add """firmId"" = '" + frm.txtByFirm + "'"
'    End If
'  End If


  'Choose appropriate data provider
  Dim chosenDP As DataProvider
  If newFilter.Filter <> vbNullString And isEntityLocationSearch Then
    Set chosenDP = mEntityLocationDP
  Else
    Set chosenDP = mDataProvider
  End If
  frm.sbStatusBar.Panels(2).Text = chosenDP.Source
  
  'MsgBox newFilter
  chosenDP.Filter = newFilter
  chosenDP.OpenRecordset
  chosenDP.SetListView frm.lvView
End Function


'
' Helper function to do a sub query to the Organization Table. This is
' to be used with ID and Locator searches.
'
'  @vFilter - the filter that the results are added to (locationId = 'mainLocationId_result')
'  @subTable - either the Person or Organization table. Any other table with a mainLocationId field would also work.
'  @subColumn - the column that is going to be querried
'  @subValue - the value of that is going to be compared
'  @isContains - sets the sub querry to use a contains() function instead of a starts-with function
'Private Sub AddSubQueryLocations(vFilter As Filter, subTable As String, subColumn As String, subValue As String, Optional isContains As Boolean)
'  Dim subCol As Collection
'  Dim mSubDataset As Dataset
'  Set mSubDataset = NewGenericDataset(subTable)
'  mSubDataset.DataProvider.Filter = _
'        IIf(isContains, _
'            LawDialect.ContainsAndArray(subColumn, subValue), _
'            LawDialect.StartsWithUpperCase(subColumn, subValue))
'
'  mSubDataset.DataProvider.Column = subColumn & "," & DatabaseName.MAINLOCATIONID_FIELD
'  mSubDataset.DataProvider.OpenRecordset
'  Set subCol = mSubDataset.DataProvider.Collection
'
'  If Not subCol Is Nothing Then
'    Dim I As Integer
'    Dim testStr As String
'    For I = 1 To subCol.Count
'      testStr = GetDataFromCollection(subCol(I), DatabaseName.MAINLOCATIONID_FIELD)
'      If testStr <> vbNullString Then
'        vFilter.AddOr LawDialect.StartsWithUpperCase( _
'                              DatabaseName.LOCATION_KEY, _
'                              testStr)
'      End If
'    Next
'  End If
'End Sub

