VERSION 5.00
Begin VB.Form frmMergeCompleted 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Merge Completed"
   ClientHeight    =   1572
   ClientLeft      =   36
   ClientTop       =   324
   ClientWidth     =   5244
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1572
   ScaleWidth      =   5244
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   3936
      TabIndex        =   2
      Top             =   96
      Width           =   1200
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   3936
      TabIndex        =   3
      Top             =   504
      Width           =   1200
   End
   Begin VB.CheckBox chkCompleteFiles 
      Caption         =   "Complete documents"
      Height          =   192
      Left            =   132
      TabIndex        =   1
      Top             =   1140
      Value           =   1  'Checked
      Width           =   3156
   End
   Begin VB.CheckBox chkUpdateFiles 
      Caption         =   "Update modified files"
      Height          =   192
      Left            =   132
      TabIndex        =   0
      Top             =   900
      Value           =   1  'Checked
      Width           =   3144
   End
   Begin VB.Label Label1 
      Caption         =   "The merg has completed. Please select which actions you would lke to take place."
      Height          =   624
      Left            =   120
      TabIndex        =   4
      Top             =   168
      Width           =   3720
   End
End
Attribute VB_Name = "frmMergeCompleted"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private mOk As Boolean

Public Property Get OK() As Boolean
  OK = mOk
End Property

Private Sub cmdCancel_Click()
  Hide
End Sub

Private Sub cmdOk_Click()
  mOk = True
  Hide
End Sub
