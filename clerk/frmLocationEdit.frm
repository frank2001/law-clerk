VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmLocationEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Location"
   ClientHeight    =   5610
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   6135
   Icon            =   "frmLocationEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   6135
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5190
      Left            =   0
      ScaleHeight     =   5190
      ScaleWidth      =   6135
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   384
      Width           =   6132
      Begin VB.CommandButton cmdEntity 
         Caption         =   "Entity"
         Height          =   276
         Left            =   5040
         TabIndex        =   17
         Top             =   144
         Width           =   1000
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   330
         Left            =   3612
         TabIndex        =   14
         Top             =   4770
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   330
         Left            =   2364
         TabIndex        =   13
         Top             =   4770
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   330
         Left            =   4860
         TabIndex        =   16
         Top             =   4770
         Width           =   1200
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   48
         Top             =   4692
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   564
         Top             =   4692
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3750
         Left            =   45
         TabIndex        =   34
         Top             =   930
         Width           =   6030
         _ExtentX        =   10636
         _ExtentY        =   6615
         _Version        =   393216
         Style           =   1
         TabsPerRow      =   4
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmLocationEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "frmLocation"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "&Reference"
         TabPicture(1)   =   "frmLocationEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "cmdRefOpen"
         Tab(1).Control(1)=   "lvReference"
         Tab(1).Control(2)=   "Label4"
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "N&ote"
         TabPicture(2)   =   "frmLocationEdit.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "txtNote"
         Tab(2).Control(1)=   "cmdClearNotepad"
         Tab(2).ControlCount=   2
         Begin VB.TextBox txtNote 
            DataField       =   "eventNote"
            DataMember      =   "Event"
            Height          =   2895
            Left            =   -74904
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   38
            Top             =   372
            Width           =   5784
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74892
            TabIndex        =   37
            Top             =   3390
            Width           =   1028
         End
         Begin VB.CommandButton cmdRefOpen 
            Caption         =   "Open"
            Height          =   288
            Left            =   -70164
            TabIndex        =   35
            Top             =   660
            Width           =   1028
         End
         Begin VB.Frame frmLocation 
            Caption         =   "Location"
            Height          =   3330
            Left            =   72
            TabIndex        =   22
            Top             =   300
            Width           =   5832
            Begin VB.ComboBox cboDatafield 
               DataField       =   "city"
               Height          =   315
               Index           =   1
               Left            =   2124
               TabIndex        =   6
               Text            =   "cboDatafield"
               Top             =   1695
               Width           =   1752
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "state"
               Height          =   315
               Index           =   0
               Left            =   996
               Style           =   2  'Dropdown List
               TabIndex        =   5
               TabStop         =   0   'False
               Top             =   1695
               Width           =   648
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "email"
               Height          =   288
               Index           =   1
               Left            =   996
               TabIndex        =   12
               Top             =   2760
               Width           =   3192
            End
            Begin VB.ComboBox cboLocationTypeName 
               Height          =   315
               ItemData        =   "frmLocationEdit.frx":0060
               Left            =   996
               List            =   "frmLocationEdit.frx":0062
               Style           =   2  'Dropdown List
               TabIndex        =   1
               Tag             =   "LocationEditLocationType"
               Top             =   315
               Width           =   3204
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "country"
               Height          =   288
               Index           =   12
               Left            =   3372
               TabIndex        =   9
               Text            =   " "
               Top             =   2055
               Visible         =   0   'False
               Width           =   1554
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "county"
               Height          =   288
               Index           =   13
               Left            =   996
               TabIndex        =   8
               Text            =   " "
               Top             =   2040
               Width           =   1554
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine1"
               Height          =   288
               Index           =   17
               Left            =   996
               TabIndex        =   2
               Top             =   660
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "zip"
               Height          =   288
               Index           =   19
               Left            =   4272
               TabIndex        =   7
               Top             =   1695
               Width           =   1056
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine3"
               Height          =   288
               Index           =   22
               Left            =   996
               TabIndex        =   4
               Top             =   1350
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine2"
               Height          =   288
               Index           =   23
               Left            =   996
               TabIndex        =   3
               Top             =   1005
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "locationTypeId"
               Height          =   252
               Index           =   0
               Left            =   5112
               TabIndex        =   0
               Top             =   252
               Visible         =   0   'False
               Width           =   492
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmLocationEdit.frx":0064
               DataField       =   "fax"
               DataMember      =   "Location"
               Height          =   285
               Index           =   2
               Left            =   3900
               TabIndex        =   11
               Top             =   2415
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmLocationEdit.frx":0077
               DataField       =   "telephone"
               DataMember      =   "Location"
               Height          =   285
               Index           =   1
               Left            =   1425
               TabIndex        =   10
               Top             =   2415
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "(street address)"
               Height          =   195
               Left            =   4320
               TabIndex        =   43
               Top             =   720
               Width           =   1080
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "(Attn, C/O, Suite #)"
               Height          =   195
               Left            =   4320
               TabIndex        =   42
               Top             =   1065
               Width           =   1365
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               Caption         =   "(P.O. Box)"
               Height          =   195
               Left            =   4320
               TabIndex        =   41
               Top             =   1425
               Width           =   720
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Email:"
               Height          =   195
               Left            =   90
               TabIndex        =   33
               Top             =   2805
               Width           =   450
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "Main Fax:"
               Height          =   195
               Left            =   3090
               TabIndex        =   31
               Top             =   2460
               Width           =   690
            End
            Begin VB.Label Label10 
               AutoSize        =   -1  'True
               Caption         =   "Main Telephone:"
               Height          =   195
               Left            =   90
               TabIndex        =   30
               Top             =   2430
               Width           =   1215
            End
            Begin VB.Label Label14 
               AutoSize        =   -1  'True
               Caption         =   "Zip:"
               Height          =   195
               Left            =   3990
               TabIndex        =   29
               Top             =   1725
               Width           =   270
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "County:"
               DataField       =   "County"
               Height          =   195
               Left            =   90
               TabIndex        =   28
               Top             =   2100
               Width           =   525
            End
            Begin VB.Label Label18 
               AutoSize        =   -1  'True
               Caption         =   "State:"
               Height          =   195
               Left            =   90
               TabIndex        =   27
               Top             =   1740
               Width           =   405
            End
            Begin VB.Label Label19 
               AutoSize        =   -1  'True
               Caption         =   "Address:"
               Height          =   195
               Left            =   90
               TabIndex        =   26
               Top             =   705
               Width           =   645
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Type:"
               Height          =   195
               Left            =   90
               TabIndex        =   25
               Top             =   345
               Width           =   420
            End
            Begin VB.Label Label13 
               AutoSize        =   -1  'True
               Caption         =   "City:"
               Height          =   195
               Left            =   1785
               TabIndex        =   24
               Top             =   1740
               Width           =   300
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Country:"
               DataField       =   "County"
               Height          =   195
               Left            =   2745
               TabIndex        =   23
               Top             =   2100
               Visible         =   0   'False
               Width           =   570
            End
         End
         Begin MSComctlLib.ListView lvReference 
            Height          =   3000
            Left            =   -74910
            TabIndex        =   36
            Top             =   645
            Width           =   4695
            _ExtentX        =   8281
            _ExtentY        =   5292
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "This ia a list of items that use this location."
            Height          =   192
            Left            =   -74916
            TabIndex        =   40
            Top             =   384
            Width           =   2928
         End
      End
      Begin VB.Label Label3 
         Caption         =   "Select the Reference tab to display persons, contacts and firms that use this location."
         Height          =   384
         Left            =   144
         TabIndex        =   39
         Top             =   492
         Width           =   5820
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "entityName"
         DataField       =   "entityName"
         DataMember      =   "Entity"
         Height          =   192
         Index           =   0
         Left            =   144
         TabIndex        =   32
         Top             =   180
         UseMnemonic     =   0   'False
         Width           =   4752
         WordWrap        =   -1  'True
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   6132
      _ExtentX        =   10821
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   6135
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   6015
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   30
         Width           =   6015
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   72
            TabIndex        =   20
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   288
      Left            =   4656
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   3240
      Visible         =   0   'False
      Width           =   708
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Location"
            Index           =   0
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Firm"
            Index           =   2
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Location"
            Index           =   0
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   2
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   0
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator503 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmLocationEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.71.00 (jjm) 2005 01 31
'ver 2.70.00 (jjm) 2004 11 11
'ver 2.70.00 (jjm) 2004 09 23
'ver 2.40.1208 (jjm)
Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mLocationHandle As String
Private mEntityHandle As String
Private mEventHandle As String

Private mNew As Boolean
Private mLocationType As Collection

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private isEntityOrganization As Boolean

Private Const CITY_DATAFIELD_INDEX = 1
Private Const COMPANYNAME_DATAFIELD_INDEX = 16
Private Const COUNTY_DATAFIELD_INDEX = 13
Private Const STATE_DATAFIELD_INDEX = 0
Private Const ZIP_DATAFIELD_INDEX = 19

Private Const TELEPHONE_MASK = "___.___.____"

Private FormMgr As New FormManager

'ver 2.71.00 (jjm) 2005 01 31 - for city dropdown
'  http://www.freevbcode.com/ShowCode.Asp?ID=2266
Private miSelStart As Integer


'
' FormInterface Interface
'

Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Private Sub EntityRefresh()
  Dim EntCol As Collection
  Set EntCol = Factory.EntityService.Find(mEntityHandle)
  SetFields lblDataField, EntCol, DatabaseName.ENTITY_TABLE
  'OLD COMPANY NAME FUNCTIONLAITY
  'If Not Org Is Nothing And mNew Then
  '  If txtDataField(16).Text = vbNullString Then
  '    txtDataField(16).Text = GetDataFromCollection(Org, DatabaseName.ENTITYNAME_FIELD)
  '  End If
  'End If
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
    Case fimNew
      Select Case fInterface.Key
        Case DatabaseName.ORGANIZATION_TABLE
          mnuFileNew_Click 2
        Case Else
          MsgBox "Insert not supported for '" & fInterface.Key & "' records."
      End Select
    Case fimSelect
      Dirty = True
      Select Case fInterface.Key
        Case DatabaseName.ORGANIZATION_TABLE
          mEntityHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.ORGANIZATION_KEY)
          EntityRefresh
          cboLocationTypeName.SetFocus
      End Select
    Case fimSave
      Dirty = True
      Select Case fInterface.Key
        Case DatabaseName.FIRM_TABLE & "Edit"
          mEntityHandle = fInterface.Child.organizationHandle
          EntityRefresh
      End Select
      
  End Select
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

' End FormInterface


Public Sub FormRefresh()
  ClearDataField txtDatafield
  ClearDataField mskDataField
  ClearDataField lblDataField
  
  txtNote.Text = vbNullString
  
  If (mNew) Then
    mLocationHandle = vbNullString
    mEventHandle = vbNullString
    
    Me.Caption = "New Location"
    
    'txtDatafield(18).Text = IniMain.ReadFile(UrlName.MISC_INI, "DefaultState")
    'txtDataField(18).Text = "WI"
    
    txtDatafield(0).Text = "Firm" 'default location type
    
    'Do not clear the entityHandle
    'EntityRefresh
  Else
    Dim Datafields As Collection
    Set Datafields = Factory.LocationService.Find(mLocationHandle)
    If Datafields Is Nothing Then
      MsgBox "Location (" & mLocationHandle & ") no longer exists!"
      mNew = True
      FormRefresh
      Exit Sub
    End If

    ' setup form
    Caption = "Edit Location (" & mLocationHandle & ")"
    SetFields txtDatafield, Datafields
    SetFields mskDataField, Datafields
    SetFields lblDataField, Datafields
    'For this to work ensure the state index is lower than the city index
    SetFields cboDatafield, Datafields
    
    ' setup handles
    mEntityHandle = GetDataFromCollection(Datafields, DatabaseName.MAINENTITYID_FIELD, True)
      
    mEventHandle = GetDataFromCollection(Datafields, DatabaseName.EVENT_KEY, True)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
    
    ReferenceRefresh
  
  End If
    
  'Test for organization
  isEntityOrganization = (Factory.OrganizationService.Find(mEntityHandle) Is Nothing)

  mnuFileProperties.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  
  ' Refresh other
  EntityRefresh
  
  ' Standard
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  Dirty = False
End Sub
 

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mLocationHandle = ListItemText(mListView.SelectedItem, DatabaseName.LOCATION_KEY)
End Property


Public Property Get LocationHandle()
  LocationHandle = mLocationHandle
End Property

Private Sub ReferenceAdd(Source As String, TableId As String, NameField As String, ForeignLocationKey)
  Dim RefDataset As Dataset
  Dim Refs As New Collection
  Dim I As Integer
  Dim j As Integer
    
  Set RefDataset = NewGenericDataset(Source)
  RefDataset.DataProvider.filter = ForeignLocationKey & " = '" & mLocationHandle & "'"
  RefDataset.DataProvider.OpenRecordset
  If Not RefDataset.DataProvider.Collection Is Nothing Then
    Set Refs = RefDataset.DataProvider.Collection
    For I = 1 To Refs.Count
      lvReference.ListItems.Add , , Source
      j = lvReference.ListItems.Count
      lvReference.ListItems(j).ListSubItems.Add , "Name", Refs(I)(NameField)
      lvReference.ListItems(j).ListSubItems.Add , "Id", Refs(I)(TableId)
    Next I
  End If

End Sub

' Sub to load the refrence tab. The reference tab displays all
' items that share the same locationId
' These items can be either a Firm, Person, or a Contact
Private Sub ReferenceRefresh()
  lvReference.ListItems.Clear
  ReferenceAdd DatabaseName.ENTITY_TABLE, DatabaseName.ENTITY_KEY, DatabaseName.ENTITYNAME_FIELD, DatabaseName.MAINLOCATIONID_FIELD
  ReferenceAdd DatabaseName.CONTACT_TABLE, DatabaseName.CONTACT_KEY, DatabaseName.CONTACTSALUTATION_FIELD, DatabaseName.LOCATION_KEY
  cmdRefOpen.Enabled = (lvReference.ListItems.Count > 0)
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
    
  If txtDatafield(COUNTY_DATAFIELD_INDEX).Text = vbNullString Or _
     cboDatafield(STATE_DATAFIELD_INDEX).Text = vbNullString Or _
     txtDatafield(ZIP_DATAFIELD_INDEX).Text = vbNullString Or _
     cboDatafield(CITY_DATAFIELD_INDEX).Text = vbNullString Then
    MsgBox "State, County, City, State and Zip fields are required location data"
    SSTab1.Tab = 0
    txtDatafield(ZIP_DATAFIELD_INDEX).SetFocus
    Exit Function
  End If
  
  
  ' Start custom Code
  Dim mDataControls As New Collection
  AddTextControl txtDatafield, mDataControls
  AddMaskControl mskDataField, mDataControls
  AddTextControl cboDatafield, mDataControls

  If mLocationHandle = vbNullString Then
    mLocationHandle = Factory.LocationService.CreateLocation( _
              GetDataFromCollection(mDataControls, DatabaseName.LOCATIONTYPE_KEY), _
              mEntityHandle, _
              GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE1_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE2_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE3_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.CITY_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.COUNTY_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.STATE_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.ZIP_FIELD), _
              vbNullString, _
              GetMask(GetDataFromCollection(mDataControls, DatabaseName.TELEPHONE_FIELD), TELEPHONE_MASK), _
              GetMask(GetDataFromCollection(mDataControls, DatabaseName.FAX_FIELD), TELEPHONE_MASK), _
              GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
              txtNote.Text)
    Save = mLocationHandle <> vbNullString
  Else
    Save = Factory.LocationService.WriteLocation(mLocationHandle, _
              GetDataFromCollection(mDataControls, DatabaseName.LOCATIONTYPE_KEY), _
              mEntityHandle, _
              GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE1_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE2_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE3_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.CITY_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.COUNTY_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.STATE_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.ZIP_FIELD), _
              vbNullString, _
              GetMask(GetDataFromCollection(mDataControls, DatabaseName.TELEPHONE_FIELD), TELEPHONE_MASK), _
              GetMask(GetDataFromCollection(mDataControls, DatabaseName.FAX_FIELD), TELEPHONE_MASK), _
              GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
              txtNote.Text)
  End If
  
  If Save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  
  IniMain.WriteFile UrlName.MISC_INI, "lastLocationType", txtDatafield(0)
    
  Dirty = Not Save
End Function


Public Sub ShowByPrimaryKey(LocationId As String)
  mLocationHandle = LocationId
  Show
End Sub


Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no locations to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub


Public Sub ShowNew(Optional entityhandle As String)
  mEntityHandle = entityhandle
  mNew = True
  
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Select Case Index
    Case STATE_DATAFIELD_INDEX
      'Update City Selection
      Factory.LocationService.LoadCitiesByState cboDatafield(CITY_DATAFIELD_INDEX), cboDatafield(STATE_DATAFIELD_INDEX).Text
    Case CITY_DATAFIELD_INDEX
      'Update county
      txtDatafield(COUNTY_DATAFIELD_INDEX).Text = Factory.LocationService.GetCountyByStateCity(cboDatafield(STATE_DATAFIELD_INDEX).Text, cboDatafield(CITY_DATAFIELD_INDEX).Text)
  End Select
  
  Dirty = True
End Sub

Private Sub cboDatafield_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case Index
    'ver 2.70.00 (jjm) 2005 01 31
    Case CITY_DATAFIELD_INDEX
      '<Delete>
      If KeyCode = 46 Then KeyCode = 0 'Disable the delete key
      
      'Change backspace to left arrow
      If KeyCode = vbKeyBack Then KeyCode = vbKeyLeft
      
      'ver 2.70.00 (jjm) 2005 02 09 - tweak
      'When the left key is pressed have the curser move left one
      'position from the selection start instead of the end of
      'the selection. This section will blank the keycode and
      'manually set the cursor position.
      If KeyCode = vbKeyLeft Then
        With cboDatafield(Index)
          miSelStart = IIf(.SelStart > 0, .SelStart - 1, 0)
          cboDatafield(Index).SelStart = miSelStart
          .SelLength = Len(.Text) - .SelStart
        End With
        
        KeyCode = 0
        Exit Sub
      End If
      
      miSelStart = cboDatafield(Index).SelStart
  End Select
End Sub

Private Sub cboDataField_KeyPress(Index As Integer, KeyAscii As Integer)
  Select Case Index
    'ver 2.70.00 (jjm) 2005 01 31
    Case CITY_DATAFIELD_INDEX
      Dim lCnt       As Long 'Generic long counter
      Dim lMax       As Long
      Dim sComboItem As String
      Dim sComboText As String 'Text currently in combobox
      Dim sText      As String 'Text after keypressed
  
      
      With cboDatafield(Index)
        lMax = .ListCount - 1
        sComboText = .Text
        sText = Left(sComboText, miSelStart) & Chr(KeyAscii)
        
        KeyAscii = 0 'Reset key pressed
        
        For lCnt = 0 To lMax
          sComboItem = .List(lCnt)
          
          If UCase(sText) = UCase(Left(sComboItem, _
                                     Len(sText))) Then
            .ListIndex = lCnt
            .Text = sComboItem
            .SelStart = Len(sText)
            .SelLength = Len(sComboItem) - (Len(sText))
            
            Exit For
          End If
        Next 'lCnt
      End With
  
  End Select
End Sub

Private Sub cboLocationTypeName_Click()
  If cboLocationTypeName.ListIndex < 0 Then
    txtDatafield(0).Text = ""
  Else
    txtDatafield(0).Text = mLocationType(cboLocationTypeName.ListIndex + 1)(DatabaseName.LOCATIONTYPE_KEY)
  End If
  Dirty = True
End Sub

'
' Controls
'
Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdEntity_Click()
  Dispatcher.ShowSearch DatabaseName.ORGANIZATION_TABLE, FormMgr, True
End Sub

Private Sub cmdRefOpen_Click()
  Select Case lvReference.SelectedItem.Text
    Case "Organization"
      FormUtil.ShowByPrimaryKey New frmOrganizationOpen, ListItemText(lvReference.SelectedItem, "Id"), FormMgr
    Case "Person"
      FormUtil.ShowByPrimaryKey New frmPersonOpen, ListItemText(lvReference.SelectedItem, "Id"), FormMgr
    Case "Contact"
      FormUtil.ShowByPrimaryKey New frmContactEdit, ListItemText(lvReference.SelectedItem, "Id"), FormMgr
  End Select
  
End Sub

Private Sub cmdSave_Click()
  If Save Then
    FormUtil.SetComboDefault cboLocationTypeName
    Unload Me
  End If
End Sub

Private Sub cmdSaveNew_Click()
  If Save Then mnuFileNew_Click 0
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  ' Custom Menu Shortcut Adjustments
  Me.KeyPreview = True
  
'  mnuFileClose.Caption = mnuFileClose.Caption + Chr(9) + "Alt+F4"
'  mnuViewPrevious.Caption = mnuViewPrevious.Caption + Chr(9) + "Ctrl+<"
'  mnuViewNext.Caption = mnuViewNext.Caption + Chr(9) + "Ctrl+>"

  FormMgr.Load Me
  
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  
  Set mLocationType = Factory.LocationTypeService.NameCollection()
  FormUtil.ComboBoxLoad mLocationType, cboLocationTypeName, "locationTypeName"
   
  'ver 2.70.00 (jjm) 2004 09 23
  Factory.LocationService.LoadStates cboDatafield(STATE_DATAFIELD_INDEX), "WI"
    
  'Setup the listview. The column headers will be: Type | Name | Id
  lvReference.ColumnHeaders.Add , "Type", "Type"
  lvReference.ColumnHeaders.Add , "Name", "Name"
  lvReference.ColumnHeaders.Add , "Id", "Id"
  lvReference.ColumnHeaders("Type").Width = 860
  lvReference.ColumnHeaders("Name").Width = lvReference.Width - (1400 + 860)
   
  
  FormRefresh
  TabControlEnable SSTab1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.DeserializePosition Me
  FormMgr.Unload
End Sub
  
Private Sub lvReference_DblClick()
  cmdRefOpen_Click
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0
      'Test for organization
      If Not isEntityOrganization Then
        MsgBox "No firm is associated with this location"
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mEntityHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmLocationOpen, mLocationHandle, FormMgr
    Case 2
      If Not isEntityOrganization Then
        MsgBox "No firm is associated with this location"
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mEntityHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.LOCATION_TABLE, mLocationHandle
  End If
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click(Index As Integer)
  Select Case Index
    Case 0
      If Not Save(True) Then Exit Sub
      mNew = True
      mLocationHandle = vbNullString
      FormRefresh
    Case 2
      FormUtil.ShowNew New frmOrganizationEdit, FormMgr, , DatabaseName.FIRM_TABLE + "Edit"
  End Select
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.LOCATION_REF, DatabaseName.LOCATION_TABLE, mLocationHandle, lblDataField(0)
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub


Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr
End Sub



Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub


Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub


Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub


Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub

Private Sub mskDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub mskDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect mskDataField(Index)
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  TabControlEnable SSTab1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Select Case Index
    Case 0
      cboLocationTypeName.ListIndex = CollectionFind(mLocationType, DatabaseName.LOCATIONTYPE_KEY, txtDatafield(Index).Text) - 1
  End Select
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDatafield(Index)
  Select Case Index
    Case COMPANYNAME_DATAFIELD_INDEX
      If mNew And txtDatafield(Index) = vbNullString Then
        txtDatafield(Index) = lblDataField(0)
      End If
  End Select
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

