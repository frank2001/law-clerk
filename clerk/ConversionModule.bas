Attribute VB_Name = "DataUtil"
'ver 2.60.00 (jjm) 2004 03 15

Option Explicit

Public Function NewGenericDataset(Source As String) As Dataset
  Dim mDataSet As Dataset
  Set mDataSet = New GenericDataset
  Set mDataSet.Session = LawSession
  mDataSet.DataProvider.Source = Source
  Set NewGenericDataset = mDataSet
End Function

'
' Sets the dataset depending on the datatype
'

'
' ver 2.60.00 (jjm) 2004 03 15 - fix to make this system more flexable.
' Not all datasets used by this funtion are folder based. This causes problems
' when searches are launched using the table as a key. Making the source parameter optional
' allows the source value to be assigned using the default for that specific dataset.
'
Public Function NewDataset(DataType As String, Optional Source As String) As Dataset
  
  Select Case DataType
    Case "(Generic)"
      Set NewDataset = New GenericDataset
    Case DatabaseName.APPLICATION_TABLE
      Set NewDataset = New ApplicationDataset
    Case DatabaseName.APPLICATIONLAWAREA_TABLE
      Set NewDataset = New ApplicationLawAreaViewDataset
    Case DatabaseName.APPLICATIONLAWYER_TABLE
      Set NewDataset = New ApplicationLawyerDataset
    Case DatabaseName.APPLICATIONLIMIT_TABLE
      Set NewDataset = New ApplicationLimitDataset
    Case DatabaseName.APPLICATIONQUESTION_TABLE
      Set NewDataset = New ApplicationQuestionDataset
      
    Case DatabaseName.CLAIM_TABLE
      Set NewDataset = New ClaimDataset
    'Case DatabaseName.CLAIMENDORSEMENT_TABLE
    '  Set NewDataset = New ClaimEndorsementDataset
    Case DatabaseName.INSTANCELAWAREA_TABLE
      Set NewDataset = New InstanceLawAreaDataset
    Case DatabaseName.INSTANCELAWERROR_TABLE
      Set NewDataset = New InstanceLawErrorDataset
    Case DatabaseName.INSTANCELAWYER_TABLE
      Set NewDataset = New InstanceLawyerDataset
    Case DatabaseName.CLAIMPAYMENT_TABLE
      Set NewDataset = New ClaimPaymentDataset
    Case DatabaseName.CLAIMRESERVE_TABLE
      Set NewDataset = New ClaimReserveDataset
    Case DatabaseName.CLEEVENT_TABLE
      Set NewDataset = New CLEEventDataset
    Case DatabaseName.CLEEVENTLAWYER_TABLE
      Set NewDataset = New CLEEventLawyerDataset
    Case DatabaseName.CONTACT_TABLE
      Set NewDataset = New ContactView
    Case DatabaseName.COVERAGE_TABLE
      Set NewDataset = New CoverageView
    Case DatabaseName.COVERAGELAWYER_TABLE
      Set NewDataset = New CoverageLawyerView
      
    Case DatabaseName.DEDUCTIBLEREIMBURSE_TABLE
      Set NewDataset = New DeductibleReimburseDataset
      
    Case DatabaseName.EFTINDIVIDUAL_TABLE
      Set NewDataset = New EFTIndividualDataset
    Case DatabaseName.EFTDEPOSITFINANCIALINST_TABLE
      Set NewDataset = New EFTDepositFinancialInstDataset
    Case DatabaseName.ENDORSEMENT_TABLE
      Set NewDataset = New EndorsementDataset
    Case DatabaseName.EVENT_TABLE
      Set NewDataset = New EventViewDataset
    Case DatabaseName.EVENTGROUP_TABLE
      Set NewDataset = New EventGroupDataset
    Case DatabaseName.EVENTTYPE_TABLE
      Set NewDataset = New EventTypeView
    Case DatabaseName.EVENTTYPERULE_TABLE
      Set NewDataset = New EventTypeGroupView
      
    Case DatabaseName.FIRM_TABLE
      Set NewDataset = New FirmViewDataset
    Case DatabaseName.FIRMINSURER_TABLE
      Set NewDataset = New FirmInsurerDataset
    Case DatabaseName.FIRMLAWYER_TABLE
      Set NewDataset = New FirmLawyerDataset
         
    Case DatabaseName.GLBATCH_TABLE
      Set NewDataset = New GLBatchDataset
    Case DatabaseName.GLENTRY_TABLE
      Set NewDataset = New GLEntryDataset
    Case DatabaseName.GLTRANSACTION_TABLE
      Set NewDataset = New GLTransactionDataset
      
    Case DatabaseName.INCIDENT_TABLE
      Set NewDataset = New IncidentDataset
    Case DatabaseName.INSTALLMENT_TABLE
      Set NewDataset = New InvoiceDataset
    Case DatabaseName.INSTALLMENTPOLICYPAYMENT_TABLE
      Set NewDataset = New InstallmentPolicyPaymentDataset
    Case DatabaseName.INSTANCE_TABLE
      Set NewDataset = New InstanceDataset
    
    Case DatabaseName.INSTANCELAWYER_TABLE
      Set NewDataset = New InstanceLawyerDataset
      
    Case DatabaseName.INSURER_TABLE
      Set NewDataset = New InsurerDataset
    Case DatabaseName.INSURER_VIEW
      Set NewDataset = New InsurerDataset
      
    Case DatabaseName.INVOICE_TABLE
      Set NewDataset = New InvoiceDataset
      
    Case DatabaseName.LAWAREA_TABLE
      Set NewDataset = New LawAreaDataset
    Case DatabaseName.LAWDISPOSITION_TABLE
      Set NewDataset = New LawDispositionData
    Case DatabaseName.LAWERROR_TABLE
      Set NewDataset = New LawErrorDataset
      
    Case DatabaseName.LAWYER_TABLE
      Set NewDataset = New LawyerDataset
    Case DatabaseName.LOCATION_TABLE
      Set NewDataset = New LocationDataset
    
    Case DatabaseName.ORGANIZATION_TABLE
      Set NewDataset = New OrganizationDataset
    
    Case DatabaseName.PERSON_TABLE
      Set NewDataset = New PersonDataset
    Case DatabaseName.POLICY_TABLE
      Set NewDataset = New PolicyDataset
      
    Case DatabaseName.POLICYLAWYERCOVERAGEVIEW_TABLE
      Set NewDataset = New PolicyLawyerCoverageDataset
    Case "PolicyLawyerCoverage" 'HACK
      Set NewDataset = New PolicyLawyerCoverageDataset
    
    Case DatabaseName.POLICYACCOUNTING_TABLE
      Set NewDataset = New PolicyAccountingDataset
      
    Case DatabaseName.QUOTE_TABLE
      Set NewDataset = New QuoteDataset
    Case "ApplicationRating" 'backwards compatability hack
      Set NewDataset = New QuoteDataset
    
    Case DatabaseName.QUOTECOVERAGE_TABLE
      Set NewDataset = New QuoteCoverageDataset
    
    Case DatabaseName.RATING_TABLE
      Set NewDataset = New RatingDataset
    Case DatabaseName.RATINGBOOKMETRICCALC_TABLE
      Set NewDataset = New RatingBookMetricCalcDataset
    Case DatabaseName.RATINGBOOKMETRIC_TABLE
      Set NewDataset = New RatingBookMetricDataset
    Case DatabaseName.RATINGBOOKRATE_TABLE
      Set NewDataset = New RatingBookRateDataset
    Case DatabaseName.RATINGLAWYER_TABLE
      Set NewDataset = New RatingLawyerDataset
    Case DatabaseName.RatingLIMIT_TABLE
      Set NewDataset = New RatingLimitDataset
    Case "ReferenceDatset": 'HACK: Make a const?
      Set NewDataset = New ReferenceViewDataset
      
    Case DatabaseName.SYSLISTENERPARAMETER_TABLE
      Set NewDataset = New SysListenerParameterDataset
      
    Case Else
      Set NewDataset = New GenericDataset
  End Select
  
  Set NewDataset.Session = LawSession
  
  'ver 2.60.00 (jjm) 2004 03 15 - Made the source parameter optional
  If Source <> vbNullString Then _
      NewDataset.DataProvider.Source = Source
  
End Function

'Quick and dirty way to get information on the fly in collection format
Public Function GenericFind(Source As String, HandleName As String, HandleValue As String) As Collection
  Dim GenericDataset As Dataset
  Set GenericDataset = NewGenericDataset(Source)
  GenericDataset.DataProvider.Filter = LawDialect.EqualString(HandleName, HandleValue)
  GenericDataset.DataProvider.OpenRecordset
  'exit if no data exits
  If GenericDataset.DataProvider.IsEmpty Then
    Set GenericFind = Nothing
    Exit Function
  End If
  
  Set GenericFind = GenericDataset.DataProvider.Collection(1)
End Function


'Quick function to get a single string field from a datasource by handle. Will return the HandleField
' @param source = the table / view being queried
' @param QueryFieldName = field name used in where clause. Used to help find the targetField
' @param QueryFieldValue = compared to the QueryFieldName
' @param TargetFieldName = the field that is returned
'
'ex; .list?qFor=<Source>[<QueryFieldName> = <QueryFieldValue>] which will return the TargetField
Public Function FindHandle(Source As String, QueryFieldName As String, QueryFieldValue As String, TargetField As String) As String
  Dim GenericDataset As Dataset
  Set GenericDataset = NewGenericDataset(Source)
  GenericDataset.DataProvider.Filter = LawDialect.EqualString(QueryFieldName, QueryFieldValue)
  GenericDataset.DataProvider.Column = TargetField
  GenericDataset.DataProvider.OpenRecordset
  
'exit if no data exits
  If GenericDataset.DataProvider.IsEmpty Then
    FindHandle = vbNullString
    Exit Function
  End If
  
  Dim Result As Collection
  Set Result = GenericDataset.DataProvider.Collection
  'return the first value
  FindHandle = GetDataFromCollection(Result(1), TargetField)
End Function


'Returns a collection containing TableColumnType data
' @param table = TableColumnType.table field. Must be an exact match.
' @param column = TableColumnType.column field. Must be an exact match.
' @param returnColumns = a field list of columns to be returned. Comma seperated.
Public Function GetTableColumnTypeCollection(table As String, Column As String, Optional returnColumns As String) As Collection
  Dim TCTDataset As Dataset
  Set TCTDataset = NewGenericDataset(DatabaseName.TABLECOLUMNTYPE_TABLE)
  TCTDataset.DataProvider.Filter = LawDialect.EqualString(DatabaseName.TABLE_FIELD, table) & " and " & _
                                   LawDialect.EqualString(DatabaseName.COLUMN_FIELD, Column)
  TCTDataset.DataProvider.OpenRecordset
  'exit if no data exits
  If TCTDataset.DataProvider.IsEmpty Then
    Set GetTableColumnTypeCollection = Nothing
    Exit Function
  End If
  
  Set GetTableColumnTypeCollection = TCTDataset.DataProvider.Collection
End Function
