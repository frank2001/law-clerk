VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmEndorsementOpen 
   Caption         =   "Endorsement"
   ClientHeight    =   5625
   ClientLeft      =   225
   ClientTop       =   855
   ClientWidth     =   8415
   Icon            =   "frmEndorsementOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5625
   ScaleWidth      =   8415
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4920
      Left            =   0
      ScaleHeight     =   4920
      ScaleWidth      =   8415
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   384
      Width           =   8412
      Begin VB.Frame fraPolicy 
         Caption         =   "Policy"
         Height          =   1500
         Left            =   5628
         TabIndex        =   6
         Top             =   72
         Width           =   1932
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmName"
            DataField       =   "firmName"
            DataMember      =   "Policy"
            Height          =   192
            Index           =   2
            Left            =   504
            TabIndex        =   28
            Top             =   492
            UseMnemonic     =   0   'False
            Width           =   696
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Firm:"
            Height          =   192
            Left            =   132
            TabIndex        =   27
            Top             =   492
            Width           =   348
         End
         Begin VB.Label lblPolicyID 
            AutoSize        =   -1  'True
            Caption         =   "NO:"
            Height          =   195
            Left            =   135
            TabIndex        =   26
            Top             =   270
            Width           =   285
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "policyNumber"
            DataField       =   "policyNumber"
            DataMember      =   "Policy"
            Height          =   195
            Index           =   0
            Left            =   510
            TabIndex        =   25
            Top             =   270
            UseMnemonic     =   0   'False
            Width           =   960
         End
      End
      Begin VB.Frame fraClaim 
         Caption         =   "Endorsement"
         Height          =   1524
         Left            =   84
         TabIndex        =   1
         Top             =   48
         Width           =   5496
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "coverageTypeId"
            DataField       =   "coverageTypeId"
            DataMember      =   "Coverage"
            Height          =   195
            Index           =   11
            Left            =   1110
            TabIndex        =   42
            Top             =   465
            UseMnemonic     =   0   'False
            Width           =   1170
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "issueDate"
            DataField       =   "issueDate"
            DataMember      =   "Coverage"
            Height          =   192
            Index           =   7
            Left            =   4332
            TabIndex        =   41
            Top             =   228
            UseMnemonic     =   0   'False
            Width           =   732
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "expirationDate"
            DataField       =   "expirationDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Coverage"
            Height          =   192
            Index           =   6
            Left            =   4332
            TabIndex        =   40
            Top             =   708
            UseMnemonic     =   0   'False
            Width           =   1044
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            DataMember      =   "Coverage"
            Height          =   192
            Index           =   5
            Left            =   4332
            TabIndex        =   39
            Top             =   468
            UseMnemonic     =   0   'False
            Width           =   948
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "premium"
            DataField       =   "premium"
            DataMember      =   "Coverage"
            Height          =   192
            Index           =   4
            Left            =   4332
            TabIndex        =   38
            Top             =   948
            UseMnemonic     =   0   'False
            Width           =   624
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Event Status:"
            Height          =   192
            Left            =   120
            TabIndex        =   37
            Top             =   948
            Width           =   924
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            Caption         =   "Description:"
            Height          =   192
            Left            =   120
            TabIndex        =   36
            Top             =   1188
            Width           =   852
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Left            =   3144
            TabIndex        =   35
            Top             =   468
            Width           =   1032
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Premium:"
            Height          =   192
            Left            =   3144
            TabIndex        =   34
            Top             =   948
            Width           =   672
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Issue Date:"
            Height          =   192
            Left            =   3144
            TabIndex        =   33
            Top             =   228
            Width           =   804
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Expiration Date:"
            Height          =   192
            Left            =   3144
            TabIndex        =   32
            Top             =   708
            Width           =   1128
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Number:"
            Height          =   192
            Left            =   120
            TabIndex        =   31
            Top             =   708
            Width           =   612
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Coverage Id:"
            Height          =   192
            Left            =   120
            TabIndex        =   30
            Top             =   228
            Width           =   924
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Type Id:"
            Height          =   192
            Left            =   120
            TabIndex        =   29
            Top             =   468
            Width           =   588
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "endorsementDesc"
            DataField       =   "endorsementDesc"
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            DataMember      =   "Endorsement"
            Height          =   192
            Index           =   17
            Left            =   1104
            TabIndex        =   5
            Top             =   1188
            UseMnemonic     =   0   'False
            Width           =   1332
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventStatus"
            DataField       =   "eventStatus"
            DataMember      =   "Event"
            Height          =   192
            Index           =   20
            Left            =   1104
            TabIndex        =   4
            Top             =   948
            UseMnemonic     =   0   'False
            Width           =   840
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "endorsementNumber"
            DataField       =   "endorsementNumber"
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            DataMember      =   "Endorsement"
            Height          =   195
            Index           =   9
            Left            =   1110
            TabIndex        =   3
            Top             =   705
            UseMnemonic     =   0   'False
            Width           =   1470
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "coverageId"
            DataField       =   "coverageId"
            DataMember      =   "Coverage"
            Height          =   192
            Index           =   21
            Left            =   1104
            TabIndex        =   2
            Top             =   228
            UseMnemonic     =   0   'False
            Width           =   828
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   3120
         Top             =   4752
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   3684
         Top             =   4752
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3132
         Left            =   84
         TabIndex        =   18
         Top             =   1740
         Width           =   7212
         _ExtentX        =   12726
         _ExtentY        =   5530
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   9
         TabHeight       =   420
         WordWrap        =   0   'False
         TabCaption(0)   =   "Lawyer"
         TabPicture(0)   =   "frmEndorsementOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Attribute"
         TabPicture(1)   =   "frmEndorsementOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin MSComctlLib.ListView lvLetter 
            Height          =   2208
            Left            =   -74700
            TabIndex        =   11
            Top             =   600
            Width           =   6924
            _ExtentX        =   12224
            _ExtentY        =   3889
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2652
            Index           =   0
            Left            =   96
            TabIndex        =   24
            Tag             =   "EndorsementCoverageLawyer"
            Top             =   372
            Width           =   7008
            _ExtentX        =   12356
            _ExtentY        =   4683
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2652
            Index           =   1
            Left            =   -74904
            TabIndex        =   43
            Tag             =   "EndorsementEventAttribute"
            Top             =   372
            Width           =   7008
            _ExtentX        =   12356
            _ExtentY        =   4683
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.Label Label19 
            Caption         =   "Fee Dispute:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   15
            Top             =   1356
            Width           =   1656
         End
         Begin VB.Label Label17 
            Caption         =   "Judgement Claimant:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   14
            Top             =   1116
            Width           =   1656
         End
         Begin VB.Label Label15 
            Caption         =   "Disposition Name:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   13
            Top             =   876
            Width           =   1656
         End
         Begin VB.Label Label2 
            Caption         =   "Disposition Id:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   12
            Top             =   636
            Width           =   1656
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   288
         Left            =   1080
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   684
         Width           =   2160
      End
      Begin MSComctlLib.ImageList imlToolBarIcons 
         Left            =   2592
         Top             =   4536
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   8
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":0044
               Key             =   "Open"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":0156
               Key             =   "Print"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":0268
               Key             =   "Copy"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":037A
               Key             =   "Delete"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":048C
               Key             =   "View Large Icons"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":059E
               Key             =   "View Small Icons"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":06B0
               Key             =   "View List"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEndorsementOpen.frx":07C2
               Key             =   "View Details"
            EndProperty
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   6828
      Top             =   2304
      _ExtentX        =   688
      _ExtentY        =   688
      _Version        =   393216
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   8412
      _ExtentX        =   14843
      _ExtentY        =   688
      BandBorders     =   0   'False
      _CBWidth        =   8415
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   990
      MinHeight2      =   330
      Width2          =   3015
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   315
      Width3          =   945
      NewRow3         =   0   'False
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   3675
         ScaleHeight     =   330
         ScaleWidth      =   2820
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   30
         Width           =   2820
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   660
            Left            =   0
            TabIndex        =   23
            Top             =   30
            Width           =   1110
            _ExtentX        =   1958
            _ExtentY        =   1164
            ButtonWidth     =   1640
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   6690
         ScaleHeight     =   315
         ScaleWidth      =   1635
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   30
         Width           =   1635
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   0
            TabIndex        =   10
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1860
            TabIndex        =   19
            Top             =   30
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1720
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Endorsement"
                        Text            =   "Endorsement"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Policy"
                        Text            =   "Policy"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   660
            Left            =   990
            TabIndex        =   20
            Top             =   30
            Width           =   750
            _ExtentX        =   1323
            _ExtentY        =   1164
            ButtonWidth     =   1244
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Endorsement"
                        Text            =   "Endorsment"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   660
            Left            =   30
            TabIndex        =   21
            Top             =   30
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   1164
            ButtonWidth     =   1349
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the claim."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Deductible"
                        Text            =   "General"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   17
      Top             =   5355
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6271
            MinWidth        =   2117
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "12/23/2009"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "2:51 PM"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Endorsement"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator999 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Lawyer"
            Enabled         =   0   'False
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator834 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator326 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator332 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditClaim 
         Caption         =   "Edit E&ndorsement"
      End
      Begin VB.Menu mnuSeparator8221 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeperator9999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "&Tab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Lawyer"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Attribute"
            Index           =   1
         End
      End
      Begin VB.Menu mnuSeperator9090 
         Caption         =   "-"
      End
      Begin VB.Menu mnuChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings..."
      End
      Begin VB.Menu mnuSeparator282 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "Tool&bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator56465 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsEndorsement 
         Caption         =   "Browse &Endorsement"
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuToolsPolicy 
         Caption         =   "Browse &Policy"
      End
      Begin VB.Menu mnuSeparator261 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About LAW Explorer..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator655 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
   End
End
Attribute VB_Name = "frmEndorsementOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0415 (jjm)
Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mEndorsementHandle As String
Private mPolicyHandle As String
Private mEventHandle As String
Private mFirmHandle As String

Private FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

Private mDataSet As Dataset
Private CoverageLawyer As Dataset
Private EventAttribute As Dataset


'Private FirmData As DataProvider
'Private DispositionData As DataProvider



'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
      Exit Sub
      
    Case fimNew
      'Show person edit if new on the search is selected
      Dim fLawyer As New frmPersonEdit
      FormMgr.Add fLawyer
      fLawyer.ShowNew True, mFirmHandle
      Exit Sub
    
    Case fimSelect
      MsgBox "Add select code here"

    Case fimSave
      MsgBox fInterface.Child.LawyerHandle & " - Save Me"
            
  End Select
  
  Refresh
  FormRefresh
  Show
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub


Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  'Display Endorsement data
  Dim EndorsementData As Collection
  Set EndorsementData = mDataSet.DataProvider.Find(DatabaseName.ENDORSEMENT_KEY, mEndorsementHandle)
  SetFields lbldatafield, EndorsementData, "Endorsement"
  
  'Display Coverage data
  Dim CoverageData As Collection
  Set CoverageData = Factory.CoverageService.Find(mEndorsementHandle)
  SetFields lbldatafield, CoverageData, "Coverage"
    
  'Set handles from coverage data
  mPolicyHandle = GetDataFromCollection(CoverageData, DatabaseName.POLICY_KEY)
  mEventHandle = GetDataFromCollection(CoverageData, DatabaseName.EVENT_KEY)
    
  'Display Policy data
  Dim PolicyData As Collection
  Set PolicyData = Factory.PolicyService.Find(mPolicyHandle)
  SetFields lbldatafield, PolicyData, "Policy"
  mFirmHandle = GetDataFromCollection(PolicyData, DatabaseName.FIRM_KEY)
  
  Dim EventData As Collection
  Set EventData = Factory.EventService.Find(mEventHandle, DatabaseName.EVENTSTATUS_FIELD)
  SetFields lbldatafield, EventData, "Event"
  
  
  'Display Detail tabs
  CoverageLawyer.DataProvider.Filter = LawDialect.EqualString(DatabaseName.COVERAGE_KEY, mEndorsementHandle)
  EventAttribute.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle)
      
  Caption = "Endorsement Open (" & mEndorsementHandle & ")"

  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
    
  FormUtil.NavigationRefresh Me, mListView, mListIndex
    
  SSTabRefresh
  
  Refresh
End Sub

Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  mEndorsementHandle = ListItemText(mListView.SelectedItem, DatabaseName.ENDORSEMENT_KEY)
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
    Case 0  'Coverage Lawyer
      Set ListViewDataProvider = CoverageLawyer.DataProvider
    Case 1  'Event Attribute
      Set ListViewDataProvider = EventAttribute.DataProvider
  End Select
End Function

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.ENDORSEMENT_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no Endorsements to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(EndorsementId As String, Optional ShowTab As Integer = 0)
  mEndorsementHandle = EndorsementId
  mTab = ShowTab
  Show
End Sub

Public Sub SSTabRefresh()
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    If lvView(mTab).ListItems.Count > 0 Then
      Dim Index As Variant
      Index = IIf(lvView(mTab).SelectedItem.Key = "", lvView(mTab).SelectedItem.Index, lvView(mTab).SelectedItem.Key)
      lvView(mTab).Visible = False
      lvView(mTab).SelectedItem.Selected = False
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
      On Error Resume Next
      lvView(mTab).ListItems(Index).Selected = True
      lvView(mTab).Visible = True
      lvView(mTab).SetFocus
    Else
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
    End If
  Else
    SSTab.Tab = mTab
  End If
  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
End Sub

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

'
' End General Methods
'

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Initialize()
  Set mDataSet = New EndorsementDataset
  Set mDataSet.Session = LawSession

  Set CoverageLawyer = New CoverageLawyerView
  Set CoverageLawyer.Session = LawSession
  
  Set EventAttribute = NewGenericDataset(DatabaseName.EVENTATTRIBUTE_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyD 'Deductible
  '      mnuFileNewDetail_Click 0
    End Select
    Exit Sub
  End If
  
  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      mnuFileNewDetail_Click SSTab.Tab
  End Select
End Sub

Public Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    SSTab_Click mTab
  Else
    SSTab.Tab = mTab
  End If
  
    ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.ButtonHeight + 48
  
  ' Setup Toolbar.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  
  ' Custom Menu Shortcut Adjustments
  Me.KeyPreview = True
  
  mnuViewPrevious.Caption = mnuViewPrevious.Caption + Chr(9) + "Ctrl+<"
  mnuViewNext.Caption = mnuViewNext.Caption + Chr(9) + "Ctrl+>"
  
  'mnuFileNewDetail(0).Caption = mnuFileNewDetail(0).Caption + vbTab + "Ctrl+Shift+D"
  
  'Setup Form Manager
  FormMgr.Load Me
  
  FormUtil.EditLoadImages Me
  
  ' Refresh form with data
  FormRefresh
  
  FormUtil.TabControlEnable SSTab
  
End Sub

Private Sub Form_Paint()
  SSTabResize
End Sub

Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub mnuChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

Private Sub mnuEditEndorsement_Click()
  'FormUtil.ShowByPrimaryKey New frmEndorsementEdit, mEndorsementHandle, FormMgr
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If

  If lvView(SSTab.Tab).ListItems.Count < 1 Then
    MsgBox "There are no entries to delete."
    Exit Sub
  End If

  Select Case SSTab.Tab
  'ToDo: add delete row
    Case 0   'mDeductible
'      With lvView(0).SelectedItem
'        Call Factory.ApplicationLawyerService.DeleteApplicationLawyer( _
'            .ListSubItems("applicationId"), .ListSubItems("lawyerId"), _
'            vbNullStringArray, vbNullStringArray)
'      End With
  
  End Select
  mnuViewRefresh_Click
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case 0  'Deductible 'ToDo: add edit row
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub


Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNewDetail_Click(Index As Integer)
  Select Case Index 'ToDo:
    Case 0  'Coverage Lawyer
    '  Dispatcher.ShowSearch DatabaseName.LAWYER_TABLE, FormMgr, True
    Case 1 'Event Attribute
    
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.ENDORSEMENT_TABLE, mEndorsementHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.ENDORSEMENT_REF, DatabaseName.ENDORSEMENT_TABLE, mEndorsementHandle, mEndorsementHandle & " " & lbldatafield(3).Caption
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.ENDORSEMENT_TABLE, FormMgr
End Sub

Private Sub mnuToolsEndorsement_Click()
  Dispatcher.BrowseEndorsementOpen mEndorsementHandle
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsPolicy_Click()
  Dispatcher.BrowsePolicyOpen mPolicyHandle
End Sub

Private Sub mnuViewNext_Click()
  ' No validate on Open forms
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  ' No validate on Open forms
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
'  Dim pointer As New WaitPointer
'  pointer.pointer = vbHourglass
  
'  SetHotKeys
End Sub

Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  picBody_Resize
End Sub


Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab
  mTab = SSTab.Tab
  
  Dim Source As DataProvider
  Set Source = ListViewDataProvider(mTab)
  If Source Is Nothing Then Exit Sub

  ' Ensure list view is loaded.
  ListViewLoad lvView(mTab), Source
  
  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
  sbStatusBar.Panels(2).Text = Source.Source
  
  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub


Private Sub tbrAdd_Click()
End Sub


Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub

Private Sub tbrBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuToolsEndorsement_Click
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
  Case "Endorsement"
    mnuToolsEndorsement_Click
  Case "Policy"
    mnuToolsPolicy_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEditEndorsement_Click
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1  'Endorsement
      mnuEditEndorsement_Click
  End Select
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileNewDetail_Click SSTab.Tab
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNewDetail_Click (ButtonMenu.Index - 1)
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

