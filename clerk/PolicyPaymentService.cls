VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PolicyAccountingService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "PolicyAccountingService"
Private mDataSet As Dataset
'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.POLICYACCOUNTING_TABLE)

On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName

End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.POLICYACCOUNTING_KEY, Handle, Column)
End Function

Public Function FindByCoverage(Handle As String, Optional Column As String) As Collection
  Set FindByCoverage = mDataSet.DataProvider.Find(DatabaseName.COVERAGE_KEY, Handle, Column)
End Function

Public Function FindByEvent(EventHandle As String, Optional Column As String) As Collection
  Set FindByEvent = mDataSet.DataProvider.Find(DatabaseName.EVENT_KEY, EventHandle, Column)
End Function


'
' Interface to server
'


' parameterOrder='PolicyAccountingId PolicyId eventTypeId paymentDate PaymentAmount attributeNames attributeValues parameterNames parameterValues'>


'
' Create.
'
Public Function CreatePolicyAccounting( _
    PolicyAccountingId As String, _
    PolicyId As String, CoverageId As String, _
    PaymentDate As Date, PaymentAmount As Double, _
    EventTypeId As String, _
    SourceTypes() As String, _
    GLAccountIds() As String, _
    GLTransactionAmounts() As Double, _
    EventNote As String, _
    InvoiceIds As String) As Variant
    
On Error GoTo Exception

  CreatePolicyAccounting = Client.Create( _
      PolicyAccountingId, _
      PolicyId, CoverageId, _
      PaymentDate, PaymentAmount, _
      EventTypeId, _
      SourceTypes, GLAccountIds, GLTransactionAmounts, _
      EventNote, InvoiceIds)
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client
End Function

'
' Write an application.
'
Public Function WritePolicyAccounting( _
    PolicyAccountingHandle As String, _
    PolicyId As String, CoverageId As String, _
    PaymentDate As Date, PaymentAmount As Double, _
    EventTypeId As String, _
    SourceTypes() As String, _
    GLAccountIds() As String, GLTransactionAmounts() As Double, _
    EventNote As String, _
    InvoiceIds As String) As Boolean
    
On Error GoTo Exception
  
  Call Client.Write( _
      PolicyAccountingHandle, _
      PolicyId, CoverageId, _
      PaymentDate, PaymentAmount, _
      EventTypeId, _
      SourceTypes, GLAccountIds, GLTransactionAmounts, _
      EventNote, InvoiceIds)
  WritePolicyAccounting = True
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client
  WritePolicyAccounting = False
End Function

'
' Delete an PaymentAmount
'
Public Function DeletePolicyAccounting( _
    PolicyAccountingHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo Exception

  Call Client.Delete(PolicyAccountingHandle, ParameterNames, ParameterValues)
  DeletePolicyAccounting = True
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client
  DeletePolicyAccounting = False
End Function



