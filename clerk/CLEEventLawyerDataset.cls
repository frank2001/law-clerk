VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLEEventLawyerDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421
'previous ver 2.00.0310 (jjm)
'CLEEventLawyer Dataset

Option Explicit
Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer

Private Sub Class_Initialize()
  ListIndex = 3
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = mvarSession.NewDataProvider
  mDataProvider.Source = DatabaseName.CLEEVENTLAWYER_VIEW
  'mDataProvider.Column = "((""cleEventId"" || '-') || ""lawyerId"") as ""name"", ""cleEventId"", ""lawyerId"", ""beginDate"", ""endDate"", ""cleCredit"", ""isUsed"""
  'mDataProvider.Sort = """cleEventId"", ""lawyerId"""
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in cle event identifier field only"
  frm.cboIn.AddItem "in cle event name field only"
  frm.cboIn.AddItem "in lawyer identifier field only"
  frm.cboIn.AddItem "in lawyer entity locator field only"
  frm.cboIn.AddItem "in lawyer entity name field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, 3)
End Sub

Private Sub CLEEventLawyerSearchFilter(frm As frmGenericSearch, newFilter As Filter)
  If frm.chkBeginDate.Value = vbChecked Then
    newFilter.Add """beginDate"" > " & LawDialect.DateFormat(frm.dtpFromBeginDate.Value) & " and ""beginDate"" < " & LawDialect.DateFormat(frm.dtpToBeginDate.Value)
  End If
End Sub
  
' Sets lvView.  Returns a collection of key fields.
Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case 0
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.CLEEVENT_KEY, Words)
      Case 1
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.EVENTNAME_FIELD, Words)
      Case 2
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.LAWYER_KEY, Words)
      Case 3
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LAWYERENTITYLOCATOR_FIELD, Words)
      Case 4
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.LAWYERENTITYNAME_FIELD, Words)
    End Select
  End If
        
  If TypeOf frm Is frmGenericSearch Then
    CLEEventLawyerSearchFilter frm, newFilter
  End If

  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

' Finds
Private Function Dataset_FindNext() As Collection
End Function

Private Function Dataset_FindPrevious() As Collection
End Function

