Attribute VB_Name = "MPrintFunctions"
'==============================================================================
'Contains functions for printing a List- or TreeView.
'Code is ThirdParty and has only been modified a little.
'Is used by CPrintListView.
'
'Scope:     DLL-Public.
'==============================================================================


Option Explicit

Public Const vbCM = 56.7

Public Sub SetPage(oP As PrintPageLayout)

    Printer.PaperSize = oP.PaperSize
    Printer.Orientation = oP.Orientation

End Sub

Sub PrintPrintText(oT As PrintText, lLeft As Long, lRight As Long)
    Dim stText As String

    If oT.Text <> "" Then
        SetPrinterFont oT.Font

        If oT.Alignment = vbLeftJustify Then
            Printer.CurrentX = lLeft
        ElseIf oT.Alignment = vbRightJustify Then
            Printer.CurrentX = Printer.ScaleWidth - lRight - Printer.TextWidth(oT.Text)
        ElseIf oT.Alignment = vbCenter Then
            Printer.CurrentX = (Printer.ScaleWidth - Printer.TextWidth(oT.Text)) / 2
        End If

        stText = oT.Text

        SetPageNumber stText

        Printer.Print stText

    End If

End Sub

Sub PrintHeaderLV(LV As ListView, lLeft As Long)
    Dim ch As ColumnHeader
    Dim iCHWidth As Long
    Dim yMarg As Long, I As Long, Max As Single, Pos As Long, j As Long

'    Dim MaxHeaderHeight As Single
'    Dim headers() As String, lines() As String

    yMarg = Printer.CurrentY

'    ReDim headers(LV.ColumnHeaders.Count - 1)
'
'    For i = 0 To LV.ColumnHeaders.Count - 1
'        MaxHeaderHeight = 0
'        With LV.ColumnHeaders(i + 1)
'            headers(i) = .Text
'
'            ' Word Wrap Headers that are larger than the width.
'            If .Width > 0 Then
'              Do While Printer.TextWidth(headers(i)) > .Width
'                  lines = Split(headers(i), vbCrLf)
'                  Max = 0
'                  For j = 0 To UBound(lines)
'                      If Printer.TextWidth(lines(i)) > Printer.TextWidth(Max) Then
'                          Max = i
'                      End If
'                  Next j
'
'                  pos = InStrRev(lines(Max), " ")
'                  If pos = 0 Then Exit Do
'
'                  lines(Max) = Left$(lines(Max), pos - 1) & vbCrLf & Mid$(lines(Max), pos + 1)
'
'                  headers(i) = Join(lines, vbCrLf)
'              Loop
'            End If
'
'            If Printer.TextHeight(headers(i)) > MaxHeaderHeight Then
'                MaxHeaderHeight = Printer.TextHeight(headers(i))
'            End If
'        End With
'    Next

    For Each ch In LV.ColumnHeaders
      If ch.Width > 0 Then
        Printer.Line (lLeft + iCHWidth, yMarg + Printer.TextHeight("A"))-(lLeft + iCHWidth + ch.Width, yMarg + Printer.TextHeight("A"))

        If ch.Alignment = lvwColumnLeft Then
          Printer.CurrentX = lLeft + iCHWidth
        ElseIf ch.Alignment = lvwColumnCenter Then
          Printer.CurrentX = lLeft + iCHWidth + (ch.Width - Printer.TextWidth(ch.Text)) / 2
        ElseIf ch.Alignment = lvwColumnRight Then
          Printer.CurrentX = lLeft + iCHWidth + ch.Width - Printer.TextWidth(ch.Text)
        End If

        Printer.CurrentY = yMarg

        Printer.Print CropText(ch.Text, ch.Width)
        iCHWidth = iCHWidth + ch.Width
      End If
    Next
    
    Printer.CurrentY = Printer.CurrentY + 2 * Screen.TwipsPerPixelY
End Sub

Sub PrintItemLV(itmX As ListItem, LV As ListView, iLeft As Long, ByVal InColor As Boolean, ByVal Mark As Boolean)
    Dim ch As ColumnHeader
    Dim iCHWidth As Long
    Dim yMarg As Long
    Dim xMarg As Long
    Dim hRow As Long
    Dim hImage As Long
    Dim hItem As Long
    Dim stText As String
    Dim Color As Long, Bold As Boolean, oldBold As Boolean, oldColor As Long

    oldBold = Printer.Font.Bold
    oldColor = Printer.ForeColor

    hItem = Printer.TextHeight(itmX.Text)
    hRow = Printer.TextHeight(itmX.Text) + 2 * Printer.TwipsPerPixelY

    yMarg = Printer.CurrentY

    For Each ch In LV.ColumnHeaders
        If ch.Width > 0 Then
            If ch.Index = 1 Then
                xMarg = iLeft + 0
            Else
                xMarg = iLeft + iCHWidth
            End If

            If ch.Index = 1 Then
                stText = itmX.Text
                Color = itmX.ForeColor
                Bold = itmX.Bold
            Else
                stText = itmX.SubItems(ch.Index - 1)
                Color = itmX.ListSubItems(ch.Index - 1).ForeColor
                Bold = itmX.ListSubItems(ch.Index - 1).Bold
            End If
            
            Printer.Font.Bold = Bold Or (Mark And itmX.Selected)
            Printer.ForeColor = IIf(InColor, IIf(Mark And itmX.Selected, vbBlue, Color), vbBlack)

            If ch.Alignment = lvwColumnLeft Then
                Printer.CurrentX = xMarg
            ElseIf ch.Alignment = lvwColumnCenter Then
                Printer.CurrentX = xMarg + (ch.Width - Printer.TextWidth(stText)) / 2
            ElseIf ch.Alignment = lvwColumnRight Then
                Printer.CurrentX = xMarg + ch.Width - Printer.TextWidth(stText)
            End If

            Printer.CurrentY = yMarg + (hRow - hItem) / 2
            Printer.Print CropText(stText, ch.Width)
            iCHWidth = iCHWidth + ch.Width
            Printer.CurrentY = yMarg + hRow

        End If

    Next

    Printer.Font.Bold = oldBold
    Printer.ForeColor = oldColor
End Sub

Function CanPrintItem(ByVal hRow As Long, iBottomMargin As Long) As Boolean

    CanPrintItem = (Printer.CurrentY + hRow) < (Printer.ScaleHeight - iBottomMargin)

End Function


Function CropText(ByVal Text As String, Width As Long) As String
  ' If width is fine, then return.
  If (Printer.TextWidth(Text) <= Width) Then
    CropText = Text
    Exit Function
  End If
  
  ' Keep cropping until it fits
  Do While Printer.TextWidth(Text & "...") > Width
    Dim Length As Integer
    Length = Len(Text)
    If Length = 1 Then
      CropText = Text
      Exit Function
    End If
    Text = Left$(Text, Length - 1)
  Loop
  
  CropText = Text & "..."
End Function


Sub SetPrinterFont(ff As StdFont)

    Printer.FontName = ff.Name
    Printer.FontSize = ff.Size
    Printer.FontBold = ff.Bold
    Printer.FontItalic = ff.Italic
    Printer.FontStrikethru = ff.Strikethrough
    Printer.FontUnderline = ff.Underline

End Sub

Sub SetPageNumber(st As String)
    Dim Pos As Long

    Pos = InStr(st, "<pag>")

    If Pos > 0 Then
        st = Left$(st, Pos - 1) & CStr(Printer.Page) & Mid$(st, Pos + 5)
    End If

End Sub

Function GetMax(iV1 As Long, iV2 As Long) As Long

    If iV1 > iV2 Then
        GetMax = iV1
    Else
        GetMax = iV2
    End If
End Function


Function CanPrintNode(ByVal stText As String, ByVal hImage As Long, iBottomMargin As Long) As Boolean
    Dim hRow As Long

    hRow = GetMax(hImage, Printer.TextHeight(stText))

    CanPrintNode = (Printer.CurrentY + hRow) < (Printer.ScaleHeight - iBottomMargin)

End Function

Function CalcLevel(nodX As Node) As Integer
    Dim iLevel As Integer
    Dim nodP As Node

    iLevel = 0
    Set nodP = nodX
    While Not nodP.Parent Is Nothing
        iLevel = iLevel + 1
        Set nodP = nodP.Parent
    Wend

    CalcLevel = iLevel

End Function

