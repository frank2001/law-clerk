VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ObjectInterfaceService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'ver 2.30.0904 (jjm)
Option Explicit

Private Const ServiceName = "ObjectInterfaceService"

Private Client As SoapClient30

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & "component/" & ServiceName & ".wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & "rpcrouter" & LawSession.Session
End Sub

'
' This service enables standardized calls to be made to the middle tier.
'
' ObjectName - variant describing the server side object which will handle the call
' Handle - string value used to determine which specific data to modify
' PropertyNames() - string array of field names.
' PropertyValues() - string array of field values corresponding with PropertyNames()
' ParameterNames() - string array of field names.
' ParameterValues() - string array of field values corresponding with ParameterNames()
'
' If a property is not included there is no effect.
'
' In order to achieve null, pass a property name with a null property value.
'

Public Function ClientReturnVarient( _
    ObjectName As String, _
    ByRef PropertyNames() As String, ByRef PropertyValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
  
  ClientReturnVarient = Client.CallService( _
    ObjectName, _
    PropertyNames(), PropertyValues(), _
    ParameterNames(), ParameterValues())
  Exit Function
  
ErrorHandler:
  MsgBox "Fault: " & Client.FaultString, vbExclamation
End Function

Public Function ClientReturnBoolean( _
    ObjectName As String, Handle As String, _
    ByRef PropertyNames() As String, ByRef PropertyValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler
  Call Client.CallService(ObjectName, Handle, _
    PropertyNames(), PropertyValues(), _
    ParameterNames(), ParameterValues())
  ClientReturnBoolean = True
  Exit Function

ErrorHandler:
  MsgBox "Fault: " & Client.FaultString, vbExclamation
End Function
