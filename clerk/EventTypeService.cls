VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EventTypeService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mDataSet As Dataset
Private mEventTypeRuleView As Dataset



Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.EVENTTYPE_TABLE)
  Set mEventTypeRuleView = NewGenericDataset(DatabaseName.EVENTTYPERULE_VIEW)

End Sub


'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.EVENTTYPE_KEY, Handle, Column)
End Function

'
' Interface to server
'
Public Function SourceCollection(SourceTypeId As String) As Collection
  mDataSet.DataProvider.Column = DatabaseName.EVENTTYPENAME_FIELD & ", " & DatabaseName.EVENTTYPE_KEY
  mDataSet.DataProvider.Filter = DatabaseName.SOURCETYPE_KEY & " = '" & SourceTypeId & "'"
  mDataSet.DataProvider.Sort = DatabaseName.EVENTTYPENAME_FIELD
  mDataSet.DataProvider.OpenRecordset
  Set SourceCollection = mDataSet.DataProvider.Collection
End Function

Public Function GroupCollection(ruleId As String) As Collection
  With mEventTypeRuleView
    .DataProvider.Column = DatabaseName.EVENTTYPENAME_FIELD & ", " & DatabaseName.EVENTTYPE_KEY
    '.DataProvider.Filter = """eventTypeId"" in (select ""eventTypeId"" from """ & DatabaseName.EVENTTYPERULE_TABLE & """ where ""eventGroupId"" = '" & EventGroup & "')"
    .DataProvider.Filter = DatabaseName.RULE_KEY & " = '" & ruleId & "'"
    .DataProvider.Sort = DatabaseName.EVENTTYPENAME_FIELD
    .DataProvider.OpenRecordset
    Set GroupCollection = .DataProvider.Collection
  End With
End Function

Public Function GroupCollectionByLocator(ruleId As String) As Collection
  With mEventTypeRuleView
    .DataProvider.Column = DatabaseName.EVENTTYPENAME_FIELD & ", " & DatabaseName.EVENTTYPE_KEY & ", " & DatabaseName.EVENTTYPELOCATOR_FIELD
    .DataProvider.Filter = DatabaseName.RULE_KEY & " = '" & ruleId & "'"
    .DataProvider.Sort = DatabaseName.EVENTTYPENAME_FIELD
    .DataProvider.OpenRecordset
    Set GroupCollectionByLocator = .DataProvider.Collection
  End With
End Function
