VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "VendorService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'ver 2.00.0318 (jjm)
Option Explicit

Private mDataSet As Dataset
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.VENDOR_TABLE)
End Sub



Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.VENDOR_Key, Handle, Column)
End Function

'Helper function to get the Firm Table information by the organizationId
Public Function GetFirmByOrganizationId(organizationId As String) As Collection
  Set GetFirmByOrganizationId = mDataSet.DataProvider.Find(DatabaseName.VENDOR_Key, organizationId)
End Function


