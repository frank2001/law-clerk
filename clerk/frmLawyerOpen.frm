VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmLawyerOpen 
   Caption         =   "Lawyer Open"
   ClientHeight    =   6495
   ClientLeft      =   2715
   ClientTop       =   2160
   ClientWidth     =   7710
   Icon            =   "frmLawyerOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6495
   ScaleWidth      =   7710
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5856
      Left            =   0
      ScaleHeight     =   5850
      ScaleWidth      =   7710
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   384
      Width           =   7716
      Begin VB.Frame fraStatus 
         Caption         =   "Status"
         Height          =   1248
         Left            =   3024
         TabIndex        =   17
         Top             =   1308
         Width           =   4608
         Begin VB.TextBox txtDataField 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            DataField       =   "firmNote"
            DataMember      =   "ContactFirm"
            Height          =   780
            Index           =   0
            Left            =   168
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   396
            Width           =   4368
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "No"
            DataField       =   "isUninsurable"
            DataMember      =   "Lawyer"
            Height          =   192
            Index           =   8
            Left            =   1200
            TabIndex        =   19
            Top             =   204
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   216
         End
         Begin VB.Label lblStatus 
            AutoSize        =   -1  'True
            ForeColor       =   &H00000000&
            Height          =   192
            Left            =   156
            TabIndex        =   18
            Top             =   204
            Width           =   600
         End
      End
      Begin VB.Frame fraFirm 
         Caption         =   "Firm"
         Height          =   1248
         Left            =   60
         TabIndex        =   14
         Top             =   1296
         Width           =   2880
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "entityLocator"
            DataField       =   "entityLocator"
            DataMember      =   "EntityFirm"
            Height          =   195
            Index           =   4
            Left            =   180
            TabIndex        =   16
            Top             =   240
            UseMnemonic     =   0   'False
            Width           =   915
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "entityName"
            DataField       =   "entityName"
            DataMember      =   "EntityFirm"
            Height          =   192
            Index           =   5
            Left            =   180
            TabIndex        =   15
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   2568
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame fraLawyer 
         Caption         =   "Lawyer"
         Height          =   1272
         Left            =   72
         TabIndex        =   7
         Top             =   0
         Width           =   7560
         Begin VB.Label lblInsureStatus 
            AutoSize        =   -1  'True
            Caption         =   "latestPolicyStatus"
            DataField       =   "latestPolicyStatus"
            DataMember      =   "Lawyer"
            Height          =   195
            Left            =   1950
            TabIndex        =   38
            Top             =   1035
            UseMnemonic     =   0   'False
            Width           =   1245
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Insure Status Id:"
            Height          =   192
            Left            =   132
            TabIndex        =   37
            Top             =   1020
            Width           =   1116
         End
         Begin VB.Label lblYears 
            AutoSize        =   -1  'True
            Caption         =   "Years in Practice:"
            Height          =   192
            Left            =   132
            TabIndex        =   23
            Top             =   804
            Width           =   1248
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "inPracticeDuration"
            DataField       =   "inPracticeDuration"
            DataMember      =   "Lawyer"
            Height          =   192
            Index           =   3
            Left            =   1944
            TabIndex        =   22
            Top             =   816
            UseMnemonic     =   0   'False
            Width           =   1308
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "State Bar Number:"
            Height          =   195
            Left            =   4050
            TabIndex        =   21
            Top             =   810
            Width           =   1305
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "stateBarNumber"
            DataField       =   "stateBarNumber"
            DataMember      =   "LawyerStateBar"
            Height          =   195
            Index           =   11
            Left            =   5850
            TabIndex        =   20
            Top             =   810
            UseMnemonic     =   0   'False
            Width           =   1140
         End
         Begin VB.Label lblName 
            AutoSize        =   -1  'True
            Caption         =   "Name:"
            Height          =   192
            Left            =   132
            TabIndex        =   13
            Top             =   336
            Width           =   480
         End
         Begin VB.Label lbladmitted 
            AutoSize        =   -1  'True
            Caption         =   "Wisc. Bar Year admitted:"
            Height          =   192
            Left            =   132
            TabIndex        =   12
            Top             =   576
            Width           =   1764
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "barAdmittedDate"
            DataField       =   "barAdmitDate"
            DataMember      =   "LawyerStateBar"
            Height          =   192
            Index           =   2
            Left            =   1944
            TabIndex        =   11
            Top             =   576
            UseMnemonic     =   0   'False
            Width           =   1224
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "entityName"
            DataField       =   "entityName"
            DataMember      =   "EntityPerson"
            Height          =   195
            Index           =   1
            Left            =   1950
            TabIndex        =   10
            Top             =   330
            UseMnemonic     =   0   'False
            Width           =   795
         End
         Begin VB.Label Label2 
            Caption         =   "Expiration Date:"
            Height          =   192
            Left            =   4044
            TabIndex        =   9
            Top             =   576
            Width           =   1692
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerExpirationDate"
            DataField       =   "expirationDate"
            DataMember      =   "Lawyer"
            Height          =   192
            Index           =   10
            Left            =   5844
            TabIndex        =   8
            Top             =   576
            UseMnemonic     =   0   'False
            Width           =   1524
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   3396
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   2004
         Width           =   1416
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3132
         Left            =   36
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   2628
         Width           =   7608
         _ExtentX        =   13414
         _ExtentY        =   5530
         _Version        =   393216
         Style           =   1
         Tabs            =   6
         TabsPerRow      =   10
         TabHeight       =   420
         TabCaption(0)   =   "Firm"
         TabPicture(0)   =   "frmLawyerOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Claim"
         TabPicture(1)   =   "frmLawyerOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "CLE"
         TabPicture(2)   =   "frmLawyerOpen.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Contact"
         TabPicture(3)   =   "frmLawyerOpen.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Policy"
         TabPicture(4)   =   "frmLawyerOpen.frx":007C
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "lvView(4)"
         Tab(4).ControlCount=   1
         TabCaption(5)   =   "Document"
         TabPicture(5)   =   "frmLawyerOpen.frx":0098
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "lvView(5)"
         Tab(5).ControlCount=   1
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   2
            Left            =   -74904
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "LawyerCLELawyer"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   1
            Left            =   -74904
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "LawyerClaim"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   3
            Left            =   -74904
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "LawyerContact"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   0
            Left            =   96
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "LawyerFirmLawyer"
            Top             =   375
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   4
            Left            =   -74904
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "LawyerPolicyLawyerCoverageView"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   5
            Left            =   -74904
            TabIndex        =   36
            TabStop         =   0   'False
            Tag             =   "LawyerDocument"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   264
      Left            =   0
      TabIndex        =   4
      Top             =   6228
      Width           =   7716
      _ExtentX        =   13600
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5054
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "7/21/2011"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "4:57 PM"
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   7716
      _ExtentX        =   13600
      _ExtentY        =   688
      _CBWidth        =   7710
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinWidth1       =   795
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   990
      MinHeight2      =   330
      Width2          =   990
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   330
      Width3          =   945
      NewRow3         =   0   'False
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   3705
         ScaleHeight     =   330
         ScaleWidth      =   2940
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   30
         Width           =   2940
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   660
            Left            =   0
            TabIndex        =   35
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1508
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1830
            TabIndex        =   31
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1640
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Firm"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Lawyer"
                        Text            =   "Lawyer"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   660
            Left            =   990
            TabIndex        =   32
            Top             =   15
            Width           =   750
            _ExtentX        =   1323
            _ExtentY        =   1164
            ButtonWidth     =   1191
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   660
            Left            =   0
            TabIndex        =   33
            Top             =   15
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   1164
            ButtonWidth     =   1270
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   6870
         ScaleHeight     =   330
         ScaleWidth      =   750
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   30
         Width           =   750
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   29
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   780
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   1092
      Top             =   948
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Lawyer"
            Enabled         =   0   'False
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewdetail 
            Caption         =   "&Firm"
            Enabled         =   0   'False
            Index           =   0
         End
         Begin VB.Menu mnuFileNewdetail 
            Caption         =   "C&laim"
            Enabled         =   0   'False
            Index           =   1
         End
         Begin VB.Menu mnuFileNewdetail 
            Caption         =   "&CLE"
            Enabled         =   0   'False
            Index           =   2
         End
         Begin VB.Menu mnuFileNewdetail 
            Caption         =   "C&ontact"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuFileNewdetail 
            Caption         =   "&Policy"
            Enabled         =   0   'False
            Index           =   4
         End
         Begin VB.Menu mnuFileNewdetail 
            Caption         =   "&Document"
            Index           =   5
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Person"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator847 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete"
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator765 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator3412 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditLawyer 
         Caption         =   "&Lawyer"
      End
      Begin VB.Menu mnuSeperator963 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator82612 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuSeperator985 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Complete Row"
         Index           =   0
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Void Row"
         Index           =   1
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "&Tab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Firm"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "Cl&aim"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&CLE"
            Index           =   2
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "C&ontact"
            Index           =   3
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Policy"
            Index           =   4
         End
      End
      Begin VB.Menu mnuSeparator123 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns"
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuSeparator2384 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator65465 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "Browse &Firm"
         Index           =   0
      End
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "Browse &Lawyer"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator28261 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditNote 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeperator347 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "Web Merge"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator655 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuPopupWebMerge 
         Caption         =   "Web Merge"
      End
      Begin VB.Menu mnuSeperator656 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSeperator657 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Complete"
         Index           =   0
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Void"
         Index           =   1
      End
   End
End
Attribute VB_Name = "frmLawyerOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ver 2.10.0416 (jjm)
' previous ver 2.10.0415 (jjm)
' previous ver 2.00.0303 (jjm) WILMIC
'
' LawyerOpen is based on FirmOpen.  See FirmOpen for an in-depth
' explanation of common methods.
'

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
  
Private mLawyerHandle As String
Private mFirmHandle As String
Private mEventHandle As String

Private FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

Private mDatafields As Collection
Private mDataSet As Dataset

' Detail Tabs
Private CLELawyer As Dataset
Private CLAIMLAWYER As Dataset
Private Contact As Dataset
Private FirmLawyer As Dataset
Private Lawyer As Dataset
Private Location As Dataset
Private Person As Dataset
Private Policy As Dataset
Private PolicyLawyer As Dataset
Private PolicyLawyerCoverageView As Dataset
Private Letter As Dataset
'Private mEvent As Dataset


'
' FormInterface Interface
'

'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    
'    Case fimSave
'      If fInterface.Key = DatabaseName.EVENT_KEY & "New" Then
'        'Setup for auto-complete
'        Dim Handles(0) As String
'        Dim ParameterNames() As String
'        Dim ParameterValues() As String
'        Handles(0) = fInterface.Child.EventHandle
'        'Automatically Complete the documents
'        Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
'      End If
      
  End Select
  
  
  Refresh
  FormRefresh
  Show
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub


Public Sub FormRefresh()

  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Set mDatafields = Factory.LawyerService.Find(mLawyerHandle)
  
  mFirmHandle = GetDataFromCollection(mDatafields, DatabaseName.LATESTFIRMID_FIELD)
  
  SetFields lblDataField, mDatafields, "Lawyer"
  SetFields lblDataField, Factory.EntityService.Find(mFirmHandle), "EntityFirm"
  SetFields lblDataField, Factory.LawyerService.FindLawyerStateBar(mLawyerHandle, "WI"), "LawyerStateBar"
    
  Dim EntityPersonData As Collection
  Set EntityPersonData = Factory.EntityService.Find(mLawyerHandle)
  SetFields lblDataField, EntityPersonData, "EntityPerson"
  mEventHandle = GetDataFromCollection(EntityPersonData, DatabaseName.EVENT_KEY)
    
  'Set the Insure Status Label - lookup in the EventType table under eventTypeName
  Dim Col As Collection
  Set Col = Factory.EventTypeService.Find(GetDataFromCollection(mDatafields, DatabaseName.LATESTPOLICYSTATUS_FIELD), DatabaseName.EVENTTYPENAME_FIELD)
  lblInsureStatus.Caption = IIf(Col Is Nothing, vbNullString, GetDataFromCollection(Col, DatabaseName.EVENTTYPENAME_FIELD))
  
  Me.Caption = "Lawyer (" & mLawyerHandle & ")"
  
  mnuToolsBrowse(0).Enabled = (mFirmHandle <> vbNullString)
    
  'Open Lawyer Dataset
  mDataSet.DataProvider.filter = DatabaseName.LAWYER_KEY & " = '" & mLawyerHandle & "'"
  'Open CLE Lawyer dataset
  CLELawyer.DataProvider.filter = DatabaseName.LAWYER_KEY & " = '" & mLawyerHandle & "'"
  'Open Claim Lawyer Dataset
  CLAIMLAWYER.DataProvider.filter = DatabaseName.LAWYER_KEY & " = '" & mLawyerHandle & "'"
  'Open Contact dataset
  Contact.DataProvider.filter = DatabaseName.PERSON_KEY & " = '" & mLawyerHandle & "'"
  'Open FirmLawyer dataset
  FirmLawyer.DataProvider.filter = DatabaseName.LAWYER_KEY & " = '" & mLawyerHandle & "'"
  'Open Person Dataset
  Person.DataProvider.filter = DatabaseName.PERSON_KEY & " = '" & mLawyerHandle & "'"
  'Open Policy Dataset
  Policy.DataProvider.filter = DatabaseName.POLICY_KEY & " = '" & mLawyerHandle & "'"
  'Open PolicyLawyer Dataset
  PolicyLawyer.DataProvider.filter = DatabaseName.LAWYER_KEY & " = '" & mLawyerHandle & "'"
  'Open PolicyLawyerCoverage Dataset
  PolicyLawyerCoverageView.DataProvider.filter = DatabaseName.LAWYER_KEY & " = '" & mLawyerHandle & "'"
  'Open ReferenceView for letters
  Letter.DataProvider.filter = DatabaseName.PERSON_KEY & " = '" & mLawyerHandle & "'"
  
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  SSTabRefresh
End Sub

Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  mLawyerHandle = ListItemText(mListView.SelectedItem, DatabaseName.LAWYER_KEY)
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
    Case 0
      Set ListViewDataProvider = FirmLawyer.DataProvider
    Case 1
      Set ListViewDataProvider = CLAIMLAWYER.DataProvider
    Case 2
      Set ListViewDataProvider = CLELawyer.DataProvider
    Case 3
      Set ListViewDataProvider = Contact.DataProvider
    Case 4
      Set ListViewDataProvider = PolicyLawyerCoverageView.DataProvider
    Case 5
      Set ListViewDataProvider = Letter.DataProvider
  End Select
End Function

'ver 2.10.0416 (jjm)
Private Sub MenuRefresh()
  Dim IsDocumentTab, IsFirmTab, IsClaimTab, IsPolicyTab As Boolean
  IsFirmTab = (SSTab.Tab = 0)
  IsClaimTab = (SSTab.Tab = 1)
  IsPolicyTab = (SSTab.Tab = 4)
  IsDocumentTab = (SSTab.Tab = 5)
  
  'Disable and enable Edit menus when Document Tab is selected
  mnuEditEventRow(0).Enabled = IsDocumentTab
  mnuEditEventRow(1).Enabled = IsDocumentTab
  mnuToolsMerge.Enabled = IsDocumentTab
  mnuToolsWebMerge.Enabled = IsDocumentTab
  mnuPopupEvent(0).Enabled = IsDocumentTab
  mnuPopupEvent(1).Enabled = IsDocumentTab
  mnuPopupMerge.Enabled = IsDocumentTab
  mnuPopupWebMerge.Enabled = IsDocumentTab
  
  
  'Browse
  mnuEditBrowseRow.Enabled = IsFirmTab Or IsClaimTab Or IsPolicyTab
  mnuPopupBrowse.Enabled = IsFirmTab Or IsClaimTab Or IsPolicyTab
  
  'Delete
  mnuEditDeleteRow.Enabled = False
  mnuPopupDelete.Enabled = False
  
  'New
  mnuPopupNew.Enabled = IsDocumentTab
  
End Sub

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = "Application")
  If View.ListItems.Count = 0 Then
    MsgBox "There are no lawyers to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(LawyerId As String, Optional ShowTab As Integer = 0)
  mLawyerHandle = LawyerId
  mTab = ShowTab
  Show
End Sub

Public Sub SSTabRefresh()
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    If lvView(mTab).ListItems.Count > 0 Then
      Dim Index As Variant
      Index = IIf(lvView(mTab).SelectedItem.Key = "", lvView(mTab).SelectedItem.Index, lvView(mTab).SelectedItem.Key)
      lvView(mTab).Visible = False
      lvView(mTab).SelectedItem.Selected = False
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
      On Error Resume Next
      lvView(mTab).ListItems(Index).Selected = True
      lvView(mTab).Visible = True
      lvView(mTab).SetFocus
      Exit Sub
    Else
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
    End If
  Else
    SSTab.Tab = mTab
  End If
  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
  sbStatusBar.Panels(2).Text = ListViewDataProvider(mTab).Source
  
  MenuRefresh
End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - lvView(Index).Left, 0)
  lvView(Index).Width = Max(SSTab.Width - (lvView(Index).Left * 2), 0)
End Sub


Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

'
' End General Methods
'



'
' Begn Controls
'

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Initialize()
   Set mDataSet = New LawyerDataset
   Set mDataSet.Session = LawSession
  
   Set FirmLawyer = NewGenericDataset(DatabaseName.FIRMLAWYER_VIEW)
   
   Set CLELawyer = New CLEEventLawyerDataset
   Set CLELawyer.Session = LawSession
   
   Set Contact = NewGenericDataset(DatabaseName.CONTACT_VIEW)
   
   Set Policy = New PolicyDataset
   Set Policy.Session = LawSession
   
   Set PolicyLawyer = New PolicyLawyerCoverageDataset
   Set PolicyLawyer.Session = LawSession
   
   Set CLAIMLAWYER = NewGenericDataset(DatabaseName.CLAIMLAWYERCOVERAGE_VIEW)
   
   Set Person = New PersonDataset
   Set Person.Session = LawSession
   
   Set PolicyLawyerCoverageView = NewGenericDataset(DatabaseName.POLICYLAWYERCOVERAGEVIEW_TABLE)
   
   Set Letter = NewGenericDataset(DatabaseName.EVENTRECIPIENT_VIEW)
End Sub

'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
'      Case vbKeyL
'        mnuEditInsertLawyer_Click
'      Case vbKeyA
'        mnuEditInsertLawArea_Click
'      Case vbKeyM
'        mnuEditInsertLimit_Click
'      Case vbKeyI
'        mnuEditInsertInsurer_Click
      Case vbKeyD
        mnuFileNewDetail_Click 5
    End Select
    Exit Sub
  End If
  
  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      Select Case SSTab.Tab
        Case 0:   'Lawyer
'          mnuEditInsertLawyer_Click
        Case 1:   'Law Area
'          mnuEditInsertLawArea_Click
        Case 2:    'Limit
'          mnuEditInsertLimit_Click
        Case 3:    'Insurer
'          mnuEditInsertInsurer_Click
        Case 4:    'Question
        Case 5:
          mnuFileNewDetail_Click 5
      End Select
  End Select
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
 ' Setup Toolbar.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.Height + 24

  FormUtil.EditLoadImages Me
  FormUtil.MenuLoad Me
  
  ' Custom Menu Insert Shortcut Adjustments
  mnuFileNewDetail(5).Caption = mnuFileNewDetail(5).Caption + Chr(9) + "Ctrl+Shift+D"
  mnuEditEditRow.Caption = mnuEditEditRow.Caption + Chr(9) + "Enter"
  
  ' Setup Form Manager
  FormMgr.Load Me
  
  ' Refresh form with data
  FormRefresh
  
  ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
End Sub

Private Sub Form_Paint()
  SSTabResize
End Sub

Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub


Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

'ver 2.10.0416 (jjm)
Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

'ver 2.10.0415 (jjm)
Private Sub mnuEditBrowseRow_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Select Case SSTab.Tab
    Case 0:    'Firm
      Dispatcher.BrowseFirmOpen ListItemText(lvView(0).SelectedItem, DatabaseName.FIRM_KEY)
    Case 1:    'Claim
      Dispatcher.BrowseClaimOpen ListItemText(lvView(1).SelectedItem, DatabaseName.CLAIM_KEY)
    Case 4:    'Policy
      Dispatcher.BrowsePolicyOpen ListItemText(lvView(4).SelectedItem, DatabaseName.POLICY_KEY)
  End Select
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If
  
'  Select Case SSTab.Tab ':
'    Case 0:
'  End Select
'  mnuViewRefresh_Click
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case 0:   'Firm
      FormUtil.ShowByListView New frmFirmOpen, FormMgr, lvView(0)
    Case 1:   'Claim
      FormUtil.ShowByListView New frmClaimOpen, FormMgr, lvView(1)
    Case 2:    'CLE
      FormUtil.ShowByListView New frmCLEEventOpen, FormMgr, lvView(2)
    Case 3:    'Contact
      If lvView(3).ListItems.Count = 0 Then
        MsgBox "There are no contacts to view or edit."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmContactEdit, ListItemText(lvView(3).SelectedItem, DatabaseName.CONTACT_KEY), FormMgr
    Case 4:
      FormUtil.ShowByListView New frmPolicyOpen, FormMgr, lvView(4)
    Case 5:
       FormUtil.ShowByListView New frmEventDocumentEdit, FormMgr, lvView(5)
'      Dim fLetter As New frmEventDocumentEdit
'      FormMgr.Add fLetter
'      fLetter.ShowByListView lvView(5)

  End Select
End Sub
'
' Edit/Insert Routines
'

Private Sub mnuEditEventRow_Click(Index As Integer)
  Dim Handles(0) As String
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  
  Handles(0) = ListItemText(lvView(5).SelectedItem, DatabaseName.EVENT_KEY)
'  Factory.FolderParameterService.ParameterLoad ListItemText(lvView(5).SelectedItem, DatabaseName.FOLDER_KEY), Index, ParameterNames, ParameterValues, True

  If Index = lawActionComplete Then
    If MsgBox("Are you sure you want to complete item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
    End If
  ElseIf Index = lawActionVoid Then
    If MsgBox("Are you sure you want to void item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetVoid Handles, ParameterNames, ParameterValues
    End If
  End If
  
  FormRefresh
End Sub

Private Sub mnuEditLawyer_Click()
  Dim Form As New frmPersonEdit
  FormMgr.Add Form
  Form.ShowByPrimaryKey mLawyerHandle
End Sub

Private Sub mnuEditMove_Click()
  'Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuEditNote_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, Factory.PersonService.Find(mLawyerHandle)(DatabaseName.EVENT_KEY), FormMgr
End Sub

'
' File menu routines
'

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNewDetail_Click(Index As Integer)
  Select Case Index
    Case 0 'Firm
    
    Case 1 'Claim
    
    Case 2 'CLE
    
    Case 3 'contact
    
    Case 4 'Policy
    
    Case 5 'Document
      Dim fLetter As New frmEventDocumentEdit
      FormMgr.Add fLetter, DatabaseName.EVENT_KEY & "New"
      fLetter.ShowNew DatabaseName.DOCUMENT_RULE, mEventHandle, lblDataField(1).Caption, mLawyerHandle
  End Select
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmPersonOpen, mLawyerHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowProperties DatabaseName.ENTITY_TABLE, mEventHandle
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.LAWYER_REF, DatabaseName.LAWYER_TABLE, mLawyerHandle, lblDataField(1)
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , hWnd
End Sub

Private Sub mnuPopupBrowse_Click()
  mnuEditBrowseRow_Click
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupEvent_Click(Index As Integer)
  mnuEditEventRow_Click Index
End Sub

Private Sub mnuPopupMerge_Click()
  mnuToolsMerge_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNewDetail_Click SSTab.Tab
End Sub

Private Sub mnuPopupWebMerge_Click()
  mnuToolsWebMerge_Click
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.LAWYER_TABLE, FormMgr
End Sub

Private Sub mnuToolsBrowse_Click(Index As Integer)
  Select Case Index
    Case 0 'Firm
      Dispatcher.BrowseFirmOpen mFirmHandle
    Case 1 'Lawyer
      Dispatcher.BrowseLawyerOpen mLawyerHandle
  End Select
End Sub

Private Sub mnuToolsMerge_Click()
  Dim pointer As New WaitPointer, Handles(0) As String
  pointer.Default

  Handles(0) = ListItemText(lvView(5).SelectedItem, DatabaseName.EVENT_KEY)
  
  ' Print Envelopes
  Factory.EventRecipientService.PrintEnvelope Handles(0)

  ' Perform Document Merge
  Factory.EventService.DocumentMerge Handles
  
  FormRefresh
End Sub

Private Sub mnuToolsWebMerge_Click()
  Factory.EventService.WebMergeByView lvView(5)
End Sub

Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

Private Sub mnuViewNext_Click()
  ' No validate on Open forms
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  ' No validate on Open forms
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Public Sub mnuViewRefresh_Click()
  FormRefresh
  SSTabRefresh
End Sub

Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

' Save column headers for current view.
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
End Sub

Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  mTab = SSTab.Tab
  
  Dim Source As DataProvider
  Set Source = ListViewDataProvider(mTab)
  If Source Is Nothing Then Exit Sub

  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
  sbStatusBar.Panels(2).Text = Source.Source
  
  ' Ensure list view is loaded.
  ListViewLoad lvView(mTab), Source
  
  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
  
  MenuRefresh
End Sub

Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub


Private Sub tbrBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuToolsBrowse_Click 1
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuToolsBrowse_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEditLawyer_Click
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub
