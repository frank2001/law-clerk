VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EventTypeGroupView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
Option Explicit

Implements Dataset

Const TableName = DatabaseName.EVENTTYPERULE_TABLE

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private mListIndex As Integer

Private Sub Class_Initialize()
  mListIndex = 0
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = TableName
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  frm.cboIn.AddItem "in event group identifier field only"
  frm.cboIn.AddItem "in event type identifier field only"
  frm.cboIn.AddItem "in event type locator field only"
  frm.cboIn.AddItem "in event type name field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, mListIndex, 0)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  mListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Select Case mListIndex
      Case 0
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.RULE_KEY, Words)
      Case 1
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.EVENTTYPE_KEY, Words)
      Case 2
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.EVENTTYPELOCATOR_FIELD, Words)
      Case 3
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.EVENTTYPENAME_FIELD, Words)
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

