VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SQLDataProvider"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Used with Malcolm 3.00 api.

Option Explicit
Implements DataProvider

Private mRecordset As MSXML2.DOMDocument

Private mConnection As String
Private mSession As String
Private mBaseFilter As String
Private mColumn As String
Private mSource As String
Private mFilter As String
Private mSort As String

'Const URL_UNSAFE = """,<>%\^[]`;/?:@=&#"
'Const DIR_UNSAFE = "\/:*?""<>|&"



Public Property Let Connection(ByVal vData As String)
  mConnection = vData
End Property


Public Property Let Session(ByVal vData As String)
  mSession = vData
End Property



'
' Base filter is used to create a standard filter to be used with
' each search. This is mainly used with the filter created in folder
' properties edit.
'
Public Property Let DataProvider_BaseFilter(ByVal vData As String)
  mBaseFilter = vData
End Property

Public Property Get DataProvider_BaseFilter() As String
  DataProvider_BaseFilter = mBaseFilter
End Property

Public Property Get DataProvider_IsEmpty() As Boolean
  If mRecordset Is Nothing Then
    DataProvider_IsEmpty = True
  Else
    DataProvider_IsEmpty = mRecordset.documentElement.selectSingleNode("data/" & mSource) Is Nothing
  End If
End Property

Public Property Get DataProvider_IsOpen() As Boolean
  DataProvider_IsOpen = Not mRecordset Is Nothing
End Property


Public Property Let DataProvider_Source(ByVal vData As String)
  mSource = vData
  Set mRecordset = Nothing
End Property


Public Property Get DataProvider_Source() As String
  DataProvider_Source = mSource
End Property


Public Property Let DataProvider_Column(ByVal vData As String)
  mColumn = vData
  Set mRecordset = Nothing
End Property


Public Property Get DataProvider_Column() As String
  DataProvider_Column = mColumn
End Property

Public Property Let DataProvider_Filter(ByVal vData As String)
  mFilter = vData
  Set mRecordset = Nothing
End Property

Public Property Get DataProvider_Filter() As String
  DataProvider_Filter = mFilter
End Property


Public Property Let DataProvider_Sort(ByVal vData As String)
  mSort = vData
  Set mRecordset = Nothing
End Property


Public Property Get DataProvider_Sort() As String
  DataProvider_Sort = mSort
End Property


Public Property Get DataProvider_Params() As String
  DataProvider_Params = "display=text/xml"
  If mColumn = "" Then
    DataProvider_Params = DataProvider_Params & "&column=" & EncodeURL("*")
  Else
    DataProvider_Params = DataProvider_Params & "&column=" & EncodeURL(mColumn)
  End If
  
  'ver 2.00.0303 (jjm)
  Dim BaseNotNull, FilterNotNull As Boolean
  BaseNotNull = mBaseFilter <> vbNullString
  FilterNotNull = mFilter <> vbNullString
  If BaseNotNull Or FilterNotNull Then
    Dim tempFilter As String
    tempFilter = IIf(BaseNotNull, "(", vbNullString) & mBaseFilter & IIf(BaseNotNull, ") ", vbNullString) & _
      IIf(FilterNotNull, IIf(BaseNotNull, " and ", vbNullString) & mFilter, vbNullString)
    DataProvider_Params = DataProvider_Params & "&filter=" & EncodeURL(tempFilter)
  End If
  If Not mSort = "" Then
    DataProvider_Params = DataProvider_Params & "&sort= " & EncodeURL(mSort)
  End If
End Property


Private Sub DataProvider_OpenRecordset(Optional Count As Integer)
  'MsgBox "DataProvider_OpenRecordset: Select " & mColumn & " By " & mSort
  'Debug.Print "===================================="
  'Debug.Print mSource
  'Debug.Print mFilter
  'Debug.Print mColumn
  'Load Custom Setting for number of Items in a list view
  If Not Count > 0 Then
    Count = LocaleItemsPerView
  End If
  
  Dim xmlHttp As New xmlHttp
  Dim url As String
  url = mConnection & "search/" & mSource
  
  Set xmlHttp = XMLUtil.HttpPost(url, _
      DataProvider_Params & IIf(Count > 0, "&count=" & Count, ""))
  
  'Browse mConnection & "search/" & mSource & "?" & DataProvider_Params & IIf(Count > 0, "&count=" & Count, "")
  'MsgBox DataProvider_Params
  'MsgBox xmlHttp.responseText
  
  If xmlHttp.responseXML.documentElement Is Nothing Then
    MsgBox "Invalid response from server: " & xmlHttp.responseText
    Set mRecordset = Nothing
    Exit Sub
  End If
  
  If Not xmlHttp.responseXML.documentElement.selectSingleNode("error") Is Nothing Then
    ErrorUtil.DisplayXMLHttpError xmlHttp, url
    Set mRecordset = Nothing
    Exit Sub
  End If
  
  Set mRecordset = xmlHttp.responseXML
  If mRecordset.parseError.srcText <> "" Then
    MsgBox mRecordset.parseError.srcText & " at line " & mRecordset.parseError.Line
    Set mRecordset = Nothing
  End If
  
End Sub

Public Property Set DataProvider_Recordset(ByVal vData As Variant)
  Set mRecordset = vData
End Property


Public Property Get DataProvider_Recordset() As Variant
  Set DataProvider_Recordset = mRecordset
End Property


Public Property Get DataProvider_Collection() As Collection
  If mRecordset Is Nothing Then Exit Property
  Set DataProvider_Collection = XMLUtil.Rowset(mRecordset.documentElement, mSource)
End Property


Public Sub DataProvider_SetListView(View As Object, Optional isIcon As Boolean = True)
  Dim RowsetNode As MSXML2.IXMLDOMNode
  Dim xmlNode As MSXML2.IXMLDOMNode
  
  ' If tag is not set, then we create column headers below.
  If View.Tag = "" Then
    View.ColumnHeaders.Clear
  Else
'    If View.ColumnHeaders.Count = 0 Then
'      ListViewModule.DeserializeColumnHeaders View, View.Tag
'    End If
  End If
  View.ListItems.Clear
    
  ' Usually happens when the middle tier is down or a servlet has not been created for the source.
  If mRecordset Is Nothing Then
    MsgBox "Unable to view data.  DataProvider " & mSource & " was not opened successfully."
    Exit Sub
  End If
  
  Set RowsetNode = mRecordset.documentElement.selectSingleNode("data")
  If RowsetNode Is Nothing Then
    MsgBox "Failed to find /xml/data/" & mSource
    Exit Sub
  End If
  
  If Not RowsetNode.hasChildNodes Then
    Exit Sub
  End If
  
  'Default Header Setup, Need to reference the result/element node
  If View.ColumnHeaders.Count = 0 Then
    Dim ElementNode As MSXML2.IXMLDOMNode
    For Each xmlNode In mRecordset.documentElement.selectSingleNode("result/elements").childNodes
      View.ColumnHeaders.Add , xmlNode.NodeName, xmlNode.NodeName
    Next xmlNode
  End If
 
  ' Scan through nodes
  Dim Column As ColumnHeader
  Dim xmlColumnNode As MSXML2.IXMLDOMNode
  Dim xItem As ListItem
  Dim First As Boolean
  For Each xmlNode In RowsetNode.childNodes
    First = True
    For Each Column In View.ColumnHeaders
      Select Case Left(Column.Key, 1)
        Case ""
        Case "~"
          xItem.ListSubItems.Add , Column.Key
        Case Else
          'If there is a node for that field
          If XMLUtil.NodeContainsChildNode(xmlNode, Column.Key) Then
            Set xmlColumnNode = xmlNode.selectSingleNode(Column.Key)
            If First Then
              Set xItem = View.ListItems.Add(, , DisplayFormat(xmlColumnNode.NodeName, DataValue(xmlColumnNode.NodeName, xmlColumnNode.Text), Column.Tag, LocaleShortDate))
              xItem.Tag = xmlColumnNode.NodeName
              If isIcon Then
                xItem.Icon = "book"
                xItem.SmallIcon = "closed"
              End If
              First = False
            Else
              ' This can happen if the column key does not exist in the underlying dataset.
              xItem.ListSubItems.Add , xmlColumnNode.NodeName, DisplayFormat(xmlColumnNode.NodeName, DataValue(xmlColumnNode.NodeName, xmlColumnNode.Text), Column.Tag, LocaleShortDate)
            End If
          Else 'else if there is no data for the specified field
            If First Then
              Set xItem = View.ListItems.Add(, , vbNullString)
              xItem.Tag = Column.Key
              If isIcon Then
                xItem.Icon = "book"
                xItem.SmallIcon = "closed"
              End If
              First = False
            Else
              xItem.ListSubItems.Add , Column.Key
            End If
          End If
      End Select
    Next Column
  Next xmlNode
End Sub


Public Function DataProvider_Find(HandleName As String, Handle, Optional Column As String) As Collection
  Dim Rowset As Collection
  mSort = ""
  mColumn = Column
  mFilter = HandleName & " = '" & Handle & "'"
  DataProvider_OpenRecordset 1
  Set Rowset = DataProvider_Collection
  If Not Rowset Is Nothing Then
    Set DataProvider_Find = Rowset(1)
  End If
End Function


Public Function DataProvider_FindMulti(HandleNames() As String, Handles, Optional Column As String) As Collection
  mSort = vbNullString
  mColumn = mColumn
  
  Dim newFilter As New filter, I As Integer, Handle As Variant
  For I = 0 To SafeUBound(HandleNames)
    Handle = Handles(I)
    If VarType(Handle) = vbDate Then
      Handle = LawSQL.DateFormat(CDate(Handle))
    Else
      Handle = "'" & Handle & "'"
    End If
    newFilter.Add HandleNames(I) & " = " & Handle
  Next I
  
  mFilter = newFilter
  DataProvider_OpenRecordset 1
  
  Dim Rowset As Collection
  Set Rowset = DataProvider_Collection
  If Not Rowset Is Nothing Then
    Set DataProvider_FindMulti = Rowset(1)
  End If
End Function
