VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmOpenDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Open"
   ClientHeight    =   3348
   ClientLeft      =   36
   ClientTop       =   276
   ClientWidth     =   7500
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3348
   ScaleWidth      =   7500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4116
      Left            =   0
      ScaleHeight     =   4116
      ScaleWidth      =   7500
      TabIndex        =   0
      Top             =   0
      Width           =   7500
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   6168
         TabIndex        =   6
         Top             =   2856
         Width           =   1200
      End
      Begin VB.CommandButton cmdOpen 
         Caption         =   "Open"
         Default         =   -1  'True
         Height          =   336
         Left            =   4896
         TabIndex        =   5
         Top             =   2856
         Width           =   1200
      End
      Begin MSComctlLib.ListView lvView 
         Height          =   2124
         Left            =   108
         TabIndex        =   4
         Top             =   612
         Width           =   7284
         _ExtentX        =   12848
         _ExtentY        =   3747
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ImageList imlOpen 
         Left            =   0
         Top             =   1788
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin MSComctlLib.Toolbar tbrView 
         Height          =   264
         Left            =   4824
         TabIndex        =   3
         Top             =   180
         Width           =   648
         _ExtentX        =   1143
         _ExtentY        =   466
         ButtonWidth     =   487
         Style           =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "List"
                     Text            =   "List"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Details"
                     Text            =   "Details"
                  EndProperty
               EndProperty
            EndProperty
         EndProperty
      End
      Begin VB.ComboBox cboLookIn 
         Height          =   288
         Left            =   828
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   192
         Width           =   3840
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Look &in:"
         Height          =   192
         Left            =   132
         TabIndex        =   1
         Top             =   240
         Width           =   552
      End
   End
End
Attribute VB_Name = "frmOpenDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mHandle As String
Private mSource As String
Private mData As Dataset
Private mDataProvider As DataProvider
Private mOk As Boolean

Public Property Get OK() As Boolean
  OK = mOk
End Property

Public Function ShowOpen() As Boolean
  Show vbModal
  ShowOpen = mOk
End Function

Private Sub cmdCancel_Click()
  mOk = False
  Hide
End Sub

Private Sub cmdOpen_Click()
  mOk = True
  Hide
End Sub

Private Sub Form_Load()

  'Load the images
  imlOpen.ListImages.Add 1, "ListView", PicDisplay.GetPicture(UrlName.LISTVIEW_IMAGE)
  
  ' set the image list to the toolbar
  tbrView.ImageList = imlOpen
  tbrView.Buttons(1).Image = "ListView"
  

End Sub

Private Sub lvview_DblClick()
  mOk = True
  Hide
End Sub
