VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PolicyLawyerCoverageDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
'ver 2.00.0311 (jjm)
Option Explicit

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer


Private Sub Class_Initialize()
  ListIndex = 2
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.POLICYLAWYERCOVERAGEVIEW_TABLE '"PolicyLawyerCoverageView"
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property

Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  frm.cboIn.AddItem "in coverage identifier field only"
  frm.cboIn.AddItem "in lawyer identifier field only"
  frm.cboIn.AddItem "in lawyer entity locator field only"
  frm.cboIn.AddItem "in lawyer entity name field only"
  frm.cboIn.AddItem "in policy identifier field only"
  frm.cboIn.AddItem "in policy number field only"
  
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, 2)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection

  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Select Case ListIndex
      Case 0
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.COVERAGE_KEY, Words)
      Case 1
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.LAWYER_KEY, Words)
      Case 2
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LAWYERENTITYLOCATOR_FIELD, Words)
      Case 3
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.LAWYERENTITYNAME_FIELD, Words)
      Case 4
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.POLICY_KEY, Words)
      Case 5
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.POLICYNUMBER_FIELD, Words)
    End Select
  End If
  
  If TypeOf frm Is frmGenericSearch Then
    If Not frm.PolicyHandle = vbNullString Then
      newFilter.Add DatabaseName.POLICY_KEY & " = '" & frm.PolicyHandle & "'"
    End If
  End If
  
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

