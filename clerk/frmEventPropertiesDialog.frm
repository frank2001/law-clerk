VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmEventPropertiesEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Properties"
   ClientHeight    =   5760
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5970
   Icon            =   "frmEventPropertiesDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   5970
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDebug 
      Caption         =   "&Debug"
      Height          =   336
      Left            =   120
      TabIndex        =   31
      Top             =   5304
      Width           =   1200
   End
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   4644
      TabIndex        =   25
      Top             =   5304
      Width           =   1200
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5112
      Left            =   96
      TabIndex        =   26
      Top             =   60
      Width           =   5748
      _ExtentX        =   10134
      _ExtentY        =   9022
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   420
      TabCaption(0)   =   "&General"
      TabPicture(0)   =   "frmEventPropertiesDialog.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabel(1)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblLabel(2)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLabel(5)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblLocation"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblLabel(3)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblLabel(4)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblLabel(7)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblLabel(8)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblDataField(24)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblDataField(23)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblDataField(21)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblDataField(22)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblDataField(29)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblDataField(28)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblDataField(26)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblDataField(25)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblDataField(27)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "lblLabel(0)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "lblDataField(20)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "txtDataField(0)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "fraLine(0)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "fraLine(1)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "cboEventTypeName"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).ControlCount=   23
      TabCaption(1)   =   "N&ote"
      TabPicture(1)   =   "frmEventPropertiesDialog.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtNote"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Reference"
      TabPicture(2)   =   "frmEventPropertiesDialog.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdReferenceProperties"
      Tab(2).Control(1)=   "lvReference"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "&Advanced"
      TabPicture(3)   =   "frmEventPropertiesDialog.frx":0060
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lvAdvanced"
      Tab(3).ControlCount=   1
      Begin VB.ComboBox cboEventTypeName 
         Height          =   315
         Left            =   1392
         Style           =   2  'Dropdown List
         TabIndex        =   30
         Top             =   1656
         Width           =   3696
      End
      Begin VB.CommandButton cmdReferenceProperties 
         Caption         =   "&Properties"
         Height          =   288
         Left            =   -70452
         TabIndex        =   28
         Top             =   492
         Width           =   1000
      End
      Begin VB.TextBox txtNote 
         DataField       =   "eventNote"
         Height          =   4500
         Left            =   -74895
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   6
         Top             =   480
         Width           =   5505
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   1
         Left            =   216
         TabIndex        =   4
         Top             =   2784
         Width           =   5076
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   0
         Left            =   156
         TabIndex        =   1
         Top             =   1260
         Width           =   5076
      End
      Begin VB.TextBox txtDataField 
         BackColor       =   &H8000000F&
         DataField       =   "eventName"
         Height          =   288
         Index           =   0
         Left            =   1032
         Locked          =   -1  'True
         TabIndex        =   0
         Text            =   "Text1"
         Top             =   804
         Width           =   4200
      End
      Begin MSComctlLib.ListView lvReference 
         Height          =   4500
         Left            =   -74895
         TabIndex        =   27
         Top             =   480
         Width           =   4305
         _ExtentX        =   7594
         _ExtentY        =   7938
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Type"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Name"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Source"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "eventId"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lvAdvanced 
         Height          =   4500
         Left            =   -74895
         TabIndex        =   29
         Top             =   480
         Width           =   5505
         _ExtentX        =   9710
         _ExtentY        =   7938
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Name"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Value"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "created"
         DataField       =   "createDate"
         Height          =   192
         Index           =   20
         Left            =   1404
         TabIndex        =   22
         Top             =   3096
         UseMnemonic     =   0   'False
         Width           =   552
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Created:"
         Height          =   192
         Index           =   0
         Left            =   216
         TabIndex        =   21
         Top             =   3096
         Width           =   612
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "due"
         DataField       =   "dueUserId"
         Height          =   192
         Index           =   27
         Left            =   3456
         TabIndex        =   20
         Top             =   3840
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   276
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "created"
         DataField       =   "createUserId"
         Height          =   192
         Index           =   25
         Left            =   3456
         TabIndex        =   19
         Top             =   3096
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   552
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "modified"
         DataField       =   "modifyUserId"
         Height          =   192
         Index           =   26
         Left            =   3456
         TabIndex        =   18
         Top             =   3468
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   624
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "complete"
         DataField       =   "completeUserId"
         Height          =   192
         Index           =   28
         Left            =   3456
         TabIndex        =   17
         Top             =   4200
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   672
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "void"
         DataField       =   "voidUserId"
         Height          =   192
         Index           =   29
         Left            =   3456
         TabIndex        =   16
         Top             =   4572
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   312
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "due"
         DataField       =   "dueDate"
         Height          =   192
         Index           =   22
         Left            =   1404
         TabIndex        =   15
         Top             =   3840
         UseMnemonic     =   0   'False
         Width           =   276
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "modified"
         DataField       =   "modifyDate"
         Height          =   192
         Index           =   21
         Left            =   1404
         TabIndex        =   14
         Top             =   3468
         UseMnemonic     =   0   'False
         Width           =   624
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "complete"
         DataField       =   "completeDate"
         Height          =   192
         Index           =   23
         Left            =   1404
         TabIndex        =   13
         Top             =   4200
         UseMnemonic     =   0   'False
         Width           =   672
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "void"
         DataField       =   "voidDate"
         Height          =   192
         Index           =   24
         Left            =   1404
         TabIndex        =   12
         Top             =   4572
         UseMnemonic     =   0   'False
         Width           =   312
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Voided:"
         Height          =   192
         Index           =   8
         Left            =   216
         TabIndex        =   11
         Top             =   4572
         Width           =   564
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Completed:"
         Height          =   192
         Index           =   7
         Left            =   216
         TabIndex        =   10
         Top             =   4203
         Width           =   828
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Due:"
         Height          =   192
         Index           =   4
         Left            =   216
         TabIndex        =   9
         Top             =   3834
         Width           =   336
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Modified:"
         Height          =   192
         Index           =   3
         Left            =   216
         TabIndex        =   8
         Top             =   3465
         Width           =   660
      End
      Begin VB.Label lblLocation 
         AutoSize        =   -1  'True
         Caption         =   "location"
         Height          =   168
         Left            =   1392
         TabIndex        =   7
         Top             =   2088
         UseMnemonic     =   0   'False
         Width           =   3840
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Name:"
         Height          =   192
         Index           =   5
         Left            =   240
         TabIndex        =   5
         Top             =   852
         Width           =   480
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Type of event:"
         Height          =   192
         Index           =   2
         Left            =   216
         TabIndex        =   3
         Top             =   1692
         Width           =   1020
      End
      Begin VB.Label lblLabel 
         Caption         =   "Location:"
         Height          =   288
         Index           =   1
         Left            =   216
         TabIndex        =   2
         Top             =   2088
         Width           =   864
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   3348
      TabIndex        =   24
      Top             =   5304
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   2040
      TabIndex        =   23
      Top             =   5304
      Width           =   1200
   End
End
Attribute VB_Name = "frmEventPropertiesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mEventHandle As String
Private mEventNote As String
Private mEventTypeHandle As String
Private mEventTypeRuleId As String
Private mEventTypes As Collection
Private mDataField As Collection
Private mDirty As Boolean
Private mNoteDirty As Boolean

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mNoteDirty And Verify Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  If mNoteDirty Then    'Save if changes are made to notepad
    Factory.EventService.SetNote mEventHandle, txtNote.Text
    mNoteDirty = False
  End If

  If mDirty Then
    Factory.EventService.SetEvent mEventHandle, mEventTypes(cboEventTypeName.ListIndex + 1) '(DatabaseName.EVENTTYPE_KEY)
    mDirty = False
  End If
  
  save = Not (mDirty And mNoteDirty)

End Function

Public Sub ShowEvent(DataField As Collection)
  Set mDataField = DataField
  mEventHandle = mDataField(DatabaseName.EVENT_KEY)
  Show
End Sub

Private Sub cboEventTypeName_Click()
  mDirty = True
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdDebug_Click()
  Dispatcher.BrowseEventOpen mEventHandle
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub cmdOk_Click()
  If save Then
    Unload Me
  End If
End Sub

Private Sub cmdReferenceProperties_Click()
    
  With lvReference.SelectedItem
    FormUtil.ShowEventProperties .ListSubItems(3)
  End With
End Sub




Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  Caption = mDataField(DatabaseName.EVENTNAME_FIELD) & " Event Properties"
 
  Dim fmt As New StdDataFormat, I As Integer
  fmt.Format = LocaleLongDate & ", " & LocaleTimeFormat
  For I = 20 To 24
    Set lblDataField(I).DataFormat = fmt
  Next I
  
  SetFields txtDataField, mDataField
  SetFields lblDataField, mDataField
  
  ' Set various date/user fields
  For I = 20 To 24
    lblDataField(I).Caption = lblDataField(I).Caption & _
      IIf(lblDataField(I + 5).Caption <> vbNullString, " (" & lblDataField(I + 5) & ")", "")
  Next I
  
  lblLocation.Caption = Factory.PreferenceService.GetPath(SystemFolders_TreeName, mDataField(DatabaseName.FOLDER_KEY))
  mEventTypeRuleId = GetDataFromCollection(mDataField, DatabaseName.EVENTTYPERULEID_FIELD)
  mEventTypeHandle = GetDataFromCollection(mDataField, DatabaseName.EVENTTYPE_KEY)
  If (mEventTypeHandle <> vbNullString) Then
    Dim EventType As Collection
    Set EventType = Factory.EventTypeService.Find(mEventTypeHandle)
    
    Dim EventTypeName As String
    EventTypeName = GetDataFromCollection(EventType, DatabaseName.EVENTTYPENAME_FIELD)
    'Load the combo box of Event Types based on the evenTypeRuleId
    'If not grouping is found, only load the combo box with the EventType of the event.
    If mEventTypeRuleId = vbNullString Then
      cboEventTypeName.AddItem EventTypeName
      cboEventTypeName.ListIndex = 0
      Set mEventTypes = New Collection
      mEventTypes.Add EventTypeName
    Else
      Set mEventTypes = Factory.EventTypeService.GroupCollection(mEventTypeRuleId)
      FormUtil.ComboBoxLoad mEventTypes, cboEventTypeName, DatabaseName.EVENTTYPENAME_FIELD
      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, mEventTypeHandle) - 1
    End If
  End If
  
  
  lvReference.ListItems.Clear
  Dim references, Row As Collection
  Set references = Factory.ReferenceService.FindDownReferences(mEventHandle, , DatabaseName.SOURCETYPE_KEY)
  If Not references Is Nothing Then
    For Each Row In references
      With lvReference.ListItems.Add(, , GetDataFromCollection(Row, DatabaseName.EVENTTYPE_KEY))
        .ListSubItems.Add , , GetDataFromCollection(Row, DatabaseName.EVENTTYPENAME_FIELD)
        .ListSubItems.Add , , GetDataFromCollection(Row, DatabaseName.SOURCETYPE_KEY)
        .ListSubItems.Add , , GetDataFromCollection(Row, DatabaseName.EVENT_KEY)
      End With
    Next Row
  End If
  Set references = Factory.ReferenceService.FindUpReferences(mEventHandle, , DatabaseName.REFERENCESOURCETYPE_KEY)
  If Not references Is Nothing Then
    For Each Row In references
      With lvReference.ListItems.Add(, , GetDataFromCollection(Row, DatabaseName.REFERENCEEVENTTYPEID_FIELD))
        .ListSubItems.Add , , GetDataFromCollection(Row, DatabaseName.REFERENCEEVENTTYPENAME_FIELD)
        .ListSubItems.Add , , GetDataFromCollection(Row, DatabaseName.REFERENCESOURCETYPE_KEY)
        .ListSubItems.Add , , GetDataFromCollection(Row, DatabaseName.REFERENCEEVENTID_FIELD)
      End With
    Next Row
  End If
  
  Dim field As DataField
  lvAdvanced.ListItems.Clear
  lvAdvanced.ListItems.Add , , "--Column--"
  For Each field In mDataField
    With lvAdvanced.ListItems.Add(, field.Name, field.Name)
      .ListSubItems.Add , , field.Value
    End With
  Next field
    
  ListViewModule.AutosizeWidth lvAdvanced
  
  TabControlEnable SSTab1
  mDirty = False
  mNoteDirty = False
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub lvReference_DblClick()
  cmdReferenceProperties_Click
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  TabControlEnable SSTab1
  Select Case SSTab1.Tab
    Case 2
      cmdReferenceProperties.Enabled = IIf(lvReference.ListItems.Count = 0, False, True)
  End Select
End Sub

Private Sub txtNote_Change()
  mNoteDirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdOK
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdOK
End Sub
