VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEventDocumentEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Event Document"
   ClientHeight    =   6588
   ClientLeft      =   120
   ClientTop       =   696
   ClientWidth     =   8364
   Icon            =   "frmEventContactEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   Picture         =   "frmEventContactEdit.frx":000C
   ScaleHeight     =   6588
   ScaleWidth      =   8364
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6168
      Left            =   0
      ScaleHeight     =   6168
      ScaleWidth      =   8364
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   384
      Width           =   8364
      Begin VB.TextBox txtDataField 
         DataField       =   "eventName"
         DataMember      =   "Event"
         Height          =   288
         Index           =   1
         Left            =   1224
         TabIndex        =   86
         Top             =   108
         Width           =   3960
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   7080
         TabIndex        =   84
         Top             =   5748
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   336
         Left            =   5748
         TabIndex        =   83
         Top             =   5748
         Width           =   1236
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   5184
         Left            =   60
         TabIndex        =   87
         Top             =   456
         Width           =   8232
         _ExtentX        =   14520
         _ExtentY        =   9165
         _Version        =   393216
         Style           =   1
         TabsPerRow      =   4
         TabHeight       =   420
         TabCaption(0)   =   "&Document"
         TabPicture(0)   =   "frmEventContactEdit.frx":0C4E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblDataField(46)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblDataField(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblDataField(45)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblDataField(47)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblDataField(48)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblDataField(42)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblDataField(41)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblDataField(43)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblDataField(44)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel(15)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel(16)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "dtpDataField(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtDataField(14)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "cboEventTypeName"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "fraMerge"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "cboDataField(0)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "&Recipient"
         TabPicture(1)   =   "frmEventContactEdit.frx":0C6A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "cmdMoveDown"
         Tab(1).Control(1)=   "cmdMoveUp"
         Tab(1).Control(2)=   "cboType"
         Tab(1).Control(3)=   "cmdNew"
         Tab(1).Control(4)=   "fraLocation"
         Tab(1).Control(5)=   "fraPerson"
         Tab(1).Control(6)=   "cmdRemove"
         Tab(1).Control(7)=   "cmdAdd"
         Tab(1).Control(8)=   "lvEventRecipient"
         Tab(1).ControlCount=   9
         TabCaption(2)   =   "N&ote"
         TabPicture(2)   =   "frmEventContactEdit.frx":0C86
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "cmdClearNotepad"
         Tab(2).Control(1)=   "txtDataField(16)"
         Tab(2).ControlCount=   2
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74880
            TabIndex        =   31
            Top             =   4776
            Width           =   1028
         End
         Begin VB.ComboBox cboDataField 
            DataField       =   "eventTypeId"
            DataMember      =   "Event"
            Height          =   288
            Index           =   0
            Left            =   1200
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   936
            Width           =   1344
         End
         Begin VB.CommandButton cmdMoveDown 
            Caption         =   "Move Do&wn"
            Height          =   288
            Left            =   -70392
            TabIndex        =   13
            Top             =   1596
            Width           =   1028
         End
         Begin VB.CommandButton cmdMoveUp 
            Caption         =   "Move &Up"
            Height          =   288
            Left            =   -69264
            TabIndex        =   14
            Top             =   1596
            Width           =   1028
         End
         Begin VB.ComboBox cboType 
            DataField       =   "recipientTypeId"
            Height          =   288
            ItemData        =   "frmEventContactEdit.frx":0CA2
            Left            =   -74844
            List            =   "frmEventContactEdit.frx":0CAF
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   1596
            Width           =   1128
         End
         Begin VB.Frame fraMerge 
            Caption         =   "Merge"
            Height          =   1596
            Left            =   96
            TabIndex        =   53
            Top             =   1848
            Width           =   6456
            Begin VB.TextBox txtDataField 
               DataField       =   "saveFileName"
               DataMember      =   "Event"
               Height          =   288
               Index           =   18
               Left            =   1848
               TabIndex        =   7
               Top             =   1116
               Width           =   4152
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "fileName"
               DataMember      =   "Event"
               Height          =   288
               Index           =   15
               Left            =   1152
               TabIndex        =   4
               Top             =   288
               Width           =   4848
            End
            Begin VB.CheckBox chkEnvelopeAll 
               Caption         =   "Envelope (all recipients)"
               DataField       =   "isEnvelope"
               DataMember      =   "Event"
               Height          =   336
               Left            =   3144
               TabIndex        =   6
               Top             =   732
               Width           =   2352
            End
            Begin VB.CheckBox chkDataField 
               Caption         =   "Modify"
               DataField       =   "isModify"
               DataMember      =   "Event"
               Height          =   336
               Index           =   0
               Left            =   2088
               TabIndex        =   5
               Top             =   744
               Width           =   900
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Save modifications to:"
               Height          =   192
               Index           =   8
               Left            =   108
               TabIndex        =   56
               Top             =   1140
               Width           =   1572
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "File name:"
               Height          =   192
               Index           =   7
               Left            =   108
               TabIndex        =   55
               Top             =   312
               Width           =   744
            End
         End
         Begin VB.ComboBox cboEventTypeName 
            Height          =   288
            Left            =   2580
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   936
            Width           =   3408
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventDesc"
            DataMember      =   "Event"
            Height          =   288
            Index           =   14
            Left            =   1212
            TabIndex        =   3
            Top             =   1296
            Width           =   4764
         End
         Begin VB.CommandButton cmdNew 
            Caption         =   "&New"
            Height          =   288
            Left            =   -68124
            TabIndex        =   11
            Top             =   1104
            Width           =   1028
         End
         Begin VB.Frame fraLocation 
            Caption         =   "Location"
            Height          =   2388
            Left            =   -74868
            TabIndex        =   42
            Top             =   2664
            Width           =   7908
            Begin VB.CheckBox chkEnvelope 
               Caption         =   "Envelope"
               DataField       =   "isEnvelope"
               DataMember      =   "EventRecipient"
               Height          =   300
               Left            =   5316
               TabIndex        =   27
               Top             =   564
               Width           =   1080
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "deliveryPointCode"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   6
               Left            =   4668
               TabIndex        =   26
               Top             =   1944
               Width           =   468
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "country"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   5
               Left            =   1056
               TabIndex        =   25
               Top             =   1920
               Width           =   1320
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "deliveryAddress"
               DataMember      =   "EventRecipient"
               Height          =   1308
               Index           =   2
               Left            =   5316
               MultiLine       =   -1  'True
               TabIndex        =   28
               Top             =   924
               Width           =   2472
            End
            Begin VB.CommandButton cmdLocation 
               Caption         =   "&Location"
               Height          =   288
               Left            =   6780
               TabIndex        =   29
               Top             =   168
               Width           =   1028
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "companyName"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   7
               Left            =   1056
               TabIndex        =   18
               Top             =   264
               Width           =   4104
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "addressLine1"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   8
               Left            =   1056
               TabIndex        =   19
               Top             =   588
               Width           =   4104
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "state"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   12
               Left            =   2844
               TabIndex        =   23
               Top             =   1584
               Width           =   492
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "zip"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   13
               Left            =   3816
               TabIndex        =   24
               Top             =   1572
               Width           =   1320
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "city"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   11
               Left            =   1056
               TabIndex        =   22
               Top             =   1584
               Width           =   1212
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "addressLine3"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   10
               Left            =   1056
               TabIndex        =   21
               Top             =   1248
               Width           =   4104
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "addressLine2"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   9
               Left            =   1056
               TabIndex        =   20
               Top             =   924
               Width           =   4104
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Delivery Point:"
               Height          =   192
               Index           =   3
               Left            =   3492
               TabIndex        =   49
               Top             =   1980
               Width           =   1032
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Country:"
               Height          =   192
               Index           =   4
               Left            =   120
               TabIndex        =   48
               Top             =   1956
               Width           =   576
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Company:"
               Height          =   192
               Left            =   120
               TabIndex        =   47
               Top             =   276
               Width           =   732
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Address:"
               Height          =   192
               Left            =   120
               TabIndex        =   46
               Top             =   660
               Width           =   648
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "City:"
               Height          =   192
               Index           =   1
               Left            =   120
               TabIndex        =   45
               Top             =   1632
               Width           =   300
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               Caption         =   "State:"
               Height          =   192
               Left            =   2352
               TabIndex        =   44
               Top             =   1632
               Width           =   408
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "Zip:"
               Height          =   192
               Left            =   3468
               TabIndex        =   43
               Top             =   1620
               Width           =   264
            End
         End
         Begin VB.Frame fraPerson 
            Caption         =   "Person"
            Height          =   624
            Left            =   -74856
            TabIndex        =   39
            Top             =   1980
            Width           =   7872
            Begin VB.CommandButton cmdPerson 
               Caption         =   "&Person"
               Height          =   288
               Left            =   6744
               TabIndex        =   17
               Top             =   216
               Width           =   1028
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "contactName"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   3
               Left            =   852
               TabIndex        =   15
               Top             =   216
               Width           =   2292
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "contactSalutation"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   4
               Left            =   4236
               TabIndex        =   16
               Top             =   216
               Width           =   1896
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Attention:"
               Height          =   192
               Index           =   1
               Left            =   108
               TabIndex        =   41
               Top             =   264
               Width           =   648
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Salutation:"
               Height          =   192
               Index           =   2
               Left            =   3324
               TabIndex        =   40
               Top             =   264
               Width           =   744
            End
         End
         Begin VB.CommandButton cmdRemove 
            Caption         =   "Re&move"
            Height          =   288
            Left            =   -68124
            TabIndex        =   10
            Top             =   756
            Width           =   1028
         End
         Begin VB.CommandButton cmdAdd 
            Caption         =   "&Add"
            Height          =   288
            Left            =   -68124
            TabIndex        =   9
            Top             =   408
            Width           =   1028
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventNote"
            DataMember      =   "Event"
            Height          =   4296
            Index           =   16
            Left            =   -74880
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   30
            Top             =   396
            Width           =   7956
         End
         Begin MSComctlLib.ListView lvEventRecipient 
            Height          =   1140
            Left            =   -74856
            TabIndex        =   8
            Tag             =   "EventEventContact"
            Top             =   384
            Width           =   6624
            _ExtentX        =   11684
            _ExtentY        =   2011
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmEventContactEdit.frx":0CC0
            DataField       =   "createDate"
            DataMember      =   "Event"
            Height          =   288
            Index           =   0
            Left            =   1212
            TabIndex        =   0
            Top             =   588
            Width           =   1332
            _ExtentX        =   2350
            _ExtentY        =   508
            _Version        =   393216
            Format          =   43515905
            CurrentDate     =   37336
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Modified:"
            Height          =   192
            Index           =   16
            Left            =   132
            TabIndex        =   82
            Top             =   3672
            Width           =   660
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Due:"
            Height          =   192
            Index           =   15
            Left            =   132
            TabIndex        =   81
            Top             =   4044
            Width           =   336
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Completed:"
            Height          =   192
            Index           =   14
            Left            =   132
            TabIndex        =   80
            Top             =   4404
            Width           =   828
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Voided:"
            Height          =   192
            Index           =   13
            Left            =   132
            TabIndex        =   79
            Top             =   4776
            Width           =   564
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "void"
            DataField       =   "voidDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   44
            Left            =   1320
            TabIndex        =   78
            Top             =   4776
            UseMnemonic     =   0   'False
            Width           =   312
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "complete"
            DataField       =   "completeDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   43
            Left            =   1320
            TabIndex        =   77
            Top             =   4404
            UseMnemonic     =   0   'False
            Width           =   672
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "modified"
            DataField       =   "modifyDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   41
            Left            =   1332
            TabIndex        =   76
            Top             =   3672
            UseMnemonic     =   0   'False
            Width           =   624
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "due"
            DataField       =   "dueDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   42
            Left            =   1320
            TabIndex        =   75
            Top             =   4044
            UseMnemonic     =   0   'False
            Width           =   276
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "void"
            DataField       =   "voidUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   48
            Left            =   3372
            TabIndex        =   74
            Top             =   4776
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   312
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "complete"
            DataField       =   "completeUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   47
            Left            =   3372
            TabIndex        =   73
            Top             =   4404
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   672
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "modified"
            DataField       =   "modifyUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   45
            Left            =   3372
            TabIndex        =   72
            Top             =   3672
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   624
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "created"
            DataField       =   "createUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   1
            Left            =   2724
            TabIndex        =   71
            Top             =   636
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   552
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "due"
            DataField       =   "dueUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   46
            Left            =   3372
            TabIndex        =   70
            Top             =   4044
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   276
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Type:"
            Height          =   192
            Index           =   2
            Left            =   156
            TabIndex        =   52
            Top             =   984
            Width           =   420
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Date:"
            Height          =   192
            Index           =   0
            Left            =   144
            TabIndex        =   51
            Top             =   624
            Width           =   384
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Description:"
            Height          =   192
            Index           =   6
            Left            =   144
            TabIndex        =   50
            Top             =   1308
            Width           =   936
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   420
         Top             =   4440
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   984
         Top             =   4440
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.Label lblSource 
         Caption         =   "Label4"
         Height          =   228
         Left            =   168
         TabIndex        =   85
         Top             =   156
         Width           =   780
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "cmdEnter"
      Height          =   336
      Left            =   3816
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   684
      Width           =   1308
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   36
      Top             =   0
      Width           =   8364
      _ExtentX        =   14753
      _ExtentY        =   677
      BandCount       =   1
      _CBWidth        =   8364
      _CBHeight       =   384
      _Version        =   "6.7.8988"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   336
         ScaleWidth      =   8268
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   24
         Width           =   8268
         Begin VB.TextBox txtDataField 
            DataField       =   "folderId"
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            DataMember      =   "Event"
            Enabled         =   0   'False
            Height          =   288
            Index           =   17
            Left            =   6792
            TabIndex        =   54
            TabStop         =   0   'False
            Top             =   24
            Visible         =   0   'False
            Width           =   936
         End
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   38
            Top             =   60
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Modified:"
      Height          =   192
      Index           =   12
      Left            =   0
      TabIndex        =   69
      Top             =   369
      Width           =   660
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Due:"
      Height          =   192
      Index           =   11
      Left            =   0
      TabIndex        =   68
      Top             =   738
      Width           =   336
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Completed:"
      Height          =   192
      Index           =   10
      Left            =   0
      TabIndex        =   67
      Top             =   1107
      Width           =   828
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Voided:"
      Height          =   192
      Index           =   9
      Left            =   0
      TabIndex        =   66
      Top             =   1476
      Width           =   564
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "void"
      DataField       =   "voidDate"
      Height          =   192
      Index           =   24
      Left            =   1188
      TabIndex        =   65
      Top             =   1476
      UseMnemonic     =   0   'False
      Width           =   312
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "complete"
      DataField       =   "completeDate"
      Height          =   192
      Index           =   23
      Left            =   1188
      TabIndex        =   64
      Top             =   1104
      UseMnemonic     =   0   'False
      Width           =   672
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "modified"
      DataField       =   "modifyDate"
      Height          =   192
      Index           =   21
      Left            =   1188
      TabIndex        =   63
      Top             =   372
      UseMnemonic     =   0   'False
      Width           =   624
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "due"
      DataField       =   "dueDate"
      Height          =   192
      Index           =   22
      Left            =   1188
      TabIndex        =   62
      Top             =   744
      UseMnemonic     =   0   'False
      Width           =   276
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "void"
      DataField       =   "voidUserId"
      Height          =   192
      Index           =   29
      Left            =   3240
      TabIndex        =   61
      Top             =   1476
      UseMnemonic     =   0   'False
      Visible         =   0   'False
      Width           =   312
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "complete"
      DataField       =   "completeUserId"
      Height          =   192
      Index           =   28
      Left            =   3240
      TabIndex        =   60
      Top             =   1104
      UseMnemonic     =   0   'False
      Visible         =   0   'False
      Width           =   672
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "modified"
      DataField       =   "modifyUserId"
      Height          =   192
      Index           =   26
      Left            =   3240
      TabIndex        =   59
      Top             =   372
      UseMnemonic     =   0   'False
      Visible         =   0   'False
      Width           =   624
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "created"
      DataField       =   "createUserId"
      Height          =   192
      Index           =   25
      Left            =   3240
      TabIndex        =   58
      Top             =   0
      UseMnemonic     =   0   'False
      Visible         =   0   'False
      Width           =   552
   End
   Begin VB.Label lblDataField 
      AutoSize        =   -1  'True
      Caption         =   "due"
      DataField       =   "dueUserId"
      Height          =   192
      Index           =   27
      Left            =   3240
      TabIndex        =   57
      Top             =   744
      UseMnemonic     =   0   'False
      Visible         =   0   'False
      Width           =   276
   End
   Begin VB.Label Label14 
      Caption         =   "Label14"
      Height          =   12
      Left            =   3252
      TabIndex        =   34
      Top             =   3096
      Width           =   972
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Event"
            Shortcut        =   ^N
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "Propert&ies"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator693 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings"
      End
      Begin VB.Menu mnuSeparator38121 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsEnvelope 
         Caption         =   "&Envelope..."
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuToolsResetFileName 
         Caption         =   "&Reset File Name"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator632 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmEventDocumentEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ver 1.90.0218 (jjm)
'
' frmUnderwritingContact
'
' This form provides the user interface to the ContactFirmDataset class.
'

'
' Future Development
'
' -Options Form that has Options for a Save and Add command button.

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

' Main Handles
Private mEventHandle As String
Private mFolderHandle As String

Private mSource As String
Private mEventGroupHandle As String
Private mSourceHandle As String
Private mEventName As String

Private mNew As Boolean

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mLabelDirty As Boolean
Private mInternal As Boolean
Private mEventTypeHandle As String

' Contact dataset is used to get the underlying contact record
Private mEventRecipientDataset As Dataset
Private mEventRecipientCollection As Collection
Private mEventRecipientRemove As Collection

'Used for populating the combo box
Private mEventTypes As Collection

Private SaveToPath As String
Private ApplicationFolder As String

' Form Manager
Private FormMgr As New FormManager

' Signal that a refresh is being called.
Private mRefresh As Boolean

' Sort Index for list item
Private SortIndex As Integer

' ver 1.90.0218 (jjm)
' Store a local variable to hold the value of chkEnvelope.Enabled
' This value is used to determine insertion of date into
' lvEventRecipient. This value is unreliable due to the form being
' passed to formutil.TabControlEnable
Private mEnvelopeEnabled As Boolean

Public Property Get EventHandle() As String
  EventHandle = mEventHandle
End Property
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Show
  Refresh
  Select Case fInterface.Message
    Case fimNew
      'Code not to get caught up in some other forms unload
      Select Case fInterface.Key
        Case DatabaseName.CONTACT_TABLE
          FormUtil.ShowNew New frmContactEdit, FormMgr, , DatabaseName.CONTACT_TABLE & "NewSelect"
        Case DatabaseName.PERSON_TABLE
          FormUtil.ShowNew New frmPersonEdit, FormMgr, , DatabaseName.PERSON_TABLE & "NewSelect"
        Case DatabaseName.LOCATION_TABLE
          FormUtil.ShowNew New frmLocationEdit, FormMgr, , DatabaseName.LOCATION_TABLE & "NewSelect"
      End Select
      
    Case fimSelect
      Select Case fInterface.Key
        Case DatabaseName.CONTACT_TABLE
          EventRecipientAddContact ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.CONTACT_KEY)
        Case DatabaseName.LOCATION_TABLE
          EventRecipientLoadLocation ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
        Case DatabaseName.PERSON_TABLE
          EventRecipientAddPerson ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.PERSON_KEY)
      End Select
      
    Case fimSave
      Select Case fInterface.Key
        'code for new person, location and contact
        Case DatabaseName.CONTACT_TABLE & "NewSelect"
          EventRecipientAddContact fInterface.Child.ContactHandle
        Case DatabaseName.PERSON_TABLE & "NewSelect"
          EventRecipientAddPerson fInterface.Child.PersonHandle
        Case DatabaseName.LOCATION_TABLE & "NewSelect"
          EventRecipientLoadLocation fInterface.Child.LocationHandle
      End Select
  End Select
  
  ButtonRefresh

End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
End Sub


Public Sub FormRefresh()
  Dim Pointer As New WaitPointer
  Dim mDatafields As New Collection
     
  Pointer.Pointer = vbHourglass
  
  ' Load all data fields with nothing
  ClearDataField txtDataField
  
  If (mNew) Then
    Caption = "New Document"
    txtDataField(1).Text = mEventName
    If txtDataField(17).Text = vbNullString Then
      txtDataField(17).Text = mFolderHandle
    End If
    ClearDataField lblDataField, "Event"
    dtpDataField(0).Value = Today
  Else
    Set mDatafields = Factory.EventService.Find(mEventHandle)
    Caption = "Edit Document (" & mEventHandle & ")"
    
    SetFields txtDataField, mDatafields, "Event"
    SetFields dtpDataField, mDatafields, "Event"
    SetFields chkDatafield, mDatafields, "Event"
        
    Dim fmt As New StdDataFormat, I As Integer
    fmt.Format = LocaleLongDate & ", " & LocaleTimeFormat
  
    For I = 41 To 44
      Set lblDataField(I).DataFormat = fmt
    Next I
    
    SetFields lblDataField, mDatafields, "Event"
    
    For I = 41 To 44
      lblDataField(I).Caption = lblDataField(I).Caption & _
        IIf(lblDataField(I + 4).Caption <> vbNullString, " (" & lblDataField(I + 4) & ")", "")
    Next I
    
    ' Hack ToDo: Find a way, other than using source group Id, to find out what
    ' Document type it is. Ex, ClaimDocument, Document, FirmDocument. Anything
    ' that has a eventGroupId ofClaimDocumet or FirmDocument also shares an
    ' eventGroupId of Document.
    mEventGroupHandle = mDatafields("sourceGroupId")
             
    ' Get Detail Info
    EventRecipientRefresh
    EventRecipientResync
  End If
  
  ' Setup mSource
  Select Case mEventGroupHandle
    Case DatabaseName.ENDORSEMENT_TABLE
      mSource = DatabaseName.ENDORSEMENT_TABLE
    Case DatabaseName.FIRM_DOCUMENT
      mSource = DatabaseName.FIRM_TABLE
    Case DatabaseName.CLAIM_DOCUMENT
      mSource = DatabaseName.CLAIM_TABLE
    Case DatabaseName.INCIDENT_DOCUMENT
      mSource = DatabaseName.INCIDENT_TABLE
    Case DatabaseName.DOCUMENT
      mSource = DatabaseName.PERSON_TABLE
    Case Else
      mSource = vbNullString
  End Select
  
  If Not mNew And Not mSource = vbNullString Then
    mSourceHandle = Factory.ReferenceService.ReferenceId1(mEventHandle, mSource)
  End If

  
  Select Case mSource
    Case DatabaseName.FIRM_TABLE
      lblSource.Caption = "Firm name:"
    Case DatabaseName.CLAIM_TABLE
      lblSource.Caption = "Claim name:"
    Case Else
      lblSource.Caption = "Name:"
  End Select
  
  'load combo boxes
  Set mEventTypes = Factory.EventTypeService.GroupCollection(mEventGroupHandle)
  If Not mEventTypes Is Nothing Then
    For I = 1 To mEventTypes.Count
      cboEventTypeName.AddItem mEventTypes.item(I)("eventTypeName")
      cboDataField(0).AddItem mEventTypes(I)(DatabaseName.EVENTTYPE_KEY)
    Next I
  Else
    MsgBox "Error loading combobox with """ & mEventGroupHandle & """ event group types.", vbExclamation
  End If

  If (mNew) Then
    FormUtil.GetComboDefault cboDataField(0)
  Else
    SetFields cboDataField, mDatafields, "Event"
  End If

  
  Set mEventRecipientRemove = New Collection
 
  ' Refresh navigation controls
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  lblLabel(8).Visible = (chkDatafield(0).Value = 1)
  txtDataField(18).Visible = (chkDatafield(0).Value = 1)
  
  txtDataField_Validate 10, False
  txtDataField_Change 0
  ButtonRefresh
  
  
  Dirty = False
End Sub
 
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  mEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
End Property

Public Function EnvelopeAllValue() As Integer
  Dim EnvAll As Integer, item As ListItem
  EnvAll = -1
  For Each item In lvEventRecipient.ListItems
    If ListItemText(item, "isEnvelope") = "Yes" Then
      If EnvAll = vbUnchecked Then
        EnvelopeAllValue = vbGrayed
        Exit Function
      End If
      EnvAll = vbChecked
    Else
      If EnvAll = vbChecked Then
        EnvelopeAllValue = vbGrayed
        Exit Function
      End If
      EnvAll = vbUnchecked
    End If
  Next item
  
  EnvelopeAllValue = IIf(EnvAll = -1, vbUnchecked, EnvAll)
End Function

Private Sub EventRecipientAddContact(ContactHandle As String)
  'Contact and Location collections are used to get information about a new contact
  Dim ContactCollection As Collection
  Dim LocationCollection As Collection
  Dim mLocationHandle As String

  Set ContactCollection = Factory.ContactService.Find(ContactHandle)
  mLocationHandle = ContactCollection(DatabaseName.LOCATION_KEY)
  Set LocationCollection = Factory.LocationService.Find(mLocationHandle)
  
  ' Get a new item and set
  Dim item As ListItem
  Set item = EventRecipientNew
  ListViewModule.SetItemCollection item, ContactCollection
  ListViewModule.SetItemCollection item, LocationCollection
  EventRecipientResync
  
  If mSourceHandle = vbNullString Then
    Select Case mSource
      Case DatabaseName.FIRM_TABLE
        mSourceHandle = ContactCollection(DatabaseName.FIRM_KEY)
        txtDataField(1).Text = Factory.FirmService.Find(mSourceHandle, """firmName""")("firmName")
      Case DatabaseName.CLAIM_TABLE
        mSourceHandle = ContactCollection(DatabaseName.CLAIM_KEY)
        txtDataField(1).Text = Factory.ClaimService.Find(mSourceHandle, """claimName""")("claimName")
      Case DatabaseName.INCIDENT_TABLE
        mSourceHandle = ContactCollection(DatabaseName.INCIDENT_KEY)
        txtDataField(1).Text = Factory.IncidentService.Find(mSourceHandle, """incidentName""")("incidentName")
      End Select
  End If
End Sub

Private Sub EventRecipientAddPerson(PersonHandle As String)
  Dim PersonCollection As Collection
  Dim LocationCollection As Collection
  Dim LocationHandle As String
  
  Set PersonCollection = Factory.PersonService.Find(PersonHandle)
  
  LocationHandle = PersonCollection(DatabaseName.LOCATION_KEY)
  Set LocationCollection = Factory.LocationService.Find(LocationHandle)
  
  Dim mItem As ListItem
  Set mItem = lvEventRecipient.SelectedItem
  
  'Set person data
  ListViewModule.SetItemCollection lvEventRecipient.SelectedItem, PersonCollection
  'Due to field name differences, the two following fields have to be manually added
  SetItemText mItem, "contactName", PersonCollection("displayName")
  SetItemText mItem, "contactSalutation", PersonCollection("salutation")
            
  'Set location data
  ListViewModule.SetItemCollection mItem, LocationCollection
  EventRecipientResync
End Sub

Private Sub EventRecipientLoadLocation(LocationHandle As String)
  ListViewModule.SetItemCollection lvEventRecipient.SelectedItem, Factory.LocationService.Find(LocationHandle)
  EventRecipientResync
End Sub

Private Function EventRecipientNew() As ListItem
  Dim item As ListItem
  Set item = ListViewModule.AddListItem(lvEventRecipient)
  item.Selected = True
  If chkEnvelopeAll Then
    item.ListSubItems("isEnvelope") = "Yes"
  End If
  SetItemText item, "~sort", "Z" & Format(SortIndex, "000")
  SortIndex = SortIndex + 1
  Set EventRecipientNew = item
End Function


'
' Refresh displayName and salutation from the Location table.
'
Private Sub EventRecipientRefresh()
  lvEventRecipient.Sorted = False
  
  mEventRecipientDataset.DataProvider.Filter = """eventId"" = '" & mEventHandle & "'"
  mEventRecipientDataset.DataProvider.Sort = """recipientOrder"""
  mEventRecipientDataset.DataProvider.OpenRecordset
  mEventRecipientDataset.DataProvider.SetListView lvEventRecipient, False
  
  SetListViewSortKey
  
  Set mEventRecipientCollection = mEventRecipientDataset.DataProvider.Collection
  If Not mEventRecipientCollection Is Nothing Then
    SetFields txtDataField, mEventRecipientCollection(1), "EventRecipient"
  End If
  lvEventRecipient.SortKey = 1
  lvEventRecipient.Sorted = True
  ButtonRefresh
End Sub


Private Sub EventRecipientResync()
  Dim Envelope As Boolean, FieldCollection As Collection
    
  Set FieldCollection = ListViewModule.ListItemCollection(lvEventRecipient.ColumnHeaders, lvEventRecipient.SelectedItem)
  SetFields txtDataField, FieldCollection, "EventRecipient"
    
  Envelope = FieldCollection("isEnvelope") <> vbNullString
  chkEnvelope.Value = IIf(Envelope, 1, 0)
  txtDataField(2).Enabled = Envelope
    
  If FieldCollection("recipientTypeId") = vbNullString Then
    SetItemText lvEventRecipient.SelectedItem, "recipientTypeId", "To"
    cboType.Text = "To"
  Else
    cboType.Text = FieldCollection("recipientTypeId")
  End If
    
  txtDataField_Validate 10, False
  
  If lvEventRecipient.ListItems.Count > 0 Then
    cmdPerson.Enabled = (lvEventRecipient.SelectedItem.ListSubItems(DatabaseName.CONTACT_KEY) = vbNullString)
    cmdLocation.Enabled = (lvEventRecipient.SelectedItem.ListSubItems(DatabaseName.CONTACT_KEY) = vbNullString)
  Else
    cmdPerson.Enabled = False
    cmdLocation.Enabled = False
  End If

End Sub
Private Sub EventTypeRefresh(Optional ForceReset As Boolean)
  Dim EventType As Collection
  Set EventType = Factory.EventTypeService.Find(cboDataField(0).Text, """documentFileName""")
  If Not EventType Is Nothing Then
    If txtDataField(15).Text = vbNullString Or ForceReset Then 'Or txtDataField(15).Text <> txtDataField(18).Text Then
      txtDataField(15).Text = EventType("documentFileName")
    End If
  End If
  
  chkDatafield(0).Value = IIf(Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, "DocumentModify"), 1, 0)
  chkEnvelopeAll.Value = IIf(Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, "DocumentEnvelope"), 1, 0)
  
  Dirty = True
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
    
  ' Start custom Code
  If cboDataField(0).ListIndex < 0 Then
    Save = False
    MsgBox "You need to select an event type to save."
    Exit Function
  End If
  
  Dim mDataControls As New Collection, data As Collection, EventRecipientHandle As String
  AddTextControl txtDataField, mDataControls, DatabaseName.EVENT_TABLE
  AddValueControl dtpDataField, mDataControls, DatabaseName.EVENT_TABLE
  AddValueControl chkDatafield, mDataControls, DatabaseName.EVENT_TABLE
  AddTextControl cboDataField, mDataControls, DatabaseName.EVENT_TABLE
  
  Dim AttributeNames(2) As String
  Dim AttributeValues(2) As String
  AttributeNames(0) = "fileName"
  AttributeValues(0) = mDataControls("fileName")
  AttributeNames(1) = "isModify"
  AttributeValues(1) = IIf(mDataControls("isModify") = 1, "Yes", "")
  AttributeNames(2) = "saveFileName"
  AttributeValues(2) = mDataControls("saveFileName")
  
  If mNew Then
    mEventHandle = Factory.EventService.CreateEvent( _
              mDataControls(DatabaseName.EVENTTYPE_KEY), mDataControls("eventName"), _
              mDataControls("eventDesc"), mDataControls("eventNote"), _
              mEventGroupHandle, mDataControls(DatabaseName.FOLDER_KEY), _
              GetDate(Today), vbNullDate, vbNullString, _
              IIf(mSourceHandle = vbNullString, vbNullString, mSource), mSourceHandle, _
              AttributeNames, AttributeValues, vbNullStringArray, vbNullStringArray)
    Save = mEventHandle <> vbNullString
  Else
    Save = Factory.EventService.WriteEvent(mEventHandle, _
              mDataControls(DatabaseName.EVENTTYPE_KEY), mDataControls("eventName"), _
              mDataControls("eventDesc"), mDataControls("eventNote"), _
              mEventGroupHandle, mDataControls(DatabaseName.FOLDER_KEY), _
              mDataControls("createDate"), vbNullDate, vbNullString, _
              AttributeNames, AttributeValues, vbNullStringArray, vbNullStringArray)
  End If
  
  If Not Save Then
    Dirty = True
    Exit Function
  End If

  mNew = False

  ' Remove EventRecipients
  Dim I As Integer
  For I = 1 To mEventRecipientRemove.Count
    Save = Factory.EventRecipientService.DeleteEventRecipient( _
        mEventRecipientRemove.item(I), vbNullStringArray, vbNullStringArray)
  Next I
  Set mEventRecipientRemove = New Collection


  Dim Order As Double
  Order = 0

  ' Save EventRecipient
  Dim item As ListItem
  For Each item In lvEventRecipient.ListItems
    
    Set data = ListViewModule.ListItemCollection(lvEventRecipient.ColumnHeaders, item)
    If data(DatabaseName.EVENTRECIPIENT_KEY) = vbNullString Then
      EventRecipientHandle = Factory.EventRecipientService.CreateEventRecipient( _
          data(DatabaseName.EVENTRECIPIENT_KEY), mEventHandle, _
          data("recipientTypeId"), Order, data(DatabaseName.CONTACT_KEY), _
          data(DatabaseName.PERSON_KEY), data(DatabaseName.LOCATION_KEY), _
          data("contactName"), data("contactSalutation"), _
          data("companyName"), data("addressLine1"), data("addressLine2"), _
          data("addressLine3"), data("city"), data("state"), data("zip"), _
          data("country"), data("carrierRoute"), data("deliveryPointCode"), _
          data("deliveryAddress"), data("isEnvelope") = "Yes", _
          vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
      ListViewModule.SetItemText item, DatabaseName.EVENTRECIPIENT_KEY, EventRecipientHandle
    Else
      Save = Factory.EventRecipientService.WriteEventRecipient( _
          data(DatabaseName.EVENTRECIPIENT_KEY), mEventHandle, _
          data("recipientTypeId"), Order, data(DatabaseName.CONTACT_KEY), _
          data(DatabaseName.PERSON_KEY), data(DatabaseName.LOCATION_KEY), _
          data("contactName"), data("contactSalutation"), _
          data("companyName"), data("addressLine1"), data("addressLine2"), _
          data("addressLine3"), data("city"), data("state"), data("zip"), _
          data("country"), data("carrierRoute"), data("deliveryPointCode"), _
          data("deliveryAddress"), data("isEnvelope") = "Yes", _
          vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
    End If
    Order = Order + 1
  Next item
  
  Dirty = Not Save
End Function

Public Sub ShowByListView(View As ListView)
 If View.ListItems.Count = 0 Then
    MsgBox "There are no contacts to view or edit." ' To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(EventHandle As String)
  mEventHandle = EventHandle
  Show
End Sub

Public Sub ShowNew(EventGroupHandle As String, Optional SourceHandle As String, Optional EventName As String, Optional FolderHandle As String, Optional PersonHandle As String)
  mEventGroupHandle = EventGroupHandle
  mSourceHandle = SourceHandle
  mEventName = EventName
  mFolderHandle = FolderHandle
  mNew = True
  Show
  Refresh
  If mSource = DatabaseName.FIRM_TABLE Then
    cmdAdd_Click
  End If
  If Not PersonHandle = vbNullString Then
    cmdNew_Click
    EventRecipientAddPerson PersonHandle
  End If
End Sub


'
' Swap simply swaps the SortKey of the selected item with the
' desired index
'
Private Sub Swap(Index As Integer)
  lvEventRecipient.Sorted = False
  
  Dim Selected As ListItem
  Set Selected = lvEventRecipient.SelectedItem
  
  Dim item As ListItem
  Set item = lvEventRecipient.ListItems(Index)
  
  Dim SortKey As String
  SortKey = Selected.ListSubItems("~sort")
  Selected.ListSubItems("~sort") = item.ListSubItems("~sort")
  item.ListSubItems("~sort") = SortKey
  
  lvEventRecipient.Sorted = True
  lvEventRecipient.ListItems(Index).Selected = True
End Sub


Public Sub ButtonRefresh()
  Dim Field As TextBox
  If lvEventRecipient.ListItems.Count < 1 Then
    cmdMoveDown.Enabled = False
    cmdMoveUp.Enabled = False
    cmdRemove.Enabled = False
    cmdPerson.Enabled = False
    ClearDataField txtDataField, "EventRecipient"
    For Each Field In txtDataField
      If Field.DataMember = "EventRecipient" Then
        Field.Enabled = False
        Field.BackColor = cmdSave.BackColor
      End If
    Next Field
    chkEnvelope.Enabled = False
    ' ver 1.90.0218 (jjm)
    mEnvelopeEnabled = False
    
    cboType.Enabled = False
    Exit Sub
  End If
  
  With lvEventRecipient.SelectedItem
    cmdMoveUp.Enabled = (.Index > 1)
    cmdMoveDown.Enabled = (.Index < lvEventRecipient.ListItems.Count)
    cmdRemove.Enabled = True
    cmdPerson.Enabled = True
  End With
  
  For Each Field In txtDataField
  If Field.DataMember = "EventRecipient" Then
    Field.Enabled = True
    Field.BackColor = txtDataField(1).BackColor
  End If
  Next Field
  
  chkEnvelope.Enabled = True
    
  ' ver 1.90.0218 (jjm)
  mEnvelopeEnabled = True
    
  cboType.Enabled = True
End Sub

Private Sub SetListViewSortKey()
  Dim I As Integer
  SortIndex = 0
  For I = 1 To lvEventRecipient.ListItems.Count
    If lvEventRecipient.ListItems(I).ListSubItems("recipientTypeId") = "CC" Then
      ListViewModule.SetItemText lvEventRecipient.ListItems(I), "~sort", "Y" & Format(SortIndex, "000")
    ElseIf lvEventRecipient.ListItems(I).ListSubItems("recipientTypeId") = "BCC" Then
      ListViewModule.SetItemText lvEventRecipient.ListItems(I), "~sort", "Z" & Format(SortIndex, "000")
    Else
      ListViewModule.SetItemText lvEventRecipient.ListItems(I), "~sort", "SortIndex" & Format(SortIndex, "000")
    End If
    SortIndex = SortIndex + 1
  Next I
End Sub


Private Sub SearchByHandle(Index As Integer)
  Dispatcher.ShowSearch DatabaseName.CONTACT_TABLE, FormMgr, True, , Index, mSourceHandle
End Sub


'
' End General Methods
'
'
'
' Begin Control Methods
'

Private Sub cboDataField_Click(Index As Integer)
  Select Case Index
    Case 0
      mEventTypeHandle = cboDataField(0).Text
      cboEventTypeName.ListIndex = cboDataField(0).ListIndex
  End Select
  
  Dirty = True
End Sub

Private Sub cboDataField_LostFocus(Index As Integer)
  Select Case Index
    Case 0
      EventTypeRefresh
  End Select
End Sub

Private Sub cboEventTypeName_Click()
  cboDataField(0).ListIndex = cboEventTypeName.ListIndex
  Dirty = True
End Sub

Private Sub cboEventTypeName_LostFocus()
  EventTypeRefresh
End Sub

Private Sub cboType_Validate(Cancel As Boolean)
  SetItemText lvEventRecipient.SelectedItem, cboType.DataField, cboType.Text
  Dirty = True
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
  
  lblLabel(8).Visible = (chkDatafield(0).Value = 1)
  txtDataField(18).Visible = (chkDatafield(0).Value = 1)
  
  If chkDatafield(0).Value = 1 And txtDataField(18) = vbNullString Then
    Dim FileName As String
    FileName = txtDataField(15).Text
    If mNew Or FileName = vbNullString Or Left(FileName, 3) <> "E:\" Then
      txtDataField(18).Text = "E:\M" & Factory.AutoKeyService.GetDocumentKey & ".WP"
    Else
      ' correct for old data/missing data
      Factory.EventService.SetAttribute mEventHandle, "saveFileName", FileName
      txtDataField(18).Text = FileName
    End If
  ElseIf chkDatafield(0).Value = 0 And Not txtDataField(18) = vbNullString Then
    txtDataField(18).Text = vbNullString
  End If
End Sub

Private Sub chkEnvelope_Click()
 ' ver 1.90.0218 (jjm)
  If Not mEnvelopeEnabled Then
    Exit Sub
  End If
  
  txtDataField(2).Enabled = (chkEnvelope.Value = 1)
  ListViewModule.SetItemText lvEventRecipient.SelectedItem, chkEnvelope.DataField, IIf(chkEnvelope.Value = 1, "Yes", vbNullString)
  If (chkEnvelope.Value = 1) Then
     ListViewModule.SetItemText lvEventRecipient.SelectedItem, txtDataField(2).DataField, txtDataField(2).Text
  End If
  
  'minternal prevents the chkEnvelopeAll_Click code from being fired
  mInternal = True
  chkEnvelopeAll.Value = EnvelopeAllValue
  mInternal = False
  Dirty = True
End Sub

Private Sub chkEnvelopeAll_Click()
  If mInternal Then
    Exit Sub
  End If
    
  Dim item As ListItem, strValue As String
  strValue = IIf(chkEnvelopeAll.Value = vbChecked, "Yes", vbNullString)
  For Each item In lvEventRecipient.ListItems
    SetItemText item, "isEnvelope", strValue
  Next item
  chkEnvelope.Value = chkEnvelopeAll.Value
  Dirty = True
End Sub

Private Sub cmdAdd_Click()
  Select Case mEventGroupHandle
    Case DatabaseName.CLAIM_DOCUMENT:
        SearchByHandle 0
        Exit Sub
    Case DatabaseName.FIRM_DOCUMENT:
        SearchByHandle 5
        Exit Sub
    Case DatabaseName.DOCUMENT
        SearchByHandle 8
        Exit Sub
    Case DatabaseName.INCIDENT_DOCUMENT
        SearchByHandle 6
        Exit Sub
  End Select
  Dispatcher.ShowSearch DatabaseName.CONTACT_TABLE, FormMgr, True
End Sub

Private Sub cmdCancel_Click()
  Dirty = False
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtDataField(16).Text = vbNullString
  txtDataField(16).SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub


'
' Load selection of location.
'
Private Sub cmdLocation_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr, True
End Sub

Private Sub cmdMoveDown_Click()
  Swap lvEventRecipient.SelectedItem.Index + 1
  ButtonRefresh
  lvEventRecipient.SetFocus
  Dirty = True
End Sub

Private Sub cmdMoveUp_Click()
  Swap lvEventRecipient.SelectedItem.Index - 1
  ButtonRefresh
  lvEventRecipient.SetFocus
  Dirty = True
End Sub

Private Sub cmdNew_Click()
  Dim item As ListItem
  Set item = EventRecipientNew
  EventRecipientResync
  ListViewModule.SetItemText item, "contactName", "New Recipient"
  ButtonRefresh
End Sub

'
' Load selection of person
'
Private Sub cmdPerson_Click()
  Dispatcher.ShowSearch DatabaseName.PERSON_TABLE, FormMgr, True
End Sub

Private Sub cmdRemove_Click()
  '
  ' Removes the item in lvEventRecipient and stores the item
  ' in mEventRecipientRemove. This way the items are not removed
  ' till save is called
  '
  Dim EventRecipientHandle As String
  EventRecipientHandle = ListViewModule.ListItemText(lvEventRecipient.SelectedItem, DatabaseName.EVENTRECIPIENT_KEY)
  If (EventRecipientHandle <> vbNullString) Then
    mEventRecipientRemove.Add EventRecipientHandle
  End If
  lvEventRecipient.ListItems.Remove lvEventRecipient.SelectedItem.Index
  lvEventRecipient.SetFocus
  If Not lvEventRecipient.ListItems.Count < 1 Then
    lvEventRecipient.SelectedItem.Selected = True
  End If
  ButtonRefresh
End Sub

Private Sub cmdSave_Click()
  If Save Then
    FormMgr.Message = fimSave
    FormUtil.SetComboDefault cboDataField(0)
    Unload Me
  End If
End Sub


Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_Initialize()
  Set mEventRecipientDataset = NewGenericDataset("EventRecipient")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
'    Select Case KeyCode
'      Case vbKeyC
'        mnuFileNewContact_Click
'        KeyCode = 0
'    End Select
'  End If
  If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , Me.hWnd
  End If
End Sub

'
' Load Controls
'
Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  DeserializeColumnHeaders lvEventRecipient, lvEventRecipient.Tag
  
  FormRefresh
  FormResize
  TabControlEnable SSTab1
End Sub

'
' Terminate Used for memory audit
'
Private Sub Form_Terminate()
  'MsgBox "frmApplicationEdit.Terminate"
End Sub

'
' See if form can unload.
'
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = 1
  End If
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvEventRecipient_Click()
  EventRecipientResync
End Sub

Private Sub lvEventRecipient_DblClick()
  EventRecipientResync
End Sub

Private Sub lvEventRecipient_ItemClick(ByVal item As MSComctlLib.ListItem)
  ButtonRefresh
End Sub

Private Sub lvEventRecipient_KeyUp(KeyCode As Integer, Shift As Integer)
  EventRecipientResync
End Sub

Private Sub mnuEditMove_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowMove(txtDataField(17).Text) Then
    txtDataField(17).Text = fForm.FolderKey
  End If
  Unload fForm
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Save(True) Then
    mNew = True
    FormRefresh
'    cboDataField(0).SetFocus
  End If
End Sub


Private Sub mnuFileProperties_Click()
  FormUtil.ShowEventProperties mEventHandle
End Sub

Private Sub mnuFileSave_Click()
  If Save Then
    FormMgr.Message = fimSave
  End If
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsEnvelope_Click()
  FormUtil.PrintEnvelope txtDataField(2).Text, txtDataField(6).Text
End Sub

Private Sub mnuToolsMerge_Click()
  If mNew Or mDirty Then
    MsgBox "Must save before merging.  Use Ctrl-S or the File/Save menu."
    Exit Sub
  End If
  
  Dim Pointer As New WaitPointer, Handles(0) As String
  Pointer.Default
  
  ' Print Envelopes
  Factory.EventRecipientService.PrintEnvelope mEventHandle
  
  ' Perform Document Merge
  Handles(0) = mEventHandle
  Factory.EventService.DocumentMerge Handles
  
  ' Refresh for changes to fileName and isEnvelope which may be modified.
  FormRefresh
End Sub

Private Sub mnuToolsResetFileName_Click()
  EventTypeRefresh True
End Sub

Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvEventRecipient, mEventRecipientDataset.DataProvider, False
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvEventRecipient, lvEventRecipient.Tag
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub






Private Sub SSTab1_Click(PreviousTab As Integer)
  TabControlEnable SSTab1
  ButtonRefresh
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDataField_Change(Index As Integer)
  Dirty = True
  Select Case Index
'    Case 0
'      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, databasename.EVENTTYPE_KEY, txtDataField(Index).Text) - 1
    Case 2
      mLabelDirty = True
  End Select
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  Select Case Index
    Case 0
      cmdEnter.Enabled = False
    Case 16
      FormUtil.NoteGotFocus cmdSave, cmdEnter
  End Select
  If Not Index = 16 Then
    FormUtil.EditTextSelect txtDataField(Index)
  End If
End Sub

Private Sub txtDataField_LostFocus(Index As Integer)
  Select Case Index
    Case 0
      cmdEnter.Enabled = True
    Case 16
      FormUtil.NoteLostFocus cmdSave, cmdEnter
  End Select
End Sub

Private Sub txtDataField_Validate(Index As Integer, Cancel As Boolean)
  If txtDataField(Index).DataMember = "EventRecipient" Then
  
    txtDataField(2) = IIf(txtDataField(3) = vbNullString, vbNullString, Replace(UCase(txtDataField(3)), ".", vbNullString) & vbCrLf) & _
                         IIf(txtDataField(3) = txtDataField(7) Or txtDataField(7) = vbNullString, vbNullString, Replace(Replace(UCase(txtDataField(7).Text), ",", vbNullString), ".", vbNullString) & vbCrLf) & _
                         IIf(txtDataField(8) = vbNullString, vbNullString, (Replace(UCase(txtDataField(8)), ".", vbNullString) & vbCrLf)) & _
                         IIf(txtDataField(9) = vbNullString, vbNullString, (Replace(UCase(txtDataField(9)), ".", vbNullString) & vbCrLf)) & _
                         IIf(txtDataField(10) = vbNullString, vbNullString, (Replace(UCase(txtDataField(10)), ".", vbNullString) & vbCrLf)) & _
                         UCase(txtDataField(11) & " " & txtDataField(12) & "  " & txtDataField(13))
                         
    ListViewModule.SetItemText lvEventRecipient.SelectedItem, txtDataField(Index).DataField, txtDataField(Index).Text
  End If
  'If Index = 0 Then
  '  Cancel = Not DescriptionRefresh
  'End If
End Sub
