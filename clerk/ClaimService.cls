VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClaimService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.20.0613 (jjm)
'previous ver 2.20.0610 (jjm)
Option Explicit

Private Const ServiceName = "ClaimService"

Private mClient As SoapClient30
Private mDataSet As Dataset
Private mLawArea As Dataset
Private mLawError As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.CLAIM_TABLE)

  
  'Setup SOAP
On Error GoTo Exception
  
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
   
End Sub


Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.CLAIM_KEY, Handle, Column)
End Function



'
' Write an application.
'
Public Function WriteClaim( _
    Handle As String, claimant As String, _
    CoverageDate As Date, _
    OccurDateRange As String, OccurDate As Date, _
    KnowDateRange As String, KnowDate As Date, _
    ClaimMadeDate As Date, Adjuster As String, _
    InceptionDate As Date, _
    Tail As Boolean, TreatyYear As Double, _
    PleadingCaption As String, Court As String, _
    CourtCaseNum As String, InsuredServeDate As Date, _
    InsurerServeDate As Date, FirmServeDate As Date, _
    TrialDateRange As String, JudgmentClaimant As Boolean, _
    FeeDispute As Boolean, LawDispositionId As String, _
    claimType As String, eventNote As String, _
    lawAreas() As String, lawErrors() As String, _
    IsReinsurerReport As Boolean, ReinsurerReportDate As Date) As Boolean
    
On Error GoTo ErrorHandler

     Call mClient.Write( _
        Handle, claimant, CoverageDate, _
        OccurDateRange, OccurDate, _
        KnowDateRange, KnowDate, _
        ClaimMadeDate, Adjuster, _
        InceptionDate, _
        Tail, TreatyYear, _
        PleadingCaption, Court, _
        CourtCaseNum, InsuredServeDate, _
        InsurerServeDate, FirmServeDate, _
        TrialDateRange, JudgmentClaimant, _
        FeeDispute, LawDispositionId, _
        claimType, eventNote, _
        lawAreas, lawErrors, _
        IsReinsurerReport, ReinsurerReportDate)
    WriteClaim = True
    Exit Function
    
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Function


Public Sub ShowNarrative(Handle As String)
  Dim Path As String
  Path = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVEPATH_INI) & _
         IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVEPREFIX_INI) & _
         Handle & _
         IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVESUFFIX_INI)
         
  ShellExecute 0&, vbNullString, GetRealPath(IniFolder.ReadFile(UrlName.MISC_INI, "casesht.bat")), _
            Path, vbNullString, SW_SHOWNORMAL
End Sub

