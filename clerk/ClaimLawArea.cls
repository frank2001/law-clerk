VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InstanceLawAreaDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
Option Explicit

' DatasetTemplate

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer


Private Sub Class_Initialize()
  ListIndex = 0
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider(DatabaseName.INSTANCELAWAREA_TABLE)
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in claim number field only"
  frm.cboIn.AddItem "in incident number field only"
  frm.cboIn.AddItem "in law area identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, UrlName.INSTANCELAWAREASEARCH_CLAIMNUMBER_INDEX)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As String
  If Not Words = "" Then
    Select Case ListIndex
      Case UrlName.INSTANCELAWAREASEARCH_CLAIMNUMBER_INDEX
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.CLAIMNUMBER_FIELD, Words)
      Case UrlName.INSTANCELAWAREASEARCH_INCIDENTNUMBER_INDEX
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.INCIDENTNUMBER_FIELD, Words)
      Case UrlName.INSTANCELAWAREASEARCH_LAWAREAID_INDEX
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.LAWAREA_KEY, Words)
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

