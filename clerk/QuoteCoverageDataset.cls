VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QuoteCoverageDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'ver 2.10.0421 (jjm)
'ver 2.00.0311 (jjm)
Option Explicit

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private mListIndex As Integer


Private Sub Class_Initialize()
  mListIndex = 0
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.QUOTECOVERAGE_VIEW
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  frm.cboIn.AddItem "in quote coverage identifier field only"
  frm.cboIn.AddItem "in quote book identifier field only"
  frm.cboIn.AddItem "in firm entity locator field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, mListIndex, 0)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  mListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Select Case mListIndex
      Case 0
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.QUOTECOVERAGE_KEY, Words)
      Case 1
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.QUOTEBOOK_KEY, Words)
      Case 2
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.FIRMENTITYLOCATOR_FIELD, Words)
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

