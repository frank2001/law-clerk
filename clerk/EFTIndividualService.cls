VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EFTIndividualService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const WSDLFile = "EFTIndividualService"

Private Const SERVICE = "EFTIndividualService"
Private Const SERVICE11 = "EFTIndividualService11"
Private Const SERVICE12 = "EFTIndividualService12"

Private mClient As SoapClient30
Private mDataSet As Dataset
Private mDepositFinancialInstDataset As Dataset
Private mServiceName As String


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
'  Set mClient = New SoapClient30
'
'  Dim url As String
'  url = HttpServer & "component/" & WSDLFile & ".wsdl"
'
'  '
'  ' Load the list of server versions supported
'  '
'  Dim ServiceList(3) As String
'  ServiceList(0) = SERVICE12
'  ServiceList(1) = SERVICE11
'  ServiceList(2) = SERVICE
'
'  '
'  ' Cycle through the list of service versions and see if they are
'  ' compatible with the middle tier.
'  '
'  Dim I As Integer
'  For I = 0 To SafeUBound(ServiceList)
'    Set mClient = Factory.ServiceMSSoapInit(url, ServiceList(I))
'    If Not mClient Is Nothing Then
'      mServiceName = ServiceList(I)
'      Exit For
'    End If
'  Next I
'
'  If mClient Is Nothing Then
'    MsgBox "EFT Individual Service Error:  Unable to connect to the server (" & HttpServer & ").  You will not be able to save changes.", vbCritical, "Error"
'    Exit Sub
'  End If
'
'  'mClient.mssoapinit HttpServer &  "component/" & ServiceName & ".wsdl", "LawyerService"
'  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & "rpcrouter" & LawSession.Session

  Set mDataSet = NewGenericDataset(DatabaseName.EFTINDIVIDUAL_TABLE)
  Set mDepositFinancialInstDataset = NewGenericDataset(DatabaseName.EFTDEPOSITFINANCIALINST_TABLE)
End Sub

Public Function GetDepositFinancialInstName(EFTDepositFinancialInstHandle As String) As String
  Dim Result As String
  mDepositFinancialInstDataset.DataProvider.filter = """" & DatabaseName.EFTDEPOSITFINANCIALINST_KEY & """ = '" & EFTDepositFinancialInstHandle & "'"
  mDepositFinancialInstDataset.DataProvider.Column = """" & DatabaseName.INSTITUTIONNAME_FIELD & """"
  mDepositFinancialInstDataset.DataProvider.OpenRecordset
  If Not mDepositFinancialInstDataset.DataProvider.Collection Is Nothing Then
    Result = mDepositFinancialInstDataset.DataProvider.Collection(1)(DatabaseName.INSTITUTIONNAME_FIELD)
  End If
  GetDepositFinancialInstName = Result
End Function

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.EFTINDIVIDUAL_KEY, Handle, Column)
End Function

'
' Interface to server
'


'
' Create an EFT Individual
'
Public Function CreateEFTIndividual( _
    EFTIndividualId As String, IndividualName As String, EFTTransactionCodeId As String, EFTDepositFinancialInst As String, _
    AccountNumber As String, IsPrenoteSent As Boolean, ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As String

On Error GoTo ErrorHandler

  Select Case mServiceName
    Case SERVICE
      CreateEFTIndividual = mClient.Create(EFTIndividualId, _
        IndividualName, EFTTransactionCodeId, _
        EFTDepositFinancialInst, _
        AccountNumber, IsPrenoteSent, _
        AttributeNames, AttributeValues, _
        ParameterNames, ParameterValues)
  End Select

  Exit Function

ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox mServiceName & " Fault: " & mClient.FaultString, vbExclamation
End Function

'
' Create an EFT Individual
'
Public Function WriteEFTIndividual( _
    EFTIndividualId As String, IndividualName As String, EFTTransactionCodeId As String, EFTDepositFinancialInst As String, _
    AccountNumber As String, IsPrenoteSent As Boolean, ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler

  Select Case mServiceName
    Case SERVICE
      Call mClient.Write(EFTIndividualId, _
        IndividualName, EFTTransactionCodeId, _
        EFTDepositFinancialInst, _
        AccountNumber, IsPrenoteSent, _
        AttributeNames, AttributeValues, _
        ParameterNames, ParameterValues)
      WriteEFTIndividual = True
  End Select

  Exit Function

ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
End Function


''
'' Delete an Lawyer
''
'Public Function DeleteLawyer( _
'    LawyerHandle As String, _
'    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
'
'On Error GoTo ErrorHandler
'
'  Call mClient.Delete(LawyerHandle, ParameterNames, ParameterValues)
'  DeleteLawyer = True
'  Exit Function
'
'ErrorHandler:
'  MsgBox "Fault: " & mClient.FaultString, vbExclamation
'  DeleteLawyer = False
'End Function


