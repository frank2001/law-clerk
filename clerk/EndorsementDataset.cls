VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EndorsementDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
Option Explicit

'Endorsement

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer


Private Sub Class_Initialize()
  ListIndex = 1
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.ENDORSEMENT_TABLE '"Endorsement"
'  mDataProvider.Sort = """endorsementId"""
'  mDataProvider.Column = """endorsementId"", ""policyId"", ""endorsementTypeId"", ""endorsementNum"", ""issueDate"""
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  frm.cboIn.AddItem "in endorsement identifier field only"
  frm.cboIn.AddItem "in firm entity locator field only"
  frm.cboIn.AddItem "in policy identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, 1)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection

  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As String
  If Not Words = "" Then
    Select Case ListIndex
      Case 0
        newFilter = LawDialect.ContainsUpperCase(DatabaseName.ENDORSEMENT_KEY, Words)
      Case 1
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.FIRMENTITYLOCATOR_FIELD, Words)
      Case 2
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.POLICYNUMBER_FIELD, Words)
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

