VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEFTIndividualEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "EFT Individual Edit"
   ClientHeight    =   3225
   ClientLeft      =   150
   ClientTop       =   1140
   ClientWidth     =   5070
   Icon            =   "frmEFTIndividualEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3225
   ScaleWidth      =   5070
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3192
      Left            =   0
      ScaleHeight     =   3195
      ScaleWidth      =   5070
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   0
      Width           =   5064
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   2508
         TabIndex        =   6
         Top             =   2772
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   3792
         TabIndex        =   7
         Top             =   2772
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   2580
         Left            =   84
         TabIndex        =   8
         Top             =   96
         Width           =   4908
         _ExtentX        =   8652
         _ExtentY        =   4551
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmEFTIndividualEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label13"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label2(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblInstitutionName"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label4"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label3"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblEftTransactionTypeName"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblDatafield(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Label5"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblDatafield(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtDataField(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtDataField(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "cmdEftDepositFinancialInstiution"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "cboTransactionCode"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "cmdFirm"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmEFTIndividualEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).Control(1)=   "cmdClearNotepad"
         Tab(1).ControlCount=   2
         Begin VB.CommandButton cmdFirm 
            Caption         =   "Firm"
            Enabled         =   0   'False
            Height          =   288
            Left            =   3768
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   390
            Width           =   1028
         End
         Begin VB.ComboBox cboTransactionCode 
            DataField       =   "eftTransactionCodeId"
            Height          =   288
            Left            =   1584
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   1080
            Width           =   1260
         End
         Begin VB.CommandButton cmdEftDepositFinancialInstiution 
            Caption         =   "Institution"
            Height          =   288
            Left            =   3768
            TabIndex        =   2
            Top             =   1464
            Width           =   1028
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "accountNumber"
            Height          =   300
            Index           =   0
            Left            =   1584
            TabIndex        =   3
            Top             =   1836
            Width           =   2136
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74940
            TabIndex        =   5
            Top             =   2172
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   1800
            Left            =   -74940
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   4
            Top             =   300
            Width           =   4788
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "individualName"
            Height          =   312
            Index           =   2
            Left            =   1584
            TabIndex        =   0
            Top             =   720
            Width           =   2136
         End
         Begin VB.Label lblDatafield 
            AutoSize        =   -1  'True
            Caption         =   "isPrenoteSent"
            DataField       =   "isPrenoteSent"
            Height          =   192
            Index           =   1
            Left            =   1584
            TabIndex        =   21
            Top             =   2280
            Width           =   1008
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Is Prenote Sent:"
            Height          =   192
            Left            =   120
            TabIndex        =   20
            Top             =   2280
            Width           =   1116
         End
         Begin VB.Label lblDatafield 
            AutoSize        =   -1  'True
            Caption         =   "eftIndividualId"
            DataField       =   "eftIndividualId"
            Height          =   192
            Index           =   0
            Left            =   1584
            TabIndex        =   19
            Top             =   438
            Width           =   984
         End
         Begin VB.Label lblEftTransactionTypeName 
            AutoSize        =   -1  'True
            Caption         =   "eftTransactionTypeName"
            Height          =   192
            Left            =   2952
            TabIndex        =   18
            Top             =   1128
            Width           =   1848
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Transaction Code:"
            Height          =   192
            Left            =   120
            TabIndex        =   17
            Top             =   1128
            Width           =   1320
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Name:"
            Height          =   192
            Left            =   120
            TabIndex        =   16
            Top             =   780
            Width           =   480
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Identifier:"
            Height          =   192
            Left            =   120
            TabIndex        =   9
            Top             =   438
            Width           =   636
         End
         Begin VB.Label lblInstitutionName 
            AutoSize        =   -1  'True
            Caption         =   "institutionName"
            DataField       =   "institutionName"
            Height          =   192
            Left            =   1584
            TabIndex        =   14
            Top             =   1512
            UseMnemonic     =   0   'False
            Width           =   1092
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Account Number:"
            Height          =   192
            Index           =   0
            Left            =   120
            TabIndex        =   11
            Top             =   1896
            Width           =   1224
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Financial Institution:"
            Height          =   192
            Left            =   120
            TabIndex        =   10
            Top             =   1512
            Width           =   1368
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   276
      TabIndex        =   15
      Top             =   780
      Width           =   1056
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&EFT Individual"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator3872 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&EFT Deposit Financial Institution"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparatro2823 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator86732 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator365 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmEFTIndividualEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.60.00 (jjm) 2004 03 23
Option Explicit

Private mEFTIndividualHandle As String
Private mEFTDepositFinancialInstHandle As String
Private mEventHandle As String
Private mNew As Boolean

Private mDirty As Boolean
Private mCancel As Boolean

'for the transaction name
Private TransactionNames() As String

' Form Manager
Private FormMgr As New FormManager



'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Private Sub EnableFirmSelect()
  cmdFirm.TabStop = mNew
  cmdFirm.TabIndex = IIf(mNew, 0, 999)
  'cmdFirm.Enabled = mNew
  cmdFirm.Visible = mNew
End Sub

Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Child is either a Firm
' select (Firm) from cmdFirm or a new firm (NewFirm) from mnuFileNewFirm.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      
    ' Select form requested new firm.
    Case fimNew
    
    ' Select form selected
    Case fimSelect
      Select Case fInterface.Key
        Case DatabaseName.EFTDEPOSITFINANCIALINST_TABLE
          mEFTDepositFinancialInstHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.EFTDEPOSITFINANCIALINST_KEY)
          lblInstitutionName.Caption = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INSTITUTIONNAME_FIELD)
          Dirty = True
        Case DatabaseName.FIRM_TABLE
          lbldatafield(0).Caption = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRM_KEY)
          'EFT Standards only allow the individualName field to be 22 characters
          'If the firm name is too long then it needs to be truncated.
          Dim tmpStr As String
          tmpStr = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRMENTITYNAME_FIELD)
          If Len(tmpStr) > 22 Then tmpStr = Left(tmpStr, 22)
          txtDataField(2).Text = tmpStr
      End Select

    ' A child form has called a save
    Case fimSave
    
  End Select
  Show

End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub


Private Sub FormRefresh()
    
  If (mNew) Then
    Caption = "New EFT Individual"
    mEventHandle = vbNullString
    ClearDataField lbldatafield
    ClearDataField txtDataField
    lblInstitutionName = vbNullString
    mEFTDepositFinancialInstHandle = vbNullString
    mEFTIndividualHandle = vbNullString
    cboTransactionCode.ListIndex = 0
  Else
    Dim Datafields As Collection
    Set Datafields = Factory.EFTIndividualService.Find(mEFTIndividualHandle)
    SetFields txtDataField, Datafields
    SetFields lbldatafield, Datafields
    
    
    Dim I As Integer
    For I = 0 To cboTransactionCode.ListCount - 1
      If cboTransactionCode.List(I) = Datafields(DatabaseName.EFTTRANSACTIONCODE_KEY) Then
        cboTransactionCode.ListIndex = I
        Exit For
      End If
    Next I
        
    mEFTDepositFinancialInstHandle = Datafields(DatabaseName.EFTDEPOSITFINANCIALINST_KEY)
    If mEFTDepositFinancialInstHandle <> vbNullString Then
      lblInstitutionName = Factory.EFTIndividualService.GetDepositFinancialInstName(mEFTDepositFinancialInstHandle)
    Else
      lblInstitutionName = vbNullString
    End If
    
    Me.Caption = "EFT Individual Edit (" & mEFTIndividualHandle & ")"

    mEventHandle = Datafields.item(DatabaseName.EVENT_KEY)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
  End If
  
  EnableFirmSelect

'  txtDataField(EFTIndividualIdIndex).Locked = Not mNew
'  txtDataField(EFTIndividualIdIndex).BackColor = IIf(mNew, txtDataField(2).BackColor, cmdCancel.BackColor)
  
  mnuFileOpen.Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  
  Dirty = False
End Sub

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Start custom Code
  
  '
  ' Validate the data. This is one of the few instances where
  ' the client validates data. In this case its fairly safe due
  ' to the fact that the data is being validated against EFT
  ' standards.
  '
  Dim ValidateMessage As String
  
  'Check Identifier - must exist
  If mNew And lbldatafield(0).Caption = vbNullString Then
    ValidateMessage = "- An identifier needs to be selected." & vbCrLf
  End If
  
  'Check Name - must exist
  If txtDataField(2).Text = vbNullString Then
    ValidateMessage = ValidateMessage & "- An individual name must to be entered." & vbCrLf
  End If
  
  'Check Name - 22 char limit
  If Len(txtDataField(2).Text) > 22 Then
    ValidateMessage = ValidateMessage & "- The individual name can only be up to 22 characters long." & vbCrLf
  End If
  
  'Check Deposit Financial Institution - must exist
  If mEFTDepositFinancialInstHandle = vbNullString Then
    ValidateMessage = ValidateMessage & "- An eft deposit financial institution must be selected." & vbCrLf
  End If
  
  'Check Account Number - must exist
  If txtDataField(0).Text = vbNullString Then
    ValidateMessage = ValidateMessage & "- An account number must to be entered." & vbCrLf
  End If
  
  'Check Account Number - 17 char limit
  If Len(txtDataField(0).Text) > 17 Then
    ValidateMessage = ValidateMessage & "- The account number can only be up to 17 characters long." & vbCrLf
  End If
  
  'Display the message
  If ValidateMessage <> vbNullString Then
    MsgBox "Unable to save due to the following:" & vbCrLf & ValidateMessage, vbCritical, "Unable to save"
    save = False
    Exit Function
  End If
  
  
  
  '
  ' Save Data
  '
  
  'Get the eventTypeId
  Dim mDataControls As New Collection
  AddTextControl txtDataField, mDataControls
  AddCaptionControl lbldatafield, mDataControls
  
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  AddParameter "EFTIndividualEventNote", _
      IIf(txtNote.Text = vbNullString, " ", txtNote.Text), _
      ParameterNames, ParameterValues
  
  If (mNew) Then
    
    mEFTIndividualHandle = Factory.EFTIndividualService.CreateEFTIndividual( _
      mDataControls(DatabaseName.EFTINDIVIDUAL_KEY), mDataControls(DatabaseName.INDIVIDUALNAME_FIELD), _
      cboTransactionCode.Text, mEFTDepositFinancialInstHandle, mDataControls(DatabaseName.ACCOUNTNUMBER_FIELD), _
      GetBoolean(mDataControls(DatabaseName.ISPRENOTESENT_FIELD)), vbNullStringArray, vbNullStringArray, _
      ParameterNames, ParameterValues)
                
    save = mEFTIndividualHandle <> vbNullString
  Else
    save = Factory.EFTIndividualService.WriteEFTIndividual( _
        mEFTIndividualHandle, mDataControls(DatabaseName.INDIVIDUALNAME_FIELD), _
        cboTransactionCode.Text, mEFTDepositFinancialInstHandle, mDataControls(DatabaseName.ACCOUNTNUMBER_FIELD), _
        GetBoolean(mDataControls(DatabaseName.ISPRENOTESENT_FIELD)), vbNullStringArray, vbNullStringArray, _
        ParameterNames, ParameterValues)
  End If

  If save Then
    mNew = False
    FormMgr.Message = fimSave
    EnableFirmSelect
  End If
  Dirty = Not save
End Function


Public Sub ShowNew()
  mNew = True
  FormRefresh
  Show
  cmdFirm_Click
End Sub

Public Sub ShowByPrimaryKey(EFTIndividualId As String)
  mEFTIndividualHandle = EFTIndividualId
  Show
End Sub

Private Sub cboTransactionCode_Click()
  lblEftTransactionTypeName.Caption = TransactionNames(cboTransactionCode.ListIndex)
  Dirty = True
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEftDepositFinancialInstiution_Click()
  Dispatcher.ShowSearch DatabaseName.EFTDEPOSITFINANCIALINST_TABLE, FormMgr, True, , , , "Select an EFT Deposit Financial Institution"
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub


Private Sub cmdFirm_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True, , , , "Select a firm for the EFT individual"
End Sub

Private Sub cmdSave_Click()
  If save Then
    FormMgr.Message = fimSave
    Unload Me
  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'ToDO: add help code
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
'    Select Case KeyCode
'    End Select
'  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  'Custom Accelerators
  KeyPreview = True
    
  FormUtil.EditLoad Me
  FormMgr.Load Me
   
  'Load the combobox
  Dim TransactionCodeData As Dataset
  Set TransactionCodeData = NewGenericDataset(EFTTRANSACTIONCODE_TABLE)
  TransactionCodeData.DataProvider.OpenRecordset
  Dim Col As Collection
  Set Col = TransactionCodeData.DataProvider.Collection
  cboTransactionCode.Clear
  Dim I As Integer
  For I = 1 To Col.Count
    cboTransactionCode.AddItem Col(I)(DatabaseName.EFTTRANSACTIONCODE_KEY)
    ReDim Preserve TransactionNames(0 To I - 1)
    TransactionNames(I - 1) = Col(I)(DatabaseName.TRANSACTIONTYPENAME_FIELD)
  Next I
     
  FormRefresh
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Resize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not save(True) Then Exit Sub
  mNew = True
  FormRefresh
End Sub

Private Sub mnuFileNewOther_Click(Index As Integer)
  Select Case Index
    Case 0
      Dispatcher.ShowNew DatabaseName.EFTDEPOSITFINANCIALINST_TABLE
  End Select
End Sub

'
' Check for changes and open application
'
Private Sub mnuFileOpen_Click()
  FormUtil.ShowByPrimaryKey New frmFirmOpen, mEFTIndividualHandle, FormMgr
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.EFTINDIVIDUAL_TABLE, mEFTIndividualHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
  
  If SSTab1.Tab = 0 Then
    EnableFirmSelect
  End If
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
