Attribute VB_Name = "XMLUtil"
Option Explicit

'Example of the xml file returned
'
'<?xml version="1.0"?>
'<xml>
'  <data>
'    <PersonView>
'      <displayName>John E. Smith</displayName>
'      <personId>PN000100004</personId>
'      <lastName>Smith</lastName>
'      <firstName>John</firstName>
'    </PersonView>
'  </data>
'  <result>
'    <start>1</start>
'    <count>1</count>
'    <max>5</max>
'    <next/>
'    <elements>
'      <displayName/>
'      <personId/>
'      <lastName/>
'      <firstName/>
'    </elements>
'  </result>
'  <request>
'    <method>POST</method>
'    <parameter name="max">5</parameter>
'    <parameter name="qReturn">displayName,personId,lastName,firstName</parameter>
'    <parameter name="qOrder">displayName</parameter>
'    <parameter name="qFor">PersonView</parameter>
'    <parameter name="display">text/xml</parameter>
'    <remote-user>someuser</remote-user>
'    <session>
'      <attribute name="josx.security.user">josx.security.auth.SubjectUser@f00e0f</attribute>
'      <id>140A6AAA7F932D870F3C0050021A08AC</id>
'    </session>
'  </request>
'</xml>





Public Function HttpPost(url As String, Params As String) As xmlHttp


On Error GoTo ErrorHandler

  Dim xmlHttp As New xmlHttp
  xmlHttp.open "POST", url, False
  xmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
  xmlHttp.send Params
  Set HttpPost = xmlHttp
  'Open "c:\xml.txt" For Append As #1
  'Write #1, xmlHttp.responseText
  'Close #1
  Exit Function
ErrorHandler:
  ErrorUtil.DisplayXMLHttpError xmlHttp, url
  Set HttpPost = xmlHttp
End Function


Public Function HttpGet(url As String) As xmlHttp
  Dim xmlHttp As New xmlHttp
  xmlHttp.open "GET", url, False
  xmlHttp.send
  Set HttpGet = xmlHttp
End Function

'Function to determine if a node contains a node by a given NodeName
Public Function NodeContainsChildNode(ParentNode As MSXML2.IXMLDOMNode, ChildNodeName As String) As Boolean
  On Error GoTo Exception
  NodeContainsChildNode = (ParentNode.selectSingleNode(ChildNodeName).Text <> vbNullString)
Exception:
End Function


Public Function Rowset(Element As MSXML2.IXMLDOMNode, Source As String) As Collection
  Dim Node As MSXML2.IXMLDOMNode
  Dim RowsetNode As MSXML2.IXMLDOMNode
  Dim Result As New Collection
    
  If Element Is Nothing Then
    Exit Function
  End If
   
  Set RowsetNode = Element.selectSingleNode("data") '("data/" & LCase(Source))
  If RowsetNode Is Nothing Then
    Exit Function
  End If
  
  If Not RowsetNode.hasChildNodes Then
    Exit Function
  End If
  
  ' Ver 2.70 XQuery
  ' Create a list of elements to be used for populating empty fields inside the collection
  ' create a collection to temp store the row
  ' Populate collection with the fields of the row
  ' if the field exists in the element list, but not in the data, enter a blank field into the field section
  ' add the row collection to the result collection
  Dim ElementNodeList, ElementNode As MSXML2.IXMLDOMNode
  Set ElementNodeList = Element.selectSingleNode("result/elements")
  
  'test for existing elements node
  If ElementNodeList Is Nothing Then
    MsgBox "XML Error: The format of the xml response from " & _
    HttpServer & " is not valid. The ""results/elements"" node is not found.", vbCritical, "XML Error"
    Exit Function
  End If
  
  
  Dim Row As Collection
  For Each Node In RowsetNode.childNodes
    Set Row = New Collection
    
    
    For Each ElementNode In ElementNodeList.childNodes

      If NodeContainsChildNode(Node, ElementNode.NodeName) Then
        Row.Add CreateDataField(Source, ElementNode.NodeName, _
            DataValue(ElementNode.NodeName, _
            Node.selectSingleNode(ElementNode.NodeName).Text)), _
            ElementNode.NodeName
      Else
        Row.Add CreateDataField(Source, ElementNode.NodeName, _
            vbNullString), ElementNode.NodeName
      End If
    Next ElementNode
    
    Result.Add Row
  Next Node
    
  Set Rowset = Result
End Function

'Only use for old backward compatability
Public Function RowsetOLD(Element As MSXML2.IXMLDOMNode, Source As String) As Collection
  Dim Node, RowsetNode As MSXML2.IXMLDOMNode, Result As New Collection
    
  If Element Is Nothing Then
    Exit Function
  End If
   
  Set RowsetNode = Element.selectSingleNode("data/" & LCase(Source))
  If Not RowsetNode.hasChildNodes Then
    Exit Function
  End If
  
  For Each Node In RowsetNode.childNodes
    Result.Add RowOLD(Node.childNodes, Source)
  Next Node
  
  Set RowsetOLD = Result
End Function
'Used with RowsetOLD
Public Function RowOLD(Nodes As MSXML2.IXMLDOMNodeList, Source As String) As Collection
  Dim Node As MSXML2.IXMLDOMNode, Result As New Collection, NodeName As String
  
  For Each Node In Nodes
    NodeName = Node.NodeName
    Result.Add CreateDataField(Source, NodeName, _
          DataValue(NodeName, Node.Text)), NodeName
  Next Node
  
  Set RowOLD = Result
End Function


'
' Currently, the only formatting needed is for dates and conversion to
' newline.  The XML is delivered
' in SQL date format and with a single return chr(10).
' The date will change shortly to the SOAP date format.
'
Public Function DataValue(NodeName As String, Text As String) As String
  If (Right(NodeName, 4) <> "Date" And Right(NodeName, 4) <> "Time") Or Len(Text) < 10 Then
    DataValue = Replace(Text, Chr(10), vbNewLine)
    Exit Function
  End If
    
  Dim DateVal As Date
  DateVal = DateSerial(Left(Text, 4), Mid(Text, 6, 2), Mid(Text, 9, 2))
  DataValue = Format(DateVal, LocaleShortDate)
  If Len(Text) > 10 Then
    DataValue = DataValue & " " & Format(TimeSerial(CInt(Mid(Text, 12, 2)), CInt(Mid(Text, 15, 2)), CInt(Mid(Text, 18, 2))), "h:mm:ss AMPM")
  End If
End Function


Public Function DisplayFormat(DataField As String, DataValue As String, _
                              Optional DataFormat As String, Optional DateFormat As String) As String
  If DataFormat <> vbNullString Then
    DisplayFormat = Format(DataValue, DataFormat)
    Exit Function
  End If
  If (Right(DataField, 4) <> "Date" And Right(DataField, 4) <> "Time") Or Len(DataValue) < 10 Or DateFormat = vbNullString Then
    DisplayFormat = DataValue
    Exit Function
  End If
  DisplayFormat = Format(DataValue, DateFormat)
End Function
