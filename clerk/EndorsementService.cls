VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EndorsementService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "EndorsementService"
Private mDataProvider As DataProvider

'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set Client = New SoapClient30
  Client.mssoapinit WSDLFile
  Client.ConnectorProperty("EndPointURL") = EndPointURL
  
  Set mDataProvider = Session.NewDataProvider(DatabaseName.ENDORSEMENT_TABLE)
End Sub
'
' Finder
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataProvider.Find(DatabaseName.ENDORSEMENT_KEY, Handle, Column)
End Function


'
' Interface to server
'

'  parameterOrder='cleEventId eventName eventTopic
'eventLocation attendingStaff cleCredit eventDate
'attributeNames attributeValues parameterNames parameterValues'>

'
' Create a Endorsement
'

Public Function CreateEndorsement( _
    EndorsementId As String, EventName As String, _
    EventTopic As String, EventLocation As String, _
    AttendingStaff As String, CLECredit As Double, _
    EventDate As Date, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
  
  CreateEndorsement = Client.Create( _
    EndorsementId, EventName, _
    EventTopic, EventLocation, _
    AttendingStaff, CLECredit, _
    EventDate, _
    AttributeNames(), AttributeValues(), _
    ParameterNames(), ParameterValues())

  Exit Function
  
ErrorHandler:
  MsgBox "Fault: " & Client.faultstring, vbExclamation
  
End Function

'
' Write a Endorsement
'
Public Function WriteEndorsement( _
    EndorsementHandle As String, EndorsementId As String, EventName As String, _
    EventTopic As String, EventLocation As String, _
    AttendingStaff As String, CLECredit As Double, _
    EventDate As Date, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
  
  Call Client.write( _
    EndorsementHandle, EndorsementId, EventName, _
    EventTopic, EventLocation, _
    AttendingStaff, CLECredit, _
    EventDate, _
    AttributeNames(), AttributeValues(), _
    ParameterNames(), ParameterValues())
  
  WriteEndorsement = True
  Exit Function
  
ErrorHandler:
  MsgBox "Fault: " & Client.faultstring, vbExclamation
  WriteEndorsement = False
End Function

'
' Delete an Endorsement
'
Public Function DeleteEndorsement( _
    EndorsementHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler

  Call Client.Delete(EndorsementHandle, ParameterNames, ParameterValues)
  DeleteEndorsement = True
  Exit Function
  
ErrorHandler:
  MsgBox "Fault: " & Client.faultstring, vbExclamation
  DeleteEndorsement = False
End Function




