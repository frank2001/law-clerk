VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FolderParameterService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit




'
'
' THIS SERVICE IS DEPRECIATED
'
'


Private Client As SoapClient30
Private Const ServiceName = "FolderParameterService"
Private mFolderParameter As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  'setup data access items first (the are less likely to error)
  Set mFolderParameter = NewGenericDataset("SysFolderActionParamView")
  Set mFolderParameter.Session = LawSession
  'mFolderParameter.DataProvider.Source =


On Error GoTo Exception
 
'  'setup SOAP Client
'  Set Client = New SoapClient30
'  Client.MSSoapInit HttpServer & "component/" & ServiceName & ".wsdl"
'  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & "rpcrouter" & LawSession.Session
MsgBox "ToDo: Folder Paramater Service Initialization being called"

Exception:
'  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Sub


'
' Interface to server
'


' parameterOrder='FolderParameterId claimId FolderParameterTypeId reimburseDate FolderParameter attributeNames attributeValues parameterNames parameterValues'>


'
' Create.
'
Public Function CreateFolderParameter( _
    FolderId As String, Action As String, _
    ParameterId As String, ParameterValue As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo Exception
  
  CreateFolderParameter = Client.Create( _
      FolderId, Action, _
      ParameterId, ParameterValue, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  Exit Function
  
Exception:
'  MsgBox "Folder Parameter Create Service Error: " & Err.Description, vbExclamation
  ErrorUtil.DisplaySoapError Err, Client, ServiceName

End Function


'
' Write an application.
'
Public Function WriteFolderParameter( _
    FolderId As String, Action As String, _
    ParameterId As String, ParameterValue As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo Exception
  
  Call Client.Write( _
      FolderId, Action, _
      ParameterId, ParameterValue, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  WriteFolderParameter = True
  Exit Function
  
Exception:
'  MsgBox "Folder Parameter Write Service Error: " & Err.Description, vbExclamation
  ErrorUtil.DisplaySoapError Err, Client, ServiceName

End Function

'
' Delete an FolderParameter
'
Public Function DeleteFolderParameter( _
    FolderId As String, Action As String, ParameterId As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo Exception

  Call Client.Delete( _
    FolderId, Action, ParameterId, _
    ParameterNames, ParameterValues)
  DeleteFolderParameter = True
  Exit Function
  
Exception:
ErrorUtil.DisplaySoapError Error, Client, ServiceName
'  MsgBox "Folder Parameter Service Error: " & Err.Description, vbExclamation
End Function


'
' Parameter Dataset Helper
'
Public Function FolderParameters(FolderId As String, ActionType As Integer, Optional ExcludeDescription As Boolean) As Collection
'  mFolderParameter.DataProvider.Filter = "folderId = '" & FolderId & "' AND action = '" & ActionText(ActionType, True) & "'" & _
'      IIf(ExcludeDescription, " AND NOT parameterId = 'Description'", vbNullString)
'  mFolderParameter.DataProvider.OpenRecordset
'  Set FolderParameters = mFolderParameter.DataProvider.Collection
  MsgBox "ToDo: FolderParamaters called"
End Function


Public Function ParameterLoad(Folder As String, ActionType As Integer, ParameterNames() As String, ParameterValues() As String, Optional ExcludeDescription As Boolean) As Integer
  Dim Params As Collection, I, Count As Integer, Row As Collection
  Set Params = FolderParameters(Folder, ActionType, ExcludeDescription)
  If Params Is Nothing Then Exit Function
  
  Count = Params.Count
  ReDim ParamNames(Count - 1) As String, ParamValues(Count - 1) As String
  
  For Each Row In Params
    ParamNames(I) = Row(DatabaseName.PARAMETER_KEY)
    ParamValues(I) = Row("parameterValue")
    I = I + 1
  Next Row
    
  ParameterNames() = ParamNames()
  ParameterValues() = ParamValues()
  ParameterLoad = Count
End Function
