VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EFTDepositFinancialInstDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'ver 2.60.00 (jjm) 2004 03 15
Option Explicit

'EFT Deposit Fininacial Inst Dataset

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer

Private Sub Class_Initialize()
  ListIndex = 2
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.EFTDEPOSITFINANCIALINST_TABLE
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  frm.cboIn.AddItem "in eft deposit financial institution identifier"
  frm.cboIn.AddItem "in institution city field only"
  frm.cboIn.AddItem "in institution name field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, 2)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection

  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As String
  If Not Words = "" Then
    Select Case ListIndex
      Case 0
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.EFTDEPOSITFINANCIALINST_KEY, Words)
      Case 1
        newFilter = LawDialect.ContainsAndArray(DatabaseName.INSTITUTIONCITY_FIELD, Words)
      Case 2
        newFilter = LawDialect.ContainsAndArray(DatabaseName.INSTITUTIONNAME_FIELD, Words)
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function


