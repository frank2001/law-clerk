VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.ocx"
Begin VB.Form frmSelectLawAreaDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Law Area"
   ClientHeight    =   5352
   ClientLeft      =   36
   ClientTop       =   312
   ClientWidth     =   6996
   Icon            =   "frmSelectLawArea.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5352
   ScaleWidth      =   6996
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   4704
      Left            =   48
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   48
      Width           =   6888
      _ExtentX        =   12150
      _ExtentY        =   8297
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   420
      TabCaption(0)   =   "Law Area"
      TabPicture(0)   =   "frmSelectLawArea.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lvAOP"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "txtAOP"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cboAOP"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      Begin VB.ComboBox cboAOP 
         Height          =   288
         ItemData        =   "frmSelectLawArea.frx":0028
         Left            =   3924
         List            =   "frmSelectLawArea.frx":0035
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   1512
      End
      Begin VB.TextBox txtAOP 
         Height          =   288
         Left            =   5508
         TabIndex        =   2
         Top             =   360
         Width           =   1116
      End
      Begin MSComctlLib.ListView lvAOP 
         Height          =   4248
         Left            =   72
         TabIndex        =   0
         Top             =   360
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   7493
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "%"
         Height          =   192
         Left            =   6672
         TabIndex        =   6
         Top             =   408
         Width           =   144
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   372
      Left            =   5760
      TabIndex        =   4
      Top             =   4884
      Width           =   1176
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   372
      Left            =   4488
      TabIndex        =   3
      Top             =   4884
      Width           =   1176
   End
End
Attribute VB_Name = "frmSelectLawAreaDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOk As Boolean
Private mSelectedLawAreas As New Collection

Public Property Get OK() As Boolean
  OK = mOk
End Property

Public Property Get ApplyLawArea() As Collection
  Set ApplyLawArea = mSelectedLawAreas
End Property

Public Property Set ApplyLawArea(vdata As Collection)
  Set mSelectedLawAreas = vdata
End Property

Private Sub LoadAOP()
  'Clear any old list
  lvAOP.ListItems.Clear
  lvAOP.ColumnHeaders.Clear
  lvAOP.ColumnHeaders.Add , , "AOP"
  lvAOP.ColumnHeaders(1).Width = lvAOP.Width - 296
  
  Dim LawAreas As Collection
  Set LawAreas = Factory.LawAreaService.NameCollection
  
  'Setup base
  Dim I As Integer
  With LawAreas
    For I = 1 To .Count
      lvAOP.ListItems.Add , .Item(I)(DatabaseName.LAWAREA_KEY), .Item(I)("lawAreaName")
    Next I
  End With
  
  'Add checkboxes
  Dim laItem As LawAreaItem
  If Not mSelectedLawAreas Is Nothing Then
    With mSelectedLawAreas
      For Each laItem In mSelectedLawAreas
        lvAOP.ListItems(laItem.Key).Checked = laItem.IsChecked
      Next laItem
    End With
  End If
End Sub

Private Sub RefreshConditions()
  cboAOP.Enabled = lvAOP.SelectedItem.Checked
  txtAOP.Enabled = lvAOP.SelectedItem.Checked
  
  If Contains(mSelectedLawAreas, lvAOP.SelectedItem.Key) Then
    cboAOP.ListIndex = mSelectedLawAreas(lvAOP.SelectedItem.Key).Operator
    txtAOP.Text = mSelectedLawAreas(lvAOP.SelectedItem.Key).Text
  Else
    'Set defaults
    cboAOP.ListIndex = 0
    txtAOP.Text = vbNullString
  End If
  
End Sub

Private Sub StoreValue()

  ' store data in the collection
  With mSelectedLawAreas(lvAOP.SelectedItem.Key)
    .Key = lvAOP.SelectedItem.Key
    .IsChecked = lvAOP.SelectedItem.Checked
    .Operator = cboAOP.ListIndex
    .Text = txtAOP.Text
  End With
  
End Sub

Private Sub cboAOP_Click()
  If lvAOP.SelectedItem.Checked Then
    StoreValue
  End If
End Sub

Private Sub cmdCancel_Click()
  mOk = False
  Me.Hide
End Sub

Private Sub cmdOK_Click()
  mOk = True
  Me.Hide
End Sub

Private Sub Form_Load()
  LoadAOP
  cboAOP.ListIndex = 0
  RefreshConditions
End Sub


Private Sub lvAOP_DblClick()
  lvAOP.SelectedItem.Checked = Not lvAOP.SelectedItem.Checked
  lvAOP_ItemCheck lvAOP.SelectedItem
End Sub

Private Sub lvAOP_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  lvAOP.SelectedItem = Item
  If Item.Checked Then
    mSelectedLawAreas.Add New LawAreaItem, lvAOP.SelectedItem.Key
    RefreshConditions
    StoreValue
  Else
    If Contains(mSelectedLawAreas, Item.Key) Then
      mSelectedLawAreas.Remove (lvAOP.SelectedItem.Key)
    End If
  End If
End Sub

Private Sub lvAOP_ItemClick(ByVal Item As MSComctlLib.ListItem)
  RefreshConditions
End Sub

Private Sub txtAOP_Validate(Cancel As Boolean)
  If lvAOP.SelectedItem.Checked Then
    StoreValue
  End If
End Sub
