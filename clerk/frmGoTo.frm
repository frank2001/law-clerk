VERSION 5.00
Begin VB.Form frmGoTo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Main Menu"
   ClientHeight    =   3120
   ClientLeft      =   150
   ClientTop       =   750
   ClientWidth     =   4710
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3120
   ScaleWidth      =   4710
   StartUpPosition =   3  'Windows Default
   Begin VB.Menu mnuGoto 
      Caption         =   "Go To"
      Begin VB.Menu mnuAccounting 
         Caption         =   "&Accounting"
         Begin VB.Menu mnuAccountingBatch 
            Caption         =   "&Batch"
            Begin VB.Menu mnuAccountingBatchExport 
               Caption         =   "&Export"
            End
            Begin VB.Menu mnuAccountingBatchNew 
               Caption         =   "&New"
            End
            Begin VB.Menu mnuAccountingBatchOpen 
               Caption         =   "&Open"
            End
         End
         Begin VB.Menu mnuAccountingFiscalCalendar 
            Caption         =   "&Fiscal Calendar"
         End
      End
      Begin VB.Menu mnuClaimAdministration 
         Caption         =   "&Claim"
         Begin VB.Menu mnuClaimCase 
            Caption         =   "&Case"
         End
         Begin VB.Menu mnuClaimDeductible 
            Caption         =   "&Deductible"
            Begin VB.Menu mnuClaimDeductibleEdit 
               Caption         =   "&Edit"
            End
            Begin VB.Menu mnuClaimDeductibleNew 
               Caption         =   "&New"
            End
         End
         Begin VB.Menu mnuClaimIncident 
            Caption         =   "&Incident"
         End
         Begin VB.Menu mnuClaimLetter 
            Caption         =   "&Letter"
         End
         Begin VB.Menu mnuClaimPayment 
            Caption         =   "&Payment"
            Begin VB.Menu mnuClaimPaymentEdit 
               Caption         =   "&Edit"
            End
            Begin VB.Menu mnuClaimPaymentNew 
               Caption         =   "&New"
            End
         End
         Begin VB.Menu mnuClaimReserve 
            Caption         =   "&Reserve"
            Begin VB.Menu mnuClaimReserveEdit 
               Caption         =   "&Edit"
            End
            Begin VB.Menu mnuClaimReserveNew 
               Caption         =   "&New"
            End
         End
      End
      Begin VB.Menu mnuReportAdministration 
         Caption         =   "&Report"
         Begin VB.Menu mnuReportAccounting 
            Caption         =   "&Accounting"
            Begin VB.Menu mnuReportAccountingPremium 
               Caption         =   "&Premium"
               Begin VB.Menu mnuReportAccountingPremiumAdvancePaymentDetail 
                  Caption         =   "&Advance Payment Detail"
                  Enabled         =   0   'False
               End
               Begin VB.Menu mnuReportAccountingPremiumReconciliation 
                  Caption         =   "&Reconciliation - 4099 Premium Register"
                  Enabled         =   0   'False
               End
               Begin VB.Menu mnuReportAccountingPremiumUnearnedPremium 
                  Caption         =   "&Unearned Premium"
                  Enabled         =   0   'False
               End
               Begin VB.Menu mnuReportAccountingPremiumWritten 
                  Caption         =   "&Written/Earned (including count of firm Id)"
                  Enabled         =   0   'False
               End
            End
            Begin VB.Menu mnuReportAccountingClaims1 
               Caption         =   "&Claims  "
               Begin VB.Menu mnuReportAccountingClaimsInternalClaims 
                  Caption         =   "&Internal Claims Bordereaux - Standard"
                  Begin VB.Menu mnuReportAccountingClaimsCalendar 
                     Caption         =   "&Calendar (Report) Year"
                     Enabled         =   0   'False
                  End
                  Begin VB.Menu mnuReportAccountingClaimsTreatyYear 
                     Caption         =   "&Treaty Year"
                     Enabled         =   0   'False
                  End
               End
               Begin VB.Menu mnuReportAccountingClaimsNetDedBorderaux 
                  Caption         =   "&Net of Deductible Borderaux"
               End
               Begin VB.Menu mnuReportAccountingClaimsMakeYourOwn 
                  Caption         =   "&Make your own Borderaux"
               End
               Begin VB.Menu mnuReportAccountingClaimsReconciliation 
                  Caption         =   "&Reconciliation - 5299 Claims Register"
               End
               Begin VB.Menu mnuReportAccountingClaimClaimCount 
                  Caption         =   "&Claim Count"
               End
            End
            Begin VB.Menu mnuReportMiscellaneous 
               Caption         =   "&Miscellaneous"
               Begin VB.Menu mnuMiscellaneousCashReceived 
                  Caption         =   "&Cash Received"
               End
               Begin VB.Menu mnuMiscellaneousReinsuranceRecoverable 
                  Caption         =   "&Reinsurance Recoverable"
                  Begin VB.Menu mnuMiscellaneousReinsuranceRecoverablePaid 
                     Caption         =   "&Paid"
                  End
                  Begin VB.Menu mnuMiscellaneousReinsuranceRecoverableIncurred 
                     Caption         =   "&Incurred"
                  End
               End
               Begin VB.Menu mnuMiscellaneousReinsuranceRecoverablePolicy 
                  Caption         =   "&Policy Count by Reinsurance Year"
               End
            End
            Begin VB.Menu mnuReportAccountingActuary 
               Caption         =   "&Actuary"
               Begin VB.Menu mnuReportAccountingActuaryLawyer 
                  Caption         =   "&Lawyer"
                  Begin VB.Menu mnuReportAccountingActuaryLawyerCount 
                     Caption         =   "&Count"
                  End
                  Begin VB.Menu mnuReportAccountingActuaryLawyerRatingFactors 
                     Caption         =   "&Rating Factor"
                  End
               End
               Begin VB.Menu mnuReportAccountingActuaryAuditor 
                  Caption         =   "Au&ditor"
                  Begin VB.Menu mnuReportAccountingActuaryAuditorsCashReceipts 
                     Caption         =   "&Cash Receipt"
                  End
                  Begin VB.Menu mnuReportAccountingActuaryAuditorsInceptionDateRegister 
                     Caption         =   "&Inception Date Register"
                  End
                  Begin VB.Menu mnuReportAccountingActuaryAuditorsPolicyRegister 
                     Caption         =   "&Policy Register"
                  End
               End
               Begin VB.Menu mnuReportAccountingActuaryClaim 
                  Caption         =   "&Claim"
                  Begin VB.Menu mnuReportAccountingActuaryClaimProduction 
                     Caption         =   "&Production"
                  End
                  Begin VB.Menu mnuReportAccountingClaimsProductionWeekly 
                     Caption         =   "&Weekly"
                     Begin VB.Menu mnuReportAccountingClaimsProductionWeeklyOpenClaimListing 
                        Caption         =   "&Open Claim Listing"
                     End
                  End
                  Begin VB.Menu mnuReportAccountingClaimsProductionSemiMonthly 
                     Caption         =   "&Semi-Monthly"
                     Begin VB.Menu mnuReportAccountingClaimsProductionSemiMonthlyAgedReinsuranceReport 
                        Caption         =   "&Aged Reinsurance Report"
                     End
                  End
                  Begin VB.Menu mnuReportAccountingClaimsProductionMonthly 
                     Caption         =   "&Monthly"
                     Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyClaimsOpened 
                        Caption         =   "&Claims Opened by Month/Calendar Year"
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyClaimsAttorney 
                        Caption         =   "Claims &Attorney Activity"
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyClaimsAttorneySection1 
                           Caption         =   "Section &1"
                        End
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyClaimsAttorneySection2 
                           Caption         =   "Section &2"
                        End
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyDeductible 
                        Caption         =   "&Deductible"
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyDeductibleReimbursementReport 
                           Caption         =   "&Reimbursement Report"
                        End
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyDeductibleWriteOffs 
                           Caption         =   "YTD &Write Off"
                        End
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyInternalClaimsBordereau 
                        Caption         =   "&Internal Claim Bordereau"
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyMonthlyReserveActivity 
                        Caption         =   "&Monthy Reserve Activity"
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyOpenClaim 
                        Caption         =   "&Open Claim by"
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyOpenClaimTreatyYear 
                           Caption         =   "&Treaty Year"
                        End
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyOpenClaimMonthOfDevelopment 
                           Caption         =   "&Month of Development"
                        End
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyYearlyOverall 
                        Caption         =   "&Yearly Overall"
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyYearlyOverallClaimActivity 
                           Caption         =   "&Claim Activity"
                        End
                        Begin VB.Menu mnuReportAccountingClaimsProductionMonthlyYearlyOVerallIncidentActivity 
                           Caption         =   "&Incident Activity"
                        End
                     End
                  End
                  Begin VB.Menu mnuReportAccountingClaimsProductionQuarterly 
                     Caption         =   "&Quarterly"
                     Begin VB.Menu mnuReportAccountingClaimsProductionQuarterlyLossErrorReport 
                        Caption         =   "&Loss / Error Reports- Bordereaux"
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionQuarterlyPolicyLimitReport 
                        Caption         =   "&Policy Limit Report"
                     End
                     Begin VB.Menu mnuReportAccountingClaimsProductionQuarterlyReinsuranceReport 
                        Caption         =   "Reinsurance Report - Quarterly Reserve Activity Report"
                     End
                  End
                  Begin VB.Menu mnuReportAccountingClaimsProductionAnnually 
                     Caption         =   "&Annually"
                     Begin VB.Menu mnuReportAccountingClaimsProductionAnnuallyTailClaimReport 
                        Caption         =   "&Tail Claim Report"
                     End
                  End
               End
               Begin VB.Menu mnuReportAccountingStatistics 
                  Caption         =   "&Statistics (on request)"
                  Begin VB.Menu mnuReportAccountingStatisticsAreaOfPractice 
                     Caption         =   "&Area of Practice"
                     Begin VB.Menu mnuReportAccountingStatisticsAppRange 
                        Caption         =   "&App Range vs Frequency/Severity"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsErrorType 
                        Caption         =   "&Error Type"
                        Begin VB.Menu mnuReportAccountingStatisticsErrorTypeAll 
                           Caption         =   "&All Areas of Practice"
                        End
                        Begin VB.Menu mnuReportAccountingStatisticsErrorTypeSpecific 
                           Caption         =   "&SpecificArea of Practice"
                        End
                        Begin VB.Menu mnuReportAccountingStatisticsErrorTypeFrequency 
                           Caption         =   "&Frequency && Severity"
                        End
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsFrequency 
                        Caption         =   "&Frequency"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsSeverity 
                        Caption         =   "&Severity"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsMissing 
                        Caption         =   "&Missing"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsDuplicated 
                        Caption         =   "&Duplicated"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsIndividual 
                        Caption         =   "&Individual Area of Practice"
                        Begin VB.Menu mnuReportAccountingStatisticsIndividualAppRange 
                           Caption         =   "&App Range vs Frequency && /Severity"
                        End
                        Begin VB.Menu mnuReportAccountingStatisticsIndividualFrequencyTrend 
                           Caption         =   "&Frequency Trend by Year"
                        End
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsRatioApp 
                        Caption         =   "&Ratio App && Frequency && Severity"
                     End
                  End
                  Begin VB.Menu mnuReportAccountingStatisticsIncurredValues 
                     Caption         =   "&Incurred Values by Year Claim Made"
                  End
                  Begin VB.Menu mnuReportAccountingStatisticsCounty 
                     Caption         =   "Individual Coun&ty"
                     Begin VB.Menu mnuReportAccountingStatisticsCountyArea 
                        Caption         =   "&Area of Practice"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsCountyErrorType 
                        Caption         =   "&ErrorType"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsCountyFrequencyTable 
                        Caption         =   "Frequency && Severity (&Table)"
                     End
                     Begin VB.Menu mnuReportAccountingStatisticsCountyFrequencyReport 
                        Caption         =   "Frequency && Severity (&Report)"
                     End
                  End
               End
            End
         End
      End
      Begin VB.Menu mnuUnderwritingAdministration 
         Caption         =   "&Underwriting"
         Begin VB.Menu mnuGotoApplication 
            Caption         =   "&Application"
            Begin VB.Menu mnuApplicationAddress 
               Caption         =   "&Address"
            End
            Begin VB.Menu mnuApplicationGeneralInformation 
               Caption         =   "&General Information"
            End
            Begin VB.Menu mnuApplicationIdentifyBlockOfBusiness 
               Caption         =   "&Identify Block of Business"
            End
            Begin VB.Menu mnuApplicationNew 
               Caption         =   "&New"
            End
         End
         Begin VB.Menu mnuCorrespondence 
            Caption         =   "&Correspondence"
            Begin VB.Menu mnuCorrespondenceEMail 
               Caption         =   "&E-Mail"
               Enabled         =   0   'False
            End
            Begin VB.Menu mnuCorrespondenceFax 
               Caption         =   "&Fax"
               Enabled         =   0   'False
            End
            Begin VB.Menu mnuCorrespondenceLetter 
               Caption         =   "&Letter"
               Enabled         =   0   'False
            End
         End
         Begin VB.Menu mnuFirm 
            Caption         =   "&Firm"
            Begin VB.Menu mnuFirmClaim 
               Caption         =   "&Claim"
            End
            Begin VB.Menu mnuFirmDiary 
               Caption         =   "&Diary"
            End
            Begin VB.Menu mnuFirmEdit 
               Caption         =   "&Edit"
            End
            Begin VB.Menu mnuFirmOpen 
               Caption         =   "&Open"
            End
            Begin VB.Menu mnuFirmNew 
               Caption         =   "&New"
            End
         End
         Begin VB.Menu mnuLawyer 
            Caption         =   "&Lawyer"
            Begin VB.Menu mnuLawyerClaim 
               Caption         =   "&Claim"
               Enabled         =   0   'False
            End
            Begin VB.Menu mnuLawyerCLEAttendance 
               Caption         =   "CLE &Attendance"
            End
            Begin VB.Menu mnuLawyerCLEEvent 
               Caption         =   "CLE E&vent"
            End
            Begin VB.Menu mnuLawyerFirmAffiliation 
               Caption         =   "&Firm Affiliation"
            End
            Begin VB.Menu mnuLawyerGeneralInformation 
               Caption         =   "&General Information"
            End
            Begin VB.Menu mnuLawyerNew 
               Caption         =   "&New"
            End
         End
         Begin VB.Menu mnuGotoMaintenance 
            Caption         =   "&Maintenance"
            Begin VB.Menu mnuMaintenanceAreasOfLaw 
               Caption         =   "Areas of &Law"
            End
            Begin VB.Menu mnuMaintenanceEFT 
               Caption         =   "&EFT"
               Begin VB.Menu mnuMaintenanceEFTIndividual 
                  Caption         =   "&Individual"
                  Enabled         =   0   'False
               End
               Begin VB.Menu mnuMaintenanceEFTInstitution 
                  Caption         =   "Ins&titution"
                  Enabled         =   0   'False
               End
            End
            Begin VB.Menu mnuMaintenanceIDChangeHistory 
               Caption         =   "&ID Change History"
               Begin VB.Menu mnuMaintenanceLawFirmIdChangeHistory 
                  Caption         =   "LawFirmIdChangeHistory"
                  Enabled         =   0   'False
               End
               Begin VB.Menu mnuMaintenanceLawyerIDChangeHistory 
                  Caption         =   "LawyerIdChangeHistory"
                  Enabled         =   0   'False
               End
            End
            Begin VB.Menu mnuMaintenanceEFTeInsurer 
               Caption         =   "I&nsurer"
            End
            Begin VB.Menu mnuMaintenancePaymentPlanCode 
               Caption         =   "&Payment Plan (Billing && Binding)"
            End
            Begin VB.Menu mnuMaintenanceRating 
               Caption         =   "&Rating"
            End
         End
         Begin VB.Menu mnuPolicyAdministration 
            Caption         =   "&Policy"
            Begin VB.Menu mnuPolicyAccounting 
               Caption         =   "&Accounting"
            End
            Begin VB.Menu mnuPolicyBindCoverage 
               Caption         =   "&Bind Coverage"
            End
            Begin VB.Menu mnuPolicyEndorsement 
               Caption         =   "&Endorsement"
            End
            Begin VB.Menu mnuPolicyIssuePolicy 
               Caption         =   "&Issue Policy"
            End
         End
         Begin VB.Menu mnuRating 
            Caption         =   "&Rating"
            Begin VB.Menu mnuRatingWorksheet 
               Caption         =   "&Worksheet"
            End
         End
      End
   End
End
Attribute VB_Name = "frmGoTo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mParent As frmMain
Private mParentInterface As FormInterface

'
' FormInterface Interface
'

'
' Save Parent
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set mParentInterface = ParentInterface
  Set mParent = ParentInterface.Parent
End Sub

'
' When parent is unloaded, remove reference to parent so it can terminate.
'
Public Sub FormParentUnload()
  Set mParent = Nothing
End Sub
'
' End General
'
'
'
'
' Menus
'

'
' Accounting
'

Private Sub mnuAccountingBatchExport_Click()
  mParent.FolderLoad "GLBatch", MenuName.GLBATCH_EXPORT
End Sub

Private Sub mnuAccountingBatchNew_Click()
  mParent.FolderLoad "GLBatch", MenuName.GLBATCH_OPEN
  mParent.FileNew DatabaseName.GLBATCH_TABLE
End Sub


Private Sub mnuAccountingBatchOpen_Click()
  mParent.FolderLoad "GLBatch", MenuName.GLBATCH_OPEN
End Sub

Private Sub mnuAccountingFiscalCalendar_Click()
  mParent.FolderLoad "FiscalCalendar"
End Sub

'
' Application
'

Private Sub mnuApplicationAddress_Click()
  mParent.FolderLoad DatabaseName.FIRM_TABLE, MenuName.FIRM_CONTACT
End Sub


Private Sub mnuApplicationGeneralInformation_Click()
  mParent.FolderLoad DatabaseName.APPLICATION_TABLE, MenuName.APPLICATION_OPEN
End Sub

Private Sub mnuApplicationIdentifyBlockOfBusiness_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, mParent.FormMgr, , 1
End Sub

Private Sub mnuApplicationNew_Click()
  Dispatcher.ShowNew DatabaseName.APPLICATION_TABLE, , mParent.FormMgr
End Sub



'Private Sub mnuApplicationReprint_Click()
' Code to reprint an application

' show advanced find

' from advanced find retrive application Id

' then export
'End Sub

'
' Claim Goto.
'

Private Sub mnuClaimCase_Click()
  mParent.FolderLoad DatabaseName.CLAIM_TABLE, MenuName.CLAIM_OPEN, 2
End Sub


'
' Deductible
'

Private Sub mnuClaimDeductibleEdit_Click()
  mParent.FolderLoad DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, MenuName.DEDUCTIBLEREIMBURSE_OPEN
End Sub

Public Sub mnuClaimDeductibleNew_Click()
  Dispatcher.ShowNew DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, , mParent.FormMgr
End Sub


Private Sub mnuClaimIncident_Click()
  mParent.FolderLoad DatabaseName.INCIDENT_TABLE, MenuName.INCIDENT_OPEN, 5
End Sub

'
' Claim Payment
'

Private Sub mnuClaimPaymentEdit_Click()
  mParent.FolderLoad DatabaseName.CLAIMPAYMENT_TABLE, MenuName.CLAIMPAYMENT_OPEN
End Sub

Public Sub mnuClaimPaymentNew_Click()
  Dispatcher.ShowNew DatabaseName.CLAIMPAYMENT_TABLE, , mParent.FormMgr
End Sub


'
'
'

Private Sub mnuClaimReserveEdit_Click()
  mParent.FolderLoad DatabaseName.CLAIMRESERVE_TABLE
End Sub

Public Sub mnuClaimReserveNew_Click()
  mParent.FileNew "Claim Reserve"
End Sub



'
' Firm Goto
'

Private Sub mnuFirmClaim_Click()
  mParent.FolderLoad DatabaseName.CLAIM_TABLE, MenuName.CLAIM_OPEN, 4
End Sub

Private Sub mnuFirmDiary_Click()
  mParent.FolderLoad DatabaseName.FIRM_TABLE, MenuName.FIRM_DIARY
End Sub

Private Sub mnuFirmEdit_Click()
  mParent.FolderLoad DatabaseName.FIRM_TABLE, MenuName.FIRM_CONTACT
End Sub

Private Sub mnuFirmNew_Click()
  Dispatcher.ShowNew DatabaseName.FIRM_TABLE, , mParent.FormMgr
End Sub

Private Sub mnuFirmOpen_Click()
  mParent.FolderLoad DatabaseName.FIRM_TABLE, MenuName.FIRM_OPEN
End Sub


'
'
'

Private Sub mnuLawyerCLEAttendance_Click()
  mParent.FolderKey = "CLEEventLawyer"  'Lawyer CLE Event
  mParent.txtWords.SetFocus
  mParent.cboIn.ListIndex = 1
End Sub

Private Sub mnuLawyerCLEEvent_Click()
  mParent.FolderKey = "CLEEvent"
  mParent.txtWords.SetFocus
End Sub

Private Sub mnuLawyerFirmAffiliation_Click()
  mParent.FolderKey = "FirmLawyer"
  mParent.txtWords.SetFocus
  mParent.cboIn.ListIndex = 1
End Sub

Private Sub mnuLawyerGeneralInformation_Click()
  mParent.FolderKey = "Lawyer"
  mParent.Views(mParent.FolderKey).DefaultMenuName = MenuName.LAWYER_OPEN
  mParent.txtWords.SetFocus
End Sub

Private Sub mnuMaintenancePaymentPlanCodeBilling_Click()
  mParent.FolderKey = "Policy Accounting Type"
End Sub

Private Sub mnuMaintenancePaymentPlanCodeBinding_Click()
  mParent.FolderKey = "Policy Accounting Type"
End Sub

Private Sub mnuMaintenanceRateBooik_Click()
  mParent.FolderKey = "Rating Book"
End Sub

Private Sub mnuMaintenanceRatingAdjustment_Click()
  mParent.FolderKey = "Rating Metric"
End Sub

Private Sub mnuLawyerNew_Click()
  Dispatcher.ShowNew DatabaseName.LAWYER_TABLE, , mParent.FormMgr
End Sub

Private Sub mnuPolicyAccounting_Click()
  mParent.FolderLoad "Policy", MenuName.POLICY_OPEN_ACCOUNTING
End Sub


Private Sub mnuHome_Click()
  mParent.FolderKey = "WILMIC"
End Sub


' Only ftView has different arrangements.
Public Property Set Main(ByVal vData As frmMain)
  Set mParent = vData
End Property
  
  
' Just return the form type
Public Property Get Main() As frmMain
  Set Main = mParent
End Property

Private Sub mnuPolicyBindCoverage_Click()
  mParent.FolderLoad "Coverage"
End Sub

Private Sub mnuPolicyEndorsement_Click()
  mParent.FolderLoad "Policy", MenuName.POLICY_OPEN_ENDORSEMENT
End Sub

Private Sub mnuPolicyIssuePolicy_Click()
  mParent.FolderLoad DatabaseName.POLICY_TABLE
End Sub

Private Sub mnuRatingWorksheet_Click()
  mParent.FolderLoad "ApplicationRating", MenuName.APPLICATIONRATING_BROWSEWORKSHEET
End Sub

