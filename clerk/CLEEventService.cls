VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLEEventService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "CLEEventService"
Private mDataProvider As DataProvider

'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mDataProvider = Session.NewDataProvider(DatabaseName.CLEEVENT_TABLE)
  
  
'Setup SOAP
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub
'
' Finder
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataProvider.Find(DatabaseName.CLEEVENT_KEY, Handle, Column)
End Function


'
' Interface to server
'

'  parameterOrder='cleEventId eventName eventTopic
'eventLocation attendingStaff cleCredit eventDate
'attributeNames attributeValues parameterNames parameterValues'>

'
' Create a CLEEvent
'

Public Function CreateCLEEvent( _
    CLEEventId As String, EventName As String, _
    EventTopic As String, EventLocation As String, _
    AttendingStaff As String, CLECredit As Double, _
    EventDate As Date, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
  
  CreateCLEEvent = Client.Create( _
    CLEEventId, EventName, _
    EventTopic, EventLocation, _
    AttendingStaff, CLECredit, _
    EventDate, _
    AttributeNames(), AttributeValues(), _
    ParameterNames(), ParameterValues())

  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Function

'
' Write a CLEEvent
'
Public Function WriteCLEEvent( _
    CLEEventHandle As String, CLEEventId As String, EventName As String, _
    EventTopic As String, EventLocation As String, _
    AttendingStaff As String, CLECredit As Double, _
    EventDate As Date, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
  
  Call Client.Write( _
    CLEEventHandle, CLEEventId, EventName, _
    EventTopic, EventLocation, _
    AttendingStaff, CLECredit, _
    EventDate, _
    AttributeNames(), AttributeValues(), _
    ParameterNames(), ParameterValues())
  
  WriteCLEEvent = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  WriteCLEEvent = False
End Function

'
' Delete an CLEEvent
'
Public Function DeleteCLEEvent( _
    CLEEventHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler

  Call Client.Delete(CLEEventHandle, ParameterNames, ParameterValues)
  DeleteCLEEvent = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  DeleteCLEEvent = False
End Function


