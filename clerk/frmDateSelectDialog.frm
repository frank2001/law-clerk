VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmDateSelectDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Date Select Dialog"
   ClientHeight    =   1572
   ClientLeft      =   3324
   ClientTop       =   5844
   ClientWidth     =   5652
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1572
   ScaleWidth      =   5652
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2796
      Left            =   0
      ScaleHeight     =   2796
      ScaleWidth      =   5652
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   0
      Width           =   5652
      Begin VB.CommandButton CancelButton 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   4380
         TabIndex        =   2
         Top             =   516
         Width           =   1200
      End
      Begin VB.CommandButton OKButton 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   4380
         TabIndex        =   1
         Top             =   120
         Width           =   1200
      End
      Begin MSComCtl2.DTPicker dtpDate 
         Height          =   288
         Left            =   96
         TabIndex        =   0
         Top             =   624
         Width           =   1368
         _ExtentX        =   2413
         _ExtentY        =   508
         _Version        =   393216
         Format          =   19202049
         CurrentDate     =   37439
      End
      Begin VB.Label lblDescription 
         AutoSize        =   -1  'True
         Caption         =   "Select a date:"
         Height          =   192
         Left            =   108
         TabIndex        =   4
         Top             =   168
         Width           =   984
      End
   End
End
Attribute VB_Name = "frmDateSelectDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.20.0612 (jjm)
Option Explicit

Private mOk As Boolean

Public Property Get OK() As Boolean
  OK = mOk
End Property



Private Sub CancelButton_Click()
  Hide
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , Me.hwnd
  End If
End Sub


Private Sub Form_Load()
  'Set default date to today.
  dtpDate.Value = Today
End Sub

Private Sub OKButton_Click()
  mOk = True
  Hide
End Sub


