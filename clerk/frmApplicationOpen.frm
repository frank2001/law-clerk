VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmApplicationOpen 
   Caption         =   "Application"
   ClientHeight    =   6630
   ClientLeft      =   3990
   ClientTop       =   3510
   ClientWidth     =   7695
   Icon            =   "frmApplicationOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6630
   ScaleWidth      =   7695
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5460
      Left            =   0
      ScaleHeight     =   5460
      ScaleWidth      =   7695
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   384
      Width           =   7695
      Begin VB.Frame fraNote 
         Caption         =   "Application Note"
         Height          =   945
         Left            =   84
         TabIndex        =   47
         Top             =   1272
         Width           =   4200
         Begin VB.TextBox txtNote 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   630
            Left            =   108
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   48
            TabStop         =   0   'False
            Top             =   204
            Width           =   4035
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Contact"
         Height          =   1200
         Left            =   84
         TabIndex        =   18
         Top             =   24
         Width           =   4200
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "county"
            DataField       =   "county"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   17
            Left            =   2460
            TabIndex        =   40
            Top             =   924
            UseMnemonic     =   0   'False
            Width           =   468
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "fax"
            DataField       =   "fax"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   18
            Left            =   2460
            TabIndex        =   39
            Top             =   696
            UseMnemonic     =   0   'False
            Width           =   204
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmEntityName"
            DataField       =   "firmEntityName"
            DataMember      =   "ContactFirm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   150
            TabIndex        =   26
            Top             =   195
            UseMnemonic     =   0   'False
            Width           =   1275
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "telephone"
            DataField       =   "telephone"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   3
            Left            =   2460
            TabIndex        =   25
            Top             =   444
            UseMnemonic     =   0   'False
            Width           =   720
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "displayName"
            DataField       =   "displayName"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   2
            Left            =   144
            TabIndex        =   24
            Top             =   444
            UseMnemonic     =   0   'False
            Width           =   972
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmId"
            DataField       =   "firmId"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   0
            Left            =   2244
            TabIndex        =   23
            Top             =   144
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   384
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "zip"
            DataField       =   "zip"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   9
            Left            =   2820
            TabIndex        =   22
            Top             =   120
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   204
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "state"
            DataField       =   "state"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   8
            Left            =   3456
            TabIndex        =   21
            Top             =   120
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   348
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "city"
            DataField       =   "city"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   7
            Left            =   144
            TabIndex        =   20
            Top             =   924
            UseMnemonic     =   0   'False
            Width           =   240
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "addressLine1"
            DataField       =   "addressLine1"
            DataMember      =   "ContactFirm"
            Height          =   192
            Index           =   4
            Left            =   144
            TabIndex        =   19
            Top             =   696
            UseMnemonic     =   0   'False
            Width           =   984
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Application"
         Height          =   2220
         Left            =   4344
         TabIndex        =   5
         Top             =   0
         Width           =   3252
         Begin VB.Label lblStatus 
            AutoSize        =   -1  'True
            Caption         =   "Status:"
            DataMember      =   "Application"
            Height          =   195
            Left            =   135
            TabIndex        =   52
            Top             =   1965
            UseMnemonic     =   0   'False
            Width           =   495
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventStatus"
            DataField       =   "eventStatus"
            DataMember      =   "Event"
            Height          =   195
            Index           =   6
            Left            =   1545
            TabIndex        =   51
            Top             =   1965
            UseMnemonic     =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLawyerCount 
            AutoSize        =   -1  'True
            Caption         =   "LawyerCount"
            Height          =   192
            Left            =   1560
            TabIndex        =   46
            Top             =   1728
            Width           =   924
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerOtherCount"
            DataField       =   "lawyerOtherCount"
            DataMember      =   "Application"
            Height          =   192
            Index           =   21
            Left            =   2796
            TabIndex        =   45
            Top             =   504
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   312
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerOfCounselCount"
            DataField       =   "lawyerOfCounselCount"
            DataMember      =   "Application"
            Height          =   192
            Index           =   20
            Left            =   2796
            TabIndex        =   44
            Top             =   300
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   672
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerPrincipalCount"
            DataField       =   "lawyerPrincipalCount"
            DataMember      =   "Application"
            Height          =   192
            Index           =   13
            Left            =   2796
            TabIndex        =   43
            Top             =   96
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   552
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Lawyers"
            Height          =   192
            Left            =   132
            TabIndex        =   42
            Top             =   1704
            Width           =   600
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Policy Term:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   17
            Top             =   972
            UseMnemonic     =   0   'False
            Width           =   900
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "policyTerm"
            DataField       =   "policyTerm"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Application"
            Height          =   192
            Index           =   15
            Left            =   1560
            TabIndex        =   16
            Top             =   972
            UseMnemonic     =   0   'False
            Width           =   816
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "receiveDate"
            DataField       =   "receiveDate"
            DataMember      =   "Application"
            Height          =   192
            Index           =   14
            Left            =   1560
            TabIndex        =   15
            Top             =   732
            UseMnemonic     =   0   'False
            Width           =   888
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Date Rec'd:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   14
            Top             =   732
            UseMnemonic     =   0   'False
            Width           =   852
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            DataMember      =   "Application"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   192
            Index           =   11
            Left            =   1560
            TabIndex        =   13
            Top             =   252
            UseMnemonic     =   0   'False
            Width           =   1116
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Left            =   144
            TabIndex        =   12
            Top             =   240
            UseMnemonic     =   0   'False
            Width           =   1032
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            Caption         =   "Application Status:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   11
            Top             =   1212
            UseMnemonic     =   0   'False
            Width           =   1320
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "applicationStatus"
            DataField       =   "applicationStatus"
            DataMember      =   "Application"
            Height          =   192
            Index           =   16
            Left            =   1560
            TabIndex        =   10
            Top             =   1212
            UseMnemonic     =   0   'False
            Width           =   1212
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Staff:"
            Height          =   192
            Left            =   144
            TabIndex        =   9
            Top             =   492
            UseMnemonic     =   0   'False
            Width           =   348
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "staffCount"
            DataField       =   "staffCount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Application"
            Height          =   192
            Index           =   12
            Left            =   1560
            TabIndex        =   8
            Top             =   492
            UseMnemonic     =   0   'False
            Width           =   696
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventTypeId"
            DataField       =   "eventTypeId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   10
            Left            =   1560
            TabIndex        =   7
            Top             =   1452
            UseMnemonic     =   0   'False
            Width           =   912
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Event Type:"
            DataMember      =   "Application"
            Height          =   192
            Left            =   132
            TabIndex        =   6
            Top             =   1452
            UseMnemonic     =   0   'False
            Width           =   864
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   4560
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   1368
         Width           =   1416
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3135
         Left            =   30
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   2265
         Width           =   7605
         _ExtentX        =   13414
         _ExtentY        =   5530
         _Version        =   393216
         Style           =   1
         Tabs            =   7
         TabsPerRow      =   10
         TabHeight       =   420
         TabCaption(0)   =   "Lawyer"
         TabPicture(0)   =   "frmApplicationOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Law Area"
         TabPicture(1)   =   "frmApplicationOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Limit"
         TabPicture(2)   =   "frmApplicationOpen.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Insurer"
         TabPicture(3)   =   "frmApplicationOpen.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Question"
         TabPicture(4)   =   "frmApplicationOpen.frx":007C
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "lvView(4)"
         Tab(4).ControlCount=   1
         TabCaption(5)   =   "Claim"
         TabPicture(5)   =   "frmApplicationOpen.frx":0098
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "lvView(5)"
         Tab(5).ControlCount=   1
         TabCaption(6)   =   "History"
         TabPicture(6)   =   "frmApplicationOpen.frx":00B4
         Tab(6).ControlEnabled=   0   'False
         Tab(6).Control(0)=   "lvView(6)"
         Tab(6).ControlCount=   1
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   3
            Left            =   -74904
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "ApplicationFirmInsurerView"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   2
            Left            =   -74904
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationLimit"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   1
            Left            =   -74904
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationLawArea"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   0
            Left            =   96
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationLawyer"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   4
            Left            =   -74904
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "ApplicationApplicationQuestion"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   5
            Left            =   -74904
            TabIndex        =   49
            TabStop         =   0   'False
            Tag             =   "ApplicationClaim"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   6
            Left            =   -74904
            TabIndex        =   50
            TabStop         =   0   'False
            Tag             =   "ApplicationFirmPolicy"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   688
      _CBWidth        =   7695
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinWidth1       =   795
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   990
      MinHeight2      =   330
      Width2          =   990
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   330
      Width3          =   945
      NewRow3         =   0   'False
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   6855
         ScaleHeight     =   330
         ScaleWidth      =   750
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   30
         Width           =   750
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   41
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1830
            TabIndex        =   35
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1720
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  ImageIndex      =   1
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Application"
                        Text            =   "Application"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   660
            Left            =   990
            TabIndex        =   34
            Top             =   15
            Width           =   750
            _ExtentX        =   1323
            _ExtentY        =   1164
            ButtonWidth     =   1244
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Application"
                        Text            =   "Application"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Contact"
                        Text            =   "Contact"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   660
            Left            =   0
            TabIndex        =   33
            Top             =   15
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   1164
            ButtonWidth     =   1349
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   6
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Lawyer"
                        Text            =   "Lawyer"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "LawArea"
                        Text            =   "Law Area"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Limit"
                        Text            =   "Limit"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Insurer"
                        Text            =   "Insurer"
                     EndProperty
                     BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Question"
                        Text            =   "Question"
                     EndProperty
                     BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "History"
                        Text            =   "History"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   3705
         ScaleHeight     =   330
         ScaleWidth      =   2925
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   30
         Width           =   2925
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   660
            Left            =   0
            TabIndex        =   31
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1640
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   36
      Top             =   6360
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5001
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "7/9/2010"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "12:52 PM"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   576
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   60
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Application"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator999 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Lawyer"
            Index           =   0
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "Law &Area"
            Index           =   1
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "Li&mit"
            Index           =   2
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "I&nsurer"
            Index           =   3
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Question"
            Index           =   4
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&History"
            Index           =   5
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Diary"
            Index           =   0
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Lawyer"
            Index           =   2
         End
      End
      Begin VB.Menu mnuSeparator847 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To "
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator3412 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Application"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact"
         Index           =   1
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   2
      End
      Begin VB.Menu mnuSeparator388 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator3832 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "T&ab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Lawyer"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "L&aw Area"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "Li&mit"
            Index           =   2
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "In&surer"
            Index           =   3
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Question"
            Index           =   4
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Claim"
            Index           =   5
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&History"
            Index           =   6
         End
      End
      Begin VB.Menu mnuSeparator123 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings"
      End
      Begin VB.Menu mnuSeparator2384 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuViewBrowseApplication 
         Caption         =   "&Browse Application"
         Enabled         =   0   'False
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuViewBrowseFirm 
         Caption         =   "Browse &Firm"
      End
      Begin VB.Menu mnuSeparator5673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeperator336 
         Caption         =   "-"
      End
      Begin VB.Menu mnuUpdateServicesMenu 
         Caption         =   "&Update Services"
         Begin VB.Menu mnuToolsUpdateService 
            Caption         =   "Update &Insure Duration"
            Index           =   0
         End
         Begin VB.Menu mnuToolsUpdateService 
            Caption         =   "Update Lawyer &CLE"
            Index           =   1
         End
      End
      Begin VB.Menu mnuSeperator665 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&New Application Folder Location..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator656 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
   End
End
Attribute VB_Name = "frmApplicationOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.73.00 (jjm) REF 74
'ver 2.10.0414
'previous ver 2.00.0305 (jjm)
'
' ApplicationOpen
'
'
' Any framework changes to ApplicationOpen should be propogated to the following
' forms:
' -ClaimOpen
' -FirmDiaryOpen
' -FirmOpen
' -LawyerOpen
' -PolicyOpen
' -CLEEventOpen
' -EndorsementOpen
' -EndorsementApplicationOpen

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
  
Private mApplicationHandle As String
Private mFirmHandle As String
Private mEventHandle As String

Public FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

' Detail Tabs
Private ApplicationLawyer As Dataset
Private mApplicationLawArea As Dataset
Private ApplicationLimit As Dataset
Private mApplicationQuestion As Dataset
Private mReference As Dataset
Private FirmInsurer As Dataset
Private mFirmViewData As DataProvider
Private Person As Dataset
Private mContact As Dataset
Private mClaim As Dataset
Private mFirmPolicy As Dataset

'ver 2.73.00 (jjm) REF 74
'Variable to flag if there is no receiveDate
Private isReceiveDate As Boolean

'ver 2.73.00 (jjm) REF 74
'when receiveDate is null display Application Edit
Private Sub DisplayNullReceive()
  If Not isReceiveDate Then
    'Call application edit
    mnuEdit_Click 0
  End If
End Sub


'
' FormInterface Interface
'

'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimSelect
      Dim Selected As Collection
      Dim item As ListItem
      Set Selected = ListViewSelected(fInterface.Child.lvView)
      For Each item In Selected
        Factory.FirmPolicyService.CreateFirmPolicy mFirmHandle, ListItemText(item, DatabaseName.POLICY_KEY), _
           vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
      Next item
  End Select

  Refresh
  FormRefresh
  Show
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Dim Datafields As Collection
  Set Datafields = Factory.ApplicationService.Find(mApplicationHandle)
  SetFields lblDataField, Datafields, "Application"
  
  'Custom count for lawyer count. Mantis #2433.
  lblLawyerCount.Caption = GetDouble(GetDataFromCollection(Datafields, DatabaseName.LAWYERPRINCIPALCOUNT_FIELD)) _
                         + GetDouble(GetDataFromCollection(Datafields, DatabaseName.LAWYEROFCOUNSELCOUNT_FIELD)) _
                         + GetDouble(GetDataFromCollection(Datafields, DatabaseName.LAWYEROTHERCOUNT_FIELD))
  
  mFirmHandle = GetDataFromCollection(Datafields, DatabaseName.FIRM_KEY)
  mEventHandle = GetDataFromCollection(Datafields, DatabaseName.EVENT_KEY)
  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
  
  If (mEventHandle = vbNullString) Then
    txtNote.Text = vbNullString
  Else
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    SetFields lblDataField, Factory.EventService.Find(mEventHandle), "Event"
  End If
  
  ' Set from FirmView
  SetFields lblDataField, mFirmViewData.Find(DatabaseName.FIRM_KEY, mFirmHandle), "ContactFirm"
  
  Caption = "Application (" & mApplicationHandle & ")"
  lblDataField(17).Caption = lblDataField(17).Caption & " County" 'somecounty County
  lblDataField(7).Caption = lblDataField(7).Caption & _
     ", " & lblDataField(8).Caption & "  " _
     & lblDataField(9).Caption 'city, state  zip in city field
  lblDataField(1).Caption = lblDataField(1).Caption & _
     " (" & lblDataField(0).Caption & ")" 'adds firmId to end of firmName
  lblDataField(3).Caption = lblDataField(3).Caption & " (phone)" 'add phone
  lblDataField(18).Caption = lblDataField(18).Caption & " (fax)"
  
  'Open applicationLawyer Dataset
  ApplicationLawyer.DataProvider.filter = DatabaseName.APPLICATION_KEY & " = '" & mApplicationHandle & "'"
  
  'Open mApplicationLawArea dataset
  mApplicationLawArea.DataProvider.filter = DatabaseName.APPLICATION_KEY & " = '" & mApplicationHandle & "'"

  'Open applicationLimit dataset
  ApplicationLimit.DataProvider.filter = DatabaseName.APPLICATION_KEY & " = '" & mApplicationHandle & "'"
  
  'Open insurer dataset
  FirmInsurer.DataProvider.filter = DatabaseName.FIRM_KEY & " = '" & mFirmHandle & "'"
   
  'Open mApplicationQuestion dataset
  mApplicationQuestion.DataProvider.filter = DatabaseName.APPLICATION_KEY & " = '" & mApplicationHandle & "'"
  
  'open Claim dataset
  mClaim.DataProvider.filter = DatabaseName.FIRM_KEY & " = '" & mFirmHandle & "'"

  'Open Firm Policy dataset
  mFirmPolicy.DataProvider.filter = DatabaseName.FIRM_KEY & " = '" & mFirmHandle & "'"
    
  ' Standard Navigation Update.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  SSTabRefresh
  
  ' use the total lawyer count field. Made lblLawyerCount into a lblDatafields
  'lblLawyerCount = GetDouble(lbldatafield(13)) + GetDouble(lbldatafield(20)) + GetDouble(lbldatafield(21))
  
  'ver 2.73.00 (jjm) REF 74
  isReceiveDate = GetDataFromCollection(Datafields, DatabaseName.RECEIVEDATE_FIELD) <> vbNullString
  
  Refresh
End Sub

'
' Set ListIndex from underlying view.  How we pull the field is
' dependent upon the list item type.  In this example, if we are
' opening from an event view, there is a different method for
' pulling the appliction id.
'
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case "Application"
      mApplicationHandle = ListItemText(mListView.SelectedItem, DatabaseName.APPLICATION_KEY)
    Case "Event"
      Dim ListEventHandle As String
      ListEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
      
      'first test for a reference event
      Dim ApplicationEventHandle As String
      ApplicationEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(ListEventHandle, DatabaseName.APPLICATION_TABLE)
                        
      If ApplicationEventHandle = vbNullString Then
        MsgBox "Underlying event does not reference an Application."
        Exit Property
      End If
      'pull the application handle
      mApplicationHandle = DataUtil.FindHandle(DatabaseName.APPLICATION_TABLE, _
                                               DatabaseName.EVENT_KEY, _
                                               ApplicationEventHandle, _
                                               DatabaseName.APPLICATION_KEY)
                                               
  End Select
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
  Case 0
    Set ListViewDataProvider = ApplicationLawyer.DataProvider
  Case 1
    Set ListViewDataProvider = mApplicationLawArea.DataProvider
  Case 2
    Set ListViewDataProvider = ApplicationLimit.DataProvider
  Case 3
    Set ListViewDataProvider = FirmInsurer.DataProvider
  Case 4
    Set ListViewDataProvider = mApplicationQuestion.DataProvider
  Case 5
    Set ListViewDataProvider = mClaim.DataProvider
  Case 6
    Set ListViewDataProvider = mFirmPolicy.DataProvider
  End Select
End Function

'ver 2.10.0415 (jjm)
Private Sub MenuRefresh()
  Dim IsClaimTab As Boolean
  IsClaimTab = (SSTab.Tab = 5)
  
  'Browse
  mnuEditBrowseRow.Enabled = IsClaimTab
  mnuPopupBrowse.Enabled = IsClaimTab
  
  'Delete
  mnuEditDeleteRow.Enabled = Not IsClaimTab
  mnuPopupDelete.Enabled = Not IsClaimTab
  
  'New
  mnuPopupNew.Enabled = Not IsClaimTab
  
End Sub

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = "Application")
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
  'ver 2.73.00 (jjm) REF 74
  DisplayNullReceive
End Sub

Public Sub ShowByPrimaryKey(ApplicationId As String, Optional ShowTab As Integer = 0)
  mApplicationHandle = ApplicationId
  mTab = ShowTab
  Show
  'ver 2.73.00 (jjm) REF 74
  DisplayNullReceive
End Sub

Public Sub SSTabRefresh()
  Dim Source As DataProvider, View As ListView, Index As Variant
  Set View = lvView(mTab)
  Set Source = ListViewDataProvider(mTab)
  
  With View
    'Ensure first tab is open.
    If SSTab.Tab = mTab Then
      If .ListItems.Count > 0 Then
        Index = IIf(.SelectedItem.Key = "", .SelectedItem.Index, .SelectedItem.Key)
        .Visible = False
        .SelectedItem.Selected = False
        ListViewLoad View, Source
        On Error Resume Next
        .ListItems(Index).Selected = True
        .Visible = True
        .SetFocus
      Else
        ListViewLoad View, Source
      End If
    Else
      SSTab.Tab = mTab
    End If
     
    MenuRefresh
    
    mnuToolsUpdateService(1).Enabled = ((mTab = 0) And (lvView(0).ListItems.Count > 0))
     
    sbStatusBar.Panels(1).Text = str(.ListItems.Count) + " object(s)"
    sbStatusBar.Panels(2).Text = Source.Source
  
    ' Custom extension for calculating law area total.
    If mTab = 1 Then
      Dim Total As Integer, item As Variant
      For Each item In .ListItems
        Total = item.ListSubItems("lawAreaPercent") + Total
      Next
      sbStatusBar.Panels(1).Text = sbStatusBar.Panels(1).Text & " - " & Total & "%"
    End If
  End With
End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
End Sub
'
' End General Methods
'
'
'
'
' Begin Controls
'

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Activate()
  Form_Resize
  SSTabResize
End Sub

Private Sub Form_Initialize()
  'Initialize the ContactFirm Dataset
  Set mFirmViewData = LawSession.NewDataProvider(DatabaseName.FIRM_VIEW)
  
  'Initialize the ApplicationLawyer Dataset
  Set ApplicationLawyer = New ApplicationLawyerDataset
  Set ApplicationLawyer.Session = LawSession
   
  'Initialize ApplicationLawArea
  Set mApplicationLawArea = NewGenericDataset(DatabaseName.APPLICATIONLAWAREA_VIEW)
  
  'Initialize the Limit Dataset
  Set ApplicationLimit = NewGenericDataset(DatabaseName.APPLICATIONLIMIT_VIEW)
     
  'Initialize the Person Dataset
  Set Person = New PersonDataset
  Set Person.Session = LawSession
   
  'Initialize the Insurer Dataset
  Set FirmInsurer = NewGenericDataset(DatabaseName.FIRMINSURER_VIEW)
   
  'Initialize the mApplicationQuestion Dataset
  Set mApplicationQuestion = NewGenericDataset(DatabaseName.APPLICATIONQUESTION_VIEW)
  
  Set mContact = NewGenericDataset(DatabaseName.CONTACT_VIEW)
   
  'Event for scanning through event records.
  Set mReference = New ReferenceDataset
  Set mReference.Session = LawSession
  
  Set mClaim = NewGenericDataset(DatabaseName.CLAIM_VIEW)
  
  Set mFirmPolicy = NewGenericDataset(DatabaseName.FIRMPOLICY_VIEW)
   
End Sub

'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyL
        mnuFileNewDetail_Click 0
      Case vbKeyA
        mnuFileNewDetail_Click 1
      Case vbKeyM
        mnuFileNewDetail_Click 2
      Case vbKeyI
        mnuFileNewDetail_Click 3
      Case vbKeyQ
        mnuFileNewDetail_Click 4
      Case vbKeyH
        mnuFileNewDetail_Click 5
    End Select
    Exit Sub
  End If
  
  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
 '     mnuFileNewDetail_Click SSTab.Tab' wont work with history
      Select Case SSTab.Tab
        Case 0:   'Lawyer
          mnuFileNewDetail_Click 0
        Case 1:   'Law Area
          mnuFileNewDetail_Click 1
        Case 2:   'Limit
          mnuFileNewDetail_Click 2
        Case 3:   'Insurer
          mnuFileNewDetail_Click 3
        Case 4:   'Question
          mnuFileNewDetail_Click 4
        Case 6:   'History
          mnuFileNewDetail_Click 5
      End Select
  End Select

End Sub


Private Sub Form_Load()
  'Load to from to previous position
  FormUtil.DeserializePosition Me
  
  ' Setup Toolbar.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.Height + 24

  FormUtil.EditLoadImages Me
  FormUtil.MenuLoad Me
  
  ' Custom Menu Shortcut Adjustments
  mnuFileNewDetail(0).Caption = mnuFileNewDetail(0).Caption + Chr(9) + "Ctrl+Shift+L"
  mnuFileNewDetail(1).Caption = mnuFileNewDetail(1).Caption + Chr(9) + "Ctrl+Shift+A"
  mnuFileNewDetail(2).Caption = mnuFileNewDetail(2).Caption + Chr(9) + "Ctrl+Shift+M"
  mnuFileNewDetail(3).Caption = mnuFileNewDetail(3).Caption + Chr(9) + "Ctrl+Shift+I"
  mnuFileNewDetail(4).Caption = mnuFileNewDetail(4).Caption + Chr(9) + "Ctrl+Shift+Q"
  mnuFileNewDetail(5).Caption = mnuFileNewDetail(5).Caption + Chr(9) + "Ctrl+Shift+H"
  
  mnuEditEditRow.Caption = mnuEditEditRow.Caption + Chr(9) + "Enter"
    
  ' Setup Form Manager
  FormMgr.Load Me
  
  FormRefresh
  
  
End Sub

' Expand body
Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)
  mnuEdit_Click 1
End Sub

Private Sub Frame4_DblClick()
  mnuEdit_Click 0
End Sub

Private Sub fraNote_DblClick()
  If mnuToolsNotepad.Enabled Then
    mnuToolsNotepad_Click
  Else
    MsgBox "Please add initial note using the Edit, Firm menu."
  End If
End Sub


Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0 'Application
      FormUtil.ShowByPrimaryKey New frmApplicationEdit, mApplicationHandle, FormMgr
    
    Case 1 'Contact
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowByFirm mFirmHandle, "Firm"

    Case 2 'Firm
      FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mFirmHandle, FormMgr
  End Select
End Sub

'ver 2.10.0414
Private Sub mnuEditBrowseRow_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass

  Select Case SSTab.Tab
    Case 5
      Browse HttpServer & WebService & UrlName.CLAIMOPEN_SERVLET + ListItemText(lvView(5).SelectedItem, DatabaseName.CLAIM_KEY)
  End Select
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If
  
  If lvView(SSTab.Tab).ListItems.Count = 0 Then
    MsgBox "There are no items to delete."
    Exit Sub
  End If
  
  Select Case SSTab.Tab
    Case 0: 'Lawyer
      With lvView(0)
        Call Factory.ApplicationLawyerService.DeleteApplicationLawyer( _
            ListItemText(.SelectedItem, DatabaseName.APPLICATION_KEY), _
            ListItemText(.SelectedItem, DatabaseName.LAWYER_KEY))
      End With
    Case 1: 'LawArea
      With lvView(1)
        Call Factory.ApplicationLawAreaService.DeleteApplicationLawArea( _
            ListItemText(.SelectedItem, DatabaseName.APPLICATION_KEY), _
            ListItemText(.SelectedItem, DatabaseName.LAWAREA_KEY))
      End With
    Case 2: 'Limit
      With lvView(2)
        Call Factory.ApplicationLimitService.DeleteApplicationLimit( _
            ListItemText(.SelectedItem, DatabaseName.APPLICATIONLIMIT_KEY), _
            vbNullStringArray, vbNullStringArray)
      End With
    Case 3: 'Insurer
      With lvView(3)
        Call Factory.FirmInsurerService.DeleteFirmInsurer( _
            ListItemText(.SelectedItem, DatabaseName.FIRM_KEY), _
            ListItemText(.SelectedItem, DatabaseName.INSURER_KEY), _
            ListItemText(.SelectedItem, "effectiveDate"), _
            vbNullStringArray, vbNullStringArray)
      End With
    Case 4: 'Question
      With lvView(4)
        Call Factory.ApplicationQuestionService.DeleteApplicationQuestion( _
            ListItemText(.SelectedItem, DatabaseName.APPLICATION_KEY), ListItemText(.SelectedItem, DatabaseName.QUESTION_KEY), _
            vbNullStringArray, vbNullStringArray)
      End With
    Case 6: 'History
      Factory.FirmPolicyService.DeleteFirmPolicy mFirmHandle, ListItemText(lvView(6).SelectedItem, DatabaseName.POLICY_KEY), vbNullStringArray, vbNullStringArray
  End Select
  FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case 0:   'Lawyer
      FormUtil.ShowByListView New frmApplicationLawyerEdit, FormMgr, lvView(0)
    Case 1:   'Law Area
      FormUtil.ShowByListView New frmApplicationLawAreaEdit, FormMgr, lvView(1)
    Case 2:    'Limit
      FormUtil.ShowByListView New frmApplicationLimitEdit, FormMgr, lvView(2)
    Case 3:    'Insurer
      'Hack for WILMIC's FirmInsurerView. The value "WILMIC__" is only found in the view.
      If "WILMIC__" = ListItemText(lvView(3).SelectedItem, DatabaseName.INSURER_KEY) Then
        MsgBox "You can not edit previous insurer information for WILMIC"
        Exit Sub
      End If
      FormUtil.ShowByListView New frmFirmInsurerEdit, FormMgr, lvView(3)
    Case 4:    'Question
      FormUtil.ShowByListView New frmApplicationQuestionEdit, FormMgr, lvView(4)
    Case 5:    'Claim
      FormUtil.ShowByListView New frmClaimOpen, FormMgr, lvView(5)
    Case 6:    'History
      FormUtil.ShowByListView New frmPolicyOpen, FormMgr, lvView(6)
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub


Private Sub mnuFileNewDetail_Click(Index As Integer)
  Select Case Index
    Case 0 'Lawyer
      Dim fForm As New frmApplicationLawyerEdit
      FormMgr.Add fForm
      fForm.ShowNew mApplicationHandle
    Case 1 'Law Area
      Dim fForm1 As New frmApplicationLawAreaEdit
      FormMgr.Add fForm1
      fForm1.ShowNew mApplicationHandle
    Case 2 'Limit
      Dim fForm2 As New frmApplicationLimitEdit
      FormMgr.Add fForm2
      fForm2.ShowNew mApplicationHandle
    Case 3 'Insurer
      Dim fForm3 As New frmFirmInsurerEdit
      FormMgr.Add fForm3
      fForm3.ShowNew mFirmHandle
    Case 4 'Question
      Dim fForm4 As New frmApplicationQuestionEdit
      FormMgr.Add fForm4
      fForm4.ShowNew mApplicationHandle
    Case 5 'History
      Dispatcher.ShowSearch DatabaseName.POLICY_TABLE, FormMgr, True
  End Select
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmFirmDiaryOpen, mFirmHandle, FormMgr
    Case 1
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
    Case 2
      Dim fForm As New frmOpenDialog
      
      'Set the tag for the list view  on the form so the View can be loaded
      fForm.lvView.Tag = lvView(0).Tag
      'Load the form
      ListViewLoad fForm.lvView, ApplicationLawyer.DataProvider, True
      
      'Fill the combobox
      fForm.cboLookIn.AddItem DatabaseName.APPLICATIONLAWYER_TABLE
      fForm.cboLookIn.ListIndex = 0
            
      fForm.Show vbModal
      
      'if the form was closed through the open button, or a double click
      'on the listview the Ok property will = true. If so, load the next form.
      If fForm.Ok Then
        FormUtil.ShowByPrimaryKey New frmLawyerOpen, ListItemText(fForm.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
      End If
      
      'Unload the form. Code on the form only hides it.
      Unload fForm
  End Select
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.APPLICATION_REF, DatabaseName.APPLICATION_TABLE, mApplicationHandle, mApplicationHandle
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  'ver 2.73.00 (jjm) REF 74
  'The ShowEventDialog is shown modaly so code is paused till
  'the proccess is completed. Grab new data and test to see if it is complete.
  Dim e As Collection
  Set e = Factory.EventService.Find(mEventHandle, DatabaseName.EVENTSTATUS_FIELD)
  If e(DatabaseName.EVENTSTATUS_FIELD) = "Complete" Then
    mnuFileExit_Click
  End If
End Sub


Private Sub mnuFileNew_Click()
  FormUtil.ShowNew New frmApplicationEdit, FormMgr, mFirmHandle
End Sub

'
' Edit/Insert Routines
'

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.APPLICATION_TABLE, mApplicationHandle
  End If
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.APPLICATION_TABLE, FormMgr
End Sub

Private Sub mnuPopupBrowse_Click()
  mnuEditBrowseRow_Click
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNewDetail_Click SSTab.Tab
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsUpdateService_Click(Index As Integer)
  Dim Key() As String
  Select Case Index
    Case 0 'Insure Duration
      If MsgBox("Are you sure you want to update insure duration?", vbYesNo) = vbYes Then
        ReDim Key(0) As String
        Key(0) = mApplicationHandle
        
        Factory.PersistenceManagerService.ExecuteRuleSession _
          "com.lawworkgroup.lom.Application", Key, "ApplicationInsureDuration", _
          vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
      End If
      
    Case 1 'Lawyer CLE
      Dim SelCount As Integer
      SelCount = ListViewModule.SelectedCount(lvView(0).hWnd)
      
      If Not (SelCount > 0 And mTab = 0) Then
        Exit Sub
      End If
      
      If MsgBox("Are you sure you want to update lawyer CLE for selected lawyers?", vbYesNo) = vbYes Then
        ReDim Key(1) As String
        Dim Lawyers() As String
        ReDim Lawyers(SelCount - 1) As String
        
        Lawyers = ListViewModule.ListItemArray(lvView(0), DatabaseName.LAWYER_KEY)
        Key(0) = mApplicationHandle
        
        Dim I As Integer
        For I = 0 To SelCount - 1
          Key(1) = Lawyers(I)
          Factory.PersistenceManagerService.ExecuteRuleSession _
            "com.lawworkgroup.lom.ApplicationLawyer", Key, "ApplicationLawyerCLE", _
            vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
        Next I
      End If

  End Select
End Sub

Private Sub mnuToolsWorkflowOptions_Click()

  Dim fFolderList As New frmFolderListDialog
  'read files from ini and load into the list view
  With fFolderList.lvFolder
    .ListItems.Add , "Application", "Application"
    .ListItems("Application").ListSubItems.Add , "Location", IniMain.ReadFile("application", "applicationfolder")
  End With
  
  ListViewModule.AutosizeWidth fFolderList.lvFolder
  fFolderList.Show vbModal, Me
  
  If fFolderList.Ok Then
    'write folders to ini
    IniMain.WriteFile "application", "applicationfolder", fFolderList.lvFolder.ListItems("Application").ListSubItems("Location")
  End If
  
  Unload fFolderList

End Sub

Private Sub mnuViewBrowseApplication_Click()
  'Browse HttpServer & WebService & "applicationopen?key=" + mApplicationHandle
End Sub

Private Sub mnuViewBrowseFirm_Click()
  Dispatcher.BrowseFirmOpen mFirmHandle
End Sub

'
' Standard choose columns for the current tab's list view.
'
Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'
' Standard view next menu
'
Private Sub mnuViewNext_Click()
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

'
' Standard view previous menu
'
Private Sub mnuViewPrevious_Click()
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

'
' Standard view refresh menu
'
Public Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

'
' Standard view a specific tab
'
Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

'
' Standard save settings
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

'
' Standard view toolbar.
'
Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  SSTabResize
End Sub

Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

Private Sub picMenu_Resize()
  tbrInsert.Top = (picMenu.Height - tbrInsert.Height) / 2
  tbrEdit.Top = (picMenu.Height - tbrEdit.Height) / 2
  tbrBrowse.Top = (picMenu.Height - tbrBrowse.Height) / 2
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  mTab = SSTab.Tab
  SSTabRefresh

  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
  
End Sub

Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub


Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEdit_Click 0
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case SSTab.Caption
    Case "Lawyer"
      mnuFileNewDetail_Click 0
    Case "Law Area"
      mnuFileNewDetail_Click 1
    Case "Limit"
      mnuFileNewDetail_Click 2
    Case "Insurer"
      mnuFileNewDetail_Click 3
    Case "Question"
      mnuFileNewDetail_Click 4
    Case "History"
      mnuFileNewDetail_Click 5
  End Select
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNewDetail_Click ButtonMenu.Index - 1
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuEdit_Click ButtonMenu.Index - 1
End Sub


Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1
      ' application browse code here
    Case 2
      mnuViewBrowseFirm_Click
  End Select
End Sub
