VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCLEEventLawyerEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CLE Lawyer Edit"
   ClientHeight    =   3465
   ClientLeft      =   5715
   ClientTop       =   3315
   ClientWidth     =   5640
   Icon            =   "frmCLELawyerEdit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   3465
   ScaleWidth      =   5640
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2988
      Left            =   0
      ScaleHeight     =   2985
      ScaleWidth      =   5640
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   384
      Width           =   5640
      Begin TabDlg.SSTab SSTab1 
         Height          =   1992
         Left            =   60
         TabIndex        =   11
         Top             =   456
         Width           =   5508
         _ExtentX        =   9710
         _ExtentY        =   3519
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "CLE"
         TabPicture(0)   =   "frmCLELawyerEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraCLEEvent"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         Begin VB.Frame fraCLEEvent 
            Caption         =   "Event"
            Height          =   1560
            Left            =   72
            TabIndex        =   21
            Top             =   324
            Width           =   3228
            Begin VB.Label lblEventLocation 
               AutoSize        =   -1  'True
               Caption         =   "Event Location:"
               Height          =   192
               Left            =   132
               TabIndex        =   30
               Top             =   1020
               Width           =   1092
            End
            Begin VB.Label lblEventID 
               AutoSize        =   -1  'True
               Caption         =   "Event ID:"
               Height          =   192
               Left            =   132
               TabIndex        =   29
               Top             =   540
               Width           =   636
            End
            Begin VB.Label lblEVentDate 
               AutoSize        =   -1  'True
               Caption         =   "Event Date:"
               Height          =   192
               Left            =   132
               TabIndex        =   28
               Top             =   780
               Width           =   828
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "eventDate"
               DataField       =   "eventDate"
               DataMember      =   "CLEEvent"
               Height          =   192
               Index           =   0
               Left            =   1296
               TabIndex        =   27
               Top             =   780
               Width           =   744
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "cleEventId"
               DataField       =   "cleEventId"
               DataMember      =   "CLEEvent"
               Height          =   192
               Index           =   1
               Left            =   1296
               TabIndex        =   26
               Top             =   540
               Width           =   756
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "eventName"
               DataField       =   "eventName"
               DataMember      =   "CLEEvent"
               Height          =   192
               Index           =   3
               Left            =   132
               TabIndex        =   25
               Top             =   240
               Width           =   840
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "eventLocation"
               DataField       =   "eventLocation"
               DataMember      =   "CLEEvent"
               Height          =   192
               Index           =   4
               Left            =   1296
               TabIndex        =   24
               Top             =   1020
               Width           =   1008
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "cleCredit"
               DataField       =   "cleCredit"
               DataMember      =   "CLEEvent"
               Height          =   192
               Index           =   6
               Left            =   1308
               TabIndex        =   23
               Top             =   1260
               Width           =   636
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Credit:"
               Height          =   192
               Left            =   132
               TabIndex        =   22
               Top             =   1260
               Width           =   456
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "Lawyer Credit"
            Height          =   1260
            Left            =   3360
            TabIndex        =   12
            Top             =   324
            Width           =   2064
            Begin VB.Label lblThru 
               AutoSize        =   -1  'True
               Caption         =   "Thru:"
               Height          =   192
               Left            =   132
               TabIndex        =   20
               Top             =   504
               Width           =   360
            End
            Begin VB.Label lblFrom 
               AutoSize        =   -1  'True
               Caption         =   "From:"
               Height          =   192
               Left            =   132
               TabIndex        =   19
               Top             =   264
               Width           =   408
            End
            Begin VB.Label lblMaxDate 
               AutoSize        =   -1  'True
               Caption         =   "Max Date:"
               Height          =   192
               Left            =   132
               TabIndex        =   18
               Top             =   732
               Width           =   720
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "beginDate"
               DataField       =   "beginDate"
               DataMember      =   "CLEEventLawyer"
               Height          =   192
               Index           =   7
               Left            =   912
               TabIndex        =   17
               Top             =   264
               Width           =   756
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "endDate"
               DataField       =   "endDate"
               DataMember      =   "CLEEventLawyer"
               Height          =   192
               Index           =   8
               Left            =   912
               TabIndex        =   16
               Top             =   504
               Width           =   624
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "maxDate"
               DataField       =   "maxDate"
               DataMember      =   "CLEEventLawyer"
               Height          =   192
               Index           =   9
               Left            =   912
               TabIndex        =   15
               Top             =   732
               Width           =   648
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Is Used:"
               Height          =   192
               Left            =   132
               TabIndex        =   14
               Top             =   960
               Width           =   588
            End
            Begin VB.Label lblDatafield 
               Caption         =   "isUsed"
               DataField       =   "isUsed"
               DataMember      =   "CLEEventLawyer"
               Height          =   156
               Index           =   5
               Left            =   912
               TabIndex        =   13
               Top             =   960
               Width           =   756
            End
         End
      End
      Begin VB.CommandButton cmdLawyer 
         Caption         =   "&Lawyer"
         Height          =   288
         Left            =   4560
         TabIndex        =   8
         Top             =   108
         Width           =   1000
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   3084
         TabIndex        =   7
         Top             =   2592
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1812
         TabIndex        =   5
         Top             =   2592
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   4356
         TabIndex        =   6
         Top             =   2592
         Width           =   1200
      End
      Begin VB.CommandButton cmdEnter 
         Caption         =   "Enter"
         Height          =   288
         Left            =   1464
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   996
         Width           =   1056
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Lawyer Name:"
         Height          =   192
         Left            =   120
         TabIndex        =   10
         Top             =   156
         Width           =   1032
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "entityName"
         DataField       =   "entityName"
         DataMember      =   "Entity"
         Height          =   195
         Index           =   2
         Left            =   1320
         TabIndex        =   9
         Top             =   150
         UseMnemonic     =   0   'False
         Width           =   795
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5640
      _ExtentX        =   9948
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   5640
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   5520
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   30
         Width           =   5520
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   564
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&CLE Event Lawyer"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator636 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewLawyer 
            Caption         =   "&Lawyer"
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditCLEEvent 
         Caption         =   "&CLE Event"
      End
      Begin VB.Menu mnuEditLawyer 
         Caption         =   "&Lawyer"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator997 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About..."
      End
   End
End
Attribute VB_Name = "frmCLEEventLawyerEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mCLEEventLawyerData As DataProvider
Private mCLEEventData As DataProvider

Private mCLEEventHandle As String
Private mLawyerHandle As String
Private mNew As Boolean
Private mFirmHandle As String
'Private mDataSet As Dataset

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private FormMgr As New FormManager

Private LawyerData As Dataset
'Private cleEventData As Dataset

Private mRefresh As Boolean





'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Public Sub CLEEventRefresh()
  'Set CLEEventLawyer Data
  Dim mDatafields As Collection
  
  Set mDatafields = Factory.CLEEventService.Find(mCLEEventHandle)
  SetFields lbldatafield, mDatafields, "CLEEvent"
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
      Exit Sub
    
    Case fimNew
      mnuFileNewLawyer_Click
      Exit Sub
      
    Case fimSelect
      mLawyerHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
      Caption = "Edit Lawyer (" & mLawyerHandle & ")"
      LawyerRefresh
      Dirty = True
      
    Case fimSave
      Select Case fInterface.Key
        Case DatabaseName.CLEEVENT_TABLE & "Edit"
          mCLEEventHandle = fInterface.Child.CLEEventHandle
        Case DatabaseName.LAWYER_TABLE
          mLawyerHandle = fInterface.Child.LawyerHandle
      End Select
      
      mNew = False
      FormRefresh
      Dirty = True
  
  End Select
  Show
  
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  Dim mDatafields As Collection
  
  pointer.pointer = vbHourglass
  
  ClearDataField lbldatafield
  
  If (mNew) Then
    Caption = "New CLE Lawyer"
    
    'The CLEEvent data will be the same
    Set mDatafields = Factory.CLEEventService.Find(mCLEEventHandle)
    SetFields lbldatafield, mDatafields, "CLEEvent"
  Else
    Caption = "Edit CLE Lawyer (" & mLawyerHandle & ")"
      
    'Set CLEEventLawyer data
    Set mDatafields = Factory.CLEEventLawyerService.Find(mCLEEventHandle, mLawyerHandle)
    SetFields lbldatafield, mDatafields, "CLEEventLawyer"
    
    LawyerRefresh
    CLEEventRefresh
    
  End If
  
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  Dirty = False
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

' End FormInterface
Private Sub LawyerRefresh()
  'Set Lawyer data
  Dim mDatafields As Collection
  
  Set mDatafields = Factory.EntityService.Find(mLawyerHandle, DatabaseName.ENTITYNAME_FIELD)
  SetFields lbldatafield, mDatafields, "Entity"
  'FirmHandle is used for the "new" functionality
  mFirmHandle = GetDataFromCollection(Factory.LawyerService.Find(mLawyerHandle, DatabaseName.LATESTFIRMID_FIELD), DatabaseName.LATESTFIRMID_FIELD)
End Sub


Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mCLEEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.CLEEVENT_KEY)
  mLawyerHandle = ListItemText(mListView.SelectedItem, DatabaseName.LAWYER_KEY)
End Property

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Begin Custom Code
  If mLawyerHandle = vbNullString Then
    MsgBox "You must select a lawyer in order to save"
    Exit Function
  End If
  
  Dim mDataControls As New Collection
  Dim Used As Boolean
  AddCaptionControl lbldatafield, mDataControls
  Used = Not (mDataControls("isUsed") = vbNullString)
   
  If mNew Then
    Dim Handle As Variant
    Handle = Factory.CLEEventLawyerService.CreateCLEEventLawyer( _
                mCLEEventHandle, mLawyerHandle, _
                GetDate(mDataControls("beginDate")), GetDate(mDataControls("endDate")), _
                GetDate(mDataControls("maxDate")), Used, _
                vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
    
    save = Not VarType(Handle) = vbEmpty
    If save Then
      mNew = False
      mLawyerHandle = Handle(1)
    End If
    
  Else
    save = Factory.CLEEventLawyerService.WriteCLEEventLawyer( _
                mCLEEventHandle, mLawyerHandle, _
                mCLEEventHandle, mLawyerHandle, _
                GetDate(mDataControls("beginDate")), GetDate(mDataControls("endDate")), _
                GetDate(mDataControls("maxDate")), Used, _
                vbNullStringArray, vbNullStringArray, _
                vbNullStringArray, vbNullStringArray)
  End If
  
  Dirty = Not save
End Function


Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no lawyers to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub


Public Sub ShowNew(CLEEventId As String)
  mCLEEventHandle = CLEEventId
  mNew = True
  Show
  Refresh
  cmdLawyer.SetFocus
  cmdLawyer_Click
  
End Sub


Public Sub ShowByPrimaryKey(CLEEventId As String, LawyerId As String)
  mCLEEventHandle = CLEEventId
  mLawyerHandle = LawyerId
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

'
' Control/Object Events
'

'
' Standard Cancel Command Button.
'
Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  PostMessage hWnd, WM_KEYDOWN, vbKeyTab, 0
End Sub

'
' Select a lawyer.
'
Private Sub cmdLawyer_Click()
  Dispatcher.ShowSearch DatabaseName.LAWYER_TABLE, FormMgr, True
End Sub

Private Sub cmdSave_Click()
  If save Then Unload Me
End Sub

'Private Sub cmdSaveNew_Click()
'  If Save Then mnuFileNew_Click
'End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdSaveNew_Click()
  If save Then
    mnuFileNew_Click
  End If
End Sub

Private Sub Form_Initialize()
'  Set mCLEEventLawyerData = LawSession.NewDataProvider(DatabaseName.CLEEVENTLAWYER_TABLE)
'  Set mCLEEventData = LawSession.NewDataProvider(DatabaseName.CLEEVENT_TABLE)

'   Set LawyerData = New LawyerDataset
'   Set LawyerData.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , Me.hWnd
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lblDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub mnuEditCLEEvent_Click()
  FormUtil.ShowByPrimaryKey New frmCLEEventEdit, mCLEEventHandle, FormMgr, , DatabaseName.CLEEVENT_TABLE & "Edit"
End Sub

Private Sub mnuEditLawyer_Click()
  FormUtil.ShowByPrimaryKey New frmPersonEdit, mLawyerHandle, FormMgr, , "EditLawyer"
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not save(True) Then
    Exit Sub
  End If
  
  mNew = True
  mLawyerHandle = vbNullString
  mnuViewRefresh_Click
  cmdLawyer.SetFocus
  cmdLawyer_Click
End Sub

Private Sub mnuFileNewLawyer_Click()
  If Not save(True) Then
    Exit Sub
  End If
  Dim fPerson As New frmPersonEdit
  FormMgr.Add fPerson
  fPerson.ShowNew True, mFirmHandle
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.CLEEVENTLAWYER_TABLE, DatabaseName.CLEEVENT_KEY, DatabaseName.LAWYER_KEY, mCLEEventHandle, mLawyerHandle
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

