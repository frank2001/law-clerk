VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ApplicationLimitService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "ApplicationLimitService"
Private mDataSet As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.APPLICATIONLIMIT_TABLE)
  
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.APPLICATIONLIMIT_KEY, Handle, Column)
End Function


'
' Interface to server
'

'
' Create an application.
'
Public Function CreateApplicationLimit( _
    ApplicationId As String, _
    PerClaimLimit As Double, _
    AggregateLimit As Double, _
    PerClaimDeductible As Double, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As String
    
On Error GoTo ErrorHandler
  
  CreateApplicationLimit = Client.Create( _
      ApplicationId, _
      PerClaimLimit, _
      AggregateLimit, _
      PerClaimDeductible, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Function

'
' Write an application.
'
Public Function WriteApplicationLimit( _
    ApplicationLimitId As String, _
    PerClaimLimit As Long, _
    AggregateLimit As Long, _
    PerClaimDeductible As Long, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo ErrorHandler
  
  Call Client.Write( _
      ApplicationLimitId, _
      PerClaimLimit, _
      AggregateLimit, _
      PerClaimDeductible, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  WriteApplicationLimit = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  'MsgBox "Fault: " & Client.FaultString, vbExclamation
  WriteApplicationLimit = False
End Function

'
' Delete an ApplicationLimit
'
Public Function DeleteApplicationLimit(ApplicationLimitId As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler
  Call Client.Delete(ApplicationLimitId, ParameterNames, ParameterValues)
  DeleteApplicationLimit = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  'MsgBox "Fault: " & Client.FaultString, vbExclamation
  DeleteApplicationLimit = False
End Function
