Attribute VB_Name = "UrlName"
' ver 2.30.0622 (jjm)
' ver 2.10.0421 (jjm)

'
' url Names
'

'
' This is a constant library for strings and other items
' use in some form of location identifing. Ex: file name,
' a section name in an ini file, ect...
'


'
' Servlet Parameters.
' used with http://localhost:8080/malcolm/servlet/%SERVLET%&%PARAMETER%
' example: http://localhost:8080/malcolm/servlet/claimopen?key=01-0001

'ToDo: Not for sure if Application open is functioning...
Public Const APPLICATIONOPEN_SERVLET = "applicationopen?key="

Public Const CLAIMOPEN_SERVLET = "claimopen?key=" '[claimId] ex: 01-0001
Public Const CLAIMRESERVEOPEN_SERVLET = "claimreserveopen?key=" '[claimReserveId] ex:CR1001808

Public Const LAWYEROPEN_SERVLET = "lawyeropen?key=" '[lawyerId] ex:BUNNBUG_
Public Const LAWYERPOLICYOPEN_SERVLET = "lawyerpolicyopen?key=" '[lawyerId]&policyid=[policyId]   ex:BUNNBUG_&policyid=0303001

Public Const FIRMOPEN_SERVLET = "firmopen?key=" '[firmId] ex: DOEJOH

Public Const POLICYOPEN_SERVLET = "policyopen?key="



'
' INI File Sections
'
Public Const BARCODE_INI = "BarCode"

Public Const CRYSTALREPORTS_INI = "crystalreports"
Public Const CRYSTAL_DRIVERNAME_INI = "CrystalDriverName"
Public Const CRYSTAL_DATABASENAME_INI = "CrystalDatabaseName"
Public Const CRYSTAL_USER_INI = "CrystalUser"
Public Const CRYSTAL_PASSWORD_INI = "CrystalPassword"

Public Const CONFDEFAULTPATH_INI = "ConfDefaultPath"
Public Const CONFPERSONALPATH_INI = "ConfPersonalPath"
Public Const CURRENTX_INI = "CurrentX"
Public Const CURRENTY_INI = "CurrentY"

Public Const DEBUGADVANCEDSEARCH_INI = "DebugAdvancedSearch"
Public Const DEFAULTTOFIRMPOLICYCONTACT_INI = "DefaultToFirmPolicyContact"

Public Const ENTERASTAB_INI = "EnterAsTab"
Public Const ENABLEWEBCONTENT_INI = "EnableWebContent"
Public Const ENVELOPE_INI = "Envelope"
Public Const ENVELOPEPRINTER_INI = "EnvelopePrinter"

Public Const FOLDER_INI = "Folder"
Public Const FOLDERKEY_INI = "FolderKey"
Public Const FOLDERWATCHKEY_INI = "FolderWatchKey"
Public Const FONTNAME_INI = "FontName"
Public Const FONTSIZE_INI = "FontSize"
Public Const FONTBOLD_INI = "FontBold"
Public Const FONTITALIC_INI = "FontItalic"
Public Const FONTUNDERLINE_INI = "FontUnderline"
Public Const FONTSTRIKETHROUGH_INI = "FontStrikethrough"
Public Const FORECOLOR_INI = "ForeColor"
Public Const FRMMAIN_INI = "frmMain"

Public Const HOME_INI = "home"

Public Const IMAGEFOLDERPATH_INI = "ImageFolderPath"
Public Const INIFOLDER_INI = "IniFolder"
Public Const INIFORM_INI = "IniForm"
Public Const INSTANCEDIARYSEARCHVIEW_INI = "instancediarysearchview"
Public Const ITEMSPERVIEW_INI = "ItemsPerView"

Public Const MAIN_INI = "Main"
Public Const MERGEPATH_INI = "mergepath"
Public Const MERGEPREFIX_INI = "mergeprefix"
Public Const MERGESUFFIX_INI = "mergesuffix"
Public Const NARRATIVEPATH_INI = "narrativepath"
Public Const NARRATIVEPREFIX_INI = "narrativeprefix"
Public Const NARRATIVESUFFIX_INI = "narrativesuffix"
Public Const MISC_INI = "misc"

Public Const ONLINE_APP_PDF_INI = "onlineapplicationpdfurl"
Public Const ORGANIZATIONLONG_INI = "OrganizationLong"
Public Const ORGANIZATIONSHORT_INI = "OrganizationShort"

Public Const PAPERSIZE_INI = "PaperSize"
Public Const PREMIUMQUOTEREPORTPATH_INI = "PremiumQuoteReportPath"
Public Const PROFILELIST_INI = "ProfileList"

Public Const REMEMBERLASTSEARCH_INI = "RememberLastSearch"

Public Const SERVERLIST_INI = "ServerList"
Public Const SYSTEMFOLDERS_TREENAME_INI = "SystemFoldersTreeName"
Public Const SYSTEMPREFERENCES_TREENAME_INI = "SystemPreferencesTreeName"

Public Const TEXTSELECTONFOCUS_INI = "TextSelectOnFocus"
Public Const TREE_INI = "Tree"

'
' Image file names
'

Public Const BACK_IMAGE = "Back.bmp"
Public Const BACKDISABLED_IMAGE = "BackDisabled.bmp"

Public Const DOWN_IMAGE = "Down.bmp"
Public Const DOWNDISABLED_IMAGE = "DownDisabled.bmp"

Public Const FORWARD_IMAGE = "Forward.bmp"
Public Const FORWARDDISABLED_IMAGE = "ForwardDisabled.bmp"

Public Const LISTVIEW_IMAGE = "ListView.bmp"

Public Const TVFOLDER_IMAGE = "TVFolder.bmp"
Public Const TVOPENFOLDER_IMAGE = "TVOpenFolder.bmp"
Public Const LAWFOLDER_IMAGE = "LAWFolder.bmp"

Public Const UP_IMAGE = "Up.bmp"
Public Const UPDISABLED_IMAGE = "UpDisabled.bmp"
Public Const UPONELEVEL_IMAGE = "UpOneLevel.bmp"
Public Const UPONELEVELDISABLED_IMAGE = "UpOneLevelDisabled.bmp"


'
' Search Constants
'

Public Const CONTACTSEARCH_CLAIMNUMBER_INDEX = 0
Public Const CONTACTSEARCH_CONTACTTYPEID_INDEX = 1
Public Const CONTACTSEARCH_CONTACTTYPENAME_INDEX = 2
Public Const CONTACTSEARCH_PERSONENTITYNAME_INDEX = 3
Public Const CONTACTSEARCH_INCIDENTNUMBER_INDEX = 4
Public Const CONTACTSEARCH_LOCATIONID_INDEX = 5
Public Const CONTACTSEARCH_ORGANIZATIONENTITYNAME_INDEX = 6
Public Const CONTACTSEARCH_ORGANIZATIONID_INDEX = 7
Public Const CONTACTSEARCH_PERSONID_INDEX = 8

Public Const CLAIMSEARCH_ADJUSTER_INDEX = 0
Public Const CLAIMSEARCH_CLAIMANT_INDEX = 1
Public Const CLAIMSEARCH_FIRMNAME_INDEX = 2
Public Const CLAIMSEARCH_CLAIMNUMBER_INDEX = 3
Public Const CLAIMSEARCH_FIRMID_INDEX = 4
Public Const CLAIMSEARCH_FIRMENTITYLOCATOR_INDEX = 5
Public Const CLAIMSEARCH_POLICYID_INDEX = 6
Public Const CLAIMSEARCH_POLICYNUMBER_INDEX = 6

Public Const COVERAGESEARCH_APPLICATIONID_INDEX = 0
Public Const COVERAGESEARCH_APPLICATIONLIMITID_INDEX = 1
Public Const COVERAGESEARCH_QUOTEID_INDEX = 2
Public Const COVERAGESEARCH_COVERAGEID_INDEX = 3
Public Const COVERAGESEARCH_FIRMENTITYLOCATOR_INDEX = 4
Public Const COVERAGESEARCH_FIRMID_INDEX = 5
Public Const COVERAGESEARCH_POLICYID_INDEX = 6

Public Const ENTITYSEARCH_ENTITYID_INDEX = 0
Public Const ENTITYSEARCH_ENTITYLOCATOR_INDEX = 1
Public Const ENTITYSEARCH_ENTITYNAME_INDEX = 2

Public Const FIRMLAWYERSEARCH_FIRMID_INDEX = 0
Public Const FIRMLAWYERSEARCH_FIRMLOCATOR_INDEX = 1
Public Const FIRMLAWYERSEARCH_LAWYERID_INDEX = 2
Public Const FIRMLAWYERSEARCH_LAWYERLOCATOR_INDEX = 3

Public Const FIRMSEARCH_CITY_INDEX = 0
Public Const FIRMSEARCH_FIRMID_INDEX = 1
Public Const FIRMSEARCH_FIRMLOCATOR_INDEX = 2
Public Const FIRMSEARCH_FIRMNAME_INDEX = 3

Public Const INCIDENTSEARCH_ADJUSTER_INDEX = 0
Public Const INCIDENTSEARCH_CLAIMNUMBER_INDEX = 1
Public Const INCIDENTSEARCH_CLAIMANT_INDEX = 2
Public Const INCIDENTSEARCH_FIRMIDENTIFIER_INDEX = 3
Public Const INCIDENTSEARCH_INCIDENTNUMBER_INDEX = 4
Public Const INCIDENTSEARCH_FIRMNAME_INDEX = 5
Public Const INCIDENTSEARCH_POLICYNUMBER_INDEX = 6
Public Const INCIDENTSEARCH_TORTFEASOR_INDEX = 7

Public Const INSTANCELAWAREASEARCH_CLAIMNUMBER_INDEX = 0
Public Const INSTANCELAWAREASEARCH_INCIDENTNUMBER_INDEX = 1
Public Const INSTANCELAWAREASEARCH_LAWAREAID_INDEX = 2

Public Const INSTANCELAWERRORSEARCH_CLAIMNUMBER_INDEX = 0
Public Const INSTANCELAWERRORSEARCH_INCIDENTNUMBER_INDEX = 1
Public Const INSTANCELAWERRORSEARCH_LAWERRORID_INDEX = 2

Public Const INSTANCELAWYERSEARCH_CLAIMNUMBER_INDEX = 0
Public Const INSTANCELAWYERSEARCH_INCIDENTNUMBER_INDEX = 1
Public Const INSTANCELAWYERSEARCH_LAWYERID_INDEX = 2

Public Const INSURERSEARCH_ENTITYLOCATOR_INDEX = 0
Public Const INSURERSEARCH_ENTITYNAME_INDEX = 1
Public Const INSURERSEARCH_INSURERID_INDEX = 2

Public Const LAWYERSEARCH_LASTESTFIRMID = 0
Public Const LAWYERSEARCH_LAWYERID_INDEX = 1
Public Const LAWYERSEARCH_LAWYERLOCATOR_INDEX = 2
Public Const LAWYERSEARCH_LAWYERNAME_INDEX = 3
Public Const LAWYERSEARCH_STATEBARNUMBER_INDEX = 4

Public Const LOCATIONSEARCH_CITY_INDEX = 0
Public Const LOCATIONSEARCH_COMPANYNAME_INDEX = 1
Public Const LOCATIONSEARCH_ENTITYID_INDEX = 2
Public Const LOCATIONSEARCH_ENTITYLOCATOR_INDEX = 3
Public Const LOCATIONSEARCH_LOCATIONID_INDEX = 4

Public Const ORGANIZATIONSEARCH_ENTITYLOCATOR_INDEX = 0
Public Const ORGANIZATIONSEARCH_ENTITYNAME_INDEX = 1
Public Const ORGANIZATIONSEARCH_ORGANIZATIONID_INDEX = 2

Public Const PERSONSEARCH_LAWYERLOCATOR_INDEX = 0
Public Const PERSONSEARCH_LASTNAME_INDEX = 1
Public Const PERSONSEARCH_ORGANIZATIONID_INDEX = 2
Public Const PERSONSEARCH_PERSONENTITYNAME = 3
Public Const PERSONSEARCH_PERSONID_INDEX = 4

Public Const QUOTESEARCH_APPLICATIONID_INDEX = 0
Public Const QUOTESEARCH_FIRMID_INDEX = 1
Public Const QUOTESEARCH_FIRMLOCATOR_INDEX = 2
Public Const QUOTESEARCH_QUOTEID_INDEX = 3

'
' System Folder Keys (folderId)
'
Public Const CLAIMLAWAREA_FOLDERKEY = "ClaimLawArea"
Public Const CLAIMLAWERROR_FOLDERKEY = "ClaimLawError"
Public Const CLAIMLAWYER_FOLDERKEY = "ClaimLawyer"

