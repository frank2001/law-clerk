VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FirmViewDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.60.00 (jjm) 2004 03 23
'ver 2.40.1124 (jjm) WILMIC
'ver 2.10.0421 (jjm)
'ver 2.00.0311 (jjm)
Option Explicit
Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private mSQLDataProvider As DataProvider

Private Words As String
Private ListIndex As Integer


Private Sub Class_Initialize()
  ListIndex = 2
End Sub

Public Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.FIRM_VIEW
  
  'Backwards compatible data provider
  Set mSQLDataProvider = vData.NewSQLDataProvider
  mSQLDataProvider.Source = DatabaseName.FIRM_VIEW
End Property


Public Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Public Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property

Public Property Get SQLDataProvider() As DataProvider
  Set SQLDataProvider = mSQLDataProvider
End Property


Public Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in city field only"
  frm.cboIn.AddItem "in firm identifier field only"
  frm.cboIn.AddItem "in firm entity locator field only"
  frm.cboIn.AddItem "in firm entity name field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, UrlName.FIRMSEARCH_FIRMLOCATOR_INDEX)
End Sub

Public Sub FirmSearchFilter(Main As frmFirmSearch, newFilter As Filter)
     Dim InsureFilter As New Filter
  Dim I As Integer
  For I = 0 To Main.chkInsureStatus.Count - 1
    If Main.chkInsureStatus(I).Value = 1 Then
      InsureFilter.AddOr """insureStatusId"" = '" & Main.chkInsureStatus(I).Tag & "'"
    End If
  Next I
  'ver 2.40.1124 (jjm) WILMIC
  If InsureFilter <> vbNullString Then
    newFilter.Add "(" & InsureFilter.Filter & ")"
  End If
  
  If Main.cboExpirationMonth.ListIndex > 0 Then
    newFilter.Add "('" & Main.cboExpirationMonth.ListIndex & "' = " & LawSQL.Month("nextEffectiveDate") & ")"
  End If
  
  'ver 2.60.00 (jjm) 2004 03 23 - Revereted to old code, previous
  'search that overtaxed the server. The overtaxing code is commented
  'out below. It involved too many nested sql statments on too large
  'of tables.
  Dim UnderwritingFilter As New Filter
  For I = 0 To Main.chkUnderwritingStatus.Count - 1
    If Main.chkUnderwritingStatus(I).Value = 1 Then
      UnderwritingFilter.AddOr """eventTypeId"" = '" & Main.chkUnderwritingStatus(I).Tag & "'"
    End If
  Next I
  
  'ver 2.00.0311 (jjm)
  If UnderwritingFilter <> "" Then
    'Ensure that the voided Items are not included
    newFilter.Add """" & DatabaseName.EVENT_KEY & """ " & IIf(Main.optExcludeOnly(0), "not", "") & " in " & _
        "(select """ & DatabaseName.REFERENCEEVENTID_FIELD & """" & _
        " from " & _
        """" & "EventReferenceDownView" & """" & _
        " WHERE """ & DatabaseName.VOIDDATE_FIELD & """ is null " & _
        " and (" & UnderwritingFilter & "))"
  End If
    
  If Main.chkClaimCount = vbChecked Then
    newFilter.Add """totalClaimCount"" " & GetOperator(Main.cboClaimCountOperator.ListIndex) & " '" & Main.txtClaimCount & "'"
  End If

  If Main.chkLawyerCount = vbChecked Then
    newFilter.Add """totalLawyerCount"" " & GetOperator(Main.cboLawyerCountOperator.ListIndex) & " '" & Main.txtLawyerCount & "'"
  End If

  If Main.chkExpirationDate = vbChecked Then
    newFilter.Add """expirationDate"" > " & LawSQL.DateFormat(Main.dtpFromExpireDate.Value) & " and ""expirationDate"" < " & LawSQL.DateFormat(Main.dtpToExpireDate.Value)
  End If

  If Main.chkCounty = vbChecked Then
    newFilter.Add """locationId"" in " & _
    "(select ""locationId"" from ""Location"" where " & LawSQL.UCase("county") & " LIKE '" & UCase(Main.txtCounty) & "%')"
  End If
  
  If Main.chkCity = vbChecked Then
     newFilter.Add """locationId"" in " & _
    "(select ""locationId"" from ""Location"" where " & LawSQL.UCase("city") & " LIKE '" & UCase(Main.txtCity) & "%')"
  End If

  If Main.chkSelectStatus = vbChecked Then
    Dim mStatus As Collection
    Dim StatusItem As ListItem
    Dim UnderFilter As New Filter
    
    Set mStatus = Main.UnderwritingStatus
    If Not mStatus Is Nothing Then
      For Each StatusItem In mStatus
        UnderFilter.AddOr """underwritingStatusId"" = '" & StatusItem.Key & "' "
      Next StatusItem
      newFilter.Add "(" & UnderFilter.Filter & ")"
    End If
  End If

  If Main.chkMultiInsurerStatus = vbChecked Then
    Dim mInsurerStatus As Collection
    Dim StatusItems As ListItem
    Dim InsurerFilter As New Filter
    
    Set mInsurerStatus = Main.InsurerStatus
    If Not mInsurerStatus Is Nothing Then
      For Each StatusItems In mInsurerStatus
        InsurerFilter.AddOr """insureStatusId"" = '" & StatusItems.Key & "'"
      Next StatusItems
      newFilter.Add "(" & InsurerFilter.Filter & ")"
    End If
  End If

  If Main.chkLawArea = vbChecked Then
    Dim LawAreaFilter As New Filter
    Dim mLawArea As Collection
    Dim LawItem As LawAreaItem
    ' Get the select law areas from the form
    Set mLawArea = Main.LawArea
    If Not mLawArea Is Nothing Then
      'Create a LawArea filter
      For Each LawItem In mLawArea
        LawAreaFilter.AddOr """lawAreaId"" = '" & LawItem.Key & "' " & _
          IIf(LawItem.Text = vbNullString, vbNullString, "and ""lawAreaPercent"" " & GetOperator(LawItem.Operator) & " '" & LawItem.Text & "' ")
      Next LawItem
      'add lawArea filter to the filter
      If Not LawAreaFilter.Filter = vbNullString Then
        newFilter.Add """" & DatabaseName.LATESTAPPLICATIONID_FIELD & """ in " & _
          "(select """ & DatabaseName.APPLICATION_KEY & """ as """ & DatabaseName.LATESTAPPLICATIONID_FIELD & """" & _
           "from """ & DatabaseName.APPLICATIONLAWAREA_TABLE & """ where " & LawAreaFilter.Filter & ")"
      End If
    End If
  End If
  
  'ver 2.50.00 (jjm) 2004 02 19
  If Main.chkInsurerName.Value = vbChecked Then
    MsgBox "TODO: Fix Search"
'    newFilter.Add """" & DatabaseName.INSURERNAME_FIELD & """ like '" & "%" & UCase(Main.txtInsurerName) & "%'"
  End If


End Sub

' Sets lvView.  Returns a collection of key fields. 'Why is this a function?
Public Function Dataset_FindByForm(Main As Form) As Collection
  'hack for old SQL functionality
  If TypeOf Main Is frmFirmSearch Then
    FindByFormSQL Main
    Exit Function
  End If
  
  
  Words = Main.txtWords
  ListIndex = Main.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case UrlName.FIRMSEARCH_CITY_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.CITY_FIELD, Words)
      Case UrlName.FIRMSEARCH_FIRMID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.FIRM_KEY, Words)
      Case UrlName.FIRMSEARCH_FIRMLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.FIRMENTITYLOCATOR_FIELD, Words)
      Case UrlName.FIRMSEARCH_FIRMNAME_INDEX
        'newFilter.Add LawDialect.Contains(LawDialect.UpperCase(DatabaseName.FIRMENTITYNAME_FIELD), UCase(Words))
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.FIRMENTITYNAME_FIELD, Words)
        'Dim WordParts() As String, I As Integer
        'WordParts = Split(Words, " ")
        'For I = 0 To SafeUBound(WordParts)
        '  newFilter.Add LawDialect.Contains(LawDialect.UpperCase(DatabaseName.FIRMENTITYNAME_FIELD), UCase(WordParts(I)))
        'Next I
    End Select
  End If
  
  'MsgBox newFilter
  
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView Main.lvView
End Function

Private Sub FindByFormSQL(Main As Form)
  Words = Main.txtWords
  ListIndex = Main.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case UrlName.FIRMSEARCH_CITY_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.CITY_FIELD, Words)
      Case UrlName.FIRMSEARCH_FIRMID_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.FIRM_KEY, Words)
      Case UrlName.FIRMSEARCH_FIRMLOCATOR_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.FIRMENTITYLOCATOR_FIELD, Words)
      Case UrlName.FIRMSEARCH_FIRMNAME_INDEX
        newFilter.Add LawSQL.UCaseWildCard(DatabaseName.FIRMENTITYNAME_FIELD, Words)
    End Select
  End If
  If TypeOf Main Is frmFirmSearch Then
    FirmSearchFilter Main, newFilter
  End If
  
  mSQLDataProvider.Filter = newFilter
  mSQLDataProvider.OpenRecordset
  mSQLDataProvider.SetListView Main.lvView
End Sub
