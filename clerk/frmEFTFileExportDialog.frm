VERSION 5.00
Begin VB.Form frmEFTFileExportDialog 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "EFT Transmission File Export"
   ClientHeight    =   1944
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   6648
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1944
   ScaleWidth      =   6648
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtPath 
      BackColor       =   &H8000000F&
      Height          =   288
      Left            =   72
      Locked          =   -1  'True
      TabIndex        =   6
      Text            =   "Text2"
      Top             =   1104
      Width           =   5100
   End
   Begin VB.TextBox txtFile 
      BackColor       =   &H8000000F&
      Height          =   288
      Left            =   72
      Locked          =   -1  'True
      TabIndex        =   5
      Text            =   "eftFileId"
      Top             =   432
      Width           =   5088
   End
   Begin VB.CommandButton cmdFolder 
      Caption         =   "&Folder"
      Height          =   336
      Left            =   72
      TabIndex        =   4
      Top             =   1524
      Width           =   1200
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancel"
      Height          =   336
      Left            =   5376
      TabIndex        =   1
      Top             =   516
      Width           =   1200
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   336
      Left            =   5376
      TabIndex        =   0
      Top             =   132
      Width           =   1200
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Location:"
      Height          =   192
      Left            =   72
      TabIndex        =   3
      Top             =   864
      Width           =   648
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "File:"
      Height          =   192
      Left            =   72
      TabIndex        =   2
      Top             =   192
      Width           =   300
   End
End
Attribute VB_Name = "frmEFTFileExportDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim mOk As Boolean

Public Property Get Ok() As Boolean
  Ok = mOk
End Property

Private Sub CancelButton_Click()
  mOk = False
  Me.Hide
End Sub

Private Sub cmdFolder_Click()
  Dim tmpStr As String
  tmpStr = BrowseForFolderAPI.BrowseFolders(hWnd, "Select a folder to export the EFT File to:", BrowseForFolders, CSIDL_DESKTOP)
  If tmpStr <> vbNullString Then
    txtPath.Text = tmpStr
  End If
End Sub

Private Sub OKButton_Click()
  'Ensure that there is an export path
  If txtPath.Text = vbNullString Then
    MsgBox "A folder path for the export file needs to be selected before the file can be exported.", vbCritical
    Exit Sub
  End If
  
  mOk = True
  Me.Hide
End Sub
