VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Dataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Datasource"
Attribute VB_Ext_KEY = "Member1" ,"Session"
Attribute VB_Ext_KEY = "Member2" ,"Session"
' Dataset Interface
'
' This is the interface used by the components to interface with data.

Option Explicit

Public Property Set Session(ByVal vData As Session)
End Property

Public Property Get Session() As Session
End Property

Public Property Get DataProvider() As DataProvider
End Property

Public Sub LoadSearch(Form As Form)
End Sub

Public Function FindByForm(Form As Form) As Collection
End Function
