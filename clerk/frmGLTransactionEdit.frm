VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmGLTransactionEdit 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "New GL Transaction"
   ClientHeight    =   2460
   ClientLeft      =   30
   ClientTop       =   315
   ClientWidth     =   6660
   Icon            =   "frmGLTransactionEdit.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2460
   ScaleWidth      =   6660
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      BorderStyle     =   0  'None
      Height          =   2484
      Left            =   12
      ScaleHeight     =   2490
      ScaleWidth      =   7815
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   48
      Width           =   7812
      Begin VB.CommandButton cmdHelp 
         Caption         =   "&Help"
         Height          =   336
         Left            =   5388
         TabIndex        =   16
         Top             =   1980
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         CausesValidation=   0   'False
         Height          =   336
         Left            =   4104
         TabIndex        =   6
         Top             =   1980
         Width           =   1200
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   2796
         TabIndex        =   5
         Top             =   1980
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   1884
         Index           =   0
         Left            =   36
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   0
         Width           =   6552
         _ExtentX        =   11562
         _ExtentY        =   3334
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "GL"
         TabPicture(0)   =   "frmGLTransactionEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label5"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label4"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label3"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label2"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label6"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "mskDataField(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtDataField(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtDataField(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtDataField(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtDataField(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboDescription"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         Begin VB.ComboBox cboDescription 
            DataField       =   "description"
            Height          =   288
            ItemData        =   "frmGLTransactionEdit.frx":0028
            Left            =   924
            List            =   "frmGLTransactionEdit.frx":002A
            Style           =   2  'Dropdown List
            TabIndex        =   15
            Top             =   660
            Width           =   2976
         End
         Begin VB.TextBox txtDataField 
            BackColor       =   &H8000000B&
            DataField       =   "name"
            DataMember      =   "GLTransaction"
            Height          =   288
            Index           =   1
            Left            =   732
            Locked          =   -1  'True
            TabIndex        =   1
            TabStop         =   0   'False
            Top             =   1308
            Width           =   2592
         End
         Begin VB.TextBox txtDataField 
            BackColor       =   &H8000000B&
            DataField       =   "reference"
            DataMember      =   "GLTransaction"
            Height          =   288
            Index           =   2
            Left            =   4560
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   1308
            Width           =   1308
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   288
            Index           =   3
            Left            =   3996
            TabIndex        =   3
            Top             =   660
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            Height          =   288
            Index           =   4
            Left            =   5268
            TabIndex        =   4
            Top             =   660
            Width           =   1200
         End
         Begin MSMask.MaskEdBox mskDataField 
            Bindings        =   "frmGLTransactionEdit.frx":002C
            DataField       =   "glAccountId"
            Height          =   288
            Index           =   0
            Left            =   96
            TabIndex        =   0
            Top             =   660
            Width           =   720
            _ExtentX        =   1270
            _ExtentY        =   503
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   4
            Mask            =   ">9999"
            PromptChar      =   "_"
         End
         Begin VB.Label Label6 
            Caption         =   "&Name:"
            Height          =   204
            Left            =   960
            TabIndex        =   14
            Top             =   348
            Width           =   960
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Account:"
            Height          =   192
            Left            =   156
            TabIndex        =   13
            Top             =   348
            Width           =   612
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Debit:"
            Height          =   192
            Left            =   4476
            TabIndex        =   12
            Top             =   360
            Width           =   696
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Reference:"
            Height          =   192
            Left            =   3612
            TabIndex        =   11
            Top             =   1332
            Width           =   792
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Name:"
            Height          =   192
            Left            =   156
            TabIndex        =   10
            Top             =   1332
            Width           =   480
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Credit:"
            Height          =   192
            Left            =   5736
            TabIndex        =   9
            Top             =   372
            Width           =   720
         End
      End
   End
End
Attribute VB_Name = "frmGLTransactionEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mListView As ListView
Private isEdit As Boolean
Private mOk As Boolean
Private mGLAccounts As Collection
Private mGroup As String

Private Function DescriptionRefresh() As Boolean
  If mskDataField(0).Text = "____" Then
    cboDescription.ListIndex = -1
    DescriptionRefresh = True
    Exit Function
  End If

  If mGLAccounts Is Nothing Then
    MsgBox "Unable to load GL Accounts."
    Exit Function
  End If
  
  
  Dim I, ListIndex As Integer
  For I = 1 To mGLAccounts.Count
    If mGLAccounts(I)(DatabaseName.GLACCOUNT_KEY) = mskDataField(0).Text Then
      ListIndex = I
      Exit For
    End If
  Next I
  If ListIndex = 0 Then
    MsgBox "Invalid general ledger identifier"
    Exit Function
  End If
  cboDescription.ListIndex = ListIndex - 1
  DescriptionRefresh = True
End Function
 
 
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


Public Property Let Group(ByVal vData As String)
  mGroup = vData
End Property


Public Property Set ListView(ByVal vData As ListView)
  Set mListView = vData
End Property


Public Property Get Ok() As Boolean
  Ok = mOk
End Property


Private Sub cboDescription_Click()
  If cboDescription.ListIndex < 0 Then
    mskDataField(0).Text = "____"
  Else
    mskDataField(0).Text = mGLAccounts(cboDescription.ListIndex + 1)(DatabaseName.GLACCOUNT_KEY)
  End If
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub cmdOk_Click()
  If Not FormUtil.ValidateControls(Me) Then
    Exit Sub
  End If
  
  Dim xItem As ListItem
  Dim fieldCount, n As Integer
  
  fieldCount = txtDataField.Count
  
'  For n = 0 To fieldCount - 1
'    If txtDataField(n).Text = "" Then
'      MsgBox "You must enter a value for " & txtDataField(n).DataField & "!", vbOKOnly
'      Exit Sub
'    End If
'  Next n
  
  If mskDataField(0).Text = vbNullString Then
    MsgBox "You must enter a value for the G/L Account."
    Exit Sub
  End If
  
  Dim Value As Double
     
  If isEdit Then
    Set xItem = mListView.SelectedItem
    xItem.Text = mskDataField(0).Text
    xItem.ListSubItems("transactionName").Text = txtDataField(1).Text
    xItem.ListSubItems("transactionReference").Text = txtDataField(2).Text
    
    Value = GetDouble(txtDataField(4))
    If Value > 0 Then
      xItem.ListSubItems("transactionAmount").Text = 0 - Value
    Else
      xItem.ListSubItems("transactionAmount").Text = GetDouble(txtDataField(3))
    End If
    xItem.ListSubItems("~debit").Text = txtDataField(3)
    xItem.ListSubItems("~credit").Text = txtDataField(4)
    
  Else
    ' Make sure G/L account is unique???
    If Not mListView.FindItem(mskDataField(0).Text) Is Nothing Then
      MsgBox "Cannot add G/L account.  Account already exists."
      Exit Sub
    End If
    
    Value = GetDouble(txtDataField(4))
    Set xItem = GLTransactionAdd(mListView, _
      mskDataField(0).Text, txtDataField(1).Text, txtDataField(2).Text, _
      IIf(Value > 0, 0 - Value, GetDouble(txtDataField(3))))
  End If
  
  mOk = True
  Unload Me
End Sub

Public Sub Edit()
  Dim xItem As ListItem
  Set xItem = mListView.SelectedItem
  
  isEdit = True
  
  mskDataField(0).Text = xItem
  txtDataField(1).Text = xItem.ListSubItems("transactionName")
  txtDataField(2).Text = xItem.ListSubItems("transactionReference")
  txtDataField(3).Text = xItem.ListSubItems("~debit")
  txtDataField(4).Text = xItem.ListSubItems("~credit")
      
  DescriptionRefresh

  Me.Caption = "GL Transaction Edit"
End Sub


Private Sub Form_Initialize()
  isEdit = False
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  '
  ' Populate EventTypeName Dropdown
  '
  Set mGLAccounts = Factory.GLAccountService.GroupCollection(mGroup)
  'custum load for combo box
  If Not mGLAccounts Is Nothing Then
    Dim I As Integer, DisplayText As String
    For I = 1 To mGLAccounts.Count
      DisplayText = GetDataFromCollection(mGLAccounts(I), DatabaseName.GLACCOUNT_KEY, True) & " - " & _
                    GetDataFromCollection(mGLAccounts(I), DatabaseName.ACCOUNTNAME_FIELD, True)
      cboDescription.AddItem DisplayText
    Next I
  End If
  
  FormUtil.DeserializePosition Me
  FormResize
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
End Sub

Private Sub mskDataField_Change(Index As Integer)
'  Dirty = True
End Sub

Private Sub mskDataField_Validate(Index As Integer, Cancel As Boolean)
  If mskDataField(0) = "__" Then
    Exit Sub
  End If
  Cancel = Not DescriptionRefresh
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtDataField_Validate(Index As Integer, Cancel As Boolean)
  If txtDataField(Index) = vbNullString Then Exit Sub
  
  Dim Value As Double
  Select Case Index
    Case 3
      Value = GetDouble(txtDataField(Index))
      If Value >= 0 Then
        txtDataField(Index) = Format(Value, "#,##0.00;(#,##0.00)")
        txtDataField(4) = ""
      Else
        txtDataField(4) = Format(0 - Value, "#,##0.00;(#,##0.00)")
        txtDataField(3) = ""
      End If
    Case 4
      Value = GetDouble(txtDataField(Index))
      If Value > 0 Then
        txtDataField(Index) = Format(Value, "#,##0.00;(#,##0.00)")
        txtDataField(3) = ""
      Else
        txtDataField(3) = Format(0 - Value, "#,##0.00;(#,##0.00)")
        txtDataField(4) = ""
      End If
  End Select
End Sub
