VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPolicyOpen 
   Caption         =   "Policy"
   ClientHeight    =   6624
   ClientLeft      =   192
   ClientTop       =   816
   ClientWidth     =   9192
   Icon            =   "frmPolicyOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6624
   ScaleWidth      =   9192
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5868
      Left            =   0
      ScaleHeight     =   5868
      ScaleWidth      =   9192
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   384
      Width           =   9192
      Begin VB.Frame fraPayment 
         Caption         =   "Payment"
         Height          =   768
         Left            =   48
         TabIndex        =   33
         Top             =   1932
         Width           =   8136
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Due"
            Height          =   192
            Index           =   3
            Left            =   7704
            TabIndex        =   49
            Top             =   240
            Width           =   300
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "dueAmount"
            DataField       =   "dueAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   7
            Left            =   7200
            TabIndex        =   48
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   816
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Return"
            Height          =   192
            Index           =   2
            Left            =   3348
            TabIndex        =   47
            Top             =   240
            Width           =   468
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "returnAmount"
            DataField       =   "returnAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   8
            Left            =   2880
            TabIndex        =   46
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   936
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "writeOff"
            DataField       =   "writeOff"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   10
            Left            =   6444
            TabIndex        =   45
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   516
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "premiumAdjust"
            DataField       =   "premiumAdjust"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   13
            Left            =   720
            TabIndex        =   44
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   1068
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "premium"
            DataField       =   "premium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   12
            Left            =   252
            TabIndex        =   43
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   624
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "financeCharge"
            DataField       =   "financeCharge"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   9
            Left            =   4944
            TabIndex        =   42
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   1044
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "receiveAmount"
            DataField       =   "receiveAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   11
            Left            =   1752
            TabIndex        =   41
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   1080
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "transfer"
            DataField       =   "transfer"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   14
            Left            =   4320
            TabIndex        =   40
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   528
         End
         Begin VB.Label Label13 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Transfer"
            Height          =   192
            Index           =   0
            Left            =   4248
            TabIndex        =   39
            Top             =   240
            Width           =   600
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Receive"
            Height          =   192
            Index           =   1
            Left            =   2220
            TabIndex        =   38
            Top             =   240
            Width           =   612
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fin/Late"
            Height          =   192
            Index           =   0
            Left            =   5376
            TabIndex        =   37
            Top             =   240
            Width           =   600
         End
         Begin VB.Label Label24 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Adjust"
            Height          =   192
            Index           =   0
            Left            =   1344
            TabIndex        =   36
            Top             =   240
            Width           =   444
         End
         Begin VB.Label Label22 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Premium"
            Height          =   192
            Index           =   0
            Left            =   240
            TabIndex        =   35
            Top             =   240
            Width           =   636
         End
         Begin VB.Label Label18 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Write Off"
            Height          =   192
            Index           =   0
            Left            =   6348
            TabIndex        =   34
            Top             =   240
            Width           =   600
         End
      End
      Begin VB.Frame fraNote 
         Caption         =   "Policy &Note"
         Height          =   1872
         Left            =   5448
         TabIndex        =   25
         Top             =   48
         Width           =   2724
         Begin VB.TextBox txtNote 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   1560
            Left            =   72
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   216
            Width           =   2592
         End
      End
      Begin VB.Frame fraFirm 
         Height          =   756
         Left            =   60
         TabIndex        =   17
         Top             =   1164
         Width           =   5316
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "city"
            DataField       =   "city"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   3
            Left            =   480
            TabIndex        =   24
            Top             =   468
            UseMnemonic     =   0   'False
            Width           =   240
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "telephone"
            DataField       =   "telephone"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   2
            Left            =   3192
            TabIndex        =   23
            Top             =   468
            UseMnemonic     =   0   'False
            Width           =   720
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "displayName"
            DataField       =   "displayName"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   1
            Left            =   756
            TabIndex        =   22
            Top             =   240
            UseMnemonic     =   0   'False
            Width           =   972
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Contact:"
            Height          =   192
            Left            =   120
            TabIndex        =   21
            Top             =   240
            Width           =   576
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "firmName"
            DataField       =   "firmName"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   0
            Left            =   132
            TabIndex        =   20
            Top             =   0
            UseMnemonic     =   0   'False
            Width           =   696
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "City:"
            Height          =   192
            Left            =   120
            TabIndex        =   19
            Top             =   468
            Width           =   300
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Telephone:"
            Height          =   192
            Left            =   2316
            TabIndex        =   18
            Top             =   468
            Width           =   828
         End
      End
      Begin VB.Frame fraPolicy 
         Caption         =   "Policy"
         Height          =   1116
         Left            =   72
         TabIndex        =   12
         Top             =   48
         Width           =   5304
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "paidToDate"
            DataField       =   "paidToDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "M/d/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   3
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   5
            Left            =   4212
            TabIndex        =   59
            Top             =   480
            Width           =   876
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Paid To Date"
            Height          =   192
            Left            =   4212
            TabIndex        =   58
            Top             =   240
            Width           =   960
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "policyNumber"
            DataField       =   "policyNumber"
            DataMember      =   "Policy"
            Height          =   192
            Index           =   17
            Left            =   648
            TabIndex        =   57
            Top             =   0
            Width           =   1008
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "expirationDate"
            DataField       =   "expirationDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "M/d/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   3
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   4
            Left            =   1272
            TabIndex        =   56
            Top             =   480
            Width           =   1044
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Expiration Date"
            Height          =   192
            Left            =   1272
            TabIndex        =   55
            Top             =   240
            Width           =   1092
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "perClaimDeductible"
            DataField       =   "perClaimDeductible"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   15
            Left            =   2820
            TabIndex        =   54
            Top             =   804
            Width           =   1428
         End
         Begin VB.Label Label23 
            AutoSize        =   -1  'True
            Caption         =   "Limits:"
            Height          =   192
            Left            =   144
            TabIndex        =   53
            Top             =   804
            Width           =   444
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "aggregateLimit"
            DataField       =   "aggregateLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   24
            Left            =   1836
            TabIndex        =   52
            Top             =   804
            UseMnemonic     =   0   'False
            Width           =   1080
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "perClaimLimit"
            DataField       =   "perClaimLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   27
            Left            =   648
            TabIndex        =   51
            Top             =   804
            UseMnemonic     =   0   'False
            Width           =   972
         End
         Begin VB.Label Label2 
            Caption         =   "/"
            Height          =   192
            Left            =   1716
            TabIndex        =   50
            Top             =   804
            Width           =   60
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "M/d/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   3
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   16
            Left            =   144
            TabIndex        =   16
            Top             =   480
            Width           =   948
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date"
            Height          =   192
            Left            =   132
            TabIndex        =   15
            Top             =   240
            Width           =   996
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "cancelEffectiveDate"
            DataField       =   "cancelEffectiveDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "M/d/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   3
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   6
            Left            =   2532
            TabIndex        =   14
            Top             =   480
            UseMnemonic     =   0   'False
            Width           =   1440
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Cancel Effective Date"
            Height          =   192
            Left            =   2520
            TabIndex        =   13
            Top             =   240
            Width           =   1536
         End
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   2976
         Left            =   72
         TabIndex        =   27
         Top             =   2880
         Width           =   8004
         _ExtentX        =   14139
         _ExtentY        =   5249
         _Version        =   393216
         Style           =   1
         Tabs            =   5
         Tab             =   4
         TabsPerRow      =   9
         TabHeight       =   420
         TabCaption(0)   =   "Lawyer"
         TabPicture(0)   =   "frmPolicyOpen.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Accounting"
         TabPicture(1)   =   "frmPolicyOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Installment"
         TabPicture(2)   =   "frmPolicyOpen.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Endorsement"
         TabPicture(3)   =   "frmPolicyOpen.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Application"
         TabPicture(4)   =   "frmPolicyOpen.frx":007C
         Tab(4).ControlEnabled=   -1  'True
         Tab(4).Control(0)=   "lvView(4)"
         Tab(4).Control(0).Enabled=   0   'False
         Tab(4).ControlCount=   1
         Begin MSComctlLib.ListView lvView 
            Height          =   2556
            Index           =   3
            Left            =   -74916
            TabIndex        =   28
            Tag             =   "PolicyPolicyEndorsement"
            Top             =   312
            Width           =   7812
            _ExtentX        =   13780
            _ExtentY        =   4509
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2556
            Index           =   0
            Left            =   -74916
            TabIndex        =   29
            Tag             =   "PolicyPolicyLawyer"
            Top             =   312
            Width           =   7812
            _ExtentX        =   13780
            _ExtentY        =   4509
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2556
            Index           =   2
            Left            =   -74916
            TabIndex        =   30
            Tag             =   "PolicyPolicyInstallment"
            Top             =   312
            Width           =   7812
            _ExtentX        =   13780
            _ExtentY        =   4509
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2556
            Index           =   1
            Left            =   -74916
            TabIndex        =   31
            Tag             =   "PolicyPolicyPayment"
            Top             =   312
            Width           =   7812
            _ExtentX        =   13780
            _ExtentY        =   4509
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2556
            Index           =   4
            Left            =   84
            TabIndex        =   32
            Tag             =   "PolicyApplicationView"
            Top             =   312
            Width           =   7812
            _ExtentX        =   13780
            _ExtentY        =   4509
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   6840
      Top             =   240
      _ExtentX        =   699
      _ExtentY        =   699
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   7452
      Top             =   240
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":0098
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":01AA
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":02BC
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":03CE
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":04E0
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":05F2
            Key             =   "View Large Icons"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":0704
            Key             =   "View Small Icons"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":0816
            Key             =   "View List"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPolicyOpen.frx":0928
            Key             =   "View Details"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   264
      Left            =   0
      TabIndex        =   0
      Top             =   6360
      Width           =   9192
      _ExtentX        =   16214
      _ExtentY        =   466
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7726
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "3/6/2013"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "4:18 PM"
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9192
      _ExtentX        =   16214
      _ExtentY        =   677
      _CBWidth        =   9192
      _CBHeight       =   384
      _Version        =   "6.7.9782"
      Child1          =   "picMenu"
      MinWidth1       =   792
      MinHeight1      =   336
      Width1          =   3504
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   984
      MinHeight2      =   336
      Width2          =   984
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   336
      Width3          =   948
      NewRow3         =   0   'False
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   3660
         ScaleHeight     =   336
         ScaleWidth      =   4488
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   24
         Width           =   4488
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   624
            Left            =   0
            TabIndex        =   7
            Top             =   12
            Width           =   1044
            _ExtentX        =   1842
            _ExtentY        =   1101
            ButtonWidth     =   1482
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   132
         ScaleHeight     =   336
         ScaleWidth      =   3348
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   24
         Width           =   3348
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   624
            Left            =   1848
            TabIndex        =   5
            Top             =   12
            Width           =   1044
            _ExtentX        =   1842
            _ExtentY        =   1101
            ButtonWidth     =   1588
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Application"
                        Text            =   "Application"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Policy"
                        Text            =   "Policy"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   624
            Left            =   996
            TabIndex        =   9
            Top             =   12
            Width           =   756
            _ExtentX        =   1334
            _ExtentY        =   1101
            ButtonWidth     =   1122
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Firm"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Policy"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   624
            Left            =   0
            TabIndex        =   10
            Top             =   12
            Width           =   888
            _ExtentX        =   1566
            _ExtentY        =   1101
            ButtonWidth     =   1207
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   5
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Lawyer"
                        Text            =   "Lawyer"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Accounting"
                        Text            =   "Accounting"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Installment"
                        Text            =   "Installment"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Endorsement"
                        Text            =   "Endorsement"
                     EndProperty
                     BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Application"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   336
         Left            =   8328
         ScaleHeight     =   336
         ScaleWidth      =   792
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   24
         Width           =   792
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   0
            TabIndex        =   3
            Top             =   12
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   1032
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.CommandButton cmdEditRow 
      Caption         =   "Hidden Edit Row"
      Default         =   -1  'True
      Height          =   312
      Left            =   3996
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   3276
      Width           =   1416
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Lawyer"
            Enabled         =   0   'False
            Index           =   0
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Accounting"
            Index           =   1
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Installment"
            Enabled         =   0   'False
            Index           =   2
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Endorsement"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "A&pplication"
            Index           =   4
         End
      End
      Begin VB.Menu mnuSeperator993 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void"
         Index           =   1
      End
      Begin VB.Menu mnuSeperator994 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator326 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Policy"
         Index           =   1
      End
      Begin VB.Menu mnuSeperator123 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuSeparator383 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "Tab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Lawyer"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Payment"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Installment"
            Index           =   2
         End
      End
      Begin VB.Menu mnuSeperator997 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings..."
      End
      Begin VB.Menu mnuSeperator995 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Workflow &Options..."
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsBrowseApplication 
         Caption         =   "Browse &Application"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuToolsBrowseFirm 
         Caption         =   "Browse &Firm"
      End
      Begin VB.Menu mnuToolsBrowsePolicy 
         Caption         =   "Browse &Policy"
      End
      Begin VB.Menu mnuSeparator8321 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator655 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator656 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
   End
End
Attribute VB_Name = "frmPolicyOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0416 (jjm)
'previous ver 2.00.0224 (jjm) wilmic
Option Explicit
Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mPolicyHandle As String
Private mFirmHandle As String
Private mEventHandle As String

Private mDataSet As Dataset

Public FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

'(Ref:Add Detail Policy Open)
Private PolicyAccountingData As Dataset
Private PolicyLawyerData As Dataset
Private InvoiceData As Dataset
Private ContactFirmData As DataProvider
Private EndorsementData As Dataset
Private ApplicationData As Dataset

Private Const TAB_POLICYACCOUNTING = 1
Private Const TAB_ENDORSEMENT = 3
Private Const TAB_ENDORSEMENTAPPLICATION = 4


'
' FormInterface Interface
'

'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Refresh
  FormRefresh
  Show
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Dim Datafields As Collection, FirmDataFields As Collection
  Set Datafields = Factory.PolicyService.Find(mPolicyHandle)
  Set FirmDataFields = ContactFirmData.Find(DatabaseName.FIRM_KEY, GetDataFromCollection(Datafields, DatabaseName.FIRM_KEY))
  
  SetFields Me.lblDataField, Datafields, DatabaseName.POLICY_TABLE
  SetFields Me.lblDataField, FirmDataFields, DatabaseName.FIRM_TABLE
  
  Me.Caption = "Policy Open - " & GetDataFromCollection(Datafields, DatabaseName.POLICYNUMBER_FIELD) & " (" & mPolicyHandle & ")"
  mFirmHandle = GetDataFromCollection(Datafields, DatabaseName.FIRM_KEY)
  'Open PolicyLawyer Dataset
  PolicyLawyerData.DataProvider.filter = DatabaseName.POLICY_KEY & " = '" & mPolicyHandle & "'"
  
  'Open Installment Dataset
  InvoiceData.DataProvider.filter = DatabaseName.POLICY_KEY & " = '" & mPolicyHandle & "'"
  
  'Open PolicyAccounting Dataset
  PolicyAccountingData.DataProvider.filter = DatabaseName.POLICY_KEY & " = '" & mPolicyHandle & "'"
  
  'Open Endorsement Dataset
  EndorsementData.DataProvider.filter = DatabaseName.POLICY_KEY & " = '" & mPolicyHandle & "'"
  
  'Open Accounting Dataset
  ApplicationData.DataProvider.filter = DatabaseName.POLICY_KEY & " = '" & mPolicyHandle & "'"
  
  'Event/Notepad
  mEventHandle = GetDataFromCollection(Datafields, DatabaseName.EVENT_KEY)
  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
  If (mEventHandle = vbNullString) Then
    txtNote.Text = vbNullString
  Else
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  End If
  
  ' Standard Navigation Update.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  SSTabRefresh
  
  Refresh
End Sub

'
' Set ListIndex from underlying view.  How we pull the field is
' dependent upon the list item type.  In this example, if we are
' opening from an event view, there is a different method for
' pulling the appliction id.
'
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case "Policy"
      mPolicyHandle = ListItemText(mListView.SelectedItem, DatabaseName.POLICY_KEY)
    Case "Event"
      Dim ListEventHandle As String
      ListEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
      
      'first test for a reference event
      Dim PolicyEventHandle As String
      PolicyEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(ListEventHandle, DatabaseName.POLICY_TABLE)
                        
      If PolicyEventHandle = vbNullString Then
        MsgBox "Underlying event does not reference a Policy."
        Exit Property
      End If
      mPolicyHandle = FindHandle(DatabaseName.POLICY_TABLE, _
                                 DatabaseName.EVENT_KEY, _
                                 PolicyEventHandle, _
                                 DatabaseName.POLICY_KEY)
  End Select
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
  Case 0
    Set ListViewDataProvider = PolicyLawyerData.DataProvider
  Case TAB_POLICYACCOUNTING
    Set ListViewDataProvider = PolicyAccountingData.DataProvider
  Case 2
    Set ListViewDataProvider = InvoiceData.DataProvider
  Case TAB_ENDORSEMENT
    Set ListViewDataProvider = EndorsementData.DataProvider
  Case TAB_ENDORSEMENTAPPLICATION
    Set ListViewDataProvider = ApplicationData.DataProvider
  End Select
End Function

Public Sub MenuRefresh()

  Dim IsLawyerTab, IsInstallmentTab As Boolean
  IsLawyerTab = (SSTab.Tab = 0)
  IsInstallmentTab = (SSTab.Tab = 2)
  
  'Browse
  mnuEditBrowseRow.Enabled = False
  mnuPopupBrowse.Enabled = False
  
  'Edit
  mnuEditEditRow.Enabled = Not (IsLawyerTab Or IsInstallmentTab)
  mnuPopupEdit.Enabled = Not (IsLawyerTab Or IsInstallmentTab)
  
  'New
  mnuPopupNew.Enabled = mnuFileNewDetail(SSTab.Tab).Enabled

  'Delete
  mnuEditDeleteRow.Enabled = False
  mnuPopupDelete.Enabled = False
    
End Sub

Private Sub newEndorsementApplication()
  Dim frmEAEdit As New frmEndorsementApplicationEdit
  FormMgr.Add frmEAEdit
  frmEAEdit.ShowNew mPolicyHandle, mFirmHandle
End Sub

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = POLICY_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no policies to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(PolicyId As String, Optional ShowTab As Integer = 0)
  mPolicyHandle = PolicyId
  mTab = ShowTab
  Show
End Sub

Public Property Let ShowTab(vData As Integer)
  mTab = vData
End Property

Public Sub SSTabRefresh()
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    If lvView(mTab).ListItems.Count > 0 Then
      Dim Index As Variant
      Index = IIf(lvView(mTab).SelectedItem.Key = "", lvView(mTab).SelectedItem.Index, lvView(mTab).SelectedItem.Key)
      lvView(mTab).Visible = False
      lvView(mTab).SelectedItem.Selected = False
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
      On Error Resume Next
      lvView(mTab).ListItems(Index).Selected = True
      lvView(mTab).Visible = True
      lvView(mTab).SetFocus
    Else
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
    End If
  Else
    SSTab.Tab = mTab
  End If
  
  MenuRefresh
  
  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
  sbStatusBar.Panels(2).Text = ListViewDataProvider(mTab).Source
End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
End Sub
'
' End General Methods
'
'
'
'
' Begin Controls
'

Private Sub cmdOk_Click()
  Unload Me
End Sub

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Initialize()
   Set mDataSet = New PolicyDataset
   Set mDataSet.Session = LawSession
   
   '(Ref:Add Detail Policy Open)
   Set PolicyAccountingData = NewGenericDataset(DatabaseName.POLICYACCOUNTING_VIEW)
   
   Set ContactFirmData = LawSession.NewDataProvider(DatabaseName.FIRM_VIEW)
   
   Set PolicyLawyerData = New PolicyLawyerCoverageDataset
   Set PolicyLawyerData.Session = LawSession
   
   'ver 2.70.01 (jjm) 2005 03 10 - modified to use InstallmentView
   Set InvoiceData = NewGenericDataset(DatabaseName.INVOICE_VIEW)
   
   Set EndorsementData = NewGenericDataset(DatabaseName.ENDORSEMENT_VIEW)
   'Set EndorsementData = New EndorsementDataset
   'Set EndorsementData.Session = LawSession
   
   Set ApplicationData = NewGenericDataset(DatabaseName.APPLICATION_VIEW)
End Sub


'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyL
'        mnuFileNewLawyer_Click
      Case vbKeyA
        mnuFileNewDetail_Click 1
      Case vbKeyM
'        mnuFileNewLimit_Click
    End Select
    Exit Sub
  End If
  
  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      mnuFileNewDetail_Click SSTab.Tab
  End Select
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  ' Setup screen.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.Height + 24

  FormUtil.EditLoadImages Me
  FormUtil.MenuLoad Me
  
  mnuEditEditRow.Caption = mnuEditEditRow.Caption + Chr(9) + "Enter"
  mnuFileNewDetail(1).Caption = mnuFileNewDetail(1).Caption + vbTab + "Ctrl+Shift+A"
  mnuFileNewDetail(3).Caption = mnuFileNewDetail(3).Caption + vbTab + "Ctrl+Shift+E"
  'Setup Form Manager
  FormMgr.Load Me
    
  SSTabResize
  FormRefresh
End Sub

Private Sub Form_Paint()
  SSTabResize
End Sub

' Expand body
Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub


Private Sub Form_Unload(cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub fraNote_DblClick()
  If mnuToolsNotepad.Enabled Then
    mnuToolsNotepad_Click
  End If
End Sub


Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  
  'Hack because the Attribute has no functionality
  If SSTab.Tab = 2 Then
    Exit Sub
  End If
  
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0 'Firm
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
    Case 1 'Policy
      FormUtil.ShowByPrimaryKey New frmPolicyEdit, mPolicyHandle, FormMgr
  End Select
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If

  If lvView(SSTab.Tab).ListItems.Count = 0 Then
    MsgBox "There are no items to delete."
    Exit Sub
  End If
  
  MsgBox "To Do: Enable Delete" 'ToDo:
'  Select Case SSTab.Tab
'    Case 0: 'Lawyer
'      With lvView(0)
'
'     'End With
'   Case 1: 'LawArea
'      With lvView(1)
'
'      End With
'   Case 2: 'Limit
'     With lvView(2)
'
'      End With
'   End Select
'   FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case 0:   'Lawyer
    
    Case TAB_POLICYACCOUNTING:
      Dim fPayment As New frmPolicyAccountingEdit
      FormMgr.Add fPayment
      fPayment.ShowByListView lvView(1)
      
    Case 2:    'Installment
'      Dim fLimit As New frm
'      FormMgr.Add fLimit
'      fLimit.ShowByListView lvView(2)
    Case TAB_ENDORSEMENT
      FormUtil.ShowByListView New frmEndorsementOpen, FormMgr, lvView(3)
    Case TAB_ENDORSEMENTAPPLICATION
      FormUtil.ShowByListView New frmEndorsementApplicationOpen, FormMgr, lvView(4)
  End Select
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNewDetail_Click(Index As Integer)
  Select Case Index
    Case TAB_POLICYACCOUNTING
      Dim frmAcctg As New frmPolicyAccountingEdit
      FormMgr.Add frmAcctg
      frmAcctg.ShowNew mPolicyHandle
    Case TAB_ENDORSEMENT
      newEndorsementApplication
    Case TAB_ENDORSEMENTAPPLICATION
      newEndorsementApplication
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.POLICY_TABLE, mPolicyHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.POLICY_REF, DatabaseName.POLICY_TABLE, mPolicyHandle, lblDataField(0)
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpSearchForHelpOn_Click()
    Dim nRet As Integer

    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hWnd, App.HelpFile, 261, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If

End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNewDetail_Click SSTab.Tab
End Sub

Private Sub mnuToolsBrowseFirm_Click()
  Dispatcher.BrowseFirmOpen mFirmHandle
End Sub

Private Sub mnuToolsBrowsePolicy_Click()
    Dispatcher.BrowsePolicyOpen mPolicyHandle
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.APPLICATION_TABLE, lawActionNew
End Sub

'
' Standard choose columns for the current tab's list view.
'
Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'
' Standard view next menu
'
Private Sub mnuViewNext_Click()
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

'
' Standard view previous menu
'
Private Sub mnuViewPrevious_Click()
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

'
' Standard view refresh menu
'
Public Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

'
' Standard view a specific tab
'
Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

'
' Standard save settings
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

'
' Standard view toolbar.
'
Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  Form_Resize
End Sub


Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

Private Sub picMenu_Resize()
  tbrInsert.Top = (picMenu.Height - tbrInsert.Height) / 2
  tbrEdit.Top = (picMenu.Height - tbrEdit.Height) / 2
  tbrBrowse.Top = (picMenu.Height - tbrBrowse.Height) / 2
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  mTab = SSTab.Tab
  SSTabRefresh
  
  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub

Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub

Private Sub tbrBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuToolsBrowsePolicy_Click
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1
'      mnuToolsBrowseApplication_Click
    Case 2
      mnuToolsBrowseFirm_Click
    Case 3
      mnuToolsBrowsePolicy_Click
      
  End Select
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEdit_Click 1
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileNewDetail_Click SSTab.Tab
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNewDetail_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuEdit_Click ButtonMenu.Index - 1
End Sub

