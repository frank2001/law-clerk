VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ApplicationQuestionService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Private Const ServiceName = "ApplicationQuestionService"

Private mClient As SoapClient30
Private mDataProvider As DataProvider
Private mHandleNames(1) As String

'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Set mDataProvider = Session.NewDataProvider(DatabaseName.APPLICATIONQUESTION_TABLE)
  mHandleNames(0) = DatabaseName.APPLICATION_KEY
  mHandleNames(1) = DatabaseName.QUESTION_KEY
End Sub


'
' Data Access
'
Public Function Find(ApplicationHandle As String, QuestionHandle As String, Optional Column As String) As Collection
  Dim Handles(1) As String
  Handles(0) = ApplicationHandle
  Handles(1) = QuestionHandle
  Set Find = mDataProvider.FindMulti(mHandleNames, Handles)
End Function


'
' Interface to server
'

'
' Create an application.
'
Public Function CreateApplicationQuestion( _
    ApplicationId As String, _
    questionId As String, _
    answer As Boolean, _
    answerNote As String, _
    answerAmount As Double) As Variant

On Error GoTo ErrorHandler
  
  CreateApplicationQuestion = mClient.Create( _
      ApplicationId, _
      questionId, _
      answer, _
      answerNote, _
      answerAmount)
  Exit Function
  
ErrorHandler:
'  MsgBox "ApplicationQuestion Service Error: " & Err.Description, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
End Function

'
' Write an application.
'
Public Function WriteApplicationQuestion( _
    ApplicationHandle As String, _
    QuestionHandle As String, _
    questionId As String, _
    answer As Boolean, _
    answerNote As String, _
    answerAmount As Double) As Boolean
    
On Error GoTo ErrorHandler
  
  Call mClient.Write( _
      ApplicationHandle, _
      QuestionHandle, _
      questionId, _
      answer, _
      answerNote, _
      answerAmount)
  WriteApplicationQuestion = True
  Exit Function
  
ErrorHandler:
  MsgBox "ApplicationQuestion Service Error: " & Err.Description, vbExclamation
End Function

'
' Delete an ApplicationQuestion
'
Public Function DeleteApplicationQuestion( _
    ApplicationHandle As String, QuestionHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo Exception
  Call mClient.Delete(ApplicationHandle, QuestionHandle, ParameterNames, ParameterValues)
  DeleteApplicationQuestion = True
  Exit Function
  
Exception:
  MsgBox "ApplicationQuestion Service Error: " & Err.Description, vbExclamation
End Function
