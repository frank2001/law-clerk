VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmIncidentOpen 
   Caption         =   "Incident Summary"
   ClientHeight    =   7905
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   9225
   Icon            =   "frmIncidentOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7905
   ScaleWidth      =   9225
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6675
      Left            =   0
      ScaleHeight     =   6675
      ScaleWidth      =   9225
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   390
      Width           =   9225
      Begin VB.Frame Frame2 
         Caption         =   "Dates"
         Height          =   1080
         Left            =   90
         TabIndex        =   45
         Top             =   2475
         Width           =   6315
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Close:"
            Height          =   192
            Index           =   7
            Left            =   132
            TabIndex        =   57
            Top             =   528
            Width           =   456
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "openDate"
            DataMember      =   "Instance"
            Height          =   192
            Index           =   17
            Left            =   948
            TabIndex        =   56
            Top             =   288
            UseMnemonic     =   0   'False
            Width           =   324
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Open:"
            Height          =   192
            Index           =   0
            Left            =   132
            TabIndex        =   55
            Top             =   288
            Width           =   432
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "closeDate"
            DataMember      =   "Instance"
            Height          =   192
            Index           =   13
            Left            =   948
            TabIndex        =   54
            Top             =   528
            UseMnemonic     =   0   'False
            Width           =   324
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Know:"
            Height          =   195
            Index           =   5
            Left            =   1890
            TabIndex        =   53
            Top             =   285
            Width           =   420
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "knowDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   11
            Left            =   2460
            TabIndex        =   52
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Occur:"
            Height          =   195
            Index           =   1
            Left            =   1890
            TabIndex        =   51
            Top             =   525
            Width           =   450
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "occurDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   33
            Left            =   2460
            TabIndex        =   50
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "occurDateRange"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   6
            Left            =   4545
            TabIndex        =   49
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Occur Range:"
            Height          =   195
            Index           =   2
            Left            =   3495
            TabIndex        =   48
            Top             =   525
            Width           =   1005
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "knowDateRange"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   5
            Left            =   4545
            TabIndex        =   47
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Know Range:"
            Height          =   195
            Index           =   0
            Left            =   3495
            TabIndex        =   46
            Top             =   285
            Width           =   975
         End
      End
      Begin VB.Frame fraCross 
         Caption         =   "Cross References"
         Height          =   2400
         Left            =   4575
         TabIndex        =   43
         Top             =   45
         Width           =   1830
         Begin VB.TextBox txtCrossRef 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   2100
            Left            =   72
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   210
            Width           =   1710
         End
      End
      Begin VB.Frame fraClaim 
         Caption         =   "Incident"
         Height          =   2385
         Left            =   105
         TabIndex        =   1
         Top             =   60
         Width           =   4416
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Policy Id:"
            Height          =   192
            Left            =   132
            TabIndex        =   26
            Top             =   1068
            Width           =   648
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "policyNumber"
            DataField       =   "policyNumber"
            DataMember      =   "Policy"
            Height          =   195
            Index           =   1
            Left            =   1110
            TabIndex        =   25
            Top             =   1065
            Width           =   960
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "lawyerName"
            DataField       =   "lawyerName"
            DataMember      =   "Lawyer"
            Height          =   192
            Index           =   7
            Left            =   1116
            TabIndex        =   24
            Top             =   348
            Width           =   912
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Tortfeasor:"
            Height          =   192
            Left            =   132
            TabIndex        =   23
            Top             =   348
            Width           =   780
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "adjuster"
            DataField       =   "adjuster"
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            DataMember      =   "Instance"
            Height          =   192
            Index           =   9
            Left            =   1116
            TabIndex        =   22
            Top             =   1704
            UseMnemonic     =   0   'False
            Width           =   576
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "claimNumber"
            DataField       =   "claimNumber"
            DataMember      =   "Claim"
            Height          =   195
            Index           =   0
            Left            =   2910
            TabIndex        =   21
            Top             =   825
            Width           =   915
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Claim:"
            Height          =   195
            Left            =   2310
            TabIndex        =   20
            Top             =   825
            Width           =   420
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmName"
            DataField       =   "firmName"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   3
            Left            =   1110
            TabIndex        =   8
            Top             =   585
            UseMnemonic     =   0   'False
            Width           =   660
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Insured:"
            Height          =   192
            Left            =   132
            TabIndex        =   7
            Top             =   588
            Width           =   564
         End
         Begin VB.Label lblDataField 
            Caption         =   "claimant"
            DataField       =   "claimant"
            DataMember      =   "Instance"
            Height          =   348
            Index           =   4
            Left            =   1116
            TabIndex        =   6
            Top             =   1308
            UseMnemonic     =   0   'False
            Width           =   3204
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Adjuster:"
            Height          =   192
            Index           =   0
            Left            =   144
            TabIndex        =   5
            Top             =   1704
            Width           =   624
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Claimant:"
            Height          =   192
            Left            =   132
            TabIndex        =   4
            Top             =   1308
            Width           =   660
         End
         Begin VB.Label lblIncidentNumber 
            AutoSize        =   -1  'True
            Caption         =   "incidentNumber"
            Height          =   195
            Left            =   1110
            TabIndex        =   3
            Top             =   825
            UseMnemonic     =   0   'False
            Width           =   1110
         End
         Begin VB.Label Label14 
            Caption         =   "Incident:"
            Height          =   192
            Left            =   132
            TabIndex        =   2
            Top             =   828
            Width           =   888
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   2604
         Top             =   1428
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   3228
         Top             =   1428
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   4668
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   876
         Width           =   1416
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   2910
         Left            =   75
         TabIndex        =   27
         Top             =   3675
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   5133
         _Version        =   393216
         Style           =   1
         Tabs            =   7
         TabsPerRow      =   9
         TabHeight       =   420
         WordWrap        =   0   'False
         TabCaption(0)   =   "Contact"
         TabPicture(0)   =   "frmIncidentOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Event"
         TabPicture(1)   =   "frmIncidentOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).Control(1)=   "optStatus(0)"
         Tab(1).Control(2)=   "optStatus(1)"
         Tab(1).Control(3)=   "optStatus(2)"
         Tab(1).Control(4)=   "optStatus(3)"
         Tab(1).ControlCount=   5
         TabCaption(2)   =   "Document"
         TabPicture(2)   =   "frmIncidentOpen.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Grievance Counsel"
         TabPicture(3)   =   "frmIncidentOpen.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Law Area"
         TabPicture(4)   =   "frmIncidentOpen.frx":007C
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "lvView(4)"
         Tab(4).ControlCount=   1
         TabCaption(5)   =   "Law Error"
         TabPicture(5)   =   "frmIncidentOpen.frx":0098
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "lvView(5)"
         Tab(5).ControlCount=   1
         TabCaption(6)   =   "Unallocated"
         TabPicture(6)   =   "frmIncidentOpen.frx":00B4
         Tab(6).ControlEnabled=   0   'False
         Tab(6).Control(0)=   "lblUnallocatedOther"
         Tab(6).Control(1)=   "Label18"
         Tab(6).Control(2)=   "lblUnallocatedCoverageCounsel"
         Tab(6).Control(3)=   "lblCoverageCounselLabel"
         Tab(6).Control(4)=   "lvView(6)"
         Tab(6).ControlCount=   5
         Begin VB.OptionButton optStatus 
            Caption         =   "All"
            Height          =   240
            Index           =   3
            Left            =   -74916
            TabIndex        =   31
            Top             =   1332
            Width           =   912
         End
         Begin VB.OptionButton optStatus 
            Caption         =   "Void"
            Height          =   240
            Index           =   2
            Left            =   -74916
            TabIndex        =   30
            Top             =   1012
            Width           =   912
         End
         Begin VB.OptionButton optStatus 
            Caption         =   "Pending"
            Height          =   240
            Index           =   1
            Left            =   -74916
            TabIndex        =   29
            Top             =   692
            Width           =   948
         End
         Begin VB.OptionButton optStatus 
            Caption         =   "Complete"
            Height          =   240
            Index           =   0
            Left            =   -74916
            TabIndex        =   28
            Top             =   372
            Value           =   -1  'True
            Width           =   972
         End
         Begin MSComctlLib.ListView lvLetter 
            Height          =   2208
            Left            =   -74700
            TabIndex        =   32
            Top             =   600
            Width           =   6924
            _ExtentX        =   12224
            _ExtentY        =   3889
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   0
            Left            =   60
            TabIndex        =   33
            Tag             =   "IncidentContact"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12435
            _ExtentY        =   4392
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2490
            Index           =   1
            Left            =   -73875
            TabIndex        =   34
            Tag             =   "IncidentEvent"
            Top             =   315
            Width           =   5985
            _ExtentX        =   10557
            _ExtentY        =   4392
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   2
            Left            =   -74940
            TabIndex        =   35
            Tag             =   "IncidentLetter"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12435
            _ExtentY        =   4392
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   3
            Left            =   -74940
            TabIndex        =   36
            Tag             =   "IncidentGrievanceConsole"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12435
            _ExtentY        =   4392
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   4
            Left            =   -74940
            TabIndex        =   41
            Tag             =   "IncidentLawArea"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12435
            _ExtentY        =   4392
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   5
            Left            =   -74940
            TabIndex        =   42
            Tag             =   "IncidentLawError"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12435
            _ExtentY        =   4392
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2490
            Index           =   6
            Left            =   -73875
            TabIndex        =   58
            Tag             =   "IncidentInstancePayment"
            Top             =   315
            Width           =   5985
            _ExtentX        =   10557
            _ExtentY        =   4392
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.Label lblCoverageCounselLabel 
            Alignment       =   1  'Right Justify
            Caption         =   "Coverage Counsel"
            Height          =   375
            Left            =   -74910
            TabIndex        =   62
            Top             =   345
            Width           =   1005
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblUnallocatedCoverageCounsel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   195
            Left            =   -74280
            TabIndex        =   61
            Top             =   795
            Width           =   375
         End
         Begin VB.Label Label18 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Other"
            Height          =   195
            Left            =   -74295
            TabIndex        =   60
            Top             =   1125
            Width           =   390
         End
         Begin VB.Label lblUnallocatedOther 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   195
            Left            =   -74280
            TabIndex        =   59
            Top             =   1380
            Width           =   375
         End
         Begin VB.Label Label19 
            Caption         =   "Fee Dispute:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   40
            Top             =   1356
            Width           =   1656
         End
         Begin VB.Label Label17 
            Caption         =   "Judgement Claimant:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   39
            Top             =   1116
            Width           =   1656
         End
         Begin VB.Label Label15 
            Caption         =   "Disposition Name:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   38
            Top             =   876
            Width           =   1656
         End
         Begin VB.Label Label2 
            Caption         =   "Disposition Id:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   37
            Top             =   636
            Width           =   1656
         End
      End
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   6828
      Top             =   2304
      _ExtentX        =   688
      _ExtentY        =   688
      _Version        =   393216
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9225
      _ExtentX        =   16272
      _ExtentY        =   688
      BandBorders     =   0   'False
      _CBWidth        =   9225
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   990
      MinHeight2      =   330
      Width2          =   3015
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   315
      Width3          =   945
      NewRow3         =   0   'False
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   3675
         ScaleHeight     =   330
         ScaleWidth      =   2820
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   30
         Width           =   2820
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   660
            Left            =   0
            TabIndex        =   19
            Top             =   30
            Width           =   1110
            _ExtentX        =   1958
            _ExtentY        =   1164
            ButtonWidth     =   1508
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   6690
         ScaleHeight     =   315
         ScaleWidth      =   2445
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   30
         Width           =   2445
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   0
            TabIndex        =   12
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1860
            TabIndex        =   15
            Top             =   30
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1640
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Lawyer"
                        Text            =   "Lawyer"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Policy"
                        Text            =   "Policy"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   660
            Left            =   990
            TabIndex        =   16
            Top             =   30
            Width           =   750
            _ExtentX        =   1323
            _ExtentY        =   1164
            ButtonWidth     =   1191
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Incident"
                        Text            =   "Incident"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Narrative"
                        Text            =   "WordPerfect Narrative"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   660
            Left            =   30
            TabIndex        =   17
            Top             =   30
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   1164
            ButtonWidth     =   1270
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the claim."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   7
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Contact"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Event"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Document"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Visible         =   0   'False
                        Text            =   "Grievance Counsel"
                     EndProperty
                     BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Law Area"
                     EndProperty
                     BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Law Error"
                     EndProperty
                     BeginProperty ButtonMenu7 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Unallocated Payment"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   14
      Top             =   7635
      Width           =   9225
      _ExtentX        =   16272
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7726
            MinWidth        =   2117
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "7/21/2011"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "4:55 PM"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Incident"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator999 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Contact"
            Index           =   0
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Event"
            Index           =   1
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Document"
            Index           =   2
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "Grievance Console"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "Law &Area"
            Index           =   4
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "La&w Error"
            Index           =   5
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Unallocated Payment"
            Index           =   6
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Claim"
            Index           =   0
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Lawyer"
            Index           =   2
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Policy"
            Index           =   3
         End
      End
      Begin VB.Menu mnuSeparator834 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator326 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator332 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator8221 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditIncident 
         Caption         =   "&Incident"
      End
      Begin VB.Menu mnuEditNarrative 
         Caption         =   "WordPerfect &Narrative"
      End
      Begin VB.Menu mnuSeperator9999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuSeperator978 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Complete Row"
         Index           =   0
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Void Row"
         Index           =   1
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "&Tab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Contact"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "E&vent"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Document"
            Index           =   2
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Grievance Console"
            Index           =   3
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "Law &Area"
            Index           =   4
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "Law &Error"
            Index           =   5
         End
      End
      Begin VB.Menu mnuSeperator9090 
         Caption         =   "-"
      End
      Begin VB.Menu mnuChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings..."
      End
      Begin VB.Menu mnuSeparator282 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "Tool&bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator56465 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "Browse &Firm"
         Index           =   0
      End
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "Browse &Lawyer"
         Index           =   1
      End
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "Browse &Policy"
         Index           =   2
      End
      Begin VB.Menu mnuSeparator261 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeperator298 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "Web Merge"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator457 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuPopupWebMerge 
         Caption         =   "Web Merge"
      End
      Begin VB.Menu mnuSeperator458 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSeperator469 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Complete"
         Index           =   0
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Void"
         Index           =   1
      End
   End
End
Attribute VB_Name = "frmIncidentOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.71.01 (jjm) 2005 03 10 - hacked interface bug
'ver 2.71.00 (jjm) 2004 02 16 - Suspend Diary functionality
'ver 2.50.00 (jjm) 2004 02 15
'previous ver 2.20.0610 (jjm)
'previous ver 2.10.0326 (jjm)
'previous ver 2.10.0326 (jjm) WILMIC
Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mIncidentHandle As String
Private mInstanceHandle As String
Private mEventHandle As String
Private mLawyerHandle As String
Private mFirmHandle As String
Private mClaimHandle As String
Private mIncidentNumber As String
Private mPolicyHandle As String

Private FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

Private mContactDataset As Dataset
Private LawArea As Dataset
Private LawError As Dataset
Private mReferenceView As Dataset
Private mLetter As Dataset
Private InstancePaymentData As Dataset

Private Const ContactTab = 0
Private Const EventTab = 1
Private Const DocumentTab = 2
Private Const GrievanceConsoleTab = 3
Private Const LawAreaTab = 4
Private Const LawErrorTab = 5
Private Const UnallocatedTab = 6

'Constants for set datafields
Private Const LAWYER_DATAMEMBER = "Lawyer"
Private Const INSTANCE_DATAMEMBER = "Instance"

'ver 2.71.00 (jjm) 2005 02 15
'SuspendDiary - for functionality
Private mDirty As Boolean

Public Function EventFilter() As String
  Dim StatusFilter As String
  If optStatus(0).Value Then
    StatusFilter = LawDialect.Exists(DatabaseName.COMPLETEDATE_FIELD) & " and " & _
                   LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  ElseIf optStatus(1).Value Then
    StatusFilter = LawDialect.ColumnIsEmpty(DatabaseName.COMPLETEDATE_FIELD) & " and " & _
                   LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  ElseIf optStatus(2).Value Then
    StatusFilter = LawDialect.Exists(DatabaseName.VOIDDATE_FIELD)
  End If
  
  EventFilter = DatabaseName.EVENTTYPERULEID_FIELD & " != '" & DatabaseName.INCIDENTDOCUMENT_RULE & "' and " & _
                LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & _
                IIf(StatusFilter = vbNullString, vbNullString, " and " & StatusFilter)
End Function

'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimSave
      'Catch EventDocument cases
'      If fInterface.Key = DatabaseName.EVENT_KEY & "New" Then
'        'Setup for auto-complete
'        Dim Handles(0) As String
'        'Set the folder and Prevent a crash if the event was
'        Handles(0) = fInterface.Child.EventHandle
'        'Automatically Complete the documents
'        Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
'      End If
        
      'ver 2.71.00 (jjm) 2005 02 02
      'ver 2.71.01 (jjm) 2005 03 10 - HACK: use form instead of interace key
      'Special Actions for insterting an event
      If fInterface.Child.Name = "frmEventEdit" Then
        'When saving a claim diary entry set mDirty to false.
        'This is so the user isn't prompted a second time to set the diary.
        If fInterface.Child.EventTypeHandle = DatabaseName.CLAIMDIARY_TYPE Then
          mDirty = False
          Refresh
          FormRefresh
          Exit Sub
        End If
      End If
      
      'ver 2.71.00 (jjm) 2005 02 01
      mDirty = True
      
  End Select
 
  Refresh
  Show
  FormRefresh
  
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Dim Datafields As Collection
  Set Datafields = Factory.InstanceService.Find(mIncidentHandle)

  mEventHandle = GetDataFromCollection(Datafields, DatabaseName.EVENT_KEY, True)
  mClaimHandle = GetDataFromCollection(Datafields, DatabaseName.OTHERINSTANCEID_FIELD) 'ver 2.50.00 (jjm) 2004 02 15
  mFirmHandle = GetDataFromCollection(Datafields, DatabaseName.FIRM_KEY)
  mPolicyHandle = GetDataFromCollection(Datafields, DatabaseName.POLICY_KEY)
  
  'cache number for display
  mIncidentNumber = GetDataFromCollection(Factory.IncidentService.Find(mIncidentHandle, DatabaseName.INCIDENTNUMBER_FIELD), _
                                          DatabaseName.INCIDENTNUMBER_FIELD, True)
  'set Instance data
  SetFields lblDataField, Datafields
  
  'Set claim info (mainly just the claim number display)
  SetFields lblDataField, _
            Factory.ClaimService.Find(mClaimHandle, DatabaseName.CLAIMNUMBER_FIELD), _
            "Claim"
  
  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
  
  Caption = "Incident Summary (" & mIncidentNumber & ")"
  lblIncidentNumber = mIncidentNumber

  mLawyerHandle = GetDataFromCollection(Datafields, DatabaseName.MAINLAWYERID_FIELD)
  If mLawyerHandle <> vbNullString Then
    lblDataField(7).Caption = Factory.EntityService.Find(mLawyerHandle, DatabaseName.ENTITYNAME_FIELD)(DatabaseName.ENTITYNAME_FIELD)
  End If
  
  'Display Policy Number #1939
  Dim PolicyData As Collection
  Set PolicyData = Factory.PolicyService.Find(mPolicyHandle)
  SetFields lblDataField, PolicyData, "Policy"
  
 
  'ver 2.73.00 (jjm) REF 170
  'Display Cross Reference and Previous Incident Number
  txtCrossRef.Text = vbNullString
  mReferenceView.DataProvider.filter = LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & " and " & _
                                       LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD) & " and " & _
                                       LawDialect.EqualString(DatabaseName.EVENTTYPE_KEY, DatabaseName.CLAIMCROSSREFERENCE_TYPE)
  mReferenceView.DataProvider.OpenRecordset
  Dim crossRef As Collection
  Set crossRef = mReferenceView.DataProvider.Collection
  If (Not crossRef Is Nothing) Then
    Dim crossRefList() As String
    crossRefList = Split(crossRef(1)(DatabaseName.EVENTDESC_FIELD), " ")
    Dim I As Integer
    For I = 0 To SafeUBound(crossRefList)
      txtCrossRef.Text = txtCrossRef.Text + crossRefList(I) + vbCrLf
    Next I
  End If
  
  
  LawArea.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mIncidentHandle)
  LawError.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mIncidentHandle)
  
  mReferenceView.DataProvider.filter = EventFilter
  
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  mLetter.DataProvider.filter = LawDialect.EqualString(DatabaseName.EVENTREFERENCE_KEY, mEventHandle) & _
                               " and " & LawDialect.EqualString(DatabaseName.EVENTTYPERULEID_FIELD, DatabaseName.INCIDENTDOCUMENT_RULE) & _
                               " and " & LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  mContactDataset.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mIncidentHandle)
  
  InstancePaymentData.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mIncidentHandle)
  
  InstancePaymentBucketRefresh
  
  
 'ver 2.50.00 (jjm) 2004 02 15
  mnuFileOpen(0).Enabled = mClaimHandle <> vbNullString
  
  mnuFileOpen(1).Enabled = Not (mFirmHandle = vbNullString)
  mnuFileOpen(2).Enabled = Not (mLawyerHandle = vbNullString)
  mnuFileOpen(3).Enabled = Not (mPolicyHandle = vbNullString)
  mnuToolsBrowse(0).Enabled = Not (mFirmHandle = vbNullString)
  mnuToolsBrowse(1).Enabled = Not (mLawyerHandle = vbNullString)
  mnuToolsBrowse(2).Enabled = Not (mPolicyHandle = vbNullString)
  
  SSTabRefresh
  Refresh
  
  'ver 2.71.00 (jjm) 2005 02 15
  If mDirty Then
    SuspendDiary
  End If
End Sub

'bug 2613 Manual update on event type breakout displays on the Unallocated (InstancePayment) tab
Private Sub InstancePaymentBucketRefresh()

  'sum buckets for each event type to be totaled
  Dim OtherBucket As Double
  Dim CounselBucket As Double
  
  Dim iPayDataset As Dataset 'dataset to pull the InstancePaymentView.
  
  'pull the data
  Set iPayDataset = NewGenericDataset(DatabaseName.INSTANCEPAYMENT_VIEW)
  Dim filter As New filter
  filter.Add LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mIncidentHandle)
  filter.Add LawDialect.EqualString(DatabaseName.EVENTSTATUS_FIELD, "Complete")
  iPayDataset.DataProvider.filter = filter.filter
  iPayDataset.DataProvider.OpenRecordset
  
  Dim iPayData As Collection
  Set iPayData = iPayDataset.DataProvider.Collection
  Dim tmpAmount As Double
  Dim I As Integer
  If Not iPayData Is Nothing Then
    For I = 1 To iPayData.Count
      tmpAmount = GetDouble(GetDataFromCollection(iPayData(I), DatabaseName.PAYMENTGENERALLEGAL_FIELD))
      Select Case GetDataFromCollection(iPayData(I), DatabaseName.EVENTTYPE_KEY)
        Case "ClmPay-UCC"
          CounselBucket = CounselBucket + tmpAmount
        Case "ClmPay-UO"
          OtherBucket = OtherBucket + tmpAmount
      End Select
    Next I
  End If
  lblUnallocatedCoverageCounsel.Caption = Format(CounselBucket, lblUnallocatedCoverageCounsel.DataFormat.Format)
  lblUnallocatedOther.Caption = Format(OtherBucket, lblUnallocatedOther.DataFormat.Format)
End Sub

'
' Set ListIndex from underlying view.  How we pull the field is
' dependent upon the list item type.  In this example, if we are
' opening from an event view, there is a different method for
' pulling the appliction id.
'
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.INCIDENT_TABLE
      mIncidentHandle = ListItemText(mListView.SelectedItem, DatabaseName.INCIDENT_KEY)
    Case DatabaseName.EVENT_TABLE
      Dim vEventHandle As String
      vEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY), DatabaseName.INSTANCE_TABLE)
      mIncidentHandle = FindHandle(DatabaseName.INSTANCE_TABLE, _
                                   DatabaseName.EVENT_KEY, _
                                   vEventHandle, _
                                   DatabaseName.INSTANCE_KEY)
  End Select
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
    Case ContactTab
      Set ListViewDataProvider = mContactDataset.DataProvider
    Case EventTab  'Event
      Set ListViewDataProvider = mReferenceView.DataProvider
    Case DocumentTab  'Letter
      Set ListViewDataProvider = mLetter.DataProvider
    Case GrievanceConsoleTab
      Set ListViewDataProvider = Nothing
    Case LawAreaTab  'Law Area
      Set ListViewDataProvider = LawArea.DataProvider
    Case LawErrorTab  'Law Error
      Set ListViewDataProvider = LawError.DataProvider
    Case UnallocatedTab  'Unallocated
      Set ListViewDataProvider = InstancePaymentData.DataProvider
  End Select
End Function

'ver 2.10.0415 (jjm) WILMIC
Private Sub MenuRefresh()
  'Disable and enable menus
  
  'Browse Menu Items
  mnuPopupBrowse.Enabled = False '((SSTab.Tab = LawyerTab) Or (SSTab.Tab = ReserveTab))
  mnuEditBrowseRow.Enabled = False
  
  'Event complete and void menu items
  Dim EventEnable As Boolean
  EventEnable = SSTab.Tab = EventTab Or _
                SSTab.Tab = DocumentTab Or _
                SSTab.Tab = UnallocatedTab
                
  mnuEditEventRow(0).Enabled = EventEnable
  mnuEditEventRow(1).Enabled = EventEnable
  mnuPopupEvent(0).Enabled = EventEnable
  mnuPopupEvent(1).Enabled = EventEnable
  
  'Merge Menu items
  Dim MergeEnabled As Boolean
  MergeEnabled = (SSTab.Tab = DocumentTab)
  mnuToolsMerge.Enabled = MergeEnabled
  mnuPopupMerge.Enabled = MergeEnabled
  mnuToolsWebMerge.Enabled = MergeEnabled
  mnuPopupWebMerge.Enabled = MergeEnabled
  
  'New menu items
  mnuPopupNew.Enabled = mnuFileNewDetail(SSTab.Tab).Enabled
  
  'Delete menu items
  mnuEditDeleteRow.Enabled = False
  mnuPopupDelete.Enabled = False
End Sub

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.INCIDENT_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no incidents to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(IncidentId As String, Optional ShowTab As Integer = 0)
  mIncidentHandle = IncidentId
  mTab = ShowTab
  Show
End Sub

Public Sub SSTabRefresh()
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    If lvView(mTab).ListItems.Count > 0 Then
      Dim Index As Variant
      Index = IIf(lvView(mTab).SelectedItem.Key = "", lvView(mTab).SelectedItem.Index, lvView(mTab).SelectedItem.Key)
      lvView(mTab).Visible = False
      lvView(mTab).SelectedItem.Selected = False
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
      On Error Resume Next
      lvView(mTab).ListItems(Index).Selected = True
      lvView(mTab).Visible = True
      lvView(mTab).SetFocus
    Else
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
    End If
  Else
    SSTab.Tab = mTab
  End If
  
  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
  MenuRefresh
  
End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
End Sub

'ver 2.71.00 (jjm) 2005 02 15
Private Sub SuspendDiary()
  
  'ver 2.71.00 (jjm) 2005 02 15
  'refresh diary status
  'Save a variable to determine if there is a pending diary
  
  Dim Refview As Dataset
  Set Refview = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
  
  Refview.DataProvider.filter = LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & " and " & _
                                LawDialect.EqualString(DatabaseName.EVENTTYPE_KEY, DatabaseName.CLAIMDIARY_TYPE) & " and " & _
                                LawDialect.ColumnIsEmpty(DatabaseName.COMPLETEDATE_FIELD) & " and " & _
                                LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  Refview.DataProvider.Column = DatabaseName.EVENT_KEY
  Refview.DataProvider.OpenRecordset
  Dim mDiaryCol As Collection
  Set mDiaryCol = Refview.DataProvider.Collection
  
  'Only continue if there is an existing diary
  If Not mDiaryCol Is Nothing Then
    'Get the diary
    Dim mDiaryHandle As String
    mDiaryHandle = mDiaryCol(1)(DatabaseName.EVENT_KEY)
    
   
    If MsgBox("Do you want to advance the diary?", vbYesNo, "Diary") = vbYes Then
      'Set the diary date and make the call.
      Dim DiaryDate As Date
      DiaryDate = DateAdd("d", 42, Today)
      
      'ver 2.71.00 (jjm) 2005 02 15
      'display event edit rather than auto incrementing the diary date
      Dim fEvent As New frmEventEdit
      'ver 2.71.01 (jjm) 2005 03 10 - hacked interface bug - removed the interface key
      FormMgr.Add fEvent ', DatabaseName.EVENT_TABLE
      fEvent.ShowByPrimaryKey mDiaryHandle, DiaryDate
      'If Not Factory.EventService.setDueDate(DiaryId, DiaryDate) Then Exit Sub
    End If
  End If
  'reset the mDirty
  mDirty = False
End Sub

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

'
' End General Methods
'

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Activate()
  Form_Resize
  SSTabResize
End Sub

Private Sub Form_Initialize()
   'Initialzie the LawArea Dataset
   Set LawArea = NewGenericDataset(DatabaseName.INSTANCELAWAREA_VIEW)
   'Initialzie the LawError Dataset
   Set LawError = NewGenericDataset(DatabaseName.INSTANCELAWERROR_VIEW)
   
   'Initialize the Contact Dataset
   Set mContactDataset = NewGenericDataset(DatabaseName.CONTACT_VIEW)
   'Initialize the Reference Dataset for documents
   Set mLetter = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
   'Initialize Event dataset
   Set mReferenceView = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
   
   Set InstancePaymentData = NewGenericDataset(DatabaseName.INSTANCEPAYMENT_VIEW)
   
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyE 'Event
        mnuFileNewDetail_Click EventTab
      Case vbKeyD 'Document
        mnuFileNewDetail_Click DocumentTab
      Case vbKeyR 'Law Error
        mnuFileNewDetail_Click LawErrorTab
      Case vbKeyA 'Law Area
        mnuFileNewDetail_Click LawAreaTab
    End Select
    Exit Sub
  End If
    
  Select Case KeyCode
    Case vbKeyInsert
      ' Custom insert key
      mnuFileNewDetail_Click SSTab.Tab
    Case vbKeyF1
      FormUtil.ShowHelp , Me.hWnd
  End Select
End Sub

Public Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    SSTab_Click mTab
  Else
    SSTab.Tab = mTab
  End If

  ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.ButtonHeight + 48

  ' Setup Toolbar.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width

  ' Custom Menu Shortcut Adjustments
  Me.KeyPreview = True

  mnuViewPrevious.Caption = mnuViewPrevious.Caption + Chr(9) + "Ctrl+<"
  mnuViewNext.Caption = mnuViewNext.Caption + Chr(9) + "Ctrl+>"
  
  'Hack for right aligned multi line label:
  lblCoverageCounselLabel.Caption = "Coverage" & vbCrLf & "Counsel"
  
  'Setup Form Manager
  FormMgr.Load Me

  FormUtil.EditLoadImages Me

  ' Refresh form with data
  FormRefresh

  FormUtil.TabControlEnable SSTab
  optStatus(1).Value = True
End Sub

Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub fraClaim_DblClick()
  mnuEditIncident_Click
End Sub



Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

'ver 2.00.0304 (jjm) WILMIC
Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub mnuChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'ver 2.00.0304 (jjm) WILMIC
Private Sub mnuEditBrowseRow_Click()
  Select Case SSTab.Tab
'    Case LawyerTab  'Lawyer
'      Browse BrowserServicePath & UrlName.LAWYEROPEN_SERVLET & ListItemText(lvView(LawyerTab).SelectedItem, DatabaseName.LAWYER_KEY)
'    Case ReserveTab  'Reserve
'      Browse BrowserServicePath & UrlName.CLAIMRESERVEOPEN_SERVLET & ListItemText(lvView(ReserveTab).SelectedItem, DatabaseName.CLAIMRESERVE_KEY)
  End Select

End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If

  If lvView(SSTab.Tab).ListItems.Count < 1 Then
    MsgBox "There are no entries to delete."
    Exit Sub
  End If

  Select Case SSTab.Tab
'    Case ContactTab
'      'ver 2.10.0326
'      Factory.ContactService.DeleteContact ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.CONTACT_KEY), vbNullStringArray, vbNullStringArray
'    Case EventTab
  End Select
  
  'ver 2.71.00 (jjm) 2005 02 15
  mDirty = True

  FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case ContactTab
      FormUtil.ShowByListView New frmContactEdit, FormMgr, lvView(ContactTab)
    Case EventTab 'Event
      If Factory.EventService.IsEventTypeInGroup(ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.EVENTTYPE_KEY), DatabaseName.EVENT_TABLE) Then
        FormUtil.ShowByPrimaryKey New frmEventEdit, ListItemText(lvView(EventTab).SelectedItem, DatabaseName.EVENT_KEY), FormMgr, , DatabaseName.EVENT_TABLE
      Else
        FormUtil.ShowEventProperties ListItemText(lvView(EventTab).SelectedItem, DatabaseName.EVENT_KEY)
      End If
    Case DocumentTab  'Letter
       FormUtil.ShowByListView New frmEventDocumentEdit, FormMgr, lvView(DocumentTab)
    Case LawAreaTab  'Law Area
      mnuFileNewDetail_Click LawAreaTab
    Case LawErrorTab  'Law Error
      mnuFileNewDetail_Click LawErrorTab
    Case UnallocatedTab
      FormUtil.ShowByListView New frmInstancePaymentEdit, FormMgr, lvView(UnallocatedTab)
  End Select
End Sub

Private Sub mnuEditEventRow_Click(Index As Integer)
  Dim Handles(0) As String
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  
  Handles(0) = ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.EVENT_KEY)
'  Factory.FolderParameterService.ParameterLoad ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.FOLDER_KEY), Index, ParameterNames, ParameterValues, True

  If Index = lawActionComplete Then
    If MsgBox("Are you sure you want to complete item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
    End If
  ElseIf Index = lawActionVoid Then
    If MsgBox("Are you sure you want to void item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetVoid Handles, ParameterNames, ParameterValues
    End If
  End If
  
  'ver 2.71.00 (jjm) 2005 02 15
  mDirty = True
  
  FormRefresh

End Sub

Private Sub mnuEditIncident_Click()
  FormUtil.ShowByPrimaryKey New frmIncidentEdit, mIncidentHandle, FormMgr
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuEditNarrative_Click()
  Factory.IncidentService.ShowNarrative mIncidentHandle
  'ver 2.71.00 (jjm) 2005 02 15
  'No form refresh call so call suspend diary directly
  SuspendDiary
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNew_Click()
  FormUtil.ShowNew New frmIncidentEdit, FormMgr
End Sub

Private Sub mnuFileNewDetail_Click(Index As Integer)
  Select Case Index
    Case ContactTab
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowNewByIncident mIncidentHandle
    Case EventTab  'Event
      Dim fEvent As New frmEventEdit
      FormMgr.Add fEvent
      fEvent.ShowNew "IncidentEvent", mEventHandle, mIncidentNumber & " " & lblDataField(3).Caption
    Case DocumentTab  'Document
      Dim fLetter As New frmEventDocumentEdit
      FormMgr.Add fLetter, DatabaseName.EVENT_KEY & "New"
      fLetter.ShowNew DatabaseName.INCIDENTDOCUMENT_RULE, mEventHandle, lblDataField(3).Caption, , , mIncidentHandle
    Case LawAreaTab
      FormUtil.ShowByPrimaryKey New frmIncidentEdit, mIncidentHandle, FormMgr, 1
    Case LawErrorTab
      FormUtil.ShowByPrimaryKey New frmIncidentEdit, mIncidentHandle, FormMgr, 2
    Case UnallocatedTab
      FormUtil.ShowNew New frmInstancePaymentEdit, FormMgr, mIncidentHandle
  End Select
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0 'Claim 'ver 2.50.00 2004 02 15
      FormUtil.ShowByPrimaryKey New frmClaimOpen, mClaimHandle, FormMgr
    Case 1 'Firm
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
    Case 1 'Lawyer
      FormUtil.ShowByPrimaryKey New frmLawyerOpen, mLawyerHandle, FormMgr
    Case 2 'Policy
      FormUtil.ShowByPrimaryKey New frmPolicyOpen, mPolicyHandle, FormMgr
    
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.INCIDENT_TABLE, mIncidentHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
  'FormUtil.SendToFolder DatabaseName.CLAIM_REF, DatabaseName.CLAIM_TABLE, lbldatafield(0).caption, lbldatafield(0).caption & " " & lblDatafield(3).Caption
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  
  'ver 2.71.00 (jjm) 2005 02 15
  mDirty = True
  
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub
Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuPopupBrowse_Click()
  mnuEditBrowseRow_Click
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupEvent_Click(Index As Integer)
  mnuEditEventRow_Click Index
End Sub

Private Sub mnuPopupMerge_Click()
  mnuToolsMerge_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNewDetail_Click mTab
End Sub

Private Sub mnuPopupWebMerge_Click()
  mnuToolsWebMerge_Click
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.INCIDENT_TABLE, FormMgr
End Sub


Private Sub mnuToolsBrowse_Click(Index As Integer)
  Select Case Index
    Case 0
      Dispatcher.BrowseFirmOpen mFirmHandle
    Case 1
      Dispatcher.BrowseLawyerOpen mLawyerHandle
    Case 2
      Dispatcher.BrowsePolicyOpen mPolicyHandle
  End Select
End Sub

Private Sub mnuToolsMerge_Click()
  Dim pointer As New WaitPointer, Handles(0) As String
  pointer.Default

  Handles(0) = ListItemText(lvView(DocumentTab).SelectedItem, DatabaseName.EVENT_KEY)
  
  ' Print Envelopes
  Factory.EventRecipientService.PrintEnvelope Handles(0)

  ' Perform Document Merge
  Factory.EventService.DocumentMerge Handles
  
  'ver 2.71.00 (jjm) 2005 02 15
  mDirty = True
  
  FormRefresh
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWebMerge_Click()
  Factory.EventService.WebMergeByView lvView(DocumentTab)
End Sub

Private Sub mnuViewNext_Click()
  ' No validate on Open forms
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  ' No validate on Open forms
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  picBody_Resize
End Sub

Private Sub optStatus_Click(Index As Integer)
  mReferenceView.DataProvider.filter = EventFilter
  
  SSTabRefresh
  If Not ActiveControl Is Nothing Then
    optStatus(Index).SetFocus
  End If
End Sub

Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab
  mTab = SSTab.Tab
  
  Dim Source As DataProvider
  Set Source = ListViewDataProvider(mTab)
  If Not Source Is Nothing Then
    ' Ensure list view is loaded.
    ListViewLoad lvView(mTab), Source
    sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
    sbStatusBar.Panels(2).Text = Source.Source
    MenuRefresh
  End If
  
  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub

Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub

Private Sub tbrBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuToolsBrowse_Click 0
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuToolsBrowse_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEditIncident_Click
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
    Case "Incident"
      mnuEditIncident_Click
    Case "Narrative"
      mnuEditNarrative_Click
  End Select
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileNewDetail_Click SSTab.Tab
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNewDetail_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

