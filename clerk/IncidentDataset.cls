VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IncidentDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
Option Explicit

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer

Private Sub Class_Initialize()
  ListIndex = 4
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.INCIDENT_TABLE '"Incident"
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  frm.cboIn.AddItem "in adjuster field only"            '0
  frm.cboIn.AddItem "in claim number field only"        '1
  frm.cboIn.AddItem "in claimant field only"            '2
  frm.cboIn.AddItem "in firm identifier field only"     '3
  frm.cboIn.AddItem "in incident number field only"     '4
  frm.cboIn.AddItem "in incident firm name field only"  '5
  frm.cboIn.AddItem "in policy number field only"       '6
  frm.cboIn.AddItem "in tortfeasor name only"           '7
  
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, INCIDENTSEARCH_INCIDENTNUMBER_INDEX)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection

  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Select Case ListIndex
      Case INCIDENTSEARCH_ADJUSTER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ADJUSTER_FIELD, Words)
      Case INCIDENTSEARCH_CLAIMNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.CLAIMNUMBER_FIELD, Words)
      Case INCIDENTSEARCH_CLAIMANT_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.CLAIMANT_FIELD, Words)
      Case INCIDENTSEARCH_FIRMIDENTIFIER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.FIRM_KEY, Words)
      Case INCIDENTSEARCH_INCIDENTNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.INCIDENTNUMBER_FIELD, Words)
      Case INCIDENTSEARCH_FIRMNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.FIRMNAME_FIELD, Words)
      Case INCIDENTSEARCH_POLICYNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.POLICYNUMBER_FIELD, Words)
      Case INCIDENTSEARCH_TORTFEASOR_INDEX
        Dim subCol As Collection
        Dim mSubDataset As Dataset
        'Query InstanceLawyerView for the lawyer names
        Set mSubDataset = NewGenericDataset(DatabaseName.INSTANCELAWYER_VIEW)
        mSubDataset.DataProvider.Filter = LawDialect.ContainsAndArray(DatabaseName.LAWYERENTITYNAME_FIELD, Words) & " and " & LawDialect.Exists(DatabaseName.INCIDENT_KEY)
        mSubDataset.DataProvider.Column = DatabaseName.INSTANCE_KEY
        mSubDataset.DataProvider.OpenRecordset
        Set subCol = mSubDataset.DataProvider.Collection
          
        If subCol Is Nothing Then
          'test for sub query. Return zero results if no filter was created from the sub query
          Dim nullDP As New DataProvider
          nullDP.SetListView frm.lvView
          Exit Function
        Else
          'Parse sub query return
          Dim I As Integer
          For I = 1 To subCol.Count
            newFilter.AddOr DatabaseName.INCIDENT_KEY & " = '" & GetDataFromCollection(subCol(I), DatabaseName.INCIDENT_KEY) & "'"
          Next I
        End If
      
        'MsgBox "To Do: Need to enable multi table search"
        'newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LAWYER_KEY) & " in " & _
        '    "(select """ & DatabaseName.LAWYER_KEY & """ from """ & DatabaseName.LAWYER_TABLE & _
        '        """ where " & LawDialect.StartsWithUpperCase(DatabaseName.LAWYERENTITYNAME_FIELD) & " like '%" & UCase(Replace(Words, " ", "%")) & "%')"
    End Select
  End If
  
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function
