VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ViewItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Collection of folder"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"Folder"
Attribute VB_Ext_KEY = "Member0" ,"Folder"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable to hold collection
Private mCol As Collection

Public Function Add(Key As String, _
                    dsDataset As Dataset, _
                    Optional IsWorkflow As Boolean, _
                    Optional mIsSearchOnly As Boolean, _
                    Optional PopupMenuForm As Form, _
                    Optional PopupMenuName As String, _
                    Optional DefaultMenuName As String, _
                    Optional OpenMenuName As String, _
                    Optional ExploreMenuName As String, _
                    Optional NewItemName As String, _
                    Optional PreferenceNote As String) As View
        

  'create a new object
  Dim objNewMember As New View

  With objNewMember
    .Key = Key
    Set .Dataset = dsDataset
    .IsWorkflow = IsWorkflow
    .mIsSearchOnly = mIsSearchOnly
    .NewItemName = IIf(IsMissing(NewItemName), vbNullString, NewItemName)
    .PreferenceNote = IIf(IsMissing(PreferenceNote), vbNullString, PreferenceNote)
    If Not PopupMenuForm Is Nothing Then
      Set .PopupMenuForm = PopupMenuForm
      .PopupMenuName = PopupMenuName
      .DefaultMenuName = DefaultMenuName
      .OpenMenuName = OpenMenuName
      .ExploreMenuName = ExploreMenuName
      'If Not IsMissing(PropertiesMenuName) Then .PropertiesMenuName = PropertiesMenuName
    End If
  End With
  
  'set the properties passed into the method
  mCol.Add objNewMember, Key

  'return the object created
  Set Add = objNewMember
  Set objNewMember = Nothing
End Function


Public Property Get item(Index As Variant) As View
Attribute item.VB_UserMemId = 0
On Error Resume Next
  If Contains(mCol, Index) Then
    Set item = mCol(Index)
    Exit Property
  End If

  Err.Clear
End Property


Public Property Get Count() As Long
  Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
  mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
  'this property allows you to enumerate
  'this collection with the For...Each syntax
  Set NewEnum = mCol.[_NewEnum]
End Property

Public Property Get ContainsKey(Key As String) As Boolean
  ContainsKey = Not item(Key) Is Nothing
End Property


Private Sub Class_Initialize()
  'creates the collection when this class is created
  Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
  'destroys collection when this class is terminated
  Set mCol = Nothing
End Sub


