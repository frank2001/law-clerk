VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Begin VB.Form frmEventOnlineAppEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Online Application"
   ClientHeight    =   3105
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   8130
   Icon            =   "frmEventOnlineAppEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3105
   ScaleWidth      =   8130
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2985
      Left            =   0
      ScaleHeight     =   2985
      ScaleWidth      =   8130
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   390
      Width           =   8130
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   6810
         TabIndex        =   18
         Top             =   2220
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   5535
         TabIndex        =   17
         Top             =   2220
         Width           =   1200
      End
      Begin VB.Frame frmFirmInfo 
         Caption         =   "Firm Information"
         Height          =   675
         Left            =   135
         TabIndex        =   13
         Top             =   1380
         Width           =   7875
         Begin VB.CommandButton cmdFirm 
            Caption         =   "Select F&irm"
            Height          =   336
            Left            =   6375
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   210
            Width           =   1296
         End
         Begin VB.Label lblFirmNameTitle 
            AutoSize        =   -1  'True
            Caption         =   "Firm Name: "
            Height          =   195
            Left            =   150
            TabIndex        =   16
            Top             =   315
            Width           =   840
         End
         Begin VB.Label lblFirmName 
            AutoSize        =   -1  'True
            Caption         =   "firmName"
            Height          =   195
            Left            =   1035
            TabIndex        =   15
            Top             =   315
            UseMnemonic     =   0   'False
            Width           =   660
         End
      End
      Begin VB.Frame frmAppInfo 
         Caption         =   "Online Application"
         Height          =   885
         Left            =   135
         TabIndex        =   6
         Top             =   450
         Width           =   7860
         Begin VB.CommandButton cmdLawyer 
            Caption         =   "&Lawyer Lookup"
            Height          =   336
            Left            =   6375
            TabIndex        =   21
            Top             =   424
            Width           =   1296
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "eventDesc"
            DataField       =   "eventDesc"
            Height          =   195
            Index           =   3
            Left            =   3270
            TabIndex        =   20
            Top             =   240
            UseMnemonic     =   0   'False
            Width           =   780
         End
         Begin VB.Label lblFirm 
            AutoSize        =   -1  'True
            Caption         =   "Firm:"
            Height          =   195
            Left            =   2655
            TabIndex        =   19
            Top             =   240
            Width           =   330
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "eventTypeId"
            DataField       =   "eventTypeId"
            Height          =   195
            Index           =   2
            Left            =   1170
            TabIndex        =   12
            Top             =   240
            UseMnemonic     =   0   'False
            Width           =   900
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Type:"
            Height          =   195
            Left            =   150
            TabIndex        =   11
            Top             =   240
            Width           =   405
         End
         Begin VB.Label lblOnlineApp 
            AutoSize        =   -1  'True
            Caption         =   "Online App #:"
            Height          =   195
            Left            =   150
            TabIndex        =   10
            Top             =   495
            Width           =   975
         End
         Begin VB.Label lblLawyer 
            AutoSize        =   -1  'True
            Caption         =   "Lawyer:"
            Height          =   195
            Left            =   2655
            TabIndex        =   9
            Top             =   495
            Width           =   555
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "eventName"
            DataField       =   "eventName"
            Height          =   195
            Index           =   0
            Left            =   3270
            TabIndex        =   8
            Top             =   495
            UseMnemonic     =   0   'False
            Width           =   825
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "eventLocator"
            DataField       =   "eventLocator"
            Height          =   195
            Index           =   1
            Left            =   1170
            TabIndex        =   7
            Top             =   495
            UseMnemonic     =   0   'False
            Width           =   945
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   135
         Top             =   2220
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   735
         Top             =   2220
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.Label lblError 
         AutoSize        =   -1  'True
         Caption         =   "Select a firm to match the given information."
         Height          =   195
         Left            =   105
         TabIndex        =   5
         Top             =   105
         Width           =   4305
         WordWrap        =   -1  'True
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   276
      TabIndex        =   1
      Top             =   780
      Width           =   1056
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8130
      _ExtentX        =   14340
      _ExtentY        =   688
      BandCount       =   2
      _CBWidth        =   8130
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinWidth1       =   3600
      MinHeight1      =   330
      Width1          =   6000
      NewRow1         =   0   'False
      Child2          =   "picUpDown"
      MinHeight2      =   330
      Width2          =   7710
      NewRow2         =   0   'False
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   5805
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   30
         Width           =   5805
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1875
            TabIndex        =   23
            Top             =   30
            Width           =   1170
            _ExtentX        =   2064
            _ExtentY        =   1164
            ButtonWidth     =   1720
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Object.ToolTipText     =   "Browse Online Application..."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "OnlineApp"
                        Text            =   "Online Application"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   330
            Left            =   990
            TabIndex        =   24
            Top             =   30
            Width           =   1065
            _ExtentX        =   1879
            _ExtentY        =   582
            ButtonWidth     =   1244
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   330
            Left            =   30
            TabIndex        =   25
            Top             =   30
            Width           =   1005
            _ExtentX        =   1773
            _ExtentY        =   582
            ButtonWidth     =   1349
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "New Firm..."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   6195
         ScaleHeight     =   330
         ScaleWidth      =   1845
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   30
         Width           =   1845
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   72
            TabIndex        =   4
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Firm"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Firm"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparatro2823 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "Browse &Online Application"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator8371 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator86732 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeparator86733 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator365 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmEventOnlineAppEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.11.0603 MR (jjm)
'previous ver 2.10.0417 (jjm)
'previous version was ancient
Option Explicit

Private mFirmEventHandle As String

Private mEventHandle As String

' ListView integration
Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mCancel As Boolean

Private mLeft As Integer
Private mTop As Integer

' Form Manager
Private FormMgr As New FormManager

Private Const NEWMENU_FIRM = 0
Private Const EDITMENU_FIRM = 0
Private Const EDITMENU_BROWSEONLINEAPPLICATION = 1
Private Const DATAFIELD_EVENTNAME = 0
Private Const DATAFIELD_EVENTLOCATOR = 1

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty
  End If
End Property

'Set the firm name display when the firm handle is set
Private Property Let FirmEventHandle(vData As String)
  If mFirmEventHandle <> vData Then
    mFirmEventHandle = vData
    Dirty = True
  End If
  
  FirmRefresh
End Property

Private Property Get FirmEventHandle() As String
  FirmEventHandle = mFirmEventHandle
End Property

Private Function getEventIdFromFirmId(firmId As String)
  getEventIdFromFirmId = DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, _
                                             DatabaseName.ENTITY_KEY, _
                                             firmId, _
                                             DatabaseName.EVENT_KEY)
End Function

Private Function getFirmId()
  getFirmId = DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, _
                                               DatabaseName.EVENT_KEY, _
                                               FirmEventHandle, _
                                               DatabaseName.ENTITY_KEY)
End Function

Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
End Property

Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Child is either a Firm
' select (Firm) from cmdFirm or a new firm (NewFirm) from mnuFileNewFirm.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form requested new firm.
    Case fimNew
      mnuFileNewOther_Click NEWMENU_FIRM
      Exit Sub
    
    ' Select form selected
    Case fimSelect
      Dim firmId As String
      firmId = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRM_KEY)
      FirmEventHandle = getEventIdFromFirmId(firmId)
      
    ' New Firm has been saved via mnuFileNewFirm.
    Case fimSave
      If fInterface.Key = DatabaseName.FIRM_TABLE & "Edit" Then
        FirmEventHandle = getEventIdFromFirmId(fInterface.Child.organizationHandle)
      End If
  End Select
  Show
End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub


Private Sub FormRefresh()
  SetFields lbldatafield, Factory.EventService.Find(mEventHandle)
  
  'setup firm
  FirmEventHandle = Factory.ReferenceService.FindReferenceHandleByEventType(mEventHandle, DatabaseName.FIRM_TYPE)
  
  Dirty = False

  ' Standard Navigation Update for all Edit form mnuViewRefresh. (up / down arrows)
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
End Sub


Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And Verify Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Start custom Code
  If FirmEventHandle = vbNullString Then
    MsgBox "A firm must be selected to save."
    Exit Function
  End If
  
  
  '
  ' Save Data
  '
  Save = Factory.EventService.SetReference(mEventHandle, FirmEventHandle)
  
  If Save Then
    FormMgr.Message = fimSave
  End If
  Dirty = Not Save
End Function
Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no events to view or edit."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(eventId As String)
  mEventHandle = eventId
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub



Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Open up a select form for firm.  The response to the select
' is processed under FormChildUnload.
'
Public Sub cmdFirm_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True, , , , "Select a firm for the application"
End Sub

Private Sub cmdLawyer_Click()
  Dispatcher.ShowSearch DatabaseName.LAWYER_TABLE, FormMgr, False, , 2, lbldatafield(DATAFIELD_EVENTNAME).Caption, "Lookup lawyer for reference"
End Sub

Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub FirmRefresh()
  Dim firmName As String
  firmName = vbNullString
  If FirmEventHandle <> vbNullString Then
    firmName = DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, _
                                   DatabaseName.EVENT_KEY, _
                                   FirmEventHandle, _
                                   DatabaseName.ENTITYNAME_FIELD)
  End If
  lblFirmName = firmName
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0) Then
    'catch new menu items
    If ((Shift And vbShiftMask) > 0) Then
      Select Case KeyCode
        Case vbKeyF
          mnuFileNewOther_Click 1
          KeyCode = 0
      End Select
      Exit Sub
    End If
  
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
  End If
  
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  'Custom Accelerators
  KeyPreview = True
  
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
 
  FormRefresh
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub


Private Sub Form_Resize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub



Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0
      If mFirmEventHandle = vbNullString Then
        MsgBox "No firm has been associated with this application.  Either insert or select a firm."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmOrganizationEdit, getFirmId(), FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
    Case 1
       'browse online application
      OnlineAppUtil.browseOnlineApplication lbldatafield(DATAFIELD_EVENTLOCATOR).Caption
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub


Private Sub mnuFileNewOther_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowNew New frmOrganizationEdit, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub



Private Sub mnuFileOpenOther_Click(Index As Integer)
  Select Case Index
    Case 0
      If FirmEventHandle = vbNullString Then
        MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmFirmOpen, getFirmId(), FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowEventProperties mEventHandle
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub


Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  
  Dim fFolderList As New frmFolderListDialog
  'read files from ini and load into the list view
  With fFolderList.lvFolder
    .ListItems.Add , "Application", "Application"
    .ListItems("Application").ListSubItems.Add , "Location", IniMain.ReadFile("application", "applicationfolder")
  End With
  
  ListViewModule.AutosizeWidth fFolderList.lvFolder
  fFolderList.Show vbModal, Me
  
  If fFolderList.Ok Then
    'write folders to ini
    IniMain.WriteFile "application", "applicationfolder", fFolderList.lvFolder.ListItems("Application").ListSubItems("Location")
  End If
  
  Unload fFolderList

End Sub


' Show By List View Functinality
Private Sub mnuView_Click()
  If (mListView Is Nothing) Then
    mnuViewPrevious.Enabled = False
    mnuViewNext.Enabled = False
    Exit Sub
  End If
  mnuViewPrevious.Enabled = mListView.Enabled And mListIndex > 1
  mnuViewNext.Enabled = mListIndex < mListView.ListItems.Count
End Sub
Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub
Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub

Private Sub picUpDown_Resize()
  tbrUpDown.Top = (picUpDown.Height - tbrUpDown.Height) / 2
End Sub



Private Sub tbrBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEdit_Click EDITMENU_BROWSEONLINEAPPLICATION
End Sub
Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
    Case "OnlineApp"
      mnuEdit_Click EDITMENU_BROWSEONLINEAPPLICATION
  End Select
End Sub
Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEdit_Click EDITMENU_FIRM
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
    Case "Firm"
      mnuEdit_Click EDITMENU_FIRM
  End Select
End Sub


Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileNewOther_Click NEWMENU_FIRM
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNewOther_Click ButtonMenu.Index
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub
