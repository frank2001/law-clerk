VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmClaimReserveEdit 
   Appearance      =   0  'Flat
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reserve"
   ClientHeight    =   4455
   ClientLeft      =   150
   ClientTop       =   750
   ClientWidth     =   6390
   Icon            =   "frmClaimReserveEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   6390
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4032
      Left            =   0
      ScaleHeight     =   4035
      ScaleWidth      =   6390
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   384
      Width           =   6396
      Begin VB.TextBox txtDataField 
         DataField       =   "eventTypeId"
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         DataMember      =   "Event"
         Enabled         =   0   'False
         Height          =   288
         Index           =   3
         Left            =   5484
         TabIndex        =   25
         Top             =   24
         Visible         =   0   'False
         Width           =   936
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   5136
         TabIndex        =   7
         Top             =   3660
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   2616
         TabIndex        =   5
         Top             =   3660
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   3876
         TabIndex        =   6
         Top             =   3660
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   2880
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   696
         Width           =   6276
         _ExtentX        =   11060
         _ExtentY        =   5080
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Properties"
         TabPicture(0)   =   "frmClaimReserveEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label5"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label4"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label3"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label2"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lvGL"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtpDataField(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtDataField(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtDataField(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "cboEventTypeName"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         Begin VB.ComboBox cboEventTypeName 
            Height          =   288
            ItemData        =   "frmClaimReserveEdit.frx":0028
            Left            =   4308
            List            =   "frmClaimReserveEdit.frx":002A
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Tag             =   "ClaimReserveEditEventTypeName"
            Top             =   552
            Width           =   1896
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "estimateIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0;(#,##0)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "ClaimReserve"
            Height          =   288
            Index           =   2
            Left            =   2856
            TabIndex        =   2
            Top             =   552
            Width           =   1284
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "estimateALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0;(#,##0)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "ClaimReserve"
            Height          =   288
            Index           =   1
            Left            =   1476
            TabIndex        =   1
            Top             =   552
            Width           =   1176
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmClaimReserveEdit.frx":002C
            DataField       =   "eventDate"
            DataMember      =   "Event"
            Height          =   288
            Index           =   0
            Left            =   108
            TabIndex        =   0
            Top             =   552
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   503
            _Version        =   393216
            Format          =   81526785
            CurrentDate     =   37118
         End
         Begin MSComctlLib.ListView lvGL 
            Height          =   1572
            Left            =   96
            TabIndex        =   4
            Tag             =   "ColumnFixed"
            Top             =   1212
            Width           =   6084
            _ExtentX        =   10742
            _ExtentY        =   2778
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "glAccountId"
               Text            =   "Account"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "transactionName"
               Text            =   "Name"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "transactionReference"
               Text            =   "Reference"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "transactionAmount"
               Text            =   "Amount"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "~debit"
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Debit"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "~credit"
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Credit"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label Label1 
            Caption         =   "Reserve Date"
            Height          =   192
            Left            =   120
            TabIndex        =   15
            Top             =   324
            Width           =   1128
         End
         Begin VB.Label Label2 
            Caption         =   "Reserve Type ID"
            Height          =   192
            Left            =   4320
            TabIndex        =   14
            Top             =   324
            Width           =   1260
         End
         Begin VB.Label Label3 
            Caption         =   "Reserve ALAE"
            Height          =   192
            Left            =   1476
            TabIndex        =   13
            Top             =   324
            Width           =   1368
         End
         Begin VB.Label Label4 
            Caption         =   "Indemnity"
            Height          =   192
            Left            =   2856
            TabIndex        =   12
            Top             =   324
            Width           =   1536
         End
         Begin VB.Label Label5 
            Caption         =   "G\L"
            Height          =   192
            Left            =   120
            TabIndex        =   11
            Top             =   984
            Width           =   1128
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   300
         Top             =   3432
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   888
         Top             =   3468
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.CommandButton cmdEnter 
         Caption         =   "Enter"
         Height          =   288
         Left            =   192
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1080
         Width           =   1056
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "claimant"
         DataField       =   "claimant"
         DataMember      =   "Instance"
         Height          =   192
         Index           =   1
         Left            =   1260
         TabIndex        =   24
         Top             =   312
         UseMnemonic     =   0   'False
         Width           =   600
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "firmName"
         DataField       =   "firmName"
         DataMember      =   "Instance"
         Height          =   195
         Index           =   0
         Left            =   1260
         TabIndex        =   23
         Top             =   75
         UseMnemonic     =   0   'False
         Width           =   660
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Claim Name:"
         Height          =   192
         Left            =   156
         TabIndex        =   22
         Top             =   72
         Width           =   924
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Claimant"
         Height          =   192
         Left            =   156
         TabIndex        =   21
         Top             =   312
         Width           =   624
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   6396
      _ExtentX        =   11271
      _ExtentY        =   688
      BandCount       =   2
      BandBorders     =   0   'False
      _CBWidth        =   6390
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picBrowseBand"
      MinWidth1       =   600
      MinHeight1      =   315
      Width1          =   4845
      NewRow1         =   0   'False
      Child2          =   "picUpDown"
      MinWidth2       =   210
      MinHeight2      =   330
      Width2          =   3990
      NewRow2         =   0   'False
      Begin VB.PictureBox picBrowseBand 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   165
         ScaleHeight     =   315
         ScaleWidth      =   4650
         TabIndex        =   19
         Top             =   30
         Width           =   4650
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   0
            TabIndex        =   20
            Top             =   15
            Width           =   1155
            _ExtentX        =   2037
            _ExtentY        =   1164
            ButtonWidth     =   1720
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Claim"
                        Object.Tag             =   "0"
                        Text            =   "Claim"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Reserve"
                        Object.Tag             =   "1"
                        Text            =   "Reserve"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   5010
         ScaleHeight     =   330
         ScaleWidth      =   1290
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   30
         Width           =   1290
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   24
            TabIndex        =   18
            Top             =   24
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator643 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator503 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "Browse &Claim"
         Index           =   0
      End
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "Browse &Reserve"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator382 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator983 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolsNotepad 
         Caption         =   "&Notepad"
      End
      Begin VB.Menu mnuSeperator988 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator987 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmClaimReserveEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 1.90.1231

Option Explicit

Private mClaimReserveHandle As String
Private mClaimHandle As String
Private mEventHandle As String

Private mNew As Boolean

Private mListItemType As String

Private mData As DataProvider

Private glData As Dataset

Private mEventTypes As Collection

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private FormMgr As New FormManager


Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


'
'FormInterface Interface
'


' FormLoad is called after Form_Initialize and before Form_Load

Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
End Sub

' End FormInterface

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  If (mNew) Then
    Caption = "New Reserve"
    FormUtil.GetComboDefault cboEventTypeName
  Else
    Dim Datafields As Collection
    Set Datafields = mData.Find(DatabaseName.CLAIMRESERVE_KEY, mClaimReserveHandle)
    If Datafields Is Nothing Then
      MsgBox "Data not found. Closing form.", vbCritical
      Unload Me
      Exit Sub
    End If
    
    'set Claim payment info
    Caption = "Edit Reserve (" & mClaimReserveHandle & ")"
    mClaimHandle = Datafields(DatabaseName.CLAIM_KEY)
    SetFields txtDataField, Datafields, "ClaimReserve"
    'Event Info
    mEventHandle = Datafields(DatabaseName.EVENT_KEY)
    Dim EventData As Collection
    Set EventData = Factory.EventService.Find(mEventHandle)
    SetFields dtpDataField, EventData, "Event"
    SetFields txtDataField, EventData, "Event"
    'Instance Info
    Dim InstanceData As Collection
    Set InstanceData = Factory.InstanceService.Find(mClaimHandle)
    SetFields lbldatafield, InstanceData, "Instance"
  End If
  
  
  ' ver 1.90.1226
  ' corrected filter to DatabaseName.EVENT_KEY
  glData.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle)
  ListViewLoad lvGL, glData.DataProvider, True
  FormUtil.GLTransactionSummary lvGL.ListItems

  Dim item As ListItem
  For Each item In lvGL.ListItems
    Dim tranAmount As Double
    tranAmount = GetDouble(item.ListSubItems(DatabaseName.TRANSACTIONAMOUNT_FIELD))
    If tranAmount < 0 Then
      item.SubItems(5) = Format(0 - tranAmount, "#,###.00;(#,###.00)")
    Else
      item.SubItems(4) = Format(tranAmount, "#,###.00;(#,###.00)")
    End If
  Next item
  
  '
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  '
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' After refresh, record is not changed if existing, changed if new
  Dirty = False
End Sub


Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mClaimReserveHandle = ListItemText(mListView.SelectedItem, DatabaseName.CLAIMRESERVE_KEY)
  Select Case mListItemType
    Case DatabaseName.CLAIMRESERVE_TABLE
      mClaimReserveHandle = ListItemText(mListView.SelectedItem, DatabaseName.CLAIMRESERVE_KEY)
    Case DatabaseName.EVENT_TABLE
      MsgBox "ToDo: Enable displaying reserves from an event source"
      'mClaimReserveHandle = Factory.ReferenceService.Find(ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY), DatabaseName.CLAIMRESERVE_TABLE)
  End Select
End Property

Private Function save(Optional Verify As Boolean) As Boolean
    ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
End Function

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.CLAIMRESERVE_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no insurers to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowNew(ClaimHandle As String)
  mClaimHandle = ClaimHandle
  mNew = True
  Show
End Sub

Public Sub ShowByPrimaryKey(ClaimReserveHandle As String)
  mClaimReserveHandle = ClaimReserveHandle
  Show
End Sub

Private Sub cbrCoolBar_Resize()
  FormResize
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSaveClose_Click()
  save
  Unload Me
End Sub

Private Sub Form_Initialize()
  Set mData = LawSession.NewDataProvider(DatabaseName.CLAIMRESERVE_TABLE)
  
  Set glData = NewGenericDataset(DatabaseName.GLTRANSACTION_VIEW)
  'Set glData = New GLTransactionDataset
  'Set glData.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        mnuViewNext_Click
      Case 188
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me

  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 48
  cbrCoolBar.Bands(2).MinHeight = tbrBrowse.ButtonHeight + 48
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  Set mEventTypes = Factory.EventTypeService.SourceCollection(DatabaseName.CLAIMRESERVE_TABLE)
  FormUtil.ComboBoxLoad mEventTypes, cboEventTypeName, DatabaseName.EVENTTYPENAME_FIELD
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvGL_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'  ListViewSort lvGL, ColumnHeader, glData.DataProvider
'  glData.DataProvider.Sort = _
'      IIf(glData.DataProvider.Sort = """" & ColumnHeader & """", _
'          """" & ColumnHeader & """" & " DESC", _
'          """" & ColumnHeader & """")
'  glData.DataProvider.OpenRecordset
'  glData.DataProvider.SetListView lvGL, False
End Sub
'
'Private Sub lvGL_DblClick()
'  If lvGL.ListItems.Count = 0 Then Exit Sub
'  cmdProperties_Click
'End Sub
Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub
Private Sub mnuFileNew_Click()
  mNew = True
  mEventHandle = vbNullString
  mnuViewRefresh_Click
  'cmdInsurer_Click
End Sub

Private Sub mnuFileProperties_Click()
  'FormUtil.ShowEventProperties mEventHandle
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.CLAIMRESERVE_TABLE, mClaimReserveHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsBrowse_Click(Index As Integer)
  Select Case Index
    Case 0 ' Claim
      Dispatcher.BrowseClaimOpen mClaimHandle
    Case 1 ' Claim Reserve
      Dispatcher.BrowseClaimReserveOpen mClaimReserveHandle
  End Select
End Sub

Private Sub mnuView_Click()
  If (mListView Is Nothing) Then
    mnuViewPrevious.Enabled = False
    mnuViewNext.Enabled = False
    Exit Sub
  End If
  mnuViewPrevious.Enabled = mListView.Enabled And mListIndex > 1
  mnuViewNext.Enabled = mListIndex < mListView.ListItems.Count
End Sub

Private Sub mnuViewNext_Click()
  mnuView_Click
  If (Not mnuViewNext.Enabled) Then
    Exit Sub
  End If
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  mnuView_Click
  If (Not mnuViewPrevious.Enabled) Then
    Exit Sub
  End If
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuToolsBrowse_Click ButtonMenu.Tag
End Sub

Private Sub tbrBrowse_Click()
  ' Default to claim reserve.
  mnuToolsBrowse_Click 1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
  Select Case Index
    Case 3
      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, txtDataField(Index).Text) - 1
  End Select

End Sub

'
'Public Sub Edit(PrimaryKey As Collection, Optional IsNew As Boolean = False)
'  Set mvarPrimaryKey = PrimaryKey
'
'  Dim DataFields As Collection
'  Set DataFields = mDataset.FindByPrimaryKey(PrimaryKey)
'  mEventHandle = DataFields(databasename.EVENT_KEY)
'
'  Call SetFields(Me.txtDataField, DataFields)
'  Call SetFields(Me.dtpDataField, DataFields)
'
'  glData.DataProvider.Filter = """mEventHandle"" = '" & mEventHandle & "'"
'  glData.DataProvider.OpenRecordset
'  glData.DataProvider.SetListView lvGL, False
'
'  Dim Item As ListItem
'  For Each Item In lvGL.ListItems
'    If Item.ListSubItems("transactionAmount") < 0 Then
'      Item.SubItems(5) = Format(0 - Item.ListSubItems("transactionAmount"), "#,###.00;(#,###.00)")
'    Else
'      Item.SubItems(4) = Format(Item.ListSubItems("transactionAmount"), "#,###.00;(#,###.00)")
'    End If
'  Next Item
'End Sub
'
'Private Sub Save()
'  DataProvider_Params
'End Sub
'
'Private Sub DataProvider_Params()
'  Dim DataProvider_Params As String
'  Dim lvCount, n As Integer
'  Dim DataFields As Collection
'
'  lvCount = lvGL.ListItems.Count
'
'  DataProvider_Params = ""
'  DataProvider_Params = "content=text/xml&mEventHandle=" & mEventHandle
'  DataProvider_Params = DataProvider_Params & "&entryDate=2001-08-01"
'  DataProvider_Params = DataProvider_Params & "&fiscalYear=2001"
'  DataProvider_Params = DataProvider_Params & "&fiscalPeriod=08"
'  For n = 1 To lvCount
'     DataProvider_Params = DataProvider_Params & "&glAccountId_" & n & "=" & lvGL.ListItems(n).Text
'     DataProvider_Params = DataProvider_Params & "&transactionName_" & n & "=" & lvGL.ListItems(n).ListSubItems("transactionName").Text
'     DataProvider_Params = DataProvider_Params & "&transactionReference_" & n & "=" & lvGL.ListItems(n).ListSubItems("transactionReference").Text
'     DataProvider_Params = DataProvider_Params & "&transactionAmount_" & n & "=" & lvGL.ListItems(n).ListSubItems("transactionAmount").Text
'  Next n
'
'  Dim xmlHttp As New xmlHttp
'  Dim xmlDataProvider As MSXML2.DOMDocument
'  xmlHttp.open "POST", "http://192.168.10.51/malcolm/servlet/claimreserveedit", False
'  xmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
'  xmlHttp.send DataProvider_Params
'
'  Set xmlDataProvider = xmlHttp.responseXML
'  If xmlDataProvider.parseError.srcText <> "" Then
'    MsgBox xmlDataProvider.parseError.srcText & " at line " & xmlDataProvider.parseError.Line
'  End If
'End Sub
'
'Private Sub cmdAdd_Click()
'  Dim fGL As New frmGLTransactionEdit
'  Set fGL.ListView = lvGL
'  fGL.Show vbModal, Me
'  Unload fGL
'End Sub
'
'Private Sub cmdRemove_Click()
'  If lvGL.ListItems.Count = 0 Then Exit Sub
'  lvGL.ListItems.Remove (lvGL.SelectedItem.Index)
'End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub
