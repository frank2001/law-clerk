VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EventRecipientService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "EventRecipientService"
Private mDataSet As Dataset
Private mEnvelope As MailLabel


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.EVENTRECIPIENT_TABLE)
  Set mEnvelope = New MailLabel

On Error GoTo ErrorHandler
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.EVENTRECIPIENT_KEY, Handle, Column)
End Function

Public Function GetEnvelope(Handle As String, Optional Column As String, Optional ForceEnvelope As Boolean) As Collection
  mDataSet.DataProvider.Filter = DatabaseName.EVENT_KEY & " = '" & Handle & "'" & _
                                 IIf(ForceEnvelope, _
                                     vbNullString, _
                                     " and " & DatabaseName.ISENVELOPE_FIELD & " = '" & DB_TRUE_VALUE & "'")
  mDataSet.DataProvider.Column = IIf(Column = vbNullString, _
                                     DatabaseName.EVENTRECIPIENT_KEY & ", " & DatabaseName.DELIVERYADDRESS_FIELD & ", " & DatabaseName.DELIVERYPOINTCODE_FIELD, _
                                     Column)
  mDataSet.DataProvider.Sort = vbNullString
  mDataSet.DataProvider.OpenRecordset
  Set GetEnvelope = mDataSet.DataProvider.Collection
End Function

Public Sub PrintEnvelope(Handle As String, Optional ForceEnvelope As Boolean)
  Dim AddressCollection As Collection
  Set AddressCollection = GetEnvelope(Handle, , ForceEnvelope)
  If AddressCollection Is Nothing Then
    Exit Sub
  End If
      
  Dim I As Integer
  For I = 1 To AddressCollection.Count
    'ver 2.50.00
    If mEnvelope.PrintEnvelope(CStr(AddressCollection(I)("deliveryAddress")), LocaleEnvelopeBarCode = 1, CStr(AddressCollection(I)("deliveryPointCode"))) Then
      SetEnvelope AddressCollection(I)(DatabaseName.EVENTRECIPIENT_KEY).Value, False
    End If
  Next I
End Sub

'
' Interface to server
'
'
' Create an EventRecipient
'
'Public Function CreateEventRecipient( _
'    eventId As String, _
'    EventRecipientTypeId As String, _
'    EventRecipientSeq As Double, _
'    contactId As String, _
'    PersonId As String, _
'    LocationId As String, _
'    RecipientName As String, _
'    RecipientSalutation As String, _
'    CompanyName As String, _
'    AddressLine1 As String, _
'    AddressLine2 As String, _
'    AddressLine3 As String, _
'    City As String, _
'    State As String, _
'    Zip As String, _
'    Country As String, _
'    CarrierRoute As String, _
'    DeliveryPointCode As String, _
'    DeliveryAddress As String, _
'    Envelope As Boolean, _
'    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
'    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As String
'
'On Error GoTo ErrorHandler
'
'  CreateEventRecipient = Client.Create( _
'    eventId, _
'    contactId, _
'    PersonId, _
'    LocationId, _
'    EventRecipientTypeId, _
'    EventRecipientSeq, _
'    RecipientName, _
'    RecipientSalutation, _
'    CompanyName, _
'    AddressLine1, _
'    AddressLine2, _
'    AddressLine3, _
'    City, _
'    State, _
'    Zip, _
'    Country, _
'    CarrierRoute, _
'    DeliveryPointCode, _
'    DeliveryAddress, _
'    Envelope, _
'    AttributeNames, AttributeValues, _
'    ParameterNames, ParameterValues)
'  Exit Function
'
'ErrorHandler:
'  ErrorUtil.DisplaySoapError Err, Client, ServiceName
'
'End Function

'
' Write an EventRecipient.
'
'Public Function WriteEventRecipient( _
'    EventRecipientId As String, _
'    EventRecipientTypeId As String, _
'    EventRecipientSeq As Double, _
'    contactId As String, _
'    PersonId As String, _
'    LocationId As String, _
'    RecipientName As String, _
'    RecipientSalutation As String, _
'    CompanyName As String, _
'    AddressLine1 As String, _
'    AddressLine2 As String, _
'    AddressLine3 As String, _
'    City As String, _
'    State As String, _
'    Zip As String, _
'    Country As String, _
'    CarrierRoute As String, _
'    DeliveryPointCode As String, _
'    DeliveryAddress As String, _
'    Envelope As Boolean, _
'    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
'    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
'
'On Error GoTo ErrorHandler
'
'  Call Client.Write( _
'      EventRecipientId, _
'      contactId, _
'      PersonId, _
'      LocationId, _
'      EventRecipientTypeId, _
'      EventRecipientSeq, _
'      RecipientName, _
'      RecipientSalutation, _
'      CompanyName, _
'      AddressLine1, _
'      AddressLine2, _
'      AddressLine3, _
'      City, _
'      State, _
'      Zip, _
'      Country, _
'      CarrierRoute, _
'      DeliveryPointCode, _
'      DeliveryAddress, _
'      Envelope, _
'      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
'  WriteEventRecipient = True
'  Exit Function
'
'ErrorHandler:
'  ErrorUtil.DisplaySoapError Err, Client, ServiceName
'  WriteEventRecipient = False
'  Err.Clear
'End Function

Public Function SetEnvelope(Handle As String, Envelope As Boolean) As Boolean
    
On Error GoTo ErrorHandler
  
  Client.SetEnvelope Handle, Envelope
  SetEnvelope = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  SetEnvelope = False
  Err.Clear
End Function

Public Function SetPerson(EventRecipientHandle As String, PersonHandle As String) As Boolean
    
On Error GoTo ErrorHandler
  
  Client.SetPerson EventRecipientHandle, PersonHandle
  SetPerson = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  SetPerson = False
End Function

'
' Delete an EventRecipient
'
Public Function DeleteEventRecipient( _
    EventRecipientHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler

  Call Client.Delete(EventRecipientHandle, ParameterNames, ParameterValues)
  DeleteEventRecipient = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  DeleteEventRecipient = False
End Function
