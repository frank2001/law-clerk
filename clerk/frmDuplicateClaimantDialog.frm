VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Begin VB.Form frmDuplicateClaimantDialog 
   Caption         =   "Duplicate Claimants"
   ClientHeight    =   6930
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7635
   LinkTopic       =   "Form1"
   ScaleHeight     =   6930
   ScaleWidth      =   7635
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6930
      Left            =   0
      ScaleHeight     =   6930
      ScaleWidth      =   7635
      TabIndex        =   0
      Top             =   0
      Width           =   7635
      Begin VB.Frame fraIncident 
         Caption         =   "Incident"
         Height          =   2610
         Left            =   120
         TabIndex        =   6
         Top             =   3225
         Width           =   7410
         Begin MSComctlLib.ListView lvIncident 
            Height          =   2295
            Left            =   105
            TabIndex        =   7
            Tag             =   "ClaimClaimContact"
            Top             =   195
            Width           =   7185
            _ExtentX        =   12674
            _ExtentY        =   4048
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "incidentId"
               Text            =   "Incident ID"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "claimant"
               Text            =   "Claimant"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "firmName"
               Text            =   "Firm Name"
               Object.Width           =   5292
            EndProperty
         End
      End
      Begin VB.Frame fraClaim 
         Caption         =   "Claim"
         Height          =   2610
         Left            =   120
         TabIndex        =   4
         Top             =   495
         Width           =   7410
         Begin MSComctlLib.ListView lvClaim 
            Height          =   2295
            Left            =   105
            TabIndex        =   5
            Tag             =   "ClaimClaimContact"
            Top             =   195
            Width           =   7185
            _ExtentX        =   12674
            _ExtentY        =   4048
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "claimId"
               Text            =   "Claim ID"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "claimant"
               Text            =   "Claimant"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "firmName"
               Text            =   "Firm Name"
               Object.Width           =   5292
            EndProperty
         End
      End
      Begin VB.CommandButton cmdNo 
         Caption         =   "&No"
         Height          =   336
         Left            =   6225
         TabIndex        =   2
         Top             =   6435
         Width           =   1296
      End
      Begin VB.CommandButton cmdYes 
         Caption         =   "&Yes"
         Height          =   336
         Left            =   4800
         TabIndex        =   1
         Top             =   6435
         Width           =   1296
      End
      Begin VB.Label Label1 
         Caption         =   "Do you wish to continue inserting this record with this claimant?"
         Height          =   195
         Left            =   285
         TabIndex        =   8
         Top             =   6075
         Width           =   4590
      End
      Begin VB.Label lblError 
         AutoSize        =   -1  'True
         Caption         =   "A similar climant has been found in the following instances:"
         Height          =   195
         Left            =   285
         TabIndex        =   3
         Top             =   165
         Width           =   4305
         WordWrap        =   -1  'True
      End
   End
End
Attribute VB_Name = "frmDuplicateClaimantDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mContinue As Boolean

Public Sub setClaimData(ClaimData As DataProvider)
  ListViewLoad lvClaim, ClaimData, True
End Sub

Public Sub setIncidentData(IncidentData As DataProvider)
  ListViewLoad lvIncident, IncidentData, True
End Sub

Public Property Get Continue() As Boolean
  Continue = mContinue
End Property

Private Sub cmdNo_Click()
  Hide
End Sub

Private Sub cmdYes_Click()
  mContinue = True
  Hide
End Sub

Private Sub lvClaim_DblClick()
  Dispatcher.BrowseClaimOpen ListItemText(lvClaim.SelectedItem, DatabaseName.CLAIM_KEY)
End Sub

Private Sub lvIncident_DblClick()
  'Browse HttpServer &  WebService & "incidentopen?key=" + ListItemText(lvIncident.SelectedItem, DatabaseName.INCIDENT_KEY)
End Sub

