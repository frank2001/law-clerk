VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InstanceDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private mDiaryProvider As DataProvider
Private Words As String
Private ListIndex As Integer

Private Sub Class_Initialize()
  ListIndex = 1
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider(DatabaseName.INSTANCE_VIEW)
  Set mDiaryProvider = vData.NewDataProvider(IniFolder.ReadFile(UrlName.MISC_INI, UrlName.INSTANCEDIARYSEARCHVIEW_INI))
  
End Property

Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property

Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property

Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in claimant field only"
  frm.cboIn.AddItem "in claim number field only"
  frm.cboIn.AddItem "in firm entity locator field only"
  frm.cboIn.AddItem "in firm name field only"
  frm.cboIn.AddItem "in incident number field only"
  frm.cboIn.AddItem "in instance identifier field only"
  frm.cboIn.AddItem "in policy number field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, 1)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case 0
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.CLAIMANT_FIELD, Words)
      Case 1
        newFilter.Add LawDialect.StartsWith(DatabaseName.CLAIMNUMBER_FIELD, Words)
      Case 2
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.FIRMENTITYLOCATOR_FIELD, Words)
      Case 3
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.FIRMNAME_FIELD, Words)
      Case 4
        newFilter.Add LawDialect.StartsWith(DatabaseName.INCIDENTNUMBER_FIELD, Words)
      Case 5
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.INSTANCE_KEY, Words)
      Case 6
        newFilter.Add LawDialect.StartsWith(DatabaseName.POLICYNUMBER_FIELD, Words)
    End Select
  End If
    
  Dim provider As DataProvider
  Set provider = InstanceSearchProvider(frm, newFilter)
  provider.OpenRecordset
  provider.SetListView frm.lvView
End Function


Private Function InstanceSearchProvider(frm As Form, vFilter As filter) As DataProvider
  Dim provider As DataProvider

  'Default data provider
  Set provider = mDataProvider

  'advance search options
  If TypeOf frm Is frmGenericSearch Then
    If frm.chkACA.Value = vbChecked Then
      vFilter.Add LawDialect.EqualString(DatabaseName.ADJUSTER_FIELD, frm.cboAdjuster.Text)
    End If
    If frm.chkDiaryDate.Value = vbChecked Then
      vFilter.Add DatabaseName.DUEDATE_FIELD & " >= " & LawDialect.DateFormat(frm.dtpDiaryFromDate) & " and " & _
            DatabaseName.DUEDATE_FIELD & " <= " & LawDialect.DateFormat(frm.dtpDiaryToDate)
      Set provider = mDiaryProvider
    End If
    'hackish - should be done by frmGenericSearch
    frm.sbStatusBar.Panels(2).Text = provider.Source
  End If

  provider.filter = vFilter
    
  'return the provider
  Set InstanceSearchProvider = provider
  
End Function




