VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AutoKeyService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "AutoKeyService"
Private mDataSet As Dataset
Private mEnvelope As MailLabel


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  
  'Setup SOAP
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub


'
' Create an application.
'
Public Function GetDocumentKey() As String
On Error GoTo ErrorHandler
  
  GetDocumentKey = Client.GetDocumentKey()
  Exit Function
  
ErrorHandler:
'  MsgBox "AutoKey Get Document Key Service Error: " & Err.Description, vbExclamation
'  Err.Clear
  ErrorUtil.DisplaySoapError Err, Client
End Function
