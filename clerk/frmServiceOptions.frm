VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmServiceOptions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Service Options"
   ClientHeight    =   4284
   ClientLeft      =   36
   ClientTop       =   276
   ClientWidth     =   4380
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4284
   ScaleWidth      =   4380
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4248
      Left            =   0
      ScaleHeight     =   4248
      ScaleWidth      =   4380
      TabIndex        =   0
      Top             =   0
      Width           =   4380
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Default         =   -1  'True
         Height          =   336
         Left            =   1812
         TabIndex        =   3
         Top             =   3804
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   3096
         TabIndex        =   2
         Top             =   3792
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3564
         Left            =   60
         TabIndex        =   1
         Top             =   84
         Width           =   4236
         _ExtentX        =   7472
         _ExtentY        =   6287
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "frmServiceOptions.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).ControlCount=   0
      End
   End
End
Attribute VB_Name = "frmServiceOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

