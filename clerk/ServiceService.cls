VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ServiceService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mClient As SoapClient30
Private Const ServiceName = "ServiceService"

Private Sub Class_Initialize()
  'Setup SOAP
On Error GoTo Exception
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName

End Sub




'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mClient = New SoapClient30
  mClient.MSSoapInit WSDLFile
  mClient.ConnectorProperty("EndPointURL") = EndPointURL
End Sub



'
' Interface to server
'
Public Sub InitService( _
    Name As String, ClassName As String, _
    CommandLine As String, _
    ParameterNames() As String, ParameterValues() As String, _
    Priority As Integer, Start As Boolean, Destroy As Boolean _
    )
    
On Error GoTo Exception
    
  mClient.Init Name, ClassName, _
      CommandLine, ParameterNames, ParameterValues, _
      Priority, Start, Destroy
  Exit Sub
  
Exception:
  'ver 2.72.00 (jjm) 2005 04 20
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Exception in Service service: " & Err.Description, vbExclamation
  'Err.Clear
End Sub

