VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmColumnPropertiesDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Column Properties"
   ClientHeight    =   3012
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   3876
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3012
   ScaleWidth      =   3876
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   2592
      TabIndex        =   6
      Top             =   2568
      Width           =   1200
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2424
      Left            =   84
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   72
      Width           =   3708
      _ExtentX        =   6541
      _ExtentY        =   4276
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   420
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmColumnProperties.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabel(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblColumnName"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLabel(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label4"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label6"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label5"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "txtDisplay"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "txtWidth"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "txtFormat"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "cboAlignment"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "chkRequired"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).ControlCount=   11
      Begin VB.CheckBox chkRequired 
         Caption         =   "Required"
         Height          =   288
         Left            =   1188
         TabIndex        =   14
         Top             =   2004
         Width           =   1476
      End
      Begin VB.ComboBox cboAlignment 
         Height          =   288
         ItemData        =   "frmColumnProperties.frx":001C
         Left            =   1188
         List            =   "frmColumnProperties.frx":0029
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1296
         Width           =   1800
      End
      Begin VB.TextBox txtFormat 
         Height          =   288
         Left            =   1188
         TabIndex        =   3
         Text            =   "format"
         Top             =   1632
         Width           =   1800
      End
      Begin VB.TextBox txtWidth 
         Height          =   288
         Left            =   1188
         TabIndex        =   1
         Text            =   "width"
         Top             =   960
         Width           =   1800
      End
      Begin VB.TextBox txtDisplay 
         Height          =   288
         Left            =   1188
         TabIndex        =   0
         Text            =   "display"
         Top             =   624
         Width           =   1800
      End
      Begin VB.Label Label5 
         Caption         =   "Alignment:"
         Height          =   192
         Left            =   144
         TabIndex        =   13
         Top             =   1344
         Width           =   744
      End
      Begin VB.Label Label6 
         Caption         =   "Format:"
         Height          =   192
         Left            =   144
         TabIndex        =   12
         Top             =   1680
         Width           =   744
      End
      Begin VB.Label Label4 
         Caption         =   "Width:"
         Height          =   192
         Left            =   144
         TabIndex        =   11
         Top             =   1008
         Width           =   744
      End
      Begin VB.Label lblLabel 
         Caption         =   "Display:"
         Height          =   192
         Index           =   1
         Left            =   144
         TabIndex        =   10
         Top             =   672
         Width           =   744
      End
      Begin VB.Label lblColumnName 
         AutoSize        =   -1  'True
         Caption         =   "column name"
         Height          =   192
         Left            =   1188
         TabIndex        =   9
         Top             =   384
         UseMnemonic     =   0   'False
         Width           =   960
      End
      Begin VB.Label lblLabel 
         Caption         =   "Name:"
         Height          =   192
         Index           =   0
         Left            =   144
         TabIndex        =   8
         Top             =   384
         Width           =   744
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   1332
      TabIndex        =   5
      Top             =   2568
      Width           =   1200
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   84
      TabIndex        =   4
      Top             =   2568
      Width           =   1200
   End
   Begin VB.Label lblRequired 
      Height          =   252
      Left            =   156
      TabIndex        =   15
      Top             =   2664
      Width           =   492
   End
End
Attribute VB_Name = "frmColumnPropertiesDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private mListItem As ListItem
Private mDirty As Boolean

Public Property Get Dirty() As Boolean
  Dirty = mDirty
End Property

Private Sub cboAlignment_Change()
  mDirty = True
End Sub

Private Sub chkRequired_Click()
  mDirty = True
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp HelpName.COLUMNPROPERTIES_TOPIC, hWnd
End Sub

Private Sub cmdOK_Click()
  mListItem.Text = txtDisplay
  mListItem.Tag = txtFormat
  mListItem.ListSubItems("Width") = txtWidth
  mListItem.ListSubItems("Alignment") = cboAlignment.ListIndex
  mListItem.ListSubItems("Required") = chkRequired.Value
  Unload Me
End Sub

Public Sub ShowListItem(Item As ListItem)
  Set mListItem = Item
  lblColumnName = mListItem.Key
  txtDisplay = mListItem.Text
  txtFormat = mListItem.Tag
  txtWidth = mListItem.ListSubItems("Width")
  cboAlignment.ListIndex = mListItem.ListSubItems("Alignment")
  chkRequired.Value = IIf(mListItem.ListSubItems("Required"), 1, 0)
  chkRequired.Enabled = False
End Sub

Private Sub Forfvm_KeyDown(KeyCode As Integer, Shift As Integer)
  cmdHelp_Click
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  mDirty = False
End Sub

Private Sub lblRequired_DblClick()
  chkRequired.Enabled = True
End Sub

Private Sub txtDisplay_Change()
  mDirty = True
End Sub

Private Sub txtFormat_Change()
  mDirty = True
End Sub

Private Sub txtWidth_Change()
  mDirty = True
End Sub
