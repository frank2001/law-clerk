VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmPersonOpen 
   Caption         =   "Person"
   ClientHeight    =   6225
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   7710
   Icon            =   "frmPersonOpen.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6225
   ScaleWidth      =   7710
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5364
      Left            =   0
      ScaleHeight     =   5370
      ScaleWidth      =   7710
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   384
      Width           =   7716
      Begin VB.Frame fraNote 
         Caption         =   "Person Note"
         Height          =   1236
         Left            =   4500
         TabIndex        =   20
         Top             =   144
         Width           =   2712
         Begin VB.TextBox txtNote 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   936
            Left            =   72
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   216
            Width           =   2568
         End
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3132
         Left            =   36
         TabIndex        =   2
         TabStop         =   0   'False
         Tag             =   "PersonEventAttribute"
         Top             =   1272
         Width           =   7608
         _ExtentX        =   13414
         _ExtentY        =   5530
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   10
         TabHeight       =   420
         TabCaption(0)   =   "Contact"
         TabPicture(0)   =   "frmPersonOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Location"
         TabPicture(1)   =   "frmPersonOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Document"
         TabPicture(2)   =   "frmPersonOpen.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Attribute"
         TabPicture(3)   =   "frmPersonOpen.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         Begin MSComctlLib.ListView lvView 
            DragIcon        =   "frmPersonOpen.frx":007C
            Height          =   2640
            Index           =   0
            Left            =   96
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "PersonContact"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            OLEDragMode     =   1
            OLEDropMode     =   1
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            OLEDragMode     =   1
            OLEDropMode     =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   2
            Left            =   -74904
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "PersonDocument"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            OLEDragMode     =   1
            OLEDropMode     =   1
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            OLEDragMode     =   1
            OLEDropMode     =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   1
            Left            =   -74904
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "PersonLocation"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            OLEDragMode     =   1
            OLEDropMode     =   1
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            OLEDragMode     =   1
            OLEDropMode     =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   3
            Left            =   -74910
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "PersonEventAttribute"
            Top             =   375
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            OLEDragMode     =   1
            OLEDropMode     =   1
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            OLEDragMode     =   1
            OLEDropMode     =   1
            NumItems        =   0
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   1548
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   2052
         Width           =   1416
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "locator"
         DataField       =   "entityLocator"
         DataMember      =   "Entity"
         Height          =   195
         Index           =   6
         Left            =   3330
         TabIndex        =   25
         Top             =   165
         Width           =   495
      End
      Begin VB.Label lblLocatorLabel 
         AutoSize        =   -1  'True
         Caption         =   "Locator:"
         Height          =   195
         Left            =   2670
         TabIndex        =   24
         Top             =   165
         Width           =   585
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "zip"
         DataField       =   "zip"
         DataMember      =   "Location"
         Height          =   192
         Index           =   5
         Left            =   2688
         TabIndex        =   19
         Top             =   924
         Width           =   204
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "state"
         DataField       =   "state"
         DataMember      =   "Location"
         Height          =   192
         Index           =   4
         Left            =   1656
         TabIndex        =   18
         Top             =   924
         Width           =   348
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "city"
         DataField       =   "city"
         DataMember      =   "Location"
         Height          =   192
         Index           =   3
         Left            =   132
         TabIndex        =   17
         Top             =   960
         Width           =   240
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "addressLine1"
         DataField       =   "addressLine1"
         DataMember      =   "Location"
         Height          =   192
         Index           =   2
         Left            =   120
         TabIndex        =   16
         Top             =   684
         Width           =   984
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "companyName"
         DataField       =   "companyName"
         DataMember      =   "Location"
         Height          =   192
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   444
         Width           =   1116
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "entityName"
         DataField       =   "entityName"
         DataMember      =   "Entity"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   165
         Width           =   795
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   7716
      _ExtentX        =   13600
      _ExtentY        =   688
      _CBWidth        =   7710
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinWidth1       =   795
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   990
      MinHeight2      =   330
      Width2          =   990
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   330
      Width3          =   945
      NewRow3         =   0   'False
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   6870
         ScaleHeight     =   330
         ScaleWidth      =   750
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   30
         Width           =   750
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   13
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1830
            TabIndex        =   9
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1640
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Person"
                        Text            =   "Person"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Contact"
                        Text            =   "Contact"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   660
            Left            =   990
            TabIndex        =   8
            Top             =   15
            Width           =   750
            _ExtentX        =   1323
            _ExtentY        =   1164
            ButtonWidth     =   1191
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Location (Primary)"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Person"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   660
            Left            =   0
            TabIndex        =   7
            Top             =   15
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   1164
            ButtonWidth     =   1270
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Contact"
                        Text            =   "Contact"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Location"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Document"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   3705
         ScaleHeight     =   330
         ScaleWidth      =   2940
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   30
         Width           =   2940
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   660
            Left            =   0
            TabIndex        =   5
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1508
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   225
      Left            =   0
      TabIndex        =   10
      Top             =   6000
      Width           =   7710
      _ExtentX        =   13600
      _ExtentY        =   397
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5054
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "7/21/2011"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "4:16 PM"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   576
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   60
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNewPerson 
            Caption         =   "&Person"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator8343 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Contact"
            Index           =   0
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Location"
            Index           =   1
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Document"
            Index           =   2
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Lawyer"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator847 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To "
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator3412 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "L&ocation (Primary)"
         Index           =   1
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Person"
         Index           =   2
      End
      Begin VB.Menu mnuSeparator876 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuSeperator125 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Complete Row"
         Index           =   0
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Void Row"
         Index           =   1
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "T&ab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Contact"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator123 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings"
      End
      Begin VB.Menu mnuSeparator2384 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuViewBrowsePerson 
         Caption         =   "&Browse Person"
         Enabled         =   0   'False
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuViewBrowseContact 
         Caption         =   "Browse &Contact"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeparator5673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeparator83121 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&Workflow..."
      End
      Begin VB.Menu mnuSeperator998 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "Web Merge"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator655 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSeperator656 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuSeperator657 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Complete"
         Index           =   0
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Void"
         Index           =   1
      End
   End
End
Attribute VB_Name = "frmPersonOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ver 2.10.0416 (jjm)
' previous ver 2.00.0110
'
' PersonOpen
'
Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
  
Private mPersonHandle As String
Private mLocationHandle As String
Private mEventHandle As String
Private mLawyerHandle As String

Public FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

' Detail Tabs
Private AttributeData As Dataset
Private LocationData As Dataset
Private DocumentData As Dataset
Private ContactData As Dataset

Private mDragSource As String

Private mFormDirty As Boolean

Private Const CONTACT_TAB_INDEX = 0
Private Const LOCATION_TAB_INDEX = 1
Private Const DOCUMENT_TAB_INDEX = 2
Private Const ATTRIBUTE_TAB_INDEX = 3


'
' FormInterface Interface
'

'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  
  Select Case fInterface.Message
    Case fimSave
      
'      If fInterface.Key = DatabaseName.EVENT_KEY & "New" Then
'        'Setup for auto-complete
'        Dim Handles(0) As String
'        Dim ParameterNames() As String
'        Dim ParameterValues() As String
'        Handles(0) = fInterface.Child.EventHandle
'        'Automatically Complete the documents
'        Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
'      End If
  End Select
  
  Refresh
  FormRefresh
  Show
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim Datafields As Collection
  Dim EntityFields As Collection
  Dim LocationFields As Collection
  Dim LawyerData As Collection
  Set Datafields = Factory.PersonService.Find(mPersonHandle)
  Set EntityFields = Factory.EntityService.Find(mPersonHandle)
  
  Caption = "Person (" & mPersonHandle & ")"
  SetFields lblDatafield, Datafields, DatabaseName.PERSON_TABLE
  SetFields lblDatafield, EntityFields, DatabaseName.ENTITY_TABLE
  
  ' Location
  mLocationHandle = GetDataFromCollection(EntityFields, DatabaseName.MAINLOCATIONID_FIELD)
  Set LocationFields = Factory.LocationService.Find(mLocationHandle)
  SetFields lblDatafield, LocationFields, "Location"
  
  mnuEdit(1).Enabled = mLocationHandle <> vbNullString
  tbrEdit.Buttons(1).ButtonMenus(1).Enabled = mLocationHandle <> vbNullString

  Set LawyerData = Factory.LawyerService.Find(mPersonHandle)
  If Not LawyerData Is Nothing Then
    mLawyerHandle = GetDataFromCollection(LawyerData, DatabaseName.LAWYER_KEY)
    'lblLawyerLocator = LawyerData("lawyerLocator")
  End If
  'lblLawyerLocator.Visible = Not LawyerData Is Nothing
  'lblLocatorLabel.Visible = Not LawyerData Is Nothing
  
  mnuFileOpen(0).Enabled = Not (mLawyerHandle = vbNullString)
  
  mEventHandle = GetDataFromCollection(EntityFields, DatabaseName.EVENT_KEY)
  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
  If (mEventHandle = vbNullString) Then
    txtNote.Text = vbNullString
  Else
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  End If
  
  'Open PersonAttribute Table
  AttributeData.DataProvider.filter = DatabaseName.EVENT_KEY & " = '" & mEventHandle & "'"
  'AttributeData.DataProvider.Filter = """" & DatabaseName.PERSON_KEY & """ = '" & mPersonHandle & "'"
  
  'Open Contact Dataset
  ContactData.DataProvider.filter = DatabaseName.PERSON_KEY & " = '" & mPersonHandle & "'"
  
  'Open Contact Dataset
  LocationData.DataProvider.filter = DatabaseName.ENTITY_KEY & " = '" & mPersonHandle & "'"
  
  'Open EventRecipientView Table
  DocumentData.DataProvider.filter = DatabaseName.PERSON_KEY & " = '" & mPersonHandle & "'"
  
  ' Standard
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  SSTabRefresh
  'Refresh
End Sub

'
' Set ListIndex from underlying view.  How we pull the field is
' dependent upon the list item type.  In this example, if we are
' opening from an event view, there is a different method for
' pulling the person id.
'
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  mPersonHandle = ListItemText(mListView.SelectedItem, DatabaseName.PERSON_KEY)
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
  Case CONTACT_TAB_INDEX
    Set ListViewDataProvider = ContactData.DataProvider
  Case LOCATION_TAB_INDEX
    Set ListViewDataProvider = LocationData.DataProvider
  Case DOCUMENT_TAB_INDEX
    Set ListViewDataProvider = DocumentData.DataProvider
  Case ATTRIBUTE_TAB_INDEX
    Set ListViewDataProvider = AttributeData.DataProvider
  End Select
End Function

Private Sub MenuRefresh()
  Dim IsDocumentTab, IsContactTab, IsLocationTab As Boolean
  IsContactTab = (SSTab.Tab = CONTACT_TAB_INDEX)
  IsDocumentTab = (SSTab.Tab = DOCUMENT_TAB_INDEX)
  IsLocationTab = (SSTab.Tab = LOCATION_TAB_INDEX)
  
  'DeleteRow
  mnuEditDeleteRow.Enabled = IsContactTab Or IsLocationTab
  mnuPopupDelete.Enabled = mnuEditDeleteRow.Enabled
  
  'Browse
  mnuEditBrowseRow.Enabled = False
  mnuPopupBrowse.Enabled = False
   
  'Merge
  mnuToolsMerge.Enabled = IsDocumentTab
  mnuToolsWebMerge.Enabled = IsDocumentTab
  mnuPopupMerge.Enabled = IsDocumentTab
  
  'Complete / Void
  mnuEditEventRow(0).Enabled = IsDocumentTab
  mnuEditEventRow(1).Enabled = IsDocumentTab
  mnuPopupEvent(0).Enabled = IsDocumentTab
  mnuPopupEvent(1).Enabled = IsDocumentTab
  
End Sub

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = "Person")
  If View.ListItems.Count = 0 Then
    MsgBox "There are no persons to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(PersonId As String, Optional ShowTab As Integer = 0)
  mPersonHandle = PersonId
  mTab = ShowTab
  Show
End Sub

Public Sub SSTabRefresh()
  Dim Source As DataProvider, View As ListView
  Set View = lvView(mTab)
  Set Source = ListViewDataProvider(mTab)

  Select Case mTab
    Case 0
      mDragSource = DatabaseName.CONTACT_TABLE
    Case 1
      mDragSource = DatabaseName.EVENTRECIPIENT_TABLE
    Case 2
      mDragSource = DatabaseName.EVENTATTRIBUTE_TABLE
  End Select


  ' This code is used to save the selected items and reload
  ' a multirow selected listview with fresh data and the same
  ' items selected if they still exist.
  With View
    'Ensure first tab is open.
    If SSTab.Tab = mTab Then
      If .ListItems.Count > 0 Then
        'Save the selected items
        Dim Selected As Collection
        If ListViewModule.SelectedCount(View.hWnd) > 0 Then
          Set Selected = ListViewModule.ListViewSelected(View)
        End If
        
        Dim Index() As Variant
        
        If Not Selected Is Nothing Then
          ReDim Index(Selected.Count)
          Dim item As ListItem
          Dim I As Integer
          For Each item In Selected
            Index(I) = IIf(item.Key = vbNullString, item.Index, item.Key)
            I = I + 1
          Next item
        Else
          ReDim Index(0)
        End If


        'Reload the data. Hide the list view for visual effect
        .Visible = False
        .SelectedItem.Selected = False
        ListViewLoad View, Source

        'Reload the selected items
        On Error Resume Next
        
        'The first item in a listview is the default item selected
        If Index(0) <> vbNullString Then
          'Disable the first item. It is automatically selected.
          .ListItems(1).Selected = False
          Dim n As Integer
          For n = 0 To SafeUBound(Index) - 1
            .ListItems(Index(n)).Selected = True
          Next n
        End If
        
        'Display the list view
        .Visible = True
        .SetFocus
      Else
        ListViewLoad View, Source
      End If
    Else
      SSTab.Tab = mTab
    End If

    sbStatusBar.Panels(1).Text = str(.ListItems.Count) + " object(s)"
    sbStatusBar.Panels(2).Text = Source.Source

  End With
  
  MenuRefresh
End Sub

'Public Sub SSTabRefresh()
'  Dim Source As DataProvider, View As ListView, Index As Variant
'  Set View = lvView(mTab)
'  Set Source = ListViewDataProvider(mTab)
'
'  Select Case mTab
'    Case 0
'      mDragSource = DatabaseName.CONTACT_TABLE
'    Case 1
'      mDragSource = DatabaseName.EVENTRECIPIENT_TABLE
'    Case 2
'      mDragSource = DatabaseName.EVENTATTRIBUTE_TABLE
'  End Select
'
'  With View
'    'Ensure first tab is open.
'    If SSTab.Tab = mTab Then
'      If .ListItems.Count > 0 Then
'        Index = IIf(.SelectedItem.Key = "", .SelectedItem.Index, .SelectedItem.Key)
'        .Visible = False
'        .SelectedItem.Selected = False
'        ListViewLoad View, Source
'        On Error Resume Next
'        .ListItems(Index).Selected = True
'        .Visible = True
'        .SetFocus
'      Else
'        ListViewLoad View, Source
'      End If
'    Else
'      SSTab.Tab = mTab
'    End If
'
'    sbStatusBar.Panels(1).Text = str(.ListItems.Count) + " object(s)"
'    sbStatusBar.Panels(2).Text = Source.Source
'
'  End With
'End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
End Sub

'
' End General Methods
'
'
'
'
' Begin Controls
'

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub



Private Sub Form_Activate()
  If mFormDirty Then
    mFormDirty = False
    FormRefresh
  End If
End Sub

Private Sub Form_Initialize()
  'Initialize Detail Datasets.
  Set ContactData = NewGenericDataset(DatabaseName.CONTACT_VIEW)
  Set LocationData = NewGenericDataset(DatabaseName.ENTITYLOCATION_VIEW)
  Set AttributeData = NewGenericDataset(DatabaseName.EVENTATTRIBUTE_TABLE)
  Set DocumentData = NewGenericDataset(DatabaseName.EVENTRECIPIENT_VIEW)
End Sub

'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  
    
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyC
        mnuFileNew_Click 0
      Case vbKeyD
        mnuFileNew_Click 1
    End Select
    Exit Sub
  End If

  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      mnuFileNew_Click SSTab.Tab
      Exit Sub
  End Select
End Sub


Private Sub Form_Load()
  'Load to from to previous position
  FormUtil.DeserializePosition Me
  
  ' Setup Toolbar.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.Height + 24

  FormUtil.EditLoadImages Me
  FormUtil.MenuLoad Me
  
  ' Custom Menu Shortcut Adjustments
  mnuFileNew(CONTACT_TAB_INDEX).Caption = mnuFileNew(CONTACT_TAB_INDEX).Caption + Chr(9) + "Ctrl+Shift+C"
  mnuFileNew(LOCATION_TAB_INDEX).Caption = mnuFileNew(LOCATION_TAB_INDEX).Caption + Chr(9) + "Ctrl+Shift+L"
  mnuFileNew(DOCUMENT_TAB_INDEX).Caption = mnuFileNew(DOCUMENT_TAB_INDEX).Caption + Chr(9) + "Ctrl+Shift+D"
  
'  mnuFileNewQuestion.Caption = "&Question" + Chr(9) + "Ctrl+Shift+Q"
  mnuEditEditRow.Caption = mnuEditEditRow.Caption + Chr(9) + "Enter"
    
  ' Setup Form Manager
  FormMgr.Load Me
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  SSTabResize
End Sub

' Expand body
Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub fraNote_DblClick()
  If mnuToolsNotepad.Enabled Then
    mnuToolsNotepad_Click
  Else
    MsgBox "To create a new note, first use the the Person Edit form"
  End If
End Sub

Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub


Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

'ver 2.10.0416 (jjm)
Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  
  'Hack because the Attribute has no functionality
  If SSTab.Tab = 2 Then
    Exit Sub
  End If
  
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub lvView_OLECompleteDrag(Index As Integer, Effect As Long)
  FormRefresh
End Sub

Private Sub lvView_OLEDragDrop(Index As Integer, data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim Rows() As String
  Dim Columns() As String
  Dim Text As String
  Dim mSource As String
  
  Text = data.GetData(vbCFText)
  Rows = Split(Text, vbCrLf)
  
  'MsgBox "Entire Text:" & vbCrLf & data.GetData(vbCFText)
  
  mSource = Rows(0)
  
  If mDragSource <> mSource Then
    MsgBox "An " & mSource & " item can not be dragged to a " & mDragSource & " view"
    Exit Sub
  End If
  
  'Preform the call base on the contact type
  Dim I As Integer
  Select Case mSource
    Case DatabaseName.CONTACT_TABLE
      For I = 1 To SafeUBound(Rows) - 1
        Columns = Split(Rows(I), vbTab)
        'Test to see if mPersonHandle is the same as the one passed
        If Columns(1) = mPersonHandle Then
          Exit Sub
        End If
        If Not Factory.ContactService.SetPerson(Columns(0), mPersonHandle) Then
          MsgBox "Items could not be moved", vbExclamation
        End If
      Next I
      
    Case DatabaseName.EVENTRECIPIENT_TABLE
      For I = 1 To SafeUBound(Rows) - 1
        Columns = Split(Rows(I), vbTab)
        If Columns(1) = mPersonHandle Then
          Exit Sub
        End If
        'MsgBox Columns(0) & " " & mPersonHandle
        If Not Factory.EventRecipientService.SetPerson(Columns(0), mPersonHandle) Then
          MsgBox "Items could not be moved", vbExclamation
        End If
      Next I
      
    Case DatabaseName.EVENTATTRIBUTE_TABLE
      For I = 1 To SafeUBound(Rows) - 1
        Columns = Split(Rows(I), vbTab)
        If Columns(1) = mPersonHandle Then
          Exit Sub
        End If
        'MsgBox Columns(0) & " " & mPersonHandle
        'If Not Factory.EventService.SetPerson(Columns(0), mPersonHandle) Then
        '  MsgBox "Items could not be moved", vbExclamation
        'End If
      Next I
        
    Case Else
      MsgBox "You cannot drag these items into this area"
      Exit Sub
  End Select
  
  'FormRefresh
  'Instead of FormRefresh, do mFormDirty.  Problems with OLE
  mFormDirty = True
  
End Sub

Private Sub lvView_OLESetData(Index As Integer, data As MSComctlLib.DataObject, DataFormat As Integer)
    
  Dim Selection As Collection
  Set Selection = ListViewModule.ListViewSelected(lvView(Index))

  'Remove items so they are not included in the selection that is refreshed
  Dim ItemEraser As ListItem
  For Each ItemEraser In Selection
    lvView(Index).ListItems.Remove ItemEraser.Index
  Next

  Dim mText As String
  mText = mDragSource & vbCrLf

  Dim item As ListItem
  Select Case mDragSource
    Case DatabaseName.CONTACT_TABLE
      For Each item In Selection
        mText = mText & ListItemText(item, DatabaseName.CONTACT_KEY) & vbTab & ListItemText(item, DatabaseName.PERSON_KEY) & vbCrLf
      Next item

    Case DatabaseName.EVENTRECIPIENT_TABLE
      For Each item In Selection
        mText = mText & ListItemText(item, DatabaseName.EVENTRECIPIENT_KEY) & vbTab & ListItemText(item, DatabaseName.PERSON_KEY) & vbCrLf
      Next item

    Case DatabaseName.EVENTATTRIBUTE_TABLE
      For Each item In Selection
        mText = mText & ListItemText(item, DatabaseName.EVENTRECIPIENT_KEY) & vbTab & ListItemText(item, DatabaseName.PERSON_KEY) & vbCrLf
      Next item

    Case Else
      Exit Sub
  End Select
  
  data.SetData mText, vbCFText
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 1 'Location
      FormUtil.ShowByPrimaryKey New frmLocationEdit, mLocationHandle, FormMgr
    Case 2 'Person
      FormUtil.ShowByPrimaryKey New frmPersonEdit, mPersonHandle, FormMgr
  End Select
End Sub

Private Sub mnuEditBrowseRow_Click()
  'Dummy Code
  'Dim Pointer As New WaitPointer
  'Pointer.Pointer = vbHourglass
  'Select Case SSTab.Tab
  '  case 0
  'End Select
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If
  
  If lvView(SSTab.Tab).ListItems.Count = 0 Then
    MsgBox "There are no items to delete."
    Exit Sub
  End If
  
  Select Case SSTab.Tab 'ToDo: add delete
    Case CONTACT_TAB_INDEX: 'Contact
      With lvView(CONTACT_TAB_INDEX)
        Call Factory.ContactService.DeleteContact( _
            ListItemText(.SelectedItem, DatabaseName.CONTACT_KEY))
      End With
      
    'Mantis 2523 - enable location delete
    Case LOCATION_TAB_INDEX: 'Location
      Call Factory.LocationService.DeleteLocation(mPersonHandle, _
            ListItemText(lvView(LOCATION_TAB_INDEX).SelectedItem, DatabaseName.LOCATION_KEY))
      
    Case DOCUMENT_TAB_INDEX: 'Document
        
  End Select
  FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case CONTACT_TAB_INDEX:   'Contact
      FormUtil.ShowByListView New frmContactEdit, FormMgr, lvView(CONTACT_TAB_INDEX)
    Case LOCATION_TAB_INDEX:
      FormUtil.ShowByListView New frmLocationEdit, FormMgr, lvView(LOCATION_TAB_INDEX)
    Case DOCUMENT_TAB_INDEX:   'Document
      FormUtil.ShowByListView New frmEventDocumentEdit, FormMgr, lvView(DOCUMENT_TAB_INDEX)
    Case ATTRIBUTE_TAB_INDEX:   'Attibute
  End Select
End Sub

Private Sub mnuEditEventRow_Click(Index As Integer)
  Dim Handles(0) As String
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  
  Handles(0) = ListItemText(lvView(1).SelectedItem, DatabaseName.EVENT_KEY)
'  Factory.FolderParameterService.ParameterLoad ListItemText(lvView(1).SelectedItem, DatabaseName.FOLDER_KEY), Index, ParameterNames, ParameterValues, True

  If Index = lawActionComplete Then
    If MsgBox("Are you sure you want to complete item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
    End If
  ElseIf Index = lawActionVoid Then
    If MsgBox("Are you sure you want to void item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetVoid Handles, ParameterNames, ParameterValues
    End If
  End If
  
  FormRefresh
End Sub

Private Sub mnuFileNewPerson_Click()
  FormUtil.ShowNew New frmPersonEdit, FormMgr
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmLawyerOpen, mLawyerHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.PERSON_TABLE, mPersonHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.PERSON_REF, DatabaseName.PERSON_TABLE, mPersonHandle, lblDatafield(0)
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
End Sub


Private Sub mnuFileNew_Click(Index As Integer)
  Select Case Index
    Case CONTACT_TAB_INDEX
      Dim fForm As New frmContactEdit
      FormMgr.Add fForm
      fForm.ShowNewByPerson mPersonHandle, mLocationHandle
    Case LOCATION_TAB_INDEX
      Dim fLocForm As New frmLocationEdit
      FormMgr.Add fLocForm
      fLocForm.ShowNew mPersonHandle
    Case DOCUMENT_TAB_INDEX
      Dim fDocument As New frmEventDocumentEdit
      FormMgr.Add fDocument, DatabaseName.EVENT_KEY & "New"
      fDocument.ShowNew DatabaseName.DOCUMENT_RULE, mEventHandle, GetDataFromCollection(Factory.EntityService.Find(mPersonHandle), DatabaseName.ENTITYNAME_FIELD, True), mPersonHandle
  End Select
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub
 
Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
 Dispatcher.ShowSearch DatabaseName.PERSON_TABLE, FormMgr
End Sub



Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupEvent_Click(Index As Integer)
  mnuEditEventRow_Click Index
End Sub

Private Sub mnuPopupMerge_Click()
  mnuToolsMerge_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNew_Click SSTab.Tab
End Sub

Private Sub mnuToolsMerge_Click()
  Dim pointer As New WaitPointer, Handles(0) As String
  pointer.Default
  
  Handles(0) = ListItemText(lvView(1).SelectedItem, DatabaseName.EVENT_KEY)
  
  ' Print Envelopes
  Factory.EventRecipientService.PrintEnvelope Handles(0)

  ' Perform Document Merge
  Factory.EventService.DocumentMerge Handles
  
  FormRefresh
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWebMerge_Click()
  Factory.EventService.WebMergeByView lvView(2)
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.PERSON_TABLE, lawActionNew
End Sub

Private Sub mnuViewBrowseApplication_Click()
'ToDo:
'  ShellExecute 0&, vbNullString, HttpServer &  WebService & "applicationopen?key=" + mPersonHandle, vbNullString, _
'      vbNullString, SW_SHOWNORMAL
End Sub

'
' Standard choose columns for the current tab's list view.
'
Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'
' Standard view next menu
'
Private Sub mnuViewNext_Click()
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

'
' Standard view previous menu
'
Private Sub mnuViewPrevious_Click()
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

'
' Standard view refresh menu
'
Public Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

'
' Standard view a specific tab
'
Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

'
' Standard save settings
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

'
' Standard view toolbar.
'
Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  SSTabResize
End Sub


Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

Private Sub picMenu_Resize()
  tbrInsert.Top = (picMenu.Height - tbrInsert.Height) / 2
  tbrEdit.Top = (picMenu.Height - tbrEdit.Height) / 2
  tbrBrowse.Top = (picMenu.Height - tbrBrowse.Height) / 2
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  mTab = SSTab.Tab
  SSTabRefresh
  'MenuRefresh

  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub

Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEdit_Click 2
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileNew_Click SSTab.Tab
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNew_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuEdit_Click ButtonMenu.Index
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index 'ToDo:
    Case 1
      '
    Case 2
      '
  End Select
End Sub
