VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FirmPolicyService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mClient As MSSOAPLib30.SoapClient30
Private mDataSet As Dataset
Private mHandleNames(1) As String
Private Const ServiceName = "FirmPolicyService"

'
' Initialize Web Service
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.FIRMPOLICY_TABLE)
  
  mHandleNames(0) = DatabaseName.FIRM_KEY
  mHandleNames(1) = DatabaseName.POLICY_KEY
  
  
  'Setup SOAP
On Error GoTo Exception
  
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
  
End Sub



'
' Data Access
'
Public Function Find(firmHandle As String, PolicyHandle As String, EffectiveHandle As Date, Optional Column As String) As Collection
  Dim Handles(1) As Variant
  Handles(0) = firmHandle
  Handles(1) = PolicyHandle
  Set Find = mDataSet.DataProvider.FindMulti(mHandleNames, Handles)
End Function


'
' Interface to server
'

'
' Create an application.
'
Public Function CreateFirmPolicy( _
    FirmId As String, PolicyId As String, _
    AttributeNames() As String, AttributeValues() As String, _
    ParameterNames() As String, ParameterValues() As String) As Variant
    
On Error GoTo Exception
    
  CreateFirmPolicy = mClient.Create( _
      FirmId, PolicyId, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName

End Function


'
' Delete an FirmPolicy
'
Public Function DeleteFirmPolicy(FirmId As String, PolicyId As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo Exception
  mClient.Delete FirmId, PolicyId, ParameterNames, ParameterValues
  DeleteFirmPolicy = True
  Exit Function
  
Exception:
  MsgBox "Exception in FirmPolicy service: " & Err.Description, vbExclamation
  Err.Clear
End Function
