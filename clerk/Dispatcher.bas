Attribute VB_Name = "Dispatcher"
Option Explicit

'2.60.00 Need to fade out this function - currently a compatability patch
'this sub is only called internally, otherwise formutil.shownew is called
Public Sub ShowNewForm(Form As Form, Optional Handle As String = vbNullString, Optional FormMgr As FormManager = Nothing, Optional FormMgrKey As String = vbNullString)
  FormUtil.ShowNew Form, FormMgr, Handle, FormMgrKey
End Sub

'
' General "Show New" code is placed here
'
Public Sub ShowNew(Source As String, Optional Handle As String, Optional FormMgr As FormManager, Optional FormMgrKey As String)
    
  Select Case Source
    Case DatabaseName.APPLICATION_TABLE
      ShowNewForm New frmApplicationEdit, Handle, FormMgr, FormMgrKey
        
    Case DatabaseName.APPLICATIONLAWAREA_TABLE
      ShowNewForm New frmApplicationLawAreaEdit, Handle, FormMgr, FormMgrKey
      
    Case DatabaseName.APPLICATIONLAWYER_TABLE
      ShowNewForm New frmApplicationLawyerEdit, Handle, FormMgr, FormMgrKey
      
    Case DatabaseName.APPLICATIONLIMIT_TABLE
      ShowNewForm New frmApplicationLimitEdit, Handle, FormMgr, FormMgrKey
      
    Case DatabaseName.APPLICATIONQUESTION_TABLE
      ShowNewForm New frmApplicationQuestionEdit, Handle, FormMgr, FormMgrKey
    
    Case DatabaseName.CLAIMPAYMENT_TABLE
      ShowNewForm New frmClaimPaymentEdit, Handle, FormMgr, FormMgrKey
    
    Case DatabaseName.CLAIMRESERVE_TABLE
      Browse HttpServer & WebService & "claimreservenew?key=&key=" + Handle + "&action=create"
 
    Case DatabaseName.CLEEVENT_TABLE
      ShowNewForm New frmCLEEventEdit, Handle, FormMgr, FormMgrKey
  
    Case DatabaseName.CLAIM_TABLE
      BrowseNewClaim

    Case DatabaseName.CONTACT_TABLE
      ShowNewForm New frmContactEdit, Handle, FormMgr, FormMgrKey
    
    Case DatabaseName.DEDUCTIBLEREIMBURSE_TABLE
      ShowNewForm New frmDeductibleReimburseEdit, Handle, FormMgr, FormMgrKey
    
'    Case DatabaseName.EFTINDIVIDUAL_TABLE
'      ShowNewForm New frmEFTIndividualEdit, Handle, FormMgr, FormMgrKey

    Case DatabaseName.FIRM_TABLE
      ShowNewForm New frmOrganizationEdit, Handle, FormMgr, FormMgrKey
    
    Case DatabaseName.FIRMINSURER_TABLE
      If Handle = vbNullString Then Exit Sub
      ShowNewForm New frmFirmInsurerEdit, Handle, FormMgr, FormMgrKey
  
    Case DatabaseName.FIRMLAWYER_TABLE
      ShowNewForm New frmFirmLawyerEdit, Handle, FormMgr, FormMgrKey
    
    Case DatabaseName.GLBATCH_TABLE
      Browse HttpServer & WebUtil & "glbatchnew.new"
      
    Case DatabaseName.INCIDENT_TABLE
      ShowNewForm New frmIncidentEdit, Handle, FormMgr, FormMgrKey
      
    Case DatabaseName.INSURER_TABLE
      Dim fInsurer As New frmOrganizationEdit
      FormMgr.Add fInsurer
      fInsurer.ShowNew DatabaseName.INSURER_TABLE
            
    Case DatabaseName.LAWYER_TABLE
      'ver 2.50.00 (jjm) 2004 02 11
      Dim fLawyer As New frmPersonEdit
      FormMgr.Add fLawyer
      fLawyer.ShowNew True
      'ShowNewForm New frmPersonEdit, , FormMgr
    
    Case DatabaseName.LOCATION_TABLE
      ShowNewForm New frmLocationEdit, Handle, FormMgr, FormMgrKey
    
    Case DatabaseName.ORGANIZATION_TABLE
      ShowNewForm New frmOrganizationEdit, Handle, FormMgr, FormMgrKey
      
    Case DatabaseName.PERSON_TABLE
      'ShowNewForm New frmPersonEdit, Handle, FormMgr, FormMgrKey
      Dim fPerson As New frmPersonEdit
      FormMgr.Add fPerson
      fPerson.ShowNew True
    
    Case DatabaseName.POLICYACCOUNTING_TABLE
      ShowNewForm New frmPolicyAccountingEdit, Handle, FormMgr, FormMgrKey
    
    Case Else
    'if all the item is not included in any other case show the genric new
    'ver 2.60.01 (jjm) 2004 07 21
      Browse HttpServer & WebService & "genericnew" & _
          "?key=" & Util.EncodeURL(Source)
  End Select
  
End Sub
'ver 2.50.00 (jjm) 2004 02 17
Private Function ShowSearchForm(Form As Form, Source As String, FormMgr As FormManager, Optional IsSelect As Boolean, Optional TabIndex As Integer, Optional SearchInIndex As Integer, Optional SearchText As String, Optional CaptionText As String, Optional EnableSelectNone As Boolean, Optional FormManagerKey As String) As Form
  If Not FormMgr Is Nothing Then
    FormMgr.Add Form, IIf(FormManagerKey = vbNullString, Source, FormManagerKey)
  End If
  Form.ShowSearch Source, IsSelect, TabIndex, SearchInIndex, SearchText, CaptionText, EnableSelectNone
  Set ShowSearchForm = Form
End Function

'ver 2.50.00 (jjm) 2004 02 17 - added select none ability - so far only for generic search
Public Function ShowSearch(Source As String, Optional FormMgr As FormManager, Optional IsSelect As Boolean, Optional TabIndex As Integer, Optional SearchInIndex As Integer = -1, Optional SearchText As String, Optional CaptionText As String, Optional EnableSelectNone As Boolean, Optional FormManagerKey As String) As Form
  ' See if form already showing.
  Dim formKey As String
  formKey = IIf(FormManagerKey = vbNullString, Source, FormManagerKey)
  
  If FormMgr.ContainsKey(Source) Then
    FormMgr(Source).Child.Show
    Set ShowSearch = FormMgr(Source).Child
    Exit Function
  End If
  
  Select Case Source
    Case DatabaseName.FIRM_TABLE
      ShowSearchForm New frmFirmSearch, DatabaseName.FIRM_TABLE, FormMgr, IsSelect, TabIndex, SearchInIndex, SearchText, CaptionText, EnableSelectNone, FormManagerKey
    Case DatabaseName.CLAIM_TABLE
      ShowSearchForm New frmClaimSearch, DatabaseName.CLAIM_TABLE, FormMgr, IsSelect, TabIndex, SearchInIndex, SearchText, CaptionText, EnableSelectNone, FormManagerKey
    Case Else
      Set ShowSearch = ShowSearchForm(New frmGenericSearch, Source, FormMgr, IsSelect, TabIndex, SearchInIndex, SearchText, CaptionText, EnableSelectNone, FormManagerKey)
  End Select
  If Not SearchText = vbNullString Then
    ShowSearch.Refresh
    ShowSearch.cmdSearchNow_Click
  End If
End Function

Public Sub BrowseClaimOpen(claimHandle As String)
  Browse HttpServer & WebUtil & "claimopen.exact?key='" & Util.EncodeURL(claimHandle)
End Sub

Public Sub BrowseClaimReserveNew(claimHandle As String)
  Browse HttpServer & WebUtil & "claimreserveedit.exact?key='" & claimHandle & "&action=create&query=reservenew"
End Sub

Public Sub BrowseClaimReserveOpen(ClaimReserveHandle As String)
  Browse HttpServer & WebUtil & "claimreserveopen.exact?key='" & Util.EncodeURL(ClaimReserveHandle)
End Sub

Public Sub BrowseClaimWorksheet(EventHandle As String)
  Browse HttpServer & WebUtil & "claimworksheetopen.exact?key=" & Util.EncodeURL(EventHandle)
End Sub

Public Sub BrowseEndorsementOpen(EndorsementHandle As String)
  Browse HttpServer & WebUtil & "endorsementopen.exact?key='" & Util.EncodeURL(EndorsementHandle)
End Sub

Public Sub BrowseEntityURL(entityhandle As String)
  Browse DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, DatabaseName.ENTITY_KEY, entityhandle, DatabaseName.ENTITYURL_FIELD)
End Sub

Public Sub BrowseEventOpen(EventHandle As String)
  Browse HttpServer & "gwt.wilmic/eventsummary.exact?key='" & Util.EncodeURL(EventHandle)
End Sub

Public Sub BrowseFirmOpen(firmHandle As String)
  Browse HttpServer & WebUtil & "firmopen.exact?key='" & Util.EncodeURL(firmHandle)
End Sub

Public Sub BrowseLawyerOpen(LawyerHandle As String)
  Browse HttpServer & WebUtil & "lawyeropen.exact?key='" & Util.EncodeURL(LawyerHandle)
End Sub

Public Sub BrowseNewClaim()
  Browse HttpServer & WebUtil & "lawyerpolicysearch.list"
End Sub

Public Sub BrowsePolicyOpen(PolicyHandle As String)
  Browse HttpServer & WebUtil & "policyopen.exact?key='" & Util.EncodeURL(PolicyHandle)
End Sub

Public Sub BrowseReceipt(EventHandle As String)
  Browse HttpServer & "ui/summary/receiptsummary.exact?query=eventId&key='" & Util.EncodeURL(EventHandle)
End Sub

Public Sub BrowseUnderwritingWorksheet(QuoteHandle As String)
  Browse HttpServer & WebUtil & _
         "applicationratingopen.exact?key='" & Util.EncodeURL(QuoteHandle)
End Sub

Public Sub BrowseServiceManager()
  Browse HttpServer & WebUtil & "Service"
End Sub
