Attribute VB_Name = "FormUtil"
Option Explicit

Public Const lawActionComplete = 0
Public Const lawActionVoid = 1
Public Const lawActionReopen = 2
Public Const lawActionNew = 3

Public Declare Function LaunchHtmlHelp Lib "Hhctrl.ocx" _
    Alias "HtmlHelpA" (ByVal hWndCaller As Long, _
    ByVal pszFile As String, ByVal uCommand As Long, _
    ByVal dwData As Long) As Long

' Constants for HTML help
Public Const HH_DISPLAY_TOPIC = &H0
Public Const HH_HELP_CONTEXT = &HF


Public Sub ShowHelp(Optional Topic As Long, Optional WindowHandle As Long)
  Dim HelpFilePath As String
  Dim lngReturn As Long
  
  HelpFilePath = App.Path & "\" & "LawSystemHelp.chm"
  
  ' Check to see if file exists
  If Len(Dir(HelpFilePath)) > 0 Then
    If Topic = 0 Then
      ' Load default help
      lngReturn = LaunchHtmlHelp(WindowHandle, HelpFilePath, HH_DISPLAY_TOPIC, 0)
    Else
      ' Load by topic
      lngReturn = LaunchHtmlHelp(WindowHandle, HelpFilePath, HH_HELP_CONTEXT, Topic)
    End If
    If CBool(lngReturn) Then
      'Help was successfully loaded using the topic
      Exit Sub
    End If
    'Load the default help if the topic fails
    MsgBox "Unable to display associated Help Contents. The default help will be displayed.", vbInformation, "Help"
    lngReturn = LaunchHtmlHelp(WindowHandle, HelpFilePath, HH_DISPLAY_TOPIC, 0)
  Else
    
    ' Displays if help file not found
    MsgBox "Unable to display Help Contents. Help file not found.", vbInformation, "Help"
  End If
  
End Sub

Public Function ActionText(ActionType As Integer, Optional IsDatabaseName As Boolean) As String
  Select Case ActionType
    Case 0
      ActionText = "Complete"
    Case 1
      ActionText = "Void"
    Case 2
      ActionText = "Reopen"
    Case 3
      ActionText = IIf(IsDatabaseName, "Create", "New")
  End Select

End Function

Public Sub EditTextSelect(Text As Control)
  If LocaleTextSelectOnFocus = True Then
    SelAll Text
  End If
End Sub

Public Sub SelAll(Text As Control)
  Text.SelStart = 0
  Text.SelLength = Len(Text.Text)
End Sub


Public Sub PostTab(hWnd As Long)
  PostMessage hWnd, WM_KEYDOWN, vbKeyTab, 0
End Sub


'
' Load up open menu.
'
Public Sub MenuUnload(Menu As Object)
  Dim item As Menu, Index As Integer
  
  ' Ensure first is visible before removing everything.
  Menu(0).Visible = True
  
  ' Clear destination.
  For Each item In Menu
    If item.Index <> 0 Then Unload item
  Next item
End Sub

'
' Load up open menu.
'
Public Sub MenuCopy(Source As Object, Destination As Object)
  Dim item As Menu, Index As Integer
  
  ' Unload any destination.
  MenuUnload Destination
  
  ' If no source, then just exit
  If Source Is Nothing Then Exit Sub
  
  ' Copy source to destination.
  For Each item In Source
    Index = item.Index
    If Index <> 0 Then
      Load Destination(Index)
      With Destination(Index)
        .Caption = item.Caption
        .Enabled = item.Enabled
        .Visible = item.Visible
      End With
    End If
  Next item
  
  ' Hide holding menu if something has been copied.
  If Destination.Count > 1 Then
    Destination(0).Visible = False
  End If
End Sub


Public Sub MenuLoad(frm As Form)
  ' Custom Menu Shortcut Adjustments
  frm.KeyPreview = True
  
  frm.mnuViewPrevious.Caption = frm.mnuViewPrevious.Caption + vbTab + "Ctrl+<"
  frm.mnuViewNext.Caption = frm.mnuViewNext.Caption + vbTab + "Ctrl+>"
End Sub

'
' Requires standard "Edit" menus and command buttons.
'
Public Sub EditLoad(frm As Form)
  On Error Resume Next
  
  MenuLoad frm
  
  frm.cmdEnter.Default = LocaleEnterAsTab
  frm.cmdEnter.TabStop = False
  
  frm.cmdCancel.Cancel = True
  frm.cmdCancel.CausesValidation = False
End Sub


Public Sub EditLoadImages(frm As Form)
  'add the images
  ' fill image list
  frm.imlUpDown.ListImages.Add 1, "Next", PicDisplay.GetPicture(UrlName.DOWN_IMAGE)
  frm.imlUpDown.ListImages.Add 2, "Previous", PicDisplay.GetPicture(UrlName.UP_IMAGE)
  frm.imlUpDown.MaskColor = &HC0C0C0
  frm.imlUpDown.UseMaskColor = True
  
  frm.imlUpDownDisabled.ListImages.Add 1, "DisNext", PicDisplay.GetPicture(UrlName.DOWNDISABLED_IMAGE)
  frm.imlUpDownDisabled.ListImages.Add 2, "DisPrevious", PicDisplay.GetPicture(UrlName.UPDISABLED_IMAGE)
  frm.imlUpDownDisabled.MaskColor = &HC0C0C0
  frm.imlUpDownDisabled.UseMaskColor = True
  
  ' set the image list to the toolbar
  frm.tbrUpDown.ImageList = frm.imlUpDown
  frm.tbrUpDown.DisabledImageList = frm.imlUpDownDisabled
  frm.tbrUpDown.Buttons(1).Image = "Next"
  frm.tbrUpDown.Buttons(2).Image = "Previous"
End Sub

Public Function ValidateControls(Form As Form) As Boolean
  On Error Resume Next
  Form.ValidateControls
  ValidateControls = Not Err
End Function


'
' Navigation Refresh
'
Public Sub NavigationRefresh(Form As Form, View As ListView, Index As Integer)
  '
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  '
  If (View Is Nothing) Then
    Form.mnuViewPrevious.Enabled = False
    Form.mnuViewNext.Enabled = False
    Form.tbrUpDown.Buttons(1).Enabled = False
    Form.tbrUpDown.Buttons(2).Enabled = False
  Else
    Form.mnuViewNext.Enabled = View.Enabled And Index < View.ListItems.Count
    Form.mnuViewPrevious.Enabled = View.Enabled And Index > 1
    Form.tbrUpDown.Buttons(1).Enabled = Form.mnuViewNext.Enabled
    Form.tbrUpDown.Buttons(2).Enabled = Form.mnuViewPrevious.Enabled
  End If
End Sub
'
' When the text note gets focus disable the default
' buttons to allow the enter key to act as a carriage
' return while editing in the note
'
Public Sub NoteGotFocus(DefaultControl As CommandButton, Optional EnterControl As CommandButton)
  DefaultControl.Default = False
  If Not EnterControl Is Nothing Then
    EnterControl.Default = False
  End If
End Sub
'
' Reset the default buttons
'
Public Sub NoteLostFocus(DefaultControl As CommandButton, Optional EnterControl As CommandButton)
  DefaultControl.Default = Not LocaleEnterAsTab
  If Not EnterControl Is Nothing Then
    EnterControl.Default = LocaleEnterAsTab
  End If
End Sub

'
' Standardize call to write to form's ini file.
'
Public Sub WriteIniFile(FormName As String, Name As String, Value As String)
  IniForm.WriteFile FormName, Name, Value
End Sub

'
' Save form position.
'
Public Sub SerializePosition(Form As Form)
  If Form.WindowState = vbNormal Then
    WriteIniFile Form.Name, "Left", Form.Left
    WriteIniFile Form.Name, "Top", Form.Top
    If Form.BorderStyle = vbSizable Then
      WriteIniFile Form.Name, "Height", Form.Height
      WriteIniFile Form.Name, "Width", Form.Width
    End If
  End If
  WriteIniFile Form.Name, "WindowState", Form.WindowState
End Sub

'
' Standardize call to read from forms ini file.
'
Public Function ReadIniFile(FormName As String, Name As String) As String
  ReadIniFile = IniForm.ReadFile(FormName, Name)
End Function

'
' Load form position.  See if other forms of the same name are
' open and offset from those.
'
Public Sub DeserializePosition(Form As Form)
  Dim mLeft, mTop As String
  mLeft = ReadIniFile(Form.Name, "Left")
  mTop = ReadIniFile(Form.Name, "Top")
  
  Dim Window As Form
  For Each Window In Forms
    If Window.Name = Form.Name And Window.hWnd <> Form.hWnd Then
      If Window.WindowState = vbMinimized Then
        Window.WindowState = vbNormal
      End If
      Window.Show
      Window.Refresh
      mLeft = Window.Left + 288
      mTop = Window.Top + 336
    End If
  Next Window
  
  If mLeft <> vbNullString Then Form.Left = mLeft
  If mTop <> vbNullString Then Form.Top = mTop
  
  Dim Value As String
  Value = ReadIniFile(Form.Name, "WindowState")
  If Value <> vbNullString Then
    If Value = vbMaximized Then
      Form.WindowState = vbMaximized
      Exit Sub
    End If
  End If
  Form.WindowState = vbNormal
  
  If Form.BorderStyle = vbSizable Then
    Value = ReadIniFile(Form.Name, "Height")
    If Value <> vbNullString Then Form.Height = Value
    Value = ReadIniFile(Form.Name, "Width")
    If Value <> vbNullString Then Form.Width = Value
  End If
End Sub

'
' Used by form main when a child is unloaded.
'
Public Function FindLastForm(Form As Form) As Form
  Dim Window As Variant
  For Each Window In Forms
    If Window.Name = Form.Name And Window.hWnd <> Form.hWnd Then
      Set FindLastForm = Window
    End If
  Next Window
End Function

Public Sub SizeSingleToolbar(Bar As ToolBar, Left As Integer)
  Bar.Height = Bar.ButtonHeight
  Bar.Width = Bar.Buttons(1).Width
  Bar.Left = Left
End Sub

Public Sub ShowByEventListView(Parent As Form, Child As Form)
  Parent.FormMgr.Add Child
  Child.ShowByListView Parent.lvView, DatabaseName.EVENT_TABLE
End Sub

Public Sub ShowByListView(Form As Form, FormMgr As FormManager, View As ListView, Optional FormMgrKey)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no items to view or edit."
    Unload Form
    Exit Sub
  End If

  If IsMissing(FormMgrKey) Then
    FormMgr.Add Form
  Else
    FormMgr.Add Form, CStr(FormMgrKey)
  End If

  Form.ShowByListView View
End Sub

Public Sub ShowByPrimaryKey(Form As Form, Handle As String, Optional FormMgr As FormManager, Optional Index, Optional FormMgrKey)
  If Not FormMgr Is Nothing Then
    If IsMissing(FormMgrKey) Then
      FormMgr.Add Form
    Else
      FormMgr.Add Form, CStr(FormMgrKey)
    End If
  End If
  If IsMissing(Index) Then
    Form.ShowByPrimaryKey Handle
  Else
    Form.ShowByPrimaryKey Handle, CInt(Index)
  End If
End Sub

Public Sub ShowByPrimaryKeys(Form As Form, FormMgr As FormManager, Handle1 As String, Optional Handle2, Optional Index)
  FormMgr.Add Form
  If IsMissing(Index) Then
    Form.ShowByPrimaryKey Handle1, Handle2
  Else
    Form.ShowByPrimaryKey Handle1, Handle2, Index
  End If
End Sub

Public Sub ShowEventDialog(EventHandle As String, ActionType As Integer)
  Dim fForm As New frmEventDialog
  fForm.ReferenceLoad EventHandle, ActionType
  fForm.Show vbModal
  Unload fForm
End Sub

Public Function ShowEventProperties(EventHandle As String) As Boolean
  If EventHandle = vbNullString Then
    Exit Function
  End If
  
  Dim DataField As collection
  Set DataField = Factory.EventService.Find(EventHandle)
  
  If DataField Is Nothing Then
    Exit Function
  End If
  
  Dim fEvent As New frmEventPropertiesEdit
  fEvent.ShowEvent DataField
  ShowEventProperties = True
End Function

'ver 2.60.00 (jjm) 2004 04 01
'Added default values, removed the use of the isMissing function
Public Sub ShowNew(Form As Form, Optional FormMgr As FormManager = Nothing, Optional Handle = vbNullString, Optional FormMgrKey = vbNullString)
On Error GoTo Exception
  If Not FormMgr Is Nothing Then
    If FormMgrKey = vbNullString Then
      FormMgr.Add Form
    Else
      FormMgr.Add Form, CStr(FormMgrKey)
    End If
  End If
  
  If Handle = vbNullString Then
    Form.ShowNew
  Else
    Form.ShowNew Handle
  End If
  
  Exit Sub
  
Exception:
  MsgBox "Error Showing New Item: " & Err.Description, vbExclamation
End Sub


Public Function ShowProperties(sourceTypeId As String, EventHandle As String) As Boolean
'  Dim SourceId As String
'  SourceId = Replace(Source, "View", vbNullString)
'  SourceId = LCase(Left(SourceId, 1)) & Mid(SourceId, 2) & "Id"
  
  Dim DataProvider As DataProvider, DataField As collection
  Set DataProvider = LawSession.NewDataProvider
  DataProvider.Source = sourceTypeId
  
  Set DataField = DataProvider.Find(DatabaseName.EVENT_KEY, EventHandle)
  If DataField Is Nothing Then
    Exit Function
  End If
  
  Dim fProps As New frmPropertiesEdit
  fProps.ShowProperties sourceTypeId, DataField
  ShowProperties = True
End Function

Public Function ShowPropertiesMulti(Source As String, HandleName1 As String, HandleName2 As String, HandleValue1 As String, HandleValue2 As String) As Boolean
  Dim DataProvider As DataProvider, DataField As collection
  Set DataProvider = LawSession.NewDataProvider
  DataProvider.Source = Source
  DataProvider.filter = """" & HandleName1 & """ = '" & HandleValue1 & "' and " & _
      """" & HandleName2 & """ = '" & HandleValue2 & "'"
  
  DataProvider.OpenRecordset 1
  If DataProvider.IsEmpty Then
    Exit Function
  End If
  
  Dim fProps As New frmPropertiesEdit
  fProps.ShowProperties Source, DataProvider.collection(1)
  ShowPropertiesMulti = True
End Function

Public Sub SendToFolder(ruleId As String, Source As String, Handle As String, Name As String)
  ' Create form and show.
  Dim Form As New frmFolderBrowseDialog
  If Not Form.ShowSendTo(ruleId) Then
    Unload Form
    Exit Sub
  End If
  
  Dim Handles(0) As String, Names(0) As String
  Handles(0) = Handle
  Names(0) = Name
  
  Call Factory.EventService.Create( _
      Form.eventTypeId, _
      ruleId, _
      Form.FolderKey, _
      Handles, _
      Names)
 
  Unload Form
End Sub

'
' Used by SStabs to fix the tab order by enabling the control.
' Just call this code form the tab click routine and in Form_Load
'
Public Sub TabControlEnable(mSSTab As SSTab)
  Dim c As Control
  For Each c In mSSTab.Parent.Controls
    If Not (TypeOf c Is ImageList Or _
            TypeOf c Is Menu Or _
            TypeOf c Is CommonDialog Or _
            TypeOf c Is Timer Or _
            TypeOf c Is PictureBox) Then
      If c.Container Is mSSTab Then
        c.Enabled = c.Left > 0
      End If
    End If
  Next
End Sub


'
' Build popup menu for Events on the fly. The open list should be of referenced items
'
'@param Popup - popup menu form being modified
'@param Handle - eventId
Public Sub MenuEventLoad(Popup As Form, Handle As String)
  With Popup
    ' Unload current menu.
    FormUtil.MenuUnload .mnuEventOpen
  
    Dim EventData As collection
    Dim I, j As Integer
    Dim sourceTypeId As String
    Dim eventTypeId As String
    Dim EventTypeRuleId As String
    
    Set EventData = Factory.EventService.Find(Handle, DatabaseName.EVENTTYPE_KEY & "," & _
                                                      DatabaseName.EVENTTYPERULEID_FIELD & "," & _
                                                      DatabaseName.SOURCETYPE_KEY)
                                                      
    eventTypeId = GetDataFromCollection(EventData, DatabaseName.EVENTTYPE_KEY)
    EventTypeRuleId = GetDataFromCollection(EventData, DatabaseName.EVENTTYPERULEID_FIELD)
    sourceTypeId = GetDataFromCollection(EventData, DatabaseName.SOURCETYPE_KEY, True)
    
    'start off the menu counter
    I = 1
     
    'test for base event (Ex: Application, Entity, Quote, etc)
    'Hooks for functionality are located: frmPopup2.mnuEventOpen_Click
    If isSourceTypeMenu(sourceTypeId) Then
        Load .mnuEventOpen(I)
        createSourceTypeMenu .mnuEventOpen(I), sourceTypeId, eventTypeId
        I = I + 1
    End If
    
    
    
    'ToDo: Sort refs by caption
    'test for reference events
    Dim Refs As collection
    Set Refs = Factory.ReferenceService.FindUpReferences(Handle, DatabaseName.SOURCETYPE_KEY & ", " & DatabaseName.REFERENCESOURCETYPE_KEY & ", " & DatabaseName.REFERENCEEVENTTYPEID_FIELD)
    If Not Refs Is Nothing Then
      Dim ReferenceSource As String
      For j = 1 To Refs.Count
        'first try referenceSourceTypeId
        ReferenceSource = GetDataFromCollection(Refs(j), DatabaseName.REFERENCESOURCETYPE_KEY)
        'if null, try sourceTypeId
        If ReferenceSource = vbNullString Then
            ReferenceSource = GetDataFromCollection(Refs(j), DatabaseName.SOURCETYPE_KEY)
        End If
        'if sourceTypeId is found, setup the open source
        If isSourceTypeMenu(ReferenceSource) Then
          Load .mnuEventOpen(I)
          createSourceTypeMenu .mnuEventOpen(I), ReferenceSource, GetDataFromCollection(Refs(j), DatabaseName.REFERENCEEVENTTYPEID_FIELD)
          I = I + 1
        End If
      Next j
      
      Load .mnuEventOpen(I)
      .mnuEventOpen(I).Tag = "Notepad"
      .mnuEventOpen(I).Caption = "Open Notepad"
      I = I + 1
      
      'Hack
      If eventTypeId = "Claim" Then
        Load .mnuEventOpen(I)
        .mnuEventOpen(I).Tag = "BrowseClaim"
        .mnuEventOpen(I).Caption = "Browse Claim"
        I = I + 1
      End If
    End If
    
    'ver 2.21.0708 jjm
    'Hack
    'eventTypeId = worksheet claim then...
    If eventTypeId = "WorksheetClaim" Then
      Load .mnuEventOpen(I)
      .mnuEventOpen(I).Tag = "BrowseWorksheet"
      .mnuEventOpen(I).Caption = "Browse Worksheet"
      I = I + 1
    End If
    
    '4249
    'Hack
    If sourceTypeId = DatabaseName.RECEIPT_TABLE Then
      Load .mnuEventOpen(I)
      .mnuEventOpen(I).Tag = DatabaseName.RECEIPT_TABLE
      .mnuEventOpen(I).Caption = "Browse Receipt"
      I = I + 1
    End If


    'Online Application Form
    If eventTypeId = DatabaseName.ONLINEAPP_TYPE Then
      Load .mnuEventOpen(I)
      .mnuEventOpen(I).Tag = "BrowseOnlineApp"
      .mnuEventOpen(I).Caption = "Browse Online Application"
      I = I + 1
      Load .mnuEventOpen(I)
      .mnuEventOpen(I).Tag = DatabaseName.ONLINEAPP_TYPE
      .mnuEventOpen(I).Caption = "Edit Online Application"
      I = I + 1
    End If
    
    
    'Test for documents
    If sourceTypeId = DatabaseName.FIRM_DOCUMENT Or _
       sourceTypeId = DatabaseName.CLAIM_DOCUMENT Or _
       sourceTypeId = DatabaseName.ENDORSEMENT_TABLE Or _
       sourceTypeId = DatabaseName.INCIDENTDOCUMENT_RULE Or _
       sourceTypeId = DatabaseName.DOCUMENT_SOURCETYPE _
    Then
      Load .mnuEventOpen(I)
      '2473 - was using eventTypeRuleId when it needs to be sourceTypeId.
      .mnuEventOpen(I).Tag = sourceTypeId
      .mnuEventOpen(I).Caption = "Edit Document"
      I = I + 1
    End If
    
    .mnuEventOpen(0).Visible = False
    .mnuEventPaste.Enabled = (Not EventBuffer Is Nothing)
  End With
End Sub
'Helper function for MenuEventLoad. Stores a safe list for Open actions against sourceTypeIds
Public Function isSourceTypeMenu(sourceTypeId As String) As Boolean
  isSourceTypeMenu = _
       sourceTypeId = DatabaseName.APPLICATION_TABLE Or _
       sourceTypeId = DatabaseName.ENDORSEMENT_TABLE Or _
       sourceTypeId = DatabaseName.CLAIMRESERVE_TABLE Or _
       sourceTypeId = DatabaseName.CLAIMPAYMENT_TABLE Or _
       sourceTypeId = DatabaseName.COVERAGE_TABLE Or _
       sourceTypeId = DatabaseName.DEDUCTIBLEREIMBURSE_TABLE Or _
       sourceTypeId = DatabaseName.ENTITY_TABLE Or _
       sourceTypeId = DatabaseName.INSTANCE_TABLE Or _
       sourceTypeId = DatabaseName.POLICY_TABLE Or _
       sourceTypeId = DatabaseName.POLICYACCOUNTING_TABLE Or _
       sourceTypeId = DatabaseName.QUOTE_TABLE Or _
       sourceTypeId = DatabaseName.QUOTEBOOK_TABLE

End Function
'Bug #1659: difference between Firm, Person and Entity as well as Incident, Claim, and Instance.
Public Sub createSourceTypeMenu(sourceTypeMenu As Menu, sourceTypeId As String, eventTypeId)
  'default the type to the sourceTypeId
  Dim RedirectType As String
  RedirectType = sourceTypeId
  
  'Hack to set the type used for the menu to be the eventTypeId which will be either Firm, Person, Incident or Claim
  If sourceTypeId = DatabaseName.ENTITY_TABLE Or _
     sourceTypeId = DatabaseName.INSTANCE_TABLE Then
        RedirectType = eventTypeId
  End If
  
  sourceTypeMenu.Tag = RedirectType
  sourceTypeMenu.Caption = "Open " & RedirectType
End Sub



Public Sub Dirty(Form As Form, IsDirty As Boolean, Optional IsNew As Boolean)
  On Error Resume Next
  
  Dim isOpen As Boolean
  isOpen = IsDirty Or IsNew
  
  Form.mnuFileSave.Enabled = isOpen
  Form.cmdSave.Enabled = isOpen
  Form.cmdSaveNew.Enabled = isOpen
  Form.cmdCancel.Caption = IIf(IsDirty, "Cancel", "Close")
  
  Form.mnuFileWorkflow(0).Enabled = Not isOpen
  Form.mnuFileWorkflow(1).Enabled = Not isOpen
  
  ' Clear any error.
  Err.Clear
End Sub


Public Sub GLTransactionLoad(View As ListView, _
    GLAccountIds() As String, GLTransactionAmounts() As Double)
    
  Dim Count As Integer, I As Integer
  Count = View.ListItems.Count
  
  If Count = 0 Then Exit Sub
  
  ReDim GLAccountIds(Count - 1)
  ReDim GLTransactionAmounts(Count - 1)
  For I = 1 To Count
    GLAccountIds(I - 1) = View.ListItems(I)
    GLTransactionAmounts(I - 1) = View.ListItems(I).ListSubItems("transactionAmount")
  Next I
    
End Sub

'Public Sub LoadListViewCollection(LV As ListView, mCollection As Collection)
'  Dim n As Integer
'  Dim xItem As ListItem
'
'  Set xItem = LV.ListItems.Add(, , mCollection(LV.ColumnHeaders(1).Key))
'  For n = 2 To LV.ColumnHeaders.Count
'    xItem.ListSubItems.Add , LV.ColumnHeaders(n).Key, mCollection(LV.ColumnHeaders(n).Key)
'  Next n
'End Sub

Public Sub ComboBoxLoad(Items As collection, Combo As ComboBox, Index As String)
  If Items Is Nothing Then Exit Sub
  Dim I, Count As Integer
  Count = Items.Count
  For I = 1 To Count
    Combo.AddItem GetDataFromCollection(Items(I), Index)
  Next I
End Sub

Public Sub ComboBoxRegistryLoad(Combo As ComboBox, Key As String)
  Dim List As String
  Combo.Clear
  List = IniMain.ReadFile("Dropdown", Key)
  If List <> vbNullString Then
    ComboBoxArrayLoad Combo, Split(List, "|")
  End If
End Sub

Public Sub ComboBoxArrayLoad(Combo As ComboBox, List As Variant)
  Dim I, Count As Integer
  Count = UBound(List)
  For I = 0 To Count
    Combo.AddItem List(I)
  Next I
End Sub

'Public Sub ShowLawyerByPrimaryKey(Form As Form, FormMgr As FormManager, LawyerHandle As String, Optional LawyerTab As Boolean, Optional FormMgrKey)
'  Dim PersonId As String
'  PersonId = Factory.LawyerService.Find(LawyerHandle, """" + DatabaseName.PERSON_KEY + """")(DatabaseName.PERSON_KEY)
'  If (PersonId = vbNullString) Then
'    MsgBox "Data corruption problem.  The lawer selected has lost its reference to a person."
'    Exit Sub
'  End If
'  Dim fPerson As New frmPersonEdit
'  SetFormMgr fPerson, FormMgr, FormMgrKey
'  fPerson.ShowByPrimaryKey PersonId, LawyerTab
'End Sub

Private Sub SetFormMgr(Form As Form, FormMgr As FormManager, Optional FormMgrKey)
  If IsMissing(FormMgrKey) Then
    FormMgr.Add Form
  Else
    FormMgr.Add Form, CStr(FormMgrKey)
  End If
End Sub

Public Function PrintEnvelope(Optional DeliveryAddress As String, Optional DeliveryPoint As String) As Boolean
  Dim fEnvelope As New frmEnvelopeLabel
  fEnvelope.DeliveryAddress = DeliveryAddress
  fEnvelope.DeliveryPoint = DeliveryPoint
  fEnvelope.PrintBarCode = LocaleEnvelopeBarCode
  fEnvelope.Show vbModal
  PrintEnvelope = fEnvelope.Ok
  Unload fEnvelope
End Function

Public Function Today(Optional DateTime As Date) As Date
  If DateTime = vbNullDate Then
    DateTime = Now
  End If
  Today = DateSerial(Year(DateTime), Month(DateTime), Day(DateTime))
End Function
Public Function GLTransactionAdd(View As ListView, _
    GLAccountId As String, Name As String, Reference As String, Amount As Double) As ListItem
  
  Dim xItem As ListItem
  Set xItem = View.ListItems.Add(, , GLAccountId)
  xItem.ListSubItems.Add , "transactionName", Name
  xItem.ListSubItems.Add , "transactionReference", Reference
  xItem.ListSubItems.Add , "transactionAmount", Amount
  xItem.ListSubItems.Add , "~debit"
  xItem.ListSubItems.Add , "~credit"
  Set GLTransactionAdd = xItem
End Function

Public Sub GLTransactionEdit(item As ListItem, _
    GLAccountId As String, Name As String, Reference As String, Amount As Double)
    
  item.Text = GLAccountId
  item.ListSubItems("transactionName").Text = Name
  item.ListSubItems("transactionReference").Text = Reference
  item.ListSubItems("transactionAmount").Text = Amount
End Sub
    

Public Sub GLTransactionSummary(Items As ListItems, _
    Optional lblDebit As Label, Optional lblCredit As Label, Optional lblBalance As Label, Optional Add As Boolean)
  
  Dim item As ListItem
  Dim Debits As Double, Credits As Double
  If Add Then
    Debits = GetDouble(lblDebit)
    Credits = GetDouble(lblCredit)
  End If
  For Each item In Items
    If item.ListSubItems("transactionAmount") < 0 Then
      item.ListSubItems("~debit") = ""
      item.ListSubItems("~credit") = Format(0 - item.ListSubItems("transactionAmount"), "#,##0.00;(#,##0.00)")
      Credits = Credits + item.ListSubItems("~credit")
    Else
      item.ListSubItems("~debit") = Format(item.ListSubItems("transactionAmount"), "#,##0.00;(#,##0.00)")
      item.ListSubItems("~credit") = ""
      Debits = Debits + item.ListSubItems("~debit")
    End If
  Next item
  
  If Not lblDebit Is Nothing Then
    lblDebit.Caption = Format(Debits, "#,##0.00;(#,##0.00)")
  End If
  
  If Not lblCredit Is Nothing Then
    lblCredit.Caption = Format(Credits, "#,##0.00;(#,##0.00)")
  End If
  
  If Not lblBalance Is Nothing Then
    lblBalance.Caption = Format(Debits - Credits, "#,##0.00;(#,##0.00)")
  End If

End Sub

Public Function ExportItem(View As ListView, ExportIndex As Integer, SaveDialog As CommonDialog, Optional FileName As String, Optional FolderKey As String) As Boolean
  Dim pointer As New WaitPointer
  pointer.Default
 
  Dim ReferenceTable As String, ServiceURI As String, Selected() As String
  Select Case ExportIndex
    Case 0
      ReferenceTable = DatabaseName.APPLICATION_TABLE
      ServiceURI = "applicationprint.new"
      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
    Case 1
      ReferenceTable = DatabaseName.APPLICATION_TABLE
      ServiceURI = "applicationpartIIprint.new"
      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
    Case 2
      ReferenceTable = DatabaseName.APPLICATION_TABLE
      ServiceURI = "applicationpartIIIprint.custom"
      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
    Case 3
      'keep in sync with frmPopup2.mnuGLBatchOpen(). Mantis #2355
      ServiceURI = "glbatchexport.custom"
      Selected = ListViewModule.ListItemArray(View, DatabaseName.GLBATCH_KEY)
    Case 4
      ServiceURI = "eventprint.custom"
      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
    Case 5
      'event exported needs to be the finance event
      ServiceURI = "financeprint.custom"
      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
    Case 6
      ServiceURI = "policyprint.custom"
      ReferenceTable = DatabaseName.POLICY_TABLE
      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
  End Select

  Dim Key As String, Params As String, I, Count As Integer
    
  'cache the sourceTypeId's of the actual records
  Dim sourceTypeCache() As String
  If ReferenceTable <> vbNullString Then
    sourceTypeCache = ListViewModule.ListItemArray(View, DatabaseName.SOURCETYPE_KEY)
  End If
    
  'Configure the params section of the url
  For I = 0 To SafeUBound(Selected)
    If ReferenceTable = vbNullString Then
      Key = Selected(I)
    Else
      'In this block, the source(I) will be an eventId field value because the ReferenceTable is set.
      
      'Test for self reference. In Malcolm 1.x Events referenced themselves.
      If sourceTypeCache(I) = ReferenceTable Then
        Key = Selected(I)
      Else
        'Search for the reference. ReferenceTable is being used to match the EventType.sourceTypeId.
        Key = Factory.ReferenceService.FindReferenceHandleBySourceType(Selected(I), ReferenceTable)
      End If
      
      'Confirm that its not the source object. This is not needed if
      'EventType.sourceTypeId is supplied in the ListView as a column.
      If Key = vbNullString Then
        Key = DataUtil.FindHandle(ReferenceTable, DatabaseName.EVENT_KEY, Selected(I), DatabaseName.EVENT_KEY)
      End If
    End If
    
    
    If Not Key = vbNullString Then
      Params = Params & IIf(Params = "", "", "&") & "key=" & EncodeURL(Key)
    End If
  Next I

  If Params = vbNullString Then
    MsgBox "Nothing was found to export."
    Exit Function
  End If
  
  'ver 2.50.00 (jjm) 2004 02 12
  'Load the default filename
  If FileName = vbNullString Then
    FileName = IniMain.ReadFile("defaults", "exportfilename")
  End If
  
  'Call the http request and show the save dialog, if save then write the file
  'bug 1854, updated service call for 3.0 api
  ExportItem = CommonDialogUtil.HttpSave(SaveDialog, HttpServer & WebUtil & ServiceURI & "?display=text/plain", Params, FileName)
  
  'If the export failed then exit
  If Not ExportItem Then Exit Function
  
  
  'Set the default file name
  'filename was passed by ref so any changes made to the filename will continute back
  If FileName <> vbNullString Then
    IniMain.WriteFile "defaults", "exportfilename", FileName
  End If
End Function

Public Sub SetTextControlFont(TextControl As Control, vFont As StdFont)
  TextControl.Font = vFont.Name
  TextControl.FontStrikethru = vFont.Strikethrough
  TextControl.FontUnderline = vFont.Underline
  TextControl.FontItalic = vFont.Italic
  TextControl.FontBold = vFont.Bold
  TextControl.FontSize = vFont.Size
End Sub


Public Sub SetComboDefault(Combo As Control, Optional Ref As String)
  IniMain.WriteFile "defaults", Combo.Tag & Ref, Combo.Text
End Sub

Public Sub GetComboDefault(Combo As Control, Optional Ref As String)
  Dim I As Integer, Key As String
    
  'Test for empty box
  If Combo.ListCount = 0 Then
    Combo.ListIndex = -1
    Exit Sub
  End If

  Combo.ListIndex = 0
  Key = IniMain.ReadFile("defaults", Combo.Tag & Ref)
  
  For I = 0 To Combo.ListCount - 1
    If Combo.List(I) = Key Then
      Combo.ListIndex = I
      Exit For
    End If
  Next I
  
  
  
End Sub
 
