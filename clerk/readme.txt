LAW Clerk readme.txt

Last updated 1/13/03

============
Introduction
============
This is file contains last minute configuration instructions. These configuartion notes have default settings but should be changed for each company's implementation. Please browse each section to determine if changes need to be made.


===================
Installation Notes
===================
It is recommended to install using the default options. If you change the installation path on setup, you will also need to change the unzip location when prompted at the end of the setup. This zip file is also installed in the bin folder of the application�s path. If you leave the default install location as is, all you have to do when prompted is click the unzip button and then close. You also will be prompted to install MS Soap 3.0. During this installation it is also recommended to use the default selections. MS XML Parser 4.0 is included in the default installation of MS Soap 3.0 and is needed for Clerk to function. 


================================
The location of the \conf folder
================================
The conf folder contains ini files that LAW depends on for the folder settings of the Explorer Window. These settings can be stored either locally or centrally. This file stores all the properties used to display data and generally is helpfull if all users use the same file. By default the conf folder is installed to ($AppPath)\local\conf. The location in which clerk will use is set in the INI file. View lawinireadme.txt for details.


==========================================
DefaultFolder.exe and [$path]\conf\default
==========================================
This file is a self extracting zip file. It is automatically launched near the end of setup. This file can also be found in [$AppPath]\bin\DefaultFolder.exe. This file needs to be unzipped to the conf folder. When prompted enter the path for the conf folder so that the files install to the [$path]\conf\default directory.

This is only a temporary measure until better install software is obtained.


=======
law.ini
=======
This is the ini file that installed to the application's folder. This file contains many of the personal settings needed for Clerk to function. For more information please check out the lawinireadme.txt supplied in applications base folder. This file contains information on different ini settings.

