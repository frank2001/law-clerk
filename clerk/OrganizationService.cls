VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OrganizationService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const ServiceName = "OrganizationService"

Private Client As SoapClient30
Private mDataSet As Dataset


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session


  Set mDataSet = NewGenericDataset(DatabaseName.ORGANIZATION_TABLE)
End Sub


'
' SOAP Interface to server
'


'
' Create an organization.
'
Public Function CreateOrganization( _
        orgType As String, isVendor As Boolean, _
        orgLocator As String, orgName As String, entityURL As String, _
        orgLegalForm As String, stateBarNumber As String, _
        EstablishDate As Date, NextEffectiveDate As Date, _
        LocationHandle, LocationTypeHandle, _
        AddressLine1 As String, AddressLine2 As String, AddressLine3 As String, _
        State As String, City As String, _
        Zip As String, County As String, _
        Country As String, _
        Telephone As String, _
        Fax As String, Email As String, _
        PersonHandle As String, eventNote As String)

On Error GoTo ErrorHandler
  
  CreateOrganization = Client.Create( _
        orgType, isVendor, _
        orgLocator, orgName, entityURL, _
        orgLegalForm, stateBarNumber, _
        EstablishDate, NextEffectiveDate, _
        LocationHandle, LocationTypeHandle, _
        AddressLine1, AddressLine2, AddressLine3, _
        State, City, _
        Zip, County, Country, _
        Telephone, _
        Fax, Email, _
        PersonHandle, eventNote)
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
'  MsgBox "Fault: " & Client.FaultString, vbExclamation
  
End Function

'
' Write an Organization
'
Public Function WriteOrganization( _
        organizationHandle As String, isVendor As Boolean, _
        orgLocator As String, orgName As String, entityURL As String, _
        orgLegalForm As String, stateBarNumber As String, _
        EstablishDate As Date, NextEffectiveDate As Date, _
        LocationHandle, LocationTypeHandle, _
        AddressLine1 As String, AddressLine2 As String, AddressLine3 As String, _
        State As String, City As String, _
        Zip As String, County As String, _
        Country As String, _
        Telephone As String, _
        Fax As String, Email As String, _
        PersonHandle As String, eventNote As String, firmHandle As String) As Boolean
On Error GoTo ErrorHandler
  
  Call Client.Write( _
        organizationHandle, isVendor, _
        orgLocator, orgName, entityURL, _
        orgLegalForm, stateBarNumber, _
        EstablishDate, NextEffectiveDate, _
        LocationHandle, LocationTypeHandle, _
        AddressLine1, AddressLine2, AddressLine3, _
        State, City, _
        Zip, County, Country, _
        Telephone, _
        Fax, Email, _
        PersonHandle, eventNote, firmHandle)
  
  
'      String organizationId,
'      String organizationLocator,
'      String organizationName,
'      String organizationLegalForm,
'      String stateBarNumber,
'      Date establishDate,
'      Date nextEffectiveDate,
'      String locationTypeId,
'      String addressLine1,
'      String addressLine2,
'      String addressLine3,
'      String city,
'      String state,
'      String zip,
'      String county,
'      String country,
'      String telephone,
'      String fax,
'      String email,
'      String eventNote,
'      String firmId,
'      String firmNumber,
'      boolean isVendor)
  
  
  
  
  
  WriteOrganization = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  WriteOrganization = False
End Function




Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.ORGANIZATION_KEY, Handle, Column)
End Function





