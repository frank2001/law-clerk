VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "comct332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEndorsementEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Endorsement"
   ClientHeight    =   5250
   ClientLeft      =   150
   ClientTop       =   750
   ClientWidth     =   8475
   Icon            =   "frmEndorsementEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5250
   ScaleWidth      =   8475
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4896
      Left            =   0
      ScaleHeight     =   4890
      ScaleWidth      =   8475
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   384
      Width           =   8472
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   5880
         TabIndex        =   14
         Top             =   4416
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   7188
         TabIndex        =   15
         Top             =   4416
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   4248
         Left            =   72
         TabIndex        =   34
         Top             =   60
         Width           =   8316
         _ExtentX        =   14658
         _ExtentY        =   7488
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmEndorsementEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label11"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label21"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label5"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label6"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Label3"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Label4"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Label14"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "Label7"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "Label8"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "Label13"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtpDataField(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "dtpDataField(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtDataField(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtDataField(3)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtDataField(5)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtDataField(6)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtDataField(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtDataField(8)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtDataField(9)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtDataField(16)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "dtpDataField(1)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtDataField(2)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmEndorsementEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).Control(1)=   "cmdClearNotepad"
         Tab(1).ControlCount=   2
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74892
            TabIndex        =   13
            Top             =   3816
            Width           =   1028
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "endorsementTypeId"
            Height          =   300
            Index           =   2
            Left            =   1860
            TabIndex        =   1
            Text            =   "endorsementTypeId"
            Top             =   1032
            Width           =   2316
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "effectiveDate"
            Height          =   300
            Index           =   1
            Left            =   5868
            TabIndex        =   8
            Top             =   1020
            Width           =   2316
            _ExtentX        =   4075
            _ExtentY        =   529
            _Version        =   393216
            Format          =   77529089
            CurrentDate     =   37335
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "pollItem"
            Height          =   300
            Index           =   16
            Left            =   5868
            TabIndex        =   11
            Text            =   "pollItem"
            Top             =   2112
            Width           =   2316
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "premium"
            Height          =   300
            Index           =   9
            Left            =   5868
            TabIndex        =   10
            Text            =   "premium"
            Top             =   1752
            Width           =   2316
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventTypeId"
            Height          =   300
            Index           =   8
            Left            =   1860
            TabIndex        =   3
            Text            =   "eventTypeId"
            Top             =   1752
            Width           =   2316
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventStatus"
            Height          =   300
            Index           =   7
            Left            =   1860
            TabIndex        =   4
            Text            =   "eventStatus"
            Top             =   2100
            Width           =   2316
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventId"
            Height          =   300
            Index           =   6
            Left            =   1860
            TabIndex        =   5
            Text            =   "eventId"
            Top             =   2460
            Width           =   2316
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "endorsementDesc"
            Height          =   300
            Index           =   5
            Left            =   1860
            TabIndex        =   6
            Text            =   "endorsementDesc"
            Top             =   2820
            Width           =   6348
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "coverageId"
            Height          =   300
            Index           =   3
            Left            =   1860
            TabIndex        =   0
            Text            =   "coverageId"
            Top             =   660
            Width           =   2316
         End
         Begin VB.TextBox txtNote 
            Height          =   3336
            Left            =   -74892
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   12
            Top             =   372
            Width           =   8088
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "endorsementNum"
            Height          =   300
            Index           =   1
            Left            =   1860
            TabIndex        =   2
            Text            =   "endorsementNum"
            Top             =   1392
            Width           =   2316
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "issueDate"
            Height          =   300
            Index           =   0
            Left            =   5868
            TabIndex        =   7
            Top             =   660
            Width           =   2316
            _ExtentX        =   4075
            _ExtentY        =   529
            _Version        =   393216
            Format          =   77529089
            CurrentDate     =   37335
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "expirationDate"
            Height          =   300
            Index           =   2
            Left            =   5868
            TabIndex        =   9
            Top             =   1392
            Width           =   2316
            _ExtentX        =   4075
            _ExtentY        =   529
            _Version        =   393216
            Format          =   77529089
            CurrentDate     =   37335
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Endorsement Type Id:"
            Height          =   192
            Left            =   120
            TabIndex        =   33
            Top             =   1092
            Width           =   1584
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Expiration Date:"
            Height          =   192
            Left            =   4548
            TabIndex        =   32
            Top             =   1446
            Width           =   1128
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Issue Date:"
            Height          =   192
            Left            =   4548
            TabIndex        =   31
            Top             =   708
            Width           =   804
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "PollItem"
            Height          =   192
            Left            =   4548
            TabIndex        =   30
            Top             =   2160
            Width           =   576
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Premium:"
            Height          =   192
            Left            =   4548
            TabIndex        =   29
            Top             =   1800
            Width           =   672
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Left            =   4548
            TabIndex        =   28
            Top             =   1074
            Width           =   1032
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Event Id:"
            Height          =   192
            Left            =   120
            TabIndex        =   27
            Top             =   2532
            Width           =   612
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Event Type Id"
            Height          =   192
            Left            =   120
            TabIndex        =   26
            Top             =   1800
            Width           =   996
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            Caption         =   "Endorsement Desc:"
            Height          =   192
            Left            =   120
            TabIndex        =   25
            Top             =   2880
            Width           =   1416
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Event Status:"
            Height          =   192
            Left            =   120
            TabIndex        =   24
            Top             =   2160
            Width           =   924
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Coverage Id:"
            Height          =   192
            Left            =   120
            TabIndex        =   16
            Top             =   708
            Width           =   924
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Endorsement Number:"
            Height          =   192
            Left            =   120
            TabIndex        =   17
            Top             =   1440
            Width           =   1608
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   276
      TabIndex        =   20
      Top             =   780
      Width           =   1056
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   8472
      _ExtentX        =   14949
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   8475
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   330
         ScaleWidth      =   8370
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   24
         Width           =   8376
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   24
            TabIndex        =   23
            Top             =   24
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   16777215
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   636
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Endorsement"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator3872 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Contact (Firm)"
            Enabled         =   0   'False
            Index           =   0
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Firm"
            Enabled         =   0   'False
            Index           =   1
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Application"
            Enabled         =   0   'False
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuS4r324 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Firm"
            Enabled         =   0   'False
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   1
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   2
      End
      Begin VB.Menu mnuSeparatro2823 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact (Firm)"
         Enabled         =   0   'False
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Enabled         =   0   'False
         Index           =   1
      End
      Begin VB.Menu mnuSeparator8371 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator86732 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
      End
      Begin VB.Menu mnuSeperator333 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Enabled         =   0   'False
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&Workflow Options..."
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmEndorsementEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mEndorsementHandle As String
Private mEventHandle As String
Private mNew As Boolean
Private mDatafields As Collection
Private mDataSet As Dataset

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mCancel As Boolean

Private mLeft As Integer
Private mTop As Integer

Private mParameterNames() As String
Private mParameterValues() As String

' Form Manager
Private FormMgr As New FormManager

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Child is either a Firm
' select (Firm) from cmdFirm or a new firm (NewFirm) from mnuFileNewFirm.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new form did not specify any action.
    Case fimNone
      Show
      Exit Sub

    ' Select form responded for something new
    Case fimNew
'      Exit Sub

    ' Select form selected
    Case fimSelect

    'Child form saved
    Case fimSave
'      If fInterface.Key = DatabaseName.FIRM_TABLE & "Edit" Then
'        mFirmHandle = fInterface.Child.FirmHandle
'        Prepare
'      End If
  End Select
  Show

End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub


Private Sub FormRefresh()
  If (mNew) Then
     Caption = "New Endorsement"
     
     ClearDataField txtDataField
     ClearDataField dtpDataField

     mEventHandle = vbNullString
  Else
    Set mDatafields = mDataSet.DataProvider.Find(DatabaseName.ENDORSEMENT_KEY, mEndorsementHandle)
    SetFields txtDataField, mDatafields
    SetFields dtpDataField, mDatafields
    
    Me.Caption = "Endorsement Edit (" & mEndorsementHandle & ")"
    
    mEventHandle = mDatafields.item(DatabaseName.EVENT_KEY)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
  End If
      
  mnuFileOpen.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  
  Dirty = False
  
  'standard navigation buttons
  FormUtil.NavigationRefresh Me, mListView, mListIndex
End Sub

Public Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mEndorsementHandle = ListItemText(mListView.SelectedItem, DatabaseName.ENDORSEMENT_KEY)
End Property

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If

  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If

  ' End Standard Code

  ' Start custom Code

  ' Client side duplication
'  If mNew And Factory.ContactService.FindByFirm(mFirmHandle, "Firm") Is Nothing Then
'    Select Case MsgBox("You can not save a new endorsement without creating a firm contact.  Would you like to create a new firm contact now?", vbYesNo)
'      Case vbYes
'        mnuFileNewOther_Click 0
'        Exit Function
'      Case Else
'        Exit Function
'    End Select
'  End If
  
  '
  ' Save Data
  '
  
  Dim mDataControls As New Collection
  AddValueControl dtpDataField, mDataControls
  AddTextControl txtDataField, mDataControls
  
'  Dim ParameterNames() As String
'  Dim ParameterValues() As String
'  ParameterNames = mParameterNames
'  ParameterValues = mParameterValues
'  AddParameter "EndorsementEventNote", _
'      IIf(txtNote.Text = vbNullString, " ", txtNote.Text), _
'      ParameterNames, ParameterValues
'
  If (mNew) Then
'    mEndorsementHandle = Factory.EndorsementService.CreateEndorsement( _
'              vbNullString, _
'              mFirmHandle, vbNullString, EndorsementTypeId, lblDataField(2).Caption, GetDate(mDataControls("effectiveDate")), GetDouble(txtDataField(1)), _
'              GetDouble(txtDataField(2)), GetDate(mDataControls("receiveDate")), _
'              vbNullStringArray, vbNullStringArray, ParameterNames, ParameterValues)
'    Save = mEndorsementHandle <> vbNullString
  Else
'    Save = Factory.EndorsementService.WriteEndorsement(mEndorsementHandle, _
'        mFirmHandle, vbNullString, EndorsementTypeId, lblDataField(2), GetDate(mDataControls("effectiveDate")), GetDouble(txtDataField(1)), _
'        GetDouble(txtDataField(2)), GetDate(mDataControls("receiveDate")), _
'        vbNullStringArray, vbNullStringArray, ParameterNames, ParameterValues)
  End If
  
  If save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  
  Dirty = Not save

End Function

Public Sub ShowNew()
  mNew = True
  Show
  
  Dirty = False

End Sub
Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no endorsements to view or edit." ' To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show

End Sub
Public Sub ShowByPrimaryKey(EndorsementId As String)
  mEndorsementHandle = EndorsementId
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Open up a select form for firm.  The response to the select
' is processed under FormChildUnload.
'

Private Sub cmdSave_Click()
  
  If save Then
    Unload Me
  End If
  
'  Dim WasNew As Boolean
'  WasNew = mNew
'  If Save Then
'    FormMgr.Message = fimSave
'    If TypeOf FormMgr.Parent Is frmEndorsementOpen Then
'      Unload Me
'      Exit Sub
'    End If
'
'    Unload Me
'
'    If WasNew Then
'      ' DON'T register FormMgr.
''      FormUtil.ShowByPrimaryKey New frmEndorsementOpen, mEndorsementHandle
'    End If
'  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_Initialize()
  Set mDataSet = New EndorsementDataset
  Set mDataSet.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyF
        mnuFileNewOther_Click 1
        KeyCode = 0
    End Select
  End If

End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  'Custom Accelerators
  KeyPreview = True
 ' mnuFileNewOther(1).Caption = mnuFileNewOther(1).Caption + vbTab + "Ctrl+Shift+F"
  
  ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
 
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
 
'  Factory.FolderParameterService.ParameterLoad DatabaseName.ENDORSEMENT_TABLE, lawActionNew, mParameterNames, mParameterValues
  
  FormRefresh
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = True
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Resize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuEdit_Click(Index As Integer)
'  Select Case Index
'    Case 0
'      Dim fContact As New frmContactEdit
'      FormMgr.Add fContact
'      fContact.ShowByFirm mFirmHandle, "Firm"
'    Case 1
'      FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mFirmHandle, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
'  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not save(True) Then Exit Sub
  mNew = True
  mEndorsementHandle = vbNullString
  FormRefresh
End Sub

Private Sub mnuFileNewOther_Click(Index As Integer)
  Select Case Index
    Case 0
'      If mFirmHandle = vbNullString Then
'        MsgBox "No firm has been associated with this endorsement.  To insert a new firm, use {File}{New}{Firm}."
'        Exit Sub
'      End If
'      Dim Contacts As Collection
'      Set Contacts = Factory.ContactService.FindByFirm(mFirmHandle, "Firm")
'      If Contacts Is Nothing Then
'        Dim fContact As New frmContactEdit
'        FormMgr.Add fContact
'        fContact.ShowNewByFirm mFirmHandle, Factory.FirmService.Find(mFirmHandle, """locationId""")(databasename.LOCATION_KEY)
'      Else
'        MsgBox "Contact record already exists for this firm."
'        Exit Sub
'      End If
    Case 1
'      FormUtil.ShowNew New frmOrganizationEdit, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub

'
' Check for changes and open endorsement
'
Private Sub mnuFileOpen_Click()
'  FormUtil.ShowByPrimaryKey New frmEndorsementOpen, mEndorsementHandle, FormMgr
End Sub

Private Sub mnuFileOpenOther_Click(Index As Integer)
'  Select Case Index
'    Case 0
'      If mFirmHandle = vbNullString Then
'        MsgBox "No firm has been associated with this endorsement.  To insert a new firm, use {File}{New}{Firm}."
'        Exit Sub
'      End If
'      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
'  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.ENDORSEMENT_TABLE, mEndorsementHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuFileSendToFolder_Click()
'  FormUtil.SendToFolder DatabaseName.ENDORSEMENT_REF, DatabaseName.ENDORSEMENT_TABLE, mEndorsementHandle, lblDataField(2).Caption
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
 ' FolderUtil.WorkflowShow DatabaseName.ENDORSEMENT_TABLE, lawActionNew, _
 '     mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  mListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  mListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub


Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  'FormResize
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub


Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
