VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Filter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Filter"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarFilter As String 'local copy

Public Sub Filter_Initialize()
  mvarFilter = ""
End Sub


Public Sub Add(str As String)
  If mvarFilter <> "" Then
    mvarFilter = mvarFilter & " and "
  End If
  mvarFilter = mvarFilter & str
End Sub


Public Sub AddOr(str As String)
  If mvarFilter <> "" Then
    mvarFilter = mvarFilter & " or "
  End If
  mvarFilter = mvarFilter & str
End Sub

Public Property Get Filter() As String
Attribute Filter.VB_UserMemId = 0
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Filter
    Filter = mvarFilter
End Property



