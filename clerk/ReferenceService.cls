VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ReferenceService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'Ver 2.00.1211

Option Explicit

Private Const ServiceName = "ReferenceService"

Private mClient As SoapClient30
Private mDataSet As Dataset


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  'Set mClient = New SoapClient30
  'mClient.mssoapinit HttpServer &  "component/" & ServiceName & ".wsdl"
  'mClient.ConnectorProperty("EndPointURL") = HttpServer &  WebService & "rpcrouter" & LawSession.Session
  
  Set mDataSet = NewGenericDataset(DatabaseName.EVENTREFERENCE_VIEW)
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.REFERENCE_KEY, Handle, Column)
End Function

Public Function FindReferenceHandleBySourceType(EventHandle As String, SourceTypeId As String) As String
  mDataSet.DataProvider.Column = DatabaseName.EVENTREFERENCE_KEY
  mDataSet.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, EventHandle) & " and " & _
                                 LawDialect.EqualString(DatabaseName.REFERENCESOURCETYPE_KEY, SourceTypeId)
  mDataSet.DataProvider.OpenRecordset
  
  Dim references As Collection
  Set references = mDataSet.DataProvider.Collection
  
  If Not references Is Nothing Then
    FindReferenceHandleBySourceType = GetDataFromCollection(references(1), DatabaseName.EVENTREFERENCE_KEY)
    Exit Function
  End If
  
  'Code smell, hack
  'No reference was found. Test for self reference. In the previous architecture objects could reference themselves.
  'The following test, as clunkys a it is, reporduces the old functionality.
  FindReferenceHandleBySourceType = DataUtil.FindHandle(SourceTypeId, _
                                                        DatabaseName.EVENT_KEY, _
                                                        EventHandle, _
                                                        DatabaseName.EVENT_KEY)
    
End Function

Public Function FindReferenceHandleByEventType(EventHandle As String, EventTypeHandle As String) As String
  mDataSet.DataProvider.Column = DatabaseName.EVENTREFERENCE_KEY
  mDataSet.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, EventHandle) & " and " & _
                                 LawDialect.EqualString(DatabaseName.REFERENCEEVENTTYPEID_FIELD, EventTypeHandle)
  mDataSet.DataProvider.OpenRecordset
  
  Dim references As Collection
  Set references = mDataSet.DataProvider.Collection
  If references Is Nothing Then Exit Function
    
  FindReferenceHandleByEventType = GetDataFromCollection(references(1), DatabaseName.EVENTREFERENCE_KEY)
End Function



'
' Return colleciton of references for a given event handle keyed by table.
'

'Ex: Starting from a Firm and getting a document, application, etc
Public Function FindDownReferences(EventReferenceHandle As String, Optional Column As String, Optional SortItem As String) As Collection
  mDataSet.DataProvider.Column = Column
  mDataSet.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENTREFERENCE_KEY, EventReferenceHandle)
  mDataSet.DataProvider.Sort = IIf(SortItem <> vbNullString, SortItem, vbNullString)
  mDataSet.DataProvider.OpenRecordset
  If mDataSet.DataProvider.IsEmpty Then
    Exit Function
  End If
  
  Set FindDownReferences = mDataSet.DataProvider.Collection
'
'  Dim references As Collection
'  Set references = mDataSet.DataProvider.Collection
'  If references Is Nothing Then Exit Function
'
'  Dim List As New Collection, I As Integer
'  For I = 1 To references.Count
'    With references.item(I)
'      List.Add .item(DatabaseName.EVENT_KEY).Value, .item(DatabaseName.EVENTTYPE_KEY)
'    End With
'  Next I
'
'  Set ReferenceList = List
End Function

'Ex: Starting from document and getting a Firm or a claim
Public Function FindUpReferences(EventHandle As String, Optional Column As String, Optional SortItem As String) As Collection
  mDataSet.DataProvider.Column = Column
  mDataSet.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, EventHandle)
  mDataSet.DataProvider.Sort = IIf(SortItem <> vbNullString, SortItem, vbNullString)
  mDataSet.DataProvider.OpenRecordset
  If mDataSet.DataProvider.IsEmpty Then
    Exit Function
  End If
  
  Set FindUpReferences = mDataSet.DataProvider.Collection
End Function

'Find all references (both up and down)
Public Function FindReferences(EventHandle As String, Optional Column As String, Optional SortItem As String) As Collection
  mDataSet.DataProvider.Column = Column
  mDataSet.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, EventHandle) & " or " & _
                                 LawDialect.EqualString(DatabaseName.EVENTREFERENCE_KEY, EventHandle)
  mDataSet.DataProvider.Sort = IIf(SortItem <> vbNullString, SortItem, vbNullString)
  mDataSet.DataProvider.OpenRecordset
  If mDataSet.DataProvider.IsEmpty Then
    Exit Function
  End If
  
  Set FindReferences = mDataSet.DataProvider.Collection
End Function
