VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DataField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarValue As Variant
Private mvarName As String
Private mvarSource As String


Public Property Let Source(ByVal vData As String)
    mvarSource = vData
End Property


Public Property Get Source() As String
    Source = mvarSource
End Property


Public Property Let Value(ByVal vData As Variant)
    mvarValue = vData
End Property


Public Property Set Value(ByVal vData As Variant)
    Set mvarValue = vData
End Property


Public Property Get Value() As Variant
Attribute Value.VB_UserMemId = 0
    If IsObject(mvarValue) Then
        Set Value = mvarValue
    Else
        Value = mvarValue
    End If
End Property


Public Property Let Name(ByVal vData As String)
  mvarName = vData
End Property


Public Property Get Name() As String
  Name = mvarName
End Property
