VERSION 5.00
Begin VB.Form frmExportDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Export"
   ClientHeight    =   1860
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   6012
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   6012
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cboExport 
      Height          =   288
      ItemData        =   "frmExportDlg.frx":0000
      Left            =   396
      List            =   "frmExportDlg.frx":0019
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1032
      Width           =   3816
   End
   Begin VB.CommandButton CancelButton 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   4680
      TabIndex        =   2
      Top             =   552
      Width           =   1200
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   4680
      TabIndex        =   1
      Top             =   120
      Width           =   1200
   End
   Begin VB.Label Label1 
      Caption         =   "To export, select the export format desired from the list below and then press the OK button."
      Height          =   588
      Left            =   408
      TabIndex        =   3
      Top             =   204
      Width           =   3900
   End
End
Attribute VB_Name = "frmExportDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private mOk As Boolean
Private mIndex As Integer

Function ShowExport(Owner As Form) As Integer
  Me.Show vbModal, Owner
  ShowExport = mIndex
End Function

Private Sub CancelButton_Click()
  Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , hWnd
  End If
End Sub

Private Sub Form_Load()
  cboExport.ListIndex = 0
  mIndex = -1
End Sub

Private Sub OKButton_Click()
  mIndex = cboExport.ListIndex
  Unload Me
End Sub
