VERSION 5.00
Begin VB.Form frmColumnSettingsAdvancedDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Advanced Column Settings"
   ClientHeight    =   2550
   ClientLeft      =   30
   ClientTop       =   315
   ClientWidth     =   5025
   Icon            =   "frmColumnSettingsAdvancedDialog.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   5025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   3684
      TabIndex        =   3
      Top             =   2124
      Width           =   1200
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh &Columns"
      Height          =   336
      Left            =   3348
      TabIndex        =   4
      Top             =   648
      Width           =   1536
   End
   Begin VB.CommandButton cmdDefault 
      Caption         =   "Restore &Defaults"
      Height          =   336
      Left            =   3360
      TabIndex        =   2
      Top             =   156
      Width           =   1524
   End
   Begin VB.CheckBox chkDefault 
      Caption         =   "&Save settings as default"
      Height          =   420
      Left            =   228
      TabIndex        =   5
      Top             =   1476
      Width           =   2436
   End
   Begin VB.Frame fraLine 
      Height          =   36
      Left            =   132
      TabIndex        =   6
      Top             =   1980
      Width           =   4740
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   2376
      TabIndex        =   1
      Top             =   2124
      Width           =   1200
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   1056
      TabIndex        =   0
      Top             =   2124
      Width           =   1200
   End
   Begin VB.Label lblInstructions 
      AutoSize        =   -1  'True
      Caption         =   "Refresh the column list:"
      Height          =   192
      Index           =   1
      Left            =   276
      TabIndex        =   9
      Top             =   696
      Width           =   1632
   End
   Begin VB.Label lblInstructions 
      AutoSize        =   -1  'True
      Caption         =   "Restore the default settings:"
      Height          =   192
      Index           =   2
      Left            =   252
      TabIndex        =   8
      Top             =   216
      Width           =   1968
   End
   Begin VB.Label lblInstructions 
      AutoSize        =   -1  'True
      Caption         =   "Make the current column settings the default available for all users:"
      Height          =   192
      Index           =   0
      Left            =   252
      TabIndex        =   7
      Top             =   1248
      Width           =   4716
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmColumnSettingsAdvancedDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Ok As Boolean
Public ColumnSettingsDialog As frmColumnSettingsDialog

Private Sub cmdCancel_Click()
  Hide
End Sub

Private Sub cmdDefault_Click()
  ColumnSettingsDialog.RestoreDefault
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp HelpName.ADVANCEDCOLUMNSETTINGS_TOPIC, hWnd
End Sub

Private Sub cmdOk_Click()
  Ok = True
  Hide
End Sub

Private Sub cmdRefresh_Click()
  ColumnSettingsDialog.ColumnRefresh
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

