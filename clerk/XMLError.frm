VERSION 5.00
Begin VB.Form frmXMLErrorDialog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "XML Error"
   ClientHeight    =   5052
   ClientLeft      =   36
   ClientTop       =   312
   ClientWidth     =   9732
   Icon            =   "XMLError.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5052
   ScaleWidth      =   9732
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   336
      Left            =   8700
      TabIndex        =   7
      Top             =   4656
      Width           =   996
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   336
      Left            =   7620
      TabIndex        =   6
      Top             =   4656
      Width           =   996
   End
   Begin VB.Label lblStackTrace 
      Caption         =   "lblStackTrace"
      Height          =   3612
      Left            =   1092
      TabIndex        =   5
      Top             =   948
      UseMnemonic     =   0   'False
      Width           =   8640
   End
   Begin VB.Label lblMessage 
      AutoSize        =   -1  'True
      Caption         =   "lblMessage"
      Height          =   192
      Left            =   1092
      TabIndex        =   4
      Top             =   384
      UseMnemonic     =   0   'False
      Width           =   8640
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblClassName 
      AutoSize        =   -1  'True
      Caption         =   "lblClassName"
      Height          =   192
      Left            =   1092
      TabIndex        =   3
      Top             =   72
      UseMnemonic     =   0   'False
      Width           =   1020
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "stackTrace:"
      Height          =   192
      Left            =   108
      TabIndex        =   2
      Top             =   936
      Width           =   852
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "message:"
      Height          =   192
      Left            =   108
      TabIndex        =   1
      Top             =   384
      Width           =   720
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "className:"
      Height          =   192
      Left            =   108
      TabIndex        =   0
      Top             =   72
      Width           =   864
   End
End
Attribute VB_Name = "frmXMLErrorDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public isCanceled As Boolean


Private Sub cmdOK_Click()
  Unload Me
End Sub

Private Sub Command1_Click()
  isCanceled = True
  Me.Hide
  
End Sub

