VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EventService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.60.00 (jjm) 2004 03 29

'ver 2.00.0303 WILMIC
Option Explicit

Private Const ServiceName = "EventService"

Private mClient As MSSOAPLib30.SoapClient30
Private mEventTypeParameter As Dataset
Private mEvent As Dataset
Private mAttribute As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mEvent = NewGenericDataset(DatabaseName.EVENT_VIEW)
  Set mAttribute = NewGenericDataset(DatabaseName.EVENTATTRIBUTE_TABLE)

On Error GoTo Exception
  
  Set mClient = New SoapClient30
  
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Sub

'
' Interface to com.lawworkgroup.component.event.server.EventService
'

'
' Create an event.  Used by send to folder.
'

Public Function Create(eventTypeId As String, _
                       EventTypeRuleId As String, _
                       FolderId As String, _
                       ParentEventIds() As String, _
                       EventNames() As String) As Variant
On Error GoTo ErrorHandler
  
  Create = mClient.Create(eventTypeId, _
                          EventTypeRuleId, _
                          FolderId, _
                          ParentEventIds, _
                          EventNames)
                          
  Exit Function

ErrorHandler:
  'MsgBox "Event Service Error: " & Err.Description, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
End Function
'
' Create an Event.
'


Public Function CreateEvent( _
    eventTypeId As String, _
    EventName As String, _
    EventDesc As String, _
    eventNote As String, _
    EventTypeRuleId As String, _
    FolderId As String, _
    dueDate As Date, _
    dueUserId As String, _
    EventReferenceId As String, _
    ByRef AttributeNames() As String, _
    ByRef AttributeValues() As String) As String
    
On Error GoTo ErrorHandler
    CreateEvent = mClient.CreateEvent( _
      eventTypeId, _
      EventName, _
      EventDesc, _
      eventNote, _
      EventTypeRuleId, _
      FolderId, _
      dueDate, _
      dueUserId, _
      EventReferenceId, _
      AttributeNames, _
      AttributeValues)
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox mServiceName & " Fault: " & mClient.FaultString, vbExclamation
End Function


'
' Set an event as completed.  Let the server determine the
' username and time.
'
Public Function SetComplete(Handles() As String, _
    ParameterNames() As String, ParameterValues() As String) As Boolean
  
On Error GoTo ErrorHandler
  
  Dim Handle(0) As String
  Dim I As Integer
  
  For I = 0 To SafeUBound(Handles)
    Handle(0) = Handles(I)
    Call mClient.SetComplete(Handle, ParameterNames, ParameterValues)
  Next I
  SetComplete = True
  
  Exit Function

ErrorHandler:
 'ToDo: Create a more readable message
'  MsgBox Err.Description & _
'      " (EventService.SetComplete[Handles=" & Join(Handles, " ") & _
'      ",ParameterNames=" & Join(ParameterNames, " ") & _
'      ",ParamterValues=" & Join(ParameterValues, " ") & "])", _
'      vbExclamation
  
  'ver 2.60.00 (jjm) 2004 03 29
  ErrorUtil.DisplaySoapError Err, mClient
End Function

'
' Move event to a different folder.
'
Public Function SetFolder(Handle As String, FolderId As String) As Boolean
  
On Error GoTo Exception
  
  Call mClient.SetFolder(Handle, FolderId)
  SetFolder = True
  Exit Function

Exception:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Event Set Folder Service Error: " & Err.Description, vbExclamation
End Function


'
' Move event to a different folder.
'
Public Function SetNote(Handle As String, Note As String) As Boolean
  
On Error GoTo Exception
  
  Call mClient.SetNote(Handle, Note)
  SetNote = True
  Exit Function

Exception:
  'MsgBox "Event Set Note Service Error: " & Err.Description, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
End Function


Public Function GetNote(Handle As String) As String
  Dim Row As Collection
  Set Row = mEvent.DataProvider.Find(DatabaseName.EVENT_KEY, Handle, DatabaseName.EVENTNOTE_FIELD)
  If Not Row Is Nothing Then
    GetNote = Row(DatabaseName.EVENTNOTE_FIELD)
  End If
End Function

'
' Set an event as completed.  Let the server determine the
' username and time.  Must use wrapper functions below
'
Public Function SetVoid(Handles() As String, _
    ParameterNames() As String, ParameterValues() As String) As Boolean
    
On Error GoTo ErrorHandler
  
  Dim Handle(0) As String
  Dim I As Integer
  
  For I = 0 To UBound(Handles)
    Handle(0) = Handles(I)
    Call mClient.SetVoid(Handle, ParameterNames, ParameterValues)
  Next I
  SetVoid = True
  
  Exit Function

ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Event Void Service Error: " & Err.Description, vbExclamation
End Function

'ver 2.72.00 (jjm) 2005 04 21
'
' Set an event as pending.  Let the server determine the
' username and time.  Must use wrapper functions below
'
Public Function SetPending(Handles() As String, _
    ParameterNames() As String, ParameterValues() As String) As Boolean
    
On Error GoTo ErrorHandler
  
  Dim Handle(0) As String
  Dim I As Integer
  
  For I = 0 To UBound(Handles)
    Handle(0) = Handles(I)
    Call mClient.SetPending(Handle, ParameterNames, ParameterValues)
  Next I
  SetPending = True
  
  Exit Function

ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Event Void Service Error: " & Err.Description, vbExclamation
End Function


'
' Internal Helpers
'

'
' Set items in list view as complete.
'
Public Sub SetViewWorkflow(ActionType As Integer, View As ListView, Index As String, _
    ParameterNames() As String, ParameterValues() As String)
    
  Dim Handles() As String
  Handles = ListItemArray(View, Index)
  Select Case ActionType
    Case lawActionComplete
      SetComplete Handles, ParameterNames, ParameterValues
    Case lawActionVoid
      SetVoid Handles, ParameterNames, ParameterValues
  End Select
End Sub

Public Sub MoveToFolder(View As ListView, FolderId As String)
  With View
    Dim Count, I As Integer
    Count = .ListItems.Count
    For I = 1 To Count
      If .ListItems(I).Selected Then
        SetFolder ListItemText(.ListItems(I), DatabaseName.EVENT_KEY), FolderId
      End If
    Next I
  End With
End Sub

Public Function ShowMove(EventHandle As String) As String
  If EventHandle = vbNullString Then
    MsgBox "Can not move because there is no associated event."
    Exit Function
  End If
  Dim fForm As New frmFolderBrowseDialog, Key As String
  Key = Find(EventHandle, DatabaseName.FOLDER_KEY)(DatabaseName.FOLDER_KEY)
  If fForm.ShowMove(Key) Then
    SetFolder EventHandle, fForm.FolderKey
    ShowMove = fForm.FolderKey
  End If
  Unload fForm
End Function

Public Sub PasteToFolder(FolderId As String)
  Dim I As Integer
  For I = 1 To EventBuffer.Count
    SetFolder EventBuffer.item(I), FolderId
  Next I
End Sub

Public Function GetAttribute(Handle As String, Name As String) As String
  mAttribute.DataProvider.filter = "eventId = '" & Handle & "'  and attributeId = '" & Name & "'"
  mAttribute.DataProvider.Column = "attributeValue"
  mAttribute.DataProvider.OpenRecordset
  Dim Result As Collection
  Set Result = mAttribute.DataProvider.Collection
  If Not Result Is Nothing Then
    GetAttribute = Result(1)("attributeValue")
  End If
End Function

Public Function GetAttributes(Handle As String) As Collection
  mAttribute.DataProvider.filter = "eventId = '" & Handle & "'"
  mAttribute.DataProvider.Column = "attributeId, attributeValue"
  mAttribute.DataProvider.Sort = vbNullString
  mAttribute.DataProvider.OpenRecordset
  Dim Rows As Collection, Row As Collection, Attributes As New Collection
  Set Rows = mAttribute.DataProvider.Collection
  If Rows Is Nothing Then
    Exit Function
  End If
  For Each Row In Rows
    Attributes.Add Row("attributeValue"), Row("attributeId")
  Next Row
  Set GetAttributes = Attributes
End Function

Public Sub AddAttributes(Handle As String, Fields As Collection)
  mAttribute.DataProvider.filter = "eventId = '" & Handle & "'"
  mAttribute.DataProvider.Column = "attributeId, attributeValue"
  mAttribute.DataProvider.OpenRecordset
  
  Dim Result As Collection, Row As Collection
  Set Result = mAttribute.DataProvider.Collection
  If Result Is Nothing Then
    Exit Sub
  End If
  
  For Each Row In Result
    Fields.Add CreateDataField(DatabaseName.EVENT_TABLE, Row("attributeId"), Row("attributeValue")), Row("attributeId")
  Next Row
End Sub

Public Function Find(Handle As String, Optional Column As String) As Collection
  Dim Fields As Collection
  Set Fields = mEvent.DataProvider.Find(DatabaseName.EVENT_KEY, Handle, Column)
  AddAttributes Handle, Fields
  Set Find = Fields
End Function


'
' Set a reference for the event.
'

Public Function SetReference(EventHandle As String, ReferenceEventHandle As String) As Boolean
On Error GoTo Exception
  
  Call mClient.SetReference(EventHandle, ReferenceEventHandle)
  SetReference = True
  Exit Function

Exception:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Event Set Reference Service Error: " & Err.Description, vbExclamation
  Err.Clear
End Function


Public Function SetAttribute(Handle As String, Name As String, Value As String) As Boolean
On Error GoTo Exception
  
  Call mClient.SetAttribute(Handle, Name, Value)
  SetAttribute = True
  Exit Function

Exception:
  'MsgBox "Event Set Attribute Service Error: " & Err.Description, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
End Function


Public Function IsEventTypeInGroup(eventTypeId As String, eventGroupId As String)
On Error GoTo Exception
  
  IsEventTypeInGroup = mClient.IsEventTypeInGroup(eventTypeId, eventGroupId)
  Exit Function

Exception:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Event Type In Group Service Error: " & Err.Description, vbExclamation
End Function


'
' Write an Event
'
Public Function WriteEvent( _
    Handle As String, _
    eventTypeId As String, _
    EventName As String, _
    EventDesc As String, _
    eventNote As String, _
    EventTypeRuleId As String, _
    FolderId As String, _
    dueDate As Date, _
    dueUserId As String, _
    AttributeNames() As String, _
    AttributeValues() As String) As Boolean
    
On Error GoTo ErrorHandler
    Call mClient.WriteEvent( _
      Handle, _
      eventTypeId, _
      EventName, _
      EventDesc, _
      eventNote, _
      EventTypeRuleId, _
      FolderId, _
      dueDate, _
      dueUserId, _
      AttributeNames, _
      AttributeValues)
    WriteEvent = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  WriteEvent = False
End Function

'ver 2.71.00 (jjm) 2005 02 02
Public Function setDueDate(eventId As String, dueDate As Date) As Boolean
  mClient.setDueDate eventId, dueDate
  setDueDate = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  setDueDate = False

End Function

Public Function SetEvent(Handle As String, EventTypeHandle As String) As Boolean
  mClient.setEventType Handle, EventTypeHandle
  SetEvent = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  SetEvent = False
End Function

'
' Document Event
'
Public Sub DocumentMerge(Handles() As String, Optional ShowCompleteDialog As Boolean)
  Dim Params As String
  Params = BuildParamList(Handles)
  
  If Params = vbNullString Then
    Exit Sub
  End If


  Dim xmlHttp As xmlHttp
  Set xmlHttp = HttpPost(HttpServer & "servlet/eventprint.custom" & "?content=text/plain", Params)
  'MsgBox xmlHttp.responseText
  WriteTextFile xmlHttp.responseText, GetRealPath(IniMain.ReadFile(UrlName.MISC_INI, "mail.txt"))
  
  ShellExecute 0&, vbNullString, GetRealPath(IniMain.ReadFile(UrlName.MISC_INI, "wpmrg.bat")), vbNullString, _
      vbNullString, SW_SHOWNORMAL

  AutoComplete Handles, ShowCompleteDialog

End Sub


'
' Document Event
'
Public Sub WebMergeByView(View As ListView)
  Dim Handles() As String
  Handles = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
  WebMerge Handles, True
End Sub

Public Sub WebMerge(Handles() As String, Optional ShowCompleteDialog As Boolean)
  Dim pointer As New WaitPointer
  pointer.Default

  ' Print Envelopes
  Dim I As Integer
  For I = 0 To SafeUBound(Handles)
    Factory.EventRecipientService.PrintEnvelope Handles(I)
  Next I
  
  Set pointer = Nothing

  ' Merge Documents
  Dim Params As String
  Params = BuildParamList(Handles)
  
  If Params = vbNullString Then
    Exit Sub
  End If
  
  Browse HttpServer & WebUtil & "merge?" & Params

  'jjm 7/18/2011 - Per Sean's specs, disable the autocomplete
  'AutoComplete Handles, ShowCompleteDialog

End Sub

Private Sub AutoComplete(Handles() As String, Optional ShowCompleteDialog As Boolean)

  'ver 2.00.0303
  If ShowCompleteDialog Then
    Dim MergeDialog As New frmMergeCompleted
    MergeDialog.Show vbModal
    If MergeDialog.Ok Then
      
      If MergeDialog.chkCompleteFiles.Value = vbChecked Then
        'Auto-complete
        Dim ParameterNames() As String
        Dim ParameterValues() As String
'        Factory.FolderParameterService.ParameterLoad _
'          Factory.EventService.Find(Handles(0), DatabaseName.FOLDER_KEY)(DatabaseName.FOLDER_KEY), _
'          lawActionComplete, ParameterNames, ParameterValues, True
        'Automatically Complete the documents
        Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
      End If
      
      If MergeDialog.chkUpdateFiles.Value = vbUnchecked Then
        'unload dialog and exit sub avoiding file modifications
        Unload MergeDialog
        Exit Sub
      End If
      
      'Unload the dialog and continue to use existing modification code
      Unload MergeDialog
    End If
  End If
  
  ' Update fileName attribute with saveFileName for modifieds
  Dim I As Integer
  For I = 0 To SafeUBound(Handles)
    If GetBoolean(GetAttribute(Handles(I), "isModify")) Then
      SetAttribute Handles(I), "fileName", GetAttribute(Handles(I), "saveFileName")
    End If
  Next I

End Sub

Private Function BuildParamList(Handles() As String) As String
  Dim Params As String, I As Integer
  For I = 0 To SafeUBound(Handles)
    Params = Params & IIf(Params = "", "", "&") & "key=" & EncodeURL(Handles(I))
  Next I
  
  BuildParamList = Params

End Function


Public Function SetPerson(EventAttributeHandle As String, PersonHandle As String) As Boolean
    
On Error GoTo ErrorHandler
  
  mClient.SetPerson EventAttributeHandle, PersonHandle
  SetPerson = True
  Exit Function
  
ErrorHandler:
  'MsgBox "Fault: " & mClient.FaultString, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
  SetPerson = False
End Function



'
' Create an EventDocument. Returns eventId
'
Public Function CreateEventDocument( _
    EventReferenceId As String, eventTypeId As String, _
    EventName As String, EventDesc As String, _
    eventNote As String, _
    EventTypeRuleId As String, _
    FolderId As String, _
    recipientTypeId() As String, _
    recipientSeq() As Integer, contactId() As String, _
    PersonId() As String, LocationId() As String, _
    RecipientName() As String, RecipientSalutation() As String, _
    CompanyName() As String, AddressLine1() As String, _
    AddressLine2() As String, AddressLine3() As String, _
    City() As String, State() As String, _
    Zip() As String, Country() As String, _
    CarrierRoute() As String, DeliveryPointCode() As String, _
    DeliveryAddress() As String, _
    isEnvelope() As Boolean, _
    AttributeNames() As String, _
    AttributeValues() As String) As String
    
On Error GoTo ErrorHandler
    CreateEventDocument = mClient.CreateEventDocument( _
          EventReferenceId, _
          eventTypeId, _
          EventName, _
          EventDesc, _
          eventNote, _
          EventTypeRuleId, _
          FolderId, _
          recipientTypeId, _
          recipientSeq, contactId, _
          PersonId, LocationId, _
          RecipientName, RecipientSalutation, _
          CompanyName, AddressLine1, _
          AddressLine2, AddressLine3, _
          City, State, _
          Zip, Country, _
          CarrierRoute, DeliveryPointCode, _
          DeliveryAddress, _
          isEnvelope, _
          AttributeNames, _
          AttributeValues)
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
End Function




'
' Write an EventDocument
'
Public Function WriteEventDocument( _
    EventHandle As String, eventTypeId As String, _
    EventName As String, EventDesc As String, _
    eventNote As String, _
    EventTypeRuleId As String, _
    FolderId As String, _
    recipientTypeId() As String, _
    recipientSeq() As Integer, contactId() As String, _
    PersonId() As String, LocationId() As String, _
    RecipientName() As String, RecipientSalutation() As String, _
    CompanyName() As String, AddressLine1() As String, _
    AddressLine2() As String, AddressLine3() As String, _
    City() As String, State() As String, _
    Zip() As String, Country() As String, _
    CarrierRoute() As String, DeliveryPointCode() As String, _
    DeliveryAddress() As String, _
    isEnvelope() As Boolean, _
    AttributeNames() As String, _
    AttributeValues() As String) As Boolean
    
On Error GoTo ErrorHandler
    Call mClient.WriteEventDocument( _
          EventHandle, _
          eventTypeId, _
          EventName, _
          EventDesc, _
          eventNote, _
          EventTypeRuleId, _
          FolderId, _
          recipientTypeId, _
          recipientSeq, contactId, _
          PersonId, LocationId, _
          RecipientName, RecipientSalutation, _
          CompanyName, AddressLine1, _
          AddressLine2, AddressLine3, _
          City, State, _
          Zip, Country, _
          CarrierRoute, DeliveryPointCode, _
          DeliveryAddress, _
          isEnvelope, _
          AttributeNames, _
          AttributeValues)
    WriteEventDocument = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  WriteEventDocument = False
End Function

