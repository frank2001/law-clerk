VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LawyerDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
Option Explicit
Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private ListIndex As Integer
Private Words As String
Public Sub LawyerSearchFilter(frm As frmGenericSearch, newFilter As Filter)
  
  If frm.chkYearsInPractice = vbChecked Then
    newFilter.Add """inPracticeDuration"" " & GetOperator(frm.cboYearsOperator.ListIndex) & " '" & frm.txtYearsInPractice & "'"
  End If
  
  If frm.chkRetroactiveDate = vbChecked Then
    newFilter.Add """retroactiveDate"" IS NOT NULL "
  End If
  
  If frm.chkCounty = vbChecked Then
   newFilter.Add """personId"" in " & _
      "(select ""personId"" from ""Person"" where ""locationId"" in " & _
      "(select ""locationId"" from ""Location"" where " & LawDialect.UpperCase("county") & " LIKE '" & UCase(frm.txtCounty) & "%'))"
  End If

End Sub

Private Sub Class_Initialize()
  ListIndex = UrlName.LAWYERSEARCH_LAWYERLOCATOR_INDEX
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.LAWYER_TABLE ' "Lawyer"
  'mDataProvider.Column = """lawyerId"", ""lawyerName"", ""barAdmittedDate"", ""inPracticeTime"", ""lawyerRetroactiveDate"", ""lawyerCode"", ""personId"""
'  mDataProvider.Sort = """lawyerId"""
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in latest firm identifier field only"
  frm.cboIn.AddItem "in lawyer identifier field only"
  frm.cboIn.AddItem "in lawyer entity locator field only"
  frm.cboIn.AddItem "in lawyer entity name field only"
  frm.cboIn.AddItem "in state bar number field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, UrlName.LAWYERSEARCH_LAWYERLOCATOR_INDEX)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case UrlName.LAWYERSEARCH_LASTESTFIRMID
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LATESTFIRMID_FIELD, Words)
      Case UrlName.LAWYERSEARCH_LAWYERLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LAWYERENTITYLOCATOR_FIELD, Words)
      Case UrlName.LAWYERSEARCH_LAWYERNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.LAWYERENTITYNAME_FIELD, Words)
      Case UrlName.LAWYERSEARCH_LAWYERID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LAWYER_KEY, Words)
      Case UrlName.LAWYERSEARCH_STATEBARNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.STATEBARNUMBER_FIELD, Words)
    End Select
  End If
  
  If TypeOf frm Is frmGenericSearch Then
    LawyerSearchFilter frm, newFilter
  End If
  
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

