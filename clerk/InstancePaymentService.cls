VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InstancePaymentService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "InstancePaymentService"
Private mDataSet As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = DataUtil.NewGenericDataset(DatabaseName.INSTANCEPAYMENT_TABLE)
  
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub


Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.INSTANCEPAYMENT_KEY, Handle, Column)
End Function


'
' Interface to server
'

'
' Create.
'
Public Function CreateInstancePayment( _
    instanceId As String, _
    eventTypeId As String, _
    paymentDate As Date, _
    referenceNumber As String, _
    checkNum As String, _
    payee As String, _
    paymentGeneralLegal As Double, _
    eventNote As String) As Variant
    
On Error GoTo Exception
  
  CreateInstancePayment = Client.Create( _
      instanceId, _
      eventTypeId, _
      paymentDate, _
      referenceNumber, _
      checkNum, _
      payee, _
      paymentGeneralLegal, _
      eventNote)
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Function


'
' Write an InstancePayment
'
Public Function WriteInstancePayment( _
    instancePaymentId As String, _
    eventTypeId As String, _
    paymentDate As Date, _
    referenceNumber As String, _
    checkNum As String, _
    payee As String, _
    paymentGeneralLegal As Double, _
    eventNote As String) As Boolean
    
On Error GoTo Exception
  
  Call Client.Write( _
      instancePaymentId, _
      eventTypeId, _
      paymentDate, _
      referenceNumber, _
      checkNum, _
      payee, _
      paymentGeneralLegal, _
      eventNote)
  WriteInstancePayment = True
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Function


