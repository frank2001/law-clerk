Attribute VB_Name = "MainModule"
'previous ver 2.70 (jjm) 2004 09 14
'previous ver 2.20.0617 (jjm)
'previous ver 2.20.0613 (jjm)
'previous ver 2.20.0611 (jjm)


'
' Main Module
'

' The main module controls the loading of the project.  The
' main form is frmMain.  This form is one of three major types.
' See the frmMain comments for more information.

' See:  Sub Main() for more information on the loading of the project and
' configuration of project for testing.


' References

' (Ref:Add Detail)
' Search the project for this reference to see all the places
' code was changed to add a table in a tab (Detail Table) to
' an "Open" form.


' Next Version
'
' -When determining the search set (data set) for a folder, it should be
' done based upon a search set type that is stored in the folder
' properties.  For example, we should be able to switch between Application
' and ApplicationView as the source for the Application folder
' without any changes to code.
'
'
'


'
' Misc. VB Questions (Anyone know?)
'
' -How do you tell if a form has been shown modally?
'


Option Explicit

'Factory for loading middle tier services.
Public Factory As ServiceFactory

'Session information set by login.
Public HttpServer As String
Public WebService As String
Public WebUserInterface As String
Public WebComponent As String
Public WebUtil As String

Public urlOnlineApplicationPDF As String


Public LawSession As Session
Public LawDialect As XQueryDialect
Public LawSQL As SQLDialect
Public UserName As String


'Store value of ini file
Public IniMain As New IniFile
Public IniForm As New IniFile
Public IniFolder As New IniFile
Public IniSearch As New IniFile

'Private IniViewPath As String
Private CONFDEFAULTPATH As String
Private CONFPERSONALPATH As String

'Folder paths
Public ImageFolderPath As String

Public Const GW_HWNDPREV = 3

Declare Function OpenIcon Lib "user32" (ByVal hWnd As Long) As Long
Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
        (ByVal lpClassName As String, ByVal lpWindowName As String) _
         As Long
Declare Function GetWindow Lib "user32" _
        (ByVal hWnd As Long, ByVal wCmd As Long) As Long
Declare Function SetForegroundWindow Lib "user32" _
        (ByVal hWnd As Long) As Long

Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)


' Global constants for the frmMain form types.
Global Const ftView = 1
Global Const ftAdvancedSearch = 2
Global Const ftSelect = 3

' Constants for form interface messages
Global Const fimNone = 0
Global Const fimSelect = 1
Global Const fimNew = 2
Global Const fimSave = 3

' Constants for the Law Area Item operators
Global Const isGreaterThan = 0
Global Const isLessThan = 1
Global Const isEqualTo = 2

' Constant for versions
Global Const LAWVersion = "Version 5.4 Beta 1"
Public MalcolmVersion As String


' Constant for pixel to inch
Global Const TwipsPerInch = 1440

' Contstants for printers
Public DefaultEnvelopePrinter As String

' Constants for Null's
Public vbNullDate As Date
Public vbNullStringArray() As String

' Support cut/paste throughout system
Public EventBufferFolder As String
Public EventBuffer As collection

Public LocaleShortDate As String
Public LocaleLongDate As String
Public LocaleTimeFormat As String

' Envelope Settings
Public LocaleEnvelopePaperSize As Integer
Public LocaleEnvelopeCurrentX As Double
Public LocaleEnvelopeCurrentY As Double
Public LocaleEnvelopeFont As New StdFont
Public LocaleEnvelopeForeColor As Long
Public LocaleEnvelopeBarCode As Integer

' Main Option Variables
Public LocaleEnterAsTab As Boolean
Public LocaleEnableWebContent As Boolean
Public LocaleTextSelectOnFocus As Boolean
Public LocaleItemsPerView As Integer
Public LocaleShortOrganization As String
Public LocaleLongOrganization As String
Public LocaleRememberLastSearch As Boolean
Public LocaleDefaultToFirmPolicyContact As Boolean
Public LocaleDebugAdvancedSearch  As Boolean

'Merge Settings
Public MergePath As String
Public MergePrefix As String
Public MergeSuffix As String
Public NarrativePath As String
Public NarrativePrefix As String
Public NarrativeSuffix As String

'The photo album to store all images
Public PicDisplay As New PictureDisplay

Public fSplash As frmSplash

'Tree variables
Public SystemPreferences_TreeName As String
Public SystemPreferences_RootName As String
Public SystemPreferences_RootKey As String
Public SystemFolders_TreeName As String
Public SystemFolders_RootName As String
Public SystemFolders_RootKey As String

'Constant for true boolean value in database. In the database
'true values are stored as the below string.
Public Const DB_TRUE_VALUE = "Yes"

' Random number to prevent http caching.
Public mCache As Long



'****************************
' Purpose:  Load the project
'****************************

Sub Main()
  If App.PrevInstance Then
    ActivatePrevInstance
  End If
  
  ' seed NoCache with random number
  Randomize
  mCache = Rnd() * 1000000000
  
  IniMain.FileName = App.Path & "\" & "law.ini"
  
  'IniViewPath = GetRealPath(IniMain.ReadFile(UrlName.MAIN_INI, "IniViewPath"))
  CONFDEFAULTPATH = GetRealPath(IniMain.ReadFile(UrlName.MAIN_INI, UrlName.CONFDEFAULTPATH_INI))
  CONFPERSONALPATH = GetRealPath(IniMain.ReadFile(UrlName.MAIN_INI, UrlName.CONFPERSONALPATH_INI))
  
  'Image folder path 'ver 2.30.0619
  ImageFolderPath = GetRealPath(IniMain.ReadFile(UrlName.MAIN_INI, UrlName.IMAGEFOLDERPATH_INI))
  If Right(ImageFolderPath, 1) <> "\" Then
    ImageFolderPath = ImageFolderPath & "\"
  End If
  
  ' Setup ini files.
  IniForm.FileName = GetRealPath(IniMain.ReadFile(UrlName.MAIN_INI, UrlName.INIFORM_INI))
  IniFolder.FileName = GetRealPath(IniMain.ReadFile(UrlName.MAIN_INI, UrlName.INIFOLDER_INI))
  IniSearch.FileName = GetRealPath(CONFPERSONALPATH + "\search.ini")
  
  LocaleShortDate = GetUserLocaleInfo(GetSystemDefaultLCID, LOCALE_SSHORTDATE)
  LocaleLongDate = GetUserLocaleInfo(GetSystemDefaultLCID, LOCALE_SLONGDATE)
  LocaleTimeFormat = GetUserLocaleInfo(GetSystemDefaultLCID, LOCALE_STIMEFORMAT)
    
  ' Convert time format to vb spec.
  If InStr(LocaleTimeFormat, "tt") Then
    LocaleTimeFormat = Left(LocaleTimeFormat, Len(LocaleTimeFormat) - 2) & "AMPM"
  End If
  
  'Get Main Settings
  LocaleEnterAsTab = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.ENTERASTAB_INI) = "True"
  LocaleEnableWebContent = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.ENABLEWEBCONTENT_INI)
  LocaleShortOrganization = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.ORGANIZATIONSHORT_INI)
  LocaleLongOrganization = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.ORGANIZATIONLONG_INI)
  LocaleTextSelectOnFocus = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.TEXTSELECTONFOCUS_INI) = "True"
  LocaleItemsPerView = GetInteger(IniMain.ReadFile(UrlName.MAIN_INI, UrlName.ITEMSPERVIEW_INI))
  LocaleRememberLastSearch = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.REMEMBERLASTSEARCH_INI) = "True"
  LocaleDefaultToFirmPolicyContact = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.DEFAULTTOFIRMPOLICYCONTACT_INI) = "True"
  LocaleDebugAdvancedSearch = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.DEBUGADVANCEDSEARCH_INI) = "True"
  
  'Get printer settings
  DefaultEnvelopePrinter = IniMain.ReadFile(UrlName.MISC_INI, UrlName.ENVELOPEPRINTER_INI)
  
  'Envelope Settings
  LocaleEnvelopePaperSize = GetInteger(IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.PAPERSIZE_INI))
  LocaleEnvelopeCurrentX = GetDouble(IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.CURRENTX_INI))
  LocaleEnvelopeCurrentY = GetDouble(IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.CURRENTY_INI))
  LocaleEnvelopeBarCode = GetInteger(IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.BARCODE_INI))
  LocaleEnvelopeForeColor = GetInteger(IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.FORECOLOR_INI))
  ' The font size cannot be equal to zero
  Dim FontSize As Double
  FontSize = GetDouble(IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.FONTSIZE_INI))
  LocaleEnvelopeFont.Size = IIf(FontSize = 0, 10, FontSize)
  LocaleEnvelopeFont.Name = IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.FONTNAME_INI)
  If LocaleEnvelopeFont.Name = vbNullString Then
    LocaleEnvelopeFont.Name = "Arial"
  End If
  LocaleEnvelopeFont.Bold = IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.FONTBOLD_INI) = "True"
  LocaleEnvelopeFont.Italic = IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.FONTITALIC_INI) = "True"
  LocaleEnvelopeFont.Underline = IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.FONTUNDERLINE_INI) = "True"
  LocaleEnvelopeFont.Strikethrough = IniMain.ReadFile(UrlName.ENVELOPE_INI, UrlName.FONTSTRIKETHROUGH_INI) = "True"
  
  
  'Get merge settings
  MergePath = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.MERGEPATH_INI)
  MergePrefix = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.MERGEPREFIX_INI)
  MergeSuffix = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.MERGESUFFIX_INI)
  NarrativePath = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVEPATH_INI)
  NarrativePrefix = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVEPREFIX_INI)
  NarrativeSuffix = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVESUFFIX_INI)
  
  
  '2.30.0825-B Setup Tree Variables
  Dim TempTest As String
  TempTest = IniFolder.ReadFile(UrlName.TREE_INI, UrlName.SYSTEMPREFERENCES_TREENAME_INI)
  SystemPreferences_TreeName = IIf(TempTest = vbNullString, "systempreferences", TempTest)
  TempTest = IniFolder.ReadFile(SystemPreferences_TreeName, "name") '"Preferences"
  SystemPreferences_RootName = IIf(TempTest = vbNullString, "Preferences", TempTest)
  TempTest = IniFolder.ReadFile(SystemPreferences_TreeName, "handle") '"preferencesroot"
  SystemPreferences_RootKey = IIf(TempTest = vbNullString, "systempreferences", TempTest)

  TempTest = IniFolder.ReadFile(UrlName.MAIN_INI, UrlName.SYSTEMFOLDERS_TREENAME_INI)
  SystemFolders_TreeName = IIf(TempTest = vbNullString, "systemfolders", TempTest)
  TempTest = IniFolder.ReadFile(SystemFolders_TreeName, "name") '"LAW Explorer"
  SystemFolders_RootName = IIf(TempTest = vbNullString, "LAW Explorer", TempTest)
  TempTest = IniFolder.ReadFile(SystemFolders_TreeName, "handle") '"foldersroot"
  SystemFolders_RootKey = IIf(TempTest = vbNullString, "systemfolders", TempTest)
 
 
  ' get web settings from law.ini
  ' NEEDS TO BE IMPLEMENTED
  ' 2.30.1203 (jjm) in template code
  WebService = IniMain.ReadFile(UrlName.MAIN_INI, "WebService") & "/"            '"component/"
  WebUserInterface = IniMain.ReadFile(UrlName.MAIN_INI, "WebUserInterface") & "/"   '"ui/"
  WebComponent = IniMain.ReadFile(UrlName.MAIN_INI, "WebComponent") & "/"       'component/"
  WebUtil = IniMain.ReadFile(UrlName.MAIN_INI, "WebUtil") & "/"           '"servlet/"
  
  urlOnlineApplicationPDF = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.ONLINE_APP_PDF_INI)
  
  
    
  Set Factory = New ServiceFactory
   ' ***********************************************
   ' Uncomment the appropriate session.
   
   'MemorySession is used for testing.
   'Set LawSession = New MemorySession
   
   'ADOSession connects directly to Oracle via ADO/ODBC
   'Set LawSession = New ADOSession
   
   'XMLSession connects to middle tier.
   Set LawSession = New XMLSession
   
   
  ' **********************************************
   
   
  ' **********************************************
  ' Uncomment a form to load
   
 
  Set fSplash = New frmSplash
  fSplash.Show
  Login
  fSplash.Refresh
  

  ' Dim fForm As New frmOpenDialog
  ' fForm.Show vbModal
  ' Exit Sub
  
  'Get malcolm version
  Dim versionHttp As xmlHttp
  Set versionHttp = HttpGet(HttpServer & "version")
  MalcolmVersion = versionHttp.responseText
  
  ' Load up main form
  Dim fMain As New frmMain, FolderId As String
  Load fMain
  FolderId = IniForm.ReadFile(fMain.Name, UrlName.FOLDERKEY_INI)
  'a folderId must be set
  If FolderId = vbNullString Then
    FolderId = fMain.tvFolders.Nodes(1).Key
  End If
  fMain.FolderKey = FolderId
  If Not fMain.tvFolders.SelectedItem Is Nothing Then
    fMain.tvFolders.SelectedItem.Expanded = IIf(fMain.tvFolders.SelectedItem.children > 0, True, False)
  End If
  fMain.Show
  
  'PersonServiceTest
  'ApplicationServiceTest
  
'   Dim fForm As New frmEventDialog
'   fForm.ReferenceLoad DatabaseName.APPLICATION_TABLE, "AP10003826", "Complete"
'   fForm.Show vbModal
'   Unload fForm

'    Dim fForm As New frmClaimPaymentEdit
'    fForm.ShowNew "00-0003"


'   CreateSearchForm().Show

   
'   Dim fForm As New frmApplicationLimitEdit
'   fForm.ShowNew "TEST"
'   fForm.Show
   
   'Dim Complete As New frmCompleteEvent
   'Complete.FolderId = "Underwriting_Applications"
   'Complete.Show
   
   'frmFolderDialog.Show
   
   'frmApplicationCreate.Show
   'frmFront.Show
   'frmXMLListView.Show
   'frmColumnSelect.Show
   'frmGLTransactionEdit.Show
   'CreateMainForm(ftSelect, "Lawyer").Show
   'frmLawyer.Show
   'CreateMainForm(ftAdvancedSearch, "Firm").Show
   'frmUnderwritingContact.Show
   'frmFirmInsurerEdit.Show
   
   'frmApplicationOpen.Show
   'frmApplicationLimitEdit.Show
   'frmApplicationLawAreaEdit.Show
End Sub


Public Sub AddNameValue(Name As String, Value As Variant, NameArr() As String, ValueArr() As Variant)
  Dim Count As Integer
  Count = SafeUBound(NameArr) + 1
  ReDim Preserve NameArr(Count)
  ReDim Preserve ValueArr(Count)
  NameArr(Count) = Name
  ValueArr(Count) = Value
End Sub

Public Sub AddParameter(Name As String, Value As String, ParameterNames() As String, ParameterValues() As String)
  Dim Count As Integer
  Count = SafeUBound(ParameterNames) + 1
  ReDim Preserve ParameterNames(Count)
  ReDim Preserve ParameterValues(Count)
  ParameterNames(Count) = Name
  ParameterValues(Count) = Value
End Sub


Public Sub ClearDataField(Datafields As Variant, Optional DataMember As String)
  Dim Control As Variant
  For Each Control In Datafields
    If DataMember = vbNullString Or Control.DataMember = DataMember Then
      If TypeOf Control Is TextBox Then
        Control.Text = vbNullString
      ElseIf TypeOf Control Is Label Then
        Control.Caption = vbNullString
      ElseIf TypeOf Control Is MaskEdBox Then
        Dim msk As MaskEdBox
        Set msk = Control
        MaskEditClear msk
      ElseIf TypeOf Control Is ComboBox Then
        Dim cbx As ComboBox
        Set cbx = Control
        With cbx
          If .ListCount > 0 Then
            .ListIndex = 0
          End If
        End With
      ElseIf TypeOf Control Is DTPicker Then
        Dim dtpDate As DTPicker
        Set dtpDate = Control
        If dtpDate.CheckBox Then
          dtpDate.Value = Today
          dtpDate.Value = vbNull
        Else
          dtpDate.Value = vbNull
          dtpDate.Value = Today
        End If
      End If
    End If
  Next
End Sub


Public Function CollectionItem(Col As Variant, Index As String) As Variant
  On Error Resume Next
  If VarType(Col) = vbEmpty Then
  ElseIf TypeOf Col Is ListItem Then
    Dim ListItem As ListItem
    Set ListItem = Col
    CollectionItem = ListItemText(ListItem, Index)
  Else
    CollectionItem = Col(Index)
  End If
  Err.Clear
End Function

Public Sub SetFields(Controls As Variant, Optional Items As Variant, Optional DataMember As String)
  Dim item As Variant
  Dim Control As Variant
  For Each Control In Controls
    If ((DataMember <> "" And Control.DataMember <> DataMember) Or Control.DataField = "") Then
      GoTo Continue
    End If
    item = CollectionItem(Items, Control.DataField)

    If TypeOf Control Is Label Then
      Control.Caption = DisplayFormat(Control.DataField, CStr(item), Control.DataFormat.Format, LocaleShortDate)
    
    ElseIf TypeOf Control Is TextBox Then
      'Formating truncates text. Mantis #2379
      If Control.DataFormat.Format <> vbNullString Then
        Control.Text = Format(item, Control.DataFormat.Format)
      Else
        Control.Text = item
      End If
      
    '2.70 (jjm) 20040914
    ElseIf TypeOf Control Is RichTextBox Then
      Control.Text = Format(item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is DTPicker Then
      If item = vbNullString Then
        Control.CheckBox = True
      End If
      Control.Value = Format(item, Control.DataFormat.Format)
    
    ElseIf TypeOf Control Is ComboBox Then
      Dim cbx As ComboBox
      Set cbx = Control
      
      Util.SetComboValue cbx, item
      
    ElseIf TypeOf Control Is MaskEdBox Then
      Control.SelStart = 0
      Control.SelLength = 9999
      Control.SelText = Format(item, Control.DataFormat.Format)

    ElseIf TypeOf Control Is CheckBox Then 'Hack for old data
      Control.Value = IIf(GetBoolean(item) Or item = "1", 1, 0)
    End If
    
Continue:
  Next
End Sub

Public Sub MaskEditClear(Control As MaskEdBox)
   Control.SelStart = 0
   Control.SelLength = 9999
   Control.SelText = ""
End Sub

Public Sub AddMaskControl(MaskControl As Object, Items As collection, Optional Source As String)
  Dim Control As MaskEdBox, Text As String
  For Each Control In MaskControl
    If (Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source)) Then
      If Control.ClipMode = mskIncludeLiterals Then
        Text = Control.Text
      Else
        Text = Control.ClipText
      End If
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Text), Control.DataField
    End If
  Next
End Sub

Public Sub AddTextControl(TextControl As Variant, Items As collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In TextControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Text), Control.DataField
    End If
  Next
End Sub

Public Sub AddValueControl(ValueControl As Variant, Items As collection, Optional Source As String)
  Dim Control As Variant
  For Each Control In ValueControl
    If Not Control.DataField = "" And (Source = vbNullString Or Control.DataMember = Source) Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Value), Control.DataField
    End If
  Next
End Sub

Public Sub AddCaptionControl(CaptionControl As Variant, Items As collection)
  Dim Control As Variant
  For Each Control In CaptionControl
    If (Not Control.DataField = "") Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Caption), Control.DataField
    End If
  Next
End Sub

Public Sub ReadValueControl(TextControl As Variant, Items As collection)
  Dim Control As Variant
  For Each Control In TextControl
    If (Not Control.DataField = "") Then
      Items.Add CreateDataField(Control.DataMember, Control.DataField, Control.Value), Control.DataField
    End If
  Next
End Sub


Public Function ListKey(cData As collection) As String
  Dim List As String
  Dim item As Variant
  For Each item In cData
    If (List = "") Then
      List = item
    Else
      List = List + ", " + item
    End If
  Next
End Function

Public Function CreateDataField(Source As String, Name As String, Value As Variant) As DataField
  Set CreateDataField = New DataField
  CreateDataField.Source = Source
  CreateDataField.Name = Name
  CreateDataField.Value = Value
End Function

Private Sub ActivatePrevInstance()
  Dim OldTitle As String
  Dim PrevHndl As Long
  Dim Result As Long

  'Save the title of the application.
  OldTitle = App.Title

  'Rename the title of this application so FindWindow
  'will not find this application instance.
  App.Title = "unwanted instance"

  'Attempt to get window handle using VB4 class name.
  PrevHndl = FindWindow("ThunderRTMain", OldTitle)

  'Check for no success.
  If PrevHndl = 0 Then
     'Attempt to get window handle using VB5 class name.
     PrevHndl = FindWindow("ThunderRT5Main", OldTitle)
  End If

  'Check if found
  If PrevHndl = 0 Then
    'Attempt to get window handle using VB6 class name
    PrevHndl = FindWindow("ThunderRT6Main", OldTitle)
  End If

  'Check if found
  If PrevHndl = 0 Then
     'No previous instance found.
     Exit Sub
  End If

  'Get handle to previous window.
  PrevHndl = GetWindow(PrevHndl, GW_HWNDPREV)

  'Restore the program.
  Result = OpenIcon(PrevHndl)

  'Activate the application.
  Result = SetForegroundWindow(PrevHndl)

  'End the application.
  End
  
End Sub

 
Public Function HttpBody(item As ListItem, Columns As collection) As String
  Dim Column As Variant
  HttpBody = ""
  For Each Column In Columns
    HttpBody = IIf(HttpBody = "", "", "&") & Column.Name & "=" & EncodeURL(item.ListSubItems(Column.Value))
  Next Column
End Function


Public Sub Login()
  Dim fLogin As New frmLoginDialog
  Dim Authenticated As Boolean
   
  While Not Authenticated
    fLogin.Show vbModal
    If Not fLogin.Ok Then
        'Login Failed so exit app
       End
    End If
    Authenticated = LawSession.Authenticate(fLogin)
  Wend
   
  ' Generic now works with {fn xxx()} calls.
  Set LawSQL = New SQLDialect
  Set LawDialect = New XQueryDialect
  
  ' Set web info
  HttpServer = fLogin.txtServerAddress.Text
  
  Unload fLogin
   
   'MsgBox "Authenticated with session: " & LawSession.Session
   
   '
   ' Compare the time between client and middle tier and note if different
   '
   If Factory.TimeService.CompareTime(GetDate(Today), Format(Today, "yyyy-mm-dd")) <> 0 Then
     'compare time zones
     Dim ServerZone, LocalZone As String
     ServerZone = Factory.TimeService.GetServerTimezone
     'LocalZone = TimeUtil.GetTimeZoneName 'old
     'Debug.Print TimeUtil2.GetCurrentTimeZone 'new
     LocalZone = TimeUtil2.GetCurrentTimeZone
      

     If ServerZone = LocalZone Then
       MsgBox "The server time is different than the time settings on your computer. This may cause data to become corrupt it you continue. Please contact your network administrator.", vbCritical, "Warning: Time Difference"
     Else
       MsgBox "The time-zone of the server is different than the time-zone set on your computer." & vbCrLf & _
              "This will create corrupt data if any changes are saved." & vbCrLf & vbCrLf & _
              "Server Time-Zone: " & vbTab & ServerZone & vbCrLf & _
              "Your Time-Zone: " & vbTab & LocalZone, vbCritical, "Warning: Time Difference"
     End If
   End If
   
End Sub


Public Sub LogOff(OldMain As frmMain)
  
  'Unload Main first. The unload in frmMain unloads it's sub forms
  Unload OldMain
  
  'Unload all other forms
  Dim fForm As Form
  For Each fForm In Forms
    Unload fForm

  Next
  
  'Reset Factory
  Set Factory = New ServiceFactory
        
  fSplash.Show
  Login
  fSplash.Refresh
    
  ' Load up main form
  Dim fMain As New frmMain, FolderId As String
  Load fMain
  FolderId = IniForm.ReadFile(fMain.Name, "folderkey")
  If FolderId = vbNullString Then
    FolderId = fMain.tvFolders.Nodes(1).Key
  End If
  fMain.FolderKey = FolderId
  If Not fMain.tvFolders.SelectedItem Is Nothing Then
    fMain.tvFolders.SelectedItem.Expanded = IIf(fMain.tvFolders.SelectedItem.children > 0, True, False)
  End If
  fMain.Show
End Sub



' Will be obsolete
Public Function IniViewFileNameOld(Source As String) As String
  Dim Ini As New IniFile
  
  Ini.FileName = CONFDEFAULTPATH & "\view.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "view"
    Exit Function
  End If
  
  Ini.FileName = CONFDEFAULTPATH & "\viewclaim.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewclaim"
    Exit Function
  End If
  
  Ini.FileName = CONFDEFAULTPATH & "\viewevent.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewevent"
    Exit Function
  End If
  
  Ini.FileName = CONFDEFAULTPATH & "\viewcontact.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewcontact"
    Exit Function
  End If
  
  Ini.FileName = CONFDEFAULTPATH & "\viewrating.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewrating"
    Exit Function
  End If
  
  Ini.FileName = CONFDEFAULTPATH & "\viewsys.ini"
  If Ini.ReadFile(Source, "View") <> vbNullString Then
    IniViewFileNameOld = "viewsys"
    Exit Function
  End If
  
  IniViewFileNameOld = "viewevent"
End Function


'
' Map a source to a file name.  Need to partition because of 64K limit.
'
Public Function FolderIniFileActive(FolderName As String) As String
  Dim FileName As String
  
  FileName = CONFPERSONALPATH & "\" & FolderName & ".ini"
  If FileUtil.IsFile(FileName) Then
    FolderIniFileActive = FileName
    Exit Function
  End If
  
  FileName = CONFDEFAULTPATH & "\" & FolderName & ".ini"
  If FileUtil.IsFile(FileName) Then
    FolderIniFileActive = FileName
    Exit Function
  End If
  
  ' Retrieve old file name setting.
  FolderIniFileActive = CONFDEFAULTPATH & "\" & IniViewFileNameOld(FolderName) & ".ini"
End Function

Public Function FolderIniFileName(FolderName As String, Optional Personal As Boolean) As String
  If Personal Then
    FolderIniFileName = CONFPERSONALPATH & "\" & FolderName & ".ini"
    Exit Function
  End If
  
  FolderIniFileName = CONFDEFAULTPATH & "\" & FolderName & ".ini"
End Function

Public Function GetOperator(opp As Integer) As String
  Select Case opp
    Case isGreaterThan
      GetOperator = ">"
    Case isLessThan
      GetOperator = "<"
    Case isEqualTo
      GetOperator = "="
  End Select
End Function

Public Function NoCache() As String
  mCache = mCache + 1
  NoCache = "noCache=" & mCache
End Function

'This Sub will display the full where clause (including the base filter of
'a data provider. This is used when debugging advanced querries
Public Sub DebugWhereClause(DP As DataProvider)
  Dim BaseFilter As String
  BaseFilter = DP.BaseFilter
  MsgBox "Where Clause of SQL:" & vbCrLf & _
         IIf(BaseFilter <> vbNullString, BaseFilter & vbCrLf, vbNullString) & _
         DP.filter
End Sub


