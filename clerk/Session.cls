VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Session"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Description = "Establishes a connection to a data source."
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Session"
'
' Session Interface
'

Option Explicit


Public Function NewDataProvider(Optional Source As String) As DataProvider
Attribute NewDataProvider.VB_UserMemId = 0
End Function

Public Function NewSQLDataProvider(Optional Source As String) As DataProvider
End Function

Public Function Authenticate(Form As Object) As Boolean
End Function

Public Property Get Session() As String
End Property

Public Property Get Connection() As String
End Property

Public Property Get SQLDialect() As SQLDialect
End Property

