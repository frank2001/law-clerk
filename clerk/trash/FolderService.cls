VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FolderService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mClient As SoapClient30
Private Const ServiceName = "FolderService"
'Private mDataProvider As DataProvider

'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mClient = New SoapClient30
  mClient.mssoapinit WSDLFile
  mClient.ConnectorProperty("EndPointURL") = EndPointURL
  
  'Set mDataProvider = Session.NewDataProvider(DatabaseName.FOLDER_TABLE)
End Sub


'
' Default finder for single key tables.
'

Public Function Find(Handle As String, Optional Column As String) As Collection
  'Set Find = mDataProvider.Find(DatabaseName.FOLDER_KEY, Handle, Column)
End Function

'
' Interface to server
'


' parameterOrder='FolderId claimId FolderTypeId reimburseDate Folder attributeNames attributeValues parameterNames parameterValues'>


'
' Create.
'
Public Function CreateFolder( _
    parentFolderId As String, _
    FolderName As String, FolderNote As String, _
    FolderType As String, FolderSource As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo Exception
  
  CreateFolder = mClient.Create( _
      parentFolderId, _
      FolderName, FolderNote, _
      FolderType, FolderSource, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  Exit Function
  
Exception:
  MsgBox "Folder Create Service Error: " & Err.Description, vbExclamation
End Function

'
' Path.
'
Public Function FindFolder(Path As String) As String
    
On Error GoTo Exception
  
  ' If no handle, return null back.
  If Path = vbNullString Then Exit Function
  
  ' Get path for handle.
  FindFolder = mClient.Find(Path)
  Exit Function
  
Exception:
  MsgBox "Folder GetPath Service Error: " & Err.Description, vbExclamation
End Function


'
' Path.
'
Public Function GetPath(FolderHandle As String) As String
    
On Error GoTo Exception
  
  ' If no handle, return null back.
  If FolderHandle = vbNullString Then Exit Function
  
  ' Get path for handle.
  GetPath = mClient.GetPath(FolderHandle)
  Exit Function
  
Exception:
  MsgBox "Folder GetPath Service Error: " & Err.Description, vbExclamation
End Function


'
' Move Folder
'
Public Function MoveFolder( _
    FolderHandle As String, parentFolderId As String) As Boolean
    
On Error GoTo Exception
  
  Call mClient.Move(FolderHandle, parentFolderId)
  MoveFolder = True
  Exit Function
  
Exception:
  MsgBox "Folder Move Service Error: " & Err.Description, vbExclamation
End Function


'
' Rename Folder
'
Public Function RenameFolder( _
    FolderHandle As String, FolderName As String) As Boolean
    
On Error GoTo Exception
  
  Call mClient.Rename(FolderHandle, FolderName)
  RenameFolder = True
  Exit Function
  
Exception:
  MsgBox "Folder Rename Service Error: " & Err.Description, vbExclamation
End Function


'
' Swap Folder Positions
'
Public Function SwapFolder( _
    FolderHandle1 As String, FolderHandle2 As String) As Boolean
    
On Error GoTo Exception
  
  Call mClient.Swap(FolderHandle1, FolderHandle2)
  SwapFolder = True
  Exit Function
  
Exception:
  MsgBox "Folder Swap Service Error: " & Err.Description, vbExclamation
End Function

'
' Write an application.
'
Public Function WriteFolder( _
    FolderHandle As String, _
    FolderNote As String, _
    FolderType As String, FolderSource As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo Exception
  
  Call mClient.write( _
      FolderHandle, _
      FolderNote, FolderType, FolderSource, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  WriteFolder = True
  Exit Function
  
Exception:
  MsgBox "Folder Write Service Error: " & Err.Description, vbExclamation
End Function

'
' Delete an Folder
'
Public Function DeleteFolder( _
    FolderHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo Exception

  Call mClient.Delete(FolderHandle, ParameterNames, ParameterValues)
  
  DeleteFolder = True
  Exit Function
  
Exception:
  MsgBox "Folder Service Error: " & Err.Description, vbExclamation
End Function


'
' Service Helpers
'

'
' Add a new workflow folder
'
Public Function AddFolder(Tree As TreeView) As Node
  Dim FolderHandle As String
  FolderHandle = CreateFolder( _
         Tree.SelectedItem.Key, "New Folder", _
         vbNullString, "Workflow", DatabaseName.EVENTVIEW_TABLE, _
         vbNullStringArray, vbNullStringArray, _
         vbNullStringArray, vbNullStringArray)
  If FolderHandle = vbNullString Then
    Exit Function
  End If
      
  ListViewModule.CopyColumnHeaders "EventViewDefault", FolderIniFileName("EventViewDefault"), FolderHandle, FolderIniFileName(FolderHandle)
  
  IniFolder.WriteFile FolderHandle, "data.type", DatabaseName.EVENT_TABLE
  IniFolder.WriteFile FolderHandle, "data.source", DatabaseName.EVENTVIEW_TABLE
  
  IniFolder.WriteFile FolderHandle, "filter", """completeDate"" is null and ""voidDate"" is null"
  IniFolder.WriteFile FolderHandle, "popup", "Popup2.mnuEvent"
  IniFolder.WriteFile FolderHandle, "popup.default", "mnuEventOpen(1)"
  IniFolder.WriteFile FolderHandle, "popup.open", "mnuEventOpen"
  
    Dim nodeX As Node
  
  Set nodeX = Tree.Nodes.Add(Tree.SelectedItem, tvwChild, FolderHandle, "New Folder", _
      "TVFolderImage", "TVOpenFolderImage")
  nodeX.ExpandedImage = "TVOpenFolderImage"
        
  nodeX.Selected = True
  Set AddFolder = nodeX
End Function

'
' Remove the selected folder.
'
Public Sub RemoveFolder(Tree As TreeView)
  If Not MsgBox("Are you sure you want to remove the folder '" & Tree.SelectedItem.FullPath & "'?", vbYesNo + vbQuestion, "Confirm Folder Delete") = vbYes Then
    Exit Sub
  End If
  If DeleteFolder(Tree.SelectedItem.Key, vbNullStringArray, vbNullStringArray) Then
    Tree.Nodes.Remove Tree.SelectedItem.Index
  End If
End Sub


