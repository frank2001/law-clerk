VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFolderPropertiesEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Properties"
   ClientHeight    =   5895
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5940
   Icon            =   "frmFolderProperties.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5895
   ScaleWidth      =   5940
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   4644
      TabIndex        =   41
      Top             =   5448
      Width           =   1200
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5196
      Left            =   84
      TabIndex        =   17
      Top             =   96
      Width           =   5760
      _ExtentX        =   10160
      _ExtentY        =   9155
      _Version        =   393216
      Style           =   1
      TabHeight       =   420
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmFolderProperties.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFolder"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraData"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraLine(2)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Note"
      TabPicture(1)   =   "frmFolderProperties.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtDataField(2)"
      Tab(1).Control(1)=   "cmdClearNotepad"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Workflow"
      TabPicture(2)   =   "frmFolderProperties.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraWorkflow(0)"
      Tab(2).Control(1)=   "fraWorkflow(1)"
      Tab(2).Control(2)=   "fraWorkflow(2)"
      Tab(2).Control(3)=   "fraWorkflow(3)"
      Tab(2).ControlCount=   4
      Begin VB.CommandButton cmdClearNotepad 
         Caption         =   "&Clear"
         Height          =   288
         Left            =   -74880
         TabIndex        =   2
         Top             =   4776
         Width           =   1028
      End
      Begin VB.Frame fraLine 
         Caption         =   "Popup Menu"
         Height          =   1680
         Index           =   2
         Left            =   96
         TabIndex        =   16
         Top             =   3132
         Width           =   5544
         Begin VB.TextBox txtPopup 
            DataField       =   "popup.default"
            Height          =   288
            Index           =   1
            Left            =   984
            TabIndex        =   25
            Text            =   "Text1"
            Top             =   564
            Width           =   2628
         End
         Begin VB.TextBox txtPopup 
            DataField       =   "popup.explore"
            Height          =   288
            Index           =   2
            Left            =   984
            TabIndex        =   26
            Text            =   "Text1"
            Top             =   900
            Width           =   2628
         End
         Begin VB.TextBox txtPopup 
            DataField       =   "popup.open"
            Height          =   288
            Index           =   3
            Left            =   984
            TabIndex        =   27
            Text            =   "Text1"
            Top             =   1236
            Width           =   2628
         End
         Begin VB.TextBox txtPopup 
            DataField       =   "popup"
            Height          =   288
            Index           =   0
            Left            =   984
            TabIndex        =   23
            Text            =   "Text1"
            Top             =   228
            Width           =   2628
         End
         Begin VB.CheckBox chkSearch 
            Caption         =   "Search"
            Height          =   216
            Left            =   3888
            TabIndex        =   24
            Top             =   264
            Width           =   1416
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Default"
            Height          =   192
            Index           =   4
            Left            =   132
            TabIndex        =   34
            Top             =   612
            Width           =   504
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Open"
            Height          =   192
            Index           =   7
            Left            =   132
            TabIndex        =   33
            Top             =   1284
            Width           =   396
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Explore"
            Height          =   192
            Index           =   8
            Left            =   132
            TabIndex        =   32
            Top             =   948
            Width           =   552
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Popup"
            Height          =   192
            Index           =   9
            Left            =   132
            TabIndex        =   31
            Top             =   276
            Width           =   480
         End
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "New"
         Height          =   1116
         Index           =   3
         Left            =   -74916
         TabIndex        =   13
         Top             =   3852
         Width           =   5556
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&New"
            Height          =   336
            Index           =   3
            Left            =   4068
            TabIndex        =   14
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "New settings are used to provide folder and parameter information when creating a new item in this folder."
            Height          =   564
            Index           =   3
            Left            =   156
            TabIndex        =   15
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "Reopen"
         Height          =   1116
         Index           =   2
         Left            =   -74916
         TabIndex        =   10
         Top             =   2688
         Width           =   5556
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&Reopen"
            Height          =   336
            Index           =   2
            Left            =   4068
            TabIndex        =   11
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "Reopen settings are used to provide folder and parameter information when a competed or voided event is reopened in this folder."
            Height          =   564
            Index           =   2
            Left            =   156
            TabIndex        =   12
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "Void"
         Height          =   1116
         Index           =   1
         Left            =   -74916
         TabIndex        =   7
         Top             =   1512
         Width           =   5556
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&Void"
            Height          =   336
            Index           =   1
            Left            =   4068
            TabIndex        =   8
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "Void settings are used to provide folder and parameter information when voiding an event in this folder."
            Height          =   564
            Index           =   1
            Left            =   156
            TabIndex        =   9
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.Frame fraWorkflow 
         Caption         =   "Complete"
         Height          =   1116
         Index           =   0
         Left            =   -74916
         TabIndex        =   4
         Top             =   336
         Width           =   5556
         Begin VB.CommandButton cmdWorkflow 
            Caption         =   "&Complete"
            Height          =   336
            Index           =   0
            Left            =   4068
            TabIndex        =   5
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "Complete settings are used to provide folder and parameter information when completing an event in this folder."
            Height          =   564
            Index           =   0
            Left            =   156
            TabIndex        =   6
            Top             =   252
            Width           =   3240
         End
      End
      Begin VB.Frame fraData 
         Caption         =   "Data"
         Height          =   1656
         Left            =   96
         TabIndex        =   3
         Top             =   1380
         Width           =   5544
         Begin VB.ComboBox cboDataType 
            Height          =   288
            ItemData        =   "frmFolderProperties.frx":0060
            Left            =   984
            List            =   "frmFolderProperties.frx":0109
            Style           =   2  'Dropdown List
            TabIndex        =   19
            Top             =   228
            Width           =   2628
         End
         Begin VB.TextBox txtNewItem 
            DataField       =   "folderSource"
            Height          =   288
            Left            =   984
            TabIndex        =   22
            Text            =   "Text1"
            Top             =   1236
            Width           =   2628
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "folderSource"
            Height          =   288
            Index           =   1
            Left            =   984
            TabIndex        =   20
            Text            =   "Text1"
            Top             =   564
            Width           =   2628
         End
         Begin VB.TextBox txtFilter 
            DataField       =   "folderSource"
            Height          =   288
            Left            =   984
            TabIndex        =   21
            Text            =   "Text1"
            Top             =   900
            Width           =   4440
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Type:"
            Height          =   192
            Left            =   132
            TabIndex        =   38
            Top             =   276
            Width           =   420
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "New"
            Height          =   192
            Index           =   6
            Left            =   132
            TabIndex        =   37
            Top             =   1284
            Width           =   324
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Filter"
            Height          =   192
            Index           =   3
            Left            =   132
            TabIndex        =   36
            Top             =   960
            Width           =   348
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Source"
            Height          =   192
            Index           =   0
            Left            =   132
            TabIndex        =   35
            Top             =   612
            Width           =   516
         End
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "folderNote"
         Height          =   4284
         Index           =   2
         Left            =   -74892
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Text            =   "frmFolderProperties.frx":0407
         Top             =   348
         Width           =   5496
      End
      Begin VB.Frame fraFolder 
         Caption         =   "Folder"
         Height          =   948
         Left            =   96
         TabIndex        =   0
         Top             =   336
         Width           =   5544
         Begin VB.TextBox txtDataField 
            DataField       =   "folderName"
            Height          =   288
            Index           =   0
            Left            =   984
            TabIndex        =   18
            Text            =   "Text1"
            Top             =   228
            Width           =   2628
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Location:"
            Height          =   192
            Index           =   1
            Left            =   132
            TabIndex        =   30
            Top             =   624
            Width           =   648
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Name:"
            Height          =   192
            Index           =   5
            Left            =   132
            TabIndex        =   29
            Top             =   276
            Width           =   480
         End
         Begin VB.Label lblLocation 
            AutoSize        =   -1  'True
            Caption         =   "location"
            Height          =   192
            Left            =   996
            TabIndex        =   28
            Top             =   624
            Width           =   4176
            WordWrap        =   -1  'True
         End
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   3348
      TabIndex        =   40
      Top             =   5448
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   2040
      TabIndex        =   39
      Top             =   5448
      Width           =   1200
   End
End
Attribute VB_Name = "frmFolderPropertiesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.60.00 (jjm) 2004 03 15
Option Explicit

Private mFolderHandle As String
Private mFolderName As String
Private mDirty As Boolean
Private mData As DataProvider
Private mOk As Boolean
Private mDataControls As Collection

' ToDo Comp w/1.80 - old source and FolderType will not be needed (min 1.90)
' Fix all previous calls to mSource and mFolderType from server.
Private mSourceOld As String
Private mFolderTypeOld As String

Private Property Let Dirty(vData As Boolean)
  If mDirty = vData Then Exit Property
  mDirty = vData
End Property

Public Property Get FolderFilter() As String
  If Not mDataControls Is Nothing Then
    FolderFilter = txtFilter
  End If
End Property

Public Property Get FolderSource() As String
  If Not mDataControls Is Nothing Then
    FolderSource = mDataControls("folderSource")
  End If
End Property

Public Sub FormRefresh()
  Dim Datafields As Collection
  Set Datafields = mData.Find(DatabaseName.FOLDER_KEY, mFolderHandle)
  
  mFolderName = Datafields("folderName")
  
  ' ToDo Comp w/1.80 - old source and FolderType will not be needed (min 1.90)
  mSourceOld = Datafields("folderSource")
  mFolderTypeOld = Datafields("folderType")
  
  Caption = mFolderName & " Properties (" & mFolderHandle & ")"
 
  SetFields txtDataField, Datafields
  
  ' Get other ini info
  txtFilter.Text = IniFolder.ReadFile(mFolderHandle, "filter")
  txtNewItem.Text = IniFolder.ReadFile(mFolderHandle, "newitem")
     
  ' Setup data source and type
  
  Dim folderDataType As String
  Dim i As Integer
  folderDataType = IniFolder.ReadFile(mFolderHandle, "data.type")
  With cboDataType
    For i = 0 To .ListCount - 1
      If folderDataType = .List(i) Then
        .ListIndex = i
      End If
    Next i
  End With
  
  Dim Source As String
  Source = IniFolder.ReadFile(mFolderHandle, "data.source")
  
  'ToDo Comp w/1.80(vb) - will always assign from Ini file above - remove below (min 1.90)
  If Source <> vbNullString Then
    txtDataField(1).Text = Source
  End If
    
    
  'ToDO: make final convert to ini instead of foldertable for source
  'txtSource(NOT YET CREATED).Text = iniFolder.ReadFile(mFodlerHandle, "source")
  
  txtPopup(0).Text = IniFolder.ReadFile(mFolderHandle, "popup")
  txtPopup(1).Text = IniFolder.ReadFile(mFolderHandle, "popup.default")
  txtPopup(2).Text = IniFolder.ReadFile(mFolderHandle, "popup.explore")
  txtPopup(3).Text = IniFolder.ReadFile(mFolderHandle, "popup.open")
  
  chkSearch.Value = GetInteger(IniFolder.ReadFile(mFolderHandle, "search"))
  
  lblLocation.Caption = Factory.FolderService.GetPath(mFolderHandle)
   
  Dirty = False
End Sub


Private Function Save() As Boolean
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  Set mDataControls = New Collection
  AddTextControl txtDataField, mDataControls
    
  If mFolderName <> mDataControls("folderName") Then
    Save = Factory.FolderService.RenameFolder(mFolderHandle, mDataControls("folderName"))
    If Not Save Then Exit Function
  End If
  
  mFolderTypeOld = IIf(cboDataType.Text = DatabaseName.EVENT_TABLE, "Workflow", "Data Model")
  
  Save = Factory.FolderService.WriteFolder(mFolderHandle, _
      mDataControls("folderNote"), _
      mFolderTypeOld, mSourceOld, _
      vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
  
  If Save Then
    IniFolder.WriteFile mFolderHandle, "filter", txtFilter.Text
    IniFolder.WriteFile mFolderHandle, "newitem", txtNewItem.Text
    IniFolder.WriteFile mFolderHandle, "version", "1.10 (" & Now & ")"
    
    IniFolder.WriteFile mFolderHandle, "data.type", cboDataType.Text
    IniFolder.WriteFile mFolderHandle, "data.source", txtDataField(1).Text
    
    IniFolder.WriteFile mFolderHandle, "popup", txtPopup(0).Text
    IniFolder.WriteFile mFolderHandle, "popup.default", txtPopup(1).Text
    IniFolder.WriteFile mFolderHandle, "popup.explore", txtPopup(2).Text
    IniFolder.WriteFile mFolderHandle, "popup.open", txtPopup(3).Text
    
    If chkSearch Then
      IniFolder.WriteFile mFolderHandle, "search", "1"
    Else
      IniFolder.RemoveKey mFolderHandle, "search"
    End If
    
    Dirty = False
  End If
  
End Function


Public Sub ShowFolder(FolderHandle As String)
  mFolderHandle = FolderHandle
  Show
End Sub

Private Sub cboDataType_Click()
  Dirty = True
End Sub

Private Sub chkSearch_Click()
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtDataField(2).Text = vbNullString
  txtDataField(2).SetFocus
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp HelpName.FOLDERPROPERTIES_TOPIC, Me.hWnd
End Sub

Private Sub cmdOk_Click()
  If mDirty Then
    If Not Save Then Exit Sub
    mOk = True
    MsgBox "You need to restart the program for changes to take effect.", vbOKOnly
  End If
  Unload Me
End Sub

Private Sub cmdWorkflow_Click(Index As Integer)
  FolderUtil.WorkflowShow mFolderHandle, Index
End Sub

Private Sub Form_Initialize()
  Set mData = LawSession.NewDataProvider(DatabaseName.FOLDER_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  If (mFolderHandle = "") Then
    MsgBox "Must use ShowFolder to call form"
    Unload Me
    Exit Sub
  End If
  
  TabControlEnable SSTab1
  
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  TabControlEnable SSTab1
End Sub

Private Sub txtDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  Select Case Index
    Case 2
      FormUtil.NoteGotFocus cmdOk
  End Select
End Sub

Private Sub txtDataField_LostFocus(Index As Integer)
  Select Case Index
    Case 2
      FormUtil.NoteLostFocus cmdOk
  End Select
End Sub

Private Sub txtFilter_Change()
  Dirty = True
End Sub

Private Sub txtNewItem_Change()
  Dirty = True
End Sub

Private Sub txtPopup_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtVersion_Change()
  Dirty = True
End Sub
