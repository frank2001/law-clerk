VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCoverageEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Coverage Edit"
   ClientHeight    =   5712
   ClientLeft      =   120
   ClientTop       =   744
   ClientWidth     =   5064
   Icon            =   "frmCoverageEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5712
   ScaleWidth      =   5064
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5325
      Left            =   0
      ScaleHeight     =   5328
      ScaleWidth      =   5064
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   384
      Width           =   5064
      Begin VB.TextBox txtDataField 
         DataField       =   "eventTypeId"
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Enabled         =   0   'False
         Height          =   288
         Index           =   0
         Left            =   4032
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   936
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   2520
         TabIndex        =   8
         Top             =   4905
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         CausesValidation=   0   'False
         Height          =   336
         Left            =   3780
         TabIndex        =   9
         Top             =   4905
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   4710
         Left            =   90
         TabIndex        =   10
         Top             =   90
         Width           =   4890
         _ExtentX        =   8615
         _ExtentY        =   8319
         _Version        =   393216
         Style           =   1
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmCoverageEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label3"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblDataField(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label5"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblDataField(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label7"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblDataField(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Label9"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblDataField(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Label1"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblDataField(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblDataField(4)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblTerm"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "Label4"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblFirmLocator"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblDataField(7)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblExpirationDate"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblExpirationDateLabel"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "dtpExpirationDate"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "dtpDataField(0)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "cmdLimit"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "cmdAccounting"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "cboEventType"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "cboTerm"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).ControlCount=   25
         TabCaption(1)   =   "&Lawyer"
         TabPicture(1)   =   "frmCoverageEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvLawyer"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "N&ote"
         TabPicture(2)   =   "frmCoverageEdit.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "txtNote"
         Tab(2).Control(1)=   "cmdClearNotepad"
         Tab(2).ControlCount=   2
         Begin VB.ComboBox cboTerm 
            Height          =   315
            ItemData        =   "frmCoverageEdit.frx":0060
            Left            =   1890
            List            =   "frmCoverageEdit.frx":0073
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   1830
            Width           =   1410
         End
         Begin VB.ComboBox cboEventType 
            DataField       =   "eventTypeId"
            DataMember      =   "Event"
            Height          =   315
            ItemData        =   "frmCoverageEdit.frx":008E
            Left            =   1884
            List            =   "frmCoverageEdit.frx":0090
            Style           =   2  'Dropdown List
            TabIndex        =   0
            Top             =   1116
            Width           =   2904
         End
         Begin VB.CommandButton cmdAccounting 
            Caption         =   "&Accounting"
            Enabled         =   0   'False
            Height          =   288
            Left            =   2232
            TabIndex        =   4
            Top             =   4275
            Width           =   1080
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74904
            TabIndex        =   7
            Top             =   3804
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   3324
            Left            =   -74916
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   6
            Top             =   372
            Width           =   4716
         End
         Begin MSComctlLib.ListView lvLawyer 
            Height          =   3144
            Left            =   -74928
            TabIndex        =   5
            Top             =   360
            Width           =   4740
            _ExtentX        =   8361
            _ExtentY        =   5546
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Name"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "RDI"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.CommandButton cmdLimit 
            Caption         =   "L&imit"
            Height          =   288
            Left            =   192
            TabIndex        =   3
            Top             =   2580
            Width           =   1000
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "issueDate"
            Height          =   285
            Index           =   0
            Left            =   1890
            TabIndex        =   1
            Top             =   1485
            Width           =   1410
            _ExtentX        =   2477
            _ExtentY        =   508
            _Version        =   393216
            Format          =   62586881
            CurrentDate     =   37264
         End
         Begin MSComCtl2.DTPicker dtpExpirationDate 
            Height          =   285
            Left            =   1890
            TabIndex        =   40
            Top             =   2205
            Width           =   1410
            _ExtentX        =   2477
            _ExtentY        =   508
            _Version        =   393216
            Format          =   62586881
            CurrentDate     =   37264
         End
         Begin VB.Label lblExpirationDateLabel 
            Caption         =   "Expiration Date:"
            Height          =   195
            Left            =   180
            TabIndex        =   41
            Top             =   2250
            Width           =   1350
         End
         Begin VB.Label lblExpirationDate 
            AutoSize        =   -1  'True
            Caption         =   "expirationDate"
            Height          =   195
            Left            =   3375
            TabIndex        =   39
            Top             =   1890
            Width           =   1020
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "policyNumber"
            DataField       =   "policyNumber"
            DataMember      =   "Policy"
            Height          =   192
            Index           =   7
            Left            =   1896
            TabIndex        =   38
            Top             =   564
            Width           =   1008
         End
         Begin VB.Label lblFirmLocator 
            AutoSize        =   -1  'True
            Caption         =   "(firmLocator)"
            Height          =   192
            Left            =   1896
            TabIndex        =   37
            Top             =   324
            Width           =   888
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Policy Number"
            Height          =   192
            Left            =   192
            TabIndex        =   36
            Top             =   564
            Width           =   1056
         End
         Begin VB.Label lblTerm 
            AutoSize        =   -1  'True
            Caption         =   "Term"
            Height          =   192
            Left            =   192
            TabIndex        =   35
            Top             =   1891
            Width           =   384
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date"
            Height          =   192
            Index           =   1
            Left            =   192
            TabIndex        =   34
            Top             =   828
            Width           =   996
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            DataMember      =   "Application"
            Height          =   192
            Index           =   4
            Left            =   1896
            TabIndex        =   33
            Top             =   828
            UseMnemonic     =   0   'False
            Width           =   948
         End
         Begin VB.Label lblLabel 
            Caption         =   "Issue Date"
            Height          =   192
            Index           =   0
            Left            =   192
            TabIndex        =   32
            Top             =   1531
            Width           =   1356
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmName"
            DataField       =   "firmName"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            DataMember      =   "Application"
            Height          =   192
            Index           =   5
            Left            =   204
            TabIndex        =   31
            Top             =   324
            UseMnemonic     =   0   'False
            Width           =   696
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "CoverageType"
            Height          =   192
            Left            =   192
            TabIndex        =   30
            Top             =   1164
            Width           =   1104
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "premium"
            DataField       =   "premium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "QuoteCoverage"
            Height          =   195
            Index           =   3
            Left            =   2745
            TabIndex        =   20
            Top             =   3930
            UseMnemonic     =   0   'False
            Width           =   585
         End
         Begin VB.Label Label9 
            Caption         =   "Premium"
            Height          =   195
            Left            =   195
            TabIndex        =   19
            Top             =   3930
            Width           =   1515
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "perClaimDeductible"
            DataField       =   "perClaimDeductible"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "QuoteCoverage"
            Height          =   195
            Index           =   2
            Left            =   1905
            TabIndex        =   18
            Top             =   3630
            UseMnemonic     =   0   'False
            Width           =   1425
         End
         Begin VB.Label Label7 
            Caption         =   "Per Claim Deductible"
            Height          =   195
            Left            =   195
            TabIndex        =   17
            Top             =   3630
            Width           =   1515
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "aggregateLimit"
            DataField       =   "aggregateLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "QuoteCoverage"
            Height          =   195
            Index           =   1
            Left            =   2250
            TabIndex        =   16
            Top             =   3330
            UseMnemonic     =   0   'False
            Width           =   1080
         End
         Begin VB.Label Label5 
            Caption         =   "Aggregate Limit"
            Height          =   195
            Left            =   195
            TabIndex        =   15
            Top             =   3330
            Width           =   1350
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "perClaimLimit"
            DataField       =   "perClaimLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "QuoteCoverage"
            Height          =   195
            Index           =   0
            Left            =   2355
            TabIndex        =   14
            Top             =   3015
            UseMnemonic     =   0   'False
            Width           =   975
         End
         Begin VB.Label Label3 
            Caption         =   "Per Claim Limit"
            Height          =   195
            Left            =   195
            TabIndex        =   13
            Top             =   3015
            Width           =   1350
         End
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "applicationTypeId"
         DataField       =   "applicationTypeId"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         DataMember      =   "Application"
         Height          =   192
         Index           =   6
         Left            =   3228
         TabIndex        =   22
         Top             =   48
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   1308
         WordWrap        =   -1  'True
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   744
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   456
      Top             =   504
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   396
      Left            =   108
      TabIndex        =   12
      Top             =   4176
      Visible         =   0   'False
      Width           =   972
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   5064
      _ExtentX        =   8932
      _ExtentY        =   677
      BandCount       =   2
      BandBorders     =   0   'False
      _CBWidth        =   5064
      _CBHeight       =   384
      _Version        =   "6.7.9782"
      Child1          =   "picMenu"
      MinHeight1      =   336
      Width1          =   3504
      NewRow1         =   0   'False
      Child2          =   "picUpDown"
      MinWidth2       =   600
      MinHeight2      =   312
      Width2          =   948
      NewRow2         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   312
         Left            =   3636
         ScaleHeight     =   312
         ScaleWidth      =   1356
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   36
         Width           =   1356
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   0
            TabIndex        =   28
            Top             =   12
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   132
         ScaleHeight     =   336
         ScaleWidth      =   3348
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   24
         Width           =   3348
         Begin MSComctlLib.Toolbar tbrApplication 
            Height          =   312
            Left            =   -36
            TabIndex        =   25
            Top             =   0
            Width           =   1272
            _ExtentX        =   2244
            _ExtentY        =   550
            ButtonWidth     =   2201
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Application"
                  Object.ToolTipText     =   "Opens the Application"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrOther 
            Height          =   312
            Left            =   2376
            TabIndex        =   26
            Top             =   0
            Width           =   1032
            _ExtentX        =   1820
            _ExtentY        =   550
            ButtonWidth     =   1355
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Other"
                  Object.ToolTipText     =   "Opens the Application"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Policy"
                        Text            =   "Policy"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrRating 
            Height          =   312
            Left            =   1200
            TabIndex        =   29
            Top             =   0
            Width           =   1068
            _ExtentX        =   1884
            _ExtentY        =   550
            ButtonWidth     =   1482
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Rating"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Edit"
                        Text            =   "Edit"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Worksheet"
                        Text            =   "Worksheet"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Application"
            Index           =   0
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Policy"
            Index           =   2
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator342 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator8372 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Rating"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Undewriting Worksheet"
         Index           =   1
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator876 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&davanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator986 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
      End
      Begin VB.Menu mnuSeparator3512 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmCoverageEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.71.00 (jjm) 2005 02 10
'ver 2.50.00 (jjm) 2004 02 15
'ver 2.40.03 (jjm) bug fix, cannot set focus to a disabled command button
'ver 2.30.0716
'ver 2.00.0115
Option Explicit

Private mCoverageHandle As String
Private mApplicationHandle As String
Private mQuoteCoverageHandle As String
Private mQuoteBookHandle As String
Private mQuoteHandle As String
Private mEventHandle As String
Private mEventTypeHandle As String
Private mFirmHandle As String
Private mPolicyHandle As String
Private mPolicyAccountingHandle As String
Private mRatingBookHandle As String

Private mQuote As Dataset
Private mListView As ListView
Private mListIndex As Integer
Private mListItemType As String

Private mQuoteCoverage As Dataset
Private mEvent As Dataset

Private mEventTypes As collection

Private mParameterNames() As String
Private mParameterValues() As String

Private mDirty As Boolean
Public FormMgr As New FormManager

'Booleans to help cut down on network overhead
Private mEnableAccounting As Boolean
Private mEnableLimit As Boolean
'ver 2.71.00 (jjm) 2005 02 10
Private mEnableTerm As Boolean


'ver 2.71.00 (jjm) 2005 02 10
'Cache a local value of the term
Private mTerm As String

'Const for the default term
Private Const TERM_DEFAULT = "Unlimited"

'ver 2.71.00 (jjm) 2005 02 10
'Cache the expiration date of the policy
Private mPolicyExpirationDate As Date

'
'Index Constants
Const FirmNameDataFieldIndex = 5



Public Sub ApplicationRefresh()
  ClearDataField lblDataField, DatabaseName.APPLICATION_TABLE
  Dim Row As collection
  Set Row = Factory.ApplicationService.Find(mApplicationHandle)
  If Not Row Is Nothing Then
    SetFields lblDataField, Row, DatabaseName.APPLICATION_TABLE
  End If
End Sub

Public Sub LimitRefresh()
    
  If mQuoteCoverageHandle = vbNullString Then
    Exit Sub
  End If
  
  ClearDataField lblDataField, DatabaseName.QUOTECOVERAGE_TABLE
  
  Dim Row As collection
  mQuoteCoverage.DataProvider.filter = LawDialect.EqualString(DatabaseName.QUOTECOVERAGE_KEY, mQuoteCoverageHandle)
  mQuoteCoverage.DataProvider.OpenRecordset
  Set Row = mQuoteCoverage.DataProvider.collection
  SetFields lblDataField, Row(1), DatabaseName.QUOTECOVERAGE_TABLE
  
End Sub


'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  ' Only reset on Property when value changes.  This avoids
  ' updating the form so often.
  If mDirty <> vData Then
    ResetDirty vData
  End If
End Property


Private Property Get EnableTerm() As Boolean
  EnableTerm = mEnableTerm
End Property


'ver 2.71.00 (jjm) 2005 02 10
'Properties to hide and show the Term
Private Property Let EnableTerm(isTerm As Boolean)
    
  'Set the variable
  mEnableTerm = isTerm
  
  'setup the form
  lblTerm.Visible = mEnableTerm
  cboTerm.Visible = mEnableTerm
  lblExpirationDate.Visible = mEnableTerm
  
  'The if no term then the items are hidden and there is no need to continue
  If Not mEnableTerm Then Exit Property
  
  
  'Pull the term values if needed
  If mTerm = vbNullString Then
    'Get the attribute
    mTerm = Factory.EventService.GetAttribute(mEventHandle, DatabaseName.TERM_ATTRIBUTE)
    'Correct for unlimited
    If mTerm = vbNullString Then mTerm = TERM_DEFAULT
  End If
  
  'Ensure mTerm = cboTerm
  If mTerm <> cboTerm.Text Then
    Dim I As Integer
    For I = 0 To cboTerm.ListCount - 1
      If cboTerm.List(I) = mTerm Then
        cboTerm.ListIndex = I
        Exit For
      End If
    Next I
  End If
  
  
End Property




' Reset the dirty flag and update the form.
Private Sub ResetDirty(Dirty As Boolean)
  mDirty = Dirty
  FormUtil.Dirty Me, Dirty
End Sub


Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub

    ' Select form selected
    Case fimSelect
      mQuoteCoverageHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.QUOTECOVERAGE_KEY)
      LimitRefresh
      
      'ver 2.40.03 (jjm) bug fix, cannot set focus to a disabled command button
      If cmdAccounting.Enabled Then cmdAccounting.SetFocus
      
      Dirty = True
      
    Case fimSave
      If fInterface.Key = DatabaseName.POLICYACCOUNTING_TABLE & mCoverageHandle Then
        If Not fInterface.Child.PolicyAccountingHandle = mPolicyAccountingHandle Then
          mPolicyAccountingHandle = fInterface.Child.PolicyAccountingHandle
          
          ' We can set mDirty here because Save will not fail and Save will set
          ' Dirty (and thus mDirty) to false when the save concludes.
          mDirty = True
          
          ' Force a save.
          Save False, True
        End If
      End If
  End Select
  Show
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
    
  ClearDataField lblDataField
  ClearDataField dtpDataField
  
  mTerm = vbNullString
  mPolicyExpirationDate = vbNullDate

  Dim CoverageData As collection
  Set CoverageData = Factory.CoverageService.Find(mCoverageHandle)
  
  If CoverageData Is Nothing Then
    MsgBox "Coverage id " & mCoverageHandle & " no longer exists."
    Exit Sub
  End If
  
  Caption = "Edit Coverage (" & mCoverageHandle & ")"
  
  mApplicationHandle = CoverageData(DatabaseName.APPLICATION_KEY)
  mQuoteBookHandle = CoverageData(DatabaseName.QUOTEBOOK_KEY)
  mQuoteCoverageHandle = CoverageData(DatabaseName.QUOTECOVERAGE_KEY)
  mQuoteHandle = CoverageData(DatabaseName.QUOTE_KEY)
  mFirmHandle = CoverageData(DatabaseName.FIRM_KEY)
  mPolicyHandle = CoverageData(DatabaseName.POLICY_KEY)
  
  mEventHandle = CoverageData(DatabaseName.EVENT_KEY)
  If (mEventHandle <> vbNullString) Then
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  End If
  
  
  
  Dim Lawyers, Row As collection
  Set Lawyers = Factory.QuoteService.Lawyers(mQuoteHandle)
  
  lvLawyer.ListItems.Clear
  
  If Not Lawyers Is Nothing Then
    For Each Row In Lawyers
      With lvLawyer.ListItems.Add(, Row(DatabaseName.LAWYER_KEY), Row(DatabaseName.LAWYERNAME_FIELD))
        .ListSubItems.Add , , GetDataFromCollection(Row, DatabaseName.RETROACTIVEDATE_FIELD)
      End With
    Next Row
    If lvLawyer.ListItems.Count > 0 Then
      ListViewModule.AutosizeWidth lvLawyer
    End If
  End If
  
  'Set coverage data
  If GetDouble(CoverageData(DatabaseName.AGGREGATELIMIT_FIELD)) <> 0 Or _
       GetDouble(CoverageData(DatabaseName.PERCLAIMLIMIT_FIELD)) <> 0 Or _
         GetDouble(CoverageData(DatabaseName.PERCLAIMDEDUCTIBLE_FIELD)) <> 0 Then
    SetFields lblDataField, CoverageData
  End If
  SetFields dtpDataField, CoverageData
  dtpDataField(0).CheckBox = False
  
  
 'Check for a PolicyAccounting record
  mPolicyAccountingHandle = GetDataFromCollection(Factory.PolicyAccountingService.FindByCoverage(mCoverageHandle), _
                                                DatabaseName.POLICYACCOUNTING_KEY)

  
  
  mnuFileOpen(0).Enabled = mApplicationHandle <> vbNullString
  mnuFileOpen(1).Enabled = mFirmHandle <> vbNullString
  mnuFileOpen(2).Enabled = mPolicyHandle <> vbNullString
  tbrOther.Buttons(1).ButtonMenus(2).Enabled = mPolicyHandle <> vbNullString

  ApplicationRefresh
  LimitRefresh
  
  
  'ver 2.50.00 (jjm) 2004 02 15 - Display the Policy Number
  If mPolicyHandle <> vbNullString Then
    Dim PolCol As collection
    Set PolCol = Factory.PolicyService.Find(mPolicyHandle, DatabaseName.POLICYNUMBER_FIELD & "," & DatabaseName.EXPIRATIONDATE_FIELD)
    SetFields lblDataField, PolCol, "Policy"
    
    'ver 2.71.00 (jjm) 2005 02 10
    'Get Policy expiration date for calculating term
    mPolicyExpirationDate = GetDate(PolCol(DatabaseName.EXPIRATIONDATE_FIELD))
  End If
  
  
  'Select the proper coverageTypeId
  Dim EventData As collection
  Set EventData = Factory.EventService.Find(mEventHandle, DatabaseName.EVENTTYPE_KEY)
  mEventTypeHandle = EventData(DatabaseName.EVENTTYPE_KEY)
  If Not (EventData Is Nothing Or mEventTypes Is Nothing) Then
    Dim I As Integer
    For I = 1 To mEventTypes.Count
      If mEventTypes(I)(DatabaseName.EVENTTYPE_KEY) = EventData(DatabaseName.EVENTTYPE_KEY) Then
        cboEventType.ListIndex = I - 1
        Exit For
      End If
    Next I
  Else
    cboEventType.ListIndex = -1
  End If
  
  
  
  'ExpirationDate EventAttribute - Mantis #2791 ver 3.4
  Dim enableExpirationDate, markAsDirty As Boolean
  enableExpirationDate = (mEventTypeHandle = DatabaseName.CVG_POLICYEXTENSION_TYPE)
  'control visiblity
  lblExpirationDateLabel.Visible = enableExpirationDate
  dtpExpirationDate.Visible = enableExpirationDate
  If enableExpirationDate Then
    Dim expireAttr As String
    expireAttr = Factory.EventService.GetAttribute(mEventHandle, DatabaseName.EXPIRATIONDATE_ATTRIBUTE)
    'Pull from custom PolicyExtensionView
    If expireAttr = vbNullString Then
      'Dirty Hack I was ordered to do by the devil - jjm
      expireAttr = FindHandle(DatabaseName.POLICYEXTENSION_VIEW, DatabaseName.POLICY_KEY, mPolicyHandle, DatabaseName.NEWEXPIRATIONDATE_FIELD)
      markAsDirty = True
    End If
    'If the policy is not in the view, default to policy's expiration date?
    If expireAttr = vbNullString Then
      MsgBox "Expiration date has been defaulted to the policy's expiration date. Please set the expiration date to the desired value before completing the coverage."
      dtpExpirationDate.Value = mPolicyExpirationDate
    Else
      dtpExpirationDate.Value = Format(expireAttr, dtpExpirationDate.DataFormat.Format)
    End If
  End If
    
  
  
  'Accounting button only enabled if the
  'current event type is in the group of CvgPolicyBinding  or CvgPolicyMisc
  
  'set booleans to store data so factory calls are not made continuously.
  mEnableAccounting = (Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, DatabaseName.CVGPOLICYBINDING_RULE) Or _
                       Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, DatabaseName.CVGPOLICYMISC_RULE))
  mEnableLimit = Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, DatabaseName.CVGLIMIT_RULE)
  
  'Enable\Disable buttons
  cmdAccounting.Enabled = mEnableAccounting
  cmdLimit.Enabled = mEnableLimit
  
  
  'ver 2.50.00 (jjm) 2004 02 15 - Display the Firm Locator
  lblFirmLocator.Left = lblDataField(FirmNameDataFieldIndex).Left + lblDataField(FirmNameDataFieldIndex).Width + 48
  lblFirmLocator.Caption = "(" & Factory.FirmService.GetFirmLocator(mFirmHandle) & ")"
  
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex

  ResetDirty False
  Dirty = (CoverageData(DatabaseName.ISSUEDATE_FIELD) = vbNullString) Or markAsDirty
  
  
  
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.COVERAGE_TABLE
      mCoverageHandle = ListItemText(mListView.SelectedItem, DatabaseName.COVERAGE_KEY)
    Case DatabaseName.EVENT_TABLE
      Dim ListEventHandle As String
      ListEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
      
      mCoverageHandle = GetDataFromCollection(Factory.CoverageService.FindByEvent(ListEventHandle, DatabaseName.COVERAGE_KEY), DatabaseName.COVERAGE_KEY)
      If mCoverageHandle <> vbNullString Then
        Exit Property
      End If
      
      'test for a reference event
      Dim CoverageEventHandle As String
      CoverageEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(ListEventHandle, DatabaseName.COVERAGE_TABLE)
                        
      If CoverageEventHandle = vbNullString Then
        MsgBox "Underlying event does not reference a Coverage."
        Exit Property
      End If
      mCoverageHandle = GetDataFromCollection(Factory.CoverageService.FindByEvent(CoverageEventHandle, DatabaseName.COVERAGE_KEY), DatabaseName.COVERAGE_KEY)
      
  End Select
End Property

Private Function Save(Optional Verify As Boolean, Optional NoValidate As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And Verify Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not NoValidate And Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
  Dim mDataControls As New collection
  AddTextControl txtDataField, mDataControls
  AddValueControl dtpDataField, mDataControls
  AddCaptionControl lblDataField, mDataControls
   
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  Dim AttributeNames() As String
  Dim AttributeValues() As String
  
'  AddParameter "CoverageEventNote", _
'      IIf(txtNote.Text = vbNullString, " ", txtNote.Text), _
'      ParameterNames, ParameterValues
'  AddParameter "CoverageEventTypeId", _
'      mEventTypeHandle, _
'      ParameterNames, ParameterValues
'
'  'ver 2.71.00 (jjm) 2005 02 10
'  If EnableTerm Then
'    AddParameter "term", mTerm, _
'        AttributeNames, AttributeValues
'  End If
  
  Save = Factory.CoverageService.WriteCoverage(mCoverageHandle, _
            mEventTypeHandle, _
            mDataControls("issueDate"), _
            mQuoteCoverageHandle, _
            txtNote.Text, _
            IIf(EnableTerm, GetInteger(mTerm), 0), _
            GetDate(dtpExpirationDate.Value))
    
  'Link between coverage and PolicyAccounting should be handled on the PolicyAccounting save
  'Factory.ev
  'Factory.EventService.SetReference mEventHandle, DatabaseName.POLICYACCOUNTING_TABLE, mPolicyAccountingHandle
  
  If Save Then
    FormMgr.Message = fimSave
  End If
  
  Dirty = Not Save
End Function
          
Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.COVERAGE_TABLE)
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(EventHandle As String)
  mEventHandle = EventHandle
  Show
End Sub
'
'
' End General
'
'
'
'
' Control Methods
'
'

Private Sub cboEventType_Click()
  
  Dim I As Integer
  For I = 1 To mEventTypes.Count
    If mEventTypes(I)("eventTypeName") = cboEventType.Text Then
      mEventTypeHandle = mEventTypes(I)(DatabaseName.EVENTTYPE_KEY)
      Exit For
    End If
  Next I
  
  
  'ver 2.71.00 (jjm) 2005 01 31 - modified - Stoped checking
  'individual types and switched to check for a group
  EnableTerm = Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, DatabaseName.OPENEXPIRATIONDATE_RULE)
  
  Dirty = True
  
End Sub

'ver 2.71.00 (jjm) 2005 02 10
Private Sub cboTerm_Click()
  mTerm = cboTerm.Text
 
  Dirty = True
  
  'Check for unlimited
  If cboTerm.Text = TERM_DEFAULT Then
    lblExpirationDate.Caption = vbNullString
    Exit Sub
  End If
  
  'If not unlimited calc expiration date
  lblExpirationDate.Caption = "(" & DateAdd("yyyy", GetDouble(mTerm), mPolicyExpirationDate) & ")"
  
End Sub

Private Sub cbrCoolBar_Resize()
  FormResize
End Sub

Private Sub cmdAccounting_Click()
  If mCoverageHandle = vbNullString Then
    MsgBox "No coverageId present"
    Exit Sub
  End If
 
  If lblDataField(4).Caption = vbNullString Then
    MsgBox "Effective date must be present to select an accounting record."
    Exit Sub
  End If
  
  ' Save coverage first.
  Save
  
  Dim formKey As String
  formKey = DatabaseName.POLICYACCOUNTING_TABLE & mCoverageHandle
  
  ' See if form already showing.
  If FormMgr.ContainsKey(formKey) Then
    FormMgr(DatabaseName.POLICYACCOUNTING_TABLE).Child.Show
    
    Exit Sub
  End If
  
  ' New form
  Dim Payment As New frmPolicyAccountingEdit
  FormMgr.Add Payment, formKey
  If mPolicyAccountingHandle = vbNullString Then
    '2472 - Always show by coverage
    Payment.ShowFromCoverage mPolicyHandle, mCoverageHandle, mEventTypeHandle
  Else
    Payment.ShowByPrimaryKey mPolicyAccountingHandle
  End If

End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdLimit_Click()
  Dispatcher.ShowSearch DatabaseName.QUOTECOVERAGE_TABLE, FormMgr, True, , 1, mQuoteBookHandle
End Sub

Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub dtpExpirationDate_Change()
  Dirty = True
End Sub

Private Sub Form_Initialize()
  Set mQuote = NewGenericDataset(DatabaseName.QUOTE_TABLE)
  Set mQuoteCoverage = NewGenericDataset(DatabaseName.QUOTECOVERAGE_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If

End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
 ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrApplication.ButtonHeight + 24
  cbrCoolBar.Bands(2).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.SizeSingleToolbar tbrApplication, 0
  FormUtil.SizeSingleToolbar tbrRating, tbrApplication.Left + tbrApplication.Width
  FormUtil.SizeSingleToolbar tbrOther, tbrRating.Left + tbrRating.Width
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me

  '
  ' Populate EventTypeName Dropdown with all PolicyBinding group.
  ' We use a GroupCollection here vs. a SourceCollection because we
  ' are looking for a sub-set of the PolicyAccounting source group.
  '
  Set mEventTypes = Factory.EventTypeService.GroupCollection(DatabaseName.COVERAGE_RULE)
  FormUtil.ComboBoxLoad mEventTypes, cboEventType, DatabaseName.EVENTTYPENAME_FIELD
   
  'FormUtil.ComboBoxLoad Factory.EventTypeService.GroupCollection(DatabaseName.COVERAGE_RULE), _
   '         cboEventType, DatabaseName.EVENTTYPE_KEY
   
  FormUtil.TabControlEnable SSTab
      
  FormResize
  FormRefresh
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub


Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey frmQuoteEdit, mQuoteHandle, FormMgr
    Case 1
      Dispatcher.BrowseUnderwritingWorksheet mQuoteHandle
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0 'Application
      FormUtil.ShowByPrimaryKey frmApplicationOpen, mApplicationHandle, FormMgr
    Case 1 'Firm
      FormUtil.ShowByPrimaryKey frmFirmOpen, mFirmHandle, FormMgr
    Case 2 'Policy
      FormUtil.ShowByPrimaryKey frmPolicyOpen, mPolicyHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.COVERAGE_REF, DatabaseName.COVERAGE_TABLE, mCoverageHandle, lblDataField(5)
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.COVERAGE_TABLE, mCoverageHandle
  End If

End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.COVERAGE_TABLE, lawActionNew, _
      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub

Private Sub SSTab_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab
  If SSTab.Tab = 0 Then
    cmdAccounting.Enabled = mEnableAccounting
    cmdLimit.Enabled = mEnableLimit
  End If
End Sub

Private Sub tbrApplication_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileOpen_Click 0
End Sub

Private Sub tbrOther_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileOpen_Click ButtonMenu.Index
End Sub

Private Sub tbrRating_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEdit_Click 0
End Sub

Private Sub tbrRating_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuEdit_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub


Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
