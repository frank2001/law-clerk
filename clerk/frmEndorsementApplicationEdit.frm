VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmEndorsementApplicationEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Endorsement Application"
   ClientHeight    =   6855
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   6705
   Icon            =   "frmEndorsementApplicationEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   6855
   ScaleWidth      =   6705
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6852
      Left            =   0
      ScaleHeight     =   6855
      ScaleWidth      =   6705
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   0
      Width           =   6708
      Begin VB.TextBox txtDataField 
         DataField       =   "firmName"
         Height          =   288
         Index           =   1
         Left            =   1164
         TabIndex        =   25
         Text            =   "firmName"
         Top             =   492
         Width           =   4200
      End
      Begin VB.CommandButton cmdPolicy 
         Caption         =   "&Policy"
         Height          =   288
         Left            =   5580
         TabIndex        =   10
         Top             =   180
         Width           =   1000
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   4104
         TabIndex        =   8
         Top             =   6408
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   5412
         TabIndex        =   9
         Top             =   6408
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   5292
         Left            =   84
         TabIndex        =   22
         Top             =   972
         Width           =   6528
         _ExtentX        =   11509
         _ExtentY        =   9340
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmEndorsementApplicationEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraAttributes"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraEndApp"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmEndorsementApplicationEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).Control(1)=   "cmdClearNotepad"
         Tab(1).ControlCount=   2
         Begin VB.Frame fraEndApp 
            Caption         =   "Endorsement Application"
            Height          =   1464
            Left            =   132
            TabIndex        =   17
            Top             =   384
            Width           =   6264
            Begin VB.ComboBox cboDataField 
               DataField       =   "eventTypeId"
               DataMember      =   "Event"
               Height          =   288
               Index           =   0
               ItemData        =   "frmEndorsementApplicationEdit.frx":0044
               Left            =   1608
               List            =   "frmEndorsementApplicationEdit.frx":0046
               Style           =   2  'Dropdown List
               TabIndex        =   0
               Top             =   300
               Width           =   3588
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "applicationTypeId"
               Height          =   288
               Index           =   0
               Left            =   2724
               TabIndex        =   18
               TabStop         =   0   'False
               Text            =   "applicationTypeId"
               Top             =   24
               Visible         =   0   'False
               Width           =   744
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmEndorsementApplicationEdit.frx":0048
               DataField       =   "receiveDate"
               Height          =   288
               Index           =   0
               Left            =   1596
               TabIndex        =   1
               Top             =   648
               Width           =   1404
               _ExtentX        =   2487
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   50331649
               CurrentDate     =   37088
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmEndorsementApplicationEdit.frx":0053
               DataField       =   "effectiveDate"
               Height          =   288
               Index           =   1
               Left            =   1608
               TabIndex        =   2
               Top             =   984
               Width           =   1404
               _ExtentX        =   2487
               _ExtentY        =   503
               _Version        =   393216
               Format          =   50331649
               CurrentDate     =   37088
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Date &Rec'd:"
               Height          =   192
               Left            =   132
               TabIndex        =   21
               Top             =   684
               Width           =   852
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Effective Date:"
               Height          =   192
               Index           =   8
               Left            =   132
               TabIndex        =   20
               Top             =   1032
               Width           =   1032
            End
            Begin VB.Label Label4 
               Caption         =   "Endorsement Type"
               Height          =   216
               Left            =   132
               TabIndex        =   19
               Top             =   312
               Width           =   1464
            End
         End
         Begin VB.Frame fraAttributes 
            Caption         =   "Attributes"
            Height          =   3060
            Left            =   132
            TabIndex        =   15
            Top             =   2112
            Width           =   6264
            Begin VB.ComboBox cboAttribute 
               Height          =   288
               Left            =   108
               Style           =   2  'Dropdown List
               TabIndex        =   3
               Top             =   300
               Width           =   2304
            End
            Begin VB.TextBox txtAttribute 
               Height          =   288
               Left            =   2592
               TabIndex        =   4
               Top             =   300
               Width           =   3516
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "attributeId"
               Height          =   288
               Index           =   7
               Left            =   4656
               TabIndex        =   16
               TabStop         =   0   'False
               Text            =   "attributeId"
               Top             =   -12
               Visible         =   0   'False
               Width           =   1404
            End
            Begin MSComctlLib.ListView lvAttribute 
               Height          =   2268
               Left            =   96
               TabIndex        =   5
               Top             =   696
               Width           =   6048
               _ExtentX        =   10663
               _ExtentY        =   3995
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   2
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "Name"
                  Object.Width           =   2540
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Value"
                  Object.Width           =   5292
               EndProperty
            End
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74928
            TabIndex        =   7
            Top             =   4908
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   4488
            Left            =   -74916
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   6
            Top             =   312
            Width           =   6360
         End
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Firm Name:"
         Height          =   192
         Left            =   204
         TabIndex        =   24
         Top             =   540
         Width           =   828
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Policy:"
         Height          =   192
         Left            =   204
         TabIndex        =   23
         Top             =   228
         Width           =   480
      End
      Begin VB.Label lbldatafield 
         AutoSize        =   -1  'True
         Caption         =   "policyNumber"
         DataField       =   "ManuallySet"
         DataMember      =   "Policy"
         Height          =   195
         Index           =   1
         Left            =   1185
         TabIndex        =   14
         Top             =   225
         Width           =   960
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   276
      TabIndex        =   13
      Top             =   780
      Width           =   1056
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Application"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator3872 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Contact (Firm)"
            Index           =   0
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Firm"
            Index           =   1
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Application"
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuS4r324 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Firm"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   1
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   2
      End
      Begin VB.Menu mnuSeparatro2823 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact (Firm)"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator8371 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator86732 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&New Application Folder Location..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmEndorsementApplicationEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.11.0603 (jjm)
'previous ver ancient
Option Explicit

Private mApplicationHandle As String
Private mPolicyHandle As String
Private mFirmHandle As String
Private mEventHandle As String
Private mNew As Boolean
Private mDatafields As Collection
Private mDataSet As Dataset
Private mAppTypes As Collection


Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mCancel As Boolean

Private mAttribute As Collection
Private mAttributeValue As Collection

' Internal Id Fields
Private mFirmDataset As Dataset

Private mLeft As Integer
Private mTop As Integer

Private mParameterNames() As String
Private mParameterValues() As String

' Form Manager
Private FormMgr As New FormManager
Private Sub AttributeRefresh()
  lvAttribute.ListItems.Clear
  If mAttributeValue Is Nothing Then
    Exit Sub
  End If
  Dim Row As Collection, Attr As String
  For Each Row In mAttribute
    Attr = Row("attributeId")
    If Contains(mAttributeValue, Attr) Then
      With lvAttribute.ListItems.Add(, Attr, Row("attributeName"))
        .ListSubItems.Add , , mAttributeValue(Attr)
      End With
    End If
  Next Row
End Sub
'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Child is either a Firm
' select (Firm) from cmdFirm or a new firm (NewFirm) from mnuFileNewFirm.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form requested new firm.
    Case fimNew
      mnuFileNewOther_Click 1
      FormRefresh
      Exit Sub
    
    ' Select form selected
    Case fimSelect
      mFirmHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRM_KEY)
      mPolicyHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.POLICY_KEY)
      Prepare
      RefreshPolicyNumber
            
    Case fimSave
      If fInterface.Key = DatabaseName.POLICY_TABLE & "Edit" Then
        mPolicyHandle = fInterface.Child.PolicyHandle
        FormRefresh
      End If
  End Select
  Show
  cboDataField(0).SetFocus
End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub


Private Sub FormRefresh()
   'receiveDate should default to today
   dtpDataField(0) = Today()
  
  If (mNew) Then
     Caption = "New Endorsement Application"
               
     dtpDataField(1) = Today() 'default Endorsement effectiveDate
     
     Prepare
     
     'manually display the policy number
     lblDataField(1).Caption = GetDataFromCollection(Factory.PolicyService.Find(mPolicyHandle, DatabaseName.POLICYNUMBER_FIELD), DatabaseName.POLICYNUMBER_FIELD)
     
     Set mAttributeValue = Nothing
     mEventHandle = vbNullString
     
     FormUtil.SetComboDefault cboDataField(0)
  Else
    Set mDatafields = Factory.ApplicationService.Find(mApplicationHandle)
    SetFields txtDataField, mDatafields
    SetFields dtpDataField, mDatafields
    SetFields cboDataField, mDatafields
    
    If mDatafields(DatabaseName.POLICY_KEY) <> vbNullString Then
      mPolicyHandle = GetDataFromCollection(mDatafields, DatabaseName.POLICY_KEY, True)
      RefreshPolicyNumber
    End If
            
    'mApplicationHandle = mDatafields.item(DatabaseName.APPLICATION_KEY)
    mFirmHandle = mDatafields.item(DatabaseName.FIRM_KEY)
         
    Me.Caption = "Endorsement Application Edit (" & mApplicationHandle & ")"
    
    mEventHandle = mDatafields.item(DatabaseName.EVENT_KEY)
    
    Set mAttributeValue = Factory.EventService.GetAttributes(mEventHandle)
    
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
    
    Dim I As Integer
    Dim EventData As Collection
    Set EventData = Factory.EventService.Find(mEventHandle, DatabaseName.EVENTTYPE_KEY)
    
    If Not EventData Is Nothing Then
      Dim EventTypeId As String
      EventTypeId = EventData(DatabaseName.EVENTTYPE_KEY)
      For I = 1 To mAppTypes.Count
        If mAppTypes(I)(DatabaseName.EVENTTYPE_KEY) = EventTypeId Then
          cboDataField(0).ListIndex = I - 1
          Exit For
        End If
      Next I
    Else
      cboDataField(0).ListIndex = -1
    End If
  End If
   
  mnuFileOpen.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  
  AttributeRefresh
  Dirty = False
End Sub

Public Sub Prepare()
  If mFirmHandle = vbNullString Then Exit Sub

  Dim Model As Collection
  Set Model = Factory.ApplicationService.applicationPrepare(mFirmHandle)
  
  SetFields txtDataField, Model
  SetFields dtpDataField, Model
  SetFields lblDataField, Model
  
End Sub

Private Sub RefreshPolicyNumber()
  'Manually display the policy number. Tags: ManuallySet
  lblDataField(1).Caption = DataUtil.FindHandle(DatabaseName.POLICY_TABLE, DatabaseName.POLICY_KEY, mPolicyHandle, DatabaseName.POLICYNUMBER_FIELD)
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Start custom Code
  
  ' Client side duplication.
  If mNew And Factory.ContactService.FindByOrganization(mFirmHandle, "Firm") Is Nothing Then
    Select Case MsgBox("You can not save a new application without creating a firm contact.  Would you like to create a new firm contact now?", vbYesNo)
      Case vbYes
        mnuFileNewOther_Click 0
        Exit Function
      Case Else
        Exit Function
    End Select
  End If
  
  
  '
  ' Save Data
  '
  
  Dim mDataControls As New Collection
  AddValueControl dtpDataField, mDataControls
  AddTextControl txtDataField, mDataControls
    
  
  'Get the eventTypeId
  Dim EventTypeId As String
  Dim I As Integer
  For I = 1 To mAppTypes.Count
    If mAppTypes(I)("eventTypeName") = cboDataField(0).Text Then
      EventTypeId = mAppTypes(I)(DatabaseName.EVENTTYPE_KEY)
      Exit For
    End If
  Next I
    
  If (mNew) Then
    mApplicationHandle = Factory.ApplicationService.CreateApplication(EventTypeId, mFirmHandle, mPolicyHandle, _
                                                                      GetDataFromCollection(mDataControls, DatabaseName.FIRMNAME_FIELD), _
                                                                      GetDate(GetDataFromCollection(mDataControls, DatabaseName.EFFECTIVEDATE_FIELD)), _
                                                                      GetDouble(GetDataFromCollection(mDataControls, DatabaseName.STAFFCOUNT_FIELD)), _
                                                                      GetDouble(GetDataFromCollection(mDataControls, DatabaseName.POLICYTERM_FIELD)), _
                                                                      GetDate(GetDataFromCollection(mDataControls, DatabaseName.RECEIVEDATE_FIELD)), _
                                                                      txtNote.Text, vbNullString)
    Save = mApplicationHandle <> vbNullString
  Else
'      Debug.Print GetDataFromCollection(mDataControls, DatabaseName.FIRMNAME_FIELD)
'      Debug.Print GetDate(GetDataFromCollection(mDataControls, DatabaseName.EFFECTIVEDATE_FIELD))
'      Debug.Print GetDouble(GetDataFromCollection(mDataControls, DatabaseName.STAFFCOUNT_FIELD))
'      Debug.Print GetDouble(GetDataFromCollection(mDataControls, DatabaseName.POLICYTERM_FIELD))
'      Debug.Print GetDataFromCollection(mDataControls, DatabaseName.RECEIVEDATE_FIELD)
'      Debug.Print txtNote.Text

    Save = Factory.ApplicationService.WriteApplication(mApplicationHandle, EventTypeId, _
                                                       GetDataFromCollection(mDataControls, DatabaseName.FIRMNAME_FIELD), _
                                                       GetDate(GetDataFromCollection(mDataControls, DatabaseName.EFFECTIVEDATE_FIELD)), _
                                                       GetDouble(GetDataFromCollection(mDataControls, DatabaseName.STAFFCOUNT_FIELD)), _
                                                       GetDouble(GetDataFromCollection(mDataControls, DatabaseName.POLICYTERM_FIELD)), _
                                                       GetDataFromCollection(mDataControls, DatabaseName.RECEIVEDATE_FIELD), _
                                                       txtNote.Text)
  End If
  
  If Save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  Dirty = Not Save
End Function

Public Sub ShowNew(Optional PolicyHandle As String, Optional firmHandle As String)
  mFirmHandle = firmHandle
  mPolicyHandle = PolicyHandle
  
  'Set the firm handle by policy if the firm handle is not passed
  If mFirmHandle = vbNullString And mPolicyHandle <> vbNullString Then
    mFirmHandle = Factory.PolicyService.Find(mPolicyHandle, DatabaseName.FIRM_KEY)(DatabaseName.FIRM_KEY)
  End If
  
  mNew = True
  Show
  
  ' ToDo:  Should be an ini setting.
  cboDataField(0).ListIndex = 0
  Dirty = False

  If mPolicyHandle = vbNullString Then
    cmdPolicy.SetFocus
    cmdPolicy_Click
  Else
    cboDataField(0).SetFocus
  End If
End Sub

Public Sub ShowByPrimaryKey(ApplicationId As String)
  mApplicationHandle = ApplicationId
  Show
End Sub

Private Sub cboAttribute_Click()
  txtDataField(7).Text = mAttribute(cboAttribute.ListIndex + 1)("attributeId")
  If Contains(mAttributeValue, txtDataField(7).Text) Then
    txtAttribute.Text = mAttributeValue(txtDataField(7).Text)
  Else
    txtAttribute.Text = vbNullString
  End If
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Open up a select form for policy.  The response to the select
' is processed under FormChildUnload.
'
Private Sub cmdPolicy_Click()
  Dispatcher.ShowSearch DatabaseName.POLICY_TABLE, FormMgr, True, , 0, mFirmHandle
End Sub

Private Sub cmdSave_Click()
  Dim WasNew As Boolean
  WasNew = mNew
  If Save Then
    FormMgr.Message = fimSave
    
    FormUtil.SetComboDefault cboDataField(0)
    
    If TypeOf FormMgr.Parent Is frmEndorsementApplicationOpen Then
      Unload Me
      Exit Sub
    End If
    
    Unload Me
    
    If WasNew Then
      ' DON'T register FormMgr.
      FormUtil.ShowByPrimaryKey New frmEndorsementApplicationOpen, mApplicationHandle
    End If
  End If
End Sub

Private Sub Command1_Click()
  MsgBox VarType(dtpDataField(0).Value)
  MsgBox VariantTypeConstants.vbVDate
  
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_Initialize()
  Set mDataSet = New ApplicationDataset
  Set mDataSet.Session = LawSession
   
  Set mFirmDataset = New FirmViewDataset
  Set mFirmDataset.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyF
        mnuFileNewOther_Click 1
        KeyCode = 0
    End Select
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  'Custom Accelerators
  KeyPreview = True
  mnuFileNewOther(1).Caption = mnuFileNewOther(1).Caption + vbTab + "Ctrl+Shift+F"
  
  FormUtil.EditLoad Me
  FormMgr.Load Me
 
'  Factory.FolderParameterService.ParameterLoad DatabaseName.APPLICATION_TABLE, lawActionNew, mParameterNames, mParameterValues
  
  Set mAttribute = Factory.AttributeService.NameCollection()
  FormUtil.ComboBoxLoad mAttribute, cboAttribute, "attributeName"
  
  Set mAppTypes = Factory.EventTypeService.GroupCollection(DatabaseName.APPENDORSEMENTEDIT_RULE)
  FormUtil.ComboBoxLoad mAppTypes, cboDataField(0), "eventTypeName"
    
  FormRefresh
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = True
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Resize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvAttribute_Click()
  If Not lvAttribute.SelectedItem Is Nothing Then
    txtDataField(7).Text = lvAttribute.SelectedItem.Key
  End If
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  If mFirmHandle = vbNullString Then
    MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
    Exit Sub
  End If
  Select Case Index
    Case 0
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowByFirm mFirmHandle, "Firm"
    Case 1
      FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mFirmHandle, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mApplicationHandle = vbNullString
  mFirmHandle = vbNullString
  FormRefresh
  cmdPolicy.SetFocus
  cmdPolicy_Click
End Sub

Private Sub mnuFileNewOther_Click(Index As Integer)
  Select Case Index
    Case 0
      If mFirmHandle = vbNullString Then
        MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
        Exit Sub
      End If
      Dim Contacts As Collection
      Set Contacts = Factory.ContactService.FindByFirm(mFirmHandle, "Firm")
      If Contacts Is Nothing Then
        Dim fContact As New frmContactEdit
        FormMgr.Add fContact
        fContact.ShowNewByOrganization mFirmHandle, GetDataFromCollection(Factory.EntityService.Find(mFirmHandle, DatabaseName.MAINLOCATIONID_FIELD), DatabaseName.MAINLOCATIONID_FIELD)
        'fContact.ShowNewByOrganization mFirmHandle, Factory.FirmService.Find(mFirmHandle, """locationId""")(DatabaseName.LOCATION_KEY)
      Else
        MsgBox "Contact record already exists for this firm."
        Exit Sub
      End If
    Case 1
      FormUtil.ShowNew New frmOrganizationEdit, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub

'
' Check for changes and open application
'
Private Sub mnuFileOpen_Click()
  FormUtil.ShowByPrimaryKey New frmApplicationOpen, mApplicationHandle, FormMgr
End Sub

Private Sub mnuFileOpenOther_Click(Index As Integer)
  Select Case Index
    Case 0
      If mFirmHandle = vbNullString Then
        MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.APPLICATION_TABLE, mApplicationHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.APPLICATION_REF, DatabaseName.APPLICATION_TABLE, mApplicationHandle, lblDataField(2).Caption
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub


Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
'  FolderUtil.WorkflowShow DatabaseName.APPLICATION_TABLE, lawActionNew, _
'      mParameterNames, mParameterValues

  Dim fFolderList As New frmFolderListDialog
  'read files from ini and load into the list view
  With fFolderList.lvFolder
    .ListItems.Add , "Endorsement", "Endorsement"
    .ListItems("Endorsement").ListSubItems.Add , "Location", IniMain.ReadFile("application", "endorsementfolder")
  End With
  
  ListViewModule.AutosizeWidth fFolderList.lvFolder
  
  fFolderList.Show vbModal, Me
  
  If fFolderList.Ok Then
    'write folders to ini
    IniMain.WriteFile "application", "endorsementfolder", fFolderList.lvFolder.ListItems("Endorsement").ListSubItems("Location")
  End If
  
  Unload fFolderList

End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub txtAttribute_GotFocus()
  FormUtil.EditTextSelect txtAttribute
End Sub

Private Sub txtAttribute_LostFocus()
  If txtDataField(7).Text = vbNullString Then
    If txtAttribute.Text <> vbNullString Then
      MsgBox "Must select an attribute first."
      txtAttribute.Text = vbNullString
    End If
    Exit Sub
  End If
  
  If Contains(mAttributeValue, txtDataField(7).Text) Then
    If mAttributeValue(txtDataField(7).Text) = txtAttribute.Text Then
      Exit Sub
    End If
    mAttributeValue.Remove txtDataField(7).Text
  End If
  
  If (txtAttribute.Text <> vbNullString) Then
    If mAttributeValue Is Nothing Then
      Set mAttributeValue = New Collection
    End If
    mAttributeValue.Add txtAttribute.Text, txtDataField(7).Text
  End If
  
  AttributeRefresh
  Dirty = True
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Select Case Index
    Case 7
      cboAttribute.ListIndex = CollectionFind(mAttribute, "attributeId", txtDataField(Index).Text) - 1
  End Select
  
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
