VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPolicyAccountingEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Policy Accounting Edit"
   ClientHeight    =   5844
   ClientLeft      =   6432
   ClientTop       =   4296
   ClientWidth     =   7248
   Icon            =   "frmPolicyPaymentEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5844
   ScaleWidth      =   7248
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5415
      Left            =   0
      ScaleHeight     =   5412
      ScaleWidth      =   7248
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   396
      Width           =   7245
      Begin VB.Frame frmPolicy 
         Caption         =   "Policy"
         Height          =   1455
         Left            =   72
         TabIndex        =   52
         Top             =   90
         Width           =   7095
         Begin VB.CommandButton cmdPolicy 
            Caption         =   "&Policy"
            Height          =   288
            Left            =   5655
            TabIndex        =   53
            Top             =   264
            Width           =   1000
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "firmName"
            DataField       =   "firmName"
            DataMember      =   "Policy"
            Height          =   195
            Index           =   1
            Left            =   750
            TabIndex        =   78
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   690
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "policyNumber"
            DataField       =   "policyNumber"
            DataMember      =   "Policy"
            Height          =   195
            Index           =   2
            Left            =   735
            TabIndex        =   77
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   960
         End
         Begin VB.Label Label18 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Write Off"
            Height          =   195
            Index           =   0
            Left            =   5475
            TabIndex        =   76
            Top             =   870
            Width           =   600
         End
         Begin VB.Label Label22 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Premium"
            Height          =   195
            Index           =   0
            Left            =   180
            TabIndex        =   75
            Top             =   870
            Width           =   630
         End
         Begin VB.Label Label24 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Adjust"
            Height          =   195
            Index           =   0
            Left            =   1215
            TabIndex        =   74
            Top             =   870
            Width           =   450
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fin/Late"
            Height          =   195
            Index           =   0
            Left            =   4530
            TabIndex        =   73
            Top             =   870
            Width           =   600
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Receive"
            Height          =   195
            Index           =   1
            Left            =   1920
            TabIndex        =   72
            Top             =   870
            Width           =   615
         End
         Begin VB.Label Label13 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Transfer"
            Height          =   195
            Index           =   0
            Left            =   3585
            TabIndex        =   71
            Top             =   870
            Width           =   600
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "transfer"
            DataField       =   "transfer"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   7
            Left            =   3660
            TabIndex        =   70
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   525
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "receiveAmount"
            DataField       =   "receiveAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   8
            Left            =   1455
            TabIndex        =   69
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   1080
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "financeCharge"
            DataField       =   "financeCharge"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   9
            Left            =   4095
            TabIndex        =   68
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   1050
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "premium"
            DataField       =   "premium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   12
            Left            =   195
            TabIndex        =   67
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   630
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "premiumAdjust"
            DataField       =   "premiumAdjust"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   13
            Left            =   585
            TabIndex        =   66
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   1065
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "writeOff"
            DataField       =   "writeOff"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   10
            Left            =   5550
            TabIndex        =   65
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   510
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "returnAmount"
            DataField       =   "returnAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   0
            Left            =   2400
            TabIndex        =   64
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   930
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Return"
            Height          =   195
            Index           =   2
            Left            =   2865
            TabIndex        =   63
            Top             =   870
            Width           =   465
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "dueAmount"
            DataField       =   "dueAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   3
            Left            =   6240
            TabIndex        =   62
            Top             =   1110
            UseMnemonic     =   0   'False
            Width           =   810
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Due"
            Height          =   195
            Index           =   3
            Left            =   6630
            TabIndex        =   61
            Top             =   870
            Width           =   300
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Policy:"
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   60
            Top             =   285
            Width           =   480
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            DataMember      =   "Policy"
            Height          =   195
            Index           =   5
            Left            =   2535
            TabIndex        =   59
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   945
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Effective:"
            Height          =   195
            Index           =   10
            Left            =   1815
            TabIndex        =   58
            Top             =   285
            Width           =   645
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Name:"
            Height          =   195
            Index           =   11
            Left            =   120
            TabIndex        =   57
            Top             =   525
            Width           =   480
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "Policy paymentMethod"
            DataField       =   "paymentMethod"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   6
            Left            =   4815
            TabIndex        =   56
            Top             =   0
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   1650
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Status:"
            Height          =   195
            Left            =   3795
            TabIndex        =   55
            Top             =   285
            Width           =   480
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventStatus"
            DataField       =   "eventStatus"
            DataMember      =   "Event"
            Height          =   195
            Index           =   4
            Left            =   4350
            TabIndex        =   54
            Top             =   285
            Width           =   840
         End
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   3420
         TabIndex        =   46
         Top             =   4950
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   4695
         TabIndex        =   47
         Top             =   4950
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         CausesValidation=   0   'False
         Height          =   336
         Left            =   5970
         TabIndex        =   48
         Top             =   4950
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3135
         Left            =   75
         TabIndex        =   49
         Top             =   1695
         Width           =   7095
         _ExtentX        =   12510
         _ExtentY        =   5525
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   5
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmPolicyPaymentEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label5"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label2"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label4(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label4(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Label12"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblBalance"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblDebits"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblCredits"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "Label6"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblEntryDate"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtpDataField(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lvGL(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lvGL(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "cmdAdd"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "cmdRemove"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "cmdProperties"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "optGL(0)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "optGL(1)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "cmdDefaults"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtDataField(5)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "cboEventTypeName"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).ControlCount=   23
         TabCaption(1)   =   "&Installment"
         TabPicture(1)   =   "frmPolicyPaymentEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "&Advanced"
         TabPicture(2)   =   "frmPolicyPaymentEdit.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Label13(1)"
         Tab(2).Control(1)=   "lblLabel(4)"
         Tab(2).Control(2)=   "Label16(1)"
         Tab(2).Control(3)=   "Label24(1)"
         Tab(2).Control(4)=   "lblLabel(7)"
         Tab(2).Control(5)=   "Label18(1)"
         Tab(2).Control(6)=   "lblLabel(5)"
         Tab(2).Control(7)=   "lblLabel(6)"
         Tab(2).Control(8)=   "lblLabel(8)"
         Tab(2).Control(9)=   "txtDataField(1)"
         Tab(2).Control(10)=   "txtDataField(2)"
         Tab(2).Control(11)=   "txtDataField(3)"
         Tab(2).Control(12)=   "txtDataField(4)"
         Tab(2).Control(13)=   "txtDataField(6)"
         Tab(2).Control(14)=   "txtDataField(7)"
         Tab(2).Control(15)=   "txtDataField(8)"
         Tab(2).Control(16)=   "txtDataField(9)"
         Tab(2).ControlCount=   17
         TabCaption(3)   =   "N&ote"
         TabPicture(3)   =   "frmPolicyPaymentEdit.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "txtNote"
         Tab(3).Control(1)=   "cmdClearNotepad"
         Tab(3).ControlCount=   2
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74916
            TabIndex        =   27
            Top             =   2508
            Width           =   1028
         End
         Begin VB.ComboBox cboEventTypeName 
            Height          =   315
            ItemData        =   "frmPolicyPaymentEdit.frx":007C
            Left            =   1548
            List            =   "frmPolicyPaymentEdit.frx":007E
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Tag             =   "PolicyAccountingEditEventTypeName"
            Top             =   564
            Width           =   3264
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            DataField       =   "eventAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Event"
            Height          =   288
            Index           =   5
            Left            =   5028
            TabIndex        =   2
            Top             =   564
            Width           =   1488
         End
         Begin VB.CommandButton cmdDefaults 
            Caption         =   "De&faults"
            Height          =   288
            Left            =   5508
            TabIndex        =   9
            Top             =   2280
            Width           =   1000
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "dueAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   9
            Left            =   -68892
            Locked          =   -1  'True
            TabIndex        =   44
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "writeOff"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   8
            Left            =   -69762
            Locked          =   -1  'True
            TabIndex        =   42
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "financeCharge"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   7
            Left            =   -70619
            Locked          =   -1  'True
            TabIndex        =   41
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "transfer"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   6
            Left            =   -71476
            Locked          =   -1  'True
            TabIndex        =   40
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "returnAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   4
            Left            =   -72333
            Locked          =   -1  'True
            TabIndex        =   39
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "receiveAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   3
            Left            =   -73190
            Locked          =   -1  'True
            TabIndex        =   38
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "premiumAdjust"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   2
            Left            =   -74047
            Locked          =   -1  'True
            TabIndex        =   37
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "premium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00;(#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "PolicyAccounting"
            Height          =   288
            Index           =   1
            Left            =   -74904
            Locked          =   -1  'True
            TabIndex        =   36
            Top             =   864
            Width           =   828
         End
         Begin VB.TextBox txtNote 
            Height          =   2052
            Left            =   -74916
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   26
            Top             =   372
            Width           =   6792
         End
         Begin VB.OptionButton optGL 
            Caption         =   "Effective"
            Height          =   240
            Index           =   1
            Left            =   84
            TabIndex        =   4
            Top             =   1836
            Visible         =   0   'False
            Width           =   924
         End
         Begin VB.OptionButton optGL 
            Caption         =   "Received"
            Height          =   240
            Index           =   0
            Left            =   84
            TabIndex        =   3
            Top             =   1572
            Visible         =   0   'False
            Width           =   996
         End
         Begin VB.CommandButton cmdProperties 
            Caption         =   "&Properties"
            Height          =   288
            Left            =   5508
            TabIndex        =   8
            Top             =   1932
            Width           =   1000
         End
         Begin VB.CommandButton cmdRemove 
            Caption         =   "Re&move"
            Height          =   288
            Left            =   5508
            TabIndex        =   7
            Top             =   1596
            Width           =   1000
         End
         Begin VB.CommandButton cmdAdd 
            Caption         =   "A&dd"
            Height          =   288
            Left            =   5508
            TabIndex        =   6
            Top             =   1260
            Width           =   1000
         End
         Begin MSComctlLib.ListView lvGL 
            Height          =   1356
            Index           =   0
            Left            =   1080
            TabIndex        =   5
            Tag             =   "ColumnFixed"
            Top             =   1248
            Width           =   4344
            _ExtentX        =   7684
            _ExtentY        =   2413
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "glAccountId"
               Text            =   "Account"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "transactionName"
               Text            =   "Name"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "transactionReference"
               Text            =   "Reference"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "transactionAmount"
               Text            =   "Amount"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "~debit"
               Text            =   "Debit"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "~credit"
               Text            =   "Credit"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView lvGL 
            Height          =   1356
            Index           =   1
            Left            =   1080
            TabIndex        =   25
            Tag             =   "ColumnFixed"
            Top             =   1248
            Width           =   4344
            _ExtentX        =   7684
            _ExtentY        =   2413
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "glAccountId"
               Text            =   "Account"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "transactionName"
               Text            =   "Name"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "transactionReference"
               Text            =   "Reference"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "transactionAmount"
               Text            =   "Amount"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "~debit"
               Text            =   "Debit"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "~credit"
               Text            =   "Credit"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2400
            Left            =   -74928
            TabIndex        =   32
            Tag             =   "PolicyAccountingInstallment"
            Top             =   360
            Width           =   6816
            _ExtentX        =   12002
            _ExtentY        =   4233
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            Checkboxes      =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmPolicyPaymentEdit.frx":0080
            DataField       =   "eventDate"
            DataMember      =   "Event"
            Height          =   288
            Index           =   0
            Left            =   168
            TabIndex        =   0
            Top             =   564
            Width           =   1212
            _ExtentX        =   2138
            _ExtentY        =   508
            _Version        =   393216
            Format          =   73007105
            CurrentDate     =   37118
         End
         Begin VB.Label lblEntryDate 
            AutoSize        =   -1  'True
            Caption         =   "entryDate"
            Height          =   192
            Left            =   1080
            TabIndex        =   51
            Top             =   984
            Width           =   696
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Entry Date:"
            Height          =   192
            Left            =   156
            TabIndex        =   50
            Top             =   984
            Width           =   780
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Note:  Amounts are only updated after a Save."
            Height          =   192
            Index           =   8
            Left            =   -74868
            TabIndex        =   45
            Top             =   1584
            Width           =   3264
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            Caption         =   "Due"
            Height          =   192
            Index           =   6
            Left            =   -68868
            TabIndex        =   43
            Top             =   600
            Width           =   780
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            Caption         =   "Return"
            Height          =   192
            Index           =   5
            Left            =   -72306
            TabIndex        =   35
            Top             =   600
            Width           =   780
         End
         Begin VB.Label Label18 
            Alignment       =   1  'Right Justify
            Caption         =   "Write Off"
            Height          =   192
            Index           =   1
            Left            =   -69732
            TabIndex        =   34
            Top             =   600
            Width           =   780
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            Caption         =   "Premium"
            Height          =   192
            Index           =   7
            Left            =   -74880
            TabIndex        =   33
            Top             =   600
            Width           =   780
         End
         Begin VB.Label Label24 
            Alignment       =   1  'Right Justify
            Caption         =   "Adjust"
            Height          =   192
            Index           =   1
            Left            =   -74022
            TabIndex        =   31
            Top             =   600
            Width           =   780
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            Caption         =   "Fin/Late"
            Height          =   192
            Index           =   1
            Left            =   -70590
            TabIndex        =   30
            Top             =   600
            Width           =   780
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            Caption         =   "Receive"
            Height          =   192
            Index           =   4
            Left            =   -73164
            TabIndex        =   29
            Top             =   600
            Width           =   780
         End
         Begin VB.Label Label13 
            Alignment       =   1  'Right Justify
            Caption         =   "Transfer"
            Height          =   192
            Index           =   1
            Left            =   -71448
            TabIndex        =   28
            Top             =   600
            Width           =   780
         End
         Begin VB.Label lblCredits 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   288
            Left            =   2820
            TabIndex        =   23
            Top             =   2688
            UseMnemonic     =   0   'False
            Width           =   1404
         End
         Begin VB.Label lblDebits 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   288
            Left            =   696
            TabIndex        =   22
            Top             =   2688
            UseMnemonic     =   0   'False
            Width           =   1404
         End
         Begin VB.Label lblBalance 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   288
            Left            =   5184
            TabIndex        =   21
            Top             =   2688
            UseMnemonic     =   0   'False
            Width           =   1404
         End
         Begin VB.Label Label12 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Balance"
            Height          =   192
            Left            =   4488
            TabIndex        =   20
            Top             =   2736
            Width           =   600
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Credits"
            Height          =   192
            Index           =   1
            Left            =   2232
            TabIndex        =   19
            Top             =   2736
            Width           =   504
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Debits"
            Height          =   192
            Index           =   0
            Left            =   132
            TabIndex        =   18
            Top             =   2736
            Width           =   468
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Received"
            Height          =   192
            Left            =   168
            TabIndex        =   17
            Top             =   324
            Width           =   708
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Policy Accounting Event"
            Height          =   192
            Left            =   1560
            TabIndex        =   16
            Top             =   324
            Width           =   1716
         End
         Begin VB.Label lblLabel 
            Caption         =   "Accounting:"
            Height          =   252
            Index           =   0
            Left            =   156
            TabIndex        =   15
            Top             =   1284
            Width           =   936
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Cash Amount"
            Height          =   192
            Left            =   5568
            TabIndex        =   14
            Top             =   312
            Width           =   948
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   168
         Top             =   2376
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   624
         Top             =   2424
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   7245
      _ExtentX        =   12785
      _ExtentY        =   699
      BandCount       =   1
      _CBWidth        =   7248
      _CBHeight       =   396
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   336
         ScaleWidth      =   7128
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   30
         Width           =   7125
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   324
            Left            =   72
            TabIndex        =   13
            Top             =   60
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   572
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   288
      Left            =   0
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   588
      Visible         =   0   'False
      Width           =   708
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator342 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuSeparator0834 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator876 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuSeperator564 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator984 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsAccountingNotepad 
         Caption         =   "&Accounting Note"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeparator3512 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmPolicyAccountingEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.73.00 (jjm) REF 242
'ver 2.71.00 (jjm) 2005 02 01
'ver 2.70.01 (jjm) 2004 12 27
'previous ver 2.60.00 (jjm) 2004 04 01
'previous ver 2.50.00 (jjm) 2004 02 18
'previous ver 2.40.1117 (jjm)
'previous ver 2.10.0507 (jjm)
'previous ver 2.10.0414 (jjm)
'previous ver 2.10.0326 (jjm)

Option Explicit

Private mPolicyAccountingHandle As String
Private mPolicyHandle As String
Private mCoverageHandle As String
Private mEventHandle As String
Private mInvoiceHandles() As String
Private mEventTypeHandle As String
Private mPolicyEventTypeHandle As String

Private mNew As Boolean
Private mListView As ListView
Private mListIndex As Integer
Private mListItemType As String

Private mGLIndex As Integer
Private mGLTransaction As Dataset
Private mInvoice As Dataset
Private mReferenceView As Dataset

Private mEventTypes As collection

Private mParameterNames() As String
Private mParameterValues() As String

' Either MS or BI.
Private mSourceType As String

' Needed for binding. This boolean works as a flag
' through out the form. It is set in two places, the
' ShowFromCoverage() sub routine and the edit (already
' an existing entry)section of FormRefresh()
Private mIsBinding As Boolean

'This flags if the system will check to see if the
'multiple GL entries are needed. This needs to be set
'to true in order for the optGL options to be displayed.
'This value is either set in ShowFromCoverage() or the
'edit (already an existing entry) section of FormRefresh().
Private mCheckForMultipleEntries As Boolean

'Flags an entry for a Policy Extension Endorsement
Private mIsPolicyExtension As Boolean

Private mEffectiveDate As Date
Private mPremium As Double

Private mReissueApp As Boolean

Private mDefault As Boolean
Private mDirty As Boolean
Public FormMgr As New FormManager

'Private mQuoteCoverageProvider As DataProvider

'ver 2.50.00 (jjm) 2004 02 18
'Used To display the GL Entry Date
Private mGLEntryDate(0 To 1) As String
Private mGLEntryDataset As Dataset

'2.70.01 (jjm) 2004 12 27
'Account for suspended entries
Private BindingSuspenseAccount As String

'Constants
Private Const DTP_PAYMENTDATE_INDEX = 0
Private Const TXT_PAYMENTAMOUNT_INDEX = 5


Private Sub CoverageRefresh()
  If mCoverageHandle = vbNullString Then
    Exit Sub
  End If
  
  Dim Coverage As collection, Application As collection, Limit As collection
  Set Coverage = Factory.CoverageService.Find(mCoverageHandle)
  If Coverage Is Nothing Then
    MsgBox "Unexpected condition.  Associated coverage (" & mCoverageHandle & _
        ") can not be found."
    Exit Sub
  End If
  
  Set Application = Factory.ApplicationService.Find(GetDataFromCollection(Coverage, DatabaseName.APPLICATION_KEY))
  mEffectiveDate = GetDataFromCollection(Application, DatabaseName.EFFECTIVEDATE_FIELD)
  
  'Get premium from Policy, otherwise get premium from QuoteCoverage record
  If mPolicyHandle = vbNullString Then
    lblDataField(5).Caption = mEffectiveDate
    lblDataField(1).Caption = GetDataFromCollection(Application, DatabaseName.FIRMNAME_FIELD)
  End If
  
  'Get Premium from QuoteCoverage
  Dim quoteCoverageHandle As String
  quoteCoverageHandle = GetDataFromCollection(Coverage, DatabaseName.QUOTECOVERAGE_KEY)
  mPremium = GetDouble(DataUtil.FindHandle(DatabaseName.QUOTECOVERAGE_TABLE, DatabaseName.QUOTECOVERAGE_KEY, quoteCoverageHandle, DatabaseName.PREMIUM_FIELD))
  
'  mQuoteCoverageProvider.Filter = LawDialect.EqualString(DatabaseName.QUOTECOVERAGE_KEY, _
'                                                        GetDataFromCollection(Coverage, DatabaseName.QUOTECOVERAGE_KEY))
'  mQuoteCoverageProvider.OpenRecordset
'  Set Limit = mQuoteCoverageProvider.Collection
'  If Not Limit Is Nothing Then
'    mPremium = GetDouble(GetDataFromCollection(Limit, DatabaseName.PREMIUM_FIELD, True))
'  End If
  
  'ToDo Comp w/1.01(middle Tier) - Remove appType (min 1.10) -- Use event type group method
  '                              - Keep mApplicationEventType
  Dim appEventType As String
  appEventType = GetDataFromCollection(Factory.EventService.Find(Application(DatabaseName.EVENT_KEY), DatabaseName.EVENTTYPE_KEY), DatabaseName.EVENTTYPE_KEY)
  mReissueApp = Factory.EventService.IsEventTypeInGroup(appEventType, DatabaseName.APPREISSUE_RULE)
  
  mIsPolicyExtension = (appEventType = DatabaseName.APP_POLICYEXTENSION_TYPE)
  
End Sub


'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form selected
    Case fimSelect
      mPolicyHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.POLICY_KEY)
      PolicyRefresh

  End Select
  Show
  dtpDataField(DTP_PAYMENTDATE_INDEX).SetFocus
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  lvView.ListItems.Clear
  mInvoiceHandles = vbNullStringArray
  
  Dim Datafields As collection
    
  ClearDataField txtDataField
  ClearDataField lblDataField
  
  
  If (mNew) Then
    ClearDataField txtDataField
    ClearDataField lblDataField
        
    dtpDataField(DTP_PAYMENTDATE_INDEX).Value = Today
    
    mEventHandle = vbNullString

    Caption = "New Policy Accounting"
    
    'ver 2.40.1114 (jjm) wilmic
    mEventTypeHandle = "PolPay-MC"
    
    'ver 2.50.00 (jjm) 2004 02 18
    lblEntryDate = vbNullString
           
  Else  'Existing Entry
    Set Datafields = Factory.PolicyAccountingService.Find(mPolicyAccountingHandle)
    If Datafields Is Nothing Then
      MsgBox "Policy Accounting record not found.", vbExclamation
      Unload Me
      Exit Sub
    End If
    
    mPolicyHandle = Datafields(DatabaseName.POLICY_KEY)
    mCoverageHandle = Datafields(DatabaseName.COVERAGE_KEY)
    
    mEventHandle = Datafields(DatabaseName.EVENT_KEY)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
      mInvoiceHandles = Split(Factory.EventService.GetAttribute(mEventHandle, "installmentIds"), ",")
    End If
    
    Dim EventData As collection
    Set EventData = Factory.EventService.Find(mEventHandle)
    If Not EventData Is Nothing Then
      mEventTypeHandle = EventData(DatabaseName.EVENTTYPE_KEY)
      SetFields lblDataField, EventData, "Event"
      SetFields dtpDataField, EventData, "Event"
      SetFields txtDataField, EventData, "Event"
    End If
    
    'Set the binding boolean. For new Policy Accounting entries this is set based on which
    'show new is passed
    mIsBinding = Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, "PolicyBinding")
     
    'check to see if there is a second GL entry.
    Dim entrycount As Integer
    mGLTransaction.DataProvider.filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle) & " and " & _
                                         LawDialect.EqualString(DatabaseName.SOURCETYPE_FIELD, "PE")
    mGLTransaction.DataProvider.OpenRecordset
    If Not mGLTransaction.DataProvider.collection Is Nothing Then
      entrycount = mGLTransaction.DataProvider.collection.Count
    End If
    
    'Setting mCheckForMultipleEntries allows a future check to determine if there
    'is a need to display the option contorls to switch between multiple entries.
    If (mIsBinding Or (entrycount > 1)) Then
      mCheckForMultipleEntries = True
    End If
    
    SetFields txtDataField, Datafields, "PolicyAccounting"
    SetFields dtpDataField, Datafields, "PolicyAccounting"
    
    Caption = "Edit Policy Accounting (" & mPolicyAccountingHandle & ")"
  End If
  
  cboEventTypeName.Clear
  Set mEventTypes = Factory.EventTypeService.GroupCollection(IIf(mIsBinding, "PolicyBinding", "PolicyMisc"))
  FormUtil.ComboBoxLoad mEventTypes, cboEventTypeName, DatabaseName.EVENTTYPENAME_FIELD

  'It is possible to set the combobox to the mEventTypeHandle even if it is new
  If Not mEventTypes Is Nothing Then
    Dim I As Integer, EventTypeItem As String
    
    'Set the default item by the event type group if there is no current eventTypeId
    EventTypeItem = IIf(mEventTypeHandle = vbNullString, GetDefaultEventTypeId, mEventTypeHandle)
        
    'Search the item for the event type and set the comboBox to that item
    For I = 1 To mEventTypes.Count
      If mEventTypes(I)(DatabaseName.EVENTTYPE_KEY) = EventTypeItem Then
        cboEventTypeName.ListIndex = I - 1
        Exit For
      End If
    Next I
  Else
    'Set the combobox index to -1 if there is nothing available
    cboEventTypeName.ListIndex = IIf(cboEventTypeName.ListCount > 0, 0, -1)
  End If

  
  
  'Set whether or not defaults were run for GL.
  mDefault = Not mNew
  
  ' Set source type
  mSourceType = IIf(mIsBinding, "BI", "MS")
  
  ' Refresh any Policy associated data.
  PolicyRefresh
  
  ' Refresh any Coverage associated data (used for binding).
  CoverageRefresh
  
  ' Refresh date to see if we have multiple entries (binding).
  dtpDataField_Change DTP_PAYMENTDATE_INDEX
  
  ' Send To
  mnuFileSendToFolder.Enabled = Not mNew
  
  'GL update
  GLRefresh
  
  'ver 2.50.00 (jjm) 2004 02 18
  'Set the initial gl date
  If Not mNew Then lblEntryDate = mGLEntryDate(mGLIndex)
  
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' Set buttons and change status.
  Dirty = False
End Sub


' Enter/Update the payment amount
'
Private Sub GLAmount(Amount As Double)
  Dim item As ListItem
  
  If optGL(1).Visible Then
    'setup supsense for now
    Set item = lvGL(0).FindItem(BindingSuspenseAccount)
    If item Is Nothing Then
      GLTransactionAdd lvGL(0), _
        BindingSuspenseAccount, vbNullString, vbNullString, 0 - Amount
    Else
      GLTransactionEdit item, _
        BindingSuspenseAccount, vbNullString, vbNullString, 0 - Amount
    End If
    
    'correction of suspense in the future
    Set item = lvGL(1).FindItem(BindingSuspenseAccount)
    If item Is Nothing Then
      GLTransactionAdd lvGL(1), _
        BindingSuspenseAccount, vbNullString, vbNullString, Amount
    Else
      GLTransactionEdit item, _
        BindingSuspenseAccount, vbNullString, vbNullString, Amount
    End If
  End If
  
  '4099: Policy Clearance Account
  Set item = lvGL(0).FindItem("4099")
  If item Is Nothing Then
    GLTransactionAdd lvGL(0), _
      "4099", vbNullString, vbNullString, Amount
  Else
    GLTransactionEdit item, _
      "4099", vbNullString, vbNullString, Amount
  End If
    
  GLSummary
End Sub

'
' Currently not used as the default eventTypeId is hard coded in FormRefresh->If isNew
'
Private Function GetDefaultEventTypeId() As String
  GetDefaultEventTypeId = IIf(mIsBinding, _
                              IniMain.ReadFile(UrlName.MISC_INI, "PolAccountingEventType"), _
                              IniMain.ReadFile(UrlName.MISC_INI, "PolAccountingEventTypeBinding"))
End Function

Private Sub GLDefault()
  ' if already have done defaults
  If mDefault Then
    Exit Sub
  End If
  mDefault = True
  
  Dim Cash As Double
  Cash = GetDouble(txtDataField(TXT_PAYMENTAMOUNT_INDEX).Text)
  
  
  'Mantis 2791 - Policy Extension Hack
  If mIsPolicyExtension Then
    GLTransactionAdd lvGL(0), "4013", vbNullString, vbNullString, 0 - Cash
    GLAmount Cash
    Exit Sub
  End If
  
  
  ' Binding Policy Accounting
  
  If mIsBinding Then
    Dim Entry As Integer
    Entry = IIf(optGL(1).Visible, 1, 0)
    '4000: Premium for New Business, 4005: Premium for Reissues
    GLTransactionAdd lvGL(Entry), IIf(mReissueApp, "4005", "4000"), vbNullString, vbNullString, 0 - mPremium
    '1250: due from insrd QPP, 1250 due from insrd
    'Bug #1891
    If Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, DatabaseName.QP_RULE) Then
        GLTransactionAdd lvGL(Entry), "1250", vbNullString, vbNullString, mPremium - Cash
    ElseIf Factory.EventService.IsEventTypeInGroup(mEventTypeHandle, DatabaseName.MP_RULE) Then
        GLTransactionAdd lvGL(Entry), "1255", vbNullString, vbNullString, mPremium - Cash
    End If
    
    GLAmount Cash
    Exit Sub
  End If
  
  
  ' Post-Binding Policy Accounting
  
  Dim item As ListItem, LateCharge As Double, FinanceCharge As Double, DueAmount As Double
  For Each item In lvView.ListItems
    If item.Checked And ListItemText(item, DatabaseName.COMPLETEDATE_FIELD) = vbNullString Then
      DueAmount = DueAmount + GetDouble(ListItemText(item, DatabaseName.PREMIUM_FIELD))
      FinanceCharge = FinanceCharge + GetDouble(ListItemText(item, DatabaseName.FINANCECHARGE_FIELD))
      LateCharge = LateCharge + GetDouble(ListItemText(item, DatabaseName.LATECHARGE_FIELD))
    End If
  Next item
  
  
  'ver 1.90.0103  (srd)
  Dim paymentMethod As String
  paymentMethod = "PolPay-" + lblDataField(6).Caption
   
  ' Select on policy binding
  Select Case paymentMethod
    Case "PolPay-QP":
      'ver 2.71.00 (jjm) 2005 02 01 Ref #22
      GLTransactionAdd lvGL(0), "1250", vbNullString, vbNullString, 0 - DueAmount '0 - Cash + FinanceCharge + LateCharge
      If (FinanceCharge <> 0) Then
        GLTransactionAdd lvGL(0), "4200", vbNullString, vbNullString, 0 - FinanceCharge
      End If
      If (LateCharge <> 0) Then
        GLTransactionAdd lvGL(0), "4205", vbNullString, vbNullString, 0 - LateCharge
      End If
      Dirty = True
  End Select
  
  ' Set cash amount and summarize.
  GLAmount Cash
End Sub


Private Sub GLClear()
  'ver  2.50.00 (jjm) 2004 02 18
  mGLEntryDate(0) = vbNullString
  mGLEntryDate(1) = vbNullString
  
  lvGL(0).ListItems.Clear
  lvGL(1).ListItems.Clear
  FormUtil.GLTransactionSummary lvGL(0).ListItems, lblDebits, lblCredits, lblBalance
End Sub

'Load existing GL data
Private Sub GLRefresh()
  If mEventHandle = vbNullString Then
    GLClear
    Exit Sub
  End If
  
  mGLTransaction.DataProvider.filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle) & " and " & _
                                       LawDialect.EqualString(DatabaseName.SOURCETYPE_FIELD, mSourceType)
  ListViewLoad lvGL(0), mGLTransaction.DataProvider, True
  
  mGLTransaction.DataProvider.filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle) & " and " & _
                                       LawDialect.EqualString(DatabaseName.SOURCETYPE_FIELD, "PE")
  ListViewLoad lvGL(1), mGLTransaction.DataProvider, True
  
  'ver 2.50.00 (jjm) 2004 02 18 'Display the entry date from the GLEntry table
  Dim HandleNames(1) As String
  Dim Handles(1) As String
  Dim Row As collection
  
  HandleNames(0) = DatabaseName.EVENT_KEY
  HandleNames(1) = DatabaseName.SOURCETYPE_FIELD
  Handles(0) = mEventHandle
  
  Handles(1) = mSourceType
  Set Row = mGLEntryDataset.DataProvider.FindMulti(HandleNames, Handles, DatabaseName.ENTRYDATE_FIELD)
  If Not Row Is Nothing Then
    mGLEntryDate(0) = Row(DatabaseName.ENTRYDATE_FIELD)
  Else
    mGLEntryDate(0) = vbNullString
  End If
  
  Handles(1) = "PE"
  Set Row = mGLEntryDataset.DataProvider.FindMulti(HandleNames, Handles, DatabaseName.ENTRYDATE_FIELD)
  If Not Row Is Nothing Then
    mGLEntryDate(1) = Row(DatabaseName.ENTRYDATE_FIELD)
  Else
    mGLEntryDate(1) = vbNullString
  End If

  
  GLSummary
End Sub


Public Sub GLSummary()
  FormUtil.GLTransactionSummary lvGL(0).ListItems, lblDebits, lblCredits, lblBalance
  FormUtil.GLTransactionSummary lvGL(1).ListItems, lblDebits, lblCredits, lblBalance, True
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


'This sub checks to see if the firm has an accounting notepad entry. If it does then the entry
'is loaded into a notepad and displayed on the screen. This is automatically done when the screen
'is first shown.
Public Sub LoadAccountingNotepad(Optional Force As Boolean)

  Dim firmHandle As String
  Dim Row As collection

  If mPolicyHandle = vbNullString Then
    'ver 2.60.00 (jjm) 2004 04 01
    If mCoverageHandle = vbNullString Then Exit Sub
    
    Dim CoverageData As collection
    Set CoverageData = Factory.CoverageService.Find(mCoverageHandle)
    firmHandle = CoverageData(DatabaseName.FIRM_KEY)
  Else
    firmHandle = Factory.PolicyService.Find(mPolicyHandle)(DatabaseName.FIRM_KEY)
  End If
  
  If firmHandle = vbNullString Then
    MsgBox "Error. Unknown FirmHandle"
    Exit Sub
  End If
     
  'get Firm's eventId
  Dim FirmEventHandle As String
  FirmEventHandle = Factory.EntityService.Find(firmHandle, DatabaseName.EVENT_KEY)(DatabaseName.EVENT_KEY)

 'fix me not to use the view
  Dim HandleNames(1) As String
  Dim Handles(1) As String
  HandleNames(0) = DatabaseName.REFERENCE_KEY
  HandleNames(1) = DatabaseName.EVENTTYPE_KEY
  Handles(0) = FirmEventHandle
  Handles(1) = DatabaseName.ACCOUNTINGNOTE_TYPE
  Set Row = mReferenceView.DataProvider.FindMulti(HandleNames, Handles)
  
  If Not Row Is Nothing Then
    'Show the notepad if there is information
    'If Row(DatabaseName.EVENT_KEY)("eventNote") <> vbNullString Or Force Then
    If Row(DatabaseName.EVENTNOTE_FIELD) <> vbNullString Or Force Then
      FormUtil.ShowByPrimaryKey New frmNotepad, Row(DatabaseName.EVENT_KEY)
    End If
  End If
End Sub

Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.POLICYACCOUNTING_TABLE
      mPolicyAccountingHandle = ListItemText(mListView.SelectedItem, DatabaseName.POLICYACCOUNTING_KEY)
    Case DatabaseName.EVENT_TABLE
      Dim ListEventHandle As String
      ListEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
      mPolicyAccountingHandle = GetDataFromCollection(Factory.PolicyAccountingService.FindByEvent(ListEventHandle, DatabaseName.POLICYACCOUNTING_KEY), DatabaseName.POLICYACCOUNTING_KEY)
  End Select
End Property


' Used by PolicyIssue to get payment handle for linking.
Public Property Get PolicyAccountingHandle() As String
  PolicyAccountingHandle = mPolicyAccountingHandle
End Property


Private Sub PolicyRefresh()
  If mPolicyHandle = vbNullString Then
    ClearDataField lblDataField, DatabaseName.POLICY_TABLE
    mPolicyEventTypeHandle = vbNullString
    Exit Sub
  End If
  
  Dim Row As collection
  Set Row = Factory.PolicyService.Find(mPolicyHandle)
  SetFields lblDataField, Row
  
  mEffectiveDate = GetDataFromCollection(Row, DatabaseName.EFFECTIVEDATE_FIELD, True)
  mInvoice.DataProvider.filter = LawDialect.EqualString(DatabaseName.POLICY_KEY, mPolicyHandle)
  mInvoice.DataProvider.OpenRecordset
  ListViewLoad lvView, mInvoice.DataProvider, True
  
  If lvView.ListItems.Count = 0 Then
    Exit Sub
  End If
  
  Dim item As ListItem
  For Each item In lvView.ListItems
    If ListItemText(item, DatabaseName.COMPLETEDATE_FIELD) <> vbNullString Or _
      ArrayContains(mInvoiceHandles, ListItemText(item, DatabaseName.INVOICE_KEY)) _
    Then
      item.Checked = True
    End If
  Next item
End Sub


Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
    
  ' Custom Code
  
  ' Duplicate server side check.
  If lblBalance <> 0 Then
    MsgBox "Can not save until accounts balance.", vbCritical
    Exit Function
  End If
  
  
  ' Get GL
  Dim BindingGLAccountIds() As String
  Dim BindingGLTransactionAmounts() As Double
  FormUtil.GLTransactionLoad lvGL(0), BindingGLAccountIds, BindingGLTransactionAmounts
  
  Dim EffectiveGLAccountIds() As String
  Dim EffectiveGLTransactionAmounts() As Double
  FormUtil.GLTransactionLoad lvGL(1), EffectiveGLAccountIds, EffectiveGLTransactionAmounts
  
  
  Dim SourceTypes() As String
  Dim GLAccountIds() As String
  Dim GLTransactionAmounts() As Double
  
  Dim Size As Integer, I, j As Integer
  Size = SafeUBound(BindingGLAccountIds) + SafeUBound(EffectiveGLAccountIds) + 1
  If Size > -1 Then
    ReDim SourceTypes(Size)
    ReDim GLAccountIds(Size)
    ReDim GLTransactionAmounts(Size)
    For I = 0 To SafeUBound(BindingGLAccountIds)
      SourceTypes(I) = mSourceType
      GLAccountIds(I) = BindingGLAccountIds(I)
      GLTransactionAmounts(I) = BindingGLTransactionAmounts(I)
    Next I
  
    For j = 0 To SafeUBound(EffectiveGLAccountIds)
      SourceTypes(I + j) = "PE"
      GLAccountIds(I + j) = EffectiveGLAccountIds(j)
      GLTransactionAmounts(I + j) = EffectiveGLTransactionAmounts(j)
    Next j
  End If
  
  Dim mDataControl As New collection
  AddTextControl txtDataField, mDataControl
  AddValueControl dtpDataField, mDataControl
  
  
  Dim installmentIds As String
  Dim item As ListItem
  For Each item In lvView.ListItems
    If item.Checked And ListItemText(item, DatabaseName.COMPLETEDATE_FIELD) = vbNullString Then
      installmentIds = installmentIds & _
                          IIf(installmentIds = vbNullString, vbNullString, ",") & ListItemText(item, DatabaseName.INVOICE_KEY)
    End If
  Next item

  If mNew Then
    mPolicyAccountingHandle = Factory.PolicyAccountingService.CreatePolicyAccounting( _
        vbNullString, _
        mPolicyHandle, mCoverageHandle, _
        GetDate(mDataControl(DatabaseName.EVENTDATE_FIELD)), _
        GetDouble(mDataControl(DatabaseName.EVENTAMOUNT_FIELD)), _
        mEventTypeHandle, _
        SourceTypes, GLAccountIds, GLTransactionAmounts, _
        txtNote.Text, installmentIds)
    Save = mPolicyAccountingHandle <> vbNullString
  Else
    Save = Factory.PolicyAccountingService.WritePolicyAccounting( _
        mPolicyAccountingHandle, _
        mPolicyHandle, mCoverageHandle, _
        GetDate(mDataControl(DatabaseName.EVENTDATE_FIELD)), _
        GetDouble(mDataControl(DatabaseName.EVENTAMOUNT_FIELD)), _
        mEventTypeHandle, _
        SourceTypes, GLAccountIds, GLTransactionAmounts, _
        txtNote.Text, installmentIds)
  End If

  If Save Then
    mNew = False
    FormMgr.Message = fimSave
    '
    IniMain.WriteFile UrlName.MISC_INI, IIf(mIsBinding, "PolAccountingEventTypeBinding", "PolAccountingEventType"), mEventTypeHandle
    FormRefresh
  Else
    Dirty = True
  End If
End Function
          
Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.POLICYACCOUNTING_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no entries to view or edit."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
  LoadAccountingNotepad
End Sub

Public Sub ShowByPrimaryKey(PolicyAccountingHandle As String)
  mPolicyAccountingHandle = PolicyAccountingHandle
  Show
  LoadAccountingNotepad
End Sub

Public Sub ShowNew(Optional PolicyHandle As String, Optional eventTypeId As String, Optional coverageHandle As String)
  mPolicyHandle = PolicyHandle
  mEventTypeHandle = eventTypeId
  mCoverageHandle = coverageHandle
  mNew = True
  Show
  LoadAccountingNotepad
  If mPolicyHandle = vbNullString Then
    cmdPolicy_Click
  End If
End Sub


Public Sub ShowFromCoverage(PolicyHandle As String, coverageHandle As String, eventTypeId As String)
  mIsBinding = Factory.EventService.IsEventTypeInGroup(eventTypeId, DatabaseName.CVGPOLICYBINDING_RULE)
  
  'ver 2.73.00 (jjm) REF 242
  Dim isTail As Boolean
  isTail = Factory.EventService.IsEventTypeInGroup(eventTypeId, DatabaseName.CLAIMTAIL_RULE)
  'mCheckForMultipleEntries = True
  mCheckForMultipleEntries = mIsBinding Or isTail
  
  '2.70.01 (jjm) 2004 12 27
  BindingSuspenseAccount = IIf(mIsBinding, "2700", "2750")
  
  
  If (Not mIsBinding) Then
    mPolicyHandle = PolicyHandle
  End If
  mEventTypeHandle = eventTypeId
  mCoverageHandle = coverageHandle
  mNew = True
  Show
  LoadAccountingNotepad
  cmdPolicy.Enabled = False
End Sub
'
'
' End General
'
'
'
'
' Control Methods
'

Private Sub cboEventTypeName_Click()
  If cboEventTypeName.ListIndex < 0 Then
    mEventTypeHandle = vbNullString
  Else
    mEventTypeHandle = mEventTypes(cboEventTypeName.ListIndex + 1)(DatabaseName.EVENTTYPE_KEY)
  End If
  Dirty = True
End Sub

Private Sub cmdAdd_Click()
  Dim fGL As New frmGLTransactionEdit
  Set fGL.ListView = lvGL(mGLIndex)
  fGL.Group = DatabaseName.POLICYACCOUNTING_TABLE
  fGL.Show vbModal, Me
  If fGL.Ok Then
    Dirty = True
  End If
  Unload fGL
  FormUtil.GLTransactionSummary lvGL(mGLIndex).ListItems, lblDebits, lblCredits, lblBalance
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

'reset all GL info
Private Sub cmdDefaults_Click()
  lvGL(0).ListItems.Clear
  lvGL(1).ListItems.Clear
  mDefault = False
  GLDefault
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab hWnd
End Sub

Private Sub cmdPolicy_Click()
  Dispatcher.ShowSearch DatabaseName.POLICY_TABLE, FormMgr, True
End Sub

Private Sub cmdProperties_Click()
  If lvGL(mGLIndex).ListItems.Count = 0 Or lvGL(mGLIndex).SelectedItem Is Nothing Then
    Exit Sub
  End If
  Dim fGL As New frmGLTransactionEdit
  Set fGL.ListView = lvGL(mGLIndex)
  fGL.Group = DatabaseName.POLICYACCOUNTING_TABLE
  fGL.Edit
  fGL.Show vbModal, Me
  
  If fGL.Ok Then
    Dirty = True
  End If
  
  Unload fGL
  FormUtil.GLTransactionSummary lvGL(mGLIndex).ListItems, lblDebits, lblCredits, lblBalance
End Sub


Private Sub cmdRemove_Click()
  With lvGL(mGLIndex)
    If .ListItems.Count = 0 Or .SelectedItem Is Nothing Then
      Exit Sub
    End If
    .ListItems.Remove .SelectedItem.Index
    FormUtil.GLTransactionSummary .ListItems, lblDebits, lblCredits, lblBalance
    If .ListItems.Count > 0 Then
      .SelectedItem = .SelectedItem
    End If
    Dirty = True
  End With
End Sub

Private Sub cmdSave_Click()
  If Save Then
    'FormUtil.SetComboDefault cboEventTypeName
    Unload Me
  End If
End Sub

Private Sub cmdSaveNew_Click()
  If Save Then mnuFileNewMenu_Click
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
  Select Case Index
    Case DTP_PAYMENTDATE_INDEX
      If Not mCheckForMultipleEntries Then
        Exit Sub
      End If
      
      '
      ' Test to see if there should be multiple entries.
      '
      If mEffectiveDate <= dtpDataField(Index).Value Or _
        (dtpDataField(Index).Month = Month(mEffectiveDate) And _
         dtpDataField(Index).Year = Year(mEffectiveDate)) _
      Then
        ' Only one entry is needed so hide the opt options that
        ' are used to switch between the two
        If optGL(1).Visible Then
          optGL(0).Value = True
          optGL(0).Visible = False
          optGL(1).Visible = False
          GLClear
          mGLIndex = 0
          FormUtil.GLTransactionSummary lvGL(mGLIndex).ListItems, lblDebits, lblCredits, lblBalance
          
          'ver 2.50.00 (jjm) 2004 02 18
          lblEntryDate.Caption = mGLEntryDate(0)
          
        End If
        Exit Sub
      End If
      
      'If the GL opts are already setup then skip the next step
      If optGL(1).Visible = True Then
        Exit Sub
      End If
      
      'This code is ran when two entries are neeeded
      optGL(0).Visible = True
      optGL(1).Visible = True
      optGL(1).Value = True
      GLClear
  End Select
End Sub

Private Sub Form_Initialize()
  Set mGLTransaction = NewGenericDataset(DatabaseName.GLTRANSACTION_VIEW)
  Set mInvoice = NewGenericDataset(DatabaseName.INVOICE_VIEW)
  'Set mQuoteCoverageProvider = NewGenericDataset(DatabaseName.QUOTECOVERAGE_TABLE).DataProvider
  Set mReferenceView = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
  
  'ver 2.50.00 (jjm) 2004 02 18
  Set mGLEntryDataset = NewGenericDataset(DatabaseName.GLENTRY_TABLE)
      
  'not for sure how this is used
'  Factory.FolderParameterService.ParameterLoad DatabaseName.POLICYACCOUNTING_TABLE, lawActionNew, mParameterNames, mParameterValues
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
 ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
 
  FormResize
  FormRefresh
  
  If lvView.ColumnHeaders.Count > 0 Then
    ListViewSort lvView, lvView.ColumnHeaders("dueDate")
  End If
  
  FormUtil.TabControlEnable SSTab
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvGL_DblClick(Index As Integer)
  If lvGL(Index).ListItems.Count = 0 Then
    Exit Sub
  End If
  cmdProperties_Click
End Sub

Private Sub lvGL_KeyPress(Index As Integer, KeyAscii As Integer)
  If KeyAscii = vbKeyInsert Then
    cmdAdd_Click
  End If
End Sub

Private Sub lvView_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView, ColumnHeader
  PolicyRefresh
End Sub

Private Sub lvView_ItemCheck(ByVal item As MSComctlLib.ListItem)
  If Not item.Checked And ListItemText(item, DatabaseName.COMPLETEDATE_FIELD) <> vbNullString Then
    item.Checked = True
    MsgBox "You cannot uncheck paid items"
    Exit Sub
  End If
End Sub

Private Sub mnuFileNewMenu_Click()
  If mIsBinding Then
    MsgBox "Cannot do a new binding accounting from here."
    Exit Sub
  End If

  If Not Save(True) Then Exit Sub
  
  mNew = True
  mCoverageHandle = vbNullString
  FormRefresh
  cmdPolicy.SetFocus
End Sub

Private Sub mnuFileSendToFolder_Click()
  'FormUtil.SendToFolder DatabaseName.PolicyAccounting_REF, DatabaseName.POLICYACCOUNTING_TABLE, mEventHandle, "Policy Name"
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.POLICYACCOUNTING_TABLE, mPolicyAccountingHandle
  End If
'  FormUtil.ShowEventProperties mEventHandle
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAccountingNotepad_Click()
  LoadAccountingNotepad True
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.POLICYACCOUNTING_TABLE, lawActionNew, _
      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView, mInvoice.DataProvider, False
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub


Private Sub optGL_Click(Index As Integer)
  mGLIndex = IIf(optGL(0).Value, 0, 1)
  
  'ver 2.50.00 (jjm) 2004 02 17 - made the code more efficient
  lvGL(0).Visible = (mGLIndex = 0)
  lvGL(1).Visible = Not (mGLIndex = 0)
  
  'ver 2.50.00 (jjm) 2004 02 18 - Display the correct GL Entry Date
  lblEntryDate.Caption = mGLEntryDate(mGLIndex)
End Sub

Private Sub SSTab_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab
  mnuViewChooseColumns.Enabled = IIf(SSTab.Tab = 1, True, False)
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub


Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
  Select Case Index
    Case TXT_PAYMENTAMOUNT_INDEX
      If Not ActiveControl Is Nothing Then
        GLAmount GetDouble(txtDataField(Index))
      End If
  End Select
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtDataField_Validate(Index As Integer, Cancel As Boolean)
  Select Case Index
    Case TXT_PAYMENTAMOUNT_INDEX
      GLDefault 'This will only auto-setup the defaults once
  End Select
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub


Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
