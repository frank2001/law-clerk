VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EFTIndividualDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'ver 2.60.00 (jjm) 2004 03 15
Option Explicit

'EFT Deposit Fininacial Inst Dataset

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer


Private Sub Class_Initialize()
  ListIndex = 1
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.EFTINDIVIDUAL_TABLE
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  
  frm.cboIn.AddItem "in account number field only"
  frm.cboIn.AddItem "in eft deposit financial institution identifier"
  frm.cboIn.AddItem "in eft individual identifier field only"
  frm.cboIn.AddItem "in eft individual name field only"
  frm.cboIn.AddItem "in eft transaction code identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, 2)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection

  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As String
  If Not Words = "" Then
    Select Case ListIndex
      Case 0
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.ACCOUNTNUMBER_FIELD, Words)
      Case 1
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.EFTDEPOSITFINANCIALINST_KEY, Words)
      Case 2
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.EFTINDIVIDUAL_KEY, Words)
      Case 3
        newFilter = LawDialect.ContainsAndArray(DatabaseName.INDIVIDUALNAME_FIELD, Words)
      Case 4
        newFilter = LawDialect.StartsWithUpperCase(DatabaseName.EFTTRANSACTIONCODE_KEY, Words)
     End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function



