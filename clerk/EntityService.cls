VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EntityService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Private Const ServiceName = "EventService"
'Private mClient As SoapClient30

Private mDataSet As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  'Set mClient = New SoapClient30
  
  'mClient.MSSoapInit HttpServer &  WebComponent & ServiceName & ".jws?wsdl"
  'mClient.ConnectorProperty("EndPointURL") = HttpServer &  WebService & ServiceName & ".jws" & LawSession.Session
  
  Set mDataSet = NewGenericDataset(DatabaseName.ENTITY_TABLE)
End Sub


Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.ENTITY_KEY, Handle, Column)
End Function


'ver 3.00 (jjm)
Public Function CreatePersonLocator(LocatorAttempt As String, Optional entityhandle As String) As String
  Dim Result As String, I As Integer
  Result = LocatorAttempt
  While Not LocatorAvailable(Result, entityhandle)
    I = I + 1
    Result = Left(Result, 7) & I
  Wend
  CreatePersonLocator = Result
End Function

'Determine if the locator is available
Private Function LocatorAvailable(locator As String, Optional entityhandle As String) As Boolean
  'Mantis 2503 - fixed locator query
  mDataSet.DataProvider.Filter = DatabaseName.ENTITYLOCATOR_FIELD & " = '" & locator & "'" & _
                                 IIf(entityhandle <> vbNullString, " and " & DatabaseName.ENTITY_KEY & " != '" & entityhandle & "'", vbNullString)
                                 
  mDataSet.DataProvider.OpenRecordset
  LocatorAvailable = (mDataSet.DataProvider.Collection Is Nothing)
End Function


'ver 2.75 (jjm)
'Check to see verify that the firmLocator being passed is not being
'used as an exisiting firmId or for as a firmLocator for another firm
'This function will return true when its OK to continue

'ver 3.00 (jjm)
'Hacked for updated architecture.
'Public Function verifyOrganizationLocator(locator As String, Optional organizationHandle As String) As Boolean
'  If locator = vbNullString Then
'    MsgBox "A locator is required to save.", vbCritical
'    Exit Function
'  End If
'
'  mDataset.DataProvider.Filter = IIf(organizationHandle <> vbNullString, _
'                                        DatabaseName.ORGANIZATION_KEY & " != '" & organizationHandle & "' and ", _
'                                        vbNullString) & _
'                                 "(" & DatabaseName.ORGANIZATION_KEY & " = '" & locator & "' or " & _
'                                 "organizationLocator = '" & locator & "')"
'  'store the following results into a collection
'  Dim results As Collection
'  mDataset.DataProvider.OpenRecordset
'  Set results = mDataset.DataProvider.Collection
'  If results Is Nothing Then
'    'all is good, return true
'    verifyOrganizationLocator = True
'  Else
'    'A duplicate was found. Display information to user
'    Dim organizationList As String, I As Integer
'    For I = 1 To results.Count
'      organizationList = organizationList & _
'                 GetDataFromCollection(results(I), DatabaseName.ORGANIZATION_KEY) & vbTab & vbTab & _
'                 GetDataFromCollection(results(I), DatabaseName.ENTITYLOCATOR_FIELD) & vbTab & vbTab & _
'                 GetDataFromCollection(results(I), DatabaseName.ENTITYNAME_FIELD) & vbCrLf
'    Next I
'    MsgBox "The following organization is are already using this locator value for their locator or a organizationId:" & vbCrLf & _
'           "ID" & vbTab & vbTab & "Locator" & vbTab & vbTab & "Name" & vbCrLf & organizationList, vbExclamation
'  End If
'End Function



'ver 3.00 (jjm)
'RE-Hacked for updated architecture. This is a much simpler version compared to the previous ones.
Public Function verifyEntityLocator(locator As String, entityhandle As String) As Boolean
  If locator = vbNullString Then
    MsgBox "A locator is required to save.", vbCritical
    Exit Function
  End If

  'Find locator conflicts
   mDataSet.DataProvider.Filter = IIf(entityhandle <> vbNullString, _
                                        DatabaseName.ENTITY_KEY & " != '" & entityhandle & "' and ", _
                                        vbNullString) & _
                                        DatabaseName.ENTITYLOCATOR_FIELD & " = '" & locator & "'"
  'store the following results into a collection
  Dim results As Collection
  mDataSet.DataProvider.OpenRecordset
  Set results = mDataSet.DataProvider.Collection
  If results Is Nothing Then
    'all is good, return true
    verifyEntityLocator = True
  Else
    'A duplicate was found. Display information to user
    Dim organizationList As String, I As Integer
    For I = 1 To results.Count
      organizationList = organizationList & _
                 GetDataFromCollection(results(I), DatabaseName.ENTITY_KEY) & vbTab & vbTab & _
                 GetDataFromCollection(results(I), DatabaseName.ENTITYLOCATOR_FIELD) & vbTab & vbTab & _
                 GetDataFromCollection(results(I), DatabaseName.ENTITYNAME_FIELD) & vbCrLf
    Next I
    MsgBox "The following organization is are already using this locator value for their locator:" & vbCrLf & _
           "ID" & vbTab & vbTab & "Locator" & vbTab & vbTab & "Name" & vbCrLf & organizationList, vbExclamation
  End If
End Function


