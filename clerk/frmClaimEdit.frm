VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmClaimEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   6300
   ClientLeft      =   372
   ClientTop       =   1116
   ClientWidth     =   6984
   Icon            =   "frmClaimEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   6300
   ScaleWidth      =   6984
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6264
      Left            =   0
      ScaleHeight     =   6264
      ScaleWidth      =   6984
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   0
      Width           =   6984
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   4428
         TabIndex        =   51
         Top             =   5868
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   5688
         TabIndex        =   52
         Top             =   5868
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   5364
         Left            =   60
         TabIndex        =   53
         Top             =   420
         Width           =   6828
         _ExtentX        =   12044
         _ExtentY        =   9462
         _Version        =   393216
         Style           =   1
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmClaimEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Frame1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Frame7"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "fraReinsureReport"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "&Lawsuit"
         TabPicture(1)   =   "frmClaimEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame3"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "Frame4"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).ControlCount=   2
         TabCaption(2)   =   "Law &Area"
         TabPicture(2)   =   "frmClaimEdit.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvItem(0)"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "cmdItemRemove(0)"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "cmdItemSelect(0)"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "cmdItemDown(0)"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "cmdItemUp(0)"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).ControlCount=   5
         TabCaption(3)   =   "La&w Error"
         TabPicture(3)   =   "frmClaimEdit.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvItem(1)"
         Tab(3).Control(0).Enabled=   0   'False
         Tab(3).Control(1)=   "cmdItemRemove(1)"
         Tab(3).Control(1).Enabled=   0   'False
         Tab(3).Control(2)=   "cmdItemSelect(1)"
         Tab(3).Control(2).Enabled=   0   'False
         Tab(3).Control(3)=   "cmdItemDown(1)"
         Tab(3).Control(3).Enabled=   0   'False
         Tab(3).Control(4)=   "cmdItemUp(1)"
         Tab(3).Control(4).Enabled=   0   'False
         Tab(3).ControlCount=   5
         TabCaption(4)   =   "N&ote"
         TabPicture(4)   =   "frmClaimEdit.frx":007C
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "txtNote"
         Tab(4).Control(0).Enabled=   0   'False
         Tab(4).ControlCount=   1
         Begin VB.Frame fraReinsureReport 
            Caption         =   "Reinsurer Report"
            Height          =   696
            Left            =   108
            TabIndex        =   63
            Top             =   2016
            Width           =   4872
            Begin VB.CheckBox chkDataField 
               Caption         =   "Is Reinsurer Report"
               DataField       =   "isReinsurerReport"
               Height          =   192
               Index           =   1
               Left            =   156
               TabIndex        =   3
               Top             =   324
               Width           =   1896
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmClaimEdit.frx":0098
               DataField       =   "reinsurerReportDate"
               Height          =   288
               Index           =   3
               Left            =   3240
               TabIndex        =   4
               Top             =   264
               Width           =   1236
               _ExtentX        =   2180
               _ExtentY        =   508
               _Version        =   393216
               Format          =   61865985
               CurrentDate     =   37781
            End
            Begin VB.Label Label16 
               AutoSize        =   -1  'True
               Caption         =   "Report Date:"
               Height          =   192
               Left            =   2220
               TabIndex        =   64
               Top             =   312
               Width           =   912
            End
         End
         Begin VB.Frame Frame7 
            Caption         =   "Law Disposition"
            Height          =   696
            Left            =   108
            TabIndex        =   62
            Top             =   2808
            Width           =   4884
            Begin VB.ComboBox cboDispositionDesc 
               Height          =   288
               Left            =   84
               Style           =   2  'Dropdown List
               TabIndex        =   5
               Top             =   276
               Width           =   3936
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmClaimEdit.frx":00A3
               DataField       =   "lawDispositionId"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   0
               Left            =   4080
               TabIndex        =   6
               Top             =   276
               Width           =   660
               _ExtentX        =   1164
               _ExtentY        =   508
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   2
               Mask            =   ">AA"
               PromptChar      =   "_"
            End
         End
         Begin VB.CommandButton cmdItemUp 
            Caption         =   "Move &Up"
            Height          =   336
            Index           =   1
            Left            =   -70140
            TabIndex        =   26
            Top             =   348
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemDown 
            Caption         =   "Move &Down"
            Height          =   336
            Index           =   1
            Left            =   -70140
            TabIndex        =   27
            Top             =   756
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemSelect 
            Caption         =   "&Select"
            Height          =   336
            Index           =   1
            Left            =   -70140
            TabIndex        =   28
            Top             =   1152
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemRemove 
            Caption         =   "&Remove"
            Height          =   336
            Index           =   1
            Left            =   -70140
            TabIndex        =   29
            Top             =   1560
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemUp 
            Caption         =   "Move &Up"
            Height          =   336
            Index           =   0
            Left            =   -70140
            TabIndex        =   21
            Top             =   348
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemDown 
            Caption         =   "Move &Down"
            Height          =   336
            Index           =   0
            Left            =   -70140
            TabIndex        =   22
            Top             =   756
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemSelect 
            Caption         =   "&Select"
            Height          =   336
            Index           =   0
            Left            =   -70140
            TabIndex        =   23
            Top             =   1152
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemRemove 
            Caption         =   "&Remove"
            Height          =   336
            Index           =   0
            Left            =   -70140
            TabIndex        =   24
            Top             =   1560
            Width           =   1200
         End
         Begin VB.TextBox txtNote 
            Height          =   4884
            Left            =   -74892
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   30
            Top             =   360
            Width           =   6612
         End
         Begin VB.Frame Frame4 
            Caption         =   "Service Dates"
            Height          =   1044
            Left            =   -74892
            TabIndex        =   46
            Top             =   2076
            Width           =   5052
            Begin MSComCtl2.DTPicker dtpDataField 
               DataField       =   "firmServeDate"
               Height          =   288
               Index           =   5
               Left            =   876
               TabIndex        =   18
               Top             =   612
               Width           =   1464
               _ExtentX        =   2604
               _ExtentY        =   508
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   61865985
               CurrentDate     =   37293
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               DataField       =   "insurerServeDate"
               Height          =   288
               Index           =   4
               Left            =   3480
               TabIndex        =   19
               Top             =   240
               Width           =   1464
               _ExtentX        =   2604
               _ExtentY        =   508
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   61865985
               CurrentDate     =   37293
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               DataField       =   "insuredServeDate"
               Height          =   288
               Index           =   2
               Left            =   876
               TabIndex        =   17
               Top             =   240
               Width           =   1464
               _ExtentX        =   2604
               _ExtentY        =   508
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   61865985
               CurrentDate     =   37293
            End
            Begin VB.Label Label14 
               AutoSize        =   -1  'True
               Caption         =   "Firm:"
               Height          =   192
               Left            =   120
               TabIndex        =   49
               Top             =   660
               Width           =   348
            End
            Begin VB.Label lblInsurer 
               Caption         =   "WILMIC:"
               Height          =   192
               Left            =   2676
               TabIndex        =   48
               Top             =   324
               Width           =   876
            End
            Begin VB.Label Label18 
               Caption         =   "Insured:"
               Height          =   192
               Left            =   132
               TabIndex        =   47
               Top             =   288
               Width           =   876
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Court"
            Height          =   1644
            Left            =   -74892
            TabIndex        =   42
            Top             =   312
            Width           =   6576
            Begin VB.TextBox txtDatafield 
               DataField       =   "trialDateRange"
               Height          =   288
               Index           =   6
               Left            =   1404
               TabIndex        =   16
               Top             =   1260
               Width           =   2856
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "courtCaseNum"
               Height          =   288
               Index           =   5
               Left            =   1392
               TabIndex        =   15
               Top             =   912
               Width           =   5076
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "court"
               Height          =   288
               Index           =   4
               Left            =   1392
               TabIndex        =   14
               Top             =   576
               Width           =   5076
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "pleadingCaption"
               Height          =   288
               Index           =   3
               Left            =   1392
               TabIndex        =   13
               Top             =   240
               Width           =   5076
            End
            Begin VB.Label Label20 
               AutoSize        =   -1  'True
               Caption         =   "Trial Date Range"
               Height          =   192
               Left            =   96
               TabIndex        =   50
               Top             =   1260
               Width           =   1236
            End
            Begin VB.Label Label15 
               Caption         =   "Court Case NO:"
               Height          =   192
               Left            =   96
               TabIndex        =   44
               Top             =   960
               Width           =   1152
            End
            Begin VB.Label Label10 
               Caption         =   "Court:"
               Height          =   192
               Left            =   96
               TabIndex        =   45
               Top             =   624
               Width           =   900
            End
            Begin VB.Label Label6 
               Caption         =   "Case Name:"
               Height          =   192
               Left            =   96
               TabIndex        =   43
               Top             =   288
               Width           =   900
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "Claim"
            Height          =   1620
            Left            =   108
            TabIndex        =   37
            Top             =   300
            Width           =   6576
            Begin VB.CheckBox chkDataField 
               Caption         =   "Is Judgment Claimant"
               DataField       =   "isJudgmentClaimant"
               Height          =   192
               Index           =   2
               Left            =   1824
               TabIndex        =   2
               Top             =   1248
               Width           =   1848
            End
            Begin VB.CheckBox chkDataField 
               Caption         =   "Is Fee Dispute"
               DataField       =   "isFeeDispute"
               Height          =   192
               Index           =   0
               Left            =   156
               TabIndex        =   1
               Top             =   1248
               Width           =   1692
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "claimant"
               DataMember      =   "Instance"
               Height          =   576
               Index           =   2
               Left            =   1116
               MultiLine       =   -1  'True
               TabIndex        =   0
               Text            =   "frmClaimEdit.frx":00B6
               Top             =   492
               Width           =   5340
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Firm Name:"
               Height          =   195
               Left            =   135
               TabIndex        =   55
               Top             =   240
               Width           =   795
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "firmName"
               DataField       =   "firmName"
               DataMember      =   "Instance"
               Height          =   195
               Index           =   0
               Left            =   1110
               TabIndex        =   54
               Top             =   240
               UseMnemonic     =   0   'False
               Width           =   660
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "Claimant:"
               Height          =   192
               Left            =   132
               TabIndex        =   38
               Top             =   540
               Width           =   660
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Date:"
            Height          =   1596
            Left            =   108
            TabIndex        =   32
            Top             =   3612
            Width           =   6576
            Begin VB.ComboBox cboDataField 
               DataField       =   "adjuster"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   0
               ItemData        =   "frmClaimEdit.frx":00BF
               Left            =   1584
               List            =   "frmClaimEdit.frx":00C1
               Style           =   2  'Dropdown List
               TabIndex        =   9
               Top             =   1224
               Width           =   1548
            End
            Begin VB.ComboBox cboClaimTypeName 
               Height          =   288
               ItemData        =   "frmClaimEdit.frx":00C3
               Left            =   4824
               List            =   "frmClaimEdit.frx":00C5
               Style           =   2  'Dropdown List
               TabIndex        =   12
               Top             =   1224
               Width           =   1548
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "occurDateRange"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   0
               Left            =   1584
               TabIndex        =   8
               Top             =   888
               Width           =   1548
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "knowDateRange"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   1
               Left            =   4824
               TabIndex        =   11
               Top             =   888
               Width           =   1548
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmClaimEdit.frx":00C7
               DataField       =   "occurDate"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   0
               Left            =   1584
               TabIndex        =   7
               Top             =   552
               Width           =   1548
               _ExtentX        =   2731
               _ExtentY        =   508
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   61865985
               CurrentDate     =   37109
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmClaimEdit.frx":00DA
               DataField       =   "knowDate"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   1
               Left            =   4824
               TabIndex        =   10
               Top             =   552
               Width           =   1548
               _ExtentX        =   2731
               _ExtentY        =   508
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   61865985
               CurrentDate     =   37109
            End
            Begin VB.Label Label17 
               AutoSize        =   -1  'True
               Caption         =   "Claim Type:"
               Height          =   192
               Left            =   3372
               TabIndex        =   61
               Top             =   1272
               Width           =   864
            End
            Begin VB.Label Label13 
               AutoSize        =   -1  'True
               Caption         =   "Adjuster:"
               Height          =   192
               Left            =   132
               TabIndex        =   60
               Top             =   1272
               Width           =   624
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               Caption         =   "Coverage Date:"
               Height          =   192
               Left            =   3348
               TabIndex        =   59
               Top             =   264
               Width           =   1140
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "coverageDate"
               DataField       =   "coverageDate"
               DataMember      =   "Instance"
               Height          =   192
               Index           =   3
               Left            =   4824
               TabIndex        =   58
               Top             =   264
               Width           =   1044
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Made Date:"
               Height          =   192
               Left            =   132
               TabIndex        =   57
               Top             =   264
               Width           =   840
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "claimMadeDate"
               DataField       =   "claimMadeDate"
               Height          =   192
               Index           =   2
               Left            =   1584
               TabIndex        =   56
               Top             =   264
               Width           =   1152
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Occur Date Range:"
               Height          =   192
               Left            =   132
               TabIndex        =   36
               Top             =   936
               Width           =   1368
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "Known Date:"
               Height          =   192
               Left            =   3348
               TabIndex        =   35
               Top             =   600
               Width           =   888
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               Caption         =   "Known Date Range:"
               Height          =   192
               Left            =   3348
               TabIndex        =   34
               Top             =   936
               Width           =   1416
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Occur Date:"
               Height          =   192
               Left            =   132
               TabIndex        =   33
               Top             =   600
               Width           =   840
            End
         End
         Begin MSComctlLib.ListView lvItem 
            Height          =   4740
            Index           =   0
            Left            =   -74790
            TabIndex        =   20
            Top             =   330
            Width           =   4545
            _ExtentX        =   8022
            _ExtentY        =   8361
            SortKey         =   1
            View            =   3
            LabelEdit       =   1
            Sorted          =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            Checkboxes      =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   10583
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   0
            EndProperty
         End
         Begin MSComctlLib.ListView lvItem 
            Height          =   4740
            Index           =   1
            Left            =   -74790
            TabIndex        =   25
            Top             =   330
            Width           =   4545
            _ExtentX        =   8022
            _ExtentY        =   8361
            SortKey         =   1
            View            =   3
            LabelEdit       =   1
            Sorted          =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            Checkboxes      =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   10583
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   0
            EndProperty
         End
      End
      Begin VB.Label Label5 
         Caption         =   "Firm Name:"
         Height          =   192
         Left            =   120
         TabIndex        =   41
         Top             =   84
         Width           =   852
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "firmName"
         DataField       =   "firmName"
         DataMember      =   "Instance"
         Height          =   192
         Index           =   1
         Left            =   1068
         TabIndex        =   40
         Top             =   84
         UseMnemonic     =   0   'False
         Width           =   696
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   348
      TabIndex        =   39
      Top             =   816
      Width           =   1056
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpenClaim 
            Caption         =   "&Claim"
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d to"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator996 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator945 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmClaimEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.70 (jjm) 2004 09 20
'ver 2.40.02 (jjm) - fixed bug to prevserve existing incidentId's
'ver 2.30.1209 (jjm)
'previous ver 2.20.0611 (jjm)

Option Explicit

Private IsChanged As Boolean
Private mvarPrimaryKey As Collection
Private Datafields As Collection

Private mClaimHandle As String
Private mPolicyHandle As String
Private mFirmHandle As String
Private mEventHandle As String

Private mDatafields As Collection
Private mDataSet As Dataset
'Private mIncidentHandle As String

Private mLawDispositionHandle As String
Private mLawDispositionCollection As Collection
Private mLawDisposition As Dataset

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mCancel As Boolean

Private mGotoApplication As Boolean

' Internal Id Fields
Private mFirmDataset As Dataset

Private mLeft As Integer
Private mTop As Integer

Private mParameterNames() As String
Private mParameterValues() As String

Private mLawArea As Collection
Private mLawError As Collection

Private mTab As Integer

Private mClaimTypes As Collection
Private mClaimType As String

'ver 2.72.00 (jjm) 2005 05 06 - removed claimant search
'ver 2.70 (jjm) 2004 09 20
'Private IsNullClaimant As Boolean

' Form Manager
Private FormMgr As New FormManager

'Constants
Private Const TXT_CLAIMANT_INDEX = 2

Public Sub AddItem(View As ListView, Optional Key As String, Optional Text As String, Optional Checked As Boolean, Optional Tag As String)
  Dim SortKey As String
  SortKey = Format(View.ListItems.Count + 1, "0000")
  
  If Key = vbNullString Then
    Key = "A" & SortKey
  End If
  
  With View.ListItems.Add(, Key, Text)
    .Checked = Checked
    .Tag = Tag
    .ListSubItems.Add , "SortKey", SortKey
  End With
End Sub

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, False
  End If
End Property

Public Function ItemArray(View As ListView) As String()
  Dim item As ListItem, List As String
  For Each item In View.ListItems
    If item.Checked Then
      List = List & IIf(List = vbNullString, "", "<!-- -->") & item.Tag
    End If
  Next item

  If List <> vbNullString Then
    ItemArray = Split(List, "<!-- -->")
  End If
End Function

Public Sub ButtonRefresh(Index As Integer)
  With lvItem(Index).SelectedItem
    cmdItemUp(Index).Enabled = (.Index > 1)
    cmdItemDown(Index).Enabled = (.Index < lvItem(Index).ListItems.Count)
    cmdItemSelect(Index).Enabled = Not .Checked
    cmdItemRemove(Index).Enabled = .Checked
  End With
End Sub

Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form requested new firm.
    Case fimNew
      'mnuFileNewFirm_Click
      Exit Sub
    
    ' Select form selected
    Case fimSelect
      mPolicyHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.POLICY_KEY)
      mFirmHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRM_KEY)
      FirmRefresh
      
    ' New Firm has been saved via mnuFileNewFirm.
    Case fimSave
'      mFirmHandle = fInterface.Child.FirmHandle
'      lblDataField(2) = fInterface.Child.txtDataField(5)
  End Select
  Show
  cboDataField(0).SetFocus
End Sub

Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub

Private Sub FormRefresh()

  'clear the data
  mClaimType = vbNullString
  txtNote.Text = vbNullString

  
  'ver 2.70 jjm 2004 11 22
  Dim DateControl As DTPicker
  For Each DateControl In dtpDataField
    DateControl.Value = Today
  Next DateControl
 
  Set mDatafields = Factory.ClaimService.Find(mClaimHandle)
  SetFields chkDataField, mDatafields
  SetFields dtpDataField, mDatafields
  SetFields txtDatafield, mDatafields
  SetFields lblDataField, mDatafields
  
  Dim InstanceDataField As Collection
  Set InstanceDataField = Factory.InstanceService.Find(mClaimHandle)
  SetFields cboDataField, InstanceDataField, "Instance"
  SetFields dtpDataField, InstanceDataField, "Instance"
  SetFields lblDataField, InstanceDataField, "Instance"
  SetFields mskDataField, InstanceDataField, "Instance"
  SetFields txtDatafield, InstanceDataField, "Instance"
    
   
  mClaimHandle = mDatafields.item(DatabaseName.CLAIM_KEY)
  mPolicyHandle = InstanceDataField(DatabaseName.POLICY_KEY)
  mFirmHandle = InstanceDataField(DatabaseName.FIRM_KEY)
  mEventHandle = InstanceDataField(DatabaseName.EVENT_KEY)
  
  Me.Caption = "Claim Edit (" & mClaimHandle & ")"
    
  'Event data
  txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  
  'Set cboClaimTypeName
  If mClaimTypes Is Nothing Then
    MsgBox "There are no ClaimType records in the TableColumnType table."
    Unload Me
    Exit Sub
  End If
  Dim I As Integer
  mClaimType = GetDataFromCollection(mDatafields, DatabaseName.CLAIMTYPE_FIELD)
  For I = 1 To mClaimTypes.Count
    If mClaimType = mClaimTypes(I)(DatabaseName.TYPE_FIELD) Then
      cboClaimTypeName.ListIndex = I - 1
      Exit For
    End If
  Next I
  
   
  Dim mLawData As Collection
  mLawDispositionHandle = mDatafields.item(DatabaseName.LAWDISPOSITION_KEY)
  If Not mLawDispositionHandle = vbNullString Then
    Set mLawData = mLawDisposition.DataProvider.Find(DatabaseName.LAWDISPOSITION_KEY, mLawDispositionHandle, DatabaseName.LAWDISPOSITIONNAME_FIELD)
  End If
  
  'ver 2.70 (jjm) 2004 09 20
  'If the Claimant is null then add the claimant search
  'IsNullClaimant = (mDatafields(DatabaseName.CLAIMANT_FIELD) = vbNullString)
  
  LawDispositionRefresh
  LawAreaRefresh
  LawErrorRefresh
     
  FirmRefresh
  Dirty = False
End Sub

Private Sub FirmRefresh()
  If mFirmHandle = vbNullString Then
    ClearDataField lblDataField, DatabaseName.FIRM_TABLE
  Else
    SetFields lblDataField, Factory.FirmService.Find(mFirmHandle), DatabaseName.FIRM_TABLE
  End If
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub LawAreaRefresh()
  lvItem(0).ListItems.Clear
  
  Dim Row As Collection
  For Each Row In mLawArea
    AddItem lvItem(0), "Key:" & Row(DatabaseName.LAWAREA_KEY), Row("lawAreaName"), , Row(DatabaseName.LAWAREA_KEY)
  Next Row
  
  Dim Items As Collection, I As Integer
  Set Items = Factory.InstanceService.LawAreaCollection(mClaimHandle)
  If Items Is Nothing Then
    Exit Sub
  End If
  
  For Each Row In Items
    I = I + 1
    lvItem(0).ListItems.item("Key:" & Row(DatabaseName.LAWAREA_KEY)).Selected = True
    lvItem(0).ListItems.item("Key:" & Row(DatabaseName.LAWAREA_KEY)).Checked = True
    Swap lvItem(0), I
  Next Row
End Sub

Private Function LawDispositionRefresh() As Boolean
  If mskDataField(0).Text = "__" Then
    cboDispositionDesc.ListIndex = -1
    LawDispositionRefresh = True
    Exit Function
  End If
 
  Dim I, ListIndex As Integer
  For I = 1 To mLawDispositionCollection.Count
    If mLawDispositionCollection(I)(DatabaseName.LAWDISPOSITION_KEY) = mskDataField(0).Text Then
      ListIndex = I
      Exit For
    End If
  Next I
  
  If ListIndex = 0 Then
    MsgBox "Invalid law disposition id"
    cboDispositionDesc.ListIndex = -1
    Exit Function
  End If
  
  cboDispositionDesc.ListIndex = ListIndex - 1
  LawDispositionRefresh = True
End Function

Private Sub LawErrorRefresh()
  lvItem(1).ListItems.Clear
  
  Dim Row As Collection
  For Each Row In mLawError
    AddItem lvItem(1), "Key:" & Row(DatabaseName.LAWERROR_KEY), Row("lawErrorName"), , Row(DatabaseName.LAWERROR_KEY)
  Next Row

  Dim Items As Collection, I As Integer
  Set Items = Factory.InstanceService.LawErrorCollection(mClaimHandle)
  If Items Is Nothing Then
    Exit Sub
  End If
  
  For Each Row In Items
    I = I + 1
    lvItem(1).ListItems.item("Key:" & Row(DatabaseName.LAWERROR_KEY)).Selected = True
    lvItem(1).ListItems.item("Key:" & Row(DatabaseName.LAWERROR_KEY)).Checked = True
    Swap lvItem(1), I
  Next Row
End Sub

Public Sub ShowByPrimaryKey(ClaimId As String, Optional ShowTab As Integer)
  mClaimHandle = ClaimId
  mTab = ShowTab
  Show
End Sub

'
' Swap simply swaps the SortKey of the selected item with the
' desired index
'
Private Sub Swap(View As ListView, Index As Integer)
  View.Sorted = False
  
  Dim Selected As ListItem
  Set Selected = View.SelectedItem
  
  Dim item As ListItem
  Set item = View.ListItems(Index)
  
  Dim SortKey As String
  SortKey = Selected.ListSubItems("SortKey")
  Selected.ListSubItems("SortKey") = item.ListSubItems("SortKey")
  item.ListSubItems("SortKey") = SortKey
  
  View.Sorted = True
  View.ListItems(Index).Selected = True
End Sub

Private Sub cboClaimTypeName_Click()
  If cboClaimTypeName.ListIndex < 0 Then
    mClaimType = vbNullString
  Else
    mClaimType = mClaimTypes(cboClaimTypeName.ListIndex + 1)(DatabaseName.TYPE_FIELD)
  End If
  Dirty = True

End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cboDispositionDesc_Click()
  If cboDispositionDesc.ListIndex < 0 Then
    mskDataField(0).Text = "__"
  Else
    Dim dispositionHandle As String
    dispositionHandle = GetDataFromCollection(mLawDispositionCollection(cboDispositionDesc.ListIndex + 1), DatabaseName.LAWDISPOSITION_KEY, True)
    mskDataField(0).Text = dispositionHandle
  End If
  Dirty = True
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdFirm_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True
End Sub

Private Sub cmdPolicy_Click()
  Dispatcher.ShowSearch DatabaseName.POLICY_TABLE, FormMgr, True
End Sub


Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdItemDown_Click(Index As Integer)
  Swap lvItem(Index), lvItem(Index).SelectedItem.Index + 1
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdItemRemove_Click(Index As Integer)
  lvItem(Index).SelectedItem.Checked = False
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdItemSelect_Click(Index As Integer)
  lvItem(Index).SelectedItem.Checked = True
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdItemUp_Click(Index As Integer)
  Swap lvItem(Index), lvItem(Index).SelectedItem.Index - 1
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
   Set mDataSet = New ClaimDataset
   Set mDataSet.Session = LawSession
   
   Set mLawDisposition = New LawDispositionData
   Set mLawDisposition.Session = LawSession
   IsChanged = False
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  FormUtil.EditLoad Me
  FormMgr.Load Me
  
  lblInsurer.Caption = LocaleShortOrganization
  
  Set mLawArea = Factory.LawAreaService.NameCollection
  Set mLawError = Factory.LawErrorService.NameCollection
  
  '
  ' Populate EventTypeName Dropdown
  '
  Set mClaimTypes = GetTableColumnTypeCollection(DatabaseName.CLAIM_TABLE, DatabaseName.CLAIMTYPE_FIELD)
  FormUtil.ComboBoxLoad mClaimTypes, cboClaimTypeName, DatabaseName.DESCRIPTION_FIELD
  
  'Load the Law Disposition Combo box
  mLawDisposition.DataProvider.Sort = DatabaseName.LAWDISPOSITIONNAME_FIELD
  mLawDisposition.DataProvider.OpenRecordset
  Set mLawDispositionCollection = mLawDisposition.DataProvider.Collection
  If Not mLawDispositionCollection Is Nothing Then
    Dim I As Integer
    Dim item As ComboItem
    For I = 1 To mLawDispositionCollection.Count
      cboDispositionDesc.AddItem mLawDispositionCollection(I)(DatabaseName.LAWDISPOSITIONNAME_FIELD)
    Next I
  End If
  
  'Load the AdjusterNames ComboBox
  FormUtil.ComboBoxRegistryLoad cboDataField(0), "adjusternames"
  
  'Ensure first tab is open.
  If SSTab1.Tab <> mTab Then
    SSTab1.Tab = mTab
  End If
  
  FormRefresh
  FormResize
  
  FormUtil.TabControlEnable SSTab1
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvItem_Click(Index As Integer)
  ButtonRefresh Index
  Dirty = True
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileOpenClaim_Click()
  FormUtil.ShowByPrimaryKey New frmClaimOpen, mClaimHandle, FormMgr
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.CLAIM_TABLE, mClaimHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.CLAIM_REF, DatabaseName.CLAIM_TABLE, mClaimHandle, mClaimHandle & " " & lblDataField(1)
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  IsChanged = False
  ClearDataField txtDatafield
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.CLAIM_TABLE
End Sub


Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mskDataField_Validate(Index As Integer, Cancel As Boolean)
  If mskDataField(0).Text = "__" Then
    Exit Sub
  End If
  Cancel = Not LawDispositionRefresh
End Sub


Private Sub mskDataField_ValidationError(Index As Integer, InvalidText As String, StartPosition As Integer)
  MsgBox InvalidText
  MsgBox StartPosition
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code - Updated to reflect no mNew
  
  ' If there have been changes, and
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And Verify Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
    
  ' Begin custom code
  
  'ver 2.72.00 (jjm) 2005 05 06 - removed claimant search
  'ver 2.70.00 (jjm) 2004 09 20 - added claimant search
  'Check for a duplicate claimant
  'If IsNullClaimant Then
  '  If ClaimUtil.isDuplicateClaimantMSG(mClaimHandle, txtDatafield(TXT_CLAIMANT_INDEX).Text) Then
  '    Exit Function
  '  End If
  'End If
  
  
  Dim mDataControls As New Collection
  AddTextControl txtDatafield, mDataControls
  AddTextControl cboDataField, mDataControls
  AddTextControl mskDataField, mDataControls
  AddValueControl dtpDataField, mDataControls
  AddValueControl chkDataField, mDataControls
  
  'Fix the lawDispositionId data when there is nothing entered.
  'The Mask Edit box returns "__" when there is no data
  If mDataControls(DatabaseName.LAWDISPOSITION_KEY) = "__" Then
    mDataControls(DatabaseName.LAWDISPOSITION_KEY) = vbNullString
  End If
  
  
  ' Claim Edit does not support adding of new claims.  New claims
  ' created via browser based interface.

  Dim lawAreas() As String
  Dim lawErrors() As String
  lawAreas = ItemArray(lvItem(0))
  lawErrors = ItemArray(lvItem(1))
  
  'ver 2.20.0611 (jjm) Added Reinsurer Report fields
  'ver 2.40.02 (jjm) Included the mIncidentHandle variable instead of vbNullString
  Save = Factory.ClaimService.WriteClaim(mClaimHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.CLAIMANT_FIELD), _
        GetDate(lblDataField(3).Caption), _
        GetDataFromCollection(mDataControls, DatabaseName.OCCURDATERANGE_FIELD), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.OCCURDATE_FIELD)), _
        GetDataFromCollection(mDataControls, DatabaseName.KNOWDATERANGE_FIELD), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.KNOWDATE_FIELD)), _
        GetDate(GetDataFromCollection(mDatafields, DatabaseName.CLAIMMADEDATE_FIELD)), _
        GetDataFromCollection(mDataControls, DatabaseName.ADJUSTER_FIELD), _
        GetDate(GetDataFromCollection(mDatafields, DatabaseName.INCEPTIONDATE_FIELD)), _
        GetBoolean(GetDataFromCollection(mDatafields, DatabaseName.ISTAIL_FIELD)), _
        GetDouble(GetDataFromCollection(mDatafields, DatabaseName.TREATYYEAR_FIELD)), _
        GetDataFromCollection(mDataControls, DatabaseName.PLEADINGCAPTION_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.COURT_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.COURTCASENUM_FIELD), _
        GetDate(mDataControls(DatabaseName.INSUREDSERVEDATE_FIELD)), _
        GetDate(mDataControls(DatabaseName.INSURERSERVEDATE_FIELD)), _
        GetDate(mDataControls(DatabaseName.FIRMSERVEDATE_FIELD)), _
        mDataControls(DatabaseName.TRIALDATERANGE_FIELD), _
        mDataControls(DatabaseName.ISJUDGMENTCLAIMANT_FIELD), _
        GetBoolean(mDataControls(DatabaseName.ISFEEDISPUTE_FIELD)), _
        mDataControls(DatabaseName.LAWDISPOSITION_KEY), _
        mClaimType, txtNote.Text, lawAreas, lawErrors, _
        mDataControls(DatabaseName.ISREINSURERREPORT_FIELD), GetDate(mDataControls(DatabaseName.REINSURERREPORTDATE_FIELD)))

  If Save Then
    Dirty = False
    FormMgr.Message = fimSave
  End If
End Function

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDatafield(Index)
End Sub


Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
