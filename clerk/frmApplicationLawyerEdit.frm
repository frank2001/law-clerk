VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmApplicationLawyerEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lawyer"
   ClientHeight    =   3180
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   4095
   Icon            =   "frmApplicationLawyerEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   4095
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2856
      Left            =   0
      ScaleHeight     =   2850
      ScaleWidth      =   4095
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   384
      Width           =   4092
      Begin VB.CommandButton cmdLawyer 
         Caption         =   "&Lawyer"
         Height          =   288
         Left            =   2976
         TabIndex        =   15
         Top             =   216
         Width           =   1000
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   1560
         TabIndex        =   13
         Top             =   2352
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   288
         TabIndex        =   12
         Top             =   2352
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2832
         TabIndex        =   14
         Top             =   2352
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   1584
         Left            =   84
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   672
         Width           =   3948
         _ExtentX        =   6959
         _ExtentY        =   2805
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "frmApplicationLawyerEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label6"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label3"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtpDataField(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtDataField(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cboDataField(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         Begin VB.ComboBox cboDataField 
            DataField       =   "lawyerDesignationId"
            Height          =   315
            Index           =   0
            ItemData        =   "frmApplicationLawyerEdit.frx":0028
            Left            =   1452
            List            =   "frmApplicationLawyerEdit.frx":002A
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   444
            Width           =   2196
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "inPracticeDuration"
            DataMember      =   "ApplicationLawyer"
            Height          =   288
            Index           =   0
            Left            =   1452
            TabIndex        =   2
            Tag             =   "ApplicationLawyerEditPosition"
            Text            =   "inPracticeDuration"
            Top             =   780
            Width           =   936
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmApplicationLawyerEdit.frx":002C
            DataField       =   "retroactiveDate"
            DataMember      =   "ApplicationLawyer"
            Height          =   288
            Index           =   0
            Left            =   1452
            TabIndex        =   3
            Top             =   1116
            Width           =   2196
            _ExtentX        =   3863
            _ExtentY        =   503
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   61997057
            CurrentDate     =   37089
         End
         Begin MSComctlLib.ImageList imlUpDown 
            Left            =   2568
            Top             =   -240
            _ExtentX        =   794
            _ExtentY        =   794
            BackColor       =   -2147483643
            _Version        =   393216
         End
         Begin MSComctlLib.ImageList imlUpDownDisabled 
            Left            =   3084
            Top             =   -228
            _ExtentX        =   794
            _ExtentY        =   794
            BackColor       =   -2147483643
            MaskColor       =   12632256
            _Version        =   393216
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Years In Practice:"
            Height          =   192
            Left            =   96
            TabIndex        =   8
            Top             =   828
            Width           =   1248
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Lawyer RDI:"
            Height          =   192
            Left            =   96
            TabIndex        =   7
            Top             =   1164
            Width           =   864
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Position:"
            Height          =   192
            Left            =   120
            TabIndex        =   6
            Top             =   492
            Width           =   612
         End
      End
      Begin VB.CommandButton cmdEnter 
         Caption         =   "Enter"
         Height          =   288
         Left            =   1260
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   732
         Width           =   1056
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         Caption         =   "lawyer"
         DataField       =   "lawyerName"
         DataMember      =   "ApplicationLawyer"
         Height          =   192
         Index           =   0
         Left            =   132
         TabIndex        =   16
         Top             =   264
         UseMnemonic     =   0   'False
         Width           =   2832
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   4092
      _ExtentX        =   7223
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   4095
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   3975
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   30
         Width           =   3975
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   10
            Top             =   0
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Application Lawyer"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator830 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewLawyer 
            Caption         =   "&Lawyer"
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Application"
            Index           =   0
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Lawyer"
            Index           =   1
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditLawyer 
         Caption         =   "&Lawyer"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator997 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator666 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmApplicationLawyerEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.00.0306 (jjm) WILMIC
Option Explicit

Private mApplicationHandle As String
Private mLawyerHandle As String
Private mFirmHandle As String
Private mPolicyHandle As String
Private mEventHandle As String
Private mCoverageHandle As String
Private mLawyerDesignationHandle As String

Private mNew As Boolean
Private mDatafields As Collection
Private mDataSet As Dataset

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private FormMgr As New FormManager

Private mRefresh As Boolean

Private mDefaultRetroactiveDate As Date

'ver 2.00.0306 (jjm)
Private mLawyerInsuredCheck As Boolean
Private mPolicyLawyerCoverageDataset As Dataset

'Mantis 2733 - load LawyerDesignationList from data
Private mLawyerDesignation As Collection

Private Const LBL_LAWYERNAME_INDEX = 0
Private Const TXT_INPRACTICEDURATION_INDEX = 0
Private Const CBO_DESIGNATION_INDEX = 0
Private Const DTP_RETROACTIVEDATE_INDEX = 0

Public Property Get PolicyHandle() As String
  PolicyHandle = mPolicyHandle
End Property

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
      Exit Sub
    
    Case fimNew
      mnuFileNewLawyer_Click
      Exit Sub
    
    Case fimSelect
      mLawyerHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
      lblDatafield(LBL_LAWYERNAME_INDEX) = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.ENTITYNAME_FIELD)

      If fInterface.Key = DatabaseName.POLICYLAWYER_TABLE Then
        mCoverageHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.COVERAGE_KEY)
      End If
      
      Caption = "Edit Lawyer (" & mLawyerHandle & ")"
      FormPrepare
      
      'ver 2.00.0306 (jjm)
      If mLawyerInsuredCheck Then
        mPolicyLawyerCoverageDataset.DataProvider.filter = _
          DatabaseName.LAWYER_KEY & " = '" & mLawyerHandle & "' and " & _
          DatabaseName.EFFECTIVEDATE_FIELD & " <= " & LawDialect.DateFormat(Today) & " and " & _
          DatabaseName.EXPIRATIONDATE_FIELD & " >= " & LawDialect.DateFormat(Today) & " and " & _
          LawDialect.ColumnIsEmpty(DatabaseName.CANCELEFFECTIVEDATE_FIELD)
        mPolicyLawyerCoverageDataset.DataProvider.Column = _
          DatabaseName.LAWYER_KEY & "," & _
          DatabaseName.FIRM_KEY & "," & _
          DatabaseName.POLICY_KEY
          
        mPolicyLawyerCoverageDataset.DataProvider.OpenRecordset
        Dim Result As New Collection
        Set Result = mPolicyLawyerCoverageDataset.DataProvider.Collection
        If Result.Count > 0 Then
          Dim Warning As String
          Dim Row As Collection
          For Each Row In Result
            Warning = Warning & _
                      Factory.FirmService.GetFirmLocator(Row(DatabaseName.FIRM_KEY)) & vbTab & vbTab & vbTab & _
                      GetDataFromCollection(Row, DatabaseName.POLICY_KEY) & vbTab & vbTab & vbTab & _
                      GetDataFromCollection(Row, DatabaseName.POLICYNUMBER_FIELD) & vbCrLf
          Next
          MsgBox "Please note that the selected lawyer is currently insured under the following:" & vbCrLf & _
                 "-firmLocator-" & vbTab & vbTab & "-policyId-" & vbTab & vbTab & "-policyNumber-" & vbCrLf & _
                 Warning
        End If
          
      End If
      
      Dirty = True
      
    'when saving a new person for selecting a lawyer
    Case fimSave
      mLawyerHandle = fInterface.Child.LawyerHandle
      lblDatafield(LBL_LAWYERNAME_INDEX) = fInterface.Child.txtDataField(3)
      Caption = "Edit Lawyer (" & mLawyerHandle & ")"
      FormPrepare
      Dirty = True
  
  End Select
  Show
  cboDataField(CBO_DESIGNATION_INDEX).SetFocus
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Public Sub FormPrepare()
  Dim Model As Collection
  Set Model = Factory.ApplicationLawyerService.ApplicationLawyerPrepare(mApplicationHandle, mLawyerHandle)
  
  SetFields lblDatafield, Model
  SetFields txtDataField, Model
  SetFields dtpDataField, Model
  setLawyerDesignationById GetDataFromCollection(Model, DatabaseName.LAWYERDESIGNATION_KEY)
  
End Sub


Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  ClearDataField txtDataField
  ClearDataField lblDatafield
  ClearDataField cboDataField
  
  ' Set default retroactive date, if any.
  dtpDataField(DTP_RETROACTIVEDATE_INDEX).Value = IIf(mDefaultRetroactiveDate = vbNullDate, Today(), mDefaultRetroactiveDate)
  dtpDataField(DTP_RETROACTIVEDATE_INDEX).Value = Null
  
  If (mNew) Then
    Caption = "New Lawyer"
    FormUtil.GetComboDefault cboDataField(CBO_DESIGNATION_INDEX)
  Else
    Set mDatafields = Factory.ApplicationLawyerService.Find(mApplicationHandle, mLawyerHandle)
    Caption = "Edit Lawyer (" & mDatafields(DatabaseName.LAWYER_KEY) & ")"
    SetFields dtpDataField, mDatafields
    SetFields txtDataField, mDatafields
    SetFields lblDatafield, mDatafields
    'mCoverageHandle = mDatafields(DatabaseName.COVERAGE_KEY)
    
    mLawyerDesignationHandle = GetDataFromCollection(mDatafields, DatabaseName.LAWYERDESIGNATION_KEY, True)
    setLawyerDesignationById mLawyerDesignationHandle
  End If
  
  If mApplicationHandle <> vbNullString Then
    Dim AppData As Collection
    Set AppData = Factory.ApplicationService.Find(mApplicationHandle)
    mFirmHandle = GetDataFromCollection(AppData, DatabaseName.FIRM_KEY)
    mPolicyHandle = GetDataFromCollection(AppData, DatabaseName.POLICY_KEY)
    mEventHandle = GetDataFromCollection(AppData, DatabaseName.EVENT_KEY)
  End If
  
  
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  Dirty = False
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

' End FormInterface

Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mApplicationHandle = ListItemText(mListView.SelectedItem, DatabaseName.APPLICATION_KEY)
  mLawyerHandle = ListItemText(mListView.SelectedItem, DatabaseName.LAWYER_KEY)
End Property

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
'   End Standard Code
  
'   Begin Custom Code
  
  If txtDataField(TXT_INPRACTICEDURATION_INDEX) = vbNullString Then
    MsgBox "You must enter Years in Practice"
    Save = False
    Exit Function
  End If

  If mNew Then
    Dim Handle As Variant
    Handle = Factory.ApplicationLawyerService.CreateApplicationLawyer( _
                  mApplicationHandle, _
                  mLawyerHandle, _
                  GetDate(dtpDataField(DTP_RETROACTIVEDATE_INDEX).Value), _
                  GetDouble(txtDataField(TXT_INPRACTICEDURATION_INDEX).Text), _
                  mLawyerDesignationHandle, _
                  mCoverageHandle)
    Save = Not VarType(Handle) = vbEmpty
    If Save Then
      mNew = False
      mLawyerHandle = Handle(1)
    End If
  Else
    Save = Factory.ApplicationLawyerService.WriteApplicationLawyer( _
                  mApplicationHandle, _
                  mLawyerHandle, _
                  lblDatafield(LBL_LAWYERNAME_INDEX).Caption, _
                  GetDate(dtpDataField(DTP_RETROACTIVEDATE_INDEX).Value), _
                  GetDouble(txtDataField(TXT_INPRACTICEDURATION_INDEX).Text), _
                  mLawyerDesignationHandle, _
                  mCoverageHandle)
  End If
  
  Dirty = Not Save
'   End Custom Code
End Function

Private Sub setLawyerDesignationById(lawyerDesignationId As String)
    cboDataField(CBO_DESIGNATION_INDEX).ListIndex = CollectionFind(mLawyerDesignation, DatabaseName.LAWYERDESIGNATION_KEY, lawyerDesignationId) - 1
End Sub


Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no lawyers to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

'ver 2.00.0306 (jjm)
Public Sub ShowNew(ApplicationId As String, Optional LawyerInsuredCheck As Boolean)
  mApplicationHandle = ApplicationId
  mNew = True
  Show
  Refresh
  cmdLawyer.SetFocus
  cmdLawyer_Click
  mLawyerInsuredCheck = LawyerInsuredCheck
End Sub


Public Sub ShowByPrimaryKey(ApplicationId As String, LawyerId As String)
  mApplicationHandle = ApplicationId
  mLawyerHandle = LawyerId
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Select Case Index
    Case CBO_DESIGNATION_INDEX
      If cboDataField(Index).ListIndex < 0 Then
        mLawyerDesignationHandle = vbNullString
      Else
        mLawyerDesignationHandle = mLawyerDesignation(cboDataField(Index).ListIndex + 1)(DatabaseName.LAWYERDESIGNATION_KEY)
      End If
  End Select


  Dirty = True
End Sub

'
' Control/Object Events
'

'
' Standard Cancel Command Button.
'
Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  PostMessage hWnd, WM_KEYDOWN, vbKeyTab, 0
End Sub

'
' Select a lawyer.
'
Private Sub cmdLawyer_Click()
  'This is how it is shown in the folder off of frmMain
  Dim EventType As String
  Dim ShowCoverage As Boolean
  
  If Not mEventHandle = vbNullString Then
    EventType = GetDataFromCollection(Factory.EventService.Find(mEventHandle, DatabaseName.EVENTTYPE_KEY), DatabaseName.EVENTTYPE_KEY)
    ShowCoverage = Factory.EventService.IsEventTypeInGroup(EventType, DatabaseName.APPLAWYERCOVERAGE_RULE)
  End If
  
  If ShowCoverage Then
    Dispatcher.ShowSearch DatabaseName.POLICYLAWYER_TABLE, FormMgr, True, , 2, , "Select a Lawyer"
  Else
    Dispatcher.ShowSearch DatabaseName.LAWYER_TABLE, FormMgr, True
  End If
End Sub

Private Sub cmdSave_Click()
  If Save Then
    FormUtil.SetComboDefault cboDataField(0)
    Unload Me
  End If
End Sub

Private Sub cmdSaveNew_Click()
  If Save Then mnuFileNew_Click
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
   Set mDataSet = NewGenericDataset(DatabaseName.APPLICATIONLAWYER_TABLE)
   
   Set mPolicyLawyerCoverageDataset = New PolicyLawyerCoverageDataset
   Set mPolicyLawyerCoverageDataset.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If

End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  ' get effective date for default retroactive date
  mDefaultRetroactiveDate = GetDataFromCollection(Factory.ApplicationService.Find(mApplicationHandle, DatabaseName.EFFECTIVEDATE_FIELD), DatabaseName.EFFECTIVEDATE_FIELD)
  
  'Mantis 2733 - load LawyerDesignationList from data
  Dim designationData As Dataset
  Set designationData = NewGenericDataset(DatabaseName.LAWYERDESIGNATION_TABLE)
  designationData.DataProvider.Column = DatabaseName.LAWYERDESIGNATION_KEY & ", " & DatabaseName.LAWYERDESIGNATIONNAME_FIELD
  designationData.DataProvider.Sort = DatabaseName.LAWYERDESIGNATIONNAME_FIELD
  designationData.DataProvider.OpenRecordset
  Set mLawyerDesignation = designationData.DataProvider.Collection
  FormUtil.ComboBoxLoad mLawyerDesignation, cboDataField(CBO_DESIGNATION_INDEX), DatabaseName.LAWYERDESIGNATIONNAME_FIELD
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuEditLawyer_Click()
  If (mLawyerHandle = vbNullString) Then
    MsgBox "No lawyer has been associated with this application."
    Exit Sub
  End If
  FormUtil.ShowByPrimaryKey New frmPersonEdit, mLawyerHandle, FormMgr
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mLawyerHandle = vbNullString
  mnuViewRefresh_Click
  cmdLawyer.SetFocus
  cmdLawyer_Click
End Sub

Private Sub mnuFileNewLawyer_Click()
  Dim firmId As String
  firmId = Factory.ApplicationService.Find(mApplicationHandle, DatabaseName.FIRM_KEY)(DatabaseName.FIRM_KEY)
  Dim fPerson As New frmPersonEdit
  FormMgr.Add fPerson, "LawyerNew"
  fPerson.ShowNew True, firmId
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmApplicationOpen, mApplicationHandle, FormMgr
    Case 1
      FormUtil.ShowByPrimaryKey New frmLawyerOpen, mLawyerHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.APPLICATIONLAWYER_TABLE, _
    DatabaseName.APPLICATION_KEY, DatabaseName.LAWYER_KEY, mApplicationHandle, mLawyerHandle
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub
