VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PrintText"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'==============================================================================
'Contains properties of a text-element (text, alignment and font).
'Code is ThirdParty and has only been modified a little.
'Is used by CPrintListView.
'
'Scope:     DLL-Public creatable.
'==============================================================================

Option Explicit

Private m_Text As String
Private m_Alignment As AlignmentConstants
Private m_Font As StdFont

' The Font property

Public Property Get Font() As StdFont
    Set Font = m_Font
End Property

Public Property Set Font(newFont As StdFont)
    ' copy the font's attributes, one by one
    Set m_Font = New StdFont
    With m_Font
        .Name = newFont.Name
        .Size = newFont.Size
        .Bold = newFont.Bold
        .Italic = newFont.Italic
        .Strikethrough = newFont.Strikethrough
        .Underline = newFont.Underline
    End With
End Property

' The Alignment property

Public Property Get Alignment() As AlignmentConstants
    Alignment = m_Alignment
End Property

Public Property Let Alignment(ByVal newValue As AlignmentConstants)
    m_Alignment = newValue
End Property

Private Sub Class_Initialize()
    Set m_Font = New StdFont
    m_Font.Name = "Arial"
    m_Font.Size = 10
End Sub

' The Text property

Public Property Get Text() As String
    Text = m_Text
End Property

Public Property Let Text(ByVal newValue As String)
    m_Text = newValue
End Property

Private Sub Class_Terminate()
    Set m_Font = Nothing
End Sub
