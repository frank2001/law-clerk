VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AttributeService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mDataSet As Dataset


Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.ATTRIBUTE_TABLE)
End Sub

Public Function GetAttributesCollection(GroupHandle As String)
  mDataSet.DataProvider.Column = "attributeId, attributeName"
  mDataSet.DataProvider.Filter = DatabaseName.GROUP_KEY & " = '" & GroupHandle & "'"
  mDataSet.DataProvider.Sort = "attributeName"
  mDataSet.DataProvider.OpenRecordset
  Set GetAttributesCollection = mDataSet.DataProvider.Collection
End Function


'ToDo - Attribute type?  Dont worry about it
Public Function NameCollection() As Collection
  mDataSet.DataProvider.Column = "attributeId, attributeName"
  mDataSet.DataProvider.Filter = vbNullString
  mDataSet.DataProvider.Sort = "attributeName"
  mDataSet.DataProvider.OpenRecordset
  Set NameCollection = mDataSet.DataProvider.Collection
End Function


