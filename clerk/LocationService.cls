VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LocationService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.70.00 (jjm) 2004 09 27

Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "LocationService"
Private mDataSet As Dataset

'2.70.00 (jjm) 2004 09 23
Private mStateCity As Collection
Private mStateCityViewDataSet As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Set mDataSet = NewGenericDataset(DatabaseName.LOCATION_TABLE)
  Set mStateCityViewDataSet = NewGenericDataset(DatabaseName.STATECITY_VIEW)
  mStateCityViewDataSet.DataProvider.Column = DatabaseName.PLACENAME_FIELD & "," & DatabaseName.MAINCOUNTYNAME_FIELD
  mStateCityViewDataSet.DataProvider.Sort = DatabaseName.PLACENAME_FIELD
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.LOCATION_KEY, Handle, Column)
End Function


'
' Interface to server
'

'parameterOrder='locationId locationTypeId firmId companyName addressLine1 addressLine2
' addressLine3 city county state zip country telephone fax email attributeNames attributeValues parameterNames parameterValues'>

'
' Create an application.
'
Public Function CreateLocation( _
    LocationTypeId As String, _
    entityId As String, _
    AddressLine1 As String, _
    AddressLine2 As String, _
    AddressLine3 As String, _
    City As String, _
    County As String, _
    State As String, _
    Zip As String, _
    Country As String, _
    Telephone As String, _
    Fax As String, _
    Email As String, _
    eventNote As String) As String
    
On Error GoTo ErrorHandler
  
  CreateLocation = Client.Create( _
    LocationTypeId, _
    entityId, _
    AddressLine1, _
    AddressLine2, _
    AddressLine3, City, County, _
    State, Zip, Country, _
    Telephone, Fax, Email, _
    eventNote)
  Exit Function
  
ErrorHandler:
    ErrorUtil.DisplaySoapError Err, Client
    'MsgBox "Fault: " & Client.FaultString, vbExclamation
End Function

'
' Write a Location
'
Public Function WriteLocation( _
    LocationHandle As String, LocationTypeId As String, _
    EntityHandle As String, _
    AddressLine1 As String, AddressLine2 As String, _
    AddressLine3 As String, City As String, County As String, _
    State As String, Zip As String, Country As String, _
    Telephone As String, Fax As String, Email As String, _
    eventNote As String) As Boolean
    
On Error GoTo ErrorHandler
  
  Call Client.Write( _
      LocationHandle, LocationTypeId, _
      EntityHandle, _
      AddressLine1, AddressLine2, _
      AddressLine3, City, County, _
      State, Zip, Country, _
      Telephone, Fax, Email, _
      eventNote)
  WriteLocation = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  'MsgBox "Fault: " & Client.FaultString, vbExclamation
  WriteLocation = False
End Function

'
' Delete an Location
'
Public Function DeleteLocation(EntityHandle As String, LocationHandle As String) As Boolean

On Error GoTo ErrorHandler
  'Mantis 2523
  Call Client.Delete(EntityHandle, LocationHandle)
  DeleteLocation = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  'MsgBox "Fault: " & Client.FaultString, vbExclamation
  DeleteLocation = False
End Function


Public Function GetCitiesByStateCollection(stateId As String) As Collection
  mStateCityViewDataSet.DataProvider.Filter = DatabaseName.STATE_KEY & " = '" & stateId & "'"
  mStateCityViewDataSet.DataProvider.OpenRecordset 7000
  Set GetCitiesByStateCollection = mStateCityViewDataSet.DataProvider.Collection
End Function

'2.70.00 (jjm) 2004 09 27
Public Function GetCountyByStateCity(stateId As String, PlaceName As String) As String
  Dim I As Integer
  For I = 1 To mStateCity.Count
    If mStateCity(I).stateId = stateId Then
      GetCountyByStateCity = mStateCity(I).GetCountyByPlaceName(PlaceName)
      Exit For
    End If
  Next I
End Function

'To be used when there is a locationId available on the view
Public Function GetLocationAddress(LocationHandle As String, Optional RecipientName As String) As String
  'Get and format data
  Dim LocationData As Collection
  Set LocationData = Find(LocationHandle)
  
  'replace old companyName with the Location/Entity-Main/entityName
  Dim entityName, mainEntityHandle As String
  mainEntityHandle = GetDataFromCollection(LocationData, DatabaseName.MAINENTITYID_FIELD)
  entityName = DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, DatabaseName.ENTITY_KEY, mainEntityHandle, DatabaseName.ENTITYNAME_FIELD)
  
  GetLocationAddress = _
    GetLocationLine(RecipientName) & _
    GetLocationLine(IIf(RecipientName = entityName, vbNullString, entityName)) & _
    GetLocationLine(GetDataFromCollection(LocationData, DatabaseName.ADDRESSLINE2_FIELD)) & _
    GetLocationLine(GetDataFromCollection(LocationData, DatabaseName.ADDRESSLINE1_FIELD)) & _
    GetLocationLine(GetDataFromCollection(LocationData, DatabaseName.ADDRESSLINE3_FIELD)) & _
    UCase(GetDataFromCollection(LocationData, DatabaseName.CITY_FIELD) & " " & _
          GetDataFromCollection(LocationData, DatabaseName.STATE_FIELD) & "  " & _
          GetDataFromCollection(LocationData, DatabaseName.ZIP_FIELD))

End Function
'Helper function for GetLocationAddress
Private Function GetLocationLine(Text) As String
  GetLocationLine = IIf(Text = vbNullString, vbNullString, (Replace(UCase(Text), ".", vbNullString) & vbCrLf))
End Function


Private Function GetStateCityCollection() As Collection
  Dim StateDataset As Dataset
  Set StateDataset = NewGenericDataset(DatabaseName.STATE_TABLE)
  StateDataset.DataProvider.OpenRecordset 1000
  
  'collection to access the dataproviders results
  Dim StateList As Collection
  Set StateList = StateDataset.DataProvider.Collection
      
  If Not StateList Is Nothing Then
    'working collection to parse the dataproviders results
    Dim tempStateCity As Collection
    Set tempStateCity = New Collection
    
    Dim I As Integer
    Dim tmpStateId As String
    Dim tmpStateName As String
    For I = 1 To StateList.Count
      tmpStateId = GetDataFromCollection(StateList(I), DatabaseName.STATE_KEY)
      tmpStateName = GetDataFromCollection(StateList(I), DatabaseName.STATENAME_FIELD)
      tempStateCity.Add NewStateCity(tmpStateId, tmpStateName)
    Next I
    Set GetStateCityCollection = tempStateCity
  Else
    MsgBox "Unable to load state and city information", vbCritical
    Set GetStateCityCollection = Nothing
  End If

End Function

'2.70.00 (jjm) 2004 09 27
Public Sub LoadCitiesByState(cboCityList As ComboBox, stateId As String)
  
  'Check to see if the state's city has been already loaded
  
  'find the state item
  Dim StateIndex As Integer
  Dim I As Integer
  For I = 1 To mStateCity.Count
    If mStateCity(I).stateId = stateId Then
      StateIndex = I
      Exit For
    End If
  Next I
  
  If StateIndex = 0 Then
    Exit Sub
  End If
  
  'Popuplate combobox
  cboCityList.Clear
  ComboBoxLoad mStateCity(StateIndex).CityList, cboCityList, DatabaseName.PLACENAME_FIELD
End Sub

'2.70.00 (jjm) 2004 09 27
Public Sub LoadStates(cboStateList As ComboBox, Optional DefaultStateCode As String)
  'Load states into data cache collection
  If mStateCity Is Nothing Then
    Set mStateCity = GetStateCityCollection
    'test a second time
    If mStateCity Is Nothing Then
      Exit Sub
    End If
  End If
  
  
  'Popuplate combobox via collection
  Dim n As Integer
  For n = 1 To mStateCity.Count
    cboStateList.AddItem mStateCity(n).stateId
  Next n
  
  If DefaultStateCode <> vbNullString Then
    Dim I As Integer
    For I = cboStateList.ListCount - 1 To 0 Step -1
      If cboStateList.List(I) = DefaultStateCode Then
        cboStateList.ListIndex = I
        Exit For
      End If
    Next I
  End If
    
End Sub


Private Function NewStateCity(v_StateId As String, v_StateName As String) As StateCity
  Set NewStateCity = New StateCity
  NewStateCity.StateCity v_StateId, v_StateName
End Function

