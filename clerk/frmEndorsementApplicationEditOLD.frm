VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEndorsementApplicationEditOLD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Endorsement Application"
   ClientHeight    =   4332
   ClientLeft      =   120
   ClientTop       =   600
   ClientWidth     =   4824
   Icon            =   "frmEndorsementApplicationEditOLD.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4332
   ScaleWidth      =   4824
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4236
      Left            =   0
      ScaleHeight     =   4236
      ScaleWidth      =   4824
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   0
      Width           =   4824
      Begin VB.CommandButton cmdPolicy 
         Caption         =   "&Policy"
         Height          =   288
         Left            =   3636
         TabIndex        =   7
         Top             =   180
         Width           =   1000
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   2136
         TabIndex        =   5
         Top             =   3828
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   3444
         TabIndex        =   6
         Top             =   3828
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3048
         Left            =   72
         TabIndex        =   8
         Top             =   648
         Width           =   4572
         _ExtentX        =   8065
         _ExtentY        =   5376
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmEndorsementApplicationEditOLD.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label14"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label3"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblDatafield(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label4"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtpDataField(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtpDataField(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtDataField(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cboDataField(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmEndorsementApplicationEditOLD.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "cmdClearNotepad"
         Tab(1).Control(1)=   "txtNote"
         Tab(1).ControlCount=   2
         Begin VB.ComboBox cboDataField 
            Height          =   288
            Index           =   0
            ItemData        =   "frmEndorsementApplicationEditOLD.frx":0044
            Left            =   1596
            List            =   "frmEndorsementApplicationEditOLD.frx":004E
            Style           =   2  'Dropdown List
            TabIndex        =   0
            Top             =   420
            Width           =   2052
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74928
            TabIndex        =   4
            Top             =   2640
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   2244
            Left            =   -74940
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   288
            Width           =   4428
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "EndorsementTypeId"
            Height          =   288
            Index           =   0
            Left            =   3300
            TabIndex        =   16
            TabStop         =   0   'False
            Text            =   "EndorsementTypeId"
            Top             =   24
            Visible         =   0   'False
            Width           =   744
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmEndorsementApplicationEditOLD.frx":006B
            DataField       =   "receiveDate"
            Height          =   288
            Index           =   0
            Left            =   1596
            TabIndex        =   1
            Top             =   756
            Width           =   1404
            _ExtentX        =   2477
            _ExtentY        =   508
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   48037889
            CurrentDate     =   37088
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmEndorsementApplicationEditOLD.frx":0076
            DataField       =   "effectiveDate"
            Height          =   288
            Index           =   1
            Left            =   1596
            TabIndex        =   2
            Top             =   1092
            Width           =   1404
            _ExtentX        =   2477
            _ExtentY        =   508
            _Version        =   393216
            Format          =   48037889
            CurrentDate     =   37088
         End
         Begin VB.Label Label4 
            Caption         =   "Endorsement Type"
            Height          =   216
            Left            =   120
            TabIndex        =   18
            Top             =   456
            Width           =   1464
         End
         Begin VB.Label lblDatafield 
            AutoSize        =   -1  'True
            Caption         =   "applicationStatus"
            DataField       =   "applicationStatus"
            Height          =   192
            Index           =   0
            Left            =   1584
            TabIndex        =   14
            Top             =   1452
            UseMnemonic     =   0   'False
            Width           =   1236
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Index           =   8
            Left            =   120
            TabIndex        =   10
            Top             =   1136
            Width           =   1032
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Date &Rec'd:"
            Height          =   192
            Left            =   120
            TabIndex        =   9
            Top             =   808
            Width           =   852
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Application Status:"
            Height          =   192
            Left            =   120
            TabIndex        =   13
            Top             =   1464
            Width           =   1320
         End
      End
      Begin VB.Label lblDatafield 
         AutoSize        =   -1  'True
         DataField       =   "policyId"
         Height          =   192
         Index           =   1
         Left            =   144
         TabIndex        =   17
         Top             =   228
         Width           =   36
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   276
      TabIndex        =   15
      Top             =   780
      Width           =   1056
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Application"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator3872 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Contact (Firm)"
            Index           =   0
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Firm"
            Index           =   1
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Application"
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuS4r324 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Firm"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   1
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   2
      End
      Begin VB.Menu mnuSeparatro2823 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact (Firm)"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator8371 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator86732 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&Workflow Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About..."
      End
   End
End
Attribute VB_Name = "frmEndorsementApplicationEditOLD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mApplicationHandle As String
Private mFirmHandle As String
Private mEventHandle As String
Private mPolicyHandle As String
Private mNew As Boolean
'Private mDatafields As Collection
Private mDataSet As Dataset

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mCancel As Boolean

' Internal Id Fields
Private mLeft As Integer
Private mTop As Integer

Private mParameterNames() As String
Private mParameterValues() As String

'Private mEndorsementTypeCollection As Collection
'Private mEndorsementTypeDataset As Dataset

' Form Manager
Private FormMgr As New FormManager

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Child is either a Firm
' select (Firm) from cmdFirm or a new firm (NewFirm) from mnuFileNewFirm.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form requested new firm.
    Case fimNew
      mnuFileNewOther_Click 1
      Exit Sub
    
    ' Select form selected
    Case fimSelect
      mPolicyHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.POLICY_KEY)
      lblDataField(1).Caption = mPolicyHandle
      'Prepare
      FormRefresh
      
    ' New Firm has been saved via mnuFileNewFirm.
    Case fimSave
     ' If fInterface.Key = DatabaseName.FIRM_TABLE & "Edit" Then
     '   mFirmHandle = fInterface.Child.FirmHandle
     '   Prepare
     ' End If
  End Select
  Show
  cboDataField(0).SetFocus
End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub


Private Sub FormRefresh()

  Dim Datafields As Collection
  
  If (mNew) Then
     Caption = "New Endorsement Application"
     
     lblDataField(0) = ""

     
     ' Defaults in case prepare doesn't work
     dtpDataField(1) = Today()
     Prepare
          
     dtpDataField(0) = Today()
     mEventHandle = vbNullString
  Else
    Set Datafields = Factory.ApplicationService.Find(mApplicationHandle)
    SetFields txtDataField, Datafields
    SetFields dtpDataField, Datafields
    SetFields lblDataField, Datafields
    SetFields cboDataField, Datafields
    
    mApplicationHandle = Datafields.Item("applicationId")
    mFirmHandle = Datafields.Item("firmId")
    Caption = "Endorsement Application Edit (" & mApplicationHandle & ")"
    
    mEventHandle = Datafields.Item(DatabaseName.EVENT_KEY)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
  End If
      
  mnuFileOpen.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  
  Dirty = False
End Sub

Public Sub Prepare()
'  If mFirmHandle = vbNullString Then Exit Sub

'  Dim xmlHttp As xmlHttp, Model As Collection
'  Set xmlHttp = XMLUtil.HttpGet(HttpServer & WebApplication & WebService & "applicationprepare?key=" + mFirmHandle & "&source=text/xml")
'  Set Model = XMLUtil.Rowset(xmlHttp.responseXML.documentElement, "application")
'  SetFields txtDataField, Model(1)
'  SetFields dtpDataField, Model(1)
'  SetFields lblDatafield, Model(1)
'  txtDataField(2).Text = "12"
End Sub


Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Start custom Code
  
  ' Client side duplication.
'  If mNew And Factory.ContactService.FindByFirm(mFirmHandle, "Firm") Is Nothing Then
'    Select Case MsgBox("You can not save a new application without creating a firm contact.  Would you like to create a new firm contact now?", vbYesNo)
'      Case vbYes
'        mnuFileNewOther_Click 0
'        Exit Function
'      Case Else
'        Exit Function
'    End Select
'  End If
  
  
  '
  ' Save Data
  '
  
  Dim mDataControls As New Collection
  AddValueControl dtpDataField, mDataControls
  
'  Dim ApplicationTypeId As String
'  Select Case cboDataField(0).Text
'    Case "Reissue Application": ApplicationTypeId = "R"
'    Case "New Business": ApplicationTypeId = "N"
'  End Select
  
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  ParameterNames = mParameterNames
  ParameterValues = mParameterValues
  AddParameter "ApplicationEventNote", _
      IIf(txtNote.Text = vbNullString, " ", txtNote.Text), _
      ParameterNames, ParameterValues
  
  If (mNew) Then
'    mApplicationHandle = Factory.ApplicationService.CreateApplication( _
'              vbNullString, _
'              mFirmHandle, vbNullString, ApplicationTypeId, txtDataField(3), GetDate(mDataControls("effectiveDate")), GetDouble(txtDataField(1)), _
'              GetDouble(txtDataField(2)), GetDate(mDataControls("receiveDate")), _
'              vbNullStringArray, vbNullStringArray, ParameterNames, ParameterValues)
'    Save = mApplicationHandle <> vbNullString
  Else
'    Save = Factory.ApplicationService.WriteApplication(mApplicationHandle, _
'        mFirmHandle, vbNullString, ApplicationTypeId, txtDataField(3), GetDate(mDataControls("effectiveDate")), GetDouble(txtDataField(1)), _
'        GetDouble(txtDataField(2)), GetDate(mDataControls("receiveDate")), _
'        vbNullStringArray, vbNullStringArray, ParameterNames, ParameterValues)
  End If
  
  If Save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  Dirty = Not Save
End Function

Public Sub ShowNew(Optional PolicyHandle As String)
  mPolicyHandle = PolicyHandle
  mNew = True
  Show
  
  ' ToDo:  Should be an ini setting.
  cboDataField(0).ListIndex = 0
  Dirty = False
  
  If mPolicyHandle = vbNullString Then
    cmdPolicy.SetFocus
    cmdPolicy_Click
  Else
    cboDataField(0).SetFocus
  End If
End Sub

Public Sub ShowByPrimaryKey(ApplicationID As String)
  mApplicationHandle = ApplicationID
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Open up a select form for policy.  The response to the select
' is processed under FormChildUnload.
'
Private Sub cmdPolicy_Click()
  Dispatcher.ShowSearch DatabaseName.POLICY_TABLE, FormMgr, True
End Sub

Private Sub cmdSave_Click()
  Dim WasNew As Boolean
  WasNew = mNew
  If Save Then
    FormMgr.Message = fimSave
    If TypeOf FormMgr.Parent Is frmApplicationOpen Then
      Unload Me
      Exit Sub
    End If
    
    Unload Me
    
    If WasNew Then
      ' DON'T register FormMgr.
      FormUtil.ShowByPrimaryKey New frmEndorsementApplicationOpen, mApplicationHandle
    End If
  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_Initialize()
  Set mDataSet = New ApplicationDataset
  Set mDataSet.Session = LawSession
   
  'Set mEndorsementTypeDataset = NewGenericDataset(DatabaseName.ENDORSEMENTTYPE_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyF
        mnuFileNewOther_Click 1
        KeyCode = 0
    End Select
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  'Custom Accelerators
  KeyPreview = True
  mnuFileNewOther(1).Caption = mnuFileNewOther(1).Caption + vbTab + "Ctrl+Shift+F"
  
  FormUtil.EditLoad Me
  FormMgr.Load Me
 
  Factory.FolderParameterService.ParameterLoad DatabaseName.APPLICATION_TABLE, lawActionNew, mParameterNames, mParameterValues
    
'  mEndorsementTypeDataset.DataProvider.OpenRecordset
'  Set mEndorsementTypeCollection = mEndorsementTypeDataset.DataProvider.Collection
'  If Not mEndorsementTypeCollection Is Nothing Then
'    Dim I As Integer
'    Dim Item As ComboItem
'    For I = 1 To mEndorsementTypeCollection.Count
'      cboDataField(0).AddItem mEndorsementTypeCollection(I)("endorsementTypeId")
'    Next I
'  End If

  FormRefresh
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = True
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Resize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub



Private Sub mnuEdit_Click(Index As Integer)
  If mFirmHandle = vbNullString Then
    MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
    Exit Sub
  End If
  Select Case Index
    Case 0
 '     Dim fContact As New frmContactEdit
 '     FormMgr.Add fContact
 '     fContact.ShowByFirm mFirmHandle, "Firm"
    Case 1
      FormUtil.ShowByPrimaryKey New frmFirmEdit, mFirmHandle, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mApplicationHandle = vbNullString
  mFirmHandle = vbNullString
  FormRefresh
  cmdPolicy.SetFocus
  cmdPolicy_Click
End Sub

Private Sub mnuFileNewOther_Click(Index As Integer)
  Select Case Index
    Case 0
 '     If mFirmHandle = vbNullString Then
 '       MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
 '       Exit Sub
 '     End If
 '     Dim Contacts As Collection
 '     Set Contacts = Factory.ContactService.FindByFirm(mFirmHandle, "Firm")
 '     If Contacts Is Nothing Then
 '       Dim fContact As New frmContactEdit
 '       FormMgr.Add fContact
 '       fContact.ShowNewByFirm mFirmHandle, Factory.FirmService.Find(mFirmHandle, """locationId""")("locationId")
'      Else
'        MsgBox "Contact record already exists for this firm."
'        Exit Sub
'      End If
    Case 1
      FormUtil.ShowNew New frmPolicyEdit, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub

'
' Check for changes and open application
'
Private Sub mnuFileOpen_Click()
  FormUtil.ShowByPrimaryKey New frmApplicationOpen, mApplicationHandle, FormMgr
End Sub

Private Sub mnuFileOpenOther_Click(Index As Integer)
  Select Case Index
    Case 0
      If mFirmHandle = vbNullString Then
        MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.APPLICATION_TABLE, mApplicationHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.APPLICATION_REF, DatabaseName.APPLICATION_TABLE, mApplicationHandle, lblDataField(2).Caption
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog DatabaseName.APPLICATION_TABLE, mApplicationHandle, Index
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  Dim fAbout As New frmAbout
  fAbout.Show vbModal, Me
End Sub




Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.APPLICATION_TABLE, lawActionNew, _
      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub


Private Sub txtDataField_Change(Index As Integer)
  Dirty = True
'  If Index = 0 Then
'    Select Case txtDataField(0).Text
'      Case "N": cboDataField(0).ListIndex = 0
'      Case "R": cboDataField(0).ListIndex = 1
'    End Select
'  End If
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
