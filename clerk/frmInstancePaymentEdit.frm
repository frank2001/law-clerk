VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmInstancePaymentEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Unallocated Payment"
   ClientHeight    =   5205
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   7905
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5205
   ScaleWidth      =   7905
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4800
      Left            =   0
      ScaleHeight     =   4800
      ScaleWidth      =   7905
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   384
      Width           =   7905
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   4140
         TabIndex        =   7
         Top             =   4305
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   5364
         TabIndex        =   8
         Top             =   4305
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   6600
         TabIndex        =   9
         Top             =   4305
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3450
         Left            =   45
         TabIndex        =   25
         Top             =   720
         Width           =   7815
         _ExtentX        =   13785
         _ExtentY        =   6085
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   4
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmInstancePaymentEdit.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label3"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label4"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label6"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Label17"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lvGL"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cboEventTypeName"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtpDataField(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtDataField(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtDataField(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtDataField(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtDataField(30)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmInstancePaymentEdit.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).Control(1)=   "cmdClearNotepad"
         Tab(1).ControlCount=   2
         Begin VB.TextBox txtNote 
            Height          =   2640
            Left            =   -74910
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   27
            Top             =   345
            Width           =   7575
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74895
            TabIndex        =   26
            Top             =   3090
            Width           =   1028
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            DataField       =   "eventAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Event"
            Height          =   288
            Index           =   30
            Left            =   6468
            TabIndex        =   5
            Text            =   "eventAmount"
            Top             =   1284
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "referenceNumber"
            DataMember      =   "Disbursement"
            Height          =   288
            Index           =   13
            Left            =   5736
            TabIndex        =   1
            Text            =   "referenceNumber"
            Top             =   612
            Width           =   1212
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "payee"
            DataMember      =   "Disbursement"
            Height          =   288
            Index           =   1
            Left            =   1200
            TabIndex        =   3
            Text            =   "payee"
            Top             =   1284
            Width           =   2436
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "checkNumber"
            DataMember      =   "Disbursement"
            Height          =   288
            Index           =   0
            Left            =   84
            TabIndex        =   2
            Text            =   "checkNumber"
            Top             =   1284
            Width           =   1068
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmInstancePaymentEdit.frx":0038
            DataField       =   "eventDate"
            DataMember      =   "Event"
            Height          =   288
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Top             =   588
            Width           =   1212
            _ExtentX        =   2143
            _ExtentY        =   503
            _Version        =   393216
            Format          =   109838337
            CurrentDate     =   37116
         End
         Begin VB.ComboBox cboEventTypeName 
            Height          =   315
            Left            =   3696
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   1284
            Width           =   2724
         End
         Begin MSComctlLib.ListView lvGL 
            Height          =   1575
            Left            =   1800
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "ColumnFixed"
            Top             =   1710
            Width           =   5880
            _ExtentX        =   10372
            _ExtentY        =   2778
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "glAccountId"
               Text            =   "Account"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "transactionName"
               Text            =   "Name"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "transactionReference"
               Text            =   "Reference"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "transactionAmount"
               Text            =   "Amount"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Debit"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Credit"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label Label17 
            Caption         =   "Amount:"
            Height          =   192
            Left            =   6468
            TabIndex        =   24
            Top             =   1032
            Width           =   1092
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Invoice:"
            Height          =   192
            Index           =   1
            Left            =   5016
            TabIndex        =   21
            Top             =   660
            Width           =   552
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Accounting Records:"
            Height          =   192
            Left            =   120
            TabIndex        =   16
            Top             =   1740
            Width           =   1488
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Payment type:"
            Height          =   192
            Left            =   3708
            TabIndex        =   15
            Top             =   1032
            Width           =   1092
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Payee:"
            Height          =   192
            Left            =   1200
            TabIndex        =   14
            Top             =   1032
            Width           =   516
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Check no:"
            Height          =   192
            Index           =   0
            Left            =   84
            TabIndex        =   13
            Top             =   1032
            Width           =   708
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Payment Date:"
            Height          =   192
            Left            =   120
            TabIndex        =   12
            Top             =   336
            Width           =   1092
            WordWrap        =   -1  'True
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   1176
         Top             =   3912
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   1428
         Top             =   3864
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.CommandButton cmdEnter 
         Caption         =   "Enter"
         Height          =   288
         Left            =   300
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   3876
         Visible         =   0   'False
         Width           =   1056
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "firmName"
         DataField       =   "firmName"
         DataMember      =   "Instance"
         Height          =   195
         Index           =   0
         Left            =   210
         TabIndex        =   23
         Top             =   135
         UseMnemonic     =   0   'False
         Width           =   660
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "claimant"
         DataField       =   "claimant"
         DataMember      =   "Instance"
         Height          =   192
         Index           =   1
         Left            =   204
         TabIndex        =   22
         Top             =   420
         UseMnemonic     =   0   'False
         Width           =   600
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   7905
      _ExtentX        =   13944
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   7905
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   7785
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   30
         Width           =   7785
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   20
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpenClaim 
            Caption         =   "&Claim"
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator834 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator382 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator990 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
      End
      Begin VB.Menu mnuSeperator989 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator951 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmInstancePaymentEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0430 (jjm)
'previous ver 1.90.0124 (jjm)

Option Explicit


Const EVENTTYPENAMELISTINDEX_PARAM = "cboEventTypeIndex"

Private mInstancePaymentHandle As String
Private mInstanceHandle As String
Private mEventHandle As String
Private mEventTypeHandle As String
Private mEventStatus As String
Private GLTransaction As Dataset

Private mNew As Boolean
Private mDatafields As Collection
Private mDataSet As Dataset
Private mListIndex As Integer
Private mListView As ListView
Private mListItemType As String
Private mDirty As Boolean

Private mEventTypes As Collection
Private mEventTypeNameListIndex As Integer

' Parameters for creating new claim payment.
Private mParameterNames() As String
Private mParameterValues() As String

Private FormMgr As New FormManager

Private Const TXT_PAYMENTAMOUNT_INDEX = 30
Private Const TXT_WILMICPAYMENTAMOUNT_INDEX = 19 'on the advanced tab
Private Const TXT_DEDUCTIBLEDIRECT_INDEX = 15
Private Const TXT_BALANCE_INDEX = 21


Private Const CURRENCY_FORMAT = "#,###.00;(#,###.00)"

Private Sub InstanceRefresh()
  ClearDataField lblDataField, "Instance"
  
  If mInstanceHandle = vbNullString Then
    Exit Sub
  End If
  
  SetFields lblDataField, Factory.InstanceService.Find(mInstanceHandle), "Instance"
  Caption = IIf(mNew, "New", "Edit") & " Payment (" & mInstanceHandle & ")"

End Sub


'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub


Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form selected
    Case fimSelect
      mInstanceHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INSTANCE_KEY)
      InstanceRefresh
      Dirty = True
  End Select
  Show
  dtpDataField(0).SetFocus
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormPrepare(PaymentAmount As Double, DirectAmount As Double)
  If txtDataField(TXT_PAYMENTAMOUNT_INDEX).Locked Then
    Exit Sub
  End If
  
  Dim xmlHttp As xmlHttp
  Dim xmlUrl As String
  xmlUrl = HttpServer & WebUtil & "claimpaymentprepare?key=" & mInstanceHandle & _
                                                        "&key=" & EncodeURL(mEventTypeHandle) & _
                                                        "&key=" & Format(PaymentAmount, "############.00") & _
                                                        "&key=" & Format(DirectAmount, "############.00") & "&source=text/xml"
  Set xmlHttp = XMLUtil.HttpGet(xmlUrl)
      
  'MsgBox xmlHttp.responseText
  Dim Model As Collection
  Set Model = XMLUtil.RowsetOLD(xmlHttp.responseXML.documentElement, "claimpayment")
  If Model Is Nothing Then
    ErrorUtil.DisplayXMLHttpError xmlHttp, xmlUrl
    'MsgBox "Error: Service was unabled to be called. " & xmlUrl, vbCritical
    Exit Sub
  End If

  SetFields Me.txtDataField, Model(1), "Amount"
End Sub

Private Sub FormRefresh()
  mEventStatus = vbNullString
    
  'clear everything
  ClearDataField txtDataField
  ClearDataField dtpDataField
  ClearDataField lblDataField
    
  If (mNew) Then
    mEventHandle = vbNullString
    cboEventTypeName.ListIndex = mEventTypeNameListIndex
    dtpDataField(0).Value = Today

    Caption = "New Unallocated Payment (" & mInstanceHandle & ")"
    txtNote.Text = vbNullString
  Else
    Dim DataField As Collection
    Set DataField = Factory.InstancePaymentService.Find(mInstancePaymentHandle)
        
    If DataField Is Nothing Then
      MsgBox "No Data Found. Form: InstancePaymentEdit", vbCritical
      Unload Me
      Exit Sub
    End If
        
        
    mInstanceHandle = DataField(DatabaseName.INSTANCE_KEY)
    
    'get the handle
    Dim disbursementHandle As String
    disbursementHandle = DataField(DatabaseName.DISBURSEMENT_KEY)
    ' get the disbursement data
    Dim DisbursementData As Collection
    Set DisbursementData = GenericFind(DatabaseName.DISBURSEMENT_TABLE, DatabaseName.DISBURSEMENT_KEY, disbursementHandle)
    ' load the data into the form
    SetFields txtDataField, DisbursementData, "Disbursement"
        
    mEventHandle = DataField(DatabaseName.EVENT_KEY)
    
    Dim EventData As Collection
    Set EventData = Factory.EventService.Find(mEventHandle)
    SetFields dtpDataField, EventData, "Event" 'sets the payment date, aka eventDate
    SetFields txtDataField, EventData, "Event" 'sets the amount, aka eventAmount
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    mEventStatus = EventData(DatabaseName.EVENTSTATUS_FIELD)
    
    
    'set the dropdown list
    mEventTypeHandle = GetDataFromCollection(EventData, DatabaseName.EVENTTYPE_KEY)
    cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, mEventTypeHandle) - 1
    
    Caption = "Unallocated Payment (" & mInstanceHandle & ")"
  End If
  
  InstanceRefresh
  
  'Filters
  
  ' ver 1.90.1226
  ' corrected filter to DatabaseName.EVENT_KEY
  GLTransaction.DataProvider.filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle)
  ListViewLoad lvGL, GLTransaction.DataProvider, True
  
  Dim item As ListItem
  Dim transactionAmount As Double
  For Each item In lvGL.ListItems
    transactionAmount = Val(item.ListSubItems(DatabaseName.TRANSACTIONAMOUNT_FIELD))
    If transactionAmount < 0 Then
      item.SubItems(5) = Format(0 - transactionAmount, CURRENCY_FORMAT)
    Else
      item.SubItems(4) = Format(transactionAmount, CURRENCY_FORMAT)
    End If
  Next item
    
  ' Set locking of txtDataField(TXT_PAYMENTAMOUNT_INDEX).
  txtDataField(TXT_PAYMENTAMOUNT_INDEX).Locked = mEventStatus = "Complete"
  
  
  '
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  '
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' After refresh, record is not changed if existing, changed if new
  Dirty = False
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.INSTANCEPAYMENT_TABLE
      mInstancePaymentHandle = ListItemText(mListView.SelectedItem, DatabaseName.INSTANCEPAYMENT_KEY)
    Case DatabaseName.EVENT_TABLE
      mInstancePaymentHandle = FindHandle(DatabaseName.INSTANCEPAYMENT_TABLE, _
                                       DatabaseName.EVENT_KEY, _
                                       ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY), _
                                       DatabaseName.INSTANCEPAYMENT_KEY)
  End Select
End Property

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  
  ' Start Custom Code
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
'  If Val(txtDataField(TXT_BALANCE_INDEX)) <> 0 Or Val(txtBalance.Text) <> 0 Then
'    MsgBox "Balance must be 0 in order to save!  See advanced tab.", vbExclamation + vbOKOnly, "Balance Error"
'    Save = False
'    Exit Function
'  End If
  
  Dim mDataControl As New Collection
  AddTextControl txtDataField, mDataControl
  AddValueControl dtpDataField, mDataControl
  
    
  If mNew Then
    mInstancePaymentHandle = Factory.InstancePaymentService.CreateInstancePayment( _
        mInstanceHandle, _
        mEventTypeHandle, _
        GetDate(GetDataFromCollection(mDataControl, DatabaseName.EVENTDATE_FIELD)), _
        GetDataFromCollection(mDataControl, DatabaseName.REFERENCENUMBER_FIELD), _
        GetDataFromCollection(mDataControl, DatabaseName.CHECKNUMBER_FIELD), _
        GetDataFromCollection(mDataControl, DatabaseName.PAYEE_FIELD), _
        GetDouble(txtDataField(TXT_PAYMENTAMOUNT_INDEX)), _
        txtNote.Text)
    Save = mInstancePaymentHandle <> vbNullString
    If Save Then
      mNew = False
    End If
  Else
    Save = Factory.InstancePaymentService.WriteInstancePayment( _
        mInstancePaymentHandle, _
        mEventTypeHandle, _
        GetDate(GetDataFromCollection(mDataControl, DatabaseName.EVENTDATE_FIELD)), _
        GetDataFromCollection(mDataControl, DatabaseName.REFERENCENUMBER_FIELD), _
        GetDataFromCollection(mDataControl, DatabaseName.CHECKNUMBER_FIELD), _
        GetDataFromCollection(mDataControl, DatabaseName.PAYEE_FIELD), _
        GetDouble(txtDataField(TXT_PAYMENTAMOUNT_INDEX)), _
        txtNote.Text)
  End If

  If Save Then
    FormRefresh
  Else
    Dirty = True
  End If
  
  'End custom code
End Function
          
Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.INSTANCEPAYMENT_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no law areas to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(instancePaymentId As String)
  mInstancePaymentHandle = instancePaymentId
  Show
End Sub


Public Sub ShowNew(Optional vInstanceHandle As String)
  mInstanceHandle = vInstanceHandle
  mNew = True
  Show
  
  If mInstanceHandle = vbNullString Then
    cmdInstance_Click
  End If
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' END GENERAL METHODS
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Private Sub cboEventTypeName_Click()
  If cboEventTypeName.ListIndex < 0 Then
    mEventTypeHandle = vbNullString
  Else
    mEventTypeHandle = mEventTypes(cboEventTypeName.ListIndex + 1)(DatabaseName.EVENTTYPE_KEY)
  End If
End Sub

'
' End General
'

Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdInstance_Click()
  Dispatcher.ShowSearch DatabaseName.INSTANCE_TABLE, FormMgr, True
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSave_Click()
  If Save Then
    Unload Me
  End If
End Sub

Private Sub cmdSaveNew_Click()
  If Save Then mnuFileNew_Click
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.INSTANCEPAYMENT_TABLE)
  Set GLTransaction = NewGenericDataset(DatabaseName.GLTRANSACTION_VIEW)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If


End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  Set mEventTypes = Factory.EventTypeService.SourceCollection(DatabaseName.INSTANCEPAYMENT_TABLE)
  FormUtil.ComboBoxLoad mEventTypes, cboEventTypeName, DatabaseName.EVENTTYPENAME_FIELD
  
  FormUtil.WriteIniFile Name, EVENTTYPENAMELISTINDEX_PARAM, 0
  
  mEventTypeNameListIndex = FormUtil.ReadIniFile(Name, EVENTTYPENAMELISTINDEX_PARAM)
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  'Default tab open to payment
  SSTab1.Tab = 0
  
  FormRefresh
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub


Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileOpenClaim_Click()
  FormUtil.ShowByPrimaryKey New frmClaimOpen, mInstanceHandle, FormMgr
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowEventProperties mEventHandle
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mInstancePaymentHandle = vbNullString
  FormRefresh
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpAbout_Click()
  'MsgBox ActiveControl.Name
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.INSTANCEPAYMENT_TABLE, lawActionNew, _
      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub


Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
  Select Case Index
    Case 2
      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, txtDataField(Index).Text) - 1
  End Select
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
