VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmWorkflowDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Workflow Options"
   ClientHeight    =   5085
   ClientLeft      =   30
   ClientTop       =   315
   ClientWidth     =   8160
   Icon            =   "frmCompleteEvent.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5085
   ScaleWidth      =   8160
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      Enabled         =   0   'False
      Height          =   336
      Left            =   6900
      TabIndex        =   10
      Top             =   4620
      Width           =   1200
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   5640
      TabIndex        =   9
      Top             =   4620
      Width           =   1200
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   4380
      TabIndex        =   8
      Top             =   4620
      Width           =   1200
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4404
      Left            =   60
      TabIndex        =   11
      Top             =   144
      Width           =   8040
      _ExtentX        =   14182
      _ExtentY        =   7779
      _Version        =   393216
      Style           =   1
      TabHeight       =   420
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmCompleteEvent.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Folder"
      TabPicture(1)   =   "frmCompleteEvent.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFolder"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Parameter"
      TabPicture(2)   =   "frmCompleteEvent.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame2"
      Tab(2).ControlCount=   1
      Begin VB.Frame fraFolder 
         Caption         =   "Folder Location"
         Height          =   3948
         Left            =   -74916
         TabIndex        =   5
         Top             =   360
         Width           =   7872
         Begin VB.CommandButton cmdModify 
            Caption         =   "&Modify..."
            Enabled         =   0   'False
            Height          =   336
            Left            =   6792
            TabIndex        =   6
            Top             =   456
            Width           =   996
         End
         Begin MSComctlLib.ListView lvFolder 
            Height          =   3360
            Left            =   108
            TabIndex        =   7
            Top             =   444
            Width           =   6612
            _ExtentX        =   11668
            _ExtentY        =   5927
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Name"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Location"
               Object.Width           =   5292
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Parameter"
         Height          =   3948
         Left            =   -74916
         TabIndex        =   2
         Top             =   360
         Width           =   7872
         Begin VB.CommandButton cmdParameterModify 
            Caption         =   "&Modify..."
            Enabled         =   0   'False
            Height          =   336
            Left            =   6792
            TabIndex        =   3
            Top             =   456
            Width           =   996
         End
         Begin MSComctlLib.ListView lvParameter 
            Height          =   3360
            Left            =   108
            TabIndex        =   4
            Top             =   444
            Width           =   6612
            _ExtentX        =   11668
            _ExtentY        =   5927
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Name"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Value"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Description"
         Height          =   3084
         Left            =   60
         TabIndex        =   0
         Top             =   360
         Width           =   7860
         Begin VB.TextBox txtDescription 
            Height          =   2700
            Left            =   96
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   1
            Top             =   252
            Width           =   7644
         End
      End
   End
End
Attribute VB_Name = "frmWorkflowDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mFolder As String
Private mActionType As Integer
Private mActionWindows As String
Private mActionDatabase As String

Private Path As String

Private mOk As Boolean
Private mOKCancel As Boolean
Private mParameterNames() As String
Private mParameterValues() As String
Private mDirty As Boolean
Private mWriteDescription As Boolean


Public Property Get ParameterNames() As String()
  ParameterNames = mParameterNames
End Property

Public Property Get ParameterValues() As String()
  ParameterValues = mParameterValues
End Property

Public Sub ParameterRefresh()
'  Dim Size, I, j As Integer
'  Size = lvFolder.ListItems.Count + lvParameter.ListItems.Count
'  If (Size > 0) Then
'    ReDim mParameterNames(Size - 1)
'    For I = 1 To lvFolder.ListItems.Count
'      mParameterNames(I - 1) = lvFolder.ListItems(I).Key
'    Next I
'    For j = I To Size
'      mParameterNames(j - 1) = lvParameter.ListItems(j - I + 1).Key
'    Next j
'
'    ReDim mParameterValues(Size - 1)
'    For I = 1 To lvFolder.ListItems.Count
'      mParameterValues(I - 1) = lvFolder.ListItems(I).ListSubItems(1)
'    Next I
'    For j = I To Size
'      mParameterValues(j - 1) = lvParameter.ListItems(j - I + 1).ListSubItems(1)
'    Next j
'  End If
End Sub

Private Function ParameterSaveView(View As ListView) As Boolean
'  Dim item As ListItem
'  For Each item In View.ListItems
'    Factory.FolderParameterService.WriteFolderParameter mFolder, mActionDatabase, item.Key, item.ListSubItems(1).Text, _
'        vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
'  Next item
End Function


Public Function ParameterSave() As Boolean
'  ParameterSaveView lvFolder
'  ParameterSaveView lvParameter
'  If mWriteDescription Then
'    Factory.FolderParameterService.WriteFolderParameter mFolder, mActionDatabase, "Description", txtDescription.Text, _
'        vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
'  Else
'    Factory.FolderParameterService.CreateFolderParameter mFolder, mActionDatabase, "Description", txtDescription.Text, _
'        vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
'  End If
End Function

'
' Returns True if the form was refreshed or needs to be refreshed.
'
Private Function FormRefresh() As Boolean
'  Dim mDataSet As Dataset
'  Set mDataSet = New GenericDataset
'  Set mDataSet.Session = LawSession
'  If mActionType = lawActionNew Then
'    mDataSet.DataProvider.Source = "SysEventActionParamView"
'    mDataSet.DataProvider.Filter = """eventGroupId"" = '" & mFolder & "' and ""action"" = 'Create'"
'  Else
'    mDataSet.DataProvider.Source = "SysFolderParameterView"
'    mDataSet.DataProvider.Filter = """folderId"" = '" & mFolder & "' and ""action"" = '" & mActionDatabase & "'"
'  End If
'  mDataSet.DataProvider.OpenRecordset
'
'  Dim Collection As Collection
'  Set Collection = mDataSet.DataProvider.Collection
'  If Collection Is Nothing Then
'    Exit Function
'  End If
'
'  ' Scan through results.
'  Dim item As ListItem
'  Dim Row As Variant
'  For Each Row In Collection
'    If Row("parameterType") = "Folder" Then
'      If Not Contains(lvFolder.ListItems, CStr(Row(DatabaseName.PARAMETER_KEY))) Then
'        FormRefresh = True
'        Select Case MsgBox("A folder location named '" & Row("parameterName") & "' is required by the event named '" & Row("eventTypeName") & "'.  Add folder name?", vbYesNoCancel, "Add Folder (" & Row(DatabaseName.PARAMETER_KEY) & ")")
'          Case vbCancel
'            Exit For
'          Case vbYes
'            Set item = lvFolder.ListItems.Add(, Row(DatabaseName.PARAMETER_KEY), Row("parameterName"))
'            item.ListSubItems.Add
'            Factory.FolderParameterService.CreateFolderParameter mFolder, mActionDatabase, Row(DatabaseName.PARAMETER_KEY).Value, vbNullString, _
'                vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
'        End Select
'      End If
'    ElseIf Not Contains(lvParameter.ListItems, CStr(Row(DatabaseName.PARAMETER_KEY))) Then
'      FormRefresh = True
'      Select Case MsgBox("A parameter named '" & Row("parameterName") & "' is required by the event named '" & Row("eventTypeName") & "'.  Add parameter name?", vbYesNoCancel, "Add Parameter (" & Row(DatabaseName.PARAMETER_KEY) & ")")
'        Case vbCancel
'          Exit For
'        Case vbYes
'          Set item = lvParameter.ListItems.Add(, Row(DatabaseName.PARAMETER_KEY), Row("parameterName"))
'          item.ListSubItems.Add
'          Factory.FolderParameterService.CreateFolderParameter mFolder, mActionDatabase, Row(DatabaseName.PARAMETER_KEY).Value, vbNullString, _
'              vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray
'      End Select
'    End If
'  Next Row
'
'  cmdModify.Enabled = lvFolder.ListItems.Count > 0
'  cmdParameterModify.Enabled = lvParameter.ListItems.Count > 0
End Function

Public Function ShowByFolder(Folder As String, ActionType As Integer, Optional OkCancel As Boolean) As Boolean
  mFolder = Folder
  mActionType = ActionType
  mActionWindows = ActionText(ActionType)
  mActionDatabase = ActionText(ActionType, True)
  mOKCancel = OkCancel
  
  Me.Show vbModal
  ShowByFolder = mOk
End Function

Public Function Validate() As Boolean
  If mDirty Then
    Select Case MsgBox("Permanently save changes made to the settings?", vbYesNoCancel)
      Case vbYes
        ParameterSave
      Case vbCancel
        Exit Function
    End Select
  End If
  Validate = True
End Function
'
' End General Methods
'
'
'
' Begin Control Methods
'


Private Sub cmdCancel_Click()
  If mOKCancel Then
    If Validate Then Me.Hide
  Else
    cmdOk_Click
  End If
End Sub

Private Sub cmdModify_Click()
'  Dim fFolder As New frmFolderBrowseDialog, Path As String
'  Path = lvFolder.SelectedItem.ListSubItems(1).Text
'  If fFolder.ShowSelect(, Path) Then
'    lvFolder.SelectedItem.ListSubItems(1).Text = fFolder.FolderPath
'    ListViewModule.AutosizeWidth lvFolder
'    mDirty = True
'  End If
'  Unload fFolder
'  lvFolder.SetFocus
End Sub

Private Sub cmdOk_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass

  If Not Validate Then Exit Sub
  ParameterRefresh
  mOk = True
  Me.Hide
End Sub

Private Sub cmdParameterModify_Click()
'  Dim Param As String
'  Param = InputBox("Enter new parameter value.", "Change Parameter Value", lvParameter.SelectedItem.SubItems(1))
'  If Param <> vbNullString Then
'    lvParameter.SelectedItem.SubItems(1) = Param
'    ListViewModule.AutosizeWidth lvParameter
'    mDirty = True
'  End If
End Sub

Private Sub cmdRefresh_Click()
'  Dim pointer As New WaitPointer
'  pointer.Default
'  FormRefresh
End Sub

Private Sub Form_Load()
  If mFolder = "" Then
    MsgBox "Must use ShowFolder when loading this form"
    Unload Me
    Exit Sub
  End If
  'Action Types
  ' 0 = Complete
  ' 1 = Void
  Dim entryId As String
  entryId = IIf(mActionType = 0, "note", "voidnote")
  txtDescription.Text = Factory.PreferenceService.getPreferenceEntryValue(mFolder, entryId)

  ' Load Parameters.
'  Dim FolderParam As Collection, Row As Collection, item As ListItem
'  Set FolderParam = Factory.FolderParameterService.FolderParameters(mFolder, mActionType)
'  If Not FolderParam Is Nothing Then
'    For Each Row In FolderParam
'      Select Case Row("parameterType")
'        Case "Description"
'          txtDescription.Text = Row("parameterValue")
'          mWriteDescription = True
'        Case "Folder"
'          With lvFolder.ListItems.Add(, Row(DatabaseName.PARAMETER_KEY), Row("parameterName"))
'            .ListSubItems.Add , , Row("parameterValue")
'          End With
'        Case Else
'          With lvParameter.ListItems.Add(, Row(DatabaseName.PARAMETER_KEY), Row("parameterName"))
'            .ListSubItems.Add , , Row("parameterValue")
'          End With
'      End Select
'    Next Row
'  End If
'
'  ' Size Views
'  If lvFolder.ListItems.Count > 0 Then
'    ListViewModule.AutosizeWidth lvFolder
'  End If
'  If lvParameter.ListItems.Count > 0 Then
'    ListViewModule.AutosizeWidth lvParameter
'  End If
  
  Caption = Caption & " - " & mActionWindows & " (" & mFolder & ")"
'  cmdModify.Enabled = lvFolder.ListItems.Count > 0
'  cmdParameterModify.Enabled = lvParameter.ListItems.Count > 0
    
  ' Update for changing Options only.
  If Not mOKCancel Then
    cmdCancel.Caption = "Close"
    cmdCancel.Default = True
    cmdOK.Visible = False
  End If
  
  ' Fix for SSTab and tabbing.
  FormUtil.TabControlEnable SSTab1
  
  mDirty = False
End Sub

Private Sub lvFolder_DblClick()
  If cmdModify.Enabled Then
'    cmdModify_Click
  End If
End Sub

Private Sub lvParameter_DblClick()
  If cmdParameterModify.Enabled Then
'    cmdParameterModify_Click
  End If
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  cmdModify.Enabled = (lvFolder.ListItems.Count <> 0)
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub txtDescription_Change()
  mDirty = True
End Sub
