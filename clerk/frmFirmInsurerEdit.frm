VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFirmInsurerEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Insurer"
   ClientHeight    =   4425
   ClientLeft      =   3480
   ClientTop       =   3060
   ClientWidth     =   5010
   Icon            =   "frmFirmInsurerEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4425
   ScaleWidth      =   5010
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3972
      Left            =   0
      ScaleHeight     =   3975
      ScaleWidth      =   5010
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   384
      Width           =   5004
      Begin VB.CommandButton cmdInsurer 
         Caption         =   "&Insurer"
         Height          =   288
         Left            =   3924
         TabIndex        =   22
         Top             =   120
         Width           =   1000
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1320
         TabIndex        =   6
         Top             =   3564
         Width           =   1176
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   2544
         TabIndex        =   7
         Top             =   3564
         Width           =   1176
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   3768
         TabIndex        =   8
         Top             =   3564
         Width           =   1176
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   2880
         Left            =   84
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   600
         Width           =   4860
         _ExtentX        =   8573
         _ExtentY        =   5080
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Firm Insurer"
         TabPicture(0)   =   "frmFirmInsurerEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label3"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label5"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label6"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label7"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label8"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblDataField(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtpDataField(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtpDataField(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtDataField(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtDataField(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtDataField(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtDataField(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         Begin VB.TextBox txtDataField 
            DataField       =   "perClaimLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "FirmInsurer"
            Height          =   288
            Index           =   0
            Left            =   1512
            TabIndex        =   2
            Top             =   1416
            Width           =   3216
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "aggregateLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "FirmInsurer"
            Height          =   288
            Index           =   1
            Left            =   1512
            TabIndex        =   3
            Top             =   1776
            Width           =   3216
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "perClaimDeductible"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "FirmInsurer"
            Height          =   288
            Index           =   2
            Left            =   1512
            TabIndex        =   4
            Top             =   2100
            Width           =   3216
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "premium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "FirmInsurer"
            Height          =   288
            Index           =   3
            Left            =   1512
            TabIndex        =   5
            Top             =   2436
            Width           =   3216
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmFirmInsurerEdit.frx":0028
            DataField       =   "effectiveDate"
            DataMember      =   "FirmInsurer"
            Height          =   288
            Index           =   0
            Left            =   1512
            TabIndex        =   0
            Top             =   744
            Width           =   1356
            _ExtentX        =   2381
            _ExtentY        =   503
            _Version        =   393216
            Format          =   87883777
            CurrentDate     =   2
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmFirmInsurerEdit.frx":003E
            DataField       =   "expirationDate"
            DataMember      =   "FirmInsurer"
            Height          =   288
            Index           =   1
            Left            =   1512
            TabIndex        =   1
            Top             =   1092
            Width           =   1356
            _ExtentX        =   2381
            _ExtentY        =   503
            _Version        =   393216
            Format          =   87883777
            CurrentDate     =   2
         End
         Begin MSComctlLib.ImageList imlUpDown 
            Left            =   3708
            Top             =   -216
            _ExtentX        =   794
            _ExtentY        =   794
            BackColor       =   -2147483643
            _Version        =   393216
         End
         Begin MSComctlLib.ImageList imlUpDownDisabled 
            Left            =   4272
            Top             =   -216
            _ExtentX        =   794
            _ExtentY        =   794
            BackColor       =   -2147483643
            MaskColor       =   12632256
            _Version        =   393216
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "entityName"
            DataField       =   "entityName"
            DataMember      =   "EntityFirm"
            Height          =   195
            Index           =   1
            Left            =   90
            TabIndex        =   23
            Top             =   435
            UseMnemonic     =   0   'False
            Width           =   795
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Premium:"
            Height          =   192
            Left            =   84
            TabIndex        =   19
            Top             =   2484
            Width           =   672
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Deductible:"
            Height          =   192
            Left            =   84
            TabIndex        =   18
            Top             =   2148
            Width           =   816
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Aggregate Limit:"
            Height          =   192
            Left            =   96
            TabIndex        =   17
            Top             =   1812
            Width           =   1164
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Per Claim Limit:"
            Height          =   192
            Left            =   84
            TabIndex        =   16
            Top             =   1476
            Width           =   1092
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Expiration Date:"
            Height          =   192
            Left            =   84
            TabIndex        =   15
            Top             =   1140
            Width           =   1128
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Left            =   84
            TabIndex        =   14
            Top             =   804
            Width           =   1032
         End
      End
      Begin VB.Label lblDataField 
         Caption         =   "insurerName"
         DataField       =   "entityName"
         DataMember      =   "EntityInsurer"
         Height          =   192
         Index           =   0
         Left            =   168
         TabIndex        =   21
         Top             =   168
         UseMnemonic     =   0   'False
         Width           =   3648
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   5004
      _ExtentX        =   8837
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   5010
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   4890
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   30
         Width           =   4890
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   24
            TabIndex        =   11
            Top             =   24
            Width           =   732
            _ExtentX        =   1270
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl +<)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   312
      Left            =   3624
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   1116
      Width           =   876
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator503 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmFirmInsurerEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0410 (jjm)
Option Explicit

Private mFirmHandle As String
Private mInsurerHandle As String 'Used to hold the initial insurerId.
Private mInsurerId As String     'Used to hold the current insurerId
Private mEffectiveHandle As Date

Private mInsurerData As DataProvider

Private mNew As Boolean

Private mListIndex As Integer
Private mListView As ListView

Private mDirty As Boolean

Private mExpirationSet As Boolean

Private FormMgr As New FormManager

'
' Standard Dirty for cmdSave and cmdSaveNew type Edits.  Use standard
' Dirty from frmApplicationEdit for forms without cmdSaveNew.
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Private Sub FirmRefresh()
  If mFirmHandle = vbNullString Then Exit Sub
  
  SetFields lbldatafield, Factory.EntityService.Find(mFirmHandle, DatabaseName.ENTITYNAME_FIELD), "EntityFirm"
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
      Exit Sub
    Case fimNew
      Exit Sub
    Case fimSelect
      Select Case fInterface.Key
        Case INSURER_TABLE
          If ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INSURER_KEY) = "ORGANIZATION" Then
            MsgBox "You cannot insert this company as an insurer. Please make a different selection."
            'ver 2.10.0408 (jjm)
            'mInsurerHandle = vbNullString
            mInsurerId = vbNullString
            Exit Sub
          End If
          
          'mInsurerHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INSURER_KEY)
          mInsurerId = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INSURER_KEY)
          lbldatafield(0) = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INSURERENTITYNAME_FIELD)
          Dirty = True
      End Select
  End Select
  
  
  If ActiveControl Is cmdInsurer Then
    If dtpDataField(0).Enabled Then
      dtpDataField(0).SetFocus
    Else
      txtDataField(0).SetFocus
    End If
  End If
End Sub


Private Sub FormRefresh()
   
  If (mNew) Then
    Me.Caption = "New Firm Insurer"
    ClearDataField txtDataField
    ClearDataField dtpDataField
    ClearDataField lbldatafield
    Dim NextEffectiveDate As Collection
    Set NextEffectiveDate = Factory.FirmService.Find(mFirmHandle, DatabaseName.NEXTEFFECTIVEDATE_FIELD)
    'ver 2.10.0409 (jjm)
    If Not NextEffectiveDate Is Nothing Then
      dtpDataField(0).Value = IIf(NextEffectiveDate(1) = vbNullString, Today, NextEffectiveDate(1))
    Else
      dtpDataField(0).Value = Today
    End If
    
    dtpDataField(1).Value = dtpDataField(0).Value + 365
  Else
  
    Dim Datafields As Collection
    Set Datafields = Factory.FirmInsurerService.Find(mFirmHandle, mInsurerHandle, mEffectiveHandle)
    
    Me.Caption = "Edit Insurer (" & mInsurerHandle & ")"

    SetFields txtDataField, Datafields, "FirmInsurer"
    SetFields dtpDataField, Datafields, "FirmInsurer"
    'Trick for when a the Insurer is ORGANIZATION
    Dirty = True
  End If
  
  FirmRefresh
  InsurerRefresh
  
  'If the insurerId = ORGANIZATION then lock the controls
  Dim IsLocked As Boolean
  IsLocked = (mInsurerHandle = "ORGANIZATION")
  txtDataField(0).Locked = IsLocked
  txtDataField(1).Locked = IsLocked
  txtDataField(2).Locked = IsLocked
  txtDataField(3).Locked = IsLocked
  cmdInsurer.Enabled = Not IsLocked
  dtpDataField(0).Enabled = Not IsLocked
  dtpDataField(1).Enabled = Not IsLocked
  
    
  ' After refresh, record is not dirty. However this needs to be
  Dirty = False
  
  '
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  '
  FormUtil.NavigationRefresh Me, mListView, mListIndex
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub InsurerRefresh()
  If mInsurerHandle = vbNullString Then
    ClearDataField lbldatafield, DatabaseName.INSURER_TABLE
    Exit Sub
  End If
  
  'Get the Insurer Name
  SetFields lbldatafield, Factory.EntityService.Find(mInsurerHandle, DatabaseName.ENTITYNAME_FIELD), "EntityInsurer"
End Sub

Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mFirmHandle = ListItemText(mListView.SelectedItem, DatabaseName.FIRM_KEY)
  mInsurerHandle = ListItemText(mListView.SelectedItem, DatabaseName.INSURER_KEY)
  mEffectiveHandle = ListItemText(mListView.SelectedItem, DatabaseName.EFFECTIVEDATE_FIELD)
  mNew = False
  
  'If form is already loaded, check to make sure that a virtural FirmInsurer record is not being scrolled to
  If Me.Visible Then
    If Not Factory.FirmInsurerService.validateInsurerRecord(mInsurerHandle) Then
      Unload Me
    End If
  End If
  
End Property

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  Dim mDataControls As New Collection
  AddTextControl txtDataField, mDataControls
  'AddTextControl cboDatafield, mDataControls
  'AddTextControl mskDatafield, mDataControls
  AddValueControl dtpDataField, mDataControls
  
  If mNew Then
    Dim Handle As Variant
    Handle = Factory.FirmInsurerService.CreateFirmInsurer( _
       mFirmHandle, mInsurerId, _
       GetDate(GetDataFromCollection(mDataControls, DatabaseName.EFFECTIVEDATE_FIELD)), _
       GetDate(GetDataFromCollection(mDataControls, DatabaseName.EXPIRATIONDATE_FIELD)), _
       GetDouble(GetDataFromCollection(mDataControls, DatabaseName.PERCLAIMLIMIT_FIELD)), _
       GetDouble(GetDataFromCollection(mDataControls, DatabaseName.AGGREGATELIMIT_FIELD)), _
       GetDouble(GetDataFromCollection(mDataControls, DatabaseName.PERCLAIMDEDUCTIBLE_FIELD)), _
       GetDouble(GetDataFromCollection(mDataControls, DatabaseName.PREMIUM_FIELD)), _
       vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
    save = Not VarType(Handle) = vbEmpty
    If save Then
      mNew = False
      mFirmHandle = Handle(0)
      mInsurerHandle = Handle(1)
    End If
  Else
    save = Factory.FirmInsurerService.WriteFirmInsurer( _
      mFirmHandle, mInsurerHandle, mEffectiveHandle, mInsurerId, _
      GetDate(GetDataFromCollection(mDataControls, DatabaseName.EFFECTIVEDATE_FIELD)), _
      GetDate(GetDataFromCollection(mDataControls, DatabaseName.EXPIRATIONDATE_FIELD)), _
      GetDouble(GetDataFromCollection(mDataControls, DatabaseName.PERCLAIMLIMIT_FIELD)), _
      GetDouble(GetDataFromCollection(mDataControls, DatabaseName.AGGREGATELIMIT_FIELD)), _
      GetDouble(GetDataFromCollection(mDataControls, DatabaseName.PERCLAIMDEDUCTIBLE_FIELD)), _
      GetDouble(GetDataFromCollection(mDataControls, DatabaseName.PREMIUM_FIELD)), _
      vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
  End If

  If save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  Dirty = Not save

End Function

Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no insurers to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  
  If Factory.FirmInsurerService.validateInsurerRecord(mInsurerHandle) Then
    Show
  Else
    Me.Visible = False
    Unload Me
  End If

End Sub

Public Sub ShowNew(FirmId As String, Optional insurerId As String)
  mFirmHandle = FirmId
  mInsurerHandle = insurerId
  
  mNew = True
  Show
  
  If mInsurerHandle = vbNullString Then
    cmdInsurer_Click
  End If
End Sub

Public Sub ShowByPrimaryKey(FirmId As String, insurerId As String, EffectiveDate As Date)
  mFirmHandle = FirmId
  mInsurerHandle = insurerId
  mEffectiveHandle = EffectiveDate
  
  If Factory.FirmInsurerService.validateInsurerRecord(mInsurerHandle) Then
    Show
  Else
    Me.Visible = False
    Unload Me
  End If
End Sub

'
'
'
'
'
'
'



Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdFirm_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True
End Sub

Private Sub cmdSave_Click()
  If save Then Unload Me
End Sub

Private Sub cmdSaveNew_Click()
  If Not save Then Exit Sub
  mnuFileNew_Click
End Sub

'
' Select a lawyer.  Can be called Publicly
'
Public Sub cmdInsurer_Click()
  Dispatcher.ShowSearch DatabaseName.INSURER_TABLE, FormMgr, True
End Sub


Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
  If Index = 0 Then
    dtpDataField(1).Value = dtpDataField(0).Value
    If Not IsNull(dtpDataField(0).Value) Then _
      dtpDataField(1).Year = dtpDataField(0).Year + 1
  End If
End Sub


Private Sub Form_Initialize()
  Set mInsurerData = LawSession.NewDataProvider(DatabaseName.INSURER_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        mnuViewNext_Click
      Case 188
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub


Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Insurer") Is Nothing Then
    MsgBox "Can not unload form until lawyer selection is complete."
    FormMgr("Insurer").Child.Show
    Cancel = 1
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  mNew = True
  mInsurerHandle = vbNullString
  mnuViewRefresh_Click
  cmdInsurer_Click
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.FIRMINSURER_TABLE, _
      DatabaseName.FIRM_KEY, DatabaseName.INSURER_KEY, mFirmHandle, mInsurerHandle
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuView_Click()
  If (mListView Is Nothing) Then
    mnuViewPrevious.Enabled = False
    mnuViewNext.Enabled = False
    Exit Sub
  End If
  mnuViewPrevious.Enabled = mListView.Enabled And mListIndex > 1
  mnuViewNext.Enabled = mListIndex < mListView.ListItems.Count
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub

Private Sub picUpDown_Resize()
  tbrUpDown.Top = (picUpDown.Height - tbrUpDown.Height) / 2
End Sub



Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub
