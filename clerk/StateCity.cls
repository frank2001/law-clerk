VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StateCity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'ver 2.70.00 (jjm) 2004 09 27

'StateCity class used to cache state / city information
'Used with LocationService

Private mCityList As Collection
Private mStateName As String
Private mStateId As String

Public Property Let stateName(v_StateName As String)
  mStateName = v_StateName
End Property

Public Property Get stateName() As String
  stateName = mStateName
End Property

Public Property Let stateId(v_StateId As String)
  mStateId = v_StateId
End Property

Public Property Get stateId() As String
  stateId = mStateId
End Property

Public Sub StateCity(v_StateId, v_StateName)
  mStateId = v_StateId
  mStateName = v_StateName
End Sub

Public Property Get CityList() As Collection
  If mCityList Is Nothing Then
    Set mCityList = Factory.LocationService.GetCitiesByStateCollection(mStateId)
  End If
  Set CityList = mCityList
End Property

Public Function GetCountyByPlaceName(mPlaceName) As String
  Dim i As Integer
  For i = 1 To mCityList.Count
    If mPlaceName = mCityList(i)(DatabaseName.PLACENAME_FIELD) Then
      GetCountyByPlaceName = mCityList(i)(DatabaseName.MAINCOUNTYNAME_FIELD)
      Exit For
    End If
  Next i
End Function
