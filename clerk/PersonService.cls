VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PersonService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "PersonService"
Private mDataSet As Dataset
Private mAttribute As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.PERSON_TABLE)
  Set mAttribute = NewGenericDataset(DatabaseName.PERSONATTRIBUTE_TABLE)
  
  'Setup SOAP
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.PERSON_KEY, Handle, Column)
End Function


'Public Function GetAttribute(Handle As String) As Collection
'  MsgBox "ToDo: Update old attributes."
'  mAttribute.DataProvider.Column = """attributeId"", ""attributeValue"""
'  mAttribute.DataProvider.Filter = """" + DatabaseName.PERSON_KEY + """ = '" + Handle + "'"
'  mAttribute.DataProvider.Sort = vbNullString
'  mAttribute.DataProvider.OpenRecordset
'  Dim Rows As Collection, Row As Collection, Attributes As New Collection
'  Set Rows = mAttribute.DataProvider.Collection
'  If Rows Is Nothing Then
'    Exit Function
'  End If
'  For Each Row In Rows
'    Attributes.Add Row("attributeValue"), Row("attributeId")
'  Next Row
'  Set GetAttribute = Attributes
'End Function
'

'
' Interface to server
'

'  parameterOrder='personId displayName salutation title firstName
'                  middleName lastName suffix nickname jobTitle
'                  socialSecurityNum attributeNames attributeValues parameterNames parameterValues'>

'
' Create an Person
'
Public Function CreatePerson( _
    DisplayName As String, Salutation As String, _
    Title As String, FirstName As String, MiddleName As String, _
    LastName As String, Suffix As String, _
    PersonEmail As String, HomeEmail As String, _
    WorkTelephone As String, MobileTelephone As String, HomeTelephone As String, _
    contactTypeId As String, _
    eventNote As String, organizationHandle As String, _
    LocationHandle) As Variant
    
On Error GoTo ErrorHandler
  
  CreatePerson = Client.Create( _
      DisplayName, Salutation, _
      Title, FirstName, MiddleName, _
      LastName, Suffix, PersonEmail, HomeEmail, _
      WorkTelephone, MobileTelephone, HomeTelephone, _
      contactTypeId, _
      eventNote, organizationHandle, _
      LocationHandle)
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  'MsgBox "Person Service Fault: " & Client.FaultString, vbExclamation
  
End Function

'
' Write an application.
'
Public Function WritePerson( _
    PersonHandle As String, DisplayName As String, Salutation As String, _
    Title As String, FirstName As String, MiddleName As String, _
    LastName As String, Suffix As String, PersonEmail As String, HomeEmail, _
    WorkTelephone As String, MobileTelephone As String, HomeTelephone As String, _
    contactTypeId As String, _
    eventNote As String, organizationHandle As String, _
    LocationHandle) As Boolean
    
On Error GoTo ErrorHandler
  
  Call Client.Write( _
      PersonHandle, _
      DisplayName, Salutation, _
      Title, FirstName, MiddleName, _
      LastName, Suffix, PersonEmail, HomeEmail, _
      WorkTelephone, MobileTelephone, HomeTelephone, _
      contactTypeId, _
      eventNote, organizationHandle, LocationHandle)
      
  WritePerson = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  'MsgBox "Person Service Fault: " & Client.FaultString, vbExclamation
  WritePerson = False
End Function

'
' Delete an Person
'
Public Function DeletePerson( _
    PersonHandle As String, _
    ParameterNames() As String, ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler
  Call Client.Delete(PersonHandle, ParameterNames, ParameterValues)
  DeletePerson = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  'MsgBox "Fault: " & Client.FaultString, vbExclamation
  DeletePerson = False
End Function



'
' LOCATOR Functions
'

'Creates a default locator
'Public Function CreateLocator(LocatorAttempt As String, Optional PersonHandle As String) As String
'  Dim Result As String, I As Integer
'  Result = LocatorAttempt
'  While Not LocatorAvailable(Result, PersonHandle)
'    I = I + 1
'    Result = Left(Result, 7) & I
'  Wend
'  CreateLocator = Result
'End Function
'
''Determins if the locator already exists as a lawyerId or a lawyerLocator
'Private Function LocatorAvailable(locator As String, Optional PersonHandle As String) As Boolean
'  mDataset.DataProvider.Filter = DatabaseName.PERSON_KEY & " != '" & PersonHandle & "' and " & _
'                                 "(" & DatabaseName.PERSON_KEY & " = '" & locator & "' or " & _
'                                 DatabaseName.PERSONLOCATOR_FIELD & " = '" & locator & "')"
'  mDataset.DataProvider.OpenRecordset
'  LocatorAvailable = (mDataset.DataProvider.Collection Is Nothing)
'End Function

'
''ver 2.75 (jjm)
''Check to see verify that the personLocator being passed is not being
''used as an exisiting personId or for as a personLocator for another person
''This function will return true when its OK to continue
'Public Function verifyPersonLocator(locator As String, Optional PersonHandle As String) As Boolean
'  If locator = vbNullString Then
'    MsgBox "A locator is required to save.", vbCritical
'    Exit Function
'  End If
'  'This querry should return all conflicting lawyerId's and locators
'  'First make sure not to include context lawyer
'  'Then check for and lawyerId's that match the proposed locator
'  'Finally check for any match locators that check for the matching locators
'  'Basically the following SQL:
'  '
'  '  select
'  '    *
'  '  From
'  '    "Lawyer"
'  '  Where
'  '    "lawyerId" != 'lawyerHandle' and
'  '    ("lawyerId" = 'locator' or  "lawyerLocator" = 'locator');
'  mDataset.DataProvider.Filter = DatabaseName.PERSON_KEY & " != '" & PersonHandle & "' and " & _
'                                 "(" & DatabaseName.PERSON_KEY & " = '" & locator & "' or " & _
'                                 DatabaseName.PERSONLOCATOR_FIELD & " = '" & locator & "')"
'  mDataset.DataProvider.Column = vbNullString
'  'store the following results into a collection
'  Dim results As Collection
'  mDataset.DataProvider.OpenRecordset
'  Set results = mDataset.DataProvider.Collection
'  If results Is Nothing Then
'    'all is good, return true
'    verifyPersonLocator = True
'  Else
'    'A duplicate was found. Display information to user
'    Dim lawyerList As String, I As Integer
'    For I = 1 To results.Count
'      lawyerList = lawyerList & _
'                 GetDataFromCollection(results(I), DatabaseName.PERSON_KEY) & vbTab & _
'                 GetDataFromCollection(results(I), DatabaseName.PERSONLOCATOR_FIELD) & vbTab & _
'                 GetDataFromCollection(results(I), DatabaseName.ENTITYNAME_FIELD) & vbCrLf
'    Next I
'    MsgBox "The following person is are already using this locator as a personLocator or a personId:" & vbCrLf & _
'           "ID" & vbTab & vbTab & "Locator " & vbTab & vbTab & "Name" & vbCrLf & lawyerList, vbCritical
'  End If
'End Function




