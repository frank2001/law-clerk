VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmOrganizationOpen 
   Caption         =   "Organization"
   ClientHeight    =   5670
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   8400
   Icon            =   "frmOrganizationOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5670
   ScaleWidth      =   8400
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4680
      Left            =   0
      ScaleHeight     =   4680
      ScaleWidth      =   8400
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   384
      Width           =   8400
      Begin VB.Frame fraNote 
         Caption         =   "Note"
         Height          =   1530
         Left            =   5616
         TabIndex        =   17
         Top             =   -12
         Width           =   2712
         Begin VB.TextBox txtNote 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   1275
            Left            =   72
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   216
            Width           =   2568
         End
      End
      Begin VB.Frame fraFirm 
         Caption         =   "Organization"
         Height          =   1530
         Left            =   24
         TabIndex        =   7
         Top             =   -12
         Width           =   5532
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Website:"
            Height          =   195
            Left            =   105
            TabIndex        =   30
            Top             =   1200
            Width           =   630
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "www.BakkeNorman.com"
            DataField       =   "entityURL"
            DataMember      =   "Entity"
            Height          =   195
            Index           =   1
            Left            =   1185
            TabIndex        =   29
            Top             =   1200
            UseMnemonic     =   0   'False
            Width           =   1770
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "fax"
            DataField       =   "fax"
            DataMember      =   "Location"
            Height          =   195
            Index           =   6
            Left            =   1185
            TabIndex        =   16
            Top             =   945
            UseMnemonic     =   0   'False
            Width           =   210
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Fax:"
            Height          =   195
            Left            =   105
            TabIndex        =   15
            Top             =   945
            Width           =   300
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Telephone:"
            Height          =   192
            Left            =   108
            TabIndex        =   14
            Top             =   672
            Width           =   828
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "telephone"
            DataField       =   "telephone"
            DataMember      =   "Location"
            Height          =   192
            Index           =   4
            Left            =   1188
            TabIndex        =   13
            Top             =   684
            UseMnemonic     =   0   'False
            Width           =   720
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "WI"
            DataField       =   "state"
            DataMember      =   "Location"
            Height          =   192
            Index           =   3
            Left            =   2736
            TabIndex        =   12
            Top             =   432
            UseMnemonic     =   0   'False
            Width           =   192
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "Madison"
            DataField       =   "city"
            DataMember      =   "Location"
            Height          =   192
            Index           =   2
            Left            =   1188
            TabIndex        =   11
            Top             =   432
            UseMnemonic     =   0   'False
            Width           =   624
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "City & State:"
            Height          =   192
            Index           =   0
            Left            =   108
            TabIndex        =   10
            Top             =   432
            UseMnemonic     =   0   'False
            Width           =   852
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "Bakke Norman S.C."
            DataField       =   "entityName"
            DataMember      =   "Entity"
            Height          =   192
            Index           =   0
            Left            =   1188
            TabIndex        =   9
            Top             =   192
            UseMnemonic     =   0   'False
            Width           =   1404
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Name:"
            Height          =   192
            Left            =   108
            TabIndex        =   8
            Top             =   192
            Width           =   480
         End
      End
      Begin MSComDlg.CommonDialog dlgCommonDialog 
         Left            =   7332
         Top             =   228
         _ExtentX        =   688
         _ExtentY        =   688
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlToolbarIcons 
         Left            =   7824
         Top             =   156
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   9
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":000C
               Key             =   "New"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":011E
               Key             =   "Open"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":0230
               Key             =   "Print"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":0342
               Key             =   "Copy"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":0454
               Key             =   "Delete"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":0566
               Key             =   "View Large Icons"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":0678
               Key             =   "View Small Icons"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":078A
               Key             =   "View List"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOrganizationOpen.frx":089C
               Key             =   "View Details"
            EndProperty
         EndProperty
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3030
         Left            =   30
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   1620
         Width           =   8295
         _ExtentX        =   14631
         _ExtentY        =   5345
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   420
         TabCaption(0)   =   "Contact"
         TabPicture(0)   =   "frmOrganizationOpen.frx":09AE
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Location"
         TabPicture(1)   =   "frmOrganizationOpen.frx":09CA
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Person"
         TabPicture(2)   =   "frmOrganizationOpen.frx":09E6
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Document"
         TabPicture(3)   =   "frmOrganizationOpen.frx":0A02
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         Begin MSComctlLib.ListView lvView 
            Height          =   2625
            Index           =   2
            Left            =   -74955
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "OrganizationPerson"
            Top             =   315
            Width           =   8175
            _ExtentX        =   14420
            _ExtentY        =   4630
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2625
            Index           =   1
            Left            =   -74955
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "OrganizationLocation"
            Top             =   315
            Width           =   8175
            _ExtentX        =   14420
            _ExtentY        =   4630
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2625
            Index           =   0
            Left            =   45
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "OrganizationContact"
            Top             =   315
            Width           =   8160
            _ExtentX        =   14393
            _ExtentY        =   4630
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2625
            Index           =   3
            Left            =   -74955
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "OrganizationDocument"
            Top             =   315
            Width           =   8160
            _ExtentX        =   14393
            _ExtentY        =   4630
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   4824
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   3372
         Width           =   1416
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   6300
         Top             =   396
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   16777215
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   6924
         Top             =   492
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin MSComDlg.CommonDialog dlgSave 
         Left            =   5325
         Top             =   1200
         _ExtentX        =   688
         _ExtentY        =   688
         _Version        =   393216
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   5
      Top             =   5400
      Width           =   8400
      _ExtentX        =   14817
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7170
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "7/21/2011"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "4:17 PM"
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   8400
      _ExtentX        =   14817
      _ExtentY        =   688
      _CBWidth        =   8400
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picMenu"
      MinWidth1       =   795
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   990
      MinHeight2      =   330
      Width2          =   990
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   330
      Width3          =   945
      NewRow3         =   0   'False
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   3705
         ScaleHeight     =   330
         ScaleWidth      =   3630
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   30
         Width           =   3630
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   660
            Left            =   0
            TabIndex        =   27
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1508
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1830
            TabIndex        =   23
            Top             =   15
            Width           =   1170
            _ExtentX        =   2064
            _ExtentY        =   1164
            ButtonWidth     =   1640
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Organization"
                        Text            =   "Organization"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Website"
                        Text            =   "Website"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   660
            Left            =   930
            TabIndex        =   24
            Top             =   15
            Width           =   840
            _ExtentX        =   1482
            _ExtentY        =   1164
            ButtonWidth     =   1191
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Organization"
                        Text            =   "Organization"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   660
            Left            =   0
            TabIndex        =   25
            Top             =   15
            Width           =   870
            _ExtentX        =   1535
            _ExtentY        =   1164
            ButtonWidth     =   1270
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   4
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Contact"
                        Text            =   "Contact"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Location"
                        Text            =   "Location"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Person"
                        Text            =   "Person"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Document"
                        Text            =   "Document"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   7560
         ScaleHeight     =   330
         ScaleWidth      =   750
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   30
         Width           =   750
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   21
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNewOrganization 
            Caption         =   "&Organization"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator998 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Contact"
            Index           =   0
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "L&ocation"
            Index           =   1
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Document"
            Index           =   2
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Organization"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator282 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileEvent 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileEvent 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator283 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator326 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditOrganization 
         Caption         =   "&Organization"
      End
      Begin VB.Menu mnuSeparator27261 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator1234 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuSeperator542 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Complete Row"
         Index           =   0
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Void Row"
         Index           =   1
      End
      Begin VB.Menu mnuSeperator631 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditExport 
         Caption         =   "Expor&t Row"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator79879 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "&Tab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Lawyer"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Insurer"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "L&ocation"
            Index           =   2
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Claim"
            Index           =   3
         End
      End
      Begin VB.Menu mnuSeperator98789 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns"
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuSeparator513 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeparator101 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsBrowseFirm 
         Caption         =   "&Browse Firm"
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuToolsBrowseWebsite 
         Caption         =   "Browse &Website"
      End
      Begin VB.Menu mnuSeparator8343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsMErge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "Web Merge"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator456 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator987 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSeperator457 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuPopupWebMerge 
         Caption         =   "Web Merge"
      End
      Begin VB.Menu mnuSeperator458 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Complete"
         Index           =   0
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Void"
         Index           =   1
      End
      Begin VB.Menu mnuSeperator459 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupExport 
         Caption         =   "Expor&t"
      End
   End
End
Attribute VB_Name = "frmOrganizationOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0417
'previous ver 2.00.0304 (jjm)
'
'
' OrganizationOpen is the reference implementation for "Open" type
' forms.
'
'

Option Explicit

' Handle and dataset to master Organizaiton information
Private mLocationHandle As String
Private mOrganizationHandle As String
Private mEventHandle As String

'Flag if it is a Firm
Private isFirm As Boolean

' Show variables
Private mListIndex As Integer
Private mListView As ListView
Private mTab As Integer

' Manage forms loaded by this form.
Private FormMgr As New FormManager

' Detail datasets
Private mPerson As Dataset
Private mContact As Dataset
Private mLocation As Dataset
Private mLetter As Dataset

'Constants
Private Const CONTACT_TAB = 0
Private Const LOCATION_TAB = 1
Private Const PERSON_TAB = 2
Private Const DOCUMENT_TAB = 3


'
' General Methods
'

'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimSelect
      
    Case fimSave
'      If fInterface.Key = DatabaseName.EVENT_KEY & "New" Then
'        'Setup for auto-complete
'        Dim Handles(0) As String
'        Dim ParameterNames() As String
'        Dim ParameterValues() As String
'        Handles(0) = fInterface.Child.EventHandle
'        'Automatically Complete the documents
'        Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
'      End If
  End Select
  
  Refresh
  FormRefresh
  Show
End Sub

'
' Called when this form is loaded from a Form Manager
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

'
' Custom refresh the form to the underlying data.
'
Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Dim Datafields As Collection
  Set Datafields = Factory.EntityService.Find(mOrganizationHandle)
  
  SetFields lbldatafield, Datafields, DatabaseName.ENTITY_TABLE
  
  Caption = "Organization - " & GetDataFromCollection(Datafields, DatabaseName.ENTITYLOCATOR_FIELD) & " (" & mOrganizationHandle & ")"
  
  mContact.DataProvider.filter = DatabaseName.ORGANIZATION_KEY & " = '" & mOrganizationHandle & "'"
  mPerson.DataProvider.filter = DatabaseName.ORGANIZATION_KEY & " = '" & mOrganizationHandle & "'"
  mLocation.DataProvider.filter = DatabaseName.ENTITY_KEY & " = '" & mOrganizationHandle & "'"
  mLetter.DataProvider.filter = "referenceEventId =  '" & mEventHandle & "' and " & _
                                "(" & _
                                    LawDialect.Contains(DatabaseName.EVENTTYPERULEID_FIELD, "Document") & " or " & _
                                    LawDialect.EqualString(DatabaseName.EVENTTYPERULEID_FIELD, "Endorsement") & _
                                ") and " & _
                                " createDate > " & LawDialect.DateFormat(GetDate("01/01/2000"))

  mLocationHandle = GetDataFromCollection(Datafields, DatabaseName.MAINLOCATIONID_FIELD)
  SetFields lbldatafield, Factory.LocationService.Find(mLocationHandle), DatabaseName.LOCATION_TABLE
  
  mEventHandle = GetDataFromCollection(Datafields, DatabaseName.EVENT_KEY)
  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
  If (mEventHandle = vbNullString) Then
    txtNote.Text = vbNullString
  Else
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  End If
  
  'Test for firm)
  isFirm = (GetDataFromCollection(Factory.FirmService.Find(mOrganizationHandle), DatabaseName.FIRM_KEY) <> vbNullString)
  mnuFileOpen(0).Enabled = isFirm
  
  
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex

  SSTabRefresh
  
  Refresh
End Sub

' Custom
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  mOrganizationHandle = ListItemText(mListView.SelectedItem, DatabaseName.ORGANIZATION_KEY)

End Property

' Custom
Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
  Case CONTACT_TAB
    Set ListViewDataProvider = mContact.DataProvider
  Case LOCATION_TAB
    Set ListViewDataProvider = mLocation.DataProvider
  Case PERSON_TAB
    Set ListViewDataProvider = mPerson.DataProvider
  Case DOCUMENT_TAB
    Set ListViewDataProvider = mLetter.DataProvider
  End Select
 
End Function

'ver 2.10.0415 (jjm)
Private Sub MenuRefresh()
  'Disable and enable Edit menus
  
  Dim IsDocumentTab, IsPersonTab As Boolean
  IsDocumentTab = (SSTab.Tab = DOCUMENT_TAB)
  IsPersonTab = (SSTab.Tab = PERSON_TAB)
  
  'void and complete
  mnuEditEventRow(0).Enabled = IsDocumentTab
  mnuEditEventRow(1).Enabled = IsDocumentTab
  mnuPopupEvent(0).Enabled = IsDocumentTab
  mnuPopupEvent(1).Enabled = IsDocumentTab
  
  'merge and export
  mnuToolsMErge.Enabled = IsDocumentTab
  mnuToolsWebMerge.Enabled = IsDocumentTab
  mnuEditExport.Enabled = IsDocumentTab
  mnuPopupMerge.Enabled = IsDocumentTab
  mnuPopupWebMerge.Enabled = IsDocumentTab
  mnuPopupExport.Enabled = IsDocumentTab
  
  'Delete
  mnuEditDeleteRow.Enabled = Not (IsDocumentTab Or IsPersonTab)
  mnuPopupDelete.Enabled = Not (IsDocumentTab Or IsPersonTab)
  
  
End Sub

Public Sub ShowByListView(View As ListView)
'  If View.ListItems.Count = 0 Then
'    MsgBox "There are no organizations to view or edit."
'    Exit Sub
'  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

'
' Show by primary key.
'
Public Sub ShowByPrimaryKey(organizationId As String, Optional Index As Integer)
  mOrganizationHandle = organizationId
  mTab = Index
  Show
End Sub

'
' Refresh contents of tab.
'
Public Sub SSTabRefresh()
  Dim Source As DataProvider, View As ListView, Index As Variant
  Set View = lvView(mTab)
  Set Source = ListViewDataProvider(mTab)
  
  With View
    'Ensure first tab is open.
    If SSTab.Tab = mTab Then
      If .ListItems.Count > 0 Then
        Index = IIf(.SelectedItem.Key = "", .SelectedItem.Index, .SelectedItem.Key)
        .Visible = False
        .SelectedItem.Selected = False
        ListViewLoad View, Source
        On Error Resume Next
        .ListItems(Index).Selected = True
        .Visible = True
        .SetFocus
      Else
        ListViewLoad View, Source
      End If
    Else
      SSTab.Tab = mTab
    End If
    
    MenuRefresh
     
    sbStatusBar.Panels(1).Text = str(.ListItems.Count) + " object(s)"
    sbStatusBar.Panels(2).Text = Source.Source
  End With

End Sub

Private Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - lvView(Index).Left, 0)
  lvView(Index).Width = Max(SSTab.Width - (lvView(Index).Left * 2), 0)
End Sub
'
'
' End General
'
'
'
' Begin Controls
'



Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Activate()
  Form_Resize
End Sub

Private Sub Form_Initialize()
  
  'Initialize Contact dataset
  Set mContact = NewGenericDataset(DatabaseName.CONTACT_VIEW)
   
  'Initialize Location dataset
  Set mLocation = NewGenericDataset(DatabaseName.ENTITYLOCATION_VIEW)
  
  'Initialize Person dataset
  Set mPerson = NewGenericDataset(DatabaseName.PERSON_VIEW)
   
  'Initialize Reference dataset
  Set mLetter = NewGenericDataset(DatabaseName.EVENTREFERENCE_VIEW)
   
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyC   'Contact
        mnuFileNew_Click 0
      Case vbKeyL   'Location
        mnuFileNew_Click 1
      Case vbKeyI   'Insurer
        mnuFileNew_Click 3
      Case vbKeyD   'Document
        mnuFileNew_Click 5
      Case vbKeyH   'History
        mnuFileNew_Click 6
    End Select
    Exit Sub
  End If
  
  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      mnuFileNew_Click SSTab.Tab
  End Select

End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  ' Custom settings.
  mnuEditEditRow.Caption = mnuEditEditRow.Caption + vbTab + "Enter"
  
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.Height + 24

 ' Custom Menu Shortcut Adjustments
  mnuFileNew(0).Caption = mnuFileNew(0).Caption + Chr(9) + "Ctrl+Shift+C"
  mnuFileNew(1).Caption = mnuFileNew(1).Caption + Chr(9) + "Ctrl+Shift+L"
  mnuFileNew(2).Caption = mnuFileNew(2).Caption + Chr(9) + "Ctrl+Shift+D"
  
  'Standard setup for 'Open' forms.
  FormMgr.Load Me
  FormUtil.MenuLoad Me
  FormUtil.EditLoadImages Me
  
  ' Refresh mDataset
  FormRefresh
End Sub


' Expand body
Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
  
  If ScaleWidth < (fraNote.Left + 48) Then Exit Sub
  
  fraNote.Width = ScaleWidth - fraNote.Left - 48
  txtNote.Width = IIf(fraNote.Width - txtNote.Left - 48 < 0, _
                        0, fraNote.Width - txtNote.Left - 48)
  SSTabResize
End Sub

' Standard
Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub fraOrganization_DblClick()
  mnuEditOrganization_Click
End Sub

Private Sub fraNote_DblClick()
  If mnuToolsNotepad.Enabled Then
    mnuToolsNotepad_Click
  Else
    MsgBox "Please add initial note using the Edit, Organization menu."
  End If
End Sub

' Standard
Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

' Standard
Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
 '   Case vbKeyDelete
 '     mnuEditDeleteRow_Click
 '     KeyCode = 0
  End Select
End Sub

'ver 2.00.0304 (jjm) WILMIC
Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If
  
  Select Case SSTab.Tab
    Case CONTACT_TAB:
      With lvView(CONTACT_TAB)
        Call Factory.ContactService.DeleteContact( _
            ListItemText(.SelectedItem, DatabaseName.CONTACT_KEY))
      End With
    Case PERSON_TAB:
      If lvView(0).ListItems.Count = 0 Then
        MsgBox "There are no lawyers available."
        Exit Sub
      End If
    'Mantis 2523 - enable location delete
    Case LOCATION_TAB:
      Call Factory.LocationService.DeleteLocation(mOrganizationHandle, _
            ListItemText(lvView(LOCATION_TAB).SelectedItem, DatabaseName.LOCATION_KEY))
    

  End Select
  
  mnuViewRefresh_Click

End Sub

'ver 2.00.0304 (jjm)

Private Sub mnuEditBrowseRow_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Select Case SSTab.Tab
    Case CONTACT_TAB:    'Contact
    
    Case LOCATION_TAB:    'Location

    Case PERSON_TAB:
      Dim LawyerHandle As String
      LawyerHandle = ListItemText(lvView(PERSON_TAB).SelectedItem, DatabaseName.LAWYER_KEY)
      If LawyerHandle <> vbNullString Then
        Dispatcher.BrowseLawyerOpen LawyerHandle
      End If
    Case DOCUMENT_TAB:   'Document
    

  End Select

End Sub

Private Sub mnuEditEditRow_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Select Case SSTab.Tab
  
    Case CONTACT_TAB:    'Contact
      FormUtil.ShowByListView New frmContactEdit, FormMgr, lvView(CONTACT_TAB)
    
    Case LOCATION_TAB:    'Location
      FormUtil.ShowByListView New frmLocationEdit, FormMgr, lvView(LOCATION_TAB)
    
    Case PERSON_TAB:   'Person
      FormUtil.ShowByListView New frmPersonEdit, FormMgr, lvView(PERSON_TAB)
    
    Case DOCUMENT_TAB:   'Document
      Dim fLetter As New frmEventDocumentEdit
      FormMgr.Add fLetter
      fLetter.ShowByListView lvView(DOCUMENT_TAB)
  End Select
End Sub


Private Sub mnuEditEventRow_Click(Index As Integer)
  Dim Handles(0) As String
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  
  Handles(0) = ListItemText(lvView(5).SelectedItem, DatabaseName.EVENT_KEY)
'  Factory.FolderParameterService.ParameterLoad ListItemText(lvView(5).SelectedItem, DatabaseName.FOLDER_KEY), Index, ParameterNames, ParameterValues, True

  If Index = lawActionComplete Then
    If MsgBox("Are you sure you want to complete item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
    End If
  ElseIf Index = lawActionVoid Then
    If MsgBox("Are you sure you want to void item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetVoid Handles, ParameterNames, ParameterValues
    End If
  End If
  
  FormRefresh
End Sub

Private Sub mnuEditExport_Click()
  Dim ExportDlg As New frmExportDialog
  Dim Index As Integer
  Index = ExportDlg.ShowExport(Me)
  
  If Index > -1 Then
    FormUtil.ExportItem lvView(SSTab.Tab), Index, dlgSave
  End If
End Sub

Private Sub mnuEditOrganization_Click()
  FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mOrganizationHandle, FormMgr
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileEvent_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNew_Click(Index As Integer)
  Select Case Index
    Case 0  'Contact
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowNewByOrganization mOrganizationHandle, mLocationHandle
    Case 1  'Location
      Dispatcher.ShowNew DatabaseName.LOCATION_TABLE, mOrganizationHandle, FormMgr
    Case 2  'Person
      Dim fPerson As New frmPersonEdit
      FormMgr.Add fPerson, DatabaseName.PERSON_TABLE & "New"
      fPerson.ShowNew isFirm, mOrganizationHandle
    Case 3  'Document
      Dim fLetter As New frmEventDocumentEdit
      FormMgr.Add fLetter, DatabaseName.EVENT_KEY & "New"
      fLetter.ShowNew DatabaseName.FIRM_DOCUMENT, mEventHandle, lbldatafield(0).Caption, , mOrganizationHandle
  End Select
End Sub

Private Sub mnuFileNewOrganization_Click()
  Dispatcher.ShowNew DatabaseName.ORGANIZATION_TABLE, , FormMgr
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mOrganizationHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.ORGANIZATION_TABLE, mOrganizationHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.FIRM_REF, DatabaseName.ORGANIZATION_TABLE, mOrganizationHandle, lbldatafield(0).Caption
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp
End Sub

Private Sub mnuPopupBrowse_Click()
  mnuEditBrowseRow_Click
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupEvent_Click(Index As Integer)
  mnuEditEventRow_Click Index
End Sub

Private Sub mnuPopupExport_Click()
  mnuEditExport_Click
End Sub

Private Sub mnuPopupMerge_Click()
  mnuToolsMerge_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNew_Click mTab
End Sub

Private Sub mnuPopupWebMerge_Click()
  mnuToolsWebMerge_Click
End Sub

'
' Load advanced find window
'
Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.ORGANIZATION_TABLE, FormMgr
End Sub

'
' Browse for the firm.
'
Private Sub mnuToolsBrowseOrganization_Click()
  MsgBox "ToDo: Setup link after Organization Summary page is created"
End Sub


Private Sub mnuToolsBrowseFirm_Click()
  Dispatcher.BrowseFirmOpen mOrganizationHandle
End Sub

Private Sub mnuToolsBrowseWebsite_Click()
  Dispatcher.BrowseEntityURL mOrganizationHandle
End Sub


Private Sub mnuToolsMerge_Click()

  Dim pointer As New WaitPointer, Handles(0) As String
  pointer.Default

  Handles(0) = ListItemText(lvView(DOCUMENT_TAB).SelectedItem, DatabaseName.EVENT_KEY)
  
  ' Print Envelopes
  Factory.EventRecipientService.PrintEnvelope Handles(0)

  ' Perform Document Merge
  Factory.EventService.DocumentMerge Handles
  
  FormRefresh
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWebMerge_Click()
  Factory.EventService.WebMergeByView lvView(DOCUMENT_TAB)
End Sub

'
' Standard choose columns for the current tab's list view.
'
Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'
' Standard save settings
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

'
' Standard go to a tab via a menu click.
'
Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

'
' Standard toggle visibility of toolbar.
'
Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
End Sub

'
' Standard view next item in parent ListView
'
Private Sub mnuViewNext_Click()
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

'
' Standard view previous item in parent ListView
'
Private Sub mnuViewPrevious_Click()
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

'
' Standard Refresh Form.
'
Public Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

'
' Resize main body.  Called when Form_Resize adjusts picBody
'
Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

'
' User clicks on new tab.  Because method can be called from Form_Load,
' we must check ActiveControl before doing anything that needs the form
' to be loaded.
'
Private Sub SSTab_Click(PreviousTab As Integer)
  TabControlEnable SSTab
  mTab = SSTab.Tab
  SSTabRefresh
    
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileEvent_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuToolsBrowseOrganization_Click
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1 'Organization
      mnuToolsBrowseFirm_Click
    Case 2 'entityURL
      mnuToolsBrowseWebsite_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEditOrganization_Click
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
    Case "Organization"
      mnuEditOrganization_Click
  End Select
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileNew_Click SSTab.Tab
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNew_Click ButtonMenu.Index - 1
End Sub

'
'Public Function Export(ExportIndex As Integer, Optional FileName As String) As Boolean
'  Dim Pointer As New WaitPointer
'  Pointer.Default
'
'  Dim ReferenceTable As String, ServiceName As String, Selected() As String
'  Select Case ExportIndex
'    Case 0
'      ReferenceTable = DatabaseName.APPLICATION_TABLE
'      ServiceName = "applicationprint"
'      Selected = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
'    Case 1
'      ReferenceTable = DatabaseName.APPLICATION_TABLE
'      ServiceName = "applicationpartIIprint"
'      Selected = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
'    Case 2
'      ReferenceTable = DatabaseName.APPLICATION_TABLE
'      ServiceName = "applicationpartIIIprint"
'      Selected = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
'    Case 3
'      ServiceName = "LAWBatchExport.txt"
'      Selected = ListViewModule.ListItemArray(lvView, DatabaseName.GLBATCH_KEY)
'    Case 4
'      ServiceName = "eventprint"
'      Selected = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
'    Case 5
'      ServiceName = "financeprint"
'      Selected = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
'    Case 6
'      ServiceName = "policyprint"
'      If mFolderKey = DatabaseName.POLICY_TABLE Then
'        Selected = ListViewModule.ListItemArray(lvView, DatabaseName.POLICY_KEY)
'      Else
'        ReferenceTable = DatabaseName.POLICY_TABLE
'        Selected = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
'      End If
'  End Select
'


' Standard next/previous toolbar.
Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDataField_DblClick(Index As Integer)
  mnuToolsNotepad_Click
End Sub
