VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEventEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Properties"
   ClientHeight    =   6225
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6690
   Icon            =   "frmeventedit.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6225
   ScaleWidth      =   6690
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   5364
      TabIndex        =   14
      Top             =   5745
      Width           =   1200
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5550
      Left            =   90
      TabIndex        =   37
      Top             =   75
      Width           =   6465
      _ExtentX        =   11404
      _ExtentY        =   9790
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   4
      TabHeight       =   420
      TabCaption(0)   =   "&General"
      TabPicture(0)   =   "frmeventedit.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabel(2)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblLabel(5)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLocation"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblLabel(3)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblLabel(8)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblDataField(23)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblDataField(22)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblDataField(21)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblDataField(27)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblDataField(26)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblDataField(25)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblDataField(24)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblLabel(0)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblDataField(20)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblLabel(4)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblLabel(6)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblLabel(7)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Label1"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Label2"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "txtDataField(0)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "fraLine(0)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "fraLine(1)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "fraLine(2)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "cmdFolder"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "cboEventTypeName"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "dtpDatafield(0)"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "cboDataField(1)"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "txtDataField(2)"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "cmdSetDiary"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "UpDown1"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "txtWeek"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "chkComplete"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "cboDataField(0)"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).ControlCount=   33
      TabCaption(1)   =   "N&ote"
      TabPicture(1)   =   "frmeventedit.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtNote"
      Tab(1).ControlCount=   1
      Begin VB.ComboBox cboDataField 
         DataField       =   "eventTypeId"
         Height          =   315
         Index           =   0
         Left            =   1452
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Tag             =   "EventEditEventTypeId"
         Top             =   540
         Width           =   1272
      End
      Begin VB.CheckBox chkComplete 
         Caption         =   "Complete:"
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   4590
         Width           =   1275
      End
      Begin VB.TextBox txtWeek 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   288
         Left            =   1920
         TabIndex        =   8
         Top             =   3148
         Width           =   375
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   288
         Left            =   2280
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   3150
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         BuddyControl    =   "txtWeek"
         BuddyDispid     =   196612
         OrigLeft        =   2400
         OrigTop         =   3120
         OrigRight       =   2640
         OrigBottom      =   3495
         Max             =   52
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.CommandButton cmdSetDiary 
         Caption         =   "&Set"
         Height          =   288
         Left            =   360
         TabIndex        =   7
         Top             =   3148
         Width           =   800
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "eventDesc"
         Height          =   288
         Index           =   2
         Left            =   1455
         TabIndex        =   3
         Text            =   "Text1"
         Top             =   1236
         Width           =   3960
      End
      Begin VB.ComboBox cboDataField 
         DataField       =   "dueUserId"
         Height          =   315
         Index           =   1
         Left            =   3570
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Tag             =   "EventEditDueUserId"
         Top             =   2616
         Width           =   1860
      End
      Begin MSComCtl2.DTPicker dtpDatafield 
         DataField       =   "dueDate"
         Height          =   285
         Index           =   0
         Left            =   1050
         TabIndex        =   5
         Top             =   2610
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   503
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   71958529
         CurrentDate     =   37313
      End
      Begin VB.ComboBox cboEventTypeName 
         Height          =   315
         Left            =   2772
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   540
         Width           =   2652
      End
      Begin VB.CommandButton cmdFolder 
         Caption         =   "&Folder"
         Height          =   288
         Left            =   4470
         TabIndex        =   4
         Top             =   1932
         Width           =   1028
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   2
         Left            =   210
         TabIndex        =   33
         Top             =   3585
         Width           =   5220
      End
      Begin VB.TextBox txtNote 
         DataField       =   "eventNote"
         Height          =   5124
         Left            =   -74916
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         Top             =   312
         Width           =   6288
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   1
         Left            =   216
         TabIndex        =   20
         Top             =   2412
         Width           =   5220
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Index           =   0
         Left            =   180
         TabIndex        =   19
         Top             =   1692
         Width           =   5256
      End
      Begin VB.TextBox txtDataField 
         DataField       =   "eventName"
         Height          =   288
         Index           =   0
         Left            =   1455
         TabIndex        =   2
         Text            =   "Text1"
         Top             =   900
         Width           =   3960
      End
      Begin VB.Label Label2 
         Caption         =   "weeks from today."
         Height          =   192
         Left            =   2676
         TabIndex        =   36
         Top             =   3192
         Width           =   2532
      End
      Begin VB.Label Label1 
         Caption         =   "diary to"
         Height          =   192
         Left            =   1272
         TabIndex        =   35
         Top             =   3192
         Width           =   612
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Description:"
         Height          =   195
         Index           =   7
         Left            =   300
         TabIndex        =   34
         Top             =   1275
         Width           =   855
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "For:"
         Height          =   195
         Index           =   6
         Left            =   3120
         TabIndex        =   18
         Top             =   2655
         Width           =   270
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Diary:"
         Height          =   195
         Index           =   4
         Left            =   420
         TabIndex        =   17
         Top             =   2670
         Width           =   420
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "created"
         DataField       =   "createDate"
         Height          =   195
         Index           =   20
         Left            =   1545
         TabIndex        =   32
         Top             =   3780
         UseMnemonic     =   0   'False
         Width           =   555
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Created:"
         Height          =   195
         Index           =   0
         Left            =   405
         TabIndex        =   31
         Top             =   3780
         Width           =   615
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "created"
         DataField       =   "createUserId"
         Height          =   195
         Index           =   24
         Left            =   3600
         TabIndex        =   30
         Top             =   3780
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "modified"
         DataField       =   "modifyUserId"
         Height          =   195
         Index           =   25
         Left            =   3600
         TabIndex        =   29
         Top             =   4155
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   630
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "complete"
         DataField       =   "completeUserId"
         Height          =   195
         Index           =   26
         Left            =   3600
         TabIndex        =   28
         Top             =   4590
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "void"
         DataField       =   "voidUserId"
         Height          =   195
         Index           =   27
         Left            =   3600
         TabIndex        =   27
         Top             =   5010
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "modified"
         DataField       =   "modifyDate"
         Height          =   195
         Index           =   21
         Left            =   1545
         TabIndex        =   26
         Top             =   4155
         UseMnemonic     =   0   'False
         Width           =   630
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "complete"
         DataField       =   "completeDate"
         Height          =   195
         Index           =   22
         Left            =   1545
         TabIndex        =   25
         Top             =   4590
         UseMnemonic     =   0   'False
         Width           =   675
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "void"
         DataField       =   "voidDate"
         Height          =   195
         Index           =   23
         Left            =   1545
         TabIndex        =   24
         Top             =   5010
         UseMnemonic     =   0   'False
         Width           =   315
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Voided:"
         Height          =   195
         Index           =   8
         Left            =   405
         TabIndex        =   23
         Top             =   5010
         Width           =   570
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Modified:"
         Height          =   195
         Index           =   3
         Left            =   390
         TabIndex        =   22
         Top             =   4155
         Width           =   660
      End
      Begin VB.Label lblLocation 
         AutoSize        =   -1  'True
         Height          =   420
         Left            =   180
         TabIndex        =   21
         Top             =   1866
         UseMnemonic     =   0   'False
         Width           =   4092
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Name:"
         Height          =   195
         Index           =   5
         Left            =   300
         TabIndex        =   16
         Top             =   930
         Width           =   480
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Type of event:"
         Height          =   192
         Index           =   2
         Left            =   288
         TabIndex        =   15
         Top             =   564
         Width           =   1020
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   4056
      TabIndex        =   13
      Top             =   5745
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   2748
      TabIndex        =   12
      Top             =   5745
      Width           =   1200
   End
End
Attribute VB_Name = "frmEventEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.70.00 (jjm) 2004 09 23
'ver 2.40.01 (jjm) wilmic - removed the cmdClearNotepad button
'ver 2.00.0303 (jjm) wilmic

Option Explicit

Private mFolderHandle As String

Private mEventHandle As String
Private mEventTypeRuleId As String
Private mEventName As String

Private mEventReferenceHandle As String

Private mEventNote As String

Private FormMgr As New FormManager

Private mEventTypes As Collection
Private mDirty As Boolean
Private mNew As Boolean

'ver 1.91.0214
Private mCreateDate As Date

'ver 2.71.00 (jjm) 2005 02 15
Private mDefaultDueDate As Date

Private Const CBO_EVENTTYPEID_INDEX = 0
Private Const DTP_DUEDATE_INDEX = 0


Public Property Get EventHandle() As String
  EventHandle = mEventHandle
End Property

Public Property Get EventTypeHandle() As String
  EventTypeHandle = cboDataField(CBO_EVENTTYPEID_INDEX).Text
End Property
'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
End Sub


Private Function save(Optional Verify As Boolean) As Boolean
    ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  Dim mDataControls As New Collection
  AddTextControl txtDataField, mDataControls
  AddValueControl dtpDataField, mDataControls
  AddTextControl cboDataField, mDataControls
  AddCaptionControl lbldatafield, mDataControls
  
 
  If mNew Then
    mEventHandle = Factory.EventService.CreateEvent( _
      GetDataFromCollection(mDataControls, DatabaseName.EVENTTYPE_KEY), _
      GetDataFromCollection(mDataControls, DatabaseName.EVENTNAME_FIELD), _
      GetDataFromCollection(mDataControls, DatabaseName.EVENTDESC_FIELD), _
      txtNote.Text, _
      mEventTypeRuleId, _
      mFolderHandle, _
      IIf(IsNull(dtpDataField(DTP_DUEDATE_INDEX).Value), vbNullDate, GetDate(GetDataFromCollection(mDataControls, DatabaseName.DUEDATE_FIELD))), _
      IIf(IsNull(dtpDataField(DTP_DUEDATE_INDEX).Value), vbNullString, GetDataFromCollection(mDataControls, DatabaseName.DUEUSERID_FIELD)), _
      mEventReferenceHandle, _
      vbNullStringArray, _
      vbNullStringArray)
  
    save = mEventHandle <> vbNullString
  Else
       
    save = Factory.EventService.WriteEvent( _
      mEventHandle, _
      GetDataFromCollection(mDataControls, DatabaseName.EVENTTYPE_KEY), _
      GetDataFromCollection(mDataControls, DatabaseName.EVENTNAME_FIELD), _
      GetDataFromCollection(mDataControls, DatabaseName.EVENTDESC_FIELD), _
      txtNote.Text, _
      mEventTypeRuleId, _
      mFolderHandle, _
      IIf(IsNull(dtpDataField(DTP_DUEDATE_INDEX).Value), vbNullDate, GetDate(GetDataFromCollection(mDataControls, DatabaseName.DUEDATE_FIELD))), _
      IIf(IsNull(dtpDataField(DTP_DUEDATE_INDEX).Value), vbNullString, GetDataFromCollection(mDataControls, DatabaseName.DUEUSERID_FIELD)), _
      vbNullStringArray, _
      vbNullStringArray)
  End If
  
  If save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  Dirty = Not save
  
  ' Do complete
  If chkComplete.Visible And chkComplete Then
    Dim Handles(0) As String
    Handles(0) = mEventHandle
    Factory.EventService.SetComplete Handles, vbNullStringArray, vbNullStringArray
  End If
End Function


Public Sub ShowByPrimaryKey(EventHandle As String, Optional defaultDueDate As Date)
  mEventHandle = EventHandle
  If defaultDueDate <> vbNullDate Then
    mDefaultDueDate = defaultDueDate
  End If
  Show
End Sub

'@param EventReferenceHandle = eventId of referenced object
'@param EventName
Public Sub ShowNew(EventTypeRuleId As String, EventReferenceHandle As String, EventName As String)
  mNew = True
  mEventTypeRuleId = EventTypeRuleId
  mEventReferenceHandle = EventReferenceHandle
  mEventName = EventName
  Show
End Sub



Private Sub cboDataField_Click(Index As Integer)
  Select Case Index
    Case 0
      'sync the evenTypeId and eventTypeName drop downs
      cboEventTypeName.ListIndex = cboDataField(0).ListIndex
  End Select
  Dirty = True
End Sub

Private Sub cboEventTypeName_Click()
  'sync the evenTypeId and eventTypeName drop downs
  cboDataField(CBO_EVENTTYPEID_INDEX).ListIndex = cboEventTypeName.ListIndex  'mEventTypes(cboEventTypeName.ListIndex + 1)(databasename.EVENTTYPE_KEY)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub
'ver 2.40.01 - removed the cmdClearNotepad button
'Private Sub cmdClearNotepad_Click()
'  txtNote.Text = vbNullString
'  txtNote.SetFocus
'End Sub

Private Sub cmdFolder_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowSelect Then
    lblLocation.Caption = fForm.FolderPath
    mFolderHandle = fForm.FolderKey
  End If
  Unload fForm
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub cmdOk_Click()
  If save Then
    FormUtil.SetComboDefault cboDataField(0)
    FormUtil.SetComboDefault cboDataField(1)
    Unload Me
  End If
End Sub

Private Sub cmdSetDiary_Click()
  dtpDataField(0).Value = DateAdd("d", 7 * Int(txtWeek.Text), Today)
  'dtpDataField(0).Day = dtpDataField(0).Day + (7 * Int(txtWeek.Text))
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  cboDataField(1).Enabled = IIf(IsNull(dtpDataField(0).Value), False, True)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  FormMgr.Load Me
  
  ' Set format of labels beofre SetFields
  Dim fmt As New StdDataFormat, I As Integer, EventTypeId As String
  fmt.Format = LocaleLongDate & ", " & LocaleTimeFormat
  For I = 20 To 23
    Set lbldatafield(I).DataFormat = fmt
  Next I
  
  ' Get list of users from law.ini
  FormUtil.ComboBoxRegistryLoad cboDataField(1), "userId"
  
  ClearDataField lbldatafield
  ClearDataField txtDataField
  
  If mNew Then
    dtpDataField(0).Value = Today()
    dtpDataField(0).Value = Null
    txtDataField(0).Text = mEventName
    Caption = "New Event"
    
    FormUtil.GetComboDefault cboDataField(1)
  Else
    Dim DataField As Collection
    Set DataField = Factory.EventService.Find(mEventHandle)
    
     
    mEventTypeRuleId = GetDataFromCollection(DataField, DatabaseName.EVENTTYPERULEID_FIELD)
    mFolderHandle = GetDataFromCollection(DataField, DatabaseName.FOLDER_KEY)
    
    ' ver 2.00.0303 (jjm)
    Caption = GetDataFromCollection(DataField, DatabaseName.EVENTTYPENAME_FIELD) & " Event Properties"
    
    lblLocation.Caption = Factory.PreferenceService.GetPath(SystemFolders_TreeName, GetDataFromCollection(DataField, DatabaseName.FOLDER_KEY))
    If GetDataFromCollection(DataField, DatabaseName.COMPLETEDATE_FIELD) <> vbNullString Then
      chkComplete.Visible = False
    End If
    
  End If
  
  ' Need mEventTypeRuleId set above.
  Set mEventTypes = Factory.EventTypeService.GroupCollection(mEventTypeRuleId)
  If Not mEventTypes Is Nothing Then
    For I = 1 To mEventTypes.Count
      cboEventTypeName.AddItem GetDataFromCollection(mEventTypes.item(I), DatabaseName.EVENTTYPENAME_FIELD)
      cboDataField(0).AddItem GetDataFromCollection(mEventTypes(I), DatabaseName.EVENTTYPE_KEY)
    Next I
  End If
  
  If Not mNew Then
    SetFields txtDataField, DataField
    SetFields lbldatafield, DataField
    SetFields dtpDataField, DataField
    SetFields cboDataField, DataField
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    'Need to save now due the formating done to the labels 1.91.0214 (jjm)
    mCreateDate = GetDate(GetDataFromCollection(DataField, DatabaseName.CREATEDATE_FIELD))
    
    'if a default due date was passed setup form
    If mDefaultDueDate <> vbNullDate Then
      dtpDataField(0).Value = mDefaultDueDate
    End If
  Else
    FormUtil.GetComboDefault cboDataField(0)
  End If
  
  fmt.Format = LocaleLongDate & ", " & LocaleTimeFormat
  For I = 20 To 23
    lbldatafield(I).Caption = lbldatafield(I).Caption & _
      IIf(lbldatafield(I + 4).Caption <> vbNullString, " (" & lbldatafield(I + 4) & ")", "")
  Next I
  
    
  'Set the default diary date according to the source type pased
  Dim DefaultDiaryTime As String
  Select Case mEventTypeRuleId
    Case DatabaseName.INCIDENTEVENT_RULE
      DefaultDiaryTime = "6"
    Case Else
      DefaultDiaryTime = "3"
  End Select
      
  txtWeek.Text = DefaultDiaryTime
    
  dtpDataField_Change 0
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormMgr.Unload
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
  Select Case Index
    Case 3
      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, txtDataField(Index).Text) - 1
  End Select
End Sub

Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Private Sub txtDataField_GotFocus(Index As Integer)
  If Not (Index = 1) Then ' prevents the note from being selected
    FormUtil.EditTextSelect txtDataField(Index)
  End If
  
End Sub
Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdOK
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdOK
End Sub

Private Sub txtWeek_GotFocus()
  FormUtil.EditTextSelect txtWeek
End Sub
