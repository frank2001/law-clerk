VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmFirmEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   10545
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   13980
   Icon            =   "frmFirmEdit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   10545
   ScaleWidth      =   13980
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   9525
      Left            =   0
      ScaleHeight     =   9525
      ScaleWidth      =   13980
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   384
      Width           =   13980
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   9150
         TabIndex        =   25
         Top             =   6120
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   336
         Left            =   7890
         TabIndex        =   24
         Top             =   6120
         Width           =   1200
      End
      Begin TabDlg.SSTab sstDetail 
         Height          =   5730
         Left            =   60
         TabIndex        =   26
         Top             =   105
         Width           =   10260
         _ExtentX        =   18098
         _ExtentY        =   10107
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmFirmEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFirm"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "frmLocation"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmFirmEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).Control(1)=   "cmdClearNotepad"
         Tab(1).ControlCount=   2
         Begin VB.Frame frmLocation 
            Caption         =   "Location"
            Height          =   3345
            Left            =   105
            TabIndex        =   28
            Top             =   2205
            Width           =   9675
            Begin VB.CommandButton cmdLocation 
               Caption         =   "Select Lo&cation"
               CausesValidation=   0   'False
               Height          =   336
               Left            =   4335
               TabIndex        =   51
               Top             =   330
               Width           =   1500
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine2"
               Height          =   288
               Index           =   23
               Left            =   996
               TabIndex        =   38
               Top             =   1005
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine3"
               Height          =   288
               Index           =   22
               Left            =   996
               TabIndex        =   37
               Top             =   1350
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "zip"
               Height          =   288
               Index           =   19
               Left            =   4272
               TabIndex        =   36
               Top             =   1695
               Width           =   1056
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine1"
               Height          =   288
               Index           =   4
               Left            =   996
               TabIndex        =   35
               Top             =   660
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "county"
               Height          =   288
               Index           =   13
               Left            =   996
               TabIndex        =   34
               Text            =   " "
               Top             =   2040
               Width           =   1554
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "country"
               Height          =   288
               Index           =   12
               Left            =   3372
               TabIndex        =   33
               Text            =   " "
               Top             =   2055
               Visible         =   0   'False
               Width           =   1554
            End
            Begin VB.ComboBox cboLocationTypeName 
               Height          =   315
               ItemData        =   "frmFirmEdit.frx":0044
               Left            =   996
               List            =   "frmFirmEdit.frx":0046
               Style           =   2  'Dropdown List
               TabIndex        =   32
               Tag             =   "LocationEditLocationType"
               Top             =   315
               Width           =   3204
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "email"
               Height          =   288
               Index           =   3
               Left            =   996
               TabIndex        =   31
               Top             =   2760
               Width           =   3192
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "state"
               Height          =   315
               Index           =   2
               Left            =   996
               Style           =   2  'Dropdown List
               TabIndex        =   30
               TabStop         =   0   'False
               Top             =   1695
               Width           =   648
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "city"
               Height          =   315
               Index           =   0
               Left            =   2124
               TabIndex        =   29
               Text            =   "cboDatafield"
               Top             =   1695
               Width           =   1752
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmFirmEdit.frx":0048
               DataField       =   "fax"
               DataMember      =   "Location"
               Height          =   285
               Index           =   2
               Left            =   3900
               TabIndex        =   39
               Top             =   2415
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmFirmEdit.frx":005B
               DataField       =   "telephone"
               DataMember      =   "Location"
               Height          =   285
               Index           =   1
               Left            =   1425
               TabIndex        =   40
               Top             =   2415
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin MSComctlLib.ListView lvReference 
               Height          =   2790
               Left            =   6495
               TabIndex        =   52
               Top             =   435
               Width           =   3060
               _ExtentX        =   5398
               _ExtentY        =   4921
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   0
            End
            Begin VB.Label Label28 
               AutoSize        =   -1  'True
               Caption         =   "This ia a list of items that use this location."
               Height          =   195
               Left            =   6525
               TabIndex        =   53
               Top             =   180
               Width           =   3120
            End
            Begin VB.Label Label27 
               AutoSize        =   -1  'True
               Caption         =   "Country:"
               DataField       =   "County"
               Height          =   195
               Left            =   2745
               TabIndex        =   50
               Top             =   2100
               Visible         =   0   'False
               Width           =   570
            End
            Begin VB.Label Label26 
               AutoSize        =   -1  'True
               Caption         =   "City:"
               Height          =   195
               Left            =   1785
               TabIndex        =   49
               Top             =   1740
               Width           =   300
            End
            Begin VB.Label Label25 
               AutoSize        =   -1  'True
               Caption         =   "Type:"
               Height          =   195
               Left            =   90
               TabIndex        =   48
               Top             =   345
               Width           =   420
            End
            Begin VB.Label Label24 
               AutoSize        =   -1  'True
               Caption         =   "Address:"
               Height          =   195
               Left            =   90
               TabIndex        =   47
               Top             =   705
               Width           =   645
            End
            Begin VB.Label Label23 
               AutoSize        =   -1  'True
               Caption         =   "State:"
               Height          =   195
               Left            =   90
               TabIndex        =   46
               Top             =   1740
               Width           =   405
            End
            Begin VB.Label Label22 
               AutoSize        =   -1  'True
               Caption         =   "County:"
               DataField       =   "County"
               Height          =   195
               Left            =   90
               TabIndex        =   45
               Top             =   2100
               Width           =   525
            End
            Begin VB.Label Label21 
               AutoSize        =   -1  'True
               Caption         =   "Zip:"
               Height          =   195
               Left            =   3990
               TabIndex        =   44
               Top             =   1725
               Width           =   270
            End
            Begin VB.Label Label20 
               AutoSize        =   -1  'True
               Caption         =   "Main Telephone:"
               Height          =   195
               Left            =   90
               TabIndex        =   43
               Top             =   2430
               Width           =   1215
            End
            Begin VB.Label Label19 
               AutoSize        =   -1  'True
               Caption         =   "Main Fax:"
               Height          =   195
               Left            =   3090
               TabIndex        =   42
               Top             =   2460
               Width           =   690
            End
            Begin VB.Label Label18 
               AutoSize        =   -1  'True
               Caption         =   "Email:"
               Height          =   195
               Left            =   90
               TabIndex        =   41
               Top             =   2805
               Width           =   450
            End
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74892
            TabIndex        =   8
            Top             =   5172
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   4425
            Left            =   -74904
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   7
            Top             =   384
            Width           =   7035
         End
         Begin VB.Frame fraFirm 
            Caption         =   "Firm"
            Height          =   1776
            Left            =   96
            TabIndex        =   16
            Top             =   384
            Width           =   7008
            Begin VB.TextBox txtDatafield 
               DataField       =   "stateBarNumber"
               DataMember      =   "Firm"
               Height          =   288
               Index           =   2
               Left            =   5880
               TabIndex        =   5
               Top             =   612
               Width           =   1020
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "organizationLocator"
               DataMember      =   "Organization"
               Height          =   288
               Index           =   1
               Left            =   840
               TabIndex        =   0
               Top             =   240
               Width           =   960
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "firmNote"
               DataMember      =   "Firm"
               Height          =   288
               Index           =   0
               Left            =   5340
               TabIndex        =   23
               Top             =   1320
               Visible         =   0   'False
               Width           =   1572
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "organizationLegalForm"
               DataMember      =   "Organization"
               Height          =   315
               Index           =   1
               ItemData        =   "frmFirmEdit.frx":006E
               Left            =   2376
               List            =   "frmFirmEdit.frx":0070
               Style           =   2  'Dropdown List
               TabIndex        =   1
               Top             =   240
               Width           =   816
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "federalTaxNumber"
               DataMember      =   "Organization"
               Height          =   288
               Index           =   17
               Left            =   5880
               TabIndex        =   3
               Top             =   240
               Width           =   1020
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "organizationName"
               DataMember      =   "Organization"
               Height          =   288
               Index           =   5
               Left            =   840
               TabIndex        =   4
               Top             =   612
               Width           =   3972
            End
            Begin MSMask.MaskEdBox mskEstablishYear 
               Height          =   288
               Left            =   4524
               TabIndex        =   2
               Top             =   240
               Width           =   564
               _ExtentX        =   979
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   4
               Mask            =   "####"
               PromptChar      =   "_"
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmFirmEdit.frx":0072
               DataField       =   "nextEffectiveDate"
               DataMember      =   "Firm"
               Height          =   288
               Index           =   0
               Left            =   1728
               TabIndex        =   6
               Top             =   1236
               Width           =   1400
               _ExtentX        =   2487
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   20709377
               CurrentDate     =   37225
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               Caption         =   "State Bar Id:"
               Height          =   192
               Left            =   4956
               TabIndex        =   27
               Top             =   660
               Width           =   864
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Next Effective Date:"
               Height          =   192
               Left            =   192
               TabIndex        =   22
               Top             =   1272
               Width           =   1392
            End
            Begin VB.Label Label16 
               AutoSize        =   -1  'True
               Caption         =   "Fed Id:"
               Height          =   192
               Left            =   5328
               TabIndex        =   21
               Top             =   288
               Width           =   492
            End
            Begin VB.Label Label13 
               Caption         =   "Establish Year:"
               Height          =   192
               Left            =   3348
               TabIndex        =   20
               Top             =   288
               Width           =   1104
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "Type:"
               Height          =   204
               Left            =   1884
               TabIndex        =   19
               Top             =   282
               Width           =   420
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               Height          =   192
               Left            =   108
               TabIndex        =   18
               Top             =   660
               Width           =   480
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Locator:"
               Height          =   192
               Left            =   108
               TabIndex        =   17
               Top             =   288
               Width           =   576
            End
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   2790
         Top             =   6015
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   3345
         Top             =   6030
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "cmdEnter"
      Height          =   336
      Left            =   3816
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   684
      Width           =   1308
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   13980
      _ExtentX        =   24659
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   13980
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   13860
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   30
         Width           =   13860
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   72
            TabIndex        =   15
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Label Label14 
      Caption         =   "Label14"
      Height          =   12
      Left            =   3252
      TabIndex        =   11
      Top             =   3096
      Width           =   972
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Firm"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuseperator9879 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewLocation 
            Caption         =   "L&ocation (Primary)"
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   0
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Diary"
            Index           =   2
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "L&ocation (Primary)"
            Index           =   3
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator28162 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact (Firm)"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "L&ocation (Primary)"
         Index           =   1
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator693 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmFirmEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.20.0611 (jjm)
'
' frmUnderwritingContact
'
' This form provides the user interface to the ContactFirmDataset class.
'

'
' Future Development
'
' -Options Form that has Options for a Save and Add command button.

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mFirmHandle As String
Private mOrganizationHandle As String
Private mLocationHandle As String
Private mEventHandle As String
Private mPersonHandle As String

Private mNew As Boolean
Private mDatafields As Collection

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private SaveToPath As String
Private ApplicationFolder As String

' Form Manager
Private FormMgr As New FormManager

' Signal that a refresh is being called.
Private mRefresh As Boolean

'Constants
Private Const TXT_FIRMLOCATOR_INDEX = 1


Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

'
' FirmHandle needed when linking/loading a new firm.  Used in
' application edit when selecting a new firm for the application.
'
Public Property Get FirmHandle() As String
  FirmHandle = mFirmHandle
End Property

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Dim Handle As String
  
  Select Case fInterface.Message
    Case fimNone
      Show
    Case fimNew
      If fInterface.Key = DatabaseName.LOCATION_TABLE Then
        mnuFileNewLocation_Click
      End If
    Case fimSelect
      If fInterface.Key = DatabaseName.LOCATION_TABLE Then
        Handle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
        If mLocationHandle <> Handle Then
          mLocationHandle = Handle
          Dirty = True
        End If
        LocationRefresh
      End If
    Case fimSave
      If fInterface.Key = DatabaseName.LOCATION_TABLE & "Edit" Then
        Handle = fInterface.Child.LocationHandle
        If mLocationHandle <> Handle Then
          mLocationHandle = Handle
          Dirty = True
        End If
        LocationRefresh
      End If
  End Select
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  MaskEditClear mskEstablishYear
  
  ClearDataField txtDataField
  'ClearDataField mskDataField
  ClearDataField dtpDataField
  ClearDataField lblDataField
  
  If (mNew) Then
    Set mDatafields = New Collection
    Me.Caption = "New Firm"
    mFirmHandle = vbNullString
    mPersonHandle = vbNullString
    dtpDataField(0).Value = Today
  Else
    Dim Row As Collection
    Set Row = Factory.FirmService.Find(mFirmHandle)
    
    mOrganizationHandle = Row(DatabaseName.ORGANIZATION_KEY)
    Dim OrgData As Collection
    Set OrgData = Factory.OrganizationService.Find(mOrganizationHandle)
    
    'need to setup organization info ---- & Row(DatabaseName.ORGANIZATIONLOCATOR_FIELD) &
    Caption = "Edit Firm - " & OrgData(DatabaseName.ORGANIZATIONLOCATOR_FIELD) & " (" & mFirmHandle & ")"
    
    If Row Is Nothing Then
      MsgBox "Firm id " & mFirmHandle & " no longer exists."
      mNew = True
      FormRefresh
      Exit Sub
    End If

    If Not OrgData(DatabaseName.ESTABLISHDATE_FIELD) = vbNullString Then
      mskEstablishYear = Year(OrgData(DatabaseName.ESTABLISHDATE_FIELD))
    End If
    SetFields txtDataField, Row, "Firm"
    'SetFields mskDataField, Row, "Firm"
    SetFields dtpDataField, Row, "Firm"
    
    SetFields txtDataField, OrgData, "Organization"
    SetFields cboDataField, OrgData, "Organization"
    

    mLocationHandle = OrgData(DatabaseName.MAINLOCATIONID_FIELD)
    mPersonHandle = OrgData(DatabaseName.MAINPERSONID_FIELD)

    mEventHandle = OrgData(DatabaseName.EVENT_KEY)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
  
  End If
  
  LocationRefresh
  
  mnuFileOpen(0).Enabled = Not mNew
  mnuFileOpen(2).Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  
  ' Refresh navigation controls
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' Set change status to false
  Dirty = False
End Sub
    
Private Sub FormResize()
  Height = picbody.Top + picbody.Height + (Height - ScaleHeight)
End Sub
    
Public Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mFirmHandle = ListItemText(mListView.SelectedItem, DatabaseName.FIRM_KEY)
End Property

Private Sub LocationRefresh()
  SetFields lblDataField, Factory.LocationService.Find(mLocationHandle), "Location"
End Sub

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
  ' See if we really should be adding a new location.
  If mNew And mLocationHandle = vbNullString Then
    Select Case MsgBox("You can not save a new firm without creating a location.  Would you like to create a location now?", vbYesNo)
      Case vbYes
        mnuFileNewLocation_Click
        Exit Function
      Case Else
        Exit Function
    End Select
  End If
  
  'Check for safe locator
  'ver 2.75.00 (jjm)
  'Make sure that the locator will not be the same as an exisiting firmId
  If Not Factory.FirmService.verifyFirmLocator(txtDataField(TXT_FIRMLOCATOR_INDEX).Text, FirmHandle) Then
    Exit Function
  End If
  
  ' Custom Code
  Dim mDataControls As New Collection
  AddTextControl txtDataField, mDataControls
  AddTextControl cboDataField, mDataControls
  'AddTextControl mskDataField, mDataControls
  AddValueControl dtpDataField, mDataControls

  Dim mEstablishDate As Date
  If mskEstablishYear.ClipText <> vbNullString Then
    mEstablishDate = CDate("1/1/" & mskEstablishYear)
  End If
  
  

  If (mNew) Then
    mFirmHandle = Factory.FirmService.CreateFirm( _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONLOCATOR_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONLOCATOR_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONNAME_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.STATEBARNUMBER_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.FEDERALTAXNUMBER_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONLEGALFORM_FIELD), _
        mEstablishDate, _
        GetDate(GetDataFromCollection(mDataControls, NEXTEFFECTIVEDATE_FIELD)), _
        mLocationHandle, mPersonHandle, txtNote.Text)
    save = mFirmHandle <> vbNullString
  Else
    save = Factory.FirmService.WriteFirm( _
        mFirmHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONLOCATOR_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONNAME_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.STATEBARNUMBER_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.FEDERALTAXNUMBER_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONLEGALFORM_FIELD), _
        mEstablishDate, _
        GetDate(GetDataFromCollection(mDataControls, NEXTEFFECTIVEDATE_FIELD)), _
        mLocationHandle, mPersonHandle, txtNote.Text)

  End If
  
  Dirty = Not save
End Function

Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no contacts to view or edit." ' To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(FirmHandle As String)
  mFirmHandle = FirmHandle
  Show
End Sub


Public Sub ShowNew()
  mNew = True
  Show
End Sub
'
' End General Methods
'
'
'
'
' Begin Control Methods
'


Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Load selection of location.
'
Private Sub cmdLocation_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr, True
End Sub

Private Sub cmdSave_Click()
  Dim WasNew As Boolean
  WasNew = mNew
  If save Then
    FormMgr.Message = fimSave
    If TypeOf FormMgr.Parent Is frmFirmOpen Then
      Unload Me
      Exit Sub
    End If
    
    Unload Me
    
    If WasNew Then
      ' do not use form manager since we unloaded.
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle
    End If
  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0) Then
    If KeyCode = vbKeyTab Then
      sstDetail.SetFocus
    End If
  End If
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyO
        mnuFileNewLocation_Click
        KeyCode = 0
    End Select
  End If
End Sub

'
' Load Controls
'
Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  mnuFileNewLocation.Caption = mnuFileNewLocation.Caption & vbTab + "Ctrl+Shift+O"
  
  FormUtil.ComboBoxRegistryLoad cboDataField(1), "organizationType"
  
  FormResize
  TabControlEnable sstDetail
  FormRefresh
End Sub

'
' Terminate Used for memory audit
'
Private Sub Form_Terminate()
  'MsgBox "frmApplicationEdit.Terminate"
End Sub

'
' See if form can unload.
'
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = 1
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowByFirm mFirmHandle, DatabaseName.FIRM_TABLE
    Case 1
      If mLocationHandle = vbNullString Then
        MsgBox "No default location has yet to be assigned to this firm."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmLocationEdit, mLocationHandle, FormMgr, , DatabaseName.LOCATION_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If save(True) Then
    mNew = True
    FormRefresh
  End If
End Sub

Private Sub mnuFileNewLocation_Click()
  Dim fForm As New frmLocationEdit
  FormMgr.Add fForm, DatabaseName.LOCATION_TABLE & "Edit"
  fForm.ShowNew mOrganizationHandle
End Sub


Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
    'Case 1 ' menu separator
    Case 2
      FormUtil.ShowByPrimaryKey New frmFirmDiaryOpen, mFirmHandle, FormMgr
    Case 3
      If mLocationHandle = vbNullString Then
        MsgBox "No default location has yet to be assigned to this person."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmLocationOpen, mLocationHandle, FormMgr, , DatabaseName.LOCATION_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.FIRM_TABLE, mFirmHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.FIRM_REF, DatabaseName.FIRM_TABLE, mFirmHandle, txtDataField(5)
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.APPLICATION_TABLE, FormMgr
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub

Private Sub mskDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub mskDataField_Validate(Index As Integer, Cancel As Boolean)
  Select Case Index
    Case 0
      If mNew Then
        If Not Factory.FirmService.Find(txtDataField(1).Text, """firmId""") Is Nothing Then
          MsgBox "Firm locator already exists."
          Cancel = True
        End If
      End If
  End Select
End Sub

Private Sub mskEstablishYear_Change()
  Dirty = True
End Sub

Private Sub mskEstablishYear_GotFocus()
  FormUtil.EditTextSelect mskEstablishYear
End Sub

Private Sub mskEstablishYear_Validate(Cancel As Boolean)
  If mskEstablishYear = "____" Then Exit Sub
  Dim mYear As Integer
  mYear = Year(Now) + 1
  If mskEstablishYear.ClipText < 1700 Or mskEstablishYear.ClipText > (Year(Now) + 2) Then
    MsgBox "Establish Year must be between 1700 and " & (Year(Now) + 2)
    Cancel = True
  End If
End Sub


Private Sub sstDetail_Click(PreviousTab As Integer)
  TabControlEnable sstDetail
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
  If Not mNew Or txtDataField(Index).Text = "" Then
    Exit Sub
  End If
  Select Case Index
    Case 15
      txtDataField(15).Text = lblDataField(5).Caption
    Case 14
      txtDataField(14).Text = lblDataField(6).Caption
  End Select
End Sub

Private Sub txtDataField_KeyPress(Index As Integer, KeyAscii As Integer)
  Select Case Index
    Case 1
      If Len(txtDataField(Index).Text) >= 6 And KeyAscii <> 8 Then
        If txtDataField(Index).SelLength = 0 Then
          KeyAscii = 0
          Exit Sub
        End If
      End If
            
      Select Case KeyAscii
        Case 32         'Converts space to '_'
          KeyAscii = 95
        Case 48 To 57   'Accept Digits
        Case 65 To 90   'Accept Upper Case letters
        Case 97 To 122  'Convert lower case to upper
          KeyAscii = KeyAscii - 32
        Case 95         'Accept the '_'
        Case 8          'Accept the backspace to delete
        Case Else       'Ignore all other key stokes
          KeyAscii = 0
      End Select
  End Select

End Sub

'Private Sub txtDataField_LostFocus(Index As Integer)
'  Select Case Index
'    Case 1 ' Middle Initial
'      txtDataField(1).Text = UCase(txtDataField(1).Text) + "."
'  End Select
'End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub


