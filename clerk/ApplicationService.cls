VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ApplicationService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "ApplicationService"
Private mDataSet As Dataset


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.APPLICATION_TABLE)
  
  'Setup SOAP
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.APPLICATION_KEY, Handle, Column)
End Function

Public Function FindByEvent(EventHandle As String, Optional Column As String) As Collection
  Set FindByEvent = mDataSet.DataProvider.Find(DatabaseName.EVENT_KEY, EventHandle, Column)
End Function

'
' Interface to server
'

'
' Create an application.
'
Public Function CreateApplication( _
    EventTypeId As String, _
    FirmId As String, _
    PolicyId As String, _
    FirmName As String, _
    EffectiveDate As Date, _
    StaffCount As Double, _
    PolicyTerm As Double, _
    ReceiveDate As Date, _
    eventNote As String, _
    FolderPath As String _
    ) As String
On Error GoTo Exception
    
  CreateApplication = Client.Create( _
      EventTypeId, _
      FirmId, _
      PolicyId, _
      FirmName, _
      EffectiveDate, _
      StaffCount, _
      PolicyTerm, _
      ReceiveDate, _
      eventNote, _
      FolderPath)
  Exit Function

Exception:
  ErrorUtil.DisplaySoapError Err, Client
End Function

'
' Write an application.
'
Public Function WriteApplication( _
    ApplicationId As String, _
    EventTypeId As String, _
    FirmName As String, _
    EffectiveDate As Date, _
    StaffCount As Double, _
    PolicyTerm As Double, _
    ReceiveDate As Date, _
    eventNote) As Boolean
On Error GoTo Exception
    
  Call Client.Write( _
      ApplicationId, _
      EventTypeId, _
      FirmName, _
      EffectiveDate, _
      StaffCount, _
      PolicyTerm, _
      ReceiveDate, _
      eventNote)
  WriteApplication = True
  Exit Function
  
      ' ReceiveDate,
  
Exception:
  'MsgBox "Fault: " & Client.FaultString, vbExclamation
  'Debug.Print Client.FaultString, vbExclamation
  ErrorUtil.DisplaySoapError Err, Client
  
  WriteApplication = False
End Function

'
' test for double array functionality
'

Public Function getExplicit() As Collection
On Error GoTo Exception

  Dim dblArray As Variant
  dblArray = Client.getExplicit()
  Dim Result As New Collection
  'popuplate the collection
  Dim I As Integer
  For I = 0 To SafeUBound(dblArray)
    Result.Add dblArray(I)(1), dblArray(I)(0)
  Next I
  Set getExplicit = Result
  Exit Function
    
Exception:
  ErrorUtil.DisplaySoapError Err, Client
  Set getExplicit = New Collection
End Function


'
' Prepare
'

Public Function applicationPrepare(FirmId As String) As Collection
On Error GoTo Exception

  Dim dblArray As Variant
  dblArray = Client.Prepare(FirmId)
  Dim Result As New Collection
  'popuplate the collection
  Dim I As Integer
  For I = 0 To SafeUBound(dblArray)
    Result.Add dblArray(I)(1), dblArray(I)(0)
  Next I
  Set applicationPrepare = Result
  Exit Function
    
Exception:
  ErrorUtil.DisplaySoapError Err, Client
  Set applicationPrepare = New Collection
End Function

