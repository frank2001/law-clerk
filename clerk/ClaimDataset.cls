VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClaimDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.50.00 (jjm) 2004 02 10
'previous ver 2.20.0612 (jjm)
'previous ver 2.20.0610 (jjm)
'previous ver 2.10.0421 (jjm)

' ClaimDataset

Option Explicit

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private mSQLDataProvider As DataProvider

Private Words As String
Private ListIndex As Integer

Private Const DEFAULT_INDEX = CLAIMSEARCH_CLAIMNUMBER_INDEX

Private Sub Class_Initialize()
  ListIndex = DEFAULT_INDEX
End Sub

Public Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider(DatabaseName.CLAIM_TABLE)
    
  'Backwards compatible data provider
  Set mSQLDataProvider = vData.NewSQLDataProvider
  mSQLDataProvider.Source = DatabaseName.CLAIM_VIEW
End Property

Public Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property

Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property

Public Property Get SQLDataProvider() As DataProvider
  Set SQLDataProvider = mSQLDataProvider
End Property

Public Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in adjuster field only"
  frm.cboIn.AddItem "in claimant field only"
  frm.cboIn.AddItem "in claim firm name field only"
  frm.cboIn.AddItem "in claim number field only"
  frm.cboIn.AddItem "in firm identifier field only"
  frm.cboIn.AddItem "in firm entity locator field only"
  frm.cboIn.AddItem "in policy identifier field only"
  frm.cboIn.AddItem "in policy number field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, DEFAULT_INDEX)
End Sub

Public Sub ClaimSearchFilter(Main As frmClaimSearch, newFilter As Filter)
  
  'Claim name
  If Main.chkClaimName.Value = 1 And Main.txtClaimName <> vbNullString Then
    newFilter.Add LawSQL.UCaseWildCard(DatabaseName.CLAIMNAME_FIELD, Main.txtClaimName)
  End If
  
  'Claimant
  If Main.chkClaimant.Value = 1 And Main.txtClaimant <> vbNullString Then
    newFilter.Add LawSQL.UCaseWildCard(DatabaseName.CLAIMANT_FIELD, Main.txtClaimant)
  End If
  
  'Tortfeasor
  If Main.chkTortfeasor.Value = 1 Then
    '"claimId" in (select "instanceId" as "claimId" from "InstanceLawyer" where "lawyerId" = 'VALUE');
    newFilter.Add """" & DatabaseName.CLAIM_KEY & """ in (select """ & DatabaseName.INSTANCE_KEY & """ as """ & DatabaseName.CLAIM_KEY & """ from """ & DatabaseName.INSTANCELAWYER_TABLE & """ where  " & LawSQL.EqualString(DatabaseName.LAWYER_KEY, Main.lblTortfeasor) & ")"
  End If
  
  'Law Area
  If Main.chkLawArea.Value = 1 Then
    newFilter.Add """" & DatabaseName.CLAIM_KEY & """ in (select """ & DatabaseName.INSTANCE_KEY & """ as """ & DatabaseName.CLAIM_KEY & """ from """ & DatabaseName.INSTANCELAWAREA_TABLE & """ where " & LawSQL.EqualString(DatabaseName.LAWAREA_KEY, Main.LawAreaKey) & ")"
  End If
  
  'Law Error
  If Main.chkLawError.Value = 1 Then
    'newFilter.Add """claimId"" in (select ""claimId"" from ""ClaimLawError"" where ""lawErrorId"" = '" & Main.LawErrorKey & "')"
    newFilter.Add """" & DatabaseName.CLAIM_KEY & """ in (select """ & DatabaseName.INSTANCE_KEY & """ as """ & DatabaseName.CLAIM_KEY & """ from """ & DatabaseName.INSTANCELAWERROR_TABLE & """ where " & LawSQL.EqualString(DatabaseName.LAWERROR_KEY, Main.LawErrorKey) & ")"
  End If
  
  'County
  If Main.chkCounty.Value = 1 Then
    MsgBox "ToDo: Enable county search"
'    newFilter.Add """" & DatabaseName.FIRM_KEY & """ in " & _
'      "(select """ & DatabaseName.ENTITY_KEY & """ as """ & DatabaseName.FIRM_KEY & """ " & _
'       "from """ & DatabaseName.entitylocation_TABLE & """ " & _
'       "where """ & DatabaseName.MAINLOCATIONID_FIELD & """ in " & _
'          "(select """ & DatabaseName.LOCATION_KEY & """ as """ & DatabaseName.MAINLOCATIONID_FIELD & """ " & _
'           "from """ & DatabaseName.LOCATION_TABLE & """ " & _
'           "where " & LawSQL.UCaseStartsWith(DatabaseName.COUNTY_FIELD, Main.txtCounty) & "))"
  End If
  
  'Status (Open, Closed, Re-Open) w/ or w/out dates
  If (Main.chkStatus.Value = 1) Then
    If Main.optStatus(0).Value Then
      newFilter.Add IIf(Main.chkDate.Value = 1, """" & DatabaseName.OPENDATE_FIELD & """ >= " & LawSQL.DateFormat(Main.dtpFrom) & " and """ & DatabaseName.OPENDATE_FIELD & """ <= " & LawSQL.DateFormat(Main.dtpTo), """" & DatabaseName.CLOSEDATE_FIELD & """ IS NULL")
    End If
    If Main.optStatus(1).Value Then
      newFilter.Add IIf(Main.chkDate.Value = 1, """" & DatabaseName.CLOSEDATE_FIELD & """ >= " & LawSQL.DateFormat(Main.dtpFrom) & " and """ & DatabaseName.CLOSEDATE_FIELD & """ <= " & LawSQL.DateFormat(Main.dtpTo), """" & DatabaseName.CLOSEDATE_FIELD & """ IS NOT NULL")
    End If
    If Main.optStatus(2).Value Then
      newFilter.Add IIf(Main.chkDate.Value = 1, """" & DatabaseName.REOPENDATE_FIELD & """ >= " & LawSQL.DateFormat(Main.dtpFrom) & " and """ & DatabaseName.REOPENDATE_FIELD & """ <= " & LawSQL.DateFormat(Main.dtpTo) & " and ", vbNullString) & """" & DatabaseName.CLOSEDATE_FIELD & """ IS NULL and """ & DatabaseName.REOPENDATE_FIELD & """ IS NOT NULL"
    End If
  End If
   
  'ACA - Adjuster
  ' ver 2.60.00 (jjm) 2004-05-17 modified the search to use the case

  If Main.chkACA.Value = 1 Then
    newFilter.Add LawSQL.UCaseWildCard(DatabaseName.ADJUSTER_FIELD, Main.cboAdjuster.Text)
  End If
  
  'Claim Diary Date
  If Main.chkDiaryDate.Value = vbChecked Then
'    newFilter.Add """claimId"" IN (SELECT ""referenceId1"" AS ""claimId"" FROM ""Reference"" " & _
'        "WHERE ""eventId"" IN (SELECT ""eventId"" FROM ""Event"" WHERE " & _
'        """eventTypeId"" = 'ClaimDiary' and ""dueDate"" >= " & lawsql.DateFormat(Main.dtpDiaryFromDate) & " and ""dueDate"" <= " & lawsql.DateFormat(Main.dtpDiaryToDate) & "))"

    newFilter.Add """" & DatabaseName.EVENT_KEY & """ IN (" & _
        "SELECT """ & DatabaseName.EVENTREFERENCE_KEY & """ AS """ & DatabaseName.EVENT_KEY & """ " & _
        "FROM """ & DatabaseName.EVENTREFERENCE_VIEW & """ " & _
        "WHERE " & _
            """" & DatabaseName.EVENTTYPE_KEY & """ = '" & DatabaseName.CLAIMDIARY_TYPE & "' and " & _
            """" & DatabaseName.EVENTSTATUS_FIELD & """ = '" & DatabaseName.PENDING_EVENTSTATUS & "' and " & _
            """" & DatabaseName.DUEDATE_FIELD & """ >= " & LawSQL.DateFormat(Main.dtpDiaryFromDate) & " and " & _
            """" & DatabaseName.DUEDATE_FIELD & """ <= " & LawSQL.DateFormat(Main.dtpDiaryToDate) & _
            IIf(Main.chkACA.Value = 1, " and " & LawSQL.UCaseWildCard(DatabaseName.DUEUSERID_FIELD, Main.cboAdjuster.Text), vbNullString) & ")"
  End If
  
  'Open As Of Date
  If Main.chkOpenAsOf.Value = vbChecked Then
    newFilter.Add """" & DatabaseName.OPENDATE_FIELD & """ <= " & LawSQL.DateFormat(Main.dtpOpenAsOf) & " and (" & _
                  """" & DatabaseName.CLOSEDATE_FIELD & """ >= " & LawSQL.DateFormat(Main.dtpOpenAsOf) & " OR " & _
                  """" & DatabaseName.CLOSEDATE_FIELD & """ is null)"
  End If
  
  
  'Is Reinsurer Report
  If Main.chkIsReinsurerReport.Value = 1 Then
    newFilter.Add """" & DatabaseName.ISREINSURERREPORT_FIELD & """ = 'Yes'"
  End If
    
  'ver 2.72.00 (jjm) 2005 04 19
  'Policy Limits Claim search option
  If Main.chkPolicyLimitsClaim.Value = 1 Then
    newFilter.Add """" & DatabaseName.EVENT_KEY & """ IN " & _
       "(SELECT """ & DatabaseName.EVENTREFERENCE_KEY & """ AS """ & DatabaseName.EVENT_KEY & """ " & _
        "FROM """ & DatabaseName.EVENTREFERENCE_VIEW & """ " & _
        "WHERE " & _
            LawSQL.EqualString(DatabaseName.EVENTTYPE_KEY, DatabaseName.POLICYLIMITSCLAIM_TYPE) & " and " & _
            LawSQL.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD) & ")"
  End If
    
  'ToDO: Insure that the debug code below is commented out
  'MsgBox newFilter.Filter
  'Debug.Print newFilter.Filter
  
End Sub

Public Function Dataset_FindByForm(frm As Form) As Collection
  'Hack for SQL API
  If TypeOf frm Is frmClaimSearch Then
    FindByFormSQL frm
    Exit Function
  End If

  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case UrlName.CLAIMSEARCH_ADJUSTER_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.ADJUSTER_FIELD, Words)
      Case UrlName.CLAIMSEARCH_CLAIMANT_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.CLAIMANT_FIELD, Words)
      Case UrlName.CLAIMSEARCH_FIRMNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.FIRMENTITYNAME_FIELD, Words)
      Case UrlName.CLAIMSEARCH_CLAIMNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.CLAIMNUMBER_FIELD, Words)
      Case UrlName.CLAIMSEARCH_FIRMENTITYLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.FIRMENTITYLOCATOR_FIELD, Words)
      Case UrlName.CLAIMSEARCH_FIRMID_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.FIRM_KEY, Words)
      Case UrlName.CLAIMSEARCH_POLICYID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.POLICY_KEY, Words)
      Case UrlName.CLAIMSEARCH_POLICYNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.POLICYNUMBER_FIELD, Words)
    End Select
  End If
  
  If TypeOf frm Is frmClaimSearch Then
    ClaimSearchFilter frm, newFilter
  End If
  
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function


'Hack for SQL API
Private Function FindByFormSQL(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case UrlName.CLAIMSEARCH_ADJUSTER_INDEX
        newFilter.Add LawSQL.UCaseWildCard(DatabaseName.ADJUSTER_FIELD, Words)
      Case UrlName.CLAIMSEARCH_CLAIMANT_INDEX
        newFilter.Add LawSQL.UCaseWildCard(DatabaseName.CLAIMANT_FIELD, Words)
      Case UrlName.CLAIMSEARCH_CLAIMNUMBER_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.CLAIMNUMBER_FIELD, Words)
      Case UrlName.CLAIMSEARCH_FIRMENTITYLOCATOR_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.FIRMNAME_FIELD, Words)
      Case UrlName.CLAIMSEARCH_FIRMNAME_INDEX
        newFilter.Add LawSQL.UCaseWildCard(DatabaseName.FIRMENTITYNAME_FIELD, Words)
      Case UrlName.CLAIMSEARCH_FIRMID_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.FIRM_KEY, Words)
      Case UrlName.CLAIMSEARCH_POLICYID_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.POLICY_KEY, Words)
      Case UrlName.CLAIMSEARCH_POLICYNUMBER_INDEX
        newFilter.Add LawSQL.UCaseStartsWith(DatabaseName.POLICYNUMBER_FIELD, Words)
      
    End Select
  End If
  
  If TypeOf frm Is frmClaimSearch Then
    ClaimSearchFilter frm, newFilter
  End If
  
  mSQLDataProvider.Filter = newFilter
  mSQLDataProvider.OpenRecordset
  mSQLDataProvider.SetListView frm.lvView
End Function

