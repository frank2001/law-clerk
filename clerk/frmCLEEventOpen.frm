VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCLEEventOpen 
   Caption         =   "CLE Event"
   ClientHeight    =   6225
   ClientLeft      =   135
   ClientTop       =   1005
   ClientWidth     =   7710
   Icon            =   "frmCLEEventOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6225
   ScaleWidth      =   7710
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5568
      Left            =   0
      ScaleHeight     =   5565
      ScaleWidth      =   7710
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   384
      Width           =   7716
      Begin VB.Frame fraCLEEvent 
         Caption         =   "CLE Event"
         Height          =   2112
         Left            =   48
         TabIndex        =   2
         Top             =   36
         Width           =   7536
         Begin VB.Label lblLocation 
            AutoSize        =   -1  'True
            Caption         =   "Location"
            Height          =   192
            Left            =   132
            TabIndex        =   28
            Top             =   996
            Width           =   612
         End
         Begin VB.Label lblTopic 
            AutoSize        =   -1  'True
            Caption         =   "Topic"
            Height          =   192
            Left            =   132
            TabIndex        =   27
            Top             =   756
            Width           =   420
         End
         Begin VB.Label lblCLECredits 
            AutoSize        =   -1  'True
            Caption         =   "CLE Credits"
            Height          =   192
            Left            =   132
            TabIndex        =   26
            Top             =   1716
            Width           =   840
         End
         Begin VB.Label lblDate 
            AutoSize        =   -1  'True
            Caption         =   "Date"
            Height          =   192
            Left            =   132
            TabIndex        =   25
            Top             =   1476
            Width           =   348
         End
         Begin VB.Label lblAttg 
            AutoSize        =   -1  'True
            Caption         =   "Attg"
            Height          =   192
            Left            =   132
            TabIndex        =   24
            Top             =   1236
            Width           =   276
         End
         Begin VB.Label lblEventName 
            AutoSize        =   -1  'True
            Caption         =   "Event Name"
            Height          =   192
            Left            =   132
            TabIndex        =   23
            Top             =   516
            Width           =   888
         End
         Begin VB.Label lblEventID 
            AutoSize        =   -1  'True
            Caption         =   "Event ID"
            Height          =   192
            Left            =   132
            TabIndex        =   22
            Top             =   276
            Width           =   600
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "cleCredit"
            DataField       =   "cleCredit"
            Height          =   192
            Index           =   6
            Left            =   1200
            TabIndex        =   21
            Top             =   1716
            Width           =   636
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventDate"
            DataField       =   "eventDate"
            Height          =   192
            Index           =   5
            Left            =   1200
            TabIndex        =   20
            Top             =   1476
            Width           =   744
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "attendingStaff"
            DataField       =   "attendingStaff"
            Height          =   192
            Index           =   4
            Left            =   1200
            TabIndex        =   19
            Top             =   1236
            Width           =   972
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventLocation"
            DataField       =   "eventLocation"
            Height          =   192
            Index           =   3
            Left            =   1200
            TabIndex        =   18
            Top             =   996
            Width           =   1008
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventTopic"
            DataField       =   "eventTopic"
            Height          =   192
            Index           =   2
            Left            =   1200
            TabIndex        =   17
            Top             =   756
            Width           =   816
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventName"
            DataField       =   "eventName"
            Height          =   192
            Index           =   1
            Left            =   1200
            TabIndex        =   16
            Top             =   528
            Width           =   840
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "cleEventId"
            DataField       =   "cleEventId"
            Height          =   192
            Index           =   0
            Left            =   1200
            TabIndex        =   15
            Top             =   276
            Width           =   756
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   1224
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   792
         Width           =   1416
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3132
         Left            =   48
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   2244
         Width           =   7608
         _ExtentX        =   13414
         _ExtentY        =   5530
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabsPerRow      =   10
         TabHeight       =   420
         TabCaption(0)   =   "Lawyer"
         TabPicture(0)   =   "frmCLEEventOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         Begin MSComctlLib.ListView lvView 
            Height          =   2640
            Index           =   0
            Left            =   96
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "CLEEventCLEEventLawyer"
            Top             =   372
            Width           =   7380
            _ExtentX        =   13018
            _ExtentY        =   4657
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   7716
      _ExtentX        =   13600
      _ExtentY        =   688
      _CBWidth        =   7710
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picMenu"
      MinWidth1       =   795
      MinHeight1      =   330
      Width1          =   3510
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   990
      MinHeight2      =   330
      Width2          =   990
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   330
      Width3          =   945
      NewRow3         =   0   'False
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   6870
         ScaleHeight     =   330
         ScaleWidth      =   750
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   30
         Width           =   750
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   14
            Top             =   12
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   165
         ScaleHeight     =   330
         ScaleWidth      =   3315
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   30
         Width           =   3315
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   660
            Left            =   1830
            TabIndex        =   10
            Top             =   15
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   1164
            ButtonWidth     =   1640
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "CLEEvent"
                        Text            =   "CLE Event"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "CLEEventLawyer"
                        Text            =   "Lawyer"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   660
            Left            =   990
            TabIndex        =   9
            Top             =   15
            Width           =   750
            _ExtentX        =   1323
            _ExtentY        =   1164
            ButtonWidth     =   1191
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "CLE Event"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Lawyer "
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   660
            Left            =   0
            TabIndex        =   8
            Top             =   15
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   1164
            ButtonWidth     =   1270
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Lawyer"
                        Text            =   "Lawyer"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   3705
         ScaleHeight     =   330
         ScaleWidth      =   2940
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   30
         Width           =   2940
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   276
            Left            =   0
            TabIndex        =   6
            Top             =   12
            Width           =   1044
            _ExtentX        =   1852
            _ExtentY        =   476
            ButtonWidth     =   1508
            ButtonHeight    =   582
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   11
      Top             =   5955
      Width           =   7710
      _ExtentX        =   13600
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5027
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "10/5/2006"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "5:34 PM"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   576
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   60
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&CLE Event"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator999 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewLawyer 
            Caption         =   "&Lawyer"
         End
      End
      Begin VB.Menu mnuSeparator847 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Enabled         =   0   'False
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Enabled         =   0   'False
         Index           =   1
      End
      Begin VB.Menu mnuSeparator673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To "
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeparator3412 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Enabled         =   0   'False
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator388 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&CLE Event"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Lawyer"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator3832 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "T&ab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Lawyer"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator123 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings"
      End
      Begin VB.Menu mnuSeparator2384 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuViewBrowseCLEEvent 
         Caption         =   "&Browse CLE Event"
         Enabled         =   0   'False
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuViewBrowseLawyer 
         Caption         =   "Browse &Lawyer"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeparator5673 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Enabled         =   0   'False
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeparator8343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&Workflow..."
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuseperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuseperator655 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
   End
End
Attribute VB_Name = "frmCLEEventOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0414 (jjm)

'
' CLEEvent Open
'

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
  
Private mCLEEventHandle As String

Public FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView

Private mData As DataProvider
' Detail Tabs
Private mCLEEventLawyer As Dataset



'
' FormInterface Interface
'

'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimSave
      If fInterface.Key = DatabaseName.CLEEVENT_TABLE Then
        mCLEEventHandle = fInterface.Child.CLEEventHandle
      End If
  End Select
  
  Refresh
  FormRefresh
  Show
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Dim Datafields As Collection
  Set Datafields = mData.Find(DatabaseName.CLEEVENT_KEY, mCLEEventHandle)
  SetFields lbldatafield, Datafields
  
  Caption = "CLE Event (" & mCLEEventHandle & ")"
  
  'Open applicationLawyer Dataset
  mCLEEventLawyer.DataProvider.Filter = DatabaseName.CLEEVENT_KEY & " = '" & mCLEEventHandle & "'"

  ' Standard Navigation Update.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  SSTabRefresh
  Refresh
End Sub

'
' Set ListIndex from underlying view.  How we pull the field is
' dependent upon the list item type.  In this example, if we are
' opening from an event view, there is a different method for
' pulling the appliction id.
'
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  mCLEEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.CLEEVENT_KEY)
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
  Case 0
    Set ListViewDataProvider = mCLEEventLawyer.DataProvider
  End Select
End Function

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = "CLEEvent")
  If View.ListItems.Count = 0 Then
    MsgBox "There are no CLE events to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(CLEEventId As String, Optional ShowTab As Integer = 0)
  mCLEEventHandle = CLEEventId
  mTab = ShowTab
  Show
End Sub

Public Sub SSTabRefresh()
  Dim Source As DataProvider, View As ListView, Index As Variant
  Set View = lvView(mTab)
  Set Source = ListViewDataProvider(mTab)
  
  With View
    'Ensure first tab is open.
    If SSTab.Tab = mTab Then
      If .ListItems.Count > 0 Then
        Index = IIf(.SelectedItem.Key = "", .SelectedItem.Index, .SelectedItem.Key)
        .Visible = False
        .SelectedItem.Selected = False
        ListViewLoad View, Source
        On Error Resume Next
        .ListItems(Index).Selected = True
        .Visible = True
        .SetFocus
      Else
        ListViewLoad View, Source
      End If
    Else
      SSTab.Tab = mTab
    End If
  
    sbStatusBar.Panels(1).Text = str(.ListItems.Count) + " object(s)"
    sbStatusBar.Panels(2).Text = Source.Source
  
    ' Custom extension for calculating law area total.
    If mTab = 1 Then
      Dim Total As Integer, item As Variant
      For Each item In .ListItems
        Total = item.ListSubItems("lawAreaPercent") + Total
      Next
      sbStatusBar.Panels(1).Text = sbStatusBar.Panels(1).Text & " - " & Total & "%"
    End If
  End With
End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
End Sub
'
' End General Methods
'
'
'
'
' Begin Controls
'

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Initialize()
  'initialize the CLE Data
  Set mData = LawSession.NewDataProvider(DatabaseName.CLEEVENT_TABLE)
  
  'Initialize the CLEEventLawyer Dataset
  Set mCLEEventLawyer = NewGenericDataset(DatabaseName.CLEEVENTLAWYER_VIEW)
 ' Set mCLEEventLawyer.Session = LawSession
   
End Sub

'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyL
        mnuFileNewLawyer_Click
    End Select
    Exit Sub
  End If
  
  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      Select Case SSTab.Tab
        Case 0:   'Lawyer
          mnuFileNewLawyer_Click
      End Select
    Case vbKeyF1
      FormUtil.ShowHelp , Me.hWnd
  End Select
End Sub


Private Sub Form_Load()
  'Load to from to previous position
  FormUtil.DeserializePosition Me
  
  ' Setup Toolbar.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.Height + 24

  FormUtil.EditLoadImages Me
  FormUtil.MenuLoad Me
  
  ' Custom Menu Shortcut Adjustments
  mnuFileNewLawyer.Caption = mnuFileNewLawyer.Caption + Chr(9) + "Ctrl+Shift+L"
    
'  mnuFileNewQuestion.Caption = "&Question" + Chr(9) + "Ctrl+Shift+Q"
  mnuEditEditRow.Caption = mnuEditEditRow.Caption + Chr(9) + "Enter"
    
  ' Setup Form Manager
  FormMgr.Load Me
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub

' Expand body
Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
  fraCLEEvent.Width = SSTab.Width
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)
  mnuEdit_Click 1
End Sub

Private Sub Frame4_DblClick()
  mnuEdit_Click 0
End Sub

Private Sub fraNote_DblClick()
  If mnuToolsNotepad.Enabled Then
    mnuToolsNotepad_Click
  Else
    MsgBox "Please add initial note using the Edit, Firm menu."
  End If
End Sub


Private Sub lblLaywerCount_Click()

End Sub

Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0 'CLE Event
      FormUtil.ShowByPrimaryKey New frmCLEEventEdit, mCLEEventHandle, FormMgr, , DatabaseName.CLEEVENT_TABLE
      
    Case 1 'Lawyer
      Dim fCLELawyerEdit As New frmCLEEventLawyerEdit
      FormMgr.Add fCLELawyerEdit ', DatabaseName.CLEEVENTLAWYER_TABLE & "Edit"
      fCLELawyerEdit.ShowByListView lvView(0)
  End Select
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If
  
  If lvView(SSTab.Tab).ListItems.Count = 0 Then
    MsgBox "There are no items to delete."
    Exit Sub
  End If
  
  Select Case SSTab.Tab
    Case 0: 'Lawyer
      With lvView(0)
        Call Factory.CLEEventLawyerService.DeleteCLEEventLawyer( _
            ListItemText(.SelectedItem, DatabaseName.CLEEVENT_KEY), ListItemText(.SelectedItem, DatabaseName.LAWYER_KEY), _
            vbNullStringArray, vbNullStringArray)
      End With
  End Select
  FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case 0:
      mnuEdit_Click 1
  End Select
End Sub

Private Sub mnuEditMove_Click()
'  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileSendToFolder_Click()
  'FormUtil.SendToFolder DatabaseName.APPLICATION_REF, DatabaseName.APPLICATION_TABLE, mCLEEventHandle, mCLEEventHandle
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
'  FormUtil.ShowEventDialog DatabaseName.APPLICATION_TABLE, mCLEEventHandle, Index
End Sub

Private Sub mnuFileNew_Click()
  FormUtil.ShowNew New frmCLEEventEdit, FormMgr, mCLEEventHandle
End Sub

'
' Edit/Insert Routines
'
Private Sub mnuFileNewLawyer_Click()
  Dim fCLELawyerEdit As New frmCLEEventLawyerEdit
  FormMgr.Add fCLELawyerEdit
  fCLELawyerEdit.ShowNew mCLEEventHandle
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileProperties_Click()
'  If Not FormUtil.ShowEventProperties(mCLEEventHandle) Then
'    FormUtil.ShowProperties DatabaseName.CLEEVENT_TABLE, mCLEEventHandle
'  End If
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub


Private Sub mnuToolsAdvancedFind_Click()
 ' Dispatcher.ShowSearch DatabaseName.APPLICATION_TABLE, FormMgr
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNewLawyer_Click
End Sub

Private Sub mnuToolsNotepad_Click()
  'FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  'FolderUtil.WorkflowShow DatabaseName.APPLICATION_TABLE, lawActionNew
End Sub

'
' Standard choose columns for the current tab's list view.
'
Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'
' Standard view next menu
'
Private Sub mnuViewNext_Click()
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

'
' Standard view previous menu
'
Private Sub mnuViewPrevious_Click()
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

'
' Standard view refresh menu
'
Public Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

'
' Standard view a specific tab
'
Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

'
' Standard save settings
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

'
' Standard view toolbar.
'
Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  SSTabResize
End Sub


Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub

Private Sub picMenu_Resize()
  tbrInsert.Top = (picMenu.Height - tbrInsert.Height) / 2
  tbrEdit.Top = (picMenu.Height - tbrEdit.Height) / 2
  tbrBrowse.Top = (picMenu.Height - tbrBrowse.Height) / 2
End Sub

'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  mTab = SSTab.Tab
  SSTabRefresh

  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub




Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEdit_Click 0
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case SSTab.Caption
    Case "Lawyer"
      mnuFileNewLawyer_Click
  End Select

End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1 ' lawyer
      mnuFileNewLawyer_Click
  End Select
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuEdit_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1
      ' application browse code here
    Case 2

  End Select
End Sub
