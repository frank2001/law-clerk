VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EntityDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'ver 2.10.0421 (jjm)
'PersonDataset

Option Explicit
Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer

Private Keys As Collection
Private KeyIndex As Integer


Private Sub Class_Initialize()
  ListIndex = UrlName.ENTITYSEARCH_ENTITYNAME_INDEX
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.ENTITY_VIEW
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in entity identifier field only"
  frm.cboIn.AddItem "in entity locator field only"
  frm.cboIn.AddItem "in entity name field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, UrlName.ENTITYSEARCH_ENTITYNAME_INDEX)
End Sub

' Sets lvView.  Returns a collection of key fields.
Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New filter
  
  Words = Replace(Words, "'", "''") 'Translate to SQL
  
  If Not Words = "" Then
    Select Case ListIndex
      Case UrlName.ENTITYSEARCH_ENTITYID_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.ENTITY_KEY, Words)
      Case UrlName.ENTITYSEARCH_ENTITYLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ENTITYLOCATOR_FIELD, Words)
      Case UrlName.ENTITYSEARCH_ENTITYNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.ENTITYNAME_FIELD, Words)
    End Select
  End If

  mDataProvider.filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function
