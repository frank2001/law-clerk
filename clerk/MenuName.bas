Attribute VB_Name = "MenuName"
'ver 2.10.0409
Option Explicit

Public Const GLBATCH_OPEN = "mnuGLBatchOpen(1)"
Public Const GLBATCH_EXPORT = "mnuGLBatchOpen(2)"
Public Const APPLICATION_OPEN = "mnuApplicationOpen(1)"
Public Const APPLICATION_NEW_LAWYER = "mnuApplicationNew(1)"

Public Const APPLICATIONRATING_EDIT = "mnuApplicationRatingOpen(1)"
Public Const APPLICATIONRATING_BROWSEWORKSHEET = "mnuApplicationRatingOpen(2)"

Public Const CLAIM_OPEN = "mnuClaimOpen(1)"
Public Const CLAIMRESERVE_OPEN = "mnuClaimReserveOpen(1)"
Public Const CLAIMPAYMENT_OPEN = "mnuClaimPaymentOpen(1)"

Public Const DEDUCTIBLEREIMBURSE_OPEN = "mnuDeductibleReimburseOpen(1)"

Public Const FIRM_NEW_INSURER = "mnuFirmNew(2)"
Public Const FIRM_OPEN = "mnuFirmOpen(1)"
Public Const FIRM_DIARY = "mnuFirmOpen(2)"
Public Const FIRM_CONTACT = "mnuFirmOpen(3)"

Public Const INCIDENT_OPEN = "mnuIncidentOpen(1)"

Public Const LAWYER_OPEN = "mnuLawyerOpen(1)"

Public Const POLICY_OPEN_ACCOUNTING = "mnuPolicyOpen(2)"
Public Const POLICY_OPEN_ENDORSEMENT = "mnuPolicyOpen(3)"
Public Const POLICY_EXPLORE_PAYMENT = "mnuPolicyExplore(5)"

