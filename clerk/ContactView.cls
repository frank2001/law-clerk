VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ContactView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private mListIndex As Integer

Private Sub Class_Initialize()
  mListIndex = UrlName.CONTACTSEARCH_CONTACTTYPENAME_INDEX
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.CONTACT_VIEW
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property

Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear

  frm.cboIn.AddItem "in claim number field only"
  frm.cboIn.AddItem "in contact type identifier field only"
  frm.cboIn.AddItem "in contact type name field only"
  frm.cboIn.AddItem "in person entity name field only"
  frm.cboIn.AddItem "in incident number field only"
  frm.cboIn.AddItem "in location identifier field only"
  frm.cboIn.AddItem "in organization entity name field only"
  frm.cboIn.AddItem "in organization identifier field only"
  frm.cboIn.AddItem "in person identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, mListIndex, UrlName.CONTACTSEARCH_CONTACTTYPENAME_INDEX)
End Sub

Public Sub ContactViewSearchFilter(Main As Form, newFilter As Filter)
  If Main.chkFirmPolicyContact Then
    newFilter.Add DatabaseName.CONTACTTYPE_KEY & " = 'Firm' "
  End If
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  mListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case mListIndex
      Case UrlName.CONTACTSEARCH_CLAIMNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.CLAIMNUMBER_FIELD, Words)
      Case UrlName.CONTACTSEARCH_ORGANIZATIONENTITYNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.ORGANIZATIONENTITYNAME_FIELD, Words)
      Case UrlName.CONTACTSEARCH_CONTACTTYPEID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.CONTACTTYPE_KEY, Words)
      Case UrlName.CONTACTSEARCH_CONTACTTYPENAME_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.CONTACTTYPENAME_FIELD, Words)
      Case UrlName.CONTACTSEARCH_PERSONENTITYNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.PERSONENTITYNAME_FIELD, Words)
      Case UrlName.CONTACTSEARCH_INCIDENTNUMBER_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.INCIDENTNUMBER_FIELD, Words)
        'MsgBox "To Do: enable search"
        'newFilter.Add """" & DatabaseName.EVENT_KEY & """ in " & _
        '    "(select """ & DatabaseName.EVENT_KEY & """ " & _
        '    "from """ & DatabaseName.INCIDENT_TABLE & """ " & _
        '    "where " & LawDialect.StartsWithUpperCase(DatabaseName.INCIDENT_KEY) & " like '%" & UCase(Words) & "%')"
      Case UrlName.CONTACTSEARCH_LOCATIONID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LOCATION_KEY, Words)
      Case UrlName.CONTACTSEARCH_ORGANIZATIONID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ORGANIZATION_KEY, Words)
      Case UrlName.CONTACTSEARCH_PERSONID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.PERSON_KEY, Words)
    End Select
  End If
  
  If TypeOf frm Is frmGenericSearch Then
    ContactViewSearchFilter frm, newFilter
  End If
  
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function


