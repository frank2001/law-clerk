!define PRODUCT_NAME "Clerk"
!define PRODUCT_VERSION "2.73.00"
!define PRODUCT_PUBLISHER "frameworkPLUS"
!define PRODUCT_WEB_SITE "http://www.frameworkplus.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\law.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_STARTMENU_REGVAL "NSIS:StartMenuDir"

; MUI 1.67 compatible ------
!include "MUI.nsh"

;For Installing DLL and other library files
!include Library.nsh

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install-blue.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall-blue.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME

; Components page
!insertmacro MUI_PAGE_COMPONENTS

; Directory page
!insertmacro MUI_PAGE_DIRECTORY

; Start menu page
var ICONS_GROUP
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "frameworkPLUS"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${PRODUCT_STARTMENU_REGVAL}"
!insertmacro MUI_PAGE_STARTMENU Application $ICONS_GROUP

; Instfiles page
!insertmacro MUI_PAGE_INSTFILES

; Finish page
!define MUI_FINISHPAGE_RUN "$INSTDIR\law.exe"
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "clerk_${PRODUCT_VERSION}_setup.exe"
InstallDir "$PROGRAMFILES\frameworkPLUS\clerk"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails show

;Main section
Section "Main" MAIN
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
  File "build\bin\law.exe"
  File "build\bin\LAWSystemHelp.chm"

  SetOutPath "$INSTDIR\img"
  SetOverwrite try
  File "build\img\Back.bmp"
  File "build\img\BackDisabled.bmp"
  File "build\img\Down.bmp"
  File "build\img\DownDisabled.bmp"
  File "build\img\Forward.bmp"
  File "build\img\ForwardDisabled.bmp"
  File "build\img\LAWFolder.bmp"
  File "build\img\ListView.bmp"
  File "build\img\TVFolder.bmp"
  File "build\img\TVOpenFolder.bmp"
  File "build\img\Up.bmp"
  File "build\img\UpDisabled.bmp"
  File "build\img\UpOneLevel.bmp"
  File "build\img\UpOneLevelDisabled.bmp"
  
  ;Lib install
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\COMCAT.DLL"   "$SYSDIR\COMCAT.DLL"   "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\COMDLG32.OCX" "$SYSDIR\COMDLG32.OCX" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\craxdrt.dll"  "$SYSDIR\craxdrt.dll"  "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\crviewer.dll" "$SYSDIR\crviewer.dll" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\hh.exe"       "$SYSDIR\hh.exe"       "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\hhctrl.ocx"   "$SYSDIR\hhctrl.ocx"   "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\itircl.dll"   "$SYSDIR\itircl.dll"   "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\itss.dll"     "$SYSDIR\itss.dll"     "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\MSBIND.DLL"   "$SYSDIR\MSBIND.DLL"   "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\MSCOMCT2.OCX" "$SYSDIR\MSCOMCT2.OCX" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\Mscomctl.ocx" "$SYSDIR\Mscomctl.ocx" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\msmask32.ocx" "$SYSDIR\msmask32.ocx" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\MSSTDFMT.DLL" "$SYSDIR\MSSTDFMT.DLL" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\RICHTX32.OCX" "$SYSDIR\RICHTX32.OCX" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\TABCTL32.OCX" "$SYSDIR\TABCTL32.OCX" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED "build\lib\VB6STKIT.DLL" "$SYSDIR\VB6STKIT.DLL" "$SYSDIR"

  ;Standard VB6 Runtimes. This code was obtained fromt he NSIS documentation
  !insertmacro InstallLib REGDLL 1 REBOOT_NOTPROTECTED "build\lib\msvbvm60.dll" "$SYSDIR\msvbvm60.dll" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED    "build\lib\oleaut32.dll" "$SYSDIR\oleaut32.dll" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED    "build\lib\olepro32.dll" "$SYSDIR\olepro32.dll" "$SYSDIR"
  !insertmacro InstallLib REGDLL 1 REBOOT_PROTECTED    "build\lib\comcat.dll"   "$SYSDIR\comcat.dll"   "$SYSDIR"
  !insertmacro InstallLib DLL    1 REBOOT_PROTECTED    "build\lib\asycfilt.dll" "$SYSDIR\asycfilt.dll" "$SYSDIR"
  !insertmacro InstallLib TLB    1 REBOOT_PROTECTED    "build\lib\stdole2.tlb"  "$SYSDIR\stdole2.tlb"  "$SYSDIR"

  ; Shortcuts
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  CreateDirectory "$SMPROGRAMS\$ICONS_GROUP\Clerk"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Clerk\Clerk.lnk" "$INSTDIR\law.exe"
  CreateShortCut "$DESKTOP\Clerk.lnk" "$INSTDIR\law.exe"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Clerk\Help.lnk" "$INSTDIR\LAWSystemHelp.chm"
  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section "Config" CONFIG
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
  File "build\conf\LAW.ini"
  File "build\conf\form.ini"
  SetOutPath "$INSTDIR\personal"
  File "build\conf\personal\search.ini"
SectionEnd

Section "SOAP" SOAP
  SetOutPath "$INSTDIR\bin"
  File "build\bin\soapsdk.exe"
  ;Run the SOAP Toolkit
  ExecWait "build\bin\soapsdk.exe"
  ;Delete "$INSTDIR\bin"
SectionEnd

Section -AdditionalIcons
  SetOutPath $INSTDIR
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Uninstall.lnk" "$INSTDIR\uninst.exe"
  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\law.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\law.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd

; Section descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${MAIN} "Main system files needed on every \
                                              install or upgrade."
  !insertmacro MUI_DESCRIPTION_TEXT ${CONFIG} "Install configuration files. Selecting \
                                              this option will overwrite the exisiting \
                                              configuration."
  !insertmacro MUI_DESCRIPTION_TEXT ${SOAP} "This selection runs the MS SOAP 3.0 installer. \
                                              MS SOAP 3.0 needs to be installed for Clerk to \
                                              function."
!insertmacro MUI_FUNCTION_DESCRIPTION_END


Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "Clerk was successfully removed from your computer."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove Clerk and all of its components?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
  !insertmacro MUI_STARTMENU_GETFOLDER "Application" $ICONS_GROUP
  Delete "$INSTDIR\uninst.exe"
  Delete "$INSTDIR\bin\soapsdk.exe"
  Delete "$INSTDIR\personal\search.ini"
  Delete "$INSTDIR\form.ini"
  Delete "$INSTDIR\LAW.ini"
  Delete "$INSTDIR\LAWSystemHelp.chm"
  Delete "$INSTDIR\img\UpOneLevelDisabled.bmp"
  Delete "$INSTDIR\img\UpOneLevel.bmp"
  Delete "$INSTDIR\img\UpDisabled.bmp"
  Delete "$INSTDIR\img\Up.bmp"
  Delete "$INSTDIR\img\TVOpenFolder.bmp"
  Delete "$INSTDIR\img\TVFolder.bmp"
  Delete "$INSTDIR\img\ListView.bmp"
  Delete "$INSTDIR\img\LAWFolder.bmp"
  Delete "$INSTDIR\img\ForwardDisabled.bmp"
  Delete "$INSTDIR\img\Forward.bmp"
  Delete "$INSTDIR\img\DownDisabled.bmp"
  Delete "$INSTDIR\img\Down.bmp"
  Delete "$INSTDIR\img\BackDisabled.bmp"
  Delete "$INSTDIR\img\Back.bmp"
  Delete "$INSTDIR\law.exe"

  Delete "$SMPROGRAMS\$ICONS_GROUP\Uninstall.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\Clerk\Help.lnk"
  Delete "$DESKTOP\Clerk.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\Clerk\Clerk.lnk"

  RMDir "$SMPROGRAMS\$ICONS_GROUP\Clerk"
  RMDir "$SMPROGRAMS\$ICONS_GROUP"
  RMDir "$INSTDIR\personal"
  RMDir "$INSTDIR\img"
  RMDir "$INSTDIR\bin"
  RMDir "$INSTDIR"

  !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\msvbvm60.dll"
  !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\oleaut32.dll"
  !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\olepro32.dll"
  !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\comcat.dll"
  !insertmacro UnInstallLib DLL    SHARED NOREMOVE "$SYSDIR\asycfilt.dll"
  !insertmacro UnInstallLib TLB    SHARED NOREMOVE "$SYSDIR\stdole2.tlb"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd