VERSION 5.00
Begin VB.Form frmEFTAccountingDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "EFT Accounting"
   ClientHeight    =   1860
   ClientLeft      =   30
   ClientTop       =   270
   ClientWidth     =   6960
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   1896
      Left            =   0
      ScaleHeight     =   1890
      ScaleWidth      =   6960
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   0
      Width           =   6960
      Begin VB.ComboBox cboEventType 
         Height          =   288
         ItemData        =   "frmEFTAccountingDialog.frx":0000
         Left            =   672
         List            =   "frmEFTAccountingDialog.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Tag             =   "EFTAccountingDialogEventType"
         Top             =   516
         Width           =   2688
      End
      Begin VB.CommandButton cmdFolder 
         Caption         =   "&Folder"
         Height          =   336
         Left            =   96
         TabIndex        =   3
         Top             =   984
         Width           =   1212
      End
      Begin VB.ComboBox cboYear 
         Height          =   288
         Left            =   2052
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   180
         Width           =   1296
      End
      Begin VB.ComboBox cboMonth 
         Height          =   288
         ItemData        =   "frmEFTAccountingDialog.frx":0004
         Left            =   672
         List            =   "frmEFTAccountingDialog.frx":002C
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   180
         Width           =   1332
      End
      Begin VB.CommandButton CancelButton 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   5628
         TabIndex        =   5
         Top             =   564
         Width           =   1200
      End
      Begin VB.CommandButton OKButton 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   5628
         TabIndex        =   4
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   192
         Left            =   96
         TabIndex        =   9
         Top             =   564
         Width           =   420
      End
      Begin VB.Label lblFolder 
         Height          =   192
         Left            =   120
         TabIndex        =   8
         Top             =   1476
         Width           =   6876
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Date:"
         Height          =   192
         Left            =   96
         TabIndex        =   7
         Top             =   228
         Width           =   384
      End
   End
End
Attribute VB_Name = "frmEFTAccountingDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 1.90.1220

Option Explicit

Public mOk As Boolean
Private mEventTypes As Collection
Public Function GetMonthDiget() As Integer
  GetMonthDiget = cboMonth.ListIndex + 1
End Function

Public Function GetEventType() As String
  Dim EventTypeId As String
  Dim I As Integer
  For I = 1 To mEventTypes.Count
    If mEventTypes(I)("eventTypeName") = cboEventType.Text Then
      EventTypeId = mEventTypes(I)(DatabaseName.EVENTTYPE_KEY)
      Exit For
    End If
  Next I
  
  GetEventType = EventTypeId
End Function

Private Sub CancelButton_Click()
  Hide
End Sub

Private Sub cmdFolder_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowSelect(, lblFolder.Caption) Then
    lblFolder.Caption = fForm.FolderPath
  End If
  Unload fForm
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , Me.hWnd
  End If
End Sub

Private Sub Form_Load()
  Dim ThisYear As Integer
  ThisYear = GetInteger(Format(Today, "yyyy"))
  
  Dim n As Integer
  For n = ThisYear - 1 To ThisYear + 1
    cboYear.AddItem n
  Next n
  
  Dim I As Integer
  For I = 1 To 12
    cboMonth.AddItem MonthName(I), I - 1
  Next I
  'cboYear.ListIndex = GetInteger(Year(DateAdd("m", 1, Today))) - ThisYear + 1
  cboYear.ListIndex = GetInteger(Year(DateAdd("m", 1, Today))) - ThisYear + 1
  cboMonth.ListIndex = GetInteger(Month(DateAdd("m", 1, Today))) - 1
  
  Set mEventTypes = Factory.EventTypeService.GroupCollection(DatabaseName.POLICYACCOUNTINGEFT_RULE)
  FormUtil.ComboBoxLoad mEventTypes, cboEventType, "eventTypeName"
  FormUtil.GetComboDefault cboEventType
  'cboEventType.ListIndex = 0
  
  lblFolder.Caption = IIf(IniMain.ReadFile(UrlName.MISC_INI, "EFTAccountingFolder") = vbNullString, vbNullString, IniMain.ReadFile(UrlName.MISC_INI, "EFTAccountingFolder"))
End Sub

Private Sub OKButton_Click()

  If lblFolder.Caption = vbNullString Then
    MsgBox "You must select a folder to continue."
    Exit Sub
  End If
    
  'Write the folder name to the ini
  IniMain.WriteFile UrlName.MISC_INI, "EFTAccountingFolder", lblFolder.Caption
  FormUtil.SetComboDefault cboEventType

  mOk = True
  Hide
End Sub

