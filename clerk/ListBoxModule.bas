Attribute VB_Name = "ListBoxModule"
Option Explicit

Private Const LB_SETTABSTOPS = &H192


Public Sub SetTabStops(hWnd As Long, TabStops() As Long)
  Call SendMessage(hWnd, LB_SETTABSTOPS, 0&, ByVal 0&)
  Call SendMessage(hWnd, LB_SETTABSTOPS, UBound(TabStops), TabStops(0))
End Sub
