VERSION 5.00
Begin VB.Form frmPopup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   2565
   ClientLeft      =   1380
   ClientTop       =   2490
   ClientWidth     =   6360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2565
   ScaleWidth      =   6360
   Begin VB.Menu mnuMisc 
      Caption         =   "Misc"
      Begin VB.Menu mnuPopupFolderList 
         Caption         =   "Folder"
         Begin VB.Menu mnuFolderNew 
            Caption         =   "&New Folder"
         End
         Begin VB.Menu mnuSeparator9732 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFolderOrganize 
            Caption         =   "Or&ganize..."
         End
         Begin VB.Menu mnuSeparator833 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFolderListDelete 
            Caption         =   "&Delete"
         End
         Begin VB.Menu mnuFolderListRename 
            Caption         =   "Rena&me"
         End
         Begin VB.Menu mnuSeparator8344 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFolderListProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuPopupBrowser 
         Caption         =   "Browser"
         Begin VB.Menu mnuBrowserOpen 
            Caption         =   "&Open"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuBrowserEdit 
            Caption         =   "&Edit"
         End
         Begin VB.Menu mnuSeparator756 
            Caption         =   "-"
         End
         Begin VB.Menu mnuBrowserProperties 
            Caption         =   "P&roperties"
            Enabled         =   0   'False
         End
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Entity"
      Begin VB.Menu mnuPopupOrganization 
         Caption         =   "Organization"
         Begin VB.Menu mnuOrganizationOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuOrganizationOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuOrganizationOpen 
            Caption         =   "&Browse"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuOrganizationSeperator1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuOrganizationNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuOrganizationNew 
               Caption         =   "Loca&tion"
               Index           =   1
            End
            Begin VB.Menu mnuOrganizationNew 
               Caption         =   "&Person"
               Index           =   2
            End
         End
         Begin VB.Menu mnuOrganizationExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuOrganizationExplore 
               Caption         =   "Loca&tion"
               Index           =   1
            End
            Begin VB.Menu mnuOrganizationExplore 
               Caption         =   "&Person"
               Index           =   2
            End
         End
         Begin VB.Menu mnuOrganizationSeperator2 
            Caption         =   "-"
         End
         Begin VB.Menu mnuOrganizationSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuOrganizationSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuOrganizationProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuPopupFirm 
         Caption         =   "Firm"
         Begin VB.Menu mnuFirmOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuFirmOpen 
            Caption         =   "Open With D&iary"
            Index           =   2
         End
         Begin VB.Menu mnuFirmOpen 
            Caption         =   "&Edit"
            Index           =   3
         End
         Begin VB.Menu mnuFirmOpen 
            Caption         =   "&Browse"
            Index           =   4
         End
         Begin VB.Menu mnuFirmSep 
            Caption         =   "-"
            Index           =   0
         End
         Begin VB.Menu mnuFirmNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuFirmNew 
               Caption         =   "&Application"
               Index           =   1
            End
            Begin VB.Menu mnuFirmNew 
               Caption         =   "&Insurer"
               Index           =   2
            End
            Begin VB.Menu mnuFirmNew 
               Caption         =   "&Lawyer"
               Index           =   3
            End
         End
         Begin VB.Menu mnuFirmExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "&Application"
               Index           =   1
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "&Claim"
               Index           =   2
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "&Contact"
               Index           =   3
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "Co&verage"
               Index           =   4
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "&Insurer"
               Index           =   5
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "&Lawyer"
               Index           =   6
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "Loca&tion"
               Index           =   7
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "&Policy"
               Index           =   8
            End
            Begin VB.Menu mnuFirmExplore 
               Caption         =   "&Rating"
               Index           =   9
            End
         End
         Begin VB.Menu mnuSeparator3431 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFirmSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuFirmSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuFirmProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuApplication 
         Caption         =   "Application"
         Begin VB.Menu mnuApplicationOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuApplicationOpen 
            Caption         =   "Open &Firm"
            Index           =   2
         End
         Begin VB.Menu mnuApplicationOpen 
            Caption         =   "&Edit"
            Index           =   3
         End
         Begin VB.Menu mnuApplicationOpen 
            Caption         =   "&Browse"
            Enabled         =   0   'False
            Index           =   4
         End
         Begin VB.Menu mnuSeparator392 
            Caption         =   "-"
            Index           =   5
         End
         Begin VB.Menu mnuApplicationNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuApplicationNew 
               Caption         =   "Application &Lawyer"
               Index           =   1
            End
            Begin VB.Menu mnuApplicationNew 
               Caption         =   "Application Law &Area"
               Index           =   2
            End
            Begin VB.Menu mnuApplicationNew 
               Caption         =   "Application Li&mit"
               Index           =   3
            End
            Begin VB.Menu mnuApplicationNew 
               Caption         =   "Application &Question"
               Index           =   4
            End
         End
         Begin VB.Menu mnuApplicationExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuApplicationExplore 
               Caption         =   "Co&verage"
               Index           =   1
            End
            Begin VB.Menu mnuApplicationExplore 
               Caption         =   "&Firm"
               Index           =   2
            End
            Begin VB.Menu mnuApplicationExplore 
               Caption         =   "&Lawyer"
               Index           =   3
            End
            Begin VB.Menu mnuApplicationExplore 
               Caption         =   "Law &Area"
               Index           =   4
            End
            Begin VB.Menu mnuApplicationExplore 
               Caption         =   "Li&mit"
               Index           =   5
            End
            Begin VB.Menu mnuApplicationExplore 
               Caption         =   "&Question"
               Index           =   6
            End
            Begin VB.Menu mnuApplicationExplore 
               Caption         =   "&Rating"
               Index           =   7
            End
         End
         Begin VB.Menu mnuSeparator827 
            Caption         =   "-"
         End
         Begin VB.Menu mnuApplicationSendTo 
            Caption         =   "Sen&d To "
            Begin VB.Menu mnuApplicationSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuApplicationProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuPopupClaim 
         Caption         =   "Claim"
         Begin VB.Menu mnuClaimOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuClaimOpen 
            Caption         =   "Open &Firm"
            Index           =   2
         End
         Begin VB.Menu mnuClaimOpen 
            Caption         =   "Open P&olicy"
            Index           =   3
         End
         Begin VB.Menu mnuClaimOpen 
            Caption         =   "&Edit"
            Index           =   4
         End
         Begin VB.Menu mnuClaimOpen 
            Caption         =   "&Browse"
            Index           =   5
         End
         Begin VB.Menu mnuSeparator937 
            Caption         =   "-"
            Index           =   6
         End
         Begin VB.Menu mnuClaimNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuClaimNew 
               Caption         =   "&Deductible Reimburse"
               Index           =   1
            End
            Begin VB.Menu mnuClaimNew 
               Caption         =   "&Payment"
               Index           =   2
            End
            Begin VB.Menu mnuClaimNew 
               Caption         =   "&Reserve"
               Index           =   3
            End
         End
         Begin VB.Menu mnuClaimExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "&Contact"
               Index           =   1
            End
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "Co&verage"
               Index           =   2
            End
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "&Deductible"
               Index           =   3
            End
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "Law &Area"
               Index           =   4
            End
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "Law &Error"
               Index           =   5
            End
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "&Laywer"
               Index           =   6
            End
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "&Payment"
               Index           =   7
            End
            Begin VB.Menu mnuClaimExplore 
               Caption         =   "&Reserve"
               Index           =   8
            End
         End
         Begin VB.Menu mnuSeperator311 
            Caption         =   "-"
         End
         Begin VB.Menu mnuClaimSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuClaimSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuClaimProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuPopupInstance 
         Caption         =   "Instance"
         Begin VB.Menu mnuInstanceOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuInstanceOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator776 
            Caption         =   "-"
         End
         Begin VB.Menu mnuInstanceSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuInstanceSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuInstanceProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuPopupLawyer 
         Caption         =   "Lawyer"
         Begin VB.Menu mnuLawyerOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuLawyerOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuLawyerOpen 
            Caption         =   "&Browse"
            Index           =   3
         End
         Begin VB.Menu mnuSeparator3321 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLawyerNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuLawyerNew 
               Caption         =   "&Lawyer"
               Enabled         =   0   'False
               Index           =   1
            End
         End
         Begin VB.Menu mnuLawyerExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuLawyerExplore 
               Caption         =   "&Application"
               Index           =   1
            End
            Begin VB.Menu mnuLawyerExplore 
               Caption         =   "&CLEEvent"
               Index           =   2
            End
            Begin VB.Menu mnuLawyerExplore 
               Caption         =   "C&laim"
               Index           =   3
            End
            Begin VB.Menu mnuLawyerExplore 
               Caption         =   "Co&ntact"
               Index           =   4
            End
            Begin VB.Menu mnuLawyerExplore 
               Caption         =   "&Firm"
               Index           =   5
            End
            Begin VB.Menu mnuLawyerExplore 
               Caption         =   "P&erson"
               Index           =   6
            End
            Begin VB.Menu mnuLawyerExplore 
               Caption         =   "&Policy"
               Index           =   7
            End
         End
         Begin VB.Menu mnuSeperator309 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLawyerSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuLawyerSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuLawyerProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuCLEEvent 
         Caption         =   "CLEEvent"
         Begin VB.Menu mnuCLEOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuCLEOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuCLEOpen 
            Caption         =   "&Browse"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuSeparator336 
            Caption         =   "-"
         End
         Begin VB.Menu mnuCLENewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuCLENew 
               Caption         =   "&CLE Event Lawyer"
               Index           =   1
            End
         End
         Begin VB.Menu mnuCLEEventExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuCLEEventExplore 
               Caption         =   "&Lawyer"
               Index           =   1
            End
         End
         Begin VB.Menu mnuSeperator711 
            Caption         =   "-"
         End
         Begin VB.Menu mnuCLEEventSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuCLEEventSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuCLEEventProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuPolicy 
         Caption         =   "Policy"
         Begin VB.Menu mnuPolicyOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuPolicyOpen 
            Caption         =   "Open With &Accounting"
            Index           =   2
         End
         Begin VB.Menu mnuPolicyOpen 
            Caption         =   "Open With En&dorsement"
            Index           =   3
         End
         Begin VB.Menu mnuPolicyOpen 
            Caption         =   "Open &Firm"
            Index           =   4
         End
         Begin VB.Menu mnuPolicyOpen 
            Caption         =   "&Edit"
            Index           =   5
         End
         Begin VB.Menu mnuPolicyOpen 
            Caption         =   "&Browse"
            Index           =   6
         End
         Begin VB.Menu mnuSeperator669 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPolicyNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuPolicyNew 
               Caption         =   "&Policy"
               Index           =   1
            End
         End
         Begin VB.Menu mnuPolicyExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuPolicyExplore 
               Caption         =   "&Accounting"
               Index           =   1
            End
            Begin VB.Menu mnuPolicyExplore 
               Caption         =   "&Claim"
               Index           =   2
            End
            Begin VB.Menu mnuPolicyExplore 
               Caption         =   "Co&verage"
               Index           =   3
            End
            Begin VB.Menu mnuPolicyExplore 
               Caption         =   "&Endorsement"
               Index           =   4
            End
            Begin VB.Menu mnuPolicyExplore 
               Caption         =   "&Firm"
               Index           =   5
            End
            Begin VB.Menu mnuPolicyExplore 
               Caption         =   "&Installment"
               Index           =   6
            End
            Begin VB.Menu mnuPolicyExplore 
               Caption         =   "&Lawyer"
               Index           =   7
            End
         End
         Begin VB.Menu mnuSeperator654 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPolicySendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuPolicySendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuPolicyProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuRating 
         Caption         =   "Rating"
         Begin VB.Menu mnuApplicationRatingOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuApplicationRatingOpen 
            Caption         =   "Browse &Worksheet"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator363 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuRatingNew 
               Caption         =   "&Rating"
               Index           =   1
            End
         End
         Begin VB.Menu mnuRatingExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuApplicationRatingExplore 
               Caption         =   "A&pplication"
               Index           =   1
            End
            Begin VB.Menu mnuApplicationRatingExplore 
               Caption         =   "&Base"
               Index           =   2
            End
            Begin VB.Menu mnuApplicationRatingExplore 
               Caption         =   "Co&verage"
               Index           =   3
            End
            Begin VB.Menu mnuApplicationRatingExplore 
               Caption         =   "&Firm"
               Index           =   4
            End
            Begin VB.Menu mnuApplicationRatingExplore 
               Caption         =   "L&awyer"
               Index           =   5
            End
            Begin VB.Menu mnuApplicationRatingExplore 
               Caption         =   "&Limit"
               Index           =   6
            End
         End
         Begin VB.Menu Seperator998 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuRatingSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuRatingProperties 
            Caption         =   "P&roperties"
         End
      End
   End
   Begin VB.Menu mnuDetail 
      Caption         =   "Detail"
      Begin VB.Menu mnuClaimLawArea 
         Caption         =   "ClaimLawArea"
         Begin VB.Menu mnuClaimLawAreaOpen 
            Caption         =   "&Open Claim"
            Index           =   1
         End
      End
      Begin VB.Menu mnuClaimLawError 
         Caption         =   "ClaimLawError"
         Begin VB.Menu mnuClaimLawErrorOpen 
            Caption         =   "&Open Claim"
            Index           =   1
         End
      End
      Begin VB.Menu mnuClaimLawyer 
         Caption         =   "ClaimLawyer"
         Begin VB.Menu mnuClaimLawyerOpen 
            Caption         =   "&Open Claim"
            Index           =   1
         End
         Begin VB.Menu mnuClaimLawyerOpen 
            Caption         =   "Open &Lawyer"
            Index           =   2
         End
      End
      Begin VB.Menu mnuClaimPayment 
         Caption         =   "ClaimPayment"
         Begin VB.Menu mnuClaimPaymentOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuClaimPaymentOpen 
            Caption         =   "Open &Claim"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator9523 
            Caption         =   "-"
         End
         Begin VB.Menu mnuClaimPaymentProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuClaimReserve 
         Caption         =   "ClaimReserve"
         Begin VB.Menu mnuClaimReserveOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuClaimReserveOpen 
            Caption         =   "Open C&laim"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator822 
            Caption         =   "-"
         End
         Begin VB.Menu mnuClaimReserveProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuDeductibleReimburse 
         Caption         =   "DeductibleReimburse"
         Begin VB.Menu mnuDeductibleReimburseOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuDeductibleReimburseOpen 
            Caption         =   "Open &Claim"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator3822 
            Caption         =   "-"
         End
         Begin VB.Menu mnuDeductibleReimburseProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuClaimEndorsement 
         Caption         =   "ClaimEndorsement"
         Begin VB.Menu mnuClaimEndorsementOpen 
            Caption         =   "&Open Claim"
            Index           =   1
         End
         Begin VB.Menu mnuClaimEndorsementOpen 
            Caption         =   "Open &Endorsement"
            Index           =   2
         End
      End
      Begin VB.Menu mnuApplicationLawArea 
         Caption         =   "ApplicationLawArea"
         Begin VB.Menu mnuApplicationLawAreaOpen 
            Caption         =   "&Open Application"
            Index           =   1
         End
         Begin VB.Menu mnuSeparator1098 
            Caption         =   "-"
         End
         Begin VB.Menu mnuApplicationLawAreaProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuPopUpApplicationLawyer 
         Caption         =   "ApplicationLawyer"
         Begin VB.Menu mnuApplicationLawyerOpen 
            Caption         =   "&Open Application"
            Index           =   1
         End
         Begin VB.Menu mnuApplicationLawyerOpen 
            Caption         =   "Open &Lawyer"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator3826 
            Caption         =   "-"
         End
         Begin VB.Menu mnuApplicationLawyerProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuApplicationLimit 
         Caption         =   "ApplicationLimit"
         Begin VB.Menu mnuApplicationLimitOpen 
            Caption         =   "&Open Application"
            Index           =   1
         End
         Begin VB.Menu mnuSeparator28271 
            Caption         =   "-"
         End
         Begin VB.Menu mnuApplicationLimitProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuCLEEventLawyer 
         Caption         =   "CLEEventLawyer"
         Begin VB.Menu mnuCLEEventLawyerOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuCLEEventLawyerOpen 
            Caption         =   "Open &CLE Event"
            Index           =   2
         End
         Begin VB.Menu mnuCLEEventLawyerOpen 
            Caption         =   "Open &Lawyer"
            Index           =   3
         End
         Begin VB.Menu mnuSeperator 
            Caption         =   "-"
         End
         Begin VB.Menu mnuCLEEventLawyerProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuContact 
         Caption         =   "Contact"
         Begin VB.Menu mnuContactOpen 
            Caption         =   "&Open"
            Enabled         =   0   'False
            Index           =   1
         End
         Begin VB.Menu mnuContactOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuSeperator656 
            Caption         =   "-"
         End
         Begin VB.Menu mnuContactProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuLawyerFirm 
         Caption         =   "LawyerFirm"
         Begin VB.Menu mnuLawyerFirmOpenFirm 
            Caption         =   "&Open Firm"
         End
      End
      Begin VB.Menu mnuFirmInsurer 
         Caption         =   "FirmInsurer"
         Begin VB.Menu mnuFirmInsurerOpen 
            Caption         =   "&Open Firm"
            Index           =   1
         End
      End
      Begin VB.Menu mnuFirmLawyer 
         Caption         =   "FirmLawyer"
         Begin VB.Menu mnuFirmLawyerOpen 
            Caption         =   "&Open Firm"
            Index           =   1
         End
         Begin VB.Menu mnuFirmLawyerOpen 
            Caption         =   "Open &Lawyer"
            Index           =   2
         End
      End
      Begin VB.Menu mnuLocation 
         Caption         =   "Location"
         Begin VB.Menu mnuLocationOpen 
            Caption         =   "Open &Location"
            Index           =   1
         End
         Begin VB.Menu mnuLocationOpen 
            Caption         =   "Open &Firm"
            Index           =   2
         End
         Begin VB.Menu mnuLocationOpen 
            Caption         =   "&Edit"
            Index           =   3
         End
         Begin VB.Menu mnuSeparator18282 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLocationExploreMenu 
            Caption         =   "E&xplore"
            Begin VB.Menu mnuLocationExplore 
               Caption         =   "&Contact"
               Index           =   1
            End
         End
         Begin VB.Menu mnuSeparator9216 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLocationProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuEndorsement 
         Caption         =   "Endorsement"
         Begin VB.Menu mnuEndorsementOpen 
            Caption         =   "&Open Endorsement"
            Index           =   0
         End
         Begin VB.Menu mnuEndorsementOpen 
            Caption         =   "Open &Policy"
            Index           =   1
         End
         Begin VB.Menu mnuEndorsementOpen 
            Caption         =   "&Edit "
            Enabled         =   0   'False
            Index           =   2
         End
         Begin VB.Menu mnuEndorsementOpen 
            Caption         =   "&Browse"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuSeperator248 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEndorsementNewMenu 
            Caption         =   "&New"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuEndorsementExploreMenu 
            Caption         =   "Exp&lore"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuSeperator693 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEndorsementSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuEndorsementSendToFolder 
               Caption         =   "SendToFolder"
               Enabled         =   0   'False
            End
         End
         Begin VB.Menu mnuEndorsementProperties 
            Caption         =   "P&roperties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuPolicyInstallment 
         Caption         =   "Policy Installment"
         Begin VB.Menu mnuPolicyInstallmentOpenPolicy 
            Caption         =   "&Open Policy"
            Index           =   0
         End
      End
      Begin VB.Menu mnuPolicyLawyerCoverage 
         Caption         =   "Policy Lawyer Coverage"
         Begin VB.Menu mnuPolicyLawyerCoverageOpen 
            Caption         =   "&Open Policy"
            Index           =   1
         End
         Begin VB.Menu mnuPolicyLawyerCoverageOpen 
            Caption         =   "Open &Lawyer"
            Index           =   2
         End
         Begin VB.Menu mnuPolicyLawyerCoverageOpen 
            Caption         =   "&Edit Lawyer"
            Index           =   3
         End
      End
      Begin VB.Menu mnuPolicyAccounting 
         Caption         =   "Policy Accounting"
         Begin VB.Menu mnuPolicyAccountingOpen 
            Caption         =   "&Open Policy"
            Index           =   1
         End
         Begin VB.Menu mnuPolicyAccountingOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator382 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPolicyPaymentProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuGl 
         Caption         =   "G\L"
         Begin VB.Menu mnuGlOpen 
            Caption         =   "&Open"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuGlOpenFirm 
            Caption         =   "Open &Firm"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuGlEdit 
            Caption         =   "&Edit"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuGlBrowse 
            Caption         =   "&Browse"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuSeperator320 
            Caption         =   "-"
         End
         Begin VB.Menu mnuGlExplore 
            Caption         =   "Explore"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuSeperator321 
            Caption         =   "-"
         End
         Begin VB.Menu mnuGlSendTo 
            Caption         =   "Send &To"
            Enabled         =   0   'False
            Begin VB.Menu mnuGlSendToMyFolderDennis 
               Caption         =   "My Folder (Dennis)"
               Enabled         =   0   'False
            End
         End
         Begin VB.Menu mnuSeperator745 
            Caption         =   "-"
         End
         Begin VB.Menu mnuGlNew 
            Caption         =   "&New"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuIncident 
         Caption         =   "Incident"
         Begin VB.Menu mnuIncidentOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuIncidentOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuIncidentOpen 
            Caption         =   "&Browse"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuSeperator966 
            Caption         =   "-"
         End
         Begin VB.Menu mnuIncidentExplore 
            Caption         =   "Explore"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuSeperator965 
            Caption         =   "-"
         End
         Begin VB.Menu mnuIncidentSentTo 
            Caption         =   "Send &To"
            Enabled         =   0   'False
            Begin VB.Menu mnuIncidentSendToMyFolderDennis 
               Caption         =   "My Folder (Dennis)"
               Enabled         =   0   'False
            End
         End
         Begin VB.Menu mnuSeperator964 
            Caption         =   "-"
         End
         Begin VB.Menu mnuIncidentNew 
            Caption         =   "&New"
            Enabled         =   0   'False
         End
      End
   End
   Begin VB.Menu mnuHiddne 
      Caption         =   "Hidden"
      Begin VB.Menu mnuClaimOpenPayment 
         Caption         =   "Claim Open Payment"
      End
      Begin VB.Menu mnuClaimOpenDeductible 
         Caption         =   "Claim Open Deductible"
      End
   End
   Begin VB.Menu mnuListView 
      Caption         =   "ListView"
      Begin VB.Menu mnuPopupView 
         Caption         =   "&View"
         Begin VB.Menu mnuViewIcons 
            Caption         =   "Lar&ge Icons"
            Index           =   0
         End
         Begin VB.Menu mnuViewIcons 
            Caption         =   "S&mall Icons"
            Index           =   1
         End
         Begin VB.Menu mnuViewIcons 
            Caption         =   "&List"
            Index           =   2
         End
         Begin VB.Menu mnuViewIcons 
            Caption         =   "&Details"
            Index           =   3
         End
      End
      Begin VB.Menu mnuSeparator33 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewArrangeIcons 
         Caption         =   "Arrange &Icons"
         Begin VB.Menu mnuListViewArrangeIcon 
            Caption         =   "mnuListViewArrangeIcon"
            Index           =   0
         End
      End
      Begin VB.Menu mnuListViewRefresh 
         Caption         =   "R&efresh"
      End
      Begin VB.Menu mnuSeparator95 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuListViewPaste 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu mnuListViewPaste 
         Caption         =   "&Paste"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator592 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewNew 
         Caption         =   "Ne&w"
      End
      Begin VB.Menu mnuSeparator343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupProperties 
         Caption         =   "P&roperties"
      End
   End
End
Attribute VB_Name = "frmPopup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.50.00 (jjm) 2004 02 16
'ver 2.00.0311 (jjm)

Option Explicit

Private mParent As Form
Private mParentInterface As FormInterface

'
' FormInterface Interface
'

'
' Save Parent
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set mParentInterface = ParentInterface
  Set mParent = ParentInterface.Parent
End Sub

'
' When parent is unloaded, remove reference to parent so it can terminate.
'
Public Sub FormParentUnload()
  Set mParent = Nothing
End Sub
  

Public Function MenuToSource(Caption As String) As String
  MenuToSource = Replace(Replace(Caption, " ", ""), "&", "")
End Function


Public Sub ShowByListView(Form As Form)
  mParent.FormMgr.Add Form
  Form.ShowByListView mParent.lvView
End Sub


Public Sub ShowByPrimaryKey(Form As Form, Key As String)
  Form.ShowByPrimaryKey ListItemText(mParent.lvView.SelectedItem, Key)
End Sub

'
' End General
'
'
' Controls
'


Public Sub Class_Initialize()
  MsgBox "New frmPopup"
End Sub

Public Sub Form_Load()
'  MsgBox "Loaded"
End Sub

Public Sub Form_Unload(Cancel As Integer)
'  MsgBox "Unloaded"
End Sub


'
' Control Array for exploring by ApplicationId.
'
Public Sub mnuApplicationExplore_Click(Index As Integer)
  ' Set id before selecting folder as the list view will change.
  Dim Key As String
  Key = ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.COVERAGE_TABLE, , 0, Key
    Case 2
      mParent.FolderLoad DatabaseName.FIRM_TABLE, , 1, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY)
    Case 3
      mParent.FolderLoad DatabaseName.APPLICATIONLAWYER_TABLE, , 0, Key
    Case 4
      mParent.FolderLoad DatabaseName.APPLICATIONLAWAREA_TABLE, , 0, Key
    Case 5
      mParent.FolderLoad DatabaseName.APPLICATIONLIMIT_TABLE, , 0, Key
    Case 6
      mParent.FolderLoad DatabaseName.APPLICATIONQUESTION_TABLE, , 0, Key
    Case 7
      mParent.FolderLoad DatabaseName.QUOTE_TABLE, , UrlName.QUOTESEARCH_APPLICATIONID_INDEX, Key
  End Select
  
End Sub


Public Sub mnuApplicationLawAreaOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    FormUtil.ShowByPrimaryKey New frmApplicationOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY), mParent.FormMgr, 1
  Next item

End Sub

Public Sub mnuApplicationLawAreaProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.APPLICATIONLAWAREA_TABLE, _
      DatabaseName.LAWAREA_KEY, DatabaseName.APPLICATION_KEY, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWAREA_KEY), _
      ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
End Sub

Public Sub mnuApplicationLawyerOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByPrimaryKey New frmApplicationOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY), mParent.FormMgr
      Case 2
        FormUtil.ShowByPrimaryKey New frmLawyerOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY), mParent.FormMgr
    End Select
  Next item
End Sub

Public Sub mnuApplicationLawyerProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.APPLICATIONLAWYER_TABLE, _
    DatabaseName.APPLICATION_KEY, DatabaseName.LAWYER_KEY, ListItemText(mParent.lvView.SelectedItem, _
   DatabaseName.APPLICATION_KEY), ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
End Sub

Public Sub mnuApplicationLimitOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    FormUtil.ShowByPrimaryKey New frmApplicationOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY), mParent.FormMgr
  Next item
End Sub

Public Sub mnuApplicationLimitProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.APPLICATIONLIMIT_TABLE, _
      DatabaseName.APPLICATIONLIMIT_KEY, DatabaseName.APPLICATION_KEY, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATIONLIMIT_KEY), _
      ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
End Sub

Public Sub mnuApplicationNew_Click(Index As Integer)
  Dispatcher.ShowNew MenuToSource(mnuApplicationNew(Index).Caption), ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY), mParent.FormMgr
End Sub


' Open an application
'
Public Sub mnuApplicationOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByListView New frmApplicationOpen, mParent.FormMgr, mParent.lvView
      Case 2
        FormUtil.ShowByPrimaryKey New frmFirmOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY), mParent.FormMgr
      Case 3
        FormUtil.ShowByPrimaryKey New frmApplicationEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY), mParent.FormMgr
      Case 4 'Browse ToDo: add code
    End Select
  Next item
End Sub

Public Sub mnuApplicationProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.APPLICATION_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
  End If
End Sub

Public Sub mnuApplicationSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuBrowserEdit_Click()
  Dim table As String
  table = Replace(LCase(mParent.FolderKey), " ", "")
  Browse HttpServer & WebService & table & "edit?p-" & table & "=" + mParent.lvView.SelectedItem
End Sub




'
' Claim
'

Public Sub mnuClaimExplore_Click(Index As Integer)
  ' Must save claim id since setting folder name creates a new selected item.
  Dim ClaimNumber As String
  ClaimNumber = ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIMNUMBER_FIELD)
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.CONTACT_TABLE, , UrlName.CONTACTSEARCH_CLAIMNUMBER_INDEX, ClaimNumber
    Case 2
      mParent.FolderLoad DatabaseName.COVERAGE_TABLE, , UrlName.COVERAGESEARCH_POLICYID_INDEX, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY)
    Case 3
      mParent.FolderLoad DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, , , ClaimNumber
    Case 4
      mParent.FolderLoad UrlName.CLAIMLAWAREA_FOLDERKEY, , , ClaimNumber
    Case 5
      mParent.FolderLoad UrlName.CLAIMLAWERROR_FOLDERKEY, , , ClaimNumber
    Case 6
      mParent.FolderLoad UrlName.CLAIMLAWYER_FOLDERKEY, , , ClaimNumber
    Case 7
      mParent.FolderLoad DatabaseName.CLAIMPAYMENT_TABLE, , , ClaimNumber
    Case 8
      mParent.FolderLoad DatabaseName.CLAIMRESERVE_TABLE, , , ClaimNumber
  End Select
End Sub

Public Sub mnuClaimLawAreaOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 7
  Next item
End Sub

Public Sub mnuClaimLawErrorOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 8
  Next item
End Sub

Public Sub mnuClaimLawyerOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 6
      Case 2
        FormUtil.ShowByPrimaryKey New frmLawyerOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY), mParent.FormMgr
    End Select
  Next item
End Sub

Public Sub mnuClaimNew_Click(Index As Integer)
  Dim ClaimHandle As String
  ClaimHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY)
  Select Case Index
    Case 1
      FormUtil.ShowNew New frmDeductibleReimburseEdit, mParent.FormMgr, ClaimHandle
    Case 2
      FormUtil.ShowNew New frmClaimPaymentEdit, mParent.FormMgr, ClaimHandle
    Case 3
      Browse HttpServer & WebService & "claimreservenew?key=&key=" + ClaimHandle + "&action=create"
  End Select
End Sub

'
' Various open on a claim list view.
'
Public Sub mnuClaimOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Claim
        FormUtil.ShowByListView New frmClaimOpen, mParent.FormMgr, mParent.lvView
      Case 2 'Firm
        FormUtil.ShowByPrimaryKey New frmFirmOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY), mParent.FormMgr, 3
      Case 3 'Policy
        FormUtil.ShowByPrimaryKey New frmPolicyOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY), mParent.FormMgr
      Case 4 'edit
        FormUtil.ShowByPrimaryKey New frmClaimEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr
      Case 5
        Dispatcher.BrowseClaimOpen mParent.lvView.SelectedItem
    End Select
  Next item
End Sub


'
' Claim Payment
'

Public Sub mnuClaimPaymentOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByPrimaryKey New frmClaimPaymentEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIMPAYMENT_KEY), mParent.FormMgr
      Case 2
        FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 4
    End Select
  Next item
End Sub


Public Sub mnuClaimPaymentProperties_Click()
  FormUtil.ShowEventProperties ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)
End Sub

Public Sub mnuClaimProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.CLAIM_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY)
  End If
End Sub

'
' Claim Reserve
'

Public Sub mnuClaimReserveOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByPrimaryKey New frmClaimReserveEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIMRESERVE_KEY), mParent.FormMgr
      Case 2
        FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 3
    End Select
  Next item
End Sub


Public Sub mnuClaimReserveProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.CLAIMRESERVE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIMRESERVE_KEY)
  End If
End Sub

Public Sub mnuClaimSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub


Public Sub mnuCLEEventExplore_Click(Index As Integer)
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.CLEEVENTLAWYER_TABLE, , 0, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLEEVENT_KEY)
  End Select
End Sub

Public Sub mnuCLEEventLawyerOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'CLEEventLawyer Edit
        FormUtil.ShowByListView New frmCLEEventLawyerEdit, mParent.FormMgr, mParent.lvView
      Case 2 'Open CLE Event
        FormUtil.ShowByPrimaryKey New frmCLEEventOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLEEVENT_KEY), mParent.FormMgr
      Case 3 'Open Lawyer
        FormUtil.ShowByPrimaryKey New frmLawyerOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY), mParent.FormMgr
    End Select
  Next item
End Sub

Public Sub mnuCLEEventLawyerProperties_Click()
'   ******************** To Do - Fix Me - HACK ***************
'   ******************* See CLEEventProperties ***************
  Dim DataField As Collection
  Set DataField = Factory.CLEEventLawyerService.Find( _
     ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLEEVENT_KEY), _
     ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY))
     
  If DataField Is Nothing Then
    Exit Sub
  End If
  Dim fProps As New frmPropertiesEdit
  fProps.ShowProperties DatabaseName.CLEEVENTLAWYER_TABLE, DataField

End Sub

Public Sub mnuCLEEventProperties_Click()
'   If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, databasename.EVENT_KEY)) Then
'   Cant do due to string manipulation of the keys:
'   the problem is CLEEvent has too many capital letters in the begaining
'   this task is currently on hold
'    FormUtil.ShowProperties DatabaseName.CLEEVENT_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLEEVENT_KEY)
'   ******************** To Do - Fix Me - HACK ***************
  Dim DataField As Collection
  Set DataField = Factory.CLEEventService.Find(ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLEEVENT_KEY))
  If DataField Is Nothing Then
    Exit Sub
  End If
  
  Dim fProps As New frmPropertiesEdit
  fProps.ShowProperties DatabaseName.CLEEVENT_TABLE, DataField
  
End Sub

Public Sub mnuCLEEventSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuCLENew_Click(Index As Integer)
  Select Case Index
    Case 1 'New CLEEventLawyer
      FormUtil.ShowNew New frmCLEEventLawyerEdit, mParent.FormMgr, mParent.lvView, DatabaseName.CLEEVENTLAWYER_TABLE & "Edit"
  End Select
End Sub

Public Sub mnuCLEOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'open
        ShowByListView New frmCLEEventOpen
      Case 2 'Edit
        FormUtil.ShowByListView New frmCLEEventEdit, mParent.FormMgr, mParent.lvView
      Case 3 'Browse
    End Select
  Next item
End Sub

'Public Sub mnuClaimEndorsementOpen_Click()
' Does this exist???
'  Dim fClaim As New frmClaimOpen
'  mParent.FormMgr.Add fClaim
'  fClaim.ShowByPrimaryKey
'  FormUtil.ShowByPrimaryKey New frmCLEEventLawyerEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 1
'End Sub

'Public Sub mnuClaimEndorsementOpenEnd_Click()
'  ' Must save claim id since setting folder name creates a new selected Item.
'  Dim EndorsementId As String
'  EndorsementId = mParent.lvView.SelectedItem.ListSubItems(databasename.Endorsement_KEY)
'  ' Setup folder
'  mParent.FolderLoad "Endorsement"
'  mParent.txtWords = EndorsementId
'  mParent.cboIn.ListIndex = 0
'  mParent.SearchNow
'End Sub




Public Sub mnuClaimDeductibleOpenClaim_Click()
  FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 5
End Sub

Public Sub mnuClaimOpenDeductible_Click()
  FormUtil.ShowByListView New frmClaimOpen, mParent.FormMgr, mParent.lvView
End Sub


Public Sub mnuCLEEventLawyerOpenLawyer_Click()
  FormUtil.ShowByPrimaryKey New frmLawyerOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY), mParent.FormMgr
End Sub


Public Sub mnuContactOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Open
      
      Case 2 'Edit
        FormUtil.ShowByListView New frmContactEdit, mParent.FormMgr, mParent.lvView
        'FormUtil.ShowByPrimaryKey New frmContactEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CONTACT_KEY), mParent.FormMgr
    End Select
  Next item

End Sub

Public Sub mnuContactProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.CONTACT_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CONTACT_KEY)
  End If
End Sub

Public Sub mnuDeductibleReimburseOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByPrimaryKey New frmDeductibleReimburseEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.DEDUCTIBLEREIMBURSE_KEY), mParent.FormMgr
      Case 2
        FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr, 5
    End Select
  Next item
End Sub



Public Sub mnuFileNew_Click(Index As Integer)
  mParent.mnuFileNew_Click Index
End Sub



Public Sub mnuEndorsementOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 0 'Open Endorsement
        FormUtil.ShowByListView New frmEndorsementOpen, mParent.FormMgr, mParent.lvView
      Case 1 'Open Policy
        FormUtil.ShowByListView New frmPolicyOpen, mParent.FormMgr, mParent.lvView
      Case 2 'Edit
        'FormUtil.ShowByListView New frmEndorsementEdit, mParent.FormMgr, mParent.lvView
      Case 3 'Browse
    End Select
  Next item
End Sub

'
' Explore Children of Firm.
'
Public Sub mnuFirmExplore_Click(Index As Integer)
' Must save firm id since setting folder name creates a new selected Item.
  Dim firmHandle, organizationHandle As String
  firmHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY)
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.APPLICATION_TABLE, , 1, firmHandle
    Case 2
      mParent.FolderLoad DatabaseName.CLAIM_TABLE, , 4, firmHandle
    Case 3
      mParent.FolderLoad DatabaseName.CONTACT_TABLE, , UrlName.CONTACTSEARCH_ORGANIZATIONID_INDEX, organizationHandle
    Case 4
      mParent.FolderLoad DatabaseName.COVERAGE_TABLE, , 4, firmHandle
    Case 5
      mParent.FolderLoad DatabaseName.FIRMINSURER_TABLE, , 0, firmHandle
    Case 6
      mParent.FolderLoad DatabaseName.FIRMLAWYER_TABLE, , 0, firmHandle
    Case 7
      mParent.FolderLoad DatabaseName.LOCATION_TABLE, , 3, firmHandle
    Case 8
      mParent.FolderLoad DatabaseName.POLICY_TABLE, , 0, firmHandle
    Case 9
      mParent.FolderLoad DatabaseName.QUOTE_TABLE, , UrlName.QUOTESEARCH_FIRMID_INDEX, firmHandle
  End Select
End Sub

Public Sub mnuFirmInsurerOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    FormUtil.ShowByListView New frmFirmInsurerEdit, mParent.FormMgr, mParent.lvView
  Next item
End Sub

Public Sub mnuFirmLawyerOpen_Click(Index As Integer)
  Select Case Index
    Case 1
      FormUtil.ShowByListView New frmFirmLawyerEdit, mParent.FormMgr, mParent.lvView
    Case 2
      FormUtil.ShowByPrimaryKey New frmLawyerOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY), mParent.FormMgr
  End Select
End Sub

'
' New Firm
'
Public Sub mnuFirmNew_Click(Index As Integer)
  Dispatcher.ShowNew MenuToSource(mnuFirmNew(Index).Caption), ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY), mParent.FormMgr
End Sub


'
' Open firm.
'
Public Sub mnuFirmOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 ' Open
        FormUtil.ShowByListView New frmFirmOpen, mParent.FormMgr, mParent.lvView
      Case 2 ' Open With Diary
        FormUtil.ShowByListView New frmFirmDiaryOpen, mParent.FormMgr, mParent.lvView
      Case 3 ' Edit 'ver 2.50.00 (jjm) 2004 02 16
        FormUtil.ShowByListView New frmOrganizationEdit, mParent.FormMgr, mParent.lvView
      Case 4
        Dispatcher.BrowseFirmOpen mParent.lvView.SelectedItem.ListSubItems(DatabaseName.FIRM_KEY)
    End Select
  Next item
End Sub

Public Sub mnuFirmProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.FIRM_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY)
  End If
End Sub

Public Sub mnuFirmSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuFolderListDelete_Click()
  mParent.mnuFileDelete_Click
End Sub

Public Sub mnuFolderListRename_Click()
  If mParent.mnuFileRename.Enabled Then
    mParent.mnuFileRename_Click
  End If
End Sub

Public Sub mnuFolderNew_Click()
  mParent.mnuFileNewFolder_Click
End Sub

Public Sub mnuFolderOrganize_Click()
  mParent.mnuToolsOrganizeFolders_Click
End Sub

Public Sub mnuFolderWorkflowOptions_Click(Index As Integer)
  FolderUtil.WorkflowShow mParent.FolderKey, Index
End Sub

Public Sub mnuIncidentOpen_Click(Index As Integer)
  Select Case Index
    Case 1 'open
      FormUtil.ShowByListView New frmIncidentOpen, mParent.FormMgr, mParent.lvView
    Case 2 'edit
      FormUtil.ShowByPrimaryKey New frmIncidentEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.INCIDENT_KEY), mParent.FormMgr
    Case 3 'browse
  End Select
End Sub

Public Sub mnuInstanceOpen_Click(Index As Integer)
  'test for Claim or Incident
  Dim isClaim As Boolean
  isClaim = ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY) <> vbNullString
  
  'Open
  If Index = 1 And isClaim Then
    FormUtil.ShowByPrimaryKey New frmClaimOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr
    
  ElseIf Index = 1 And Not isClaim Then
    FormUtil.ShowByPrimaryKey New frmIncidentOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.INCIDENT_KEY), mParent.FormMgr
    
  'Edit
  ElseIf Index = 2 And isClaim Then
    FormUtil.ShowByPrimaryKey New frmClaimEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CLAIM_KEY), mParent.FormMgr
    
  ElseIf Index = 2 And Not isClaim Then
     FormUtil.ShowByPrimaryKey New frmIncidentEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.INCIDENT_KEY), mParent.FormMgr
  End If
  
End Sub

Private Sub mnuInstanceProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.INSTANCE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)
  End If
End Sub

Private Sub mnuInstanceSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuLawyerExplore_Click(Index As Integer)
  ' Set id before selecting folder as the list view will change.
  Dim firmHandle, PersonHandle, LawyerHandle As String
  firmHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
  PersonHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.PERSON_KEY)
  LawyerHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.APPLICATIONLAWYER_TABLE, , 1, firmHandle
    Case 2
      mParent.FolderLoad DatabaseName.CLEEVENTLAWYER_TABLE, , 2, LawyerHandle
    Case 3
      mParent.FolderLoad UrlName.CLAIMLAWYER_FOLDERKEY, , UrlName.INSTANCELAWYERSEARCH_LAWYERID_INDEX, LawyerHandle
    Case 4
      mParent.FolderLoad DatabaseName.CONTACT_TABLE, , UrlName.CONTACTSEARCH_PERSONID_INDEX, PersonHandle
    Case 5
      mParent.FolderLoad DatabaseName.FIRMLAWYER_TABLE, , 0, firmHandle
    Case 6
      mParent.FolderLoad DatabaseName.PERSON_TABLE, , 2, LawyerHandle
    Case 7
      ' Setup folder
      mParent.FolderLoad "PolicyLawyer", , 1, LawyerHandle
    
  End Select
End Sub

''Public Sub mnuLawyerFirmOpenFirm_Click()
''  FormUtil.ShowByPrimaryKey New frmFirmOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY), mParent.FormMgr
''End Sub

Public Sub mnuLawyerOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Open
        FormUtil.ShowByListView New frmLawyerOpen, mParent.FormMgr, mParent.lvView
     Case 2 'Edit
        Dim fPerson As New frmPersonEdit
        mParent.FormMgr.Add fPerson
        fPerson.ShowByListView mParent.lvView, True
        'ShowByListView New frmPersonEdit
     Case 3 'Browse
       Dispatcher.BrowseLawyerOpen mParent.lvView.SelectedItem.ListSubItems(DatabaseName.LAWYER_KEY)
     End Select
  Next item
End Sub

Public Sub mnuFolderListProperties_Click()
  mParent.mnuFileProperties_Click
End Sub

Public Sub mnuLawyerProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.LAWYER_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
  End If
End Sub

Public Sub mnuLawyerSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuListViewArrangeIcon_Click(Index As Integer)
  Call mParent.lvView_ColumnClick(mParent.lvView.ColumnHeaders(Index))
End Sub

Public Sub mnuListViewChooseColumns_Click()
  mParent.mnuViewChooseColumns_Click
End Sub

Public Sub mnuListViewNew_Click()
  mParent.mnuFileNewItem_Click
End Sub

Public Sub mnuListViewPaste_Click(Index As Integer)
  mParent.Popup2.mnuEventPaste_Click
End Sub

Public Sub mnuListViewRefresh_Click()
  mParent.mnuViewRefresh_Click
End Sub

Public Sub mnuLocationExplore_Click(Index As Integer)
  Dim LocationHandle As String
  LocationHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.CONTACT_TABLE, , UrlName.CONTACTSEARCH_LOCATIONID_INDEX, LocationHandle
  End Select
End Sub

Public Sub mnuLocationOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Open Location
        FormUtil.ShowByListView New frmLocationOpen, mParent.FormMgr, mParent.lvView
      Case 2 'Open Firm
        FormUtil.ShowByPrimaryKey New frmFirmOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.ENTITY_KEY), mParent.FormMgr, 2
      Case 3 'Edit
        FormUtil.ShowByListView New frmLocationEdit, mParent.FormMgr, mParent.lvView
     End Select
  Next item
End Sub

Public Sub mnuLocationProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.LOCATION_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
  End If
End Sub

Public Sub mnuOrganizationExplore_Click(Index As Integer)
' Must save firm id since setting folder name creates a new selected Item.
  Dim organizationHandle As String
  organizationHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.ORGANIZATION_KEY)
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.LOCATION_TABLE, , UrlName.LOCATIONSEARCH_ENTITYID_INDEX, organizationHandle
    Case 2
      mParent.FolderLoad DatabaseName.PERSON_TABLE, , UrlName.PERSONSEARCH_ORGANIZATIONID_INDEX, organizationHandle
  End Select
End Sub

Public Sub mnuOrganizationNew_Click(Index As Integer)
  Dispatcher.ShowNew MenuToSource(mnuFirmNew(Index).Caption), ListItemText(mParent.lvView.SelectedItem, DatabaseName.ORGANIZATION_KEY), mParent.FormMgr
End Sub

Public Sub mnuOrganizationOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 ' Open
        FormUtil.ShowByListView New frmOrganizationOpen, mParent.FormMgr, mParent.lvView
      Case 2 ' Edit
        FormUtil.ShowByListView New frmOrganizationEdit, mParent.FormMgr, mParent.lvView
    End Select
  Next item

End Sub

Public Sub mnuOrganizationProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.ORGANIZATION_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.ORGANIZATION_KEY)
  End If
End Sub

Public Sub mnuOrganizationSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuPolicyExplore_Click(Index As Integer)
  ' Must save firm id since setting folder name creates a new selected item.
  Dim PolicyHandle As String
  PolicyHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY)
  
  Select Case Index
    Case 1 'Accounting (Payment)
      mParent.FolderLoad DatabaseName.POLICYACCOUNTING_TABLE, , 2, PolicyHandle
    Case 2 'Claim
      mParent.FolderLoad DatabaseName.CLAIM_TABLE, , 6, PolicyHandle
    Case 3 'Coverage
      mParent.FolderLoad DatabaseName.COVERAGE_TABLE, , 6, PolicyHandle
    Case 4 'Endorsement
      mParent.FolderLoad DatabaseName.ENDORSEMENT_TABLE, , 2, PolicyHandle
    Case 5 'Firm
      mParent.FolderLoad DatabaseName.FIRM_TABLE, , 1, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY)
    Case 6 'Installment
      mParent.FolderLoad DatabaseName.INSTALLMENT_TABLE, , 2, PolicyHandle
    Case 7 'Lawyer
      mParent.FolderLoad DatabaseName.POLICYLAWYER_TABLE, , 4, PolicyHandle
  End Select
End Sub

Public Sub mnuPolicyInstallmentOpenPolicy_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 0
        FormUtil.ShowByPrimaryKey New frmPolicyOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY), mParent.FormMgr, 2
    End Select
  Next item

End Sub

Public Sub mnuPolicyLawyerCoverageOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByPrimaryKey New frmPolicyOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY), mParent.FormMgr
      Case 2
        FormUtil.ShowByPrimaryKey New frmLawyerOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY), mParent.FormMgr
      Case 3
        FormUtil.ShowByPrimaryKey New frmPersonEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWYER_KEY), mParent.FormMgr
    End Select
  Next item
End Sub

Public Sub mnuPolicyOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Open
        FormUtil.ShowByListView New frmPolicyOpen, mParent.FormMgr, mParent.lvView
      Case 2 'Open With Accounting
        Dim frmPolicy As New frmPolicyOpen
        frmPolicy.ShowTab = 1
        FormUtil.ShowByListView frmPolicy, mParent.FormMgr, mParent.lvView
        Exit For  'ToDo: FixMe
      Case 3 'Open with Endorsement
        Dim fPolicy As New frmPolicyOpen
        fPolicy.ShowTab = 3
        FormUtil.ShowByListView fPolicy, mParent.FormMgr, mParent.lvView
        Exit For 'ToDo: Fix Me
      Case 4 'Open Firm
        Me.ShowByListView New frmFirmOpen
      Case 5 'Edit
        FormUtil.ShowByPrimaryKey New frmPolicyEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY), mParent.FormMgr
      Case 6 'Browse
        Dispatcher.BrowsePolicyOpen ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY)
    End Select
  Next item
End Sub

Public Sub mnuPolicyOpenFirm_Click()
  FormUtil.ShowByPrimaryKey New frmFirmOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY), mParent.FormMgr
End Sub

Public Sub mnuPolicyAccountingOpen_Click(Index As Integer)
  Dim Selection As Collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1
        FormUtil.ShowByPrimaryKey New frmPolicyOpen, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY), mParent.FormMgr
      Case 2
        FormUtil.ShowByPrimaryKey New frmPolicyAccountingEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICYACCOUNTING_KEY), mParent.FormMgr
    End Select
  Next item
End Sub

Public Sub mnuPolicyProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.POLICY_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY)
  End If
End Sub

Public Sub mnuPolicySendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuPopupFolderList_Click()
  'MsgBox "Click"
End Sub

Public Sub mnuPopupProperties_Click()
  mParent.mnuFileProperties_Click
End Sub

Public Sub mnuApplicationRatingExplore_Click(Index As Integer)
  ' Set id before selecting folder as the list view will change.
  Dim Key As String
  Key = ListItemText(mParent.lvView.SelectedItem, DatabaseName.QUOTE_KEY)

  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.APPLICATION_TABLE, , 0, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
    Case 2
      mParent.FolderLoad DatabaseName.RATING_TABLE, , 0, Key
    Case 3
      mParent.FolderLoad DatabaseName.COVERAGE_TABLE, , 2, Key
    Case 4
      mParent.FolderLoad DatabaseName.FIRM_TABLE, , 1, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY)
    Case 5
      mParent.FolderLoad DatabaseName.RATINGLAWYER_TABLE, , 0, Key
    Case 6
      mParent.FolderLoad DatabaseName.RatingLIMIT_TABLE, , 1, Key
  End Select
End Sub

Public Sub mnuApplicationRatingOpen_Click(Index As Integer)
  ' Set id before selecting folder as the list view will change.
  Dim Key As String
  Key = ListItemText(mParent.lvView.SelectedItem, DatabaseName.QUOTE_KEY)
  Select Case Index
    Case 1 'Edit
      FormUtil.ShowByListView New frmQuoteEdit, mParent.FormMgr, mParent.lvView
    Case 2 'Browse Worksheet
      Dispatcher.BrowseUnderwritingWorksheet Key
  End Select
End Sub

Public Sub mnuRatingProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.QUOTE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.QUOTE_KEY)
  End If
End Sub

Public Sub mnuRatingSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuViewIcons_Click(Index As Integer)
  mParent.lvView.View = Index
  mParent.tbToolBar.Buttons("lvMode" & Index).Value = tbrPressed
End Sub

