VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmPersonEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Person"
   ClientHeight    =   7488
   ClientLeft      =   156
   ClientTop       =   1440
   ClientWidth     =   8040
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7488
   ScaleWidth      =   8040
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   7110
      Left            =   0
      ScaleHeight     =   7116
      ScaleWidth      =   8040
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   384
      Width           =   8040
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   6720
         TabIndex        =   37
         Top             =   6540
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   336
         Left            =   5520
         TabIndex        =   36
         Top             =   6540
         Width           =   1140
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   960
         Top             =   6480
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   1560
         Top             =   6480
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   5880
         Left            =   90
         TabIndex        =   38
         Top             =   510
         Width           =   7845
         _ExtentX        =   13843
         _ExtentY        =   10372
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   5
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmPersonEdit.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "frmLocation"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraLocation"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraEntity"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "&Lawyer"
         TabPicture(1)   =   "frmPersonEdit.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lbllawyerID"
         Tab(1).Control(1)=   "lblPositionID"
         Tab(1).Control(2)=   "lblLawyerRetroactiveDate"
         Tab(1).Control(3)=   "lblBarAdmitttedDate"
         Tab(1).Control(4)=   "lblYearsInPratice"
         Tab(1).Control(5)=   "Label8"
         Tab(1).Control(6)=   "Label9"
         Tab(1).Control(7)=   "lblInsureStatusName"
         Tab(1).Control(8)=   "dtpDataField(0)"
         Tab(1).Control(9)=   "mskDataField(0)"
         Tab(1).Control(10)=   "cboDatafield(1)"
         Tab(1).Control(11)=   "txtDatafield(20)"
         Tab(1).Control(12)=   "chkDatafield(0)"
         Tab(1).Control(13)=   "txtDatafield(9)"
         Tab(1).Control(14)=   "mskYearAdmitted"
         Tab(1).ControlCount=   15
         TabCaption(2)   =   "&Attribute"
         TabPicture(2)   =   "frmPersonEdit.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lblLabel"
         Tab(2).Control(1)=   "lvAttribute"
         Tab(2).Control(2)=   "cboAttribute"
         Tab(2).Control(3)=   "txtAttribute"
         Tab(2).Control(4)=   "txtDatafield(7)"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "txtDatafield(8)"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).Control(6)=   "cboContactType"
         Tab(2).ControlCount=   7
         TabCaption(3)   =   "N&ote"
         TabPicture(3)   =   "frmPersonEdit.frx":0054
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "txtNote"
         Tab(3).Control(1)=   "cmdClearNotepad"
         Tab(3).ControlCount=   2
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74880
            TabIndex        =   77
            Top             =   5145
            Width           =   1000
         End
         Begin VB.TextBox txtNote 
            Height          =   4680
            Left            =   -74880
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   76
            Text            =   "frmPersonEdit.frx":0070
            Top             =   372
            Width           =   6240
         End
         Begin VB.ComboBox cboContactType 
            Height          =   315
            Left            =   -73215
            Style           =   2  'Dropdown List
            TabIndex        =   73
            Top             =   3390
            Width           =   2808
         End
         Begin VB.TextBox txtDatafield 
            DataField       =   "contactTypeId"
            DataMember      =   "Person"
            Height          =   288
            Index           =   8
            Left            =   -69690
            TabIndex        =   72
            TabStop         =   0   'False
            Text            =   "Text1"
            Top             =   3405
            Visible         =   0   'False
            Width           =   852
         End
         Begin VB.TextBox txtDatafield 
            DataField       =   "attributeId"
            DataMember      =   "Person"
            Height          =   288
            Index           =   7
            Left            =   -69765
            TabIndex        =   71
            TabStop         =   0   'False
            Text            =   "Text1"
            Top             =   240
            Visible         =   0   'False
            Width           =   852
         End
         Begin VB.TextBox txtAttribute 
            Height          =   288
            Left            =   -72102
            TabIndex        =   70
            Top             =   495
            Width           =   3348
         End
         Begin VB.ComboBox cboAttribute 
            Height          =   315
            Left            =   -74784
            Style           =   2  'Dropdown List
            TabIndex        =   69
            Top             =   480
            Width           =   2304
         End
         Begin VB.Frame fraEntity 
            Caption         =   "Organization"
            Height          =   1110
            Left            =   120
            TabIndex        =   61
            Top             =   2625
            Width           =   7590
            Begin VB.CommandButton cmdEntity 
               Caption         =   "Firm"
               Height          =   288
               Left            =   6390
               TabIndex        =   13
               Top             =   372
               Width           =   1050
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "entityName"
               DataField       =   "entityName"
               DataMember      =   "EntityOrganization"
               Height          =   195
               Index           =   20
               Left            =   1170
               TabIndex        =   68
               Top             =   270
               UseMnemonic     =   0   'False
               Width           =   795
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               Height          =   195
               Index           =   5
               Left            =   150
               TabIndex        =   67
               Top             =   270
               Width           =   480
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "entityLocator"
               DataField       =   "entityLocator"
               DataMember      =   "EntityOrganization"
               Height          =   195
               Index           =   0
               Left            =   1170
               TabIndex        =   66
               Top             =   525
               Width           =   915
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               Caption         =   "Locator:"
               Height          =   195
               Left            =   150
               TabIndex        =   65
               Top             =   525
               Width           =   570
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Select"
               Height          =   195
               Index           =   1
               Left            =   6660
               TabIndex        =   64
               Top             =   150
               Width           =   450
            End
            Begin VB.Label lblStatBarId 
               AutoSize        =   -1  'True
               Caption         =   "State Bar Id:"
               Height          =   195
               Left            =   150
               TabIndex        =   63
               Top             =   780
               Width           =   885
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "stateBarNumber"
               DataField       =   "stateBarNumber"
               DataMember      =   "Firm"
               Height          =   195
               Index           =   1
               Left            =   1170
               TabIndex        =   62
               Top             =   780
               Width           =   1140
            End
         End
         Begin VB.Frame fraLocation 
            Caption         =   "Primary Location"
            Height          =   1956
            Left            =   120
            TabIndex        =   43
            Top             =   3795
            Width           =   7590
            Begin VB.CommandButton cmdLocation 
               Caption         =   "Lo&cation"
               Height          =   288
               Left            =   6390
               TabIndex        =   14
               Top             =   372
               Width           =   1050
            End
            Begin VB.Label Label13 
               AutoSize        =   -1  'True
               Caption         =   "Entity Name:"
               Height          =   195
               Left            =   150
               TabIndex        =   60
               Top             =   285
               Width           =   900
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "City:"
               Height          =   192
               Left            =   144
               TabIndex        =   59
               Top             =   1404
               Width           =   300
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               Caption         =   "State:"
               Height          =   195
               Left            =   2535
               TabIndex        =   58
               Top             =   1410
               Width           =   405
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "Zip:"
               Height          =   195
               Left            =   3675
               TabIndex        =   57
               Top             =   1410
               Width           =   270
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "Main Fax:"
               Height          =   192
               Left            =   2112
               TabIndex        =   56
               Top             =   1668
               Width           =   684
            End
            Begin VB.Label Label10 
               AutoSize        =   -1  'True
               Caption         =   "Main Tel:"
               Height          =   192
               Left            =   144
               TabIndex        =   55
               Top             =   1668
               Width           =   660
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "entityName"
               DataField       =   "entityName"
               DataMember      =   "LocationEntity"
               Height          =   195
               Index           =   7
               Left            =   1170
               TabIndex        =   54
               Top             =   285
               UseMnemonic     =   0   'False
               Width           =   795
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "addressLine2"
               DataField       =   "addressLine2"
               DataMember      =   "Location"
               Height          =   195
               Index           =   9
               Left            =   1170
               TabIndex        =   53
               Top             =   870
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "addressLine3"
               DataField       =   "addressLine3"
               DataMember      =   "Location"
               Height          =   195
               Index           =   10
               Left            =   1170
               TabIndex        =   52
               Top             =   1110
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "city"
               DataField       =   "city"
               DataMember      =   "Location"
               Height          =   195
               Index           =   11
               Left            =   1170
               TabIndex        =   51
               Top             =   1410
               UseMnemonic     =   0   'False
               Width           =   240
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "state"
               DataField       =   "state"
               DataMember      =   "Location"
               Height          =   195
               Index           =   14
               Left            =   3030
               TabIndex        =   50
               Top             =   1410
               UseMnemonic     =   0   'False
               Width           =   345
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "zip"
               DataField       =   "zip"
               DataMember      =   "Location"
               Height          =   195
               Index           =   15
               Left            =   4020
               TabIndex        =   49
               Top             =   1410
               UseMnemonic     =   0   'False
               Width           =   210
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "fax"
               DataField       =   "fax"
               DataMember      =   "Location"
               Height          =   192
               Index           =   17
               Left            =   3060
               TabIndex        =   48
               Top             =   1668
               UseMnemonic     =   0   'False
               Width           =   204
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "telephone"
               DataField       =   "telephone"
               DataMember      =   "Location"
               Height          =   192
               Index           =   16
               Left            =   1020
               TabIndex        =   47
               Top             =   1668
               UseMnemonic     =   0   'False
               Width           =   720
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Address:"
               Height          =   192
               Left            =   144
               TabIndex        =   46
               Top             =   624
               Width           =   648
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "addressLine1"
               DataField       =   "addressLine1"
               DataMember      =   "Location"
               Height          =   195
               Index           =   8
               Left            =   1170
               TabIndex        =   45
               Top             =   630
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "Select"
               Height          =   192
               Left            =   6660
               TabIndex        =   44
               Top             =   156
               Width           =   456
            End
         End
         Begin MSMask.MaskEdBox mskYearAdmitted 
            Height          =   285
            Left            =   -72945
            TabIndex        =   16
            Top             =   915
            Width           =   1485
            _ExtentX        =   2625
            _ExtentY        =   508
            _Version        =   393216
            MaxLength       =   4
            Mask            =   "####"
            PromptChar      =   "_"
         End
         Begin VB.TextBox txtDatafield 
            DataField       =   "stateBarNumber"
            DataMember      =   "LawyerStateBar"
            Height          =   288
            Index           =   9
            Left            =   -72948
            TabIndex        =   20
            Text            =   "stateBarNumber"
            Top             =   2268
            Width           =   2895
         End
         Begin VB.CheckBox chkDatafield 
            Caption         =   "Uninsurable"
            DataField       =   "Uninsurable"
            DataMember      =   "Lawyer"
            Height          =   192
            Index           =   0
            Left            =   -72936
            TabIndex        =   21
            Top             =   2976
            Width           =   2124
         End
         Begin VB.TextBox txtDatafield 
            DataField       =   "inPracticeDuration"
            DataMember      =   "Lawyer"
            Height          =   288
            Index           =   20
            Left            =   -72948
            TabIndex        =   17
            Text            =   "Years In Practice"
            Top             =   1248
            Width           =   1470
         End
         Begin VB.ComboBox cboDatafield 
            DataField       =   "lawyerDesignationId"
            DataMember      =   "Lawyer"
            Height          =   315
            Index           =   1
            ItemData        =   "frmPersonEdit.frx":0076
            Left            =   -72948
            List            =   "frmPersonEdit.frx":0078
            Style           =   2  'Dropdown List
            TabIndex        =   18
            Top             =   1596
            Width           =   2895
         End
         Begin VB.Frame frmLocation 
            Caption         =   "Person"
            Height          =   2190
            Left            =   120
            TabIndex        =   27
            Top             =   390
            Width           =   7590
            Begin VB.TextBox txtDatafield 
               DataField       =   "homeEmail"
               DataMember      =   "Person"
               Height          =   288
               Index           =   6
               Left            =   1500
               TabIndex        =   8
               Top             =   1770
               Width           =   3150
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "email"
               DataMember      =   "Person"
               Height          =   288
               Index           =   14
               Left            =   1500
               TabIndex        =   7
               Top             =   1425
               Width           =   3150
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "suffix"
               DataMember      =   "Person"
               Height          =   288
               Index           =   5
               Left            =   5640
               TabIndex        =   4
               Top             =   324
               Width           =   456
            End
            Begin VB.CheckBox chkNewLawyer 
               Caption         =   "&New Lawyer"
               Height          =   240
               Left            =   5895
               TabIndex        =   12
               Top             =   1794
               Width           =   1248
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "title"
               DataMember      =   "Person"
               Height          =   315
               Index           =   0
               ItemData        =   "frmPersonEdit.frx":007A
               Left            =   108
               List            =   "frmPersonEdit.frx":008D
               TabIndex        =   0
               Text            =   "Atty."
               Top             =   324
               Width           =   744
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "firstName"
               DataMember      =   "Person"
               Height          =   288
               Index           =   0
               Left            =   876
               TabIndex        =   1
               Top             =   324
               Width           =   1380
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "middleName"
               DataMember      =   "Person"
               Height          =   288
               Index           =   1
               Left            =   2304
               TabIndex        =   2
               Top             =   324
               Width           =   1368
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "lastName"
               DataMember      =   "Person"
               Height          =   288
               Index           =   2
               Left            =   3708
               TabIndex        =   3
               Top             =   324
               Width           =   1896
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "salutation"
               DataMember      =   "Person"
               Height          =   288
               Index           =   4
               Left            =   1500
               TabIndex        =   6
               Top             =   1080
               Width           =   3144
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "entityName"
               DataMember      =   "Entity"
               Height          =   288
               Index           =   3
               Left            =   1500
               TabIndex        =   5
               Top             =   732
               Width           =   3135
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmPersonEdit.frx":00AE
               DataField       =   "workTelephone"
               DataMember      =   "Person"
               Height          =   285
               Index           =   1
               Left            =   5895
               TabIndex        =   9
               Top             =   735
               Width           =   1395
               _ExtentX        =   2477
               _ExtentY        =   508
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmPersonEdit.frx":00BF
               DataField       =   "mobileTelephone"
               DataMember      =   "Person"
               Height          =   285
               Index           =   2
               Left            =   5895
               TabIndex        =   10
               Top             =   1080
               Width           =   1395
               _ExtentX        =   2477
               _ExtentY        =   508
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmPersonEdit.frx":00D0
               DataField       =   "homeTelephone"
               DataMember      =   "Person"
               Height          =   285
               Index           =   3
               Left            =   5895
               TabIndex        =   11
               Top             =   1425
               Width           =   1395
               _ExtentX        =   2477
               _ExtentY        =   508
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin VB.Label Label16 
               AutoSize        =   -1  'True
               Caption         =   "Home Email:"
               Height          =   195
               Left            =   90
               TabIndex        =   81
               Top             =   1815
               Width           =   885
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "Work Phone:"
               Height          =   195
               Left            =   4815
               TabIndex        =   80
               Top             =   780
               Width           =   945
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Mobile Phone:"
               Height          =   195
               Index           =   2
               Left            =   4815
               TabIndex        =   79
               Top             =   1125
               Width           =   1020
            End
            Begin VB.Label Label14 
               AutoSize        =   -1  'True
               Caption         =   "Home Phone:"
               Height          =   195
               Left            =   4815
               TabIndex        =   78
               Top             =   1470
               Width           =   975
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               Caption         =   "Work Email:"
               Height          =   195
               Left            =   105
               TabIndex        =   42
               Top             =   1470
               Width           =   855
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Default Salutation:"
               Height          =   192
               Index           =   0
               Left            =   108
               TabIndex        =   29
               Top             =   1128
               Width           =   1284
            End
            Begin VB.Label Label20 
               AutoSize        =   -1  'True
               Caption         =   "Display Name:"
               Height          =   192
               Left            =   108
               TabIndex        =   28
               Top             =   792
               Width           =   1068
            End
         End
         Begin MSMask.MaskEdBox mskDataField 
            Bindings        =   "frmPersonEdit.frx":00E1
            DataField       =   "entityLocator"
            DataMember      =   "Entity"
            Height          =   288
            Index           =   0
            Left            =   -72972
            TabIndex        =   15
            Top             =   408
            Width           =   1152
            _ExtentX        =   2032
            _ExtentY        =   508
            _Version        =   393216
            MaxLength       =   8
            Mask            =   ">AAAAAAAA"
            PromptChar      =   "_"
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmPersonEdit.frx":00F2
            DataField       =   "retroactiveDate"
            DataMember      =   "Lawyer"
            Height          =   285
            Index           =   0
            Left            =   -72945
            TabIndex        =   19
            Top             =   1935
            Width           =   1485
            _ExtentX        =   2625
            _ExtentY        =   508
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   79626241
            CurrentDate     =   37085
         End
         Begin MSComctlLib.ListView lvAttribute 
            Height          =   2310
            Left            =   -74784
            TabIndex        =   74
            Top             =   870
            Width           =   6030
            _ExtentX        =   10605
            _ExtentY        =   4106
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Name"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Value"
               Object.Width           =   5292
            EndProperty
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Default contact type:"
            Height          =   195
            Left            =   -74790
            TabIndex        =   75
            Top             =   3450
            Width           =   1485
         End
         Begin VB.Label lblInsureStatusName 
            AutoSize        =   -1  'True
            Caption         =   "insureStatusName"
            DataField       =   "insureStatusName"
            DataMember      =   "Lawyer"
            Height          =   192
            Left            =   -72948
            TabIndex        =   41
            Top             =   2664
            Width           =   1320
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Insurer Status:"
            Height          =   192
            Left            =   -74868
            TabIndex        =   40
            Top             =   2664
            Width           =   996
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "State Bar Id:"
            Height          =   192
            Left            =   -74868
            TabIndex        =   39
            Top             =   2316
            Width           =   864
         End
         Begin VB.Label lblYearsInPratice 
            Caption         =   "Years In Practice:"
            Height          =   192
            Left            =   -74868
            TabIndex        =   34
            Top             =   1308
            Width           =   1812
         End
         Begin VB.Label lblBarAdmitttedDate 
            Caption         =   "Year Admitted:"
            Height          =   192
            Left            =   -74868
            TabIndex        =   33
            Top             =   972
            Width           =   1764
         End
         Begin VB.Label lblLawyerRetroactiveDate 
            AutoSize        =   -1  'True
            Caption         =   "Retroactive Date:"
            Height          =   192
            Left            =   -74868
            TabIndex        =   32
            Top             =   1980
            Width           =   1248
         End
         Begin VB.Label lblPositionID 
            Caption         =   "Position Id:"
            Height          =   192
            Left            =   -74868
            TabIndex        =   31
            Top             =   1644
            Width           =   972
         End
         Begin VB.Label lbllawyerID 
            Caption         =   "Locator:"
            Height          =   192
            Left            =   -74868
            TabIndex        =   30
            Top             =   456
            Width           =   1212
         End
      End
      Begin VB.Label lblDisplayName 
         AutoSize        =   -1  'True
         Caption         =   "lblDisplayName"
         Height          =   192
         Left            =   144
         TabIndex        =   35
         Top             =   144
         Width           =   1164
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   396
      Left            =   1152
      TabIndex        =   25
      Top             =   1884
      Visible         =   0   'False
      Width           =   972
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   8040
      _ExtentX        =   14182
      _ExtentY        =   677
      BandCount       =   1
      _CBWidth        =   8040
      _CBHeight       =   384
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   336
         ScaleWidth      =   7944
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   24
         Width           =   7944
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   24
            Top             =   60
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Person"
            Index           =   0
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "L&ocation"
            Index           =   2
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Organization"
            Index           =   3
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Person"
            Index           =   0
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "L&ocation"
            Index           =   2
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Lawyer"
            Index           =   3
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "L&ocation"
         Index           =   0
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMakeLawyer 
         Caption         =   "&Make Lawyer"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator503 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmPersonEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.50.00 (jjm) 2001 02 11
'ver 2.30.0710 (jjm)
'ver 2.10.0402 (jjm) wilmic
'ver 2.10.0324 (jjm) wilmic

Option Explicit

Private mPersonHandle As String
Private mOrganizationHandle As String
Private mLocationHandle As String
Private mLocationTypeHandle As String
Private mLawyerHandle As String
Private mEventHandle As String
Private mLawyerDesignationHandle As String

Private mNew As Boolean
Private mPersonDataset As Dataset

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private mAttribute As collection
Private mAttributeValue As collection
Private mContactType As collection

Private FormMgr As New FormManager

'ver 2.00.0310 (jjm)
'Private mFirmHandle As String

'ver 2.10.0324 (jjm)
Private mInPracticeDate As Date

'Blank Mask String
Private Const BLANK_LOCATOR = "________"
Private Const TELEPHONE_MASK = "___.___.____"


'ver 2.73.00 (jjm) REF 225
Private isCI As Boolean

'For Location Type collection
Private mLocationType As collection

'ver 2.71.00 (jjm) 2005 01 31 - for city dropdown
'  http://www.freevbcode.com/ShowCode.Asp?ID=2266
Private miSelStart As Integer


'Mantis 2733 - load LawyerDesignationList from data
Private mLawyerDesignation As collection


'Constants
Private Const LBL_ENTITYLOCATOR_INDEX = 0
Private Const LBL_STATEBARNUMBER_INDEX = 1

Private Const MSK_ENTITYLOCATOR_INDEX = 0
Private Const MSK_TELEPHONE_INDEX = 1
Private Const MSK_FAX_INDEX = 2

Private Const TXT_COUNTY_INDEX = 13
Private Const TXT_ENTITYNAME_INDEX = 3
Private Const TXT_FIRSTNAME_INDEX = 0
Private Const TXT_INPRACTICEDURATION_INDEX = 20
Private Const TXT_LASTNAME_INDEX = 2
Private Const TXT_MIDDLENAME_INDEX = 1
Private Const TXT_SALUTATION_INDEX = 4
Private Const TXT_SUFFIX_INDEX = 5
Private Const TXT_ZIP_INDEX = 19

Private Const CBO_TITLE_INDEX = 0
Private Const CBO_DESIGNATION_INDEX = 1

Private Const TAB_LAWYER_INDEX = 1


Private Sub AttributeRefresh()
  lvAttribute.ListItems.Clear
  If mAttributeValue Is Nothing Then
    Exit Sub
  End If
  Dim Row As collection, Attr As String
  For Each Row In mAttribute
    Attr = Row("attributeId")
    If Contains(mAttributeValue, Attr) Then
      If mAttributeValue(Attr) <> vbNullString Then
        With lvAttribute.ListItems.Add(, Attr, Row("attributeName"))
          .ListSubItems.Add , , mAttributeValue(Attr)
        End With
      End If
    End If
  Next Row
End Sub


Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Private Property Get isLawyer() As Boolean
  isLawyer = mLawyerHandle <> vbNullString Or chkNewLawyer.Value = vbChecked
End Property

Private Property Let LocationHandle(vData As String)
  mLocationHandle = vData
  
  'Refresh Location Data
  If mLocationHandle = vbNullString Then
    ClearDataField lblDataField, DatabaseName.LOCATION_TABLE
    'Location -> Entity-Main -> entityName
    ClearDataField lblDataField, "LocationEntity"
  Else
    Dim LocationData As collection
    Set LocationData = Factory.LocationService.Find(mLocationHandle)
    SetFields lblDataField, LocationData, DatabaseName.LOCATION_TABLE
    Dim LocationEntityData As collection
    Dim entityhandle As String
    entityhandle = GetDataFromCollection(LocationData, DatabaseName.MAINENTITYID_FIELD, True)
    Set LocationEntityData = Factory.EntityService.Find(entityhandle, DatabaseName.ENTITYNAME_FIELD)
    SetFields lblDataField, LocationEntityData, "LocationEntity"
  End If
  

End Property

Private Property Get LocationHandle() As String
  LocationHandle = mLocationHandle
End Property


'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub


'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
    Case fimNew
      If fInterface.Key = DatabaseName.LOCATION_TABLE Then
        mnuFileNew_Click (2)
      End If
      'mantis 2459
      If fInterface.Key = DatabaseName.ORGANIZATION_TABLE Or _
         fInterface.Key = DatabaseName.FIRM_TABLE Then
        mnuFileNew_Click (3)
      End If
    Case fimSelect
      If fInterface.Key = DatabaseName.LOCATION_TABLE Then
        'ver 2.73.00 (jjm) REF 225 - select none functionality
        If fInterface.Child.ReturnNull Then
          LocationHandle = vbNullString
          Dirty = True
          Exit Sub
        End If
      
        LocationHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
        Dirty = True
      End If
      
      'ver 2.00.0310 (jjm) wilmic
      If fInterface.Key = DatabaseName.FIRM_TABLE Or _
         fInterface.Key = DatabaseName.ORGANIZATION_TABLE Then
        Dim Handle As String
        
        
        'ver 2.50.00 (jjm) 2004 02 19
        If fInterface.Child.ReturnNull Then
          mOrganizationHandle = vbNullString
          ClearDataField lblDataField, "EntityOrganization"
          ClearDataField lblDataField, "Firm"
          Dirty = True
          Exit Sub
        End If
        
        Dim isFirmSearch As Boolean
        isFirmSearch = fInterface.Key = DatabaseName.FIRM_TABLE
        Handle = ListItemText(fInterface.Child.lvView.SelectedItem, IIf(isFirmSearch, DatabaseName.FIRM_KEY, DatabaseName.ORGANIZATION_KEY))
        
        ' Test for active policy if it is a Firm select. This prevents the user from setting the lawyer to
        ' a currently insured firm. If the lawyer is to be added it must be done through an Add Lawyer endorsement.
        If isFirmSearch And _
            isLawyer And _
            Factory.FirmService.IsCurrentInsured(Handle) Then
          MsgBox "You cannot modify the organizationId in this manner because the firm is currently insured by WILMIC"
          cmdEntity_Click
          Exit Sub
        End If
        
        mOrganizationHandle = Handle
        OrganizationRefresh
        Dirty = True
      End If

        
      
    Case fimSave
      Dirty = True
      If fInterface.Key = DatabaseName.LOCATION_TABLE & "Edit" Then
        LocationHandle = fInterface.Child.LocationHandle
      End If
      'mantis 2459
      If fInterface.Key = DatabaseName.ORGANIZATION_TABLE & "New" Then
        mOrganizationHandle = fInterface.Child.organizationHandle
        OrganizationRefresh
      End If
  End Select
End Sub
 
 
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Get PersonHandle()
  PersonHandle = mPersonHandle
End Property

'Refresh Firm display info
Private Sub OrganizationRefresh()
  If mOrganizationHandle = vbNullString Then
    ClearDataField lblDataField, "EntityOrganization"
    Exit Sub
  End If
  
  Dim EntityOrgCol As collection
  Set EntityOrgCol = Factory.EntityService.Find(mOrganizationHandle)
  SetFields lblDataField, EntityOrgCol, "EntityOrganization"
  
  'Populate location handle if new and organization is passed
  If mNew And LocationHandle = vbNullString Then
    LocationHandle = GetDataFromCollection(EntityOrgCol, DatabaseName.MAINLOCATIONID_FIELD)
  End If
  
  'Only set when a lawyer
  SetFields lblDataField, Factory.FirmService.Find(mOrganizationHandle), "Firm"

End Sub

Private Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  ClearDataField lblDataField
  ClearDataField txtDataField
  ClearDataField mskDataField
  txtNote.Text = vbNullString
  
  mLawyerHandle = vbNullString
  mEventHandle = vbNullString
  mInPracticeDate = vbNullDate
  
  lblInsureStatusName.Caption = vbNullString
  lblDisplayName.Caption = vbNullString
  
  Set mAttributeValue = Nothing
  
  'Organization Handle is cleared in the File_new function because
  'the it is possible to se the organizationId on the ShowNew call.
  
  If (mNew) Then
    Caption = "New Person"
    cboDataField(CBO_DESIGNATION_INDEX).ListIndex = 0
       
    'CANNOT be with the current application lawyer entry
    'Validate that a defaulted firm is not CI
    'If chkNewLawyer.Value = vbChecked And _
    '    mOrganizationHandle <> vbNullString And _
    '    Factory.FirmService.IsCurrentInsured(mOrganizationHandle) Then
    '  MsgBox "The firm for this lawyer can not be set because the referenced firm is currently insured by WILMIC. The lawyer association must be set by application or endorsement"
    '  mOrganizationHandle = vbNullString
    'End If
    
    
  Else
    Dim PersonData As collection
    Dim LawyerData As collection
    Set PersonData = Factory.PersonService.Find(mPersonHandle)
    
    If PersonData Is Nothing Then
      MsgBox "Person id " & mPersonHandle & " no longer exists."
      mNew = True
      FormRefresh
      Exit Sub
    End If
    
    Caption = "Edit Person (" & mPersonHandle & ")"
    
    SetFields txtDataField, PersonData, DatabaseName.PERSON_TABLE
    SetFields cboDataField, PersonData, DatabaseName.PERSON_TABLE
    SetFields mskDataField, PersonData, DatabaseName.PERSON_TABLE
    
    Dim EntityData As New collection
    Set EntityData = Factory.EntityService.Find(mPersonHandle)
    SetFields txtDataField, EntityData, DatabaseName.ENTITY_TABLE
    SetFields mskDataField, EntityData, DatabaseName.ENTITY_TABLE
    
    mEventHandle = GetDataFromCollection(EntityData, DatabaseName.EVENT_KEY)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
      'Load the attribute data
      Set mAttributeValue = Factory.EventService.GetAttributes(mEventHandle)
    End If
    
    LocationHandle = GetDataFromCollection(EntityData, DatabaseName.MAINLOCATIONID_FIELD)
              
    Set LawyerData = Factory.LawyerService.Find(mPersonHandle)
    If Not LawyerData Is Nothing Then
      'ver 2.00.0310 (jjm) wilmic
      'ver 2.50.00 (jjm) 2001 02 11 - Made the firm service call return more fields than just the firm name
      mOrganizationHandle = GetDataFromCollection(LawyerData, DatabaseName.LATESTFIRMID_FIELD)
    
      mLawyerHandle = GetDataFromCollection(LawyerData, DatabaseName.LAWYER_KEY)
      SetFields txtDataField, LawyerData, DatabaseName.LAWYER_TABLE
      SetFields cboDataField, LawyerData, DatabaseName.LAWYER_TABLE
      SetFields dtpDataField, LawyerData, DatabaseName.LAWYER_TABLE
      SetFields lblDataField, LawyerData, DatabaseName.LAWYER_TABLE
      
      ' Hack ToDO: Clean data
      chkDatafield(0).Value = IIf(GetBoolean(LawyerData(DatabaseName.ISUNINSURABLE_FIELD)), 1, 0)

      Dim StateBarData As collection
      Set StateBarData = Factory.LawyerService.FindLawyerStateBar(mLawyerHandle, "WI")
      SetFields txtDataField, StateBarData, DatabaseName.LAWYERSTATEBAR_TABLE
      Dim admitDate As String
      admitDate = GetDataFromCollection(StateBarData, DatabaseName.BARADMITDATE_FIELD)
      If admitDate <> vbNullString Then
        mskYearAdmitted.Text = Year(admitDate)
      End If
      
      
      'ver 2.10.0324 (jjm)
      mInPracticeDate = GetDate(GetDataFromCollection(LawyerData, DatabaseName.INPRACTICEDATE_FIELD))
      txtDataField(TXT_INPRACTICEDURATION_INDEX).BackColor = IIf(mInPracticeDate = vbNullDate, txtDataField(2).BackColor, cmdSave.BackColor)
      txtDataField(TXT_INPRACTICEDURATION_INDEX).Locked = Not (mInPracticeDate = vbNullDate)
      
      'Set the Insure Status Label - lookup in the EventType table under eventTypeName
      Dim Col As collection
      Set Col = Factory.EventTypeService.Find(GetDataFromCollection(LawyerData, DatabaseName.LATESTPOLICYSTATUS_FIELD), DatabaseName.EVENTTYPENAME_FIELD)
      If Not Col Is Nothing Then 'Can't use iif because of vb constraints
        lblInsureStatusName.Caption = GetDataFromCollection(Col, DatabaseName.EVENTTYPENAME_FIELD)
      End If
      
      'ver 2.73.00 (jjm) REF 225
      'set the isCI flag
      isCI = (GetDataFromCollection(Factory.LawyerService.Find(mLawyerHandle, DatabaseName.LATESTPOLICYSTATUS_FIELD), DatabaseName.LATESTPOLICYSTATUS_FIELD) = DatabaseName.CURRENTINSURED_TYPE)
      
      
      mLawyerDesignationHandle = GetDataFromCollection(LawyerData, DatabaseName.LAWYERDESIGNATION_KEY)
      cboDataField(CBO_DESIGNATION_INDEX).ListIndex = CollectionFind(mLawyerDesignation, DatabaseName.LAWYERDESIGNATION_KEY, mLawyerDesignationHandle) - 1
      
    End If
  
    txtDataField(4).Text = PersonData(DatabaseName.SALUTATION_FIELD)
  End If
    
  SetupEntity
  
  AttributeRefresh
  cboAttribute.ListIndex = -1
  
  'Handle Organization and Location info. This is done here because organization and location
  'handles are optional arguments to the ShowNew method.
  OrganizationRefresh
  
  mnuFileOpen(0).Enabled = Not mNew
  mnuFileOpen(2).Enabled = mLocationHandle <> vbNullString
  mnuFileOpen(3).Enabled = mLawyerHandle <> vbNullString
  mnuFileProperties.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  chkNewLawyer.Visible = mLawyerHandle = vbNullString 'And mNew
  SSTab1.TabVisible(TAB_LAWYER_INDEX) = Not mNew And mLawyerHandle <> vbNullString
  'SSTab1.TabVisible(2) = SSTab1.TabVisible(1)
  
  
  ' Refresh navigation
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  SSTab1_Click SSTab1.Tab
   
  Dirty = False
End Sub


Public Property Get LawyerHandle() As String
  LawyerHandle = mLawyerHandle
End Property


Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mPersonHandle = ListItemText(mListView.SelectedItem, DatabaseName.PERSON_KEY)
  'Hack for using a Lawyer as source if the source does not contain personId
  If mPersonHandle = vbNullString Then
    mPersonHandle = ListItemText(mListView.SelectedItem, DatabaseName.LAWYER_KEY)
  End If
  
  
End Property


'Create a locator if it is blank
Private Sub ProcessLocator()
  If mskDataField(MSK_ENTITYLOCATOR_INDEX).Text = BLANK_LOCATOR Then

    'Get the Locator
    'First grab the first four letters of Last Name.
    'Then the first three letters of the first name
    'Then the middle inital
    mskDataField(MSK_ENTITYLOCATOR_INDEX).SelText = UCase(Left$(Replace(txtDataField(TXT_LASTNAME_INDEX).Text, "'", "") & "____", 4) & _
                   Left$(Replace(txtDataField(TXT_FIRSTNAME_INDEX).Text, ".", "") & "___", 3) & _
                   Left$(txtDataField(TXT_MIDDLENAME_INDEX).Text & "_", 1))
                   
    'mskDataField(MSK_ENTITYLOCATOR_INDEX).Text = Factory.PersonService.CreateLocator(mskDataField(MSK_ENTITYLOCATOR_INDEX).Text, mPersonHandle)
    mskDataField(MSK_ENTITYLOCATOR_INDEX).Text = Factory.EntityService.CreatePersonLocator(mskDataField(MSK_ENTITYLOCATOR_INDEX).Text, mPersonHandle)
    FormUtil.SelAll mskDataField(0)
    
  End If

End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
  ' Client side duplication of middle tier rules.
  
  If Not (mLawyerHandle = vbNullString And chkNewLawyer.Value = 0) _
     And mskDataField(0).Text = BLANK_LOCATOR Then
    MsgBox "A locator is required for lawyer records"
    SSTab1.Tab = 1
    mskDataField(MSK_ENTITYLOCATOR_INDEX).SetFocus
    Exit Function
  End If
  
  If txtDataField(TXT_ENTITYNAME_INDEX).Text = vbNullString Then
    MsgBox "Display name is required for person records"
    SSTab1.Tab = 0
    txtDataField(TXT_ENTITYNAME_INDEX).SetFocus
    Exit Function
  End If
  
  'Check for safe locator
  'ver 2.75.00 (jjm)
  'Make sure that the locator will not be the same as an exisiting firmId
  If Not Factory.EntityService.verifyEntityLocator(mskDataField(0).Text, mPersonHandle) Then
    Exit Function
  End If
  
  ' Custom Code
  'Need to load into two collections because the email field would create a double index
  Dim mDataControls As New collection
  Dim mLocDataControls As New collection
   
  'Entity data
  AddTextControl txtDataField, mDataControls, DatabaseName.ENTITY_TABLE
  AddMaskControl mskDataField, mDataControls, DatabaseName.ENTITY_TABLE
  'Person Data
  AddTextControl txtDataField, mDataControls, DatabaseName.PERSON_TABLE
  AddTextControl cboDataField, mDataControls, DatabaseName.PERSON_TABLE
  AddValueControl dtpDataField, mDataControls, DatabaseName.PERSON_TABLE
  AddMaskControl mskDataField, mDataControls, DatabaseName.PERSON_TABLE
  'Lawyer data
  AddTextControl txtDataField, mDataControls, DatabaseName.LAWYER_TABLE
  AddTextControl cboDataField, mDataControls, DatabaseName.LAWYER_TABLE
  AddValueControl dtpDataField, mDataControls, DatabaseName.LAWYER_TABLE
  'LawyerStateBar data
  AddTextControl txtDataField, mDataControls, DatabaseName.LAWYERSTATEBAR_TABLE
    
  ' If no lawyer, then save through person
  If mLawyerHandle = vbNullString And chkNewLawyer.Value = 0 Then
    If mNew Then
      mPersonHandle = Factory.PersonService.CreatePerson( _
                GetDataFromCollection(mDataControls, DatabaseName.ENTITYNAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.SALUTATION_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.TITLE_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.FIRSTNAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.MIDDLENAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.LASTNAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.SUFFIX_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.HOMEEMAIL_FIELD), _
                GetMask(GetDataFromCollection(mDataControls, DatabaseName.WORKTELEPHONE_FIELD), TELEPHONE_MASK), _
                GetMask(GetDataFromCollection(mDataControls, DatabaseName.MOBILETELEPHONE_FIELD), TELEPHONE_MASK), _
                GetMask(GetDataFromCollection(mDataControls, DatabaseName.HOMETELEPHONE_FIELD), TELEPHONE_MASK), _
                GetDataFromCollection(mDataControls, DatabaseName.CONTACTTYPE_KEY), _
                txtNote.Text, _
                mOrganizationHandle, mLocationHandle)
      Save = Not mPersonHandle = vbNullString
    Else
      Save = Factory.PersonService.WritePerson(mPersonHandle, _
                GetDataFromCollection(mDataControls, DatabaseName.ENTITYNAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.SALUTATION_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.TITLE_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.FIRSTNAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.MIDDLENAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.LASTNAME_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.SUFFIX_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
                GetDataFromCollection(mDataControls, DatabaseName.HOMEEMAIL_FIELD), _
                GetMask(GetDataFromCollection(mDataControls, DatabaseName.WORKTELEPHONE_FIELD), TELEPHONE_MASK), _
                GetMask(GetDataFromCollection(mDataControls, DatabaseName.MOBILETELEPHONE_FIELD), TELEPHONE_MASK), _
                GetMask(GetDataFromCollection(mDataControls, DatabaseName.HOMETELEPHONE_FIELD), TELEPHONE_MASK), _
                GetDataFromCollection(mDataControls, DatabaseName.CONTACTTYPE_KEY), _
                txtNote.Text, _
                mOrganizationHandle, mLocationHandle)
    End If
  
  ' otherwise, save person and lawyer throught the lawyer service.
  Else
    Dim mBarAdmittedDate As Date
    'ver 2.10.0402 (jjm)
    If mskYearAdmitted.Text <> "____" Then
      mBarAdmittedDate = CDate("1/1/" & mskYearAdmitted.Text)
    End If
    Dim Uninsurable As Boolean
    Uninsurable = IIf(chkDatafield(0).Value = vbChecked, True, False)
    
    'ver 2.00.0310 (jjm) wilmic ' added firm handle to save
    If mNew Then
      Dim Handle As String
      Handle = Factory.LawyerService.CreateLawyer( _
            GetDataFromCollection(mDataControls, DatabaseName.ENTITYLOCATOR_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.ENTITYNAME_FIELD), _
            mLawyerDesignationHandle, GetDate(GetDataFromCollection(mDataControls, DatabaseName.RETROACTIVEDATE_FIELD)), _
            GetDouble(GetDataFromCollection(mDataControls, DatabaseName.INPRACTICEDURATION_FIELD)), _
            Uninsurable, mBarAdmittedDate, GetDataFromCollection(mDataControls, DatabaseName.STATEBARNUMBER_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.SALUTATION_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.TITLE_FIELD), GetDataFromCollection(mDataControls, DatabaseName.FIRSTNAME_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.MIDDLENAME_FIELD), GetDataFromCollection(mDataControls, DatabaseName.LASTNAME_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.SUFFIX_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.CONTACTTYPE_KEY), _
            GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.HOMEEMAIL_FIELD), _
            GetMask(GetDataFromCollection(mDataControls, DatabaseName.WORKTELEPHONE_FIELD), TELEPHONE_MASK), _
            GetMask(GetDataFromCollection(mDataControls, DatabaseName.MOBILETELEPHONE_FIELD), TELEPHONE_MASK), _
            GetMask(GetDataFromCollection(mDataControls, DatabaseName.HOMETELEPHONE_FIELD), TELEPHONE_MASK), _
            mOrganizationHandle, txtNote.Text, mLocationHandle)
      Save = Handle <> vbNullString
      If Save Then
        'The Lawyer Handle and the Person handle have the same value
        mLawyerHandle = Handle
        mPersonHandle = Handle
      End If
    Else
      Save = Factory.LawyerService.WriteLawyer( _
            mPersonHandle, GetDataFromCollection(mDataControls, DatabaseName.ENTITYLOCATOR_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.ENTITYNAME_FIELD), _
            mLawyerDesignationHandle, _
            GetDate(GetDataFromCollection(mDataControls, DatabaseName.RETROACTIVEDATE_FIELD)), _
            GetDouble(GetDataFromCollection(mDataControls, DatabaseName.INPRACTICEDURATION_FIELD)), _
            Uninsurable, mBarAdmittedDate, GetDataFromCollection(mDataControls, DatabaseName.STATEBARNUMBER_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.SALUTATION_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.TITLE_FIELD), GetDataFromCollection(mDataControls, DatabaseName.FIRSTNAME_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.MIDDLENAME_FIELD), GetDataFromCollection(mDataControls, DatabaseName.LASTNAME_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.SUFFIX_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.CONTACTTYPE_KEY), _
            GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
            GetDataFromCollection(mDataControls, DatabaseName.HOMEEMAIL_FIELD), _
            GetMask(GetDataFromCollection(mDataControls, DatabaseName.WORKTELEPHONE_FIELD), TELEPHONE_MASK), _
            GetMask(GetDataFromCollection(mDataControls, DatabaseName.MOBILETELEPHONE_FIELD), TELEPHONE_MASK), _
            GetMask(GetDataFromCollection(mDataControls, DatabaseName.HOMETELEPHONE_FIELD), TELEPHONE_MASK), _
            mOrganizationHandle, txtNote.Text, mLocationHandle, _
            chkNewLawyer.Value = 1)
    End If
  End If
  
  'Set the event handle if it is a new item
  If mNew And Save Then
    mEventHandle = Factory.EntityService.Find(mPersonHandle)(DatabaseName.EVENT_KEY)
  End If
  
  'Save Attributes
  If Save Then
    If Not mAttributeValue Is Nothing Then
      Dim Size As Integer, I As Integer, Attr As String
      Size = mAttributeValue.Count
      If Size > 0 Then
        Dim Row As collection
        For Each Row In mAttribute
          Attr = Row("attributeId")
          If Contains(mAttributeValue, Attr) Then
            If Not Factory.EventService.SetAttribute(mEventHandle, Attr, mAttributeValue(Attr)) Then
              MsgBox "Attribute information has not been saved however the rest of the person data has.", vbCritical, "Error"
            End If
          End If
        Next Row
      End If
    End If
  End If
    
  If Save Then
    FormMgr.Message = fimSave
    mNew = False
  End If
  
  Dirty = Not Save
End Function

Private Sub SetupEntity()
  fraEntity.Caption = IIf(isLawyer, "&Firm", "&Organization")
  cmdEntity.Caption = IIf(isLawyer, "Firm", "Organization")
  
  lblStatBarId.Visible = isLawyer
  lblDataField(LBL_STATEBARNUMBER_INDEX).Visible = isLawyer
End Sub


Public Sub ShowByPrimaryKey(PersonId As String, Optional LawyerTab As Boolean)
  mPersonHandle = PersonId
  Show
  If LawyerTab Then
    SSTab1.Tab = 1
    mskYearAdmitted.SetFocus
    
  End If
End Sub

Public Sub ShowByListView(View As ListView, Optional LawyerTab As Boolean)
'  If View.ListItems.Count = 0 Then
'    MsgBox "There are no persons to view or edit.  To insert, press [Insert]."
'    Exit Sub
'  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
  If LawyerTab And SSTab1.TabVisible(1) Then
    SSTab1.Tab = 1
    mskYearAdmitted.SetFocus
  End If
End Sub

Public Sub ShowNew(Optional NewLawyer As Boolean, Optional organizationHandle As String)
  mOrganizationHandle = organizationHandle
  mNew = True
  Show
    
  If Not IsMissing(NewLawyer) Then
    If NewLawyer Then
      chkNewLawyer.Value = 1
      SSTab1.Tab = 0
      cboDataField(0).SetFocus
    End If
  End If
End Sub


Private Sub cboAttribute_Click()
  'ver 2.30.0709
  If cboAttribute.ListIndex = -1 Then
    txtAttribute.Text = vbNullString
    Exit Sub
  End If
  
  txtDataField(7).Text = mAttribute(cboAttribute.ListIndex + 1)("attributeId")
  If Contains(mAttributeValue, txtDataField(7).Text) Then
    txtAttribute.Text = mAttributeValue(txtDataField(7).Text)
  Else
    txtAttribute.Text = vbNullString
  End If
End Sub

Private Sub cboContactType_Click()
  'ver 2.30.0709
  If cboContactType.ListIndex = -1 Then
    txtDataField(8).Text = vbNullString
    Exit Sub
  End If
  
  txtDataField(8).Text = mContactType(cboContactType.ListIndex + 1)(DatabaseName.CONTACTTYPE_KEY)
End Sub


Private Sub cboDataField_Click(Index As Integer)
  
  Select Case Index
    'change the title, update the salutation
    Case CBO_TITLE_INDEX
      If txtDataField(TXT_LASTNAME_INDEX) <> vbNullString Then
        txtDataField(TXT_SALUTATION_INDEX).Text = cboDataField(Index).Text & " " & txtDataField(TXT_LASTNAME_INDEX).Text
      End If
    Case CBO_DESIGNATION_INDEX
      If cboDataField(Index).ListIndex < 0 Then
        mLawyerDesignationHandle = vbNullString
      Else
        mLawyerDesignationHandle = mLawyerDesignation(cboDataField(Index).ListIndex + 1)(DatabaseName.LAWYERDESIGNATION_KEY)
      End If
  End Select
  
  Dirty = True
End Sub





Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub chkNewLawyer_Click()
  SSTab1.TabVisible(TAB_LAWYER_INDEX) = chkNewLawyer.Value
  
  SetupEntity
  
  If chkNewLawyer.Value = vbChecked Then
    SSTab1.Tab = TAB_LAWYER_INDEX
    mskDataField(MSK_ENTITYLOCATOR_INDEX).SetFocus
    
' Cannot be used with the current application lawyer entry
'    If mOrganizationHandle <> vbNullString And _
'    Factory.FirmService.IsCurrentInsured(mOrganizationHandle) Then
'      MsgBox "The firm previously selected for this lawyer can not be set because the referenced firm is currently insured by WILMIC. The lawyer association must be set by application or endorsement"
'      mOrganizationHandle = Null
'      OrganizationRefresh
'    End If
      
  End If
End Sub

'
' Controls
'
Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdEntity_Click()
  'ver 2.50.00 (jjm) 2004 02 17 - call it to allow a select none
  'ver 2.50.00 (jjm) 2004 02 11 - added text for the search form caption
  'ver 2.00.0310 (jjm) wilmic
  If isLawyer Then
    Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True, , , , "Select a Firm for " & IIf(txtDataField(TXT_ENTITYNAME_INDEX).Text = vbNullString, "a lawyer", txtDataField(TXT_ENTITYNAME_INDEX).Text), True
  Else
    Dispatcher.ShowSearch DatabaseName.ORGANIZATION_TABLE, FormMgr, True, , , , "Select an Organization for " & IIf(txtDataField(TXT_ENTITYNAME_INDEX).Text = vbNullString, "a person", txtDataField(TXT_ENTITYNAME_INDEX).Text), True
  End If
End Sub


Private Sub cmdLocation_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, _
                        FormMgr, True, , _
                        IIf(mOrganizationHandle <> vbNullString, UrlName.LOCATIONSEARCH_ENTITYLOCATOR_INDEX, -1), _
                        IIf(mOrganizationHandle <> vbNullString, lblDataField(LBL_ENTITYLOCATOR_INDEX).Caption, vbNullString), _
                        "Search for a location for " & lblDisplayName.Caption, Not isCI
End Sub

Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
   Set mPersonDataset = New PersonDataset
   Set mPersonDataset.Session = LawSession
      
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  ' Custom Menu Shortcut Adjustments
  Me.KeyPreview = True
  
  mnuFileExit.Caption = mnuFileExit.Caption + Chr(9) + "Alt+F4"
  mnuViewPrevious.Caption = mnuViewPrevious.Caption + Chr(9) + "Ctrl+<"
  mnuViewNext.Caption = mnuViewNext.Caption + Chr(9) + "Ctrl+>"

  FormMgr.Load Me
  
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  Set mAttribute = Factory.AttributeService.GetAttributesCollection(DatabaseName.PERSON_TABLE)
  FormUtil.ComboBoxLoad mAttribute, cboAttribute, "attributeName"
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  
  Set mContactType = Factory.ContactTypeService.NameCollection()
  FormUtil.ComboBoxLoad mContactType, cboContactType, "contactTypeName"
  
  'Mantis 2733 - load LawyerDesignationList from data
  Dim designationData As Dataset
  Set designationData = NewGenericDataset(DatabaseName.LAWYERDESIGNATION_TABLE)
  designationData.DataProvider.Column = DatabaseName.LAWYERDESIGNATION_KEY & ", " & DatabaseName.LAWYERDESIGNATIONNAME_FIELD
  designationData.DataProvider.Sort = DatabaseName.LAWYERDESIGNATIONNAME_FIELD
  designationData.DataProvider.OpenRecordset
  Set mLawyerDesignation = designationData.DataProvider.collection
  FormUtil.ComboBoxLoad mLawyerDesignation, cboDataField(CBO_DESIGNATION_INDEX), DatabaseName.LAWYERDESIGNATIONNAME_FIELD
  
  FormResize
  
  FormRefresh
  
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub


Private Sub lvAttribute_Click()
  txtDataField(7).Text = lvAttribute.SelectedItem.Key
End Sub



Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0
      If mLocationHandle = vbNullString Then
        MsgBox "No default location has yet to be assigned to this person."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmLocationEdit, mLocationHandle, FormMgr, , DatabaseName.LOCATION_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub


Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmPersonOpen, mPersonHandle, FormMgr
    Case 2
      FormUtil.ShowByPrimaryKey New frmLocationOpen, mLocationHandle, FormMgr
    Case 3
      FormUtil.ShowByPrimaryKey New frmLawyerOpen, mLawyerHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  Select Case SSTab1.Tab
    Case 0
      FormUtil.ShowProperties DatabaseName.PERSON_TABLE, mPersonHandle
    Case 1
      FormUtil.ShowProperties DatabaseName.LAWYER_TABLE, mLawyerHandle
  End Select
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.PERSON_REF, DatabaseName.PERSON_TABLE, mPersonHandle, lblDisplayName.Caption
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub


Private Sub mnuFileNew_Click(Index As Integer)
  Select Case Index
    Case 0
      If Not Save(True) Then Exit Sub
      mNew = True
      mOrganizationHandle = vbNullString
      FormRefresh
    Case 1 'This if the menu separator
    Case 2
      'save if a person handle does not exist
      If (mPersonHandle = vbNullString) Then
        If (Not (Save)) Then
          MsgBox "The person record must be saved before entering a new location record for the person."
        End If
      End If
      FormUtil.ShowNew New frmLocationEdit, FormMgr, mPersonHandle, DatabaseName.LOCATION_TABLE & "Edit"
    Case 3
      FormUtil.ShowNew New frmOrganizationEdit, FormMgr, , DatabaseName.ORGANIZATION_TABLE & "New"
  End Select
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.PERSON_TABLE, FormMgr
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub

Private Sub mskDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub mskDataField_GotFocus(Index As Integer)
  Select Case Index
    Case 0
      ProcessLocator
  End Select
End Sub

Private Sub mskYearAdmitted_Change()
  'ver 2.10.0402 (jjm)
  Dirty = True
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
  
  'ver 2.50.00 (jjm)
  If SSTab1.Tab = 1 Then
    ProcessLocator
  End If
  
  'ver 2.30.0708 (jjm)
  If SSTab1.Tab = 2 And mLawyerHandle <> vbNullString Then
    cmdEntity.Enabled = Not isCI
  End If
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtAttribute_GotFocus()
  FormUtil.EditTextSelect txtAttribute
End Sub

Private Sub txtAttribute_LostFocus()
  If txtDataField(7).Text = vbNullString Then
    If txtAttribute.Text <> vbNullString Then
      MsgBox "Must select an attribute first."
      txtAttribute.Text = vbNullString
    End If
    Exit Sub
  End If
  
  'Removes the value if it already exists in the collection
  'so it can be re-added
  If Contains(mAttributeValue, txtDataField(7).Text) Then
    If mAttributeValue(txtDataField(7).Text) = txtAttribute.Text Then
      Exit Sub
    End If
    mAttributeValue.Remove txtDataField(7).Text
  End If
  
  'If (txtAttribute.Text <> vbNullString) Then
    If mAttributeValue Is Nothing Then
      Set mAttributeValue = New collection
    End If
    mAttributeValue.Add txtAttribute.Text, txtDataField(7).Text
  'End If
  
  AttributeRefresh
  Dirty = True
End Sub


Private Sub txtDatafield_Change(Index As Integer)
  If Index < 3 Or Index = 5 Then
    'Update display name
    txtDataField(TXT_ENTITYNAME_INDEX).Text = txtDataField(TXT_FIRSTNAME_INDEX).Text & _
        IIf(txtDataField(TXT_MIDDLENAME_INDEX).Text = "", "", " " & txtDataField(TXT_MIDDLENAME_INDEX).Text) & _
        IIf(txtDataField(TXT_LASTNAME_INDEX).Text = "", "", " " & txtDataField(TXT_LASTNAME_INDEX).Text) & _
        IIf(txtDataField(TXT_SUFFIX_INDEX).Text = "", "", " " & txtDataField(TXT_SUFFIX_INDEX).Text)
    'Change Salutation
    If txtDataField(TXT_LASTNAME_INDEX) <> vbNullString Then
      txtDataField(TXT_SALUTATION_INDEX).Text = cboDataField(CBO_TITLE_INDEX).Text & " " & txtDataField(TXT_LASTNAME_INDEX).Text
    End If
  End If

  Select Case Index
    Case TXT_ENTITYNAME_INDEX
      'set the hidden lawyer name and display label
      lblDisplayName.Caption = txtDataField(Index)
    Case 7
      cboAttribute.ListIndex = CollectionFind(mAttribute, DatabaseName.ATTRIBUTE_KEY, txtDataField(Index).Text) - 1
    Case 8
      cboContactType.ListIndex = CollectionFind(mContactType, DatabaseName.CONTACTTYPE_KEY, txtDataField(Index).Text) - 1
  End Select
  Dirty = True
End Sub


Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtDataField_KeyPress(Index As Integer, KeyAscii As Integer)
  If Index < 3 Or Index = 5 Then
    'Automatically capitalize the first letter if the text is empty
    If Len(txtDataField(Index).Text) = 0 Then
      If KeyAscii >= Asc("a") And KeyAscii <= Asc("z") Then
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
      End If
      'If the text field is the middle initial, add a '.'
      'at the end but only for the first letter
      If Index = 1 Then
        txtDataField(Index).Text = txtDataField(Index).Text & "."
      End If
    End If
  End If
End Sub


Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtYearAdmitted_Change()
  Dirty = True
End Sub

Private Sub mskYearAdmitted_GotFocus()
  FormUtil.EditTextSelect mskYearAdmitted
End Sub
