VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form frmEventDocumentEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Event Document"
   ClientHeight    =   6585
   ClientLeft      =   525
   ClientTop       =   1275
   ClientWidth     =   8370
   Icon            =   "frmEventDocumentEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   Picture         =   "frmEventDocumentEdit.frx":000C
   ScaleHeight     =   6585
   ScaleWidth      =   8370
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6168
      Left            =   0
      ScaleHeight     =   6165
      ScaleWidth      =   8370
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   384
      Width           =   8364
      Begin VB.TextBox txtDataField 
         DataField       =   "eventName"
         DataMember      =   "Event"
         Height          =   288
         Index           =   1
         Left            =   1224
         TabIndex        =   74
         Top             =   108
         Width           =   3960
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   7080
         TabIndex        =   72
         Top             =   5748
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   336
         Left            =   5748
         TabIndex        =   71
         Top             =   5748
         Width           =   1236
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   5184
         Left            =   60
         TabIndex        =   75
         Top             =   456
         Width           =   8232
         _ExtentX        =   14526
         _ExtentY        =   9155
         _Version        =   393216
         Style           =   1
         TabsPerRow      =   4
         TabHeight       =   420
         TabCaption(0)   =   "&Document"
         TabPicture(0)   =   "frmEventDocumentEdit.frx":0C4E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblDataField(46)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblDataField(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblDataField(45)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblDataField(47)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblDataField(48)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblDataField(42)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblDataField(41)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblDataField(43)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblDataField(44)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel(15)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel(16)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblDataField(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "dtpDataField(0)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtDataField(14)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "cboEventTypeName"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "fraMerge"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "&Recipient"
         TabPicture(1)   =   "frmEventDocumentEdit.frx":0C6A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvEventRecipient"
         Tab(1).Control(1)=   "cmdAdd"
         Tab(1).Control(2)=   "cmdRemove"
         Tab(1).Control(3)=   "fraPerson"
         Tab(1).Control(4)=   "fraLocation"
         Tab(1).Control(5)=   "cmdNew"
         Tab(1).Control(6)=   "cboType"
         Tab(1).Control(7)=   "cmdMoveUp"
         Tab(1).Control(8)=   "cmdMoveDown"
         Tab(1).ControlCount=   9
         TabCaption(2)   =   "N&ote"
         TabPicture(2)   =   "frmEventDocumentEdit.frx":0C86
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "rtfDataField(0)"
         Tab(2).ControlCount=   1
         Begin RichTextLib.RichTextBox rtfDataField 
            Bindings        =   "frmEventDocumentEdit.frx":0CA2
            DataField       =   "eventNote"
            DataMember      =   "Event"
            Height          =   4668
            Index           =   0
            Left            =   -74916
            TabIndex        =   77
            Top             =   396
            Width           =   8040
            _ExtentX        =   14182
            _ExtentY        =   8229
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"frmEventDocumentEdit.frx":0CB2
         End
         Begin VB.CommandButton cmdMoveDown 
            Caption         =   "Move Do&wn"
            Height          =   288
            Left            =   -70392
            TabIndex        =   12
            Top             =   1596
            Width           =   1028
         End
         Begin VB.CommandButton cmdMoveUp 
            Caption         =   "Move &Up"
            Height          =   288
            Left            =   -69264
            TabIndex        =   13
            Top             =   1596
            Width           =   1028
         End
         Begin VB.ComboBox cboType 
            DataField       =   "recipientTypeId"
            Height          =   315
            ItemData        =   "frmEventDocumentEdit.frx":0D34
            Left            =   -74844
            List            =   "frmEventDocumentEdit.frx":0D41
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   1596
            Width           =   1128
         End
         Begin VB.Frame fraMerge 
            Caption         =   "Merge"
            Height          =   1596
            Left            =   96
            TabIndex        =   50
            Top             =   1848
            Width           =   6456
            Begin VB.TextBox txtDataField 
               DataField       =   "saveFileName"
               DataMember      =   "Event"
               Height          =   288
               Index           =   18
               Left            =   1848
               TabIndex        =   6
               Top             =   1116
               Width           =   4152
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "fileName"
               DataMember      =   "Event"
               Height          =   288
               Index           =   15
               Left            =   1152
               TabIndex        =   3
               Top             =   288
               Width           =   4848
            End
            Begin VB.CheckBox chkEnvelopeAll 
               Caption         =   "Envelope (all recipients)"
               DataField       =   "isEnvelope"
               DataMember      =   "Event"
               Height          =   336
               Left            =   3144
               TabIndex        =   5
               Top             =   732
               Width           =   2352
            End
            Begin VB.CheckBox chkDataField 
               Caption         =   "Modify"
               DataField       =   "isModify"
               DataMember      =   "Event"
               Height          =   336
               Index           =   0
               Left            =   2088
               TabIndex        =   4
               Top             =   744
               Width           =   900
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Save modifications to:"
               Height          =   192
               Index           =   8
               Left            =   108
               TabIndex        =   53
               Top             =   1140
               Width           =   1572
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "File name:"
               Height          =   192
               Index           =   7
               Left            =   108
               TabIndex        =   52
               Top             =   312
               Width           =   744
            End
         End
         Begin VB.ComboBox cboEventTypeName 
            Height          =   315
            Left            =   1212
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   936
            Width           =   3408
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventDesc"
            DataMember      =   "Event"
            Height          =   288
            Index           =   14
            Left            =   1212
            TabIndex        =   2
            Top             =   1296
            Width           =   4764
         End
         Begin VB.CommandButton cmdNew 
            Caption         =   "&New"
            Height          =   288
            Left            =   -68124
            TabIndex        =   10
            Top             =   1104
            Width           =   1028
         End
         Begin VB.Frame fraLocation 
            Caption         =   "Location"
            Height          =   2388
            Left            =   -74868
            TabIndex        =   39
            Top             =   2664
            Width           =   7908
            Begin VB.CheckBox chkEnvelope 
               Caption         =   "Envelope"
               DataField       =   "isEnvelope"
               DataMember      =   "EventRecipient"
               Height          =   300
               Left            =   5316
               TabIndex        =   26
               Top             =   564
               Width           =   1080
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "deliveryPointCode"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   6
               Left            =   4668
               TabIndex        =   25
               Top             =   1944
               Width           =   468
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "country"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   5
               Left            =   1056
               TabIndex        =   24
               Top             =   1920
               Width           =   1320
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "deliveryAddress"
               DataMember      =   "EventRecipient"
               Height          =   1308
               Index           =   2
               Left            =   5316
               MultiLine       =   -1  'True
               TabIndex        =   27
               Top             =   924
               Width           =   2472
            End
            Begin VB.CommandButton cmdLocation 
               Caption         =   "&Location"
               Height          =   288
               Left            =   6780
               TabIndex        =   28
               Top             =   168
               Width           =   1028
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "companyName"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   7
               Left            =   1056
               TabIndex        =   17
               Top             =   264
               Width           =   4104
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "addressLine1"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   8
               Left            =   1056
               TabIndex        =   18
               Top             =   588
               Width           =   4104
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "state"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   12
               Left            =   2844
               TabIndex        =   22
               Top             =   1584
               Width           =   492
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "zip"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   13
               Left            =   3816
               TabIndex        =   23
               Top             =   1572
               Width           =   1320
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "city"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   11
               Left            =   1056
               TabIndex        =   21
               Top             =   1584
               Width           =   1212
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "addressLine3"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   10
               Left            =   1056
               TabIndex        =   20
               Top             =   1248
               Width           =   4104
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "addressLine2"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   9
               Left            =   1056
               TabIndex        =   19
               Top             =   924
               Width           =   4104
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Delivery Point:"
               Height          =   192
               Index           =   3
               Left            =   3492
               TabIndex        =   46
               Top             =   1980
               Width           =   1032
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Country:"
               Height          =   192
               Index           =   4
               Left            =   120
               TabIndex        =   45
               Top             =   1956
               Width           =   576
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Company:"
               Height          =   192
               Left            =   120
               TabIndex        =   44
               Top             =   276
               Width           =   732
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Address:"
               Height          =   192
               Left            =   120
               TabIndex        =   43
               Top             =   660
               Width           =   648
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "City:"
               Height          =   192
               Index           =   1
               Left            =   120
               TabIndex        =   42
               Top             =   1632
               Width           =   300
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               Caption         =   "State:"
               Height          =   192
               Left            =   2352
               TabIndex        =   41
               Top             =   1632
               Width           =   408
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "Zip:"
               Height          =   192
               Left            =   3468
               TabIndex        =   40
               Top             =   1620
               Width           =   264
            End
         End
         Begin VB.Frame fraPerson 
            Caption         =   "Person"
            Height          =   624
            Left            =   -74856
            TabIndex        =   36
            Top             =   1980
            Width           =   7872
            Begin VB.CommandButton cmdPerson 
               Caption         =   "&Person"
               Height          =   288
               Left            =   6744
               TabIndex        =   16
               Top             =   216
               Width           =   1028
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "recipientName"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   3
               Left            =   852
               TabIndex        =   14
               Top             =   216
               Width           =   2292
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "recipientSalutation"
               DataMember      =   "EventRecipient"
               Height          =   288
               Index           =   4
               Left            =   4236
               TabIndex        =   15
               Top             =   216
               Width           =   1896
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Attention:"
               Height          =   192
               Index           =   1
               Left            =   108
               TabIndex        =   38
               Top             =   264
               Width           =   648
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Salutation:"
               Height          =   192
               Index           =   2
               Left            =   3324
               TabIndex        =   37
               Top             =   264
               Width           =   744
            End
         End
         Begin VB.CommandButton cmdRemove 
            Caption         =   "Re&move"
            Height          =   288
            Left            =   -68124
            TabIndex        =   9
            Top             =   756
            Width           =   1028
         End
         Begin VB.CommandButton cmdAdd 
            Caption         =   "&Add"
            Height          =   288
            Left            =   -68124
            TabIndex        =   8
            Top             =   408
            Width           =   1028
         End
         Begin MSComctlLib.ListView lvEventRecipient 
            Height          =   1140
            Left            =   -74856
            TabIndex        =   7
            Tag             =   "EventEventContact"
            Top             =   384
            Width           =   6624
            _ExtentX        =   11668
            _ExtentY        =   2011
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmEventDocumentEdit.frx":0D52
            DataField       =   "createDate"
            DataMember      =   "Event"
            Height          =   288
            Index           =   0
            Left            =   1212
            TabIndex        =   0
            Top             =   588
            Width           =   1332
            _ExtentX        =   2355
            _ExtentY        =   503
            _Version        =   393216
            Format          =   66715649
            CurrentDate     =   37336
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "eventTypeId"
            DataField       =   "eventTypeId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   0
            Left            =   4752
            TabIndex        =   76
            Top             =   984
            UseMnemonic     =   0   'False
            Width           =   912
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Modified:"
            Height          =   192
            Index           =   16
            Left            =   132
            TabIndex        =   70
            Top             =   3672
            Width           =   660
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Due:"
            Height          =   192
            Index           =   15
            Left            =   132
            TabIndex        =   69
            Top             =   4044
            Width           =   336
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Completed:"
            Height          =   192
            Index           =   14
            Left            =   132
            TabIndex        =   68
            Top             =   4404
            Width           =   828
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Voided:"
            Height          =   192
            Index           =   13
            Left            =   132
            TabIndex        =   67
            Top             =   4776
            Width           =   564
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "void"
            DataField       =   "voidDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   44
            Left            =   1320
            TabIndex        =   66
            Top             =   4776
            UseMnemonic     =   0   'False
            Width           =   312
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "complete"
            DataField       =   "completeDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   43
            Left            =   1320
            TabIndex        =   65
            Top             =   4404
            UseMnemonic     =   0   'False
            Width           =   672
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "modified"
            DataField       =   "modifyDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   41
            Left            =   1332
            TabIndex        =   64
            Top             =   3672
            UseMnemonic     =   0   'False
            Width           =   624
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "due"
            DataField       =   "dueDate"
            DataMember      =   "Event"
            Height          =   192
            Index           =   42
            Left            =   1320
            TabIndex        =   63
            Top             =   4044
            UseMnemonic     =   0   'False
            Width           =   276
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "void"
            DataField       =   "voidUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   48
            Left            =   3372
            TabIndex        =   62
            Top             =   4776
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   312
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "complete"
            DataField       =   "completeUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   47
            Left            =   3372
            TabIndex        =   61
            Top             =   4404
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   672
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "modified"
            DataField       =   "modifyUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   45
            Left            =   3372
            TabIndex        =   60
            Top             =   3672
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   624
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "created"
            DataField       =   "createUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   1
            Left            =   2724
            TabIndex        =   59
            Top             =   636
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   552
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "due"
            DataField       =   "dueUserId"
            DataMember      =   "Event"
            Height          =   192
            Index           =   46
            Left            =   3372
            TabIndex        =   58
            Top             =   4044
            UseMnemonic     =   0   'False
            Visible         =   0   'False
            Width           =   276
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Type:"
            Height          =   192
            Index           =   2
            Left            =   156
            TabIndex        =   49
            Top             =   984
            Width           =   420
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Date:"
            Height          =   192
            Index           =   0
            Left            =   144
            TabIndex        =   48
            Top             =   624
            Width           =   384
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Description:"
            Height          =   192
            Index           =   6
            Left            =   144
            TabIndex        =   47
            Top             =   1308
            Width           =   936
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   420
         Top             =   4440
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   984
         Top             =   4440
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.Label lblSource 
         Caption         =   "Dynamic"
         Height          =   228
         Left            =   168
         TabIndex        =   73
         Top             =   156
         Width           =   780
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "cmdEnter"
      Height          =   336
      Left            =   3816
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   684
      Width           =   1308
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   8364
      _ExtentX        =   14764
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   8370
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   8250
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   30
         Width           =   8250
         Begin VB.TextBox txtDataField 
            DataField       =   "folderId"
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            DataMember      =   "Event"
            Enabled         =   0   'False
            Height          =   288
            Index           =   17
            Left            =   6792
            TabIndex        =   51
            TabStop         =   0   'False
            Top             =   24
            Visible         =   0   'False
            Width           =   936
         End
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   35
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Modified:"
      Height          =   192
      Index           =   12
      Left            =   0
      TabIndex        =   57
      Top             =   369
      Width           =   660
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Due:"
      Height          =   192
      Index           =   11
      Left            =   0
      TabIndex        =   56
      Top             =   738
      Width           =   336
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Completed:"
      Height          =   192
      Index           =   10
      Left            =   0
      TabIndex        =   55
      Top             =   1107
      Width           =   828
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Voided:"
      Height          =   192
      Index           =   9
      Left            =   0
      TabIndex        =   54
      Top             =   1476
      Width           =   564
   End
   Begin VB.Label Label14 
      Caption         =   "Label14"
      Height          =   12
      Left            =   3252
      TabIndex        =   31
      Top             =   3096
      Width           =   972
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Event"
            Shortcut        =   ^N
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "Propert&ies"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator693 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings"
      End
      Begin VB.Menu mnuSeparator38121 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsEnvelope 
         Caption         =   "&Envelope..."
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "&Web Merge"
      End
      Begin VB.Menu mnuToolsResetFileName 
         Caption         =   "&Reset File Name"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator632 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmEventDocumentEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.70.00 (jjm) 2004 11 04 - contactName fix / hack
'ver 2.70.00 (jjm) 2004 09 14
'ver 2.50.00 (jjm) 2004 02 23
'ver 2.50.00 (jjm) 2004 02 12
'ver 1.90.0218 (jjm)
'
' frmUnderwritingContact
'
' This form provides the user interface to the ContactFirmDataset class.
'

'
' Future Development
'
' -Options Form that has Options for a Save and Add command button.

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

' Main Handles
Private mEventHandle As String
Private mFolderHandle As String

'Private mSource As String
Private mEventTypeRuleHandle As String
'mReferenceEventHandle stores the eventId of Entity (Person or Organization) or Instance (Incident or Claim)that owns the document
Private mReferenceEventHandle As String
Private mEventName As String

'Handles for the Referenced Organization, Person or Instance. Mainly used to pull additional information to
'setup the form.
'These are to be assoicated with referenced event (example: event for Organization or Instance) and
'Do not directly deal with the organizationId, personId, etc in the EventRecipient table
Private mBaseInstanceHandle As String
Private mBaseOrganizationHandle As String
Private mBasePersonHandle As String

'Cache claimNumber and incidentNumber fields. These will be used in the searches
Private mClaimNumber As String
Private mIncidentNumber As String

Private mNew As Boolean

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mLabelDirty As Boolean
Private mInternal As Boolean
Private mEventTypeHandle As String

' Contact dataset is used to get the underlying contact record
Private mEventRecipientDataset As Dataset
Private mEventRecipientCollection As Collection

'Used for populating the combo box
Private mEventTypes As Collection

Private SaveToPath As String
Private ApplicationFolder As String

' Form Manager
Private FormMgr As New FormManager

' Signal that a refresh is being called.
Private mRefresh As Boolean

' Sort Index for list item
Private SortIndex As Integer


'Constants used in this form
'Used to store the index for the eventTypeId lblDataField 'ver 2.50.00 (jjm) 2004 02 12
Private Const CHK_ISMODIFY_INDEX = 0

Private Const LBL_EVENTTYPEID_INDEX = 0

Private Const DTP_CREATEDATE_INDEX = 0

Private Const TXT_CONTACTNAME_INDEX = 3
Private Const TXT_DELIVERYADDRESS_INDEX = 2
Private Const TXT_EVENTNAME_INDEX = 1
Private Const TXT_FILENAME_INDEX = 15
Private Const TXT_FOLDERID_INDEX = 17
Private Const TXT_SAVEFILENAME_INDEX = 18

Private Const EVENT_RECIPIENT_DATAMEMBER = "EventRecipient"
Private Const EVENT_DATAMEMBER = "Event"

Private Const SORT_COLUMN = "~sort"


' ver 1.90.0218 (jjm)
' Store a local variable to hold the value of chkEnvelope.Enabled
' This value is used to determine insertion of date into
' lvEventRecipient. This value is unreliable due to the form being
' passed to formutil.TabControlEnable
Private mEnvelopeEnabled As Boolean

Public Property Get EventHandle() As String
  EventHandle = mEventHandle
End Property

'ver 2.50.00 (jjm) 2004 02 12
Public Property Get EventTypeHandle() As String
  EventTypeHandle = mEventTypeHandle
End Property

'ver 2.50.00 (jjm) 2004 02 12
Public Property Let EventTypeHandle(vData As String)
  mEventTypeHandle = vData
  'Set the label
  lblDataField(LBL_EVENTTYPEID_INDEX).Caption = mEventTypeHandle
  'Set the Combo for the Event Type selection
  
  'Use the eventTypeId to get the event name
  If Not mEventTypes Is Nothing Then
    Dim TypeName As String
    Dim I As Integer
    For I = 1 To mEventTypes.Count
      If mEventTypeHandle = mEventTypes(I)(DatabaseName.EVENTTYPE_KEY) Then
        TypeName = mEventTypes(I)(DatabaseName.EVENTTYPENAME_FIELD)
        Exit For
      End If
    Next I
    
    'If the combo box is not set set it now. This should only be called from a FormRefresh
    If TypeName <> cboEventTypeName.Text Then
      Dim n As Integer
      For n = 0 To cboEventTypeName.ListCount - 1
        If cboEventTypeName.List(n) = TypeName Then
          cboEventTypeName.ListIndex = n
          Exit For
        End If
      Next n
    End If
  End If
End Property

Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Show
  Refresh
  Select Case fInterface.Message
    Case fimNew
      'Code not to get caught up in some other forms unload
      Select Case fInterface.Key
        Case DatabaseName.CONTACT_TABLE
          FormUtil.ShowNew New frmContactEdit, FormMgr, , DatabaseName.CONTACT_TABLE & "NewSelect"
        Case DatabaseName.PERSON_TABLE
          Dim fPerson As New frmPersonEdit
          FormMgr.Add fPerson, DatabaseName.PERSON_TABLE & "NewSelect"
          fPerson.ShowNew
        Case DatabaseName.LOCATION_TABLE
          FormUtil.ShowNew New frmLocationEdit, FormMgr, , DatabaseName.LOCATION_TABLE & "NewSelect"
      End Select
      
    Case fimSelect
      Select Case fInterface.Key
        Case DatabaseName.CONTACT_TABLE
          EventRecipientAddContact ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.CONTACT_KEY)
        Case DatabaseName.LOCATION_TABLE
          EventRecipientLoadLocation ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
        Case DatabaseName.PERSON_TABLE
          EventRecipientAddPerson ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.PERSON_KEY)
      End Select
      
    Case fimSave
      Select Case fInterface.Key
        'code for new person, location and contact
        Case DatabaseName.CONTACT_TABLE & "NewSelect"
          EventRecipientAddContact fInterface.Child.ContactHandle
        Case DatabaseName.PERSON_TABLE & "NewSelect"
          EventRecipientAddPerson fInterface.Child.PersonHandle
        Case DatabaseName.LOCATION_TABLE & "NewSelect"
          EventRecipientLoadLocation fInterface.Child.LocationHandle
      End Select
  End Select
  
  ButtonRefresh

End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
End Sub


Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  Dim mDatafields As New Collection
     
  pointer.pointer = vbHourglass
  
  ' Load all data fields with nothing
  ClearDataField txtDataField
  
  If (mNew) Then
    Caption = "New Document"
    txtDataField(TXT_EVENTNAME_INDEX).Text = mEventName
    If txtDataField(TXT_FOLDERID_INDEX).Text = vbNullString Then
      txtDataField(TXT_FOLDERID_INDEX).Text = mFolderHandle
    End If
    ClearDataField lblDataField, EVENT_DATAMEMBER
    dtpDataField(DTP_CREATEDATE_INDEX).Value = Today
  Else
    Set mDatafields = Factory.EventService.Find(mEventHandle)
    Caption = "Edit Document (" & mEventHandle & ")"
    
    SetFields txtDataField, mDatafields, EVENT_DATAMEMBER
    SetFields dtpDataField, mDatafields, EVENT_DATAMEMBER
    SetFields chkDatafield, mDatafields, EVENT_DATAMEMBER
    SetFields rtfDataField, mDatafields, EVENT_DATAMEMBER
       
    'Create Date format and set date format to event date field labels (ex: modifyDate, etc..)
    Dim fmt As New StdDataFormat, I As Integer
    fmt.Format = LocaleLongDate & ", " & LocaleTimeFormat
    For I = 41 To 44
      Set lblDataField(I).DataFormat = fmt
    Next I
    
    SetFields lblDataField, mDatafields, EVENT_DATAMEMBER
    
    'Set status user Ids (ex: modifyUserId, voidUserId, etc...)
    '(not for sure why this is not do with set data fields)
    For I = 41 To 44
      lblDataField(I).Caption = lblDataField(I).Caption & _
        IIf(lblDataField(I + 4).Caption <> vbNullString, " (" & lblDataField(I + 4) & ")", "")
    Next I
    
    mEventTypeRuleHandle = GetDataFromCollection(mDatafields, DatabaseName.EVENTTYPERULEID_FIELD)

    'MsgBox "ToDo: need to determine how to get Person, Organization, and Instance handles here"
    
             
    ' Get Detail Info
    EventRecipientRefresh
    EventRecipientResync
  End If
  
  'Configure sort key for recipient list
  Dim c As Integer
  For c = 1 To lvEventRecipient.ColumnHeaders.Count
    If (lvEventRecipient.ColumnHeaders.item(c).Key = SORT_COLUMN) Then
      lvEventRecipient.SortKey = c - 1
    End If
  Next c
  
  
  '
  '  NEED TO CONVERT MSOURCE
  '

  ' Setup mSource
  Dim mSource As String
  Select Case mEventTypeRuleHandle
    Case DatabaseName.ENDORSEMENT_TABLE
      mSource = DatabaseName.ENDORSEMENT_TABLE
    Case DatabaseName.FIRM_DOCUMENT
      mSource = DatabaseName.FIRM_TABLE
    Case DatabaseName.CLAIM_DOCUMENT
      mSource = DatabaseName.CLAIM_TABLE
    Case DatabaseName.INCIDENTDOCUMENT_RULE
      mSource = DatabaseName.INCIDENT_TABLE
    Case DatabaseName.DOCUMENT_RULE
      mSource = DatabaseName.PERSON_TABLE
    Case Else
      mSource = vbNullString
  End Select
  
  'Sets label depending on source
  Select Case mSource
    Case DatabaseName.FIRM_TABLE
      lblSource.Caption = "Firm name:"
    Case DatabaseName.CLAIM_TABLE
      lblSource.Caption = "Claim name:"
    Case DatabaseName.INCIDENT_TABLE
      lblSource.Caption = "Insured:"
    Case Else
      lblSource.Caption = "Name:"
  End Select

  
  ' After the intial conversion the mSource was being used
  ' to set the label. For now the label will be set to be Name
  ' lblSource.Caption = "Name:"
  
  If mBaseInstanceHandle <> vbNullString Then
    mClaimNumber = GetDataFromCollection(Factory.ClaimService.Find(mBaseInstanceHandle, DatabaseName.CLAIMNUMBER_FIELD), DatabaseName.CLAIMNUMBER_FIELD)
    mIncidentNumber = GetDataFromCollection(Factory.IncidentService.Find(mBaseInstanceHandle, DatabaseName.INCIDENTNUMBER_FIELD), DatabaseName.INCIDENTNUMBER_FIELD)
  End If
  
  Dim tempEventTypeId As String
  tempEventTypeId = GetDataFromCollection(mDatafields, DatabaseName.EVENTTYPE_KEY)
  
  'load combo boxes and filter by the Event Rule
  If mEventTypeRuleHandle <> vbNullString Then
    Set mEventTypes = Factory.EventTypeService.GroupCollection(mEventTypeRuleHandle)
  ElseIf tempEventTypeId <> vbNullString Then
    Set mEventTypes = New Collection
    mEventTypes.Add Factory.EventTypeService.Find(tempEventTypeId, DatabaseName.EVENTTYPE_KEY & "," & DatabaseName.EVENTTYPENAME_FIELD)
  End If
  
  If Not mEventTypes Is Nothing Then
    For I = 1 To mEventTypes.Count
      cboEventTypeName.AddItem GetDataFromCollection(mEventTypes.item(I), (DatabaseName.EVENTTYPENAME_FIELD))
    Next I
  Else
    MsgBox "Error loading combobox with """ & mEventTypeRuleHandle & """ event rule types.", vbExclamation
  End If
  
  'ver 2.50.00 (jjm) 2004 02 12
  'This needs to be set after the combo box is loaded
  If (mNew) Then
    FormUtil.GetComboDefault cboEventTypeName
  Else
    EventTypeHandle = tempEventTypeId
  End If
 
  ' Refresh navigation controls
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  lblLabel(8).Visible = (chkDatafield(CHK_ISMODIFY_INDEX).Value = 1)
  txtDataField(TXT_SAVEFILENAME_INDEX).Visible = (chkDatafield(0).Value = 1)
  
  'passes the input for address line 3, this is a hack to trigger the address display update
  txtDataField_Validate 10, False
  txtDatafield_Change 0
  ButtonRefresh
  
  
  Dirty = False
End Sub
 
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  mEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY)
End Property

Public Function EnvelopeAllValue() As Integer
  Dim EnvAll As Integer, item As ListItem
  EnvAll = -1
  For Each item In lvEventRecipient.ListItems
    If GetBoolean(ListItemText(item, "isEnvelope")) Then
      If EnvAll = vbUnchecked Then
        EnvelopeAllValue = vbGrayed
        Exit Function
      End If
      EnvAll = vbChecked
    Else
      If EnvAll = vbChecked Then
        EnvelopeAllValue = vbGrayed
        Exit Function
      End If
      EnvAll = vbUnchecked
    End If
  Next item
  
  EnvelopeAllValue = IIf(EnvAll = -1, vbUnchecked, EnvAll)
End Function

Private Sub EventRecipientAddContact(ContactHandle As String)
  'Contact and Location collections are used to get information about a new contact
  Dim ContactCollection As Collection
  Dim LocationCollection As Collection
  Dim EntityCollection As Collection
  Dim mLocationHandle As String
  Dim mPersonHandle As String

  Set ContactCollection = Factory.ContactService.Find(ContactHandle)
  mLocationHandle = GetDataFromCollection(ContactCollection, DatabaseName.LOCATION_KEY, True)
  mPersonHandle = GetDataFromCollection(ContactCollection, DatabaseName.PERSON_KEY, True)
  
  Set LocationCollection = Factory.LocationService.Find(mLocationHandle)
  
  'Recipient Name - Pull the entityName from Entity for the Contact to populate receipientName
  Dim entityName As String
  Set EntityCollection = Factory.EntityService.Find(mPersonHandle, DatabaseName.ENTITYNAME_FIELD)
  entityName = GetDataFromCollection(EntityCollection, DatabaseName.ENTITYNAME_FIELD, True)
  
  'Company Name - Pull the entityName for the Location to populate companyName
  Dim CompanyName As String
  Dim locationEntityHandle As String
  locationEntityHandle = GetDataFromCollection(LocationCollection, DatabaseName.MAINENTITYID_FIELD, True)
  CompanyName = DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, DatabaseName.ENTITY_KEY, _
                                   locationEntityHandle, DatabaseName.ENTITYNAME_FIELD)
  
  ' Get a new item and set
  Dim item As ListItem
  Set item = EventRecipientNew
  ListViewModule.SetItemCollection item, ContactCollection
  ListViewModule.SetItemCollection item, LocationCollection
  'Manually set recipientName and recipientSalutation
  SetItemText item, DatabaseName.RECIPIENTNAME_FIELD, entityName
  SetItemText item, DatabaseName.RECIPIENTSALUTATION_FIELD, GetDataFromCollection(ContactCollection, DatabaseName.CONTACTSALUTATION_FIELD)
  'Manually set company name
  ListViewModule.SetItemText lvEventRecipient.SelectedItem, DatabaseName.COMPANYNAME_FIELD, CompanyName
  
  EventRecipientResync
    
End Sub

Private Sub EventRecipientAddPerson(PersonHandle As String)
On Error GoTo Exception

  Dim EntityCollection As Collection
  Dim PersonCollection As Collection
  Dim LocationCollection As Collection
  Dim LocationHandle As String

  Set EntityCollection = Factory.EntityService.Find(PersonHandle, DatabaseName.ENTITYNAME_FIELD & ", " & DatabaseName.MAINLOCATIONID_FIELD)
  Set PersonCollection = Factory.PersonService.Find(PersonHandle, DatabaseName.SALUTATION_FIELD)

  LocationHandle = GetDataFromCollection(EntityCollection, DatabaseName.MAINLOCATIONID_FIELD)
  Set LocationCollection = Factory.LocationService.Find(LocationHandle)

  Dim mItem As ListItem
  Set mItem = lvEventRecipient.SelectedItem

  'Set person data
  ListViewModule.SetItemCollection lvEventRecipient.SelectedItem, PersonCollection
  'Due to field name differences, the following fields have to be manually set
  SetItemText mItem, DatabaseName.PERSON_KEY, PersonHandle 'Mantis 2528 - personId was not being set in the EventRecipient record
  SetItemText mItem, DatabaseName.RECIPIENTNAME_FIELD, EntityCollection("entityName")
  SetItemText mItem, DatabaseName.RECIPIENTSALUTATION_FIELD, PersonCollection("salutation")
  'Mantis 2554 - set company name
  Dim CompanyName As String
  CompanyName = DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, DatabaseName.ENTITY_KEY, _
                                    GetDataFromCollection(LocationCollection, DatabaseName.MAINENTITYID_FIELD, True), _
                                    DatabaseName.ENTITYNAME_FIELD)
  SetItemText mItem, DatabaseName.COMPANYNAME_FIELD, CompanyName
  'Set location data
  ListViewModule.SetItemCollection mItem, LocationCollection
  EventRecipientResync
  
  'Bug #2004 - added exit sub. This was missing when I added the error code which was added to help debug a mystery crash
  Exit Sub

Exception:
  MsgBox "Error: " & Err.Number & vbCrLf & Err.Description & vbCrLf & Err.Source, vbCritical
End Sub

Private Sub EventRecipientLoadLocation(LocationHandle As String)
  Dim LocationData As Collection
  Set LocationData = Factory.LocationService.Find(LocationHandle)
  ListViewModule.SetItemCollection lvEventRecipient.SelectedItem, LocationData
  'set company name. Get the entityName value from the Entity table using the Location.mainEntityId value.
  Dim entityName As String
  Dim entityhandle As String
  entityhandle = GetDataFromCollection(LocationData, DatabaseName.MAINENTITYID_FIELD, True)
  entityName = DataUtil.FindHandle(DatabaseName.ENTITY_TABLE, DatabaseName.ENTITY_KEY, _
                                   entityhandle, DatabaseName.ENTITYNAME_FIELD)
  ListViewModule.SetItemText lvEventRecipient.SelectedItem, DatabaseName.COMPANYNAME_FIELD, entityName
  'Sync the view with the display fields
  EventRecipientResync
End Sub

Private Function EventRecipientNew() As ListItem
  Dim item As ListItem
  Set item = ListViewModule.AddListItem(lvEventRecipient)
  'MsgBox "Debug: Added an item to the list. Currently contains: " & lvEventRecipient.ListItems.Count
  item.Selected = True
  If chkEnvelopeAll Then
    item.ListSubItems("isEnvelope") = DB_TRUE_VALUE
  End If
  SetItemText item, SORT_COLUMN, "Z" & Format(SortIndex, "000")
  SortIndex = SortIndex + 1
  Set EventRecipientNew = item
End Function


'
' Refresh displayName and salutation from the Location table.
'
Private Sub EventRecipientRefresh()
  lvEventRecipient.Sorted = False
  
  mEventRecipientDataset.DataProvider.filter = DatabaseName.EVENT_KEY & " = '" & mEventHandle & "'"
  mEventRecipientDataset.DataProvider.Sort = DatabaseName.RECIPIENTSEQ_FIELD
  mEventRecipientDataset.DataProvider.OpenRecordset
  mEventRecipientDataset.DataProvider.SetListView lvEventRecipient, False
  
  SetListViewSortKey
  
  Set mEventRecipientCollection = mEventRecipientDataset.DataProvider.Collection
  If Not mEventRecipientCollection Is Nothing Then
    SetFields txtDataField, mEventRecipientCollection(1), EVENT_RECIPIENT_DATAMEMBER
  End If
  lvEventRecipient.SortKey = 1
  lvEventRecipient.Sorted = True
  ButtonRefresh
End Sub


'
' Sync the txtDatafield controls with the lvEventRecipient list items. This is called
' when a recpient in the list is selected. In this case the list view is used for the
' source data.
'
Private Sub EventRecipientResync()
  Dim Envelope As Boolean, FieldCollection As Collection
    
  Set FieldCollection = ListViewModule.ListItemCollection(lvEventRecipient.ColumnHeaders, lvEventRecipient.SelectedItem)
  SetFields txtDataField, FieldCollection, EVENT_RECIPIENT_DATAMEMBER
    
  Envelope = GetDataFromCollection(FieldCollection, DatabaseName.ISENVELOPE_FIELD) <> vbNullString
  chkEnvelope.Value = IIf(Envelope, 1, 0)
  txtDataField(TXT_DELIVERYADDRESS_INDEX).Enabled = Envelope
    
  If FieldCollection("recipientTypeId") = vbNullString Then
    SetItemText lvEventRecipient.SelectedItem, "recipientTypeId", "To"
    cboType.Text = "To"
  Else
    cboType.Text = GetDataFromCollection(FieldCollection, "recipientTypeId", True)
  End If
  
  'passes the input for address line 3, this is a hack to trigger the address display update
  txtDataField_Validate 10, False
  
  'Set the Delivery Address 'ver 2.50.00 (jjm) 2004 02 23
  If Envelope Then
    ListViewModule.SetItemText lvEventRecipient.SelectedItem, txtDataField(TXT_DELIVERYADDRESS_INDEX).DataField, txtDataField(TXT_DELIVERYADDRESS_INDEX).Text
  End If
   
  If lvEventRecipient.ListItems.Count > 0 Then
    cmdPerson.Enabled = (ListItemText(lvEventRecipient.SelectedItem, DatabaseName.CONTACT_KEY) = vbNullString)
    cmdLocation.Enabled = (ListItemText(lvEventRecipient.SelectedItem, DatabaseName.CONTACT_KEY) = vbNullString)
  Else
    cmdPerson.Enabled = False
    cmdLocation.Enabled = False
  End If

End Sub

'Sets the File name field according to the event type selected
Private Sub EventTypeRefresh(Optional ForceReset As Boolean)
  Dim EventType As Collection
  Set EventType = Factory.EventTypeService.Find(EventTypeHandle, DatabaseName.DOCUMENTFILENAME_FIELD)
  If Not EventType Is Nothing Then
    If txtDataField(15).Text = vbNullString Or ForceReset Then 'Or txtDataField(15).Text <> txtDataField(18).Text Then
      txtDataField(15).Text = EventType("documentFileName")
    End If
  End If
  
  chkDatafield(0).Value = IIf(Factory.EventService.IsEventTypeInGroup(EventTypeHandle, "DocumentModify"), 1, 0)
  chkEnvelopeAll.Value = IIf(Factory.EventService.IsEventTypeInGroup(EventTypeHandle, "DocumentEnvelope"), 1, 0)
  
  Dirty = True
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
    
  ' Start custom Code
  If EventTypeHandle = vbNullString Then
    Save = False
    MsgBox "You need to select an event type to save."
    Exit Function
  End If
  
  Dim mDataControls As New Collection, data As Collection, EventRecipientHandle As String
  AddTextControl txtDataField, mDataControls, DatabaseName.EVENT_TABLE
  AddTextControl rtfDataField, mDataControls, DatabaseName.EVENT_TABLE
  AddValueControl dtpDataField, mDataControls, DatabaseName.EVENT_TABLE
  AddValueControl chkDatafield, mDataControls, DatabaseName.EVENT_TABLE
  AddCaptionControl lblDataField, mDataControls  ', DatabaseName.EVENT_TABLE
  
  Dim AttributeNames(2) As String
  Dim AttributeValues(2) As String
  AttributeNames(0) = DatabaseName.FILENAME_ATTRIBUTE
  AttributeValues(0) = GetDataFromCollection(mDataControls, DatabaseName.FILENAME_ATTRIBUTE)
  AttributeNames(1) = DatabaseName.ISMODIFY_ATTRIBUTE
  AttributeValues(1) = IIf(GetDataFromCollection(mDataControls, DatabaseName.ISMODIFY_ATTRIBUTE) = 1, DB_TRUE_VALUE, "")
  AttributeNames(2) = DatabaseName.SAVEFILENAME_ATTRIBUTE
  AttributeValues(2) = GetDataFromCollection(mDataControls, DatabaseName.SAVEFILENAME_ATTRIBUTE)
  
  
  'Load recipient table into arrays
  'NOTE: use an individual line for each declarations. This is a VB6 work around. Otherwise when the values
  '      get passed to the following screen strange things will happen.
  Dim ListCount As Integer
  ListCount = lvEventRecipient.ListItems.Count - 1
  
  If ListCount > -1 Then
    ReDim recipientTypeId(ListCount) As String
    ReDim recipientSeq(ListCount) As Integer
    ReDim contactId(ListCount) As String
    ReDim PersonId(ListCount) As String
    ReDim LocationId(ListCount) As String
    ReDim RecipientName(ListCount) As String
    ReDim RecipientSalutation(ListCount) As String
    ReDim CompanyName(ListCount) As String
    ReDim AddressLine1(ListCount) As String
    ReDim AddressLine2(ListCount) As String
    ReDim AddressLine3(ListCount) As String
    ReDim City(ListCount) As String
    ReDim State(ListCount) As String
    ReDim Zip(ListCount) As String
    ReDim Country(ListCount) As String
    ReDim CarrierRoute(ListCount) As String
    ReDim DeliveryPointCode(ListCount) As String
    ReDim DeliveryAddress(ListCount) As String
    ReDim isEnvelope(ListCount) As Boolean
    
    Dim item As ListItem
    Dim I As Integer
    Dim order As Integer
    'For Each item In lvEventRecipient.ListItems
    For I = 0 To ListCount
      'Check to see if the recipient was a read or write
      Set data = ListViewModule.ListItemCollection(lvEventRecipient.ColumnHeaders, lvEventRecipient.ListItems(I + 1))
      recipientTypeId(I) = GetDataFromCollection(data, DatabaseName.RECIPIENTTYPEID_FIELD)
      recipientSeq(I) = order
      contactId(I) = GetDataFromCollection(data, DatabaseName.CONTACT_KEY)
      PersonId(I) = GetDataFromCollection(data, DatabaseName.PERSON_KEY)
      LocationId(I) = GetDataFromCollection(data, DatabaseName.LOCATION_KEY)
      RecipientName(I) = GetDataFromCollection(data, DatabaseName.RECIPIENTNAME_FIELD)
      RecipientSalutation(I) = GetDataFromCollection(data, DatabaseName.RECIPIENTSALUTATION_FIELD)
      CompanyName(I) = GetDataFromCollection(data, DatabaseName.COMPANYNAME_FIELD)
      AddressLine1(I) = GetDataFromCollection(data, DatabaseName.ADDRESSLINE1_FIELD)
      AddressLine2(I) = GetDataFromCollection(data, DatabaseName.ADDRESSLINE2_FIELD)
      AddressLine3(I) = GetDataFromCollection(data, DatabaseName.ADDRESSLINE3_FIELD)
      City(I) = GetDataFromCollection(data, DatabaseName.CITY_FIELD)
      State(I) = GetDataFromCollection(data, DatabaseName.STATE_FIELD)
      Zip(I) = GetDataFromCollection(data, DatabaseName.ZIP_FIELD)
      Country(I) = GetDataFromCollection(data, DatabaseName.COUNTRY_FIELD)
      CarrierRoute(I) = GetDataFromCollection(data, DatabaseName.CARRIERROUTE_FIELD)
      DeliveryPointCode(I) = GetDataFromCollection(data, DatabaseName.DELIVERYPOINTCODE_FIELD)
      DeliveryAddress(I) = GetDataFromCollection(data, DatabaseName.DELIVERYADDRESS_FIELD)
      isEnvelope(I) = GetBoolean(GetDataFromCollection(data, DatabaseName.ISENVELOPE_FIELD))
      'increment the order
      order = order + 1
    Next I
  End If
  
  If mNew Then
    mEventHandle = Factory.EventService.CreateEventDocument( _
              mReferenceEventHandle, _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTTYPE_KEY), _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTNAME_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTDESC_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTNOTE_FIELD), _
              mEventTypeRuleHandle, _
              GetDataFromCollection(mDataControls, DatabaseName.FOLDER_KEY), _
              recipientTypeId, recipientSeq, _
              contactId, PersonId, LocationId, RecipientName, _
              RecipientSalutation, CompanyName, _
              AddressLine1, AddressLine2, AddressLine3, City, State, Zip, _
              Country, CarrierRoute, DeliveryPointCode, DeliveryAddress, isEnvelope, _
              AttributeNames, AttributeValues)
    Save = mEventHandle <> vbNullString
  Else
    Save = Factory.EventService.WriteEventDocument(mEventHandle, _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTTYPE_KEY), _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTNAME_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTDESC_FIELD), _
              GetDataFromCollection(mDataControls, DatabaseName.EVENTNOTE_FIELD), _
              mEventTypeRuleHandle, _
              GetDataFromCollection(mDataControls, DatabaseName.FOLDER_KEY), _
              recipientTypeId, recipientSeq, _
              contactId, PersonId, LocationId, RecipientName, _
              RecipientSalutation, CompanyName, _
              AddressLine1, AddressLine2, AddressLine3, City, State, Zip, _
              Country, CarrierRoute, DeliveryPointCode, DeliveryAddress, isEnvelope, _
              AttributeNames, AttributeValues)
  End If
  
  If Not Save Then
    Dirty = True
    Exit Function
  End If

  mNew = False

  
  Dirty = Not Save
End Function

Public Sub ShowByListView(View As ListView)
 If View.ListItems.Count = 0 Then
    MsgBox "There are no contacts to view or edit." ' To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(vEventHandle As String)
  mEventHandle = vEventHandle
  Show
End Sub

Public Sub ShowNew(Rule As String, _
                  Optional ReferenceEventHandle As String, _
                  Optional EventName As String, _
                  Optional PersonHandle As String, _
                  Optional organizationHandle As String, _
                  Optional InstanceHandle As String, _
                  Optional FolderHandle As String)
                  
  'This is the Rule containing the list of EventTypes to load
  'the screen by. Example values: Document, FirmDocument, ClaimDocument, etc..
  mEventTypeRuleHandle = Rule
  
  'This will be the eventId of the Entity or Instance record
  mReferenceEventHandle = ReferenceEventHandle
  
  mBaseInstanceHandle = InstanceHandle
  mBaseOrganizationHandle = organizationHandle
  mBasePersonHandle = PersonHandle
  
  mEventName = EventName
  mFolderHandle = FolderHandle
  mNew = True
  Show
  Refresh
  
  If mBaseOrganizationHandle <> vbNullString Then
    cmdAdd_Click
  End If
  If mBasePersonHandle <> vbNullString Then
    cmdNew_Click
    EventRecipientAddPerson mBasePersonHandle
  End If
  
End Sub


'
' Swap simply swaps the SortKey of the selected item with the
' desired index
'
Private Sub Swap(Index As Integer)
  lvEventRecipient.Sorted = False
  
  Dim Selected As ListItem
  Set Selected = lvEventRecipient.SelectedItem
  
  Dim item As ListItem
  Set item = lvEventRecipient.ListItems(Index)
  
  Dim SortKey As String
  SortKey = Selected.ListSubItems(SORT_COLUMN)
  Selected.ListSubItems(SORT_COLUMN) = item.ListSubItems(SORT_COLUMN)
  item.ListSubItems(SORT_COLUMN) = SortKey
  
  
  lvEventRecipient.Sorted = True
  lvEventRecipient.ListItems(Index).Selected = True
End Sub


Public Sub ButtonRefresh()
  Dim field As TextBox
  If lvEventRecipient.ListItems.Count < 1 Then
    cmdMoveDown.Enabled = False
    cmdMoveUp.Enabled = False
    cmdRemove.Enabled = False
    cmdPerson.Enabled = False
    ClearDataField txtDataField, EVENT_RECIPIENT_DATAMEMBER
    For Each field In txtDataField
      If field.DataMember = EVENT_RECIPIENT_DATAMEMBER Then
        field.Enabled = False
        field.BackColor = cmdSave.BackColor
      End If
    Next field
    chkEnvelope.Enabled = False
    ' ver 1.90.0218 (jjm)
    mEnvelopeEnabled = False
    
    cboType.Enabled = False
    Exit Sub
  End If
  
  With lvEventRecipient.SelectedItem
    cmdMoveUp.Enabled = (.Index > 1)
    cmdMoveDown.Enabled = (.Index < lvEventRecipient.ListItems.Count)
    cmdRemove.Enabled = True
    cmdPerson.Enabled = True
  End With
  
  For Each field In txtDataField
  If field.DataMember = EVENT_RECIPIENT_DATAMEMBER Then
    field.Enabled = True
    field.BackColor = txtDataField(TXT_EVENTNAME_INDEX).BackColor
  End If
  Next field
  
  chkEnvelope.Enabled = True
    
  ' ver 1.90.0218 (jjm)
  mEnvelopeEnabled = True
    
  cboType.Enabled = True
End Sub

' There is a "~sort" column in lvEventRecipient used for sorting. This column exists to To, CC, etc
' entries grouped together when the columns are sorted.
'
' Please note that if a refresh columns is created, a "~sort" column will manually have to be added
' to the settings ini file.
'
Private Sub SetListViewSortKey()
  Dim I As Integer
  SortIndex = 0
  
  Dim recType As String
  For I = 1 To lvEventRecipient.ListItems.Count
    recType = lvEventRecipient.ListItems(I).ListSubItems(DatabaseName.RECIPIENTTYPEID_FIELD)
    If recType = "CC" Then
      ListViewModule.SetItemText lvEventRecipient.ListItems(I), SORT_COLUMN, "Y" & Format(SortIndex, "000")
    ElseIf recType = "BCC" Then
      ListViewModule.SetItemText lvEventRecipient.ListItems(I), SORT_COLUMN, "Z" & Format(SortIndex, "000")
    Else 'To
      ListViewModule.SetItemText lvEventRecipient.ListItems(I), SORT_COLUMN, "SortIndex" & Format(SortIndex, "000")
    End If
    SortIndex = SortIndex + 1
  Next I
End Sub


Private Sub SearchByHandle(Index As Integer, Optional SearchText As String)
  Dispatcher.ShowSearch DatabaseName.CONTACT_TABLE, FormMgr, True, , Index, SearchText
End Sub


'
' End General Methods
'
'
'
' Begin Control Methods
'



Private Sub cboEventTypeName_Click()
  Dim I As Integer
  For I = 1 To mEventTypes.Count
    If mEventTypes(I)(DatabaseName.EVENTTYPENAME_FIELD) = cboEventTypeName.Text Then
      EventTypeHandle = mEventTypes(I)(DatabaseName.EVENTTYPE_KEY)
      Exit For
    End If
  Next I
  Dirty = True
End Sub

Private Sub cboEventTypeName_LostFocus()
  EventTypeRefresh
End Sub

Private Sub cboType_Validate(Cancel As Boolean)
  SetItemText lvEventRecipient.SelectedItem, cboType.DataField, cboType.Text
  Dirty = True
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
  
  lblLabel(8).Visible = (chkDatafield(CHK_ISMODIFY_INDEX).Value = 1)
  txtDataField(TXT_SAVEFILENAME_INDEX).Visible = (chkDatafield(CHK_ISMODIFY_INDEX).Value = 1)
  
  If chkDatafield(CHK_ISMODIFY_INDEX).Value = 1 And _
     txtDataField(TXT_SAVEFILENAME_INDEX).Text = vbNullString Then
     
    Dim FileName As String
    FileName = txtDataField(TXT_FILENAME_INDEX).Text
    If mNew Or FileName = vbNullString Or InStr(FileName, MergePath) = 0 Then
      txtDataField(TXT_SAVEFILENAME_INDEX).Text = MergePath & MergePrefix & Factory.AutoKeyService.GetDocumentKey & MergeSuffix
    Else
      ' correct for old data/missing data
      Factory.EventService.SetAttribute mEventHandle, "saveFileName", FileName
      txtDataField(TXT_SAVEFILENAME_INDEX).Text = FileName
    End If
  ElseIf chkDatafield(CHK_ISMODIFY_INDEX).Value = 0 And Not txtDataField(TXT_SAVEFILENAME_INDEX) = vbNullString Then
    txtDataField(TXT_SAVEFILENAME_INDEX).Text = vbNullString
  End If
End Sub

Private Sub chkEnvelope_Click()
 ' ver 1.90.0218 (jjm)
  If Not mEnvelopeEnabled Then
    Exit Sub
  End If
  
  txtDataField(TXT_DELIVERYADDRESS_INDEX).Enabled = (chkEnvelope.Value = 1)
  ListViewModule.SetItemText lvEventRecipient.SelectedItem, chkEnvelope.DataField, IIf(chkEnvelope.Value = 1, DB_TRUE_VALUE, vbNullString)
  If (chkEnvelope.Value = 1) Then
     ListViewModule.SetItemText lvEventRecipient.SelectedItem, txtDataField(TXT_DELIVERYADDRESS_INDEX).DataField, txtDataField(TXT_DELIVERYADDRESS_INDEX).Text
  End If
  
  'minternal prevents the chkEnvelopeAll_Click code from being fired
  mInternal = True
  chkEnvelopeAll.Value = EnvelopeAllValue
  mInternal = False
  Dirty = True
End Sub

Private Sub chkEnvelopeAll_Click()
  If mInternal Then
    Exit Sub
  End If
    
  Dim item As ListItem, strValue As String
  strValue = IIf(chkEnvelopeAll.Value = vbChecked, DB_TRUE_VALUE, vbNullString)
  For Each item In lvEventRecipient.ListItems
    SetItemText item, "isEnvelope", strValue
  Next item
  chkEnvelope.Value = chkEnvelopeAll.Value
  Dirty = True
End Sub

Private Sub cmdAdd_Click()
  Select Case mEventTypeRuleHandle
    Case DatabaseName.CLAIM_DOCUMENT:
        SearchByHandle UrlName.CONTACTSEARCH_CLAIMNUMBER_INDEX, mClaimNumber
        Exit Sub
    Case DatabaseName.FIRM_DOCUMENT:
        SearchByHandle UrlName.CONTACTSEARCH_ORGANIZATIONID_INDEX, mBaseOrganizationHandle
        Exit Sub
    Case DatabaseName.DOCUMENT_RULE
        SearchByHandle UrlName.CONTACTSEARCH_ORGANIZATIONID_INDEX, mBaseOrganizationHandle
        Exit Sub
    Case DatabaseName.INCIDENTDOCUMENT_RULE
        SearchByHandle UrlName.CONTACTSEARCH_INCIDENTNUMBER_INDEX, mIncidentNumber
        Exit Sub
  End Select
  Dispatcher.ShowSearch DatabaseName.CONTACT_TABLE, FormMgr, True
End Sub

Private Sub cmdCancel_Click()
  Dirty = False
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtDataField(16).Text = vbNullString
  txtDataField(16).SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub


'
' Load selection of location.
'
Private Sub cmdLocation_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr, True
End Sub

Private Sub cmdMoveDown_Click()
  Swap lvEventRecipient.SelectedItem.Index + 1
  ButtonRefresh
  lvEventRecipient.SetFocus
  Dirty = True
End Sub

Private Sub cmdMoveUp_Click()
  Swap lvEventRecipient.SelectedItem.Index - 1
  ButtonRefresh
  lvEventRecipient.SetFocus
  Dirty = True
End Sub

Private Sub cmdNew_Click()
  Dim item As ListItem
  Set item = EventRecipientNew
  EventRecipientResync
  ListViewModule.SetItemText item, "contactName", "New Recipient"
  ButtonRefresh
End Sub

'
' Load selection of person
'
Private Sub cmdPerson_Click()
  Dispatcher.ShowSearch DatabaseName.PERSON_TABLE, FormMgr, True
End Sub

Private Sub cmdRemove_Click()
  '
  ' Removes the item in lvEventRecipient
  '
  lvEventRecipient.ListItems.Remove lvEventRecipient.SelectedItem.Index
  lvEventRecipient.SetFocus
  If Not lvEventRecipient.ListItems.Count < 1 Then
    lvEventRecipient.SelectedItem.Selected = True
  End If
  ButtonRefresh
End Sub

Private Sub cmdSave_Click()
  If Save Then
    FormMgr.Message = fimSave
    FormUtil.SetComboDefault cboEventTypeName
    Unload Me
  End If
End Sub


Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_Initialize()
  Set mEventRecipientDataset = NewGenericDataset(EVENT_RECIPIENT_DATAMEMBER)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
'    Select Case KeyCode
'      Case vbKeyC
'        mnuFileNewContact_Click
'        KeyCode = 0
'    End Select
'  End If
  If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , Me.hWnd
  End If
End Sub

'
' Load Controls
'
Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  DeserializeColumnHeaders lvEventRecipient, lvEventRecipient.Tag
    
  FormRefresh
  FormResize
  TabControlEnable SSTab1
End Sub

'
' Terminate Used for memory audit
'
Private Sub Form_Terminate()
  'MsgBox "frmApplicationEdit.Terminate"
End Sub

'
' See if form can unload.
'
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = 1
  End If
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvEventRecipient_Click()
  EventRecipientResync
End Sub

Private Sub lvEventRecipient_DblClick()
  EventRecipientResync
End Sub

Private Sub lvEventRecipient_ItemClick(ByVal item As MSComctlLib.ListItem)
  ButtonRefresh
End Sub

Private Sub lvEventRecipient_KeyUp(KeyCode As Integer, Shift As Integer)
  EventRecipientResync
End Sub

Private Sub mnuEditMove_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowMove(txtDataField(TXT_FOLDERID_INDEX).Text) Then
    txtDataField(TXT_FOLDERID_INDEX).Text = fForm.FolderKey
  End If
  Unload fForm
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Save(True) Then
    mNew = True
    FormRefresh
'    cboDataField(0).SetFocus
  End If
End Sub


Private Sub mnuFileProperties_Click()
  FormUtil.ShowEventProperties mEventHandle
End Sub

Private Sub mnuFileSave_Click()
  If Save Then
    FormMgr.Message = fimSave
  End If
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsEnvelope_Click()
  FormUtil.PrintEnvelope txtDataField(TXT_DELIVERYADDRESS_INDEX).Text, txtDataField(6).Text
End Sub

Private Sub mnuToolsMerge_Click()
  If mNew Or mDirty Then
    MsgBox "Must save before merging.  Use Ctrl-S or the File/Save menu."
    Exit Sub
  End If
  
  Dim pointer As New WaitPointer, Handles(0) As String
  pointer.Default
  
  ' Print Envelopes
  Factory.EventRecipientService.PrintEnvelope mEventHandle
  
  ' Perform Document Merge
  Handles(0) = mEventHandle
  Factory.EventService.DocumentMerge Handles
  
  ' Refresh for changes to fileName and isEnvelope which may be modified.
  FormRefresh
End Sub

Private Sub mnuToolsResetFileName_Click()
  EventTypeRefresh True
End Sub

'For inital implementation simply copied the existing merge code.
Private Sub mnuToolsWebMerge_Click()
  If mNew Or mDirty Then
    MsgBox "Must save before merging.  Use Ctrl-S or the File/Save menu."
    Exit Sub
  End If
  
  Dim Handles(0) As String
    
  ' Perform Document Merge
  Handles(0) = mEventHandle
  Factory.EventService.WebMerge Handles
  
  ' Refresh for changes to fileName and isEnvelope which may be modified.
  FormRefresh
End Sub

Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvEventRecipient, mEventRecipientDataset.DataProvider, False
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvEventRecipient, lvEventRecipient.Tag
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub

Private Sub rtfDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  TabControlEnable SSTab1
  ButtonRefresh
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
  Select Case Index
'    Case 0
'      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, databasename.EVENTTYPE_KEY, txtDataField(Index).Text) - 1
    Case 2
       mLabelDirty = True
  End Select
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  Select Case Index
    Case 0
      cmdEnter.Enabled = False
    Case 16
      FormUtil.NoteGotFocus cmdSave, cmdEnter
  End Select
  If Not Index = 16 Then
    FormUtil.EditTextSelect txtDataField(Index)
  End If
End Sub

Private Sub txtDataField_LostFocus(Index As Integer)
  Select Case Index
    Case 0
      cmdEnter.Enabled = True
    Case 16
      FormUtil.NoteLostFocus cmdSave, cmdEnter
  End Select
End Sub

Private Sub txtDataField_Validate(Index As Integer, Cancel As Boolean)
  If txtDataField(Index).DataMember = EVENT_RECIPIENT_DATAMEMBER Then
  
    txtDataField(TXT_DELIVERYADDRESS_INDEX) = IIf(txtDataField(TXT_CONTACTNAME_INDEX) = vbNullString, vbNullString, Replace(UCase(txtDataField(TXT_CONTACTNAME_INDEX)), ".", vbNullString) & vbCrLf) & _
                         IIf(txtDataField(TXT_CONTACTNAME_INDEX) = txtDataField(7) Or txtDataField(7) = vbNullString, vbNullString, Replace(Replace(UCase(txtDataField(7).Text), ",", vbNullString), ".", vbNullString) & vbCrLf) & _
                         IIf(txtDataField(9) = vbNullString, vbNullString, (Replace(UCase(txtDataField(9)), ".", vbNullString) & vbCrLf)) & _
                         IIf(txtDataField(8) = vbNullString, vbNullString, (Replace(UCase(txtDataField(8)), ".", vbNullString) & vbCrLf)) & _
                         IIf(txtDataField(10) = vbNullString, vbNullString, (Replace(UCase(txtDataField(10)), ".", vbNullString) & vbCrLf)) & _
                         UCase(txtDataField(11) & " " & txtDataField(12) & "  " & txtDataField(13))
                         
    ListViewModule.SetItemText lvEventRecipient.SelectedItem, txtDataField(Index).DataField, txtDataField(Index).Text
  End If
  'If Index = 0 Then
  '  Cancel = Not DescriptionRefresh
  'End If
End Sub
