VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PersistenceManagerService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'ver 2.00.1216

Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "PersistenceManagerService"


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & LawSession.Session
End Sub

'
' Interface to server
'
Public Function ExecuteRuleSession(ClassName As String, _
    Key() As String, Action As String, _
    AttributeNames() As String, AttributeValues() As String, _
    ParameterNames() As String, ParameterValues() As String) As Boolean

On Error GoTo Exception

  Call Client.ExecuteRuleSession(ClassName, Key, Action, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  
  ExecuteRuleSession = True
  Exit Function
  
Exception:
  MsgBox "Fault: " & Client.FaultString, vbExclamation
  ExecuteRuleSession = False

End Function
