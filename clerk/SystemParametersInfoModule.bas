Attribute VB_Name = "SystemParametersInfoModule"
Option Explicit

Private Const LF_FACESIZE = 32
Private Const SPI_GETNONCLIENTMETRICS = 41

Private Type LOGFONT
    lfHeight As Long
    lfWidth As Long
    lfEscapement As Long
    lfOrientation As Long
    lfWeight As Long
    lfItalic As Byte
    lfUnderline As Byte
    lfStrikeOut As Byte
    lfCharSet As Byte
    lfOutPrecision As Byte
    lfClipPrecision As Byte
    lfQuality As Byte
    lfPitchAndFamily As Byte
    lfFaceName(1 To LF_FACESIZE) As Byte
End Type

Private Type NONCLIENTMETRICS
    cbSize As Long
    iBorderWidth As Long
    iScrollWidth As Long
    iScrollHeight As Long
    iCaptionWidth As Long
    iCaptionHeight As Long
    lfCaptionFont As LOGFONT
    iSMCaptionWidth As Long
    iSMCaptionHeight As Long
    lfSMCaptionFont As LOGFONT
    iMenuWidth As Long
    iMenuHeight As Long
    lfMenuFont As LOGFONT
    lfStatusFont As LOGFONT
    lfMessageFont As LOGFONT
End Type

Private Declare Function SystemParametersInfo Lib "user32" _
Alias "SystemParametersInfoA" (ByVal uAction As Long, _
ByVal uParam As Long, ByRef lpvParam As Any, _
ByVal fuWinIni As Long) As Long



Public Function MenuFontName()
Dim s As String
Dim I As Byte
Dim ncm As NONCLIENTMETRICS
Dim sdfont As StdFont
  
  ncm.cbSize = Len(ncm)
 
  If SystemParametersInfo(SPI_GETNONCLIENTMETRICS, ncm.cbSize, ncm, 0) Then
    s = StrConv(ncm.lfMenuFont.lfFaceName, vbUnicode)
    I = InStr(s, vbNullChar)
    If I > 0 Then s = Left(s, I - 1)
  End If
  MenuFontName = s
  
End Function
