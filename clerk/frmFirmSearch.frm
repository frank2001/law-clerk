VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFirmSearch 
   Caption         =   "Select Firm"
   ClientHeight    =   7035
   ClientLeft      =   2565
   ClientTop       =   2115
   ClientWidth     =   8415
   Icon            =   "frmFirmSearch.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7035
   ScaleWidth      =   8415
   Begin VB.PictureBox picSelectNone 
      BorderStyle     =   0  'None
      Height          =   456
      Left            =   384
      ScaleHeight     =   450
      ScaleWidth      =   1485
      TabIndex        =   48
      Top             =   5640
      Width           =   1488
      Begin VB.CommandButton cmdSelectNone 
         Caption         =   "Select None"
         Height          =   336
         Left            =   108
         TabIndex        =   49
         Top             =   48
         Width           =   1200
      End
   End
   Begin VB.PictureBox picButtons 
      BorderStyle     =   0  'None
      Height          =   456
      Left            =   1836
      ScaleHeight     =   450
      ScaleWidth      =   4200
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   5136
      Width           =   4200
      Begin VB.CommandButton cmdNew 
         Caption         =   "&New"
         Height          =   336
         Left            =   1416
         TabIndex        =   45
         Top             =   48
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2736
         TabIndex        =   46
         Top             =   48
         Width           =   1200
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Enabled         =   0   'False
         Height          =   336
         Left            =   96
         TabIndex        =   44
         Top             =   48
         Width           =   1200
      End
   End
   Begin VB.PictureBox picSelect 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   492
      Left            =   0
      ScaleHeight     =   492
      ScaleMode       =   0  'User
      ScaleWidth      =   6764.775
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   5025
      Width           =   8412
   End
   Begin VB.Timer tmrSearch 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   6732
      Top             =   5292
   End
   Begin VB.PictureBox picListView 
      Align           =   1  'Align Top
      BackColor       =   &H80000005&
      Height          =   1392
      Left            =   0
      ScaleHeight     =   1335
      ScaleWidth      =   8355
      TabIndex        =   27
      TabStop         =   0   'False
      Tag             =   "Firm"
      Top             =   3630
      Width           =   8412
      Begin VB.PictureBox picSearchStatus 
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         Height          =   240
         Left            =   1704
         ScaleHeight     =   240
         ScaleWidth      =   4995
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   4992
         Begin VB.Label lblSearchStatus 
            AutoSize        =   -1  'True
            BackColor       =   &H80000009&
            BackStyle       =   0  'Transparent
            Caption         =   "Search Status"
            Height          =   192
            Left            =   0
            TabIndex        =   31
            Top             =   0
            Width           =   996
         End
      End
      Begin MSComctlLib.ListView lvView 
         Height          =   4152
         Left            =   1704
         TabIndex        =   39
         Top             =   12
         Width           =   5856
         _ExtentX        =   10319
         _ExtentY        =   7329
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         _Version        =   393217
         Icons           =   "imlIcons"
         SmallIcons      =   "imlSmallIcons"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.Label lblListView 
         BackColor       =   &H80000005&
         Caption         =   "Double click or press Enter on an item to view or edit the information. "
         Height          =   1572
         Index           =   1
         Left            =   84
         TabIndex        =   29
         Top             =   720
         Width           =   1212
      End
      Begin VB.Label lblListView 
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "Table Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   84
         TabIndex        =   28
         Top             =   0
         Width           =   1524
         WordWrap        =   -1  'True
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   264
      Left            =   0
      TabIndex        =   24
      Top             =   6768
      Width           =   8412
      _ExtentX        =   14843
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7170
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "5/28/2009"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "2:45 PM"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   7344
      Top             =   5100
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":000C
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":011E
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":0230
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":0342
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":0454
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":0566
            Key             =   "View Large Icons"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":0678
            Key             =   "View Small Icons"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":078A
            Key             =   "View List"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":089C
            Key             =   "View Details"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":09AE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlIcons 
      Left            =   7176
      Top             =   5700
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":0D0A
            Key             =   "book"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlSmallIcons 
      Left            =   10836
      Top             =   8364
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   13
      ImageHeight     =   13
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":1024
            Key             =   "closed"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":1196
            Key             =   "cylinder"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":1308
            Key             =   "leaf"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":147A
            Key             =   "open"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":15EC
            Key             =   "smlBook"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFirmSearch.frx":189E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picSearchTab 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3264
      Left            =   0
      ScaleHeight     =   3270
      ScaleWidth      =   8415
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   360
      Width           =   8412
      Begin VB.PictureBox picSearchButtons 
         BorderStyle     =   0  'None
         Height          =   2244
         Left            =   6348
         ScaleHeight     =   2250
         ScaleWidth      =   1410
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   276
         Width           =   1404
         Begin VB.CommandButton cmdSearchNow 
            Caption         =   "&Search Now"
            Default         =   -1  'True
            Height          =   336
            Left            =   108
            TabIndex        =   36
            Top             =   36
            Width           =   1080
         End
         Begin VB.CommandButton cmdNewSearch 
            Caption         =   "New Search"
            Height          =   336
            Left            =   96
            TabIndex        =   37
            Top             =   444
            Width           =   1080
         End
      End
      Begin TabDlg.SSTab sstabSearch 
         Height          =   3108
         Left            =   84
         TabIndex        =   47
         Top             =   84
         Width           =   6276
         _ExtentX        =   11060
         _ExtentY        =   5477
         _Version        =   393216
         Style           =   1
         TabHeight       =   420
         TabCaption(0)   =   "Firm"
         TabPicture(0)   =   "frmFirmSearch.frx":1B50
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label3"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraWhereFirm"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtWords"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboIn"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Block of Business"
         TabPicture(1)   =   "frmFirmSearch.frx":1B6C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Label2"
         Tab(1).Control(1)=   "cboExpirationMonth"
         Tab(1).Control(2)=   "Frame1"
         Tab(1).Control(3)=   "frmUnderwritingStatus"
         Tab(1).ControlCount=   4
         TabCaption(2)   =   "Advanced"
         TabPicture(2)   =   "frmFirmSearch.frx":1B88
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Label4"
         Tab(2).Control(1)=   "Label5"
         Tab(2).Control(2)=   "chkLawyerCount"
         Tab(2).Control(3)=   "chkClaimCount"
         Tab(2).Control(4)=   "chkExpirationDate"
         Tab(2).Control(5)=   "chkCounty"
         Tab(2).Control(6)=   "chkCity"
         Tab(2).Control(7)=   "cboClaimCountOperator"
         Tab(2).Control(8)=   "cboLawyerCountOperator"
         Tab(2).Control(9)=   "dtpFromExpireDate"
         Tab(2).Control(10)=   "txtCity"
         Tab(2).Control(11)=   "txtClaimCount"
         Tab(2).Control(12)=   "txtLawyerCount"
         Tab(2).Control(13)=   "txtCounty"
         Tab(2).Control(14)=   "chkLawArea"
         Tab(2).Control(15)=   "cmdLawArea"
         Tab(2).Control(16)=   "dtpToExpireDate"
         Tab(2).Control(17)=   "chkSelectStatus"
         Tab(2).Control(18)=   "cmdUnderwritingStatus"
         Tab(2).Control(19)=   "chkMultiInsurerStatus"
         Tab(2).Control(20)=   "cmdInsurerStatus"
         Tab(2).Control(21)=   "txtInsurerName"
         Tab(2).Control(22)=   "chkInsurerName"
         Tab(2).ControlCount=   23
         Begin VB.Frame frmUnderwritingStatus 
            Caption         =   "&Underwriting Decision"
            Height          =   1584
            Left            =   -72708
            TabIndex        =   56
            Top             =   792
            Width           =   3252
            Begin VB.OptionButton optExcludeOnly 
               Caption         =   "Only"
               Height          =   192
               Index           =   1
               Left            =   132
               TabIndex        =   64
               Top             =   420
               Width           =   996
            End
            Begin VB.OptionButton optExcludeOnly 
               Caption         =   "Exclude"
               Height          =   192
               Index           =   0
               Left            =   120
               TabIndex        =   63
               Top             =   228
               Value           =   -1  'True
               Width           =   996
            End
            Begin VB.CheckBox chkUnderwritingStatus 
               Caption         =   "Denied Reissue (DR)"
               Height          =   192
               Index           =   2
               Left            =   1092
               TabIndex        =   62
               Tag             =   "DR"
               Top             =   852
               Width           =   2088
            End
            Begin VB.CheckBox chkUnderwritingStatus 
               Caption         =   "Denied Quotation (DQ)"
               Height          =   192
               Index           =   3
               Left            =   1092
               TabIndex        =   61
               Tag             =   "DQ"
               Top             =   1056
               Width           =   2088
            End
            Begin VB.CheckBox chkUnderwritingStatus 
               Caption         =   "Not Offering (NO)"
               Height          =   192
               Index           =   5
               Left            =   1092
               TabIndex        =   60
               Tag             =   "NO"
               Top             =   228
               Width           =   2088
            End
            Begin VB.CheckBox chkUnderwritingStatus 
               Caption         =   "Denied Application (DA)"
               Height          =   192
               Index           =   1
               Left            =   1092
               TabIndex        =   59
               Tag             =   "DA"
               Top             =   648
               Width           =   2088
            End
            Begin VB.CheckBox chkUnderwritingStatus 
               Caption         =   "Don't Solicit (DS)"
               Height          =   192
               Index           =   0
               Left            =   1092
               TabIndex        =   58
               Tag             =   "DS"
               Top             =   432
               Width           =   2088
            End
            Begin VB.CheckBox chkUnderwritingStatus 
               Caption         =   "Firm Dissolved (FD)"
               Height          =   192
               Index           =   4
               Left            =   1092
               TabIndex        =   57
               Tag             =   "FD"
               Top             =   1260
               Width           =   2088
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "&Insure Status Id"
            Height          =   1380
            Left            =   -74916
            TabIndex        =   52
            Top             =   792
            Width           =   2136
            Begin VB.CheckBox chkInsureStatus 
               Caption         =   "Currently Insured (CI)"
               Height          =   192
               Index           =   0
               Left            =   108
               TabIndex        =   55
               Tag             =   "CI"
               Top             =   252
               Width           =   1932
            End
            Begin VB.CheckBox chkInsureStatus 
               Caption         =   "Never Insured (NI)"
               Height          =   192
               Index           =   2
               Left            =   108
               TabIndex        =   54
               Tag             =   "NI"
               Top             =   672
               Width           =   1932
            End
            Begin VB.CheckBox chkInsureStatus 
               Caption         =   "Previously Insured (PI)"
               Height          =   192
               Index           =   1
               Left            =   108
               TabIndex        =   53
               Tag             =   "PI"
               Top             =   456
               Width           =   1932
            End
         End
         Begin VB.CheckBox chkInsurerName 
            Caption         =   "Insurer Name:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   51
            Top             =   2088
            Width           =   1392
         End
         Begin VB.TextBox txtInsurerName 
            Height          =   288
            Left            =   -72960
            TabIndex        =   50
            Top             =   2040
            Width           =   2808
         End
         Begin VB.CommandButton cmdInsurerStatus 
            Caption         =   "Insure Status"
            Height          =   288
            Left            =   -70308
            TabIndex        =   23
            Top             =   2364
            Width           =   1164
         End
         Begin VB.CheckBox chkMultiInsurerStatus 
            Caption         =   "Insure Status:"
            Height          =   192
            Left            =   -71748
            TabIndex        =   22
            Top             =   2412
            Width           =   1500
         End
         Begin VB.CommandButton cmdUnderwritingStatus 
            Caption         =   "Underwriting"
            Height          =   288
            Left            =   -72960
            TabIndex        =   21
            Top             =   2712
            Width           =   1080
         End
         Begin VB.CheckBox chkSelectStatus 
            Caption         =   "Underwriting Status:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   20
            Top             =   2748
            Width           =   1848
         End
         Begin MSComCtl2.DTPicker dtpToExpireDate 
            Height          =   288
            Left            =   -70992
            TabIndex        =   13
            Top             =   1044
            Width           =   1224
            _ExtentX        =   2170
            _ExtentY        =   503
            _Version        =   393216
            Format          =   145948673
            CurrentDate     =   37532
         End
         Begin VB.CommandButton cmdLawArea 
            Caption         =   "Law Area"
            Height          =   288
            Left            =   -72960
            TabIndex        =   19
            Top             =   2364
            Width           =   1080
         End
         Begin VB.CheckBox chkLawArea 
            Caption         =   "Law Area:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   18
            Top             =   2412
            Width           =   1452
         End
         Begin VB.ComboBox cboExpirationMonth 
            Height          =   315
            ItemData        =   "frmFirmSearch.frx":1BA4
            Left            =   -73500
            List            =   "frmFirmSearch.frx":1BD2
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   372
            Width           =   1704
         End
         Begin VB.TextBox txtCounty 
            Height          =   288
            Left            =   -72960
            TabIndex        =   15
            Top             =   1380
            Width           =   2808
         End
         Begin VB.TextBox txtLawyerCount 
            Height          =   288
            Left            =   -71532
            TabIndex        =   10
            Top             =   708
            Width           =   588
         End
         Begin VB.TextBox txtClaimCount 
            Height          =   288
            Left            =   -71532
            TabIndex        =   7
            Top             =   372
            Width           =   588
         End
         Begin VB.TextBox txtCity 
            Height          =   288
            Left            =   -72960
            TabIndex        =   17
            Top             =   1716
            Width           =   2808
         End
         Begin MSComCtl2.DTPicker dtpFromExpireDate 
            Height          =   288
            Left            =   -72504
            TabIndex        =   12
            Top             =   1044
            Width           =   1176
            _ExtentX        =   2064
            _ExtentY        =   503
            _Version        =   393216
            Format          =   145948673
            CurrentDate     =   37524
         End
         Begin VB.ComboBox cboLawyerCountOperator 
            Height          =   315
            ItemData        =   "frmFirmSearch.frx":1C3A
            Left            =   -72960
            List            =   "frmFirmSearch.frx":1C47
            Style           =   2  'Dropdown List
            TabIndex        =   9
            Top             =   708
            Width           =   1380
         End
         Begin VB.ComboBox cboClaimCountOperator 
            Height          =   315
            ItemData        =   "frmFirmSearch.frx":1C77
            Left            =   -72960
            List            =   "frmFirmSearch.frx":1C84
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   372
            Width           =   1380
         End
         Begin VB.CheckBox chkCity 
            Caption         =   "City:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   16
            Top             =   1764
            Width           =   1392
         End
         Begin VB.CheckBox chkCounty 
            Caption         =   "County:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   14
            Top             =   1428
            Width           =   1392
         End
         Begin VB.CheckBox chkExpirationDate 
            Caption         =   "Expiration Date:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   11
            Top             =   1092
            Width           =   1488
         End
         Begin VB.CheckBox chkClaimCount 
            Caption         =   "Claim Count:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   5
            Top             =   420
            Width           =   1392
         End
         Begin VB.CheckBox chkLawyerCount 
            Caption         =   "Lawyer Count:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   8
            Top             =   756
            Width           =   1392
         End
         Begin VB.ComboBox cboIn 
            Height          =   315
            ItemData        =   "frmFirmSearch.frx":1CB4
            Left            =   1632
            List            =   "frmFirmSearch.frx":1CB6
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Tag             =   "Firm"
            Top             =   696
            Width           =   2160
         End
         Begin VB.TextBox txtWords 
            Height          =   300
            Left            =   1632
            TabIndex        =   0
            Top             =   360
            Width           =   2148
         End
         Begin VB.Frame fraWhereFirm 
            BorderStyle     =   0  'None
            Height          =   312
            Left            =   48
            TabIndex        =   33
            Top             =   1548
            Width           =   5352
            Begin VB.ComboBox cboWhereFirm 
               Height          =   288
               ItemData        =   "frmFirmSearch.frx":1CB8
               Left            =   1596
               List            =   "frmFirmSearch.frx":1CD4
               Style           =   2  'Dropdown List
               TabIndex        =   3
               Top             =   18
               Visible         =   0   'False
               Width           =   3432
            End
            Begin VB.CheckBox chkWhereFirm 
               Caption         =   "Where firm"
               Height          =   228
               Left            =   96
               TabIndex        =   2
               Top             =   48
               Visible         =   0   'False
               Width           =   1392
            End
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "To:"
            Height          =   192
            Left            =   -71280
            TabIndex        =   43
            Top             =   1092
            Width           =   228
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "From:"
            Height          =   192
            Left            =   -72960
            TabIndex        =   42
            Top             =   1092
            Width           =   408
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Expiration Month:"
            Height          =   192
            Left            =   -74796
            TabIndex        =   41
            Top             =   420
            Width           =   1212
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Search f&or:"
            Height          =   192
            Left            =   228
            TabIndex        =   40
            Top             =   444
            Width           =   768
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Search in:"
            Height          =   192
            Left            =   228
            TabIndex        =   38
            Top             =   744
            Width           =   708
         End
      End
   End
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            Object.ToolTipText     =   "New"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Open"
            Object.ToolTipText     =   "Open"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Print"
            Object.ToolTipText     =   "Print"
            ImageKey        =   "Print"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Move"
            Object.ToolTipText     =   "Move to Folder"
            ImageKey        =   "Copy"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Delete"
            Object.ToolTipText     =   "Delete"
            ImageKey        =   "Delete"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode0"
            Object.ToolTipText     =   "View Large Icons"
            ImageKey        =   "View Large Icons"
            Style           =   2
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode1"
            Object.ToolTipText     =   "View Small Icons"
            ImageKey        =   "View Small Icons"
            Style           =   2
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode2"
            Object.ToolTipText     =   "View List"
            ImageKey        =   "View List"
            Style           =   2
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode3"
            Object.ToolTipText     =   "View Details"
            ImageKey        =   "View Details"
            Style           =   2
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "mnuFileOpen"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "&Print..."
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFilePrintSetup 
         Caption         =   "Print Set&up..."
      End
      Begin VB.Menu mnuFileBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditCut 
         Caption         =   "Cu&t"
         Enabled         =   0   'False
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "&Paste"
         Enabled         =   0   'False
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEditBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditRotate 
         Caption         =   "Rotate"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSeparator9221 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditSelectAll 
         Caption         =   "Select &All"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuEditInvertSelection 
         Caption         =   "&Invert Selection"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "Status &Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "Lar&ge Icons"
         Index           =   0
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "S&mall Icons"
         Index           =   1
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&List"
         Index           =   2
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&Details"
         Index           =   3
      End
      Begin VB.Menu mnuViewBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewArrangeIcons 
         Caption         =   "Arrange &Icons"
         Begin VB.Menu mnuArrangeIcon 
            Caption         =   "Columns"
            Index           =   0
         End
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuViewBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsOptions 
         Caption         =   "&Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "&Search For Help On..."
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "&Popups"
      Visible         =   0   'False
      Begin VB.Menu mnuListItem 
         Caption         =   "mnuListItem"
         Begin VB.Menu mnuListItemSelect 
            Caption         =   "Select"
         End
         Begin VB.Menu mnuListItemOpen 
            Caption         =   "mnuListItemOpen"
            Index           =   0
         End
         Begin VB.Menu mnuSeparator3421 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuListItemSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuSeparator783 
            Caption         =   "-"
         End
         Begin VB.Menu mnuListItemProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuSubItem 
         Caption         =   "mnuSubItem"
         Begin VB.Menu mnuSubItemView 
            Caption         =   "&View"
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "Lar&ge Icons"
               Index           =   0
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "S&mall Icons"
               Index           =   1
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "&List"
               Index           =   2
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "&Details"
               Index           =   3
            End
         End
         Begin VB.Menu mnuSeparator33 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemArrangeIcons 
            Caption         =   "Arrange &Icons"
            Begin VB.Menu mnuSubItemArrangeIcon 
               Caption         =   "mnuListViewArrangeIcon"
               Index           =   0
            End
         End
         Begin VB.Menu mnuSubItemRefresh 
            Caption         =   "R&efresh"
         End
         Begin VB.Menu mnuSeparator95 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemChooseColumns 
            Caption         =   "Choose Colum&ns..."
         End
         Begin VB.Menu mnuSeparator592 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemNew 
            Caption         =   "Ne&w"
         End
         Begin VB.Menu mnuSeparator343 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemProperties 
            Caption         =   "P&roperties"
         End
      End
   End
End
Attribute VB_Name = "frmFirmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.50.00 (jjm) 2004 02 10
'ver 2.00.0311 (jjm) Core
'
' Main Form
'

'
' The main form can be one of 3 form types set by the the following
' global constants from MainModule:
'
' ftView - Folder list and main window
' ftAdvancedSearch - Look in list, advanced search and a list view
' ftSelect - Advanced search and list view pinned to a folder
'


Option Explicit

' Public manager is needed by popup menus.
Public FormMgr As New FormManager

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

'local variable(s) to hold property value(s)
Private mFolderKey As String
Private mCaptionText As String
Private mView As View
Private mDataSet As FirmViewDataset 'Bug#1675
Private mIsSearchOnly As Boolean

'ver 2.50.00 (jjm) 2004 02 17
Private mEnableSelectNone As Boolean
Public ReturnNull As Boolean

Private lastSearch As String
Private lastCount As Integer

Private mbMoving As Boolean
Const sglSplitLimit = 1000
Private isLoaded As Boolean

Private mLawArea As Collection
Private mUnderwritingStatus As Collection
Private mInsurerStatus As Collection

Public TxtWordsIndex As Integer

Private mPopup(1) As Form
Private mDefaultMenu As Menu
Private mUnloaded As Boolean


'
' Start Generic Section
'
' Generic code is copied from frmGenericSearch.  In an object
' oriented world, this would be included in an ancestor.
'
'

'
' If a child is unloaded, find last form of same name.  Load that first, then
' load the main form.
'
Public Sub FormChildUnload(ByRef vData As FormInterface)
  Dim LastForm As Form
  Set LastForm = FindLastForm(vData.Child)
  If LastForm Is Nothing Then
    Show
    Refresh
  Else
    LastForm.Show
  End If
End Sub

'
' Called when this form is loaded from a Form Manager
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub


'
' Do nothing if a parent form is unloaded
'
Public Sub FormParentUnload()

End Sub

Public Property Get LawArea() As Collection
  Set LawArea = mLawArea
End Property

Public Property Get InsurerStatus() As Collection
  Set InsurerStatus = mInsurerStatus
End Property

Public Property Get UnderwritingStatus() As Collection
  Set UnderwritingStatus = mUnderwritingStatus
End Property

' Refresh folder

Public Sub MenuListItem(Optional X, Optional Y)
  MenuViewRefresh
  
  On Error Resume Next
  PopupMenu mnuListItem, , X, Y, mDefaultMenu
  On Error GoTo 0
  
  ' Either default menu doesn't exist or control array doesn't exist, or the default
  ' menu is a sub menu.
  If Err = 340 Or Err = 427 Or Err = 490 Then
    PopupMenu mnuListItem, , X, Y
  End If
End Sub

Public Sub MenuSubItem(Optional X, Optional Y)
  ' Eventually add Paste functionality here.
  PopupMenu mnuSubItem, , X, Y
End Sub


Public Sub MenuViewRefresh()
  Dim Count As Integer, IsCount, Selected As Integer, IsSelected, Workflow As Boolean
  Count = lvView.ListItems.Count
  IsCount = Count > 0
  Selected = ListViewModule.SelectedCount(lvView.hWnd)
  IsSelected = Selected > 0
  Workflow = mView.IsWorkflow
  
  
  ' set arrange check
  If (lvView.ColumnHeaders.Count > 0) Then
'    mnuArrangeIcon(lvView.SortKey + 1).Checked = True
'    Popup.mnuListViewArrangeIcon(lvView.SortKey + 1).Checked = True
  End If
  
  ' If an item selected, copy over popup open and explore.
  If IsSelected Then
    sbStatusBar.Panels(1).Text = Selected & " of " & Count & " object(s) selected"
    
    ' Copy over Open and Explore
    FormUtil.MenuCopy mView.OpenMenu, mnuFileOpen
    FormUtil.MenuCopy mView.OpenMenu, mnuListItemOpen
  Else
    sbStatusBar.Panels(1).Text = Count & " object(s)"
  End If
  
  mnuFileOpenMenu.Enabled = IsSelected And Not mnuFileOpen(0).Visible
'  mnuFilePrint.Enabled = IsCount
'  mnuFileExport.Enabled = IsSelected
  mnuFileSendToFolder.Enabled = IsSelected And Not Workflow
    
  mnuEditCut.Enabled = IsSelected And Workflow
  mnuEditPaste.Enabled = Not EventBuffer Is Nothing And Workflow
    
  mnuEditInvertSelection.Enabled = IsCount
  mnuEditSelectAll.Enabled = IsCount
  
  cmdOK.Enabled = IsSelected
  If IsSelected Then
    cmdOK.Default = True
  Else
    cmdSearchNow.Default = True
  End If
  
  If (mIsSearchOnly) Then
    Set mDefaultMenu = mnuListItemOpen(mView.DefaultMenuIndex)
  Else
    Set mDefaultMenu = mnuListItemSelect
  End If
End Sub


Private Sub SetArrangeMenu()
End Sub


Public Sub LoadView()
  ' Setup list view for searching.
  lvView.ListItems.Clear
  picListView_Resize
  lblSearchStatus.Caption = "Enter your search criteria to begin."
  picSearchStatus.Visible = True
End Sub


Public Sub SearchNow()
  ' Show hourglass
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  lvView.ListItems.Clear
  lblSearchStatus.Caption = "Searching..."
  picSearchStatus.Visible = True
  lblSearchStatus.Refresh
  
  'lastSearch is used for tmrSearch
  lastSearch = txtWords.Text
  
  'Setup column, sort and find
  If lvView.ColumnHeaders.Count > 0 Then
    'Bug#1675: Updated to use SQLDataProvider
    mDataSet.SQLDataProvider.Column = LawSQL.Column(lvView.ColumnHeaders)
    mDataSet.SQLDataProvider.Sort = LawSQL.Sort(lvView)
  End If
  
  'Bug#1675: call the dataset directly
  mDataSet.Dataset_FindByForm Me
  
  'SQL Debug - This is turned on and off in the Main Options
  If LocaleDebugAdvancedSearch Then
    DebugWhereClause mView.Dataset.DataProvider
  End If

  SearchResult
  MenuViewRefresh
End Sub

Public Sub SearchResult()
  If lvView.ListItems.Count = 0 Then
    lblSearchStatus.Caption = "Search is complete.  There are no results to display."
    picSearchStatus.Visible = True
  Else
    picSearchStatus.Visible = False
  End If
  MenuViewRefresh
  SetArrangeMenu
End Sub

'ver 2.50.00 2004 02 17
Public Sub ShowSearch(FolderKey As String, Optional IsSelect As Boolean, Optional TabIndex As Integer, Optional SearchInIndex As Integer, Optional SearchText As String, Optional CaptionText As String, Optional EnableSelectNone As Boolean)
  mFolderKey = FolderKey
  mCaptionText = CaptionText
  mIsSearchOnly = Not IsSelect
  mEnableSelectNone = EnableSelectNone
  
  Show
  
  sstabSearch.Tab = TabIndex
  
  If Not SearchInIndex = -1 Then
    cboIn.ListIndex = SearchInIndex
  End If
  txtWords = SearchText
End Sub

Public Sub ShowAdvancedSearch(FolderKey As String)
  mFolderKey = FolderKey
  mIsSearchOnly = True
  Show
End Sub

Public Sub ShowSelect(FolderKey As String)
  mFolderKey = FolderKey
  Show
End Sub
'
' End General Methods
'


'
' Control Methods
'

Private Sub cmdClose_Click()
  mnuFileExit_Click
End Sub

Private Sub cmdInsurerStatus_Click()
  Dim fStatus As New frmListSelectDialog
  
  fStatus.Caption = "Insurer Status Select"
  
  fStatus.lvView.ColumnHeaders.Clear
  fStatus.lvView.ListItems.Clear
  fStatus.lvView.ColumnHeaders.Add , , "Insurer Status"
  fStatus.lvView.ColumnHeaders(1).Width = fStatus.lvView.Width - 296
    
  'No current group holds these items so they need to be hard coded
  fStatus.lvView.ListItems.Add , "CI", "Currently Insured"
  fStatus.lvView.ListItems.Add , "FD", "Firm Dissolved"
  fStatus.lvView.ListItems.Add , "NI", "Never Insured"
  fStatus.lvView.ListItems.Add , "PI", "Previously Insured"
  
  'Add checkboxes
  Dim mItem As ListItem
  If Not mInsurerStatus Is Nothing Then
    With mInsurerStatus
      For Each mItem In mInsurerStatus
        fStatus.lvView.ListItems(mItem.Key).Checked = True
      Next mItem
    End With
  End If
 
  'show the form
  fStatus.Show vbModal, Me
  
  Dim I As Integer
  If fStatus.Ok Then
    'Get a list of the checked items
    Set mInsurerStatus = New Collection
    For I = 1 To fStatus.lvView.ListItems.Count
      If fStatus.lvView.ListItems.item(I).Checked Then
        mInsurerStatus.Add fStatus.lvView.ListItems(I)
      End If
    Next I
    'User friendly feature
    chkMultiInsurerStatus.Value = vbChecked
  End If
  
  Unload fStatus
End Sub

Private Sub cmdNew_Click()
  mnuFileNew_Click
End Sub

Private Sub cmdNewSearch_Click()
  ' Resets the form
  txtWords.Text = ""
  lvView.ListItems.Clear
  lblSearchStatus.Caption = "Enter your search criteria to begin."
  chkWhereFirm.Value = vbUnchecked

  'Block of business
  cboExpirationMonth.ListIndex = -1
  Dim I As Integer
  For I = 0 To 2
    chkInsureStatus(I).Value = vbUnchecked
  Next I
  For I = 0 To 3
    chkUnderwritingStatus(I).Value = vbUnchecked
  Next I
  chkUnderwritingStatus(5).Value = vbUnchecked
  optExcludeOnly(0).Value = True

  
  'Advanced
  chkWhereFirm.Value = vbUnchecked
  chkClaimCount.Value = vbUnchecked
  chkLawyerCount.Value = vbUnchecked
  chkExpirationDate.Value = vbUnchecked
  chkCounty.Value = vbUnchecked
  chkCity.Value = vbUnchecked
  chkLawArea.Value = vbUnchecked
  chkMultiInsurerStatus.Value = vbUnchecked
  txtClaimCount.Text = vbNullString
  txtLawyerCount.Text = vbNullString
  txtCounty.Text = vbNullString
  txtCity.Text = vbNullString
  cboClaimCountOperator.ListIndex = 0
  cboLawyerCountOperator.ListIndex = 0
  chkSelectStatus.Value = vbUnchecked
  Set mLawArea = Nothing
  Set mUnderwritingStatus = Nothing
  Set mInsurerStatus = Nothing
    
  MenuViewRefresh
End Sub

Private Sub cmdOk_Click()
  If mIsSearchOnly Then
    MenuViewRefresh
    CallByName Me, mDefaultMenu.Name & "_Click", VbMethod, mDefaultMenu.Index
  Else
    FormMgr.Message = fimSelect
    Unload Me
  End If
End Sub

Private Sub cmdSearchNow_Click()
  mnuViewRefresh_Click
End Sub

'ver 2.50.00 (jjm) 2004 02 17
Private Sub cmdSelectNone_Click()
  ReturnNull = True
  FormMgr.Message = fimSelect
  Unload Me
End Sub

Private Sub cmdUnderwritingStatus_Click()
  Dim fStatus As New frmListSelectDialog
  
  fStatus.Caption = "Underwriting Status Select"
    
  'load the view
  Dim StatusList As Collection, Row As Collection
  Set StatusList = Factory.EventTypeService.GroupCollection("UnderwritingStatus")
  
  fStatus.lvView.ColumnHeaders.Clear
  fStatus.lvView.ListItems.Clear
  fStatus.lvView.ColumnHeaders.Add , , "Underwriting Status"
  fStatus.lvView.ColumnHeaders(1).Width = fStatus.lvView.Width - 296
  
  Dim I As Integer
  With StatusList
    For I = 1 To .Count
      fStatus.lvView.ListItems.Add , .item(I)(DatabaseName.EVENTTYPE_KEY), .item(I)("eventTypeName")
    Next I
  End With
  
  'Add checkboxes
  Dim mItem As ListItem
  If Not mUnderwritingStatus Is Nothing Then
    With mUnderwritingStatus
      For Each mItem In mUnderwritingStatus
        fStatus.lvView.ListItems(mItem.Key).Checked = True
      Next mItem
    End With
  End If
 
  'show the form
  fStatus.Show vbModal, Me
  
  If fStatus.Ok Then
    'Get a list of the checked items
    Set mUnderwritingStatus = New Collection
    For I = 1 To fStatus.lvView.ListItems.Count
      If fStatus.lvView.ListItems.item(I).Checked Then
        mUnderwritingStatus.Add fStatus.lvView.ListItems(I)
      End If
    Next I
    
    'User friendly feature
    chkSelectStatus.Value = vbChecked
  End If
  
  Unload fStatus
End Sub

Private Sub dtpFromExpireDate_Change()
  chkExpirationDate = vbChecked
End Sub

Private Sub dtpToExpireDate_Change()
  chkExpirationDate = vbChecked
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyInsert Then
    mnuFileNew_Click
  End If
End Sub

Private Sub Form_Load()
  If mUnloaded Then
    MsgBox Name & ": Illegal Load Attempt.  Please report."
    Unload Me
    Exit Sub
  End If
  
  ' Load form manager.
  FormMgr.Load Me
   
  ' Setup child forms.
  Set mPopup(0) = New frmPopup
  Load mPopup(0)
  FormMgr.Add mPopup(0)
  
  Set mPopup(1) = New frmPopup2
  Load mPopup(1)
  FormMgr.Add mPopup(1)
  
  ' Setup view.
  Set mView = New View
  FolderUtil.ViewLoad mView, mFolderKey, mPopup
    
  Caption = IIf(mCaptionText = vbNullString, IIf(mIsSearchOnly, "Search for a ", "Select a ") & mFolderKey, mCaptionText)
  
  ' Open dataset if necessary.
  'bug#1675 - use custom dataset
  Set mDataSet = New FirmViewDataset
  Set mDataSet.Dataset_Session = LawSession
  sbStatusBar.Panels(2).Text = mDataSet.SQLDataProvider.Source
    
  lvView.Tag = mFolderKey & IIf(mIsSearchOnly, "Search", "Select")
  lvView.ColumnHeaders.Clear
  lvView.ListItems.Clear
  ListViewModule.DeserializeColumnHeaders lvView, lvView.Tag
  'bug#1675 - use dataset directly instead of using the mView.dataset
  mDataSet.Dataset_LoadSearch Me
  
  LoadView

  ' Set toolbar
  tbToolBar.Buttons("lvMode" & lvView.View).Value = tbrPressed
    
  ' Setup form
  If mIsSearchOnly Then
    picSelect.Visible = False
    picSelect.Enabled = False
    mnuListItemSelect.Visible = False
    cmdClose.Enabled = False
    picButtons.Left = -20000
  End If
  
  cboLawyerCountOperator.ListIndex = 0
  cboClaimCountOperator.ListIndex = 0
  
  'chkUnderDecision.Value = False
  'chkUnderDecision_Click
  
  'Get view settings from ini file.
  mnuViewToolbar.Checked = IniMain.ReadFile(UrlName.MAIN_INI, "ViewToolbar")
  mnuViewStatusBar.Checked = IniMain.ReadFile(UrlName.MAIN_INI, "ViewStatusBar")
  
  ' Hide popup.
  mnuPopup.Visible = False
  
  ' SSTab Fix
  FormUtil.TabControlEnable sstabSearch
  
  ' position settings
  ' set defaults first
  Top = 0
  Left = 0
  Height = 7200
  Width = 9600
  FormUtil.DeserializePosition Me
    
  dtpFromExpireDate.Value = Today
  dtpToExpireDate.Value = Today
    
  FormLoad
End Sub

Private Sub Form_Resize()
  ' Do not resize when minimized.
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  tbToolBar.Visible = mnuViewToolbar.Checked
  sbStatusBar.Visible = mnuViewStatusBar.Checked
  
  Dim BottomHeight As Integer
  BottomHeight = IIf(sbStatusBar.Visible, sbStatusBar.Height, 0)
  BottomHeight = BottomHeight + IIf(picSelect.Visible, picSelect.Height, 0)
  
  picListView.Height = Max(Me.ScaleHeight - BottomHeight - picListView.Top, 0)
  
  'ver 2.50.00 (jjm) 2004 02 18
  picSelectNone.Visible = mEnableSelectNone
  
  If mIsSearchOnly = False Then
    picButtons.Top = picSelect.Top
    'ver 2.50.00 (jjm) 2004 02 17
    If picSelectNone.Visible Then picSelectNone.Top = picSelect.Top
  End If
End Sub

'
' Uncomment MsgBox to test for proper form termination.
'
Private Sub Form_Terminate()
  'MsgBox "frmMain Terminate"
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  
  ' Close down form manager
  FormMgr.Unload
  
  ' Unload child forms
  Unload mPopup(0)
  Unload mPopup(1)
  
  mUnloaded = True
End Sub

Private Sub lblListView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  picListView_MouseUp Button, Shift, X, Y
End Sub

Public Sub lvView_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  'HACK bug #1675: don't pass the dataprovider because it is an SQL data provider
  ListViewSort lvView, ColumnHeader
  'HACK bug #1675: Manually call the search to reload the screen with the new sort
  SearchNow
End Sub

Private Sub lvview_DblClick()
  If cmdOK.Enabled Then cmdOk_Click
End Sub

Private Sub lvView_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF10 And (Shift And vbShiftMask) > 0 Then
    If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
      MenuSubItem picListView.Left + 24, picListView.Top + 24
    Else
      MenuListItem picListView.Left + 24, picListView.Top + 24
    End If
    KeyCode = 0
    Exit Sub
  End If
End Sub

Private Sub lvView_KeyUp(KeyCode As Integer, Shift As Integer)
  MenuViewRefresh
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView.hWnd, X, Y) And ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    lvView.SelectedItem.Selected = False
  End If
   If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
       MenuSubItem
     Else
       MenuListItem
     End If
  End If
  ' Don't call anything here in case form was unloaded in MenuSubItem.
End Sub

Private Sub lvView_GotFocus()
  If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    cmdOK.Default = True
  End If
End Sub

Private Sub mnuArrangeIcon_Click(Index As Integer)
  Call lvView_ColumnClick(lvView.ColumnHeaders(Index))
End Sub

Private Sub mnuControl_Click()
  MsgBox Me.ActiveControl.Name & " TabIndex: " & Me.ActiveControl.TabIndex
End Sub

Private Sub mnuEditInvertSelection_Click()
  Dim n, lvCount As Integer
  lvCount = lvView.ListItems.Count
  
  For n = 1 To lvCount
    If lvView.ListItems(n).Selected = True Then
      lvView.ListItems(n).Selected = False
    Else
      lvView.ListItems(n).Selected = True
    End If
  Next n
  MenuViewRefresh
End Sub

Private Sub mnuEditRotate_Click()
  cboIn.ListIndex = (cboIn.ListIndex + 1) Mod cboIn.ListCount
  txtWords.SetFocus
End Sub

Private Sub mnuEditSelectAll_Click()
  ListViewModule.SelectAll lvView.hWnd
  MenuViewRefresh
End Sub

Private Sub mnuFile_Click()
  mnuFilePrint.Enabled = (lvView.ListItems.Count > 0)
  If ActiveControl Is lvView Then
    mnuFileProperties.Tag = vbNullString
    If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
      mnuFileProperties.Tag = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.EVENT_KEY)
    End If
    mnuFileProperties.Enabled = mnuFileProperties.Tag <> vbNullString
  End If
  MenuViewRefresh
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNew_Click()
  ' The parent form should use the FormInterface.Message property
  ' to see if there is a request for a new record (fimNew).  See
  ' frmUnderwritingComment.FormChildUnload method.
  If mIsSearchOnly Then
    Dispatcher.ShowNew DatabaseName.FIRM_TABLE, , FormMgr
    Exit Sub
  End If
  
  FormMgr.Message = fimNew
  Unload Me
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  CallByName mView.PopupMenuForm, mView.OpenMenuName & "_Click", VbMethod, Index
End Sub

Private Sub mnuFilePrint_Click()
  PrintUtil.PrintDialog Me, lvView
End Sub

Private Sub mnuFilePrintSetup_Click()
  PrintUtil.PrintSetupDialog Me
End Sub

Private Sub mnuFileSendToFolder_Click()
  Dim Source As String, ruleId As String
  Source = DatabaseName.FIRM_TABLE
  ruleId = DatabaseName.FIRM_REF
  
  ' Create form and show.
  Dim Form As New frmFolderBrowseDialog
  If Not Form.ShowSendTo(ruleId) Then
    Unload Form
    Exit Sub
  End If
  
  '
  ' Form is OK
  '
  Refresh
  Dim pointer As New WaitPointer
  pointer.Default
  
  Call Factory.EventService.Create( _
      Form.EventTypeId, _
      ruleId, _
      Form.FolderKey, _
      ListViewText(lvView, DatabaseName.EVENT_KEY), _
      ListViewText(lvView, DatabaseName.FIRMNAME_FIELD))
  Unload Form
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Public Sub mnuListItemOpen_Click(Index As Integer)
  mnuFileOpen_Click Index
End Sub

Private Sub mnuListItemSelect_Click()
  cmdOk_Click
End Sub

Private Sub mnuListItemSendToFolder_Click()
  mnuFileSendToFolder_Click
End Sub

Private Sub mnuListViewMode_Click(Index As Integer)
  lvView.View = Index
  tbToolBar.Buttons("lvMode" & Index).Value = tbrPressed
End Sub

Private Sub mnuToolsOptions_Click()
  Dim fOptions As New frmMainOptionsDialog
  fOptions.Show vbModal, Me
  Unload fOptions
  picListView_Resize
End Sub

'
' Choose Columns.  Public because it is called from mPopup menu.
'
Public Sub mnuViewChooseColumns_Click()
  If ListViewModule.ColumnSettings(lvView, mView.Dataset.DataProvider.Source, lvView.Tag) Then
    LoadView
    lvView.Visible = True
  End If
End Sub

'
' Refresh - Public because called from mPopup
'
Public Sub mnuViewRefresh_Click()
  SearchNow
End Sub

'
' Serialize column headers
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView, lvView.Tag
End Sub

Private Sub mnuViewStatusBar_Click()
  mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
  Form_Resize
  IniMain.WriteFile UrlName.MAIN_INI, "ViewStatusbar", mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  Form_Resize
  IniMain.WriteFile UrlName.MAIN_INI, "ViewToolbar", mnuViewToolbar.Checked
End Sub

Private Sub picListView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  lvView_MouseUp Button, Shift, X, Y
End Sub

Private Sub picListView_Resize()
  ' Do not resize when minimized
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  lvView.Left = IIf(LocaleEnableWebContent = True, 1716, 0)
  lvView.Height = Max(picListView.ScaleHeight - lvView.Top, 0)
  lvView.Width = Max(picListView.ScaleWidth - lvView.Left, 0)
  
  picSearchStatus.Top = lvView.Top + IIf(lvView.View = lvwReport, 240, 0)
  picSearchStatus.Left = lvView.Left
  picSearchStatus.Width = lvView.Width
  
  If Me.Width < 7180 Then
    Me.Width = 7180
    sstabSearch.Width = Me.Width - picSearchButtons.Width
    Exit Sub
   End If
    
   sstabSearch.Width = Me.Width - picSearchButtons.Width
   picSearchButtons.Left = sstabSearch.Left + sstabSearch.Width
   txtWords.Width = sstabSearch.Width - 1850
   cboIn.Width = txtWords.Width
End Sub

Private Sub picSelect_Resize()
  If Not mIsSearchOnly Then
    'ver 2.50.00 (jjm) 2004 02 17
     If mEnableSelectNone Then
       picSelectNone.Left = Max(ScaleWidth - picSelectNone.Width, 0)
       picButtons.Left = Max(ScaleWidth - (picButtons.Width + picSelectNone.Width), 0)
     Else
       picButtons.Left = Max(ScaleWidth - picButtons.Width, 0)
     End If
  End If

End Sub

Private Sub sstabSearch_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable sstabSearch
  If sstabSearch.Tab = 1 And PreviousTab <> 1 Then _
    cboExpirationMonth.SetFocus
  If sstabSearch.Tab = 0 And PreviousTab <> 0 Then _
    txtWords.SetFocus
End Sub

'
' This is a rare case where the ToolBar is the master for functionality.
' Usually, menus contain the functionality
'
Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "New"
            mnuFileNew_Click
        Case "Open"
            MsgBox "Add open code" 'ToDo:
        Case "Print"
            mnuFilePrint_Click
        Case "Move"
            
        Case "Delete"
            MsgBox "Add delete code" 'ToDo:
        Case "View Large Icons"
            lvView.View = lvwIcon
            picListView_Resize
        Case "View Small Icons"
            lvView.View = lvwSmallIcon
            picListView_Resize
        Case "View List"
            lvView.View = lvwList
            picListView_Resize
        Case "View Details"
            lvView.View = lvwReport
            picListView_Resize
    End Select
End Sub

Private Sub tmrSearch_Timer()
  tmrSearch.Enabled = False
  If Len(txtWords.Text) > 2 And lastSearch <> txtWords.Text Then
    cmdSearchNow_Click
  End If
End Sub

Private Sub txtCity_Change()
  chkCity.Value = IIf(txtCity.Text = vbNullString, vbUnchecked, vbChecked)
End Sub

Private Sub txtClaimCount_Change()
  chkClaimCount.Value = IIf(txtClaimCount.Text = vbNullString, vbUnchecked, vbChecked)
End Sub

Private Sub txtCounty_Change()
  chkCounty.Value = IIf(txtCounty.Text = vbNullString, vbUnchecked, vbChecked)
End Sub

Private Sub txtinsurername_Change()
  chkInsurerName.Value = IIf(txtInsurerName.Text = vbNullString, vbUnchecked, vbChecked)
End Sub

Private Sub txtLawyerCount_Change()
chkLawyerCount.Value = IIf(txtLawyerCount.Text = vbNullString, vbUnchecked, vbChecked)
End Sub

Private Sub txtWords_GotFocus()
  FormUtil.SelAll txtWords
End Sub

Private Sub txtWords_KeyDown(KeyCode As Integer, Shift As Integer)
  If (KeyCode = vbKeyDown Or KeyCode = vbKeyUp) And (Shift And vbShiftMask) = 0 Then
    SendMessage lvView.hWnd, WM_KEYDOWN, KeyCode, 0
    KeyCode = 0
    cmdOK.Default = True
    Exit Sub
  End If
  If (KeyCode = vbKeyL) And (Shift And vbCtrlMask) > 0 Then
    cboIn.ListIndex = IIf(cboIn.ListIndex = cboIn.ListCount - 1, _
      0, cboIn.ListIndex + 1)
    txtWords_KeyPress 0
    KeyCode = 0
  End If
End Sub

Private Sub txtWords_KeyPress(KeyAscii As Integer)
  tmrSearch.Enabled = False
  cmdSearchNow.Default = True
  tmrSearch.Enabled = True
End Sub
'
'
'
' End Generic Section
'
'
'
'
' Start Custom Section
'
'

Private Sub FormLoad()
  cboExpirationMonth.ListIndex = 0
  cboWhereFirm.ListIndex = 0
  chkWhereFirm.Value = 0
End Sub

'ver 2.00.0311 (jjm)
Private Sub cboExpirationMonth_Click()
  If cboExpirationMonth.ListIndex > 0 Then
    Dim I As Integer
    For I = 0 To chkUnderwritingStatus.Count - 1
      chkUnderwritingStatus(I).Value = vbChecked
    Next
  End If
End Sub

Private Sub cboWhereFirm_Click()
  chkWhereFirm.Value = 1
End Sub

'ver 2.00.0311 (jjm)
'Private Sub chkUnderwritingStatus_Click(Index As Integer)
'  If chkUnderwritingStatus(Index).Value = 0 Then
'    chkUnderwritingStatus(5).Value = 0
'  End If
'
'  If chkUnderwritingStatus(5).Value = 1 Then
'    Dim I As Integer
'    For I = 0 To 4
'      chkUnderwritingStatus(I).Value = 1
'    Next
'  End If
'End Sub

Private Sub chkWhereFirm_Click()
  cboWhereFirm.Visible = IIf(chkWhereFirm.Value = 1, True, False)
End Sub

Private Sub cmdLawArea_Click()
  Dim fLawArea As New frmSelectLawAreaDialog
  Set fLawArea.ApplyLawArea = mLawArea
  fLawArea.Show vbModal, Me
  
  If fLawArea.Ok Then
    Set mLawArea = fLawArea.ApplyLawArea
    chkLawArea.Value = vbChecked
  End If
  
  Unload fLawArea

End Sub
