Attribute VB_Name = "OnlineAppUtil"
Option Explicit

Public Sub retrieveOnlineApplications()

  Dim xmlHttp As New xmlHttp
  Dim Request As String
  Dim xmlDoc As DOMDocument
    
  Set xmlHttp = XMLUtil.HttpGet(LawSession.Connection & "online/retrieve?" & NoCache)
  
  Select Case xmlHttp.Status
    Case 500
      MsgBox "Error retrieving applications." & vbCrLf & xmlHttp.responseText, vbCritical
    Case 200
      MsgBox "Applications retrieved." & vbCrLf & xmlHttp.responseText
  End Select
  
End Sub

Public Sub browseOnlineApplication(onlineAppNumber As String)
  'mantis 2778
  If urlOnlineApplicationPDF = vbNullString Then
    MsgBox "Online Application PDF URL is not configured. In " & IniFolder.FileName & _
    " add a " & UrlName.ONLINE_APP_PDF_INI & " entry to the " & UrlName.MISC_INI & " section. The value should be contain the URL without any parameters.", vbCritical
    Exit Sub
  End If
  
  Browse urlOnlineApplicationPDF & "?applicantId=" & onlineAppNumber
End Sub


