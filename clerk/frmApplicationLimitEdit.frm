VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmApplicationLimitEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Limit"
   ClientHeight    =   4860
   ClientLeft      =   2736
   ClientTop       =   5040
   ClientWidth     =   5424
   Icon            =   "frmApplicationLimitEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4860
   ScaleWidth      =   5424
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4212
      Left            =   0
      ScaleHeight     =   4212
      ScaleWidth      =   5424
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   384
      Width           =   5424
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         CausesValidation=   0   'False
         Height          =   336
         Left            =   4164
         TabIndex        =   38
         Top             =   3816
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   2928
         TabIndex        =   37
         Top             =   3816
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1692
         TabIndex        =   36
         Top             =   3816
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3624
         Left            =   84
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   96
         Width           =   5268
         _ExtentX        =   9292
         _ExtentY        =   6414
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabHeight       =   420
         TabCaption(0)   =   "Edit"
         TabPicture(0)   =   "frmApplicationLimitEdit.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Frame1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "New"
         TabPicture(1)   =   "frmApplicationLimitEdit.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "fraLimit"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "fraDeductibles"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).ControlCount=   2
         Begin VB.Frame Frame1 
            Caption         =   "Limits"
            Height          =   1356
            Left            =   -74880
            TabIndex        =   32
            Top             =   372
            Width           =   4740
            Begin VB.TextBox txtDataField 
               DataField       =   "perClaimDeductible"
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "#,##0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   1
               EndProperty
               DataMember      =   "ApplicationLimit"
               Height          =   288
               Index           =   2
               Left            =   1452
               TabIndex        =   35
               Text            =   "perClaimDedcutible"
               Top             =   924
               Width           =   3168
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "perClaimLimit"
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "#,##0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   1
               EndProperty
               DataMember      =   "ApplicationLimit"
               Height          =   288
               Index           =   0
               Left            =   1464
               TabIndex        =   33
               Text            =   "perClaimLimit"
               Top             =   252
               Width           =   3156
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "aggregateLimit"
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "#,##0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   1
               EndProperty
               DataMember      =   "ApplicationLimit"
               Height          =   288
               Index           =   1
               Left            =   1452
               TabIndex        =   34
               Text            =   "aggregateLimit"
               Top             =   588
               Width           =   3156
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Deductible:"
               Height          =   192
               Left            =   132
               TabIndex        =   41
               Top             =   972
               Width           =   816
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Aggregate Limit:"
               Height          =   192
               Left            =   132
               TabIndex        =   40
               Top             =   636
               Width           =   1164
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Per Claim Limit"
               Height          =   192
               Left            =   132
               TabIndex        =   39
               Top             =   300
               Width           =   1056
            End
         End
         Begin VB.Frame fraDeductibles 
            Caption         =   "&Deductibles"
            Height          =   780
            Left            =   60
            TabIndex        =   21
            Top             =   2688
            Width           =   5124
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$1,500"
               Height          =   192
               Index           =   0
               Left            =   108
               TabIndex        =   22
               Tag             =   "1500"
               Top             =   228
               Width           =   996
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$2,500"
               Height          =   192
               Index           =   1
               Left            =   1104
               TabIndex        =   23
               Tag             =   "2500"
               Top             =   228
               Width           =   996
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$5,000"
               Height          =   192
               Index           =   2
               Left            =   2100
               TabIndex        =   24
               Tag             =   "5000"
               Top             =   228
               Width           =   996
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$7,500"
               Height          =   192
               Index           =   3
               Left            =   3096
               TabIndex        =   25
               Tag             =   "7500"
               Top             =   228
               Width           =   996
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$10,000"
               Height          =   192
               Index           =   4
               Left            =   4092
               TabIndex        =   26
               Tag             =   "10000"
               Top             =   228
               Width           =   1008
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$100,000"
               Height          =   192
               Index           =   9
               Left            =   4092
               TabIndex        =   31
               Tag             =   "100000"
               Top             =   468
               Width           =   1008
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$75,000"
               Height          =   192
               Index           =   8
               Left            =   3096
               TabIndex        =   30
               Tag             =   "75000"
               Top             =   468
               Width           =   996
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$50,000"
               Height          =   192
               Index           =   7
               Left            =   2100
               TabIndex        =   29
               Tag             =   "50000"
               Top             =   468
               Width           =   996
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$25,000"
               Height          =   192
               Index           =   6
               Left            =   1104
               TabIndex        =   28
               Tag             =   "25000"
               Top             =   468
               Width           =   996
            End
            Begin VB.CheckBox chkDeductibles 
               Caption         =   "$15,000"
               Height          =   192
               Index           =   5
               Left            =   108
               TabIndex        =   27
               Tag             =   "15000"
               Top             =   468
               Width           =   996
            End
         End
         Begin VB.Frame fraLimit 
            Caption         =   "&Limits of Liability"
            Height          =   2268
            Left            =   60
            OLEDropMode     =   1  'Manual
            TabIndex        =   6
            Top             =   360
            Width           =   5124
            Begin VB.CheckBox chkClaim 
               Caption         =   "$1,000,000 /$3,000,000"
               Height          =   228
               Index           =   13
               Left            =   108
               TabIndex        =   11
               Tag             =   "1000000 3000000"
               Top             =   1356
               Width           =   2232
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$10,000,000 /$10,000,000 "
               Height          =   228
               Index           =   12
               Left            =   2364
               TabIndex        =   20
               Tag             =   "10000000 10000000"
               Top             =   1908
               Width           =   2400
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$9,000,000 /$9,000,000 "
               Height          =   228
               Index           =   11
               Left            =   2364
               TabIndex        =   19
               Tag             =   "9000000 9000000"
               Top             =   1632
               Width           =   2244
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$8,000,000 /$8,000,000 "
               Height          =   228
               Index           =   10
               Left            =   2364
               TabIndex        =   18
               Tag             =   "8000000 8000000"
               Top             =   1356
               Width           =   2304
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$7,000,000 /$7,000,000 "
               Height          =   228
               Index           =   9
               Left            =   2364
               TabIndex        =   17
               Tag             =   "7000000 7000000"
               Top             =   1080
               Width           =   2304
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$6,000,000 /$6,000,000 "
               Height          =   228
               Index           =   8
               Left            =   2364
               TabIndex        =   16
               Tag             =   "6000000 6000000"
               Top             =   804
               Width           =   2304
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$5,000,000 /$5,000,000 "
               Height          =   228
               Index           =   7
               Left            =   2364
               TabIndex        =   15
               Tag             =   "5000000 5000000"
               Top             =   528
               Width           =   2304
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$4,000,000 /$4,000,000 "
               Height          =   228
               Index           =   6
               Left            =   2364
               TabIndex        =   14
               Tag             =   "4000000 4000000"
               Top             =   252
               Width           =   2304
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$3,000,000 /$3,000,000"
               Height          =   228
               Index           =   5
               Left            =   108
               TabIndex        =   13
               Tag             =   "3000000 3000000"
               Top             =   1908
               Width           =   2244
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$2,000,000 /$2,000,000"
               Height          =   228
               Index           =   4
               Left            =   108
               TabIndex        =   12
               Tag             =   "2000000 2000000"
               Top             =   1632
               Width           =   2232
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$1,000,000 /$2,000,000"
               Height          =   228
               Index           =   3
               Left            =   108
               TabIndex        =   10
               Tag             =   "1000000 2000000"
               Top             =   1080
               Width           =   2232
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$500,000 /$1,000,000"
               Height          =   228
               Index           =   2
               Left            =   108
               TabIndex        =   9
               Tag             =   "500000 1000000"
               Top             =   804
               Width           =   2232
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$250,000 /$750,000"
               Height          =   228
               Index           =   1
               Left            =   108
               TabIndex        =   8
               Tag             =   "250000 750000"
               Top             =   528
               Width           =   2232
            End
            Begin VB.CheckBox chkClaim 
               Caption         =   "$100,000 /$300,000"
               Height          =   228
               Index           =   0
               Left            =   108
               TabIndex        =   7
               Tag             =   "100000 300000"
               Top             =   252
               Width           =   2232
            End
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5424
      _ExtentX        =   9567
      _ExtentY        =   677
      BandCount       =   1
      _CBWidth        =   5424
      _CBHeight       =   384
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   336
         Left            =   24
         ScaleHeight     =   336
         ScaleWidth      =   5328
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   24
         Width           =   5328
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   24
            TabIndex        =   2
            Top             =   24
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   288
      Left            =   2376
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   420
      Width           =   1008
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   0
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator996 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator693 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmApplicationLimitEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
' This form is a standard "Edit" style form.
'

Option Explicit

' Master data.
Private mApplicationLimitHandle As String

' ApplicationHandle is required for new limits.
Private mApplicationHandle As String

' Pull limits by agg/ded.
Private mDataProvider As DataProvider

' Show information.
Private mListIndex As Integer
Private mListView As ListView

' Data status.
Private mNew As Boolean
Private mDirty As Boolean

' Manager subordinate forms.
Private FormMgr As New FormManager

'Constants
Private Const TXT_PERCLAIMLIMIT_INDEX = 0
Private Const TXT_AGGREGATELIMIT_INDEX = 1
Private Const TXT_PERCLAIMDEDUCTIBLE_INDEX = 2



'
' General Properties/Methods
'

'
' Standard Change for cmdSave and cmdSaveNew type Edits.  Use standard
' Change from frmApplicationEdit for forms without cmdSaveNew.
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Public Function FindLimit(PerClaimLimit As Long, AggregateLimit As Long, PerClaimDeductible As Long) As Boolean
  mDataProvider.filter = _
      DatabaseName.APPLICATION_KEY & "='" & mApplicationHandle & "' and " & _
      DatabaseName.PERCLAIMLIMIT_FIELD & "=" & PerClaimLimit & " and " & _
      DatabaseName.AGGREGATELIMIT_FIELD & "=" & AggregateLimit & " and " & _
      DatabaseName.PERCLAIMDEDUCTIBLE_FIELD & "=" & PerClaimDeductible
  mDataProvider.OpenRecordset 1
  FindLimit = Not mDataProvider.IsEmpty
End Function

'
' Notified when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
End Sub

'
' Notified when this form has been loaded by a form manager.
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notified when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormUtil.NavigationRefresh Me, mListView, mListIndex
End Sub


'
' Custom refresh.
'
Private Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass

  ClearDataField txtDataField
  
  If (mNew) Then
    Me.Caption = "New Application Limit"
  Else
    Dim Datafields As collection
    Set Datafields = Factory.ApplicationLimitService.Find(mApplicationLimitHandle)
    mApplicationHandle = Datafields(DatabaseName.APPLICATION_KEY)
    Caption = "Edit Application Limit"
    SetFields txtDataField, Datafields
  End If
  
  ' Refresh navigation
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' After refresh, record is not changed if existing, changed if new
  Dirty = False
End Sub
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub
'
' Get limit models from form for insert
'
Private Function LimitModels() As collection
  Dim Result As New collection
  Dim Deductibles(9) As Long
  Dim I, j, DeductibleCount As Integer
  Dim Limit As LimitModel
  Dim Limits() As String

  ' Get all deductibles
  For I = 0 To chkDeductibles.Count - 1
    If chkDeductibles(I).Value = 1 Then
      Deductibles(DeductibleCount) = chkDeductibles(I).Tag
      DeductibleCount = DeductibleCount + 1
    End If
  Next I

  ' If no deductibles, then nothing to save.
  If DeductibleCount = 0 Then
    Exit Function
  End If

  For I = 0 To chkClaim.Count - 1
    If chkClaim(I).Value = 1 Then
      Limits() = Split(chkClaim(I).Tag, " ")
      For j = 0 To DeductibleCount - 1
        Set Limit = New LimitModel
        Limit.PerClaimLimit = GetDouble(Limits(0))
        Limit.AggregateLimit = GetDouble(Limits(1))
        Limit.PerClaimDeductible = GetDouble(Deductibles(j))
        Result.Add Limit
      Next j
    End If
  Next I

  Set LimitModels = Result
End Function

'
' Custom ListIndex to set Handle's appropriately
'
Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mApplicationLimitHandle = ListItemText(mListView.SelectedItem, DatabaseName.APPLICATIONLIMIT_KEY)
End Property

'
' Save an application limit.  ApplicationLimit uses the ApplicationLimitId for
' a handle.  Changing the value of an existing ApplicationLimitId is not
' supported.  Must do a delete/insert.
'
' The create process assigns a unique ApplicationLimitId which is assigned
' to the ApplicationLimitHandle.
'
Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't Save(True).
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Begin Custom Code
  Dim Hourglass As New WaitPointer
  Hourglass.Default
  
  If mNew Then
    Dim Models As collection
    Set Models = LimitModels
    
    If Not Models Is Nothing Then
      Dim I As Integer
      For I = 1 To Models.Count
        If Not FindLimit(Models(I).PerClaimLimit, Models(I).AggregateLimit, Models(I).PerClaimDeductible) Then
          Call Factory.ApplicationLimitService.CreateApplicationLimit( _
              mApplicationHandle, _
              Models(I).PerClaimLimit, _
              Models(I).AggregateLimit, _
              Models(I).PerClaimDeductible, _
              vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
          Save = True
        End If
      Next I
    End If
    
    If Not Save Then
      MsgBox "No new limits were saved"
    End If
    
'    mApplicationLimitHandle = Factory.ApplicationLimitService.CreateApplicationLimit( _
'      vbNullString, mApplicationHandle, _
'      GetDouble(txtDataField(0)), GetDouble(txtDataField(1)), _
'      GetDouble(txtDataField(2)), 0, _
'      vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
'    Save = Len(mApplicationLimitHandle) > 0
'    mNew = Not Save
  Else
    Save = Factory.ApplicationLimitService.WriteApplicationLimit( _
      mApplicationLimitHandle, _
      GetLong(txtDataField(TXT_PERCLAIMLIMIT_INDEX).Text), _
      GetLong(txtDataField(TXT_AGGREGATELIMIT_INDEX).Text), _
      GetLong(txtDataField(TXT_PERCLAIMDEDUCTIBLE_INDEX).Text), _
      vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
  End If

  ' mDataset not used.  No need to refresh.
  Dirty = Not Save
  ' End Custom Code
End Function


Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no limits to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub


Public Sub ShowNew(ApplicationId As String)
  mApplicationHandle = ApplicationId
  mNew = True
  Show
End Sub


Public Sub ShowByPrimaryKey(ApplicationLimitId As String)
  mApplicationLimitHandle = ApplicationLimitId
  Show
End Sub
' End General
'
'
'
'
' Control/Object Events
'

Private Sub chkClaim_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub chkDeductibles_Click(Index As Integer)
  Dirty = True
End Sub

'
' Standard Cancel Command Button.
'
Private Sub cmdCancel_Click()
  Unload Me
End Sub

'
' Standard Enter Command Button to support Enter As Tab
'
Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Standard Save Command Button
'
Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

'
' Standard Save/New Command Button
'
Private Sub cmdSaveNew_Click()
  If Save Then mnuFileNew_Click
End Sub

'
' Initialize memory only.  Do not touch controls.
'
Private Sub Form_Initialize()
  Set mDataProvider = LawSession.NewDataProvider(DatabaseName.APPLICATIONLIMIT_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub


Private Sub Form_Load()
  ' Standard load for configuring menu items.  Requires the
  ' following controls:
  '   mnuFileExit
  '   mnuViewPrevious
  '   mnuViewNext
  '   cmdCancel
  '   cmdEnter
  FormUtil.EditLoad Me
    
  ' Standard load for navigation requires:
  ' imlUpDown
  ' imlUpDownDisabled
  ' tbrUpDown
  FormUtil.EditLoadImages Me
    
  ' Standard manager load
  FormMgr.Load Me
  
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  If mNew Then
    SSTab.TabEnabled(0) = False
    SSTab.TabVisible(0) = False
  Else
    SSTab.TabEnabled(1) = False
    SSTab.TabVisible(1) = False
  End If
  
  FormRefresh
  TabControlEnable SSTab
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

'
' Custom Terminate used for tracking memory problems.
'
Private Sub Form_Terminate()
  'MsgBox "frmApplicationLimitEdit Terminate"
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

'
' Standard File/Close Menu for all Edit forms.
'

'
' Custom File/New to set handles.
'
Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mApplicationLimitHandle = vbNullString
  FormRefresh
  Show
  txtDataField(0).SetFocus
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.APPLICATIONLIMIT_TABLE, _
      DatabaseName.APPLICATIONLIMIT_KEY, DatabaseName.APPLICATION_KEY, mApplicationLimitHandle, mApplicationHandle
End Sub

'
' Standard File/Save Menu for all Edit forms.
'
Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

'
' Standard View/Next Menu for all Edit forms.
'
Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

'
' Standard View/Previous Menu for all Edit forms.
'
Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub


Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

'
' Standard View/Toolbar Menu
'
Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub

Private Sub SSTab_Click(PreviousTab As Integer)
  TabControlEnable SSTab
End Sub

'
' Standard navigation toolbar.
'
Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

'
' Custom tracking of changes.
'
Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

'
' Custom tracking of changes.
'
Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

