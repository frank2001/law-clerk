VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmApplicationQuestionEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Question"
   ClientHeight    =   3150
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   5190
   Icon            =   "frmApplicationQuestionEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   5190
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2748
      Left            =   0
      ScaleHeight     =   2745
      ScaleWidth      =   5190
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   384
      Width           =   5196
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1428
         TabIndex        =   12
         Top             =   2376
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   2688
         TabIndex        =   13
         Top             =   2376
         Width           =   1188
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   3924
         TabIndex        =   14
         Top             =   2376
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   2196
         Left            =   84
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   72
         Width           =   5040
         _ExtentX        =   8890
         _ExtentY        =   3863
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&Question"
         TabPicture(0)   =   "frmApplicationQuestionEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "cboDescription"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtDataField(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "mskDataField(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtDataField(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtDataField(3)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "fraYesNo"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "&Options"
         TabPicture(1)   =   "frmApplicationQuestionEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Label1"
         Tab(1).Control(1)=   "chkDataField(0)"
         Tab(1).Control(2)=   "chkDataField(1)"
         Tab(1).Control(3)=   "chkDataField(2)"
         Tab(1).Control(4)=   "chkDataField(3)"
         Tab(1).Control(5)=   "chkDataField(4)"
         Tab(1).Control(6)=   "txtExclude"
         Tab(1).Control(7)=   "txtDataField(1)"
         Tab(1).ControlCount=   8
         Begin VB.Frame fraYesNo 
            Height          =   768
            Left            =   48
            TabIndex        =   23
            Top             =   756
            Width           =   720
            Begin VB.OptionButton optYesNo 
               Caption         =   "Yes"
               Height          =   204
               Index           =   0
               Left            =   36
               TabIndex        =   25
               Top             =   204
               Width           =   660
            End
            Begin VB.OptionButton optYesNo 
               Caption         =   "No"
               Height          =   204
               Index           =   1
               Left            =   36
               TabIndex        =   24
               Top             =   480
               Width           =   636
            End
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "isAnswer"
            Enabled         =   0   'False
            Height          =   288
            Index           =   3
            Left            =   2964
            TabIndex        =   22
            Text            =   "Hidden"
            Top             =   1644
            Visible         =   0   'False
            Width           =   744
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "answerAmount"
            Height          =   288
            Index           =   2
            Left            =   828
            TabIndex        =   4
            Text            =   "answerAmount"
            Top             =   1668
            Width           =   1224
         End
         Begin MSMask.MaskEdBox mskDataField 
            DataField       =   "questionId"
            Height          =   288
            Index           =   0
            Left            =   144
            TabIndex        =   1
            Top             =   408
            Width           =   552
            _ExtentX        =   953
            _ExtentY        =   503
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   2
            Mask            =   ">AA"
            PromptChar      =   "_"
         End
         Begin VB.TextBox txtDataField 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   288
            Index           =   1
            Left            =   -71796
            TabIndex        =   7
            Top             =   708
            Width           =   972
         End
         Begin VB.TextBox txtExclude 
            Height          =   288
            Left            =   -74820
            TabIndex        =   11
            Top             =   1776
            Width           =   4620
         End
         Begin VB.CheckBox chkDataField 
            Caption         =   "Exclude other entities"
            Height          =   198
            Index           =   4
            Left            =   -74820
            TabIndex        =   10
            Tag             =   "EL"
            Top             =   1500
            Width           =   3408
         End
         Begin VB.CheckBox chkDataField 
            Caption         =   "Grievance counsel coverage"
            Height          =   198
            Index           =   3
            Left            =   -74820
            TabIndex        =   9
            Tag             =   "GC"
            Top             =   1248
            Width           =   3336
         End
         Begin VB.CheckBox chkDataField 
            Caption         =   "Defendants reimbursement coverage"
            Height          =   198
            Index           =   2
            Left            =   -74820
            TabIndex        =   8
            Tag             =   "DF"
            Top             =   996
            Width           =   3456
         End
         Begin VB.CheckBox chkDataField 
            Caption         =   "SEC coverage"
            Height          =   198
            Index           =   1
            Left            =   -74820
            TabIndex        =   6
            Tag             =   "SC"
            Top             =   744
            Width           =   1584
         End
         Begin VB.CheckBox chkDataField 
            Caption         =   "Abstracter && title insurance agent coverage"
            Height          =   198
            Index           =   0
            Left            =   -74820
            TabIndex        =   0
            Tag             =   "AT"
            Top             =   492
            Width           =   4080
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "answerNote"
            Height          =   696
            Index           =   0
            Left            =   828
            MultiLine       =   -1  'True
            TabIndex        =   3
            Text            =   "frmApplicationQuestionEdit.frx":0044
            Top             =   840
            Width           =   3984
         End
         Begin VB.ComboBox cboDescription 
            DataField       =   "lawAreaId"
            Height          =   315
            ItemData        =   "frmApplicationQuestionEdit.frx":004F
            Left            =   828
            List            =   "frmApplicationQuestionEdit.frx":0051
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   408
            Width           =   3996
         End
         Begin VB.Label Label2 
            Caption         =   "Amount:"
            Height          =   288
            Left            =   84
            TabIndex        =   21
            Top             =   1680
            Width           =   636
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Cost per lawyer:"
            Height          =   192
            Left            =   -73080
            TabIndex        =   20
            Top             =   756
            Width           =   1140
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   5196
      _ExtentX        =   9155
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   5190
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   5070
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   30
         Width           =   5070
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   24
            TabIndex        =   18
            Top             =   24
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   1572
      TabIndex        =   19
      Top             =   456
      Width           =   1056
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   16777215
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   624
      Top             =   96
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator382 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator456 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmApplicationQuestionEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.71.00 (jjm) 2005 02 04 - GC default fix
'ver 2.20.0617 (jjm)
Option Explicit

Private mApplicationHandle As String
Private mQuestionHandle As String

Private mNew As Boolean
Private mDataSet As Dataset
Private mListView As ListView
Private mListIndex As Integer

Private mLawyerCount As Integer

' Get question for drop down.
Private mQuestionDataset As Dataset
Private mQuestionCollection As Collection

Private mDirty As Boolean
Public FormMgr As New FormManager

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Private Function DescriptionRefresh() As Boolean
  If mskDataField(0).Text = "__" Then
    cboDescription.ListIndex = -1
    DescriptionRefresh = True
    Exit Function
  End If
  
  If mQuestionCollection Is Nothing Then
    Exit Function
  End If
  
  
  Dim I, ListIndex As Integer
  For I = 1 To mQuestionCollection.Count
    If mQuestionCollection(I)(DatabaseName.QUESTION_KEY) = mskDataField(0).Text Then
      ListIndex = I
      Exit For
    End If
  Next I
  If ListIndex = 0 Then
    MsgBox "Invalid question id"
    Exit Function
  End If
  cboDescription.ListIndex = ListIndex - 1
  DescriptionRefresh = True
End Function

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  SSTab1.TabEnabled(1) = mNew
  SSTab1.TabVisible(1) = mNew
  
  If (mNew) Then
    ClearDataField txtDataField
    ClearDataField mskDataField
    txtDataField(3).Text = DB_TRUE_VALUE ' for Option Button
    
    'ver 2.71.00 (jjm) 2005 02 04
    'Greivance Counsel is a question that should be added
    'for every application so the checkbox is checkd by default.
    '(This should be a business rule.) When adding additional questions
    'the gc checkbox should not be checked if one already exisits.
    If Not FindQuestion(DatabaseName.GRIEVANCECOUNSEL_QUESTION) Then
      chkDatafield(3).Value = vbChecked ' for grievance checkbox
    End If
    
    SSTab1.Tab = 1
  Else
    Dim Datafields As Collection
    Set Datafields = Factory.ApplicationQuestionService.Find(mApplicationHandle, mQuestionHandle)
    SetFields txtDataField, Datafields
    SetFields mskDataField, Datafields
    
    'Reset the text note so it does not truncate text
    txtDataField(0).Text = GetDataFromCollection(Factory.ApplicationQuestionService.Find(mApplicationHandle, mQuestionHandle, DatabaseName.ANSWERNOTE_FIELD), DatabaseName.ANSWERNOTE_FIELD)
  End If
    
  DescriptionRefresh
  
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  Dim Application As Collection
  Set Application = Factory.ApplicationService.Find(mApplicationHandle)
  mLawyerCount = GetDouble(Application("lawyerPrincipalCount")) + GetDouble(Application("lawyerOfCounselCount")) + GetDouble(Application("lawyerOtherCount"))
  
  ' Set/Clear marks dirty.  Make clean.
  Dirty = False
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mApplicationHandle = ListItemText(mListView.SelectedItem, DatabaseName.APPLICATION_KEY)
  mQuestionHandle = ListItemText(mListView.SelectedItem, DatabaseName.QUESTION_KEY)
End Property


Private Function GetNote(QuestionHandle As String)
  Dim Note As String
  Note = mQuestionDataset.DataProvider.Find(DatabaseName.QUESTION_KEY, QuestionHandle)("questionNote")
  Select Case QuestionHandle
    Case "DF":
      Note = Replace(Note, "[amount]", Format(mLawyerCount * 30, "$#,##0"))
    Case "EL":
      Note = Replace(Note, "[exclude]", txtExclude.Text)
    Case "SC":
      Note = Replace(Note, "[amount]", Format(mLawyerCount * GetDouble(txtDataField(1).Text), "$#,##0"))
  End Select
  GetNote = Note
End Function

Private Function QuestionModels() As Collection
  Dim Result As New Collection
  Dim I As Integer
  Dim Question As QuestionModel
  
  'Add specific first if exists.
  If mskDataField(0) <> "__" Then
    Set Question = New QuestionModel
    Question.questionId = mskDataField(0)
    Question.answer = optYesNo(0)
    Question.answerNote = txtDataField(0)
    Question.answerAmount = GetDouble(txtDataField(2))
    Result.Add Question
  End If
  
  For I = 0 To chkDatafield.Count - 1
    If chkDatafield(I).Value = 1 Then
       Set Question = New QuestionModel
        Question.questionId = chkDatafield(I).Tag
        Question.answer = True
        If chkDatafield(I).Tag = "SC" Then
          Question.answerAmount = GetDouble(txtDataField(1).Text)
        End If
        Question.answerNote = GetNote(chkDatafield(I).Tag)
        
        Result.Add Question
    End If
  Next I
  
  Set QuestionModels = Result
End Function

'Query to see if the question id already exisits
Public Function FindQuestion(questionId As String) As Boolean
  mDataSet.DataProvider.Filter = _
      DatabaseName.APPLICATION_KEY & " = '" & mApplicationHandle & "' and " & _
      DatabaseName.QUESTION_KEY & " = '" & questionId & "'"
  mDataSet.DataProvider.OpenRecordset
  FindQuestion = Not mDataSet.DataProvider.IsEmpty
End Function

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't Save(True).
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Begin Custom Code
  Dim Hourglass As New WaitPointer
  Hourglass.Default
  
  If mNew Then
    Dim Models As Collection
    Set Models = QuestionModels
    
    If Not Models Is Nothing Then
      Dim I As Integer
      For I = 1 To Models.Count
        If FindQuestion(Models(I).questionId) Then
          MsgBox "Question id " & Models(I).questionId & " already exists."
          Exit Function
        End If
      Next I
      
      For I = 1 To Models.Count
        Dim Handle As Variant
        Handle = Factory.ApplicationQuestionService.CreateApplicationQuestion( _
              mApplicationHandle, _
              Models(I).questionId, _
              Models(I).answer, _
              Models(I).answerNote, _
              Models(I).answerAmount)
        save = Not VarType(Handle) = vbEmpty
        If save Then
          mApplicationHandle = Handle(0)
          mQuestionHandle = Handle(1)
          mNew = False
        End If
      Next I
    End If
  
    If Not save Then
      MsgBox "No new questions were saved"
    End If
    
  Else 'mNew = false
    save = Factory.ApplicationQuestionService.WriteApplicationQuestion( _
          mApplicationHandle, _
          mQuestionHandle, _
          mskDataField(0).Text, _
          optYesNo(0).Value, _
          txtDataField(0), _
          GetDouble(txtDataField(2)))
    If save Then
      mQuestionHandle = mskDataField(0).Text
    End If
  End If
    
  FormRefresh
  'End Custom Code
End Function

Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no questions to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(ApplicationId As String, questionId As String)
  mApplicationHandle = ApplicationId
  mQuestionHandle = questionId
  Show
End Sub


Public Sub ShowNew(ApplicationId As String)
  mApplicationHandle = ApplicationId
  mNew = True
  Show
  SSTab1.Tab = 1
  chkDatafield(0).SetFocus
End Sub


'
' End General
'


'
' Control Methods
'

Private Sub cboDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub cboDataField_Scroll(Index As Integer)
  Dirty = True
End Sub

Private Sub cboDescription_Click()
  'test to insure the combo box is loaded right
  If cboDescription.ListIndex < 0 Then
    mskDataField(0).Text = "__"
    Exit Sub
  End If
  'set msk field to collection
  mskDataField(0).Text = GetDataFromCollection(mQuestionCollection(cboDescription.ListIndex + 1), DatabaseName.QUESTION_KEY)
  If (mNew Or txtDataField(0).Text = vbNullString) Then
    txtDataField(0).Text = GetNote(mskDataField(0).Text)
  End If
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
End Sub

'
' Standard Cancel Command Button.
'
Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSave_Click()
  If save Then Unload Me
End Sub

Private Sub cmdSaveNew_Click()
  If save Then mnuFileNew_Click
End Sub

Private Sub Form_Initialize()
   'Set the Datasets Here
   
   Set mDataSet = New ApplicationQuestionDataset
   Set mDataSet.Session = LawSession
   
   Set mQuestionDataset = New GenericDataset
   Set mQuestionDataset.Session = LawSession
   mQuestionDataset.DataProvider.Source = DatabaseName.QUESTION_TABLE
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If

End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  ' Setup screen.
 
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
 ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  '
  ' Populate Description Dropdown
  '
  
  mQuestionDataset.DataProvider.OpenRecordset
  Set mQuestionCollection = mQuestionDataset.DataProvider.Collection
  If Not mQuestionCollection Is Nothing Then
    Dim I As Integer
    Dim item As ComboItem
    For I = 1 To mQuestionCollection.Count
      cboDescription.AddItem mQuestionCollection(I)("questionName")
    Next I
  Else
    'Exit the form if Question table is unable to be pulled
    MsgBox "Error: Unabled to retreive the Question list from the database", vbCritical
    Exit Sub
  End If
  
  
  FormUtil.TabControlEnable SSTab1
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not save(True) Then Exit Sub
  mNew = True
  mQuestionHandle = vbNullString
  mnuViewRefresh_Click
  'mskDataField(0).SetFocus
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.APPLICATIONQUESTION_TABLE, _
      DatabaseName.APPLICATION_KEY, DatabaseName.QUESTION_KEY, mApplicationHandle, mQuestionHandle
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub

Private Sub mskDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub mskDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect mskDataField(Index)
End Sub

Private Sub mskDataField_Validate(Index As Integer, Cancel As Boolean)
  If mskDataField(0) = "__" Then
    Exit Sub
  End If
  Cancel = Not DescriptionRefresh
End Sub

Private Sub optYesNo_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
  
  If ActiveControl Is Nothing Then
    chkDatafield(0).TabIndex = 0
  End If
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub


Private Sub txtDatafield_Change(Index As Integer)
  Select Case Index
    Case 1
      If txtDataField(1).Text <> "" Then
        chkDatafield(1).Value = vbChecked
      End If
    Case 3
      optYesNo(0).Value = GetBoolean(txtDataField(3).Text)
      optYesNo(1).Value = Not GetBoolean(txtDataField(3).Text)
  End Select
  Dirty = True
End Sub


'Private Function Validate() As Boolean
'  If Not mDirty Then
'    Validate = True
'    Exit Function
'  End If
'  Dim Result As Integer
'  Select Case MsgBox("Save " + IIf(mNew, "new", "changes to the") + " application law area?", vbYesNoCancel)
'    Case vbYes
'      Validate = Save
'    Case vbNo
'      Validate = True
'    Case vbNo
'      Validate = False
'  End Select
'End Function

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub
