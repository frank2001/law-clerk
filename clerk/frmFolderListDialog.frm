VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.ocx"
Begin VB.Form frmFolderListDialog 
   Caption         =   "Form Folder List Dialog"
   ClientHeight    =   5136
   ClientLeft      =   48
   ClientTop       =   288
   ClientWidth     =   8184
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5136
   ScaleWidth      =   8184
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   5628
      TabIndex        =   1
      Top             =   4656
      Width           =   1200
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   6888
      TabIndex        =   0
      Top             =   4656
      Width           =   1200
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4404
      Left            =   60
      TabIndex        =   2
      Top             =   132
      Width           =   8040
      _ExtentX        =   14182
      _ExtentY        =   7768
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   420
      TabCaption(0)   =   "Folder"
      TabPicture(0)   =   "frmFolderListDialog.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFolder"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame fraFolder 
         Caption         =   "Folder Location"
         Height          =   3948
         Left            =   60
         TabIndex        =   3
         Top             =   360
         Width           =   7872
         Begin VB.CommandButton cmdModify 
            Caption         =   "&Modify..."
            Height          =   336
            Left            =   6792
            TabIndex        =   4
            Top             =   456
            Width           =   996
         End
         Begin MSComctlLib.ListView lvFolder 
            Height          =   3360
            Left            =   108
            TabIndex        =   5
            Top             =   444
            Width           =   6612
            _ExtentX        =   11663
            _ExtentY        =   5927
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Name"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Location"
               Object.Width           =   5292
            EndProperty
         End
      End
   End
End
Attribute VB_Name = "frmFolderListDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOk As Boolean

Public Property Get OK() As Boolean
  OK = mOk
End Property

Private Sub cmdCancel_Click()
  Hide
  mOk = False
End Sub

Private Sub cmdModify_Click()
  Dim fFolder As New frmFolderBrowseDialog, Path As String
  Path = lvFolder.SelectedItem.ListSubItems(1).Text
  If fFolder.ShowSelect(, Path) Then
    lvFolder.SelectedItem.ListSubItems(1).Text = fFolder.FolderPath
    ListViewModule.AutosizeWidth lvFolder
  End If
  Unload fFolder
  lvFolder.SetFocus
End Sub

Private Sub cmdOK_Click()
  mOk = True
  Hide
End Sub

Private Sub lvFolder_DblClick()
  If cmdModify.Enabled Then
    cmdModify_Click
  End If
End Sub
