VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmClaimOpen 
   Caption         =   "Claim Summary"
   ClientHeight    =   7704
   ClientLeft      =   192
   ClientTop       =   816
   ClientWidth     =   10464
   Icon            =   "frmClaimOpen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7704
   ScaleWidth      =   10464
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6915
      Left            =   0
      ScaleHeight     =   6912
      ScaleWidth      =   10464
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   384
      Width           =   10464
      Begin VB.Frame Frame2 
         Caption         =   "Previous Incidents"
         Height          =   1080
         Left            =   8145
         TabIndex        =   111
         Top             =   2115
         Width           =   1830
         Begin VB.TextBox txtPrevIncident 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   750
            Left            =   75
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   112
            TabStop         =   0   'False
            Top             =   240
            Width           =   1710
         End
      End
      Begin VB.Frame fraCross 
         Caption         =   "Cross References"
         Height          =   2025
         Left            =   8130
         TabIndex        =   109
         Top             =   45
         Width           =   1830
         Begin VB.TextBox txtCrossRef 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   1725
            Left            =   72
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   110
            TabStop         =   0   'False
            Top             =   210
            Width           =   1710
         End
      End
      Begin VB.Frame fraPolicy 
         Caption         =   "Policy"
         Height          =   2028
         Left            =   5010
         TabIndex        =   84
         Top             =   48
         Width           =   3045
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Expiration Date:"
            Height          =   192
            Left            =   132
            TabIndex        =   98
            Top             =   1008
            Width           =   1128
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Aggregate:"
            Height          =   192
            Left            =   132
            TabIndex        =   97
            Top             =   1488
            Width           =   804
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "expirationDate"
            DataField       =   "expirationDate"
            DataMember      =   "Policy"
            Height          =   192
            Index           =   27
            Left            =   1332
            TabIndex        =   96
            Top             =   1008
            Width           =   1044
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            DataMember      =   "Policy"
            Height          =   192
            Index           =   32
            Left            =   1332
            TabIndex        =   95
            Top             =   768
            Width           =   948
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Left            =   132
            TabIndex        =   94
            Top             =   768
            Width           =   1032
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "policyNumber"
            DataField       =   "policyNumber"
            DataMember      =   "Policy"
            Height          =   195
            Index           =   31
            Left            =   1335
            TabIndex        =   93
            Top             =   285
            Width           =   960
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Policy No:"
            Height          =   195
            Left            =   135
            TabIndex        =   92
            Top             =   285
            Width           =   720
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Treaty Year:"
            Height          =   192
            Left            =   132
            TabIndex        =   91
            Top             =   528
            Width           =   888
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "treatyId"
            DataField       =   "treatyId"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Policy"
            Height          =   195
            Index           =   30
            Left            =   1335
            TabIndex        =   90
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   525
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "perClaimLimit"
            DataField       =   "perClaimLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   29
            Left            =   1332
            TabIndex        =   87
            Top             =   1248
            UseMnemonic     =   0   'False
            Width           =   972
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "aggregateLimit"
            DataField       =   "aggregateLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Policy"
            Height          =   192
            Index           =   28
            Left            =   1332
            TabIndex        =   86
            Top             =   1488
            UseMnemonic     =   0   'False
            Width           =   1080
         End
         Begin VB.Label Label23 
            AutoSize        =   -1  'True
            Caption         =   "Limit per claim:"
            Height          =   192
            Left            =   132
            TabIndex        =   85
            Top             =   1248
            Width           =   1056
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Claim Dates"
         Height          =   1080
         Left            =   84
         TabIndex        =   22
         Top             =   2115
         Width           =   7965
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Known Range:"
            Height          =   192
            Index           =   3
            Left            =   5340
            TabIndex        =   106
            Top             =   288
            Width           =   1032
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "knowDateRange"
            DataMember      =   "Instance"
            Height          =   192
            Index           =   34
            Left            =   6408
            TabIndex        =   105
            Top             =   288
            UseMnemonic     =   0   'False
            Width           =   336
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Occur Range:"
            Height          =   195
            Index           =   2
            Left            =   5340
            TabIndex        =   104
            Top             =   525
            Width           =   1005
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "occurDateRange"
            DataMember      =   "Instance"
            Height          =   192
            Index           =   21
            Left            =   6408
            TabIndex        =   103
            Top             =   528
            UseMnemonic     =   0   'False
            Width           =   336
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "occurDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   33
            Left            =   4305
            TabIndex        =   89
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Occur:"
            Height          =   195
            Index           =   1
            Left            =   3735
            TabIndex        =   88
            Top             =   525
            Width           =   450
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "knowDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   24
            Left            =   4305
            TabIndex        =   54
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Known:"
            Height          =   192
            Index           =   2
            Left            =   3732
            TabIndex        =   53
            Top             =   288
            Width           =   504
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "Instance"
            DataField       =   "closeDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   6
            Left            =   945
            TabIndex        =   32
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   615
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Coverage:"
            Height          =   195
            Index           =   1
            Left            =   132
            TabIndex        =   31
            Top             =   750
            Width           =   750
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "ReOpen:"
            Height          =   195
            Index           =   1
            Left            =   1980
            TabIndex        =   30
            Top             =   285
            Width           =   645
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Open:"
            Height          =   192
            Index           =   0
            Left            =   132
            TabIndex        =   29
            Top             =   288
            Width           =   432
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "coverageDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   15
            Left            =   948
            TabIndex        =   28
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "Instance"
            DataField       =   "openDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   13
            Left            =   945
            TabIndex        =   27
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   615
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "reOpenDate"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   12
            Left            =   2700
            TabIndex        =   26
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "claimMadeDate"
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   8
            Left            =   2700
            TabIndex        =   25
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Made:"
            Height          =   195
            Left            =   1980
            TabIndex        =   24
            Top             =   525
            Width           =   450
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Close:"
            Height          =   192
            Index           =   0
            Left            =   132
            TabIndex        =   23
            Top             =   528
            Width           =   456
         End
      End
      Begin VB.Frame fraClaim 
         Caption         =   "Claim"
         Height          =   2028
         Left            =   84
         TabIndex        =   11
         Top             =   48
         Width           =   4845
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "adjuster"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Instance"
            Height          =   195
            Index           =   9
            Left            =   1080
            TabIndex        =   102
            Top             =   1470
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "isTail"
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   20
            Left            =   1080
            TabIndex        =   101
            Top             =   1695
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label lblPolicyLimitClaimData 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "adjuster"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Left            =   3705
            TabIndex        =   100
            Top             =   1470
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Policy Limits Claim:"
            Height          =   195
            Index           =   2
            Left            =   2265
            TabIndex        =   99
            Top             =   1470
            Width           =   1320
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Is Reinsurer Report:"
            Height          =   195
            Left            =   2250
            TabIndex        =   82
            Top             =   1695
            Width           =   1410
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "isReinsurerReport"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   26
            Left            =   3705
            TabIndex        =   81
            Top             =   1695
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "perClaimDeductible"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   17
            Left            =   3705
            TabIndex        =   21
            Top             =   1230
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label22 
            AutoSize        =   -1  'True
            Caption         =   "Deductible:"
            Height          =   195
            Left            =   2250
            TabIndex        =   20
            Top             =   1230
            Width           =   810
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "data"
            DataField       =   "firmName"
            DataMember      =   "Instance"
            Height          =   195
            Index           =   3
            Left            =   1110
            TabIndex        =   19
            Top             =   288
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Firm Name:"
            Height          =   195
            Left            =   135
            TabIndex        =   18
            Top             =   285
            Width           =   795
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Claim Type:"
            Height          =   204
            Left            =   132
            TabIndex        =   17
            Top             =   1224
            Width           =   912
         End
         Begin VB.Label lblDataField 
            DataField       =   "claimant"
            DataMember      =   "Instance"
            Height          =   576
            Index           =   4
            Left            =   1116
            TabIndex        =   16
            Top             =   528
            UseMnemonic     =   0   'False
            Width           =   2844
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblClaimType 
            AutoSize        =   -1  'True
            Caption         =   "claimType"
            Height          =   195
            Left            =   1080
            TabIndex        =   15
            Top             =   1230
            UseMnemonic     =   0   'False
            Width           =   720
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Adjuster:"
            Height          =   192
            Index           =   0
            Left            =   144
            TabIndex        =   14
            Top             =   1464
            Width           =   624
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Claimant:"
            Height          =   192
            Left            =   132
            TabIndex        =   13
            Top             =   528
            Width           =   660
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Is Tail Claim:"
            Height          =   195
            Left            =   144
            TabIndex        =   12
            Top             =   1695
            Width           =   930
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   2925
         Top             =   2910
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   3495
         Top             =   2910
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   3540
         Left            =   90
         TabIndex        =   45
         Top             =   3300
         Width           =   9555
         _ExtentX        =   16849
         _ExtentY        =   6244
         _Version        =   393216
         Style           =   1
         Tabs            =   11
         TabsPerRow      =   11
         TabHeight       =   420
         WordWrap        =   0   'False
         TabCaption(0)   =   "Contact"
         TabPicture(0)   =   "frmClaimOpen.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvView(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Event"
         TabPicture(1)   =   "frmClaimOpen.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).Control(1)=   "optStatus(0)"
         Tab(1).Control(2)=   "optStatus(1)"
         Tab(1).Control(3)=   "optStatus(2)"
         Tab(1).Control(4)=   "optStatus(3)"
         Tab(1).ControlCount=   5
         TabCaption(2)   =   "Document"
         TabPicture(2)   =   "frmClaimOpen.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Reserve"
         TabPicture(3)   =   "frmClaimOpen.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lblDataField(23)"
         Tab(3).Control(1)=   "lblLabel(11)"
         Tab(3).Control(2)=   "lblDataField(22)"
         Tab(3).Control(3)=   "lblLabel(10)"
         Tab(3).Control(4)=   "lblDataField(19)"
         Tab(3).Control(5)=   "lblLabel(9)"
         Tab(3).Control(6)=   "lblDataField(18)"
         Tab(3).Control(7)=   "lblLabel(8)"
         Tab(3).Control(8)=   "lblLabel(13)"
         Tab(3).Control(9)=   "lblNetIncTotal"
         Tab(3).Control(10)=   "lvView(3)"
         Tab(3).ControlCount=   11
         TabCaption(4)   =   "Payment"
         TabPicture(4)   =   "frmClaimOpen.frx":007C
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "ilsClaim"
         Tab(4).Control(1)=   "lvView(4)"
         Tab(4).Control(2)=   "lblLabel(15)"
         Tab(4).Control(3)=   "lblDefendantReimbursement"
         Tab(4).Control(4)=   "lblGreivanceCounselCaption(0)"
         Tab(4).Control(5)=   "lblGreivanceCounsel"
         Tab(4).Control(6)=   "lblDataField(7)"
         Tab(4).Control(7)=   "lblLabel(4)"
         Tab(4).Control(8)=   "lblDataField(10)"
         Tab(4).Control(9)=   "lblLabel(5)"
         Tab(4).Control(10)=   "lblDataField(14)"
         Tab(4).Control(11)=   "lblLabel(6)"
         Tab(4).Control(12)=   "lblDataField(16)"
         Tab(4).Control(13)=   "lblLabel(7)"
         Tab(4).ControlCount=   14
         TabCaption(5)   =   "Deductible"
         TabPicture(5)   =   "frmClaimOpen.frx":0098
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "lblLabel(12)"
         Tab(5).Control(1)=   "lblDataField(25)"
         Tab(5).Control(2)=   "lblLabel(3)"
         Tab(5).Control(3)=   "lblDataField(5)"
         Tab(5).Control(4)=   "lblDataField(2)"
         Tab(5).Control(5)=   "lblLabel(2)"
         Tab(5).Control(6)=   "lblDataField(1)"
         Tab(5).Control(7)=   "lblLabel(1)"
         Tab(5).Control(8)=   "lblDataField(0)"
         Tab(5).Control(9)=   "lblLabel(0)"
         Tab(5).Control(10)=   "lvView(5)"
         Tab(5).ControlCount=   11
         TabCaption(6)   =   "Tortfeasor"
         TabPicture(6)   =   "frmClaimOpen.frx":00B4
         Tab(6).ControlEnabled=   0   'False
         Tab(6).Control(0)=   "lvView(6)"
         Tab(6).ControlCount=   1
         TabCaption(7)   =   "Law Area"
         TabPicture(7)   =   "frmClaimOpen.frx":00D0
         Tab(7).ControlEnabled=   0   'False
         Tab(7).Control(0)=   "lvView(7)"
         Tab(7).ControlCount=   1
         TabCaption(8)   =   "Law Error"
         TabPicture(8)   =   "frmClaimOpen.frx":00EC
         Tab(8).ControlEnabled=   0   'False
         Tab(8).Control(0)=   "lvView(8)"
         Tab(8).Control(1)=   "imlToolBarIcons"
         Tab(8).ControlCount=   2
         TabCaption(9)   =   "Incident"
         TabPicture(9)   =   "frmClaimOpen.frx":0108
         Tab(9).ControlEnabled=   0   'False
         Tab(9).Control(0)=   "lvView(9)"
         Tab(9).ControlCount=   1
         TabCaption(10)  =   "Unallocated"
         TabPicture(10)  =   "frmClaimOpen.frx":0124
         Tab(10).ControlEnabled=   0   'False
         Tab(10).Control(0)=   "lblCoverageCounselLabel"
         Tab(10).Control(1)=   "lblUnallocatedCoverageCounsel"
         Tab(10).Control(2)=   "Label18"
         Tab(10).Control(3)=   "lblUnallocatedOther"
         Tab(10).Control(4)=   "lvView(10)"
         Tab(10).ControlCount=   5
         Begin VB.OptionButton optStatus 
            Caption         =   "All"
            Height          =   240
            Index           =   3
            Left            =   -74916
            TabIndex        =   4
            Top             =   1332
            Width           =   912
         End
         Begin VB.OptionButton optStatus 
            Caption         =   "Void"
            Height          =   240
            Index           =   2
            Left            =   -74916
            TabIndex        =   3
            Top             =   1012
            Width           =   912
         End
         Begin VB.OptionButton optStatus 
            Caption         =   "Pending"
            Height          =   240
            Index           =   1
            Left            =   -74916
            TabIndex        =   2
            Top             =   692
            Width           =   948
         End
         Begin VB.OptionButton optStatus 
            Caption         =   "Complete"
            Height          =   240
            Index           =   0
            Left            =   -74916
            TabIndex        =   1
            Top             =   372
            Value           =   -1  'True
            Width           =   972
         End
         Begin MSComctlLib.ImageList ilsClaim 
            Left            =   -68712
            Top             =   60
            _ExtentX        =   804
            _ExtentY        =   804
            BackColor       =   -2147483643
            ImageWidth      =   74
            ImageHeight     =   15
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   2
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":0140
                  Key             =   ""
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":0EB4
                  Key             =   ""
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.ListView lvLetter 
            Height          =   2208
            Left            =   -74700
            TabIndex        =   37
            Top             =   600
            Width           =   6924
            _ExtentX        =   12234
            _ExtentY        =   3895
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   0
            Left            =   60
            TabIndex        =   0
            Tag             =   "ClaimClaimContact"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12425
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ImageList imlToolBarIcons 
            Left            =   -72528
            Top             =   2736
            _ExtentX        =   804
            _ExtentY        =   804
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   8
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":1C28
                  Key             =   "Open"
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":1D3A
                  Key             =   "Print"
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":1E4C
                  Key             =   "Copy"
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":1F5E
                  Key             =   "Delete"
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":2070
                  Key             =   "View Large Icons"
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":2182
                  Key             =   "View Small Icons"
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":2294
                  Key             =   "View List"
               EndProperty
               BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmClaimOpen.frx":23A6
                  Key             =   "View Details"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   6
            Left            =   -74940
            TabIndex        =   9
            Tag             =   "ClaimClaimLawyer"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12425
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   1
            Left            =   -73875
            TabIndex        =   44
            Tag             =   "ClaimEvent"
            Top             =   312
            Width           =   6036
            _ExtentX        =   10626
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   2
            Left            =   -74940
            TabIndex        =   5
            Tag             =   "ClaimClaimLetter"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12425
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2490
            Index           =   3
            Left            =   -73875
            TabIndex        =   6
            Tag             =   "ClaimClaimReserve"
            Top             =   312
            Width           =   6000
            _ExtentX        =   10583
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   4
            Left            =   -73875
            TabIndex        =   7
            Tag             =   "ClaimClaimPayment"
            Top             =   312
            Width           =   6264
            _ExtentX        =   11028
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   5
            Left            =   -74124
            TabIndex        =   8
            Tag             =   "ClaimDeductibleReimburse"
            Top             =   312
            Width           =   6240
            _ExtentX        =   11007
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   7
            Left            =   -74940
            TabIndex        =   46
            Tag             =   "ClaimClaimLawArea"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12425
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   8
            Left            =   -74940
            TabIndex        =   47
            Tag             =   "ClaimClaimLawError"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12425
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2496
            Index           =   9
            Left            =   -74940
            TabIndex        =   83
            Tag             =   "ClaimIncident"
            Top             =   312
            Width           =   7056
            _ExtentX        =   12425
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2490
            Index           =   10
            Left            =   -73875
            TabIndex        =   113
            Tag             =   "ClaimInstancePayment"
            Top             =   312
            Width           =   7050
            _ExtentX        =   12425
            _ExtentY        =   4382
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Def Reimb"
            Height          =   195
            Index           =   15
            Left            =   -74685
            TabIndex        =   121
            Top             =   2895
            Width           =   750
         End
         Begin VB.Label lblDefendantReimbursement 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   195
            Left            =   -74310
            TabIndex        =   120
            Top             =   3150
            Width           =   375
         End
         Begin VB.Label lblGreivanceCounselCaption 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Griev Counsel"
            Height          =   195
            Index           =   0
            Left            =   -74925
            TabIndex        =   119
            Top             =   2385
            Width           =   990
         End
         Begin VB.Label lblGreivanceCounsel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   195
            Left            =   -74310
            TabIndex        =   118
            Top             =   2640
            Width           =   375
         End
         Begin VB.Label lblUnallocatedOther 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   195
            Left            =   -74280
            TabIndex        =   117
            Top             =   1380
            Width           =   375
         End
         Begin VB.Label Label18 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Other"
            Height          =   195
            Left            =   -74295
            TabIndex        =   116
            Top             =   1125
            Width           =   390
         End
         Begin VB.Label lblUnallocatedCoverageCounsel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   195
            Left            =   -74280
            TabIndex        =   115
            Top             =   795
            Width           =   375
         End
         Begin VB.Label lblCoverageCounselLabel 
            Alignment       =   1  'Right Justify
            Caption         =   "Coverage Counsel"
            Height          =   375
            Left            =   -74910
            TabIndex        =   114
            Top             =   345
            Width           =   1005
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblNetIncTotal 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   195
            Left            =   -74325
            TabIndex        =   108
            Top             =   2400
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Net Inc Total"
            Height          =   195
            Index           =   13
            Left            =   -74880
            TabIndex        =   107
            Top             =   2190
            Width           =   930
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "paymentIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   7
            Left            =   -74310
            TabIndex        =   80
            Top             =   1110
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Pay Ind"
            Height          =   195
            Index           =   4
            Left            =   -74475
            TabIndex        =   79
            Top             =   855
            Width           =   540
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "paymentALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   10
            Left            =   -74310
            TabIndex        =   78
            Top             =   600
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Pay ALAE"
            Height          =   195
            Index           =   5
            Left            =   -74670
            TabIndex        =   77
            Top             =   345
            Width           =   735
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "deductibleIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   14
            Left            =   -74310
            TabIndex        =   76
            Top             =   2130
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Ded Ind"
            Height          =   195
            Index           =   6
            Left            =   -74490
            TabIndex        =   75
            Top             =   1875
            Width           =   570
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "deductibleALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   16
            Left            =   -74310
            TabIndex        =   74
            Top             =   1620
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Ded ALAE"
            Height          =   195
            Index           =   7
            Left            =   -74685
            TabIndex        =   73
            Top             =   1365
            Width           =   750
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Net Inc ALAE"
            Height          =   195
            Index           =   8
            Left            =   -74895
            TabIndex        =   72
            Top             =   1260
            Width           =   975
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "netIncurredALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   18
            Left            =   -74325
            TabIndex        =   71
            Top             =   1476
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Net Inc Ind"
            Height          =   195
            Index           =   9
            Left            =   -74760
            TabIndex        =   70
            Top             =   1728
            Width           =   795
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "netIncurredIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   19
            Left            =   -74325
            TabIndex        =   69
            Top             =   1932
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Res ALAE"
            Height          =   195
            Index           =   10
            Left            =   -74685
            TabIndex        =   68
            Top             =   345
            Width           =   750
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "reserveALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   22
            Left            =   -74325
            TabIndex        =   67
            Top             =   540
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Res Ind"
            Height          =   195
            Index           =   11
            Left            =   -74505
            TabIndex        =   66
            Top             =   804
            Width           =   555
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "reserveIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   195
            Index           =   23
            Left            =   -74325
            TabIndex        =   65
            Top             =   1008
            Width           =   375
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Advance"
            Height          =   192
            Index           =   0
            Left            =   -74796
            TabIndex        =   64
            Top             =   336
            Width           =   648
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "deductibleAdvance"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00;(""$""#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   192
            Index           =   0
            Left            =   -74520
            TabIndex        =   63
            Top             =   540
            Width           =   372
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Reimburse"
            Height          =   192
            Index           =   1
            Left            =   -74940
            TabIndex        =   62
            Top             =   804
            Width           =   792
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "deductibleReimburse"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00;(""$""#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   192
            Index           =   1
            Left            =   -74520
            TabIndex        =   61
            Top             =   1008
            Width           =   372
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Balance"
            Height          =   192
            Index           =   2
            Left            =   -74748
            TabIndex        =   60
            Top             =   1728
            Width           =   600
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "deductibleBalance"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00;(""$""#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   192
            Index           =   2
            Left            =   -74520
            TabIndex        =   59
            Top             =   1932
            Width           =   372
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "deductibleDirect"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00;(""$""#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   192
            Index           =   5
            Left            =   -74520
            TabIndex        =   58
            Top             =   2400
            Width           =   372
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Direct"
            Height          =   192
            Index           =   3
            Left            =   -74568
            TabIndex        =   57
            Top             =   2196
            Width           =   420
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1,000"
            DataField       =   "deductibleWriteOff"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00;(""$""#,##0.00)"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ClaimData"
            Height          =   192
            Index           =   25
            Left            =   -74532
            TabIndex        =   56
            Top             =   1476
            Width           =   372
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Write-off"
            Height          =   192
            Index           =   12
            Left            =   -74748
            TabIndex        =   55
            Top             =   1260
            Width           =   588
         End
         Begin VB.Label Label19 
            Caption         =   "Fee Dispute:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   41
            Top             =   1356
            Width           =   1656
         End
         Begin VB.Label Label17 
            Caption         =   "Judgement Claimant:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   40
            Top             =   1116
            Width           =   1656
         End
         Begin VB.Label Label15 
            Caption         =   "Disposition Name:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   39
            Top             =   876
            Width           =   1656
         End
         Begin VB.Label Label2 
            Caption         =   "Disposition Id:"
            Height          =   204
            Left            =   -74904
            TabIndex        =   38
            Top             =   636
            Width           =   1656
         End
      End
      Begin VB.CommandButton cmdEditRow 
         Caption         =   "Hidden Edit Row"
         Default         =   -1  'True
         Height          =   312
         Left            =   2616
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   1224
         Width           =   1416
      End
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   6828
      Top             =   2304
      _ExtentX        =   699
      _ExtentY        =   699
      _Version        =   393216
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   10464
      _ExtentX        =   18457
      _ExtentY        =   677
      BandBorders     =   0   'False
      _CBWidth        =   10464
      _CBHeight       =   384
      _Version        =   "6.7.9782"
      Child1          =   "picMenu"
      MinHeight1      =   336
      Width1          =   3504
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   984
      MinHeight2      =   336
      Width2          =   3012
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   312
      Width3          =   948
      NewRow3         =   0   'False
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   3636
         ScaleHeight     =   336
         ScaleWidth      =   2856
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   24
         Width           =   2856
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   312
            Left            =   0
            TabIndex        =   52
            Top             =   36
            Width           =   1116
            _ExtentX        =   1969
            _ExtentY        =   550
            ButtonWidth     =   1482
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   312
         Left            =   6648
         ScaleHeight     =   312
         ScaleWidth      =   3744
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   36
         Width           =   3744
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   0
            TabIndex        =   36
            Top             =   12
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   132
         ScaleHeight     =   336
         ScaleWidth      =   3348
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   24
         Width           =   3348
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   624
            Left            =   1860
            TabIndex        =   48
            Top             =   36
            Width           =   1044
            _ExtentX        =   1842
            _ExtentY        =   1101
            ButtonWidth     =   1588
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Claim"
                        Text            =   "Claim"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   624
            Left            =   996
            TabIndex        =   49
            Top             =   36
            Width           =   756
            _ExtentX        =   1334
            _ExtentY        =   1101
            ButtonWidth     =   1122
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Claim"
                        Text            =   "Claim"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Narrative"
                        Text            =   "WordPerfect Narrative"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   624
            Left            =   36
            TabIndex        =   50
            Top             =   36
            Width           =   888
            _ExtentX        =   1566
            _ExtentY        =   1101
            ButtonWidth     =   1207
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the claim."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   11
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Contact"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Event"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Document"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Reserve"
                     EndProperty
                     BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Payment"
                     EndProperty
                     BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Deductible"
                     EndProperty
                     BeginProperty ButtonMenu7 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Visible         =   0   'False
                        Text            =   "Tortfeasor"
                     EndProperty
                     BeginProperty ButtonMenu8 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Law Area"
                     EndProperty
                     BeginProperty ButtonMenu9 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Visible         =   0   'False
                        Text            =   "Law Error"
                     EndProperty
                     BeginProperty ButtonMenu10 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Incident"
                     EndProperty
                     BeginProperty ButtonMenu11 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "Unallocated Payment"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   276
      Left            =   0
      TabIndex        =   43
      Top             =   7428
      Width           =   10464
      _ExtentX        =   18457
      _ExtentY        =   487
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9970
            MinWidth        =   2117
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "1/14/2014"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "9:27 AM"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Claim"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeperator999 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "C&ontact"
            Index           =   0
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Event"
            Index           =   1
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "Do&cument"
            Index           =   2
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Reserve"
            Index           =   3
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Payment"
            Index           =   4
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Deductible"
            Index           =   5
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Tortfeasor"
            Enabled         =   0   'False
            Index           =   6
            Visible         =   0   'False
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "Law &Area"
            Index           =   7
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "La&w Error"
            Index           =   8
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Incident"
            Index           =   9
         End
         Begin VB.Menu mnuFileNewDetail 
            Caption         =   "&Unallocated Payment"
            Index           =   10
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   0
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Policy"
            Index           =   1
         End
      End
      Begin VB.Menu mnuSeparator834 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator326 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator332 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator8221 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditClaim 
         Caption         =   "&Claim"
      End
      Begin VB.Menu mnuEditNarrative 
         Caption         =   "WordPerfect &Narrative"
      End
      Begin VB.Menu mnuSeperator9999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuSeperator978 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Complete Row"
         Index           =   0
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Void Row"
         Index           =   1
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Pend Row"
         Index           =   2
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeparator111 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "&Tab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Deductible"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "E&vent"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Tortfeasor"
            Index           =   2
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Payment"
            Index           =   3
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Reserve"
            Index           =   4
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Contact"
            Index           =   5
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Letter"
            Index           =   6
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "Law &Area"
            Index           =   7
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "Law &Error"
            Index           =   8
         End
      End
      Begin VB.Menu mnuSeperator9090 
         Caption         =   "-"
      End
      Begin VB.Menu mnuChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "&Save Settings..."
      End
      Begin VB.Menu mnuSeparator282 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "Tool&bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator56465 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsClaim 
         Caption         =   "Browse &Claim"
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuToolsFirm 
         Caption         =   "Browse &Firm"
      End
      Begin VB.Menu mnuSeparator261 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeperator298 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "Web Merge"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator457 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupMerge 
         Caption         =   "&Merge"
      End
      Begin VB.Menu mnuPopupWebMerge 
         Caption         =   "Web Merge"
      End
      Begin VB.Menu mnuSeperator458 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSeperator469 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Complete"
         Index           =   0
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Void"
         Index           =   1
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Pend"
         Index           =   2
      End
   End
End
Attribute VB_Name = "frmClaimOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.71.01 (jjm) 2005 03 10 - hacked interface bug
'ver 2.70 (jjm) 2004 09 14
'previous ver 2.50.00 (jjm) 2004 02 10
'previous ver 2.20.0610 (jjm)
'previous ver 2.10.0326 (jjm)

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mClaimHandle As String
Private mFirmHandle As String
Private mPolicyHandle As String
Private mEventHandle As String
'cache claim number for display and name purposes
Private mClaimNumber As String

Private FormMgr As New FormManager

Private mListIndex As Integer
Private mListItemType As String
Private mTab As Integer
Private mListView As ListView


Private ContactData As Dataset
Private mReferenceView As Dataset
Private mLetter As Dataset
Private CLAIMRESERVE As Dataset
Private Payment As Dataset
Private mDeductible As Dataset
Private InstanceLawyer As Dataset
Private LawArea As Dataset
Private LawError As Dataset
Private IncidentData As Dataset
Private InstancePaymentData As Dataset

Private DispositionData As DataProvider
  
'ver 2.71.00 (jjm) 2005 02 01
'SuspendDiary - for functionality
Private mDirty As Boolean
  
Private Const ContactTab = 0
Private Const EventTab = 1
Private Const DocumentTab = 2
Private Const ReserveTab = 3
Private Const PaymentTab = 4
Private Const DeductibleTab = 5
Private Const LawyerTab = 6
Private Const LawAreaTab = 7
Private Const LawErrorTab = 8
Private Const IncidentTab = 9
Private Const UnallocatedTab = 10


Public Function EventFilter() As String
  Dim StatusFilter As String
  If optStatus(0).Value Then
    StatusFilter = LawDialect.Exists(DatabaseName.COMPLETEDATE_FIELD) & " and " & _
                   LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  ElseIf optStatus(1).Value Then
    StatusFilter = LawDialect.ColumnIsEmpty(DatabaseName.COMPLETEDATE_FIELD) & " and " & _
                   LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  ElseIf optStatus(2).Value Then
    StatusFilter = LawDialect.Exists(DatabaseName.VOIDDATE_FIELD)
  End If
  
  EventFilter = DatabaseName.EVENTTYPERULEID_FIELD & " != '" & DatabaseName.CLAIM_DOCUMENT & "' and " & _
                LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & _
                IIf(StatusFilter = vbNullString, vbNullString, " and " & StatusFilter)
End Function


'
' Called when a child is unloaded
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimSave
'      'Special actions for new document
'      If fInterface.Key = "New" & DatabaseName.EVENT_TABLE & "Doc" Then
'        'Setup for auto-complete
'        Dim Handles(0) As String
'        Dim ParameterNames() As String
'        Dim ParameterValues() As String
'        Handles(0) = fInterface.Child.EventHandle
'        'Automatically Complete the documents
'        Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
'      End If
      
      'Special Actions for insterting an event
      'ver 2.71.01 (jjm) 2005 03 10 - HACK: use form name instead of interace key
      If fInterface.Child.Name = "frmEventEdit" Then
        'When saving a claim diary entry set mDirty to false.
        'This is so the user isn't prompted a second time to set the diary.
        If fInterface.Child.EventTypeHandle = DatabaseName.CLAIMDIARY_TYPE Then
          mDirty = False
          Refresh
          FormRefresh
          Exit Sub
        End If
      End If
      
      'ver 2.71.00 (jjm) 2005 02 01
      mDirty = True
  End Select
 
  Refresh
  Show
  FormRefresh
  
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Dim Datafields As collection
  Set Datafields = Factory.ClaimService.Find(mClaimHandle)
  SetFields lblDataField, Datafields, "ClaimData"
  
  'cache claim number
  mClaimNumber = GetDataFromCollection(Datafields, DatabaseName.CLAIMNUMBER_FIELD, True)
  
  Dim claimType, claimTypeDescription As String
  claimType = GetDataFromCollection(Datafields, DatabaseName.CLAIMTYPE_FIELD)
  If claimType <> vbNullString Then   'Pull data from TableColumnType
    Dim TCTDataset As Dataset
    Dim TCTField(2) As String
    Dim TCTHandle(2) As String
    TCTField(0) = DatabaseName.TABLE_FIELD
    TCTHandle(0) = DatabaseName.CLAIM_TABLE
    TCTField(1) = DatabaseName.COLUMN_FIELD
    TCTHandle(1) = DatabaseName.CLAIMTYPE_FIELD
    TCTField(2) = DatabaseName.TYPE_FIELD
    TCTHandle(2) = claimType
    Set TCTDataset = NewGenericDataset(DatabaseName.TABLECOLUMNTYPE_TABLE)
    claimTypeDescription = GetDataFromCollection( _
                  TCTDataset.DataProvider.FindMulti(TCTField, TCTHandle, DatabaseName.DESCRIPTION_FIELD), _
                  DatabaseName.DESCRIPTION_FIELD)
  End If
  lblClaimType.Caption = claimTypeDescription
  
    
  Dim InstanceData As collection
  Set InstanceData = Factory.InstanceService.Find(mClaimHandle)
  SetFields lblDataField, InstanceData, "Instance"

  mEventHandle = GetDataFromCollection(InstanceData, DatabaseName.EVENT_KEY)
  
  
  'ver 2.73.00 (jjm) REF 235
  'set the display for Net Inc Total on the Reserve tab
  Dim netIncTot As Double
  netIncTot = GetDouble(GetDataFromCollection(Datafields, DatabaseName.NETINCURREDALAE_FIELD)) + _
              GetDouble(GetDataFromCollection(Datafields, DatabaseName.NETINCURREDINDEMNITY_FIELD))
  lblNetIncTotal.Caption = Format(netIncTot, lblNetIncTotal.DataFormat.Format)
  
  mFirmHandle = GetDataFromCollection(InstanceData, DatabaseName.FIRM_KEY)
  mPolicyHandle = GetDataFromCollection(InstanceData, DatabaseName.POLICY_KEY)
    
  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
  
  'Set event fields (ClaimType)
  SetFields lblDataField, Factory.EventService.Find(mEventHandle), "Event"
  
  If GetDataFromCollection(Datafields, DatabaseName.LAWDISPOSITION_KEY) <> "" Then
    SetFields lblDataField, DispositionData.Find(DatabaseName.LAWDISPOSITION_KEY, Datafields(DatabaseName.LAWDISPOSITION_KEY)), "LawDispositionData"
  End If
  
  SetFields lblDataField, Factory.FirmService.Find(mFirmHandle), "ContactFirm"
    
  'Set Policy Information 'ver 2.70 (jjm) 2004 09 14
  Dim PolicyDatafields As collection
  Set PolicyDatafields = Factory.PolicyService.Find(mPolicyHandle)
  SetFields lblDataField, PolicyDatafields, "Policy"
    
  Caption = "Claim Summary (" & mClaimNumber & ")"

  'ver 2.72.00 (jjm) 2004 04 19
  'Policy Limit Claim
  'Display Yes or No
  Dim references As collection
  mReferenceView.DataProvider.filter = LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & " and " & _
                                       LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD) & " and " & _
                                       LawDialect.EqualString(DatabaseName.EVENTTYPE_KEY, DatabaseName.POLICYLIMITSCLAIM_TYPE)
  mReferenceView.DataProvider.OpenRecordset
  Set references = mReferenceView.DataProvider.collection
  lblPolicyLimitClaimData.Caption = IIf(Not references Is Nothing, "Yes", "No")

  'ver 2.73.00 (jjm) REF 170
  'Display Cross Reference and Previous Incident Number
  txtCrossRef.Text = vbNullString
  mReferenceView.DataProvider.filter = LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & " and " & _
                                       LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD) & " and " & _
                                       LawDialect.EqualString(DatabaseName.EVENTTYPE_KEY, DatabaseName.CLAIMCROSSREFERENCE_TYPE)
  mReferenceView.DataProvider.OpenRecordset
  Dim crossRef As collection
  Set crossRef = mReferenceView.DataProvider.collection
  If (Not crossRef Is Nothing) Then
    Dim crossRefList() As String
    crossRefList = Split(crossRef(1)(DatabaseName.EVENTDESC_FIELD), " ")
    Dim I As Integer
    For I = 0 To SafeUBound(crossRefList)
      txtCrossRef.Text = txtCrossRef.Text + crossRefList(I) + vbCrLf
    Next I
  End If
  
  
  'Display Previous Incidents
  txtPrevIncident.Text = vbNullString
  IncidentData.DataProvider.filter = LawDialect.EqualString(DatabaseName.OTHERINSTANCEID_FIELD, mClaimHandle)
  IncidentData.DataProvider.OpenRecordset
  Dim prevInc As collection
  Set prevInc = IncidentData.DataProvider.collection
  If (Not prevInc Is Nothing) Then
    Dim j As Integer
    For j = 1 To prevInc.Count
      txtPrevIncident.Text = txtPrevIncident.Text + prevInc(j)(DatabaseName.INCIDENTNUMBER_FIELD) + vbCrLf
    Next j
  End If
  
  
  'Total Supplemental Payments
  Dim gcALAE, dfIndemnity As Double
  Dim claimCoverageData As Dataset
  Dim ccData As collection
  Set claimCoverageData = NewGenericDataset(DatabaseName.CLAIMCOVERAGE_VIEW)
  claimCoverageData.DataProvider.filter = LawDialect.EqualString(DatabaseName.CLAIM_KEY, mClaimHandle)
  claimCoverageData.DataProvider.Column = DatabaseName.COVERAGETYPE_KEY & "," & DatabaseName.PAYMENTALAE_FIELD & "," & DatabaseName.PAYMENTINDEMNITY_FIELD
  
  claimCoverageData.DataProvider.OpenRecordset
  Set ccData = claimCoverageData.DataProvider.collection
  
  gcALAE = 0
  dfIndemnity = 0
  
  If Not ccData Is Nothing Then
    Dim cvgType As String
    Dim k As Integer
    For k = 1 To ccData.Count
       cvgType = GetDataFromCollection(ccData(k), DatabaseName.COVERAGETYPE_KEY, True)
       Select Case cvgType
         Case DatabaseName.GREIVANCECOUNSEL_COVERAGETYPE
           gcALAE = gcALAE + GetDouble(GetDataFromCollection(ccData(k), DatabaseName.PAYMENTALAE_FIELD))
         Case DatabaseName.DEFENDANTSREIMBURSEMENT_COVERAGETYPE
           dfIndemnity = dfIndemnity + GetDouble(GetDataFromCollection(ccData(k), DatabaseName.PAYMENTINDEMNITY_FIELD))
       End Select
    Next k
  End If
  
  
  lblGreivanceCounsel.Caption = IIf(gcALAE <> 0, Format(gcALAE, lblGreivanceCounsel.DataFormat.Format), vbNullString)
  lblDefendantReimbursement.Caption = IIf(dfIndemnity <> 0, Format(dfIndemnity, lblDefendantReimbursement.DataFormat.Format), vbNullString)
  
  lblPolicyLimitClaimData.Caption = IIf(Not references Is Nothing, "Yes", "No")
  LawArea.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mClaimHandle)
  LawError.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mClaimHandle)
  Payment.DataProvider.filter = LawDialect.EqualString(DatabaseName.CLAIM_KEY, mClaimHandle)
  mDeductible.DataProvider.filter = LawDialect.EqualString(DatabaseName.CLAIM_KEY, mClaimHandle)
  InstanceLawyer.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mClaimHandle)
  CLAIMRESERVE.DataProvider.filter = LawDialect.EqualString(DatabaseName.CLAIM_KEY, mClaimHandle)
  ContactData.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mClaimHandle)
  IncidentData.DataProvider.filter = LawDialect.EqualString(DatabaseName.OTHERINSTANCEID_FIELD, mClaimHandle)
  'reset reference after it was used for gathering Cross References
  mReferenceView.DataProvider.filter = EventFilter
  
  mLetter.DataProvider.filter = LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & " and " & _
                                LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD) & " and " & _
                                LawDialect.EqualString(DatabaseName.EVENTTYPERULEID_FIELD, DatabaseName.CLAIM_DOCUMENT)
                                
  InstancePaymentData.DataProvider.filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mClaimHandle)
  
  InstancePaymentBucketRefresh

  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  mnuFileOpen(0).Enabled = Not (mFirmHandle = vbNullString)
  mnuFileOpen(1).Enabled = Not (mPolicyHandle = vbNullString)
  
  SSTabRefresh
  Refresh
  
  
  'ver 2.71.00 (jjm) 2005 02 01
  If mDirty Then
    SuspendDiary
  End If
  
End Sub

'bug 2613 Manual update on event type breakout displays on the Unallocated (InstancePayment) tab
Private Sub InstancePaymentBucketRefresh()

  'sum buckets for each event type to be totaled
  Dim OtherBucket As Double
  Dim CounselBucket As Double
  
  Dim iPayDataset As Dataset 'dataset to pull the InstancePaymentView.
  
  'pull the data
  Set iPayDataset = NewGenericDataset(DatabaseName.INSTANCEPAYMENT_VIEW)
  Dim filter As New filter
  filter.Add LawDialect.EqualString(DatabaseName.INSTANCE_KEY, mClaimHandle)
  filter.Add LawDialect.EqualString(DatabaseName.EVENTSTATUS_FIELD, "Complete")
  iPayDataset.DataProvider.filter = filter.filter
  iPayDataset.DataProvider.Column = DatabaseName.EVENTTYPE_KEY & ", " & DatabaseName.PAYMENTGENERALLEGAL_FIELD
  iPayDataset.DataProvider.OpenRecordset
  
  Dim iPayData As collection
  Set iPayData = iPayDataset.DataProvider.collection
  Dim tmpAmount As Double
  Dim I As Integer
  If Not iPayData Is Nothing Then
    For I = 1 To iPayData.Count
      tmpAmount = GetDouble(GetDataFromCollection(iPayData(I), DatabaseName.PAYMENTGENERALLEGAL_FIELD))
      Select Case GetDataFromCollection(iPayData(I), DatabaseName.EVENTTYPE_KEY)
        Case "ClmPay-UCC"
          CounselBucket = CounselBucket + tmpAmount
        Case "ClmPay-UO"
          OtherBucket = OtherBucket + tmpAmount
      End Select
    Next I
  End If
  lblUnallocatedCoverageCounsel.Caption = Format(CounselBucket, lblUnallocatedCoverageCounsel.DataFormat.Format)
  lblUnallocatedOther.Caption = Format(OtherBucket, lblUnallocatedOther.DataFormat.Format)
End Sub


'
' Set ListIndex from underlying view.  How we pull the field is
' dependent upon the list item type.  In this example, if we are
' opening from an event view, there is a different method for
' pulling the appliction id.
'
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.CLAIM_TABLE
      mClaimHandle = ListItemText(mListView.SelectedItem, DatabaseName.CLAIM_KEY)
    Case DatabaseName.EVENT_TABLE
      Dim vEventHandle As String
      vEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY), DatabaseName.INSTANCE_TABLE)
      mClaimHandle = GetDataFromCollection(Factory.InstanceService.FindByEvent(vEventHandle, DatabaseName.INSTANCE_KEY), DatabaseName.INSTANCE_KEY)
    Case Else
      MsgBox "Need to enable ClaimOpen to be loaded by: " & mListItemType
  End Select
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
  Case DeductibleTab  'mDeductible
    Set ListViewDataProvider = mDeductible.DataProvider
  Case EventTab  'Event
    Set ListViewDataProvider = mReferenceView.DataProvider
  Case LawyerTab  'Lawyer
    Set ListViewDataProvider = InstanceLawyer.DataProvider
  Case PaymentTab  'Payment
    Set ListViewDataProvider = Payment.DataProvider
  Case ReserveTab  'Reserve
    Set ListViewDataProvider = CLAIMRESERVE.DataProvider
  Case ContactTab  'Contact
    Set ListViewDataProvider = ContactData.DataProvider
  Case DocumentTab  'Letter
    Set ListViewDataProvider = mLetter.DataProvider
  Case LawAreaTab  'Law Area
    Set ListViewDataProvider = LawArea.DataProvider
  Case LawErrorTab  'Law Error
    Set ListViewDataProvider = LawError.DataProvider
  Case IncidentTab  'Incident
    Set ListViewDataProvider = IncidentData.DataProvider
  Case UnallocatedTab  'Unallocated
    Set ListViewDataProvider = InstancePaymentData.DataProvider
  End Select
End Function

'ver 2.10.0415 (jjm) WILMIC
Private Sub MenuRefresh()
  'Disable and enable menus
  
  'Browse Menu Items
  mnuEditBrowseRow.Enabled = ((SSTab.Tab = LawyerTab) Or _
                              (SSTab.Tab = ReserveTab))
  mnuPopupBrowse.Enabled = ((SSTab.Tab = LawyerTab) Or _
                            (SSTab.Tab = ReserveTab))
  
  'Event complete and void menu items
  Dim EventEnable As Boolean
  EventEnable = (SSTab.Tab = EventTab Or _
                 SSTab.Tab = DocumentTab Or _
                 SSTab.Tab = ReserveTab Or _
                 SSTab.Tab = PaymentTab Or _
                 SSTab.Tab = DeductibleTab Or _
                 SSTab.Tab = UnallocatedTab)
  mnuEditEventRow(0).Enabled = EventEnable
  mnuEditEventRow(1).Enabled = EventEnable
  mnuPopupEvent(0).Enabled = EventEnable
  mnuPopupEvent(1).Enabled = EventEnable
  
  'Merge Menu items
  Dim mergeEnable As Boolean
  mergeEnable = (SSTab.Tab = DocumentTab)
  mnuToolsMerge.Enabled = mergeEnable
  mnuToolsWebMerge.Enabled = mergeEnable
  mnuPopupMerge.Enabled = mergeEnable
  mnuPopupWebMerge.Enabled = mergeEnable
  
  'New menu items
  mnuPopupNew.Enabled = mnuFileNewDetail(SSTab.Tab).Enabled
  
  'Delete menu items
  'ver 2.10.0326 (jjm)
  mnuEditDeleteRow.Enabled = (SSTab.Tab = ContactTab)
  mnuPopupDelete.Enabled = (SSTab.Tab = ContactTab)
End Sub

Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.CLAIM_TABLE)
'  If View.ListItems.Count = 0 Then
'    MsgBox "There are no claims to view or edit.  To insert, press [Insert]."
'    Exit Sub
'  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(ClaimId As String, Optional ShowTab As Integer = 0)
  mClaimHandle = ClaimId
  mTab = ShowTab
  Show
End Sub

Public Sub SSTabRefresh()
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    If lvView(mTab).ListItems.Count > 0 Then
      Dim Index As Variant
      Index = IIf(lvView(mTab).SelectedItem.Key = "", lvView(mTab).SelectedItem.Index, lvView(mTab).SelectedItem.Key)
      lvView(mTab).Visible = False
      lvView(mTab).SelectedItem.Selected = False
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
      On Error Resume Next
      lvView(mTab).ListItems(Index).Selected = True
      lvView(mTab).Visible = True
      lvView(mTab).SetFocus
    Else
      ListViewLoad lvView(mTab), ListViewDataProvider(mTab)
    End If
  Else
    SSTab.Tab = mTab
  End If
  
  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
  MenuRefresh
  
End Sub

Public Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - 48, 0)
  lvView(Index).Width = Max(SSTab.Width - lvView(Index).Left - 48, 0)
End Sub

Private Sub SuspendDiary()
  
  'ver 2.71.00 (jjm) 2005 02 15
  'refresh diary status
  'Save a variable to determine if there is a pending diary
  
  Dim Refview As Dataset
  Set Refview = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
  Refview.DataProvider.filter = LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mEventHandle) & " and " & _
                                LawDialect.EqualString(DatabaseName.EVENTTYPE_KEY, DatabaseName.CLAIMDIARY_TYPE) & " and " & _
                                LawDialect.ColumnIsEmpty(DatabaseName.COMPLETEDATE_FIELD) & " and " & _
                                LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  Refview.DataProvider.Column = DatabaseName.EVENT_KEY
  Refview.DataProvider.OpenRecordset
  Dim mDiaryCol As collection
  Set mDiaryCol = Refview.DataProvider.collection
  
  'Only continue if there is an existing diary
  If Not mDiaryCol Is Nothing Then
    'Get the diary
    Dim mDiaryHandle As String
    mDiaryHandle = mDiaryCol(1)(DatabaseName.EVENT_KEY)
    
   
    If MsgBox("Do you want to advance the diary?", vbYesNo, "Diary") = vbYes Then
      'Set the diary date and make the call.
      Dim DiaryDate As Date
      DiaryDate = DateAdd("d", 21, Today)
      
      'ver 2.71.00 (jjm) 2005 02 15
      'display event edit rather than auto incrementing the diary date
      Dim fEvent As New frmEventEdit
      'ver 2.71.01 (jjm) 2005 03 10 - hacked interface bug - removed the interface key
      FormMgr.Add fEvent ', DatabaseName.EVENT_TABLE
      fEvent.ShowByPrimaryKey mDiaryHandle, DiaryDate
      'If Not Factory.EventService.setDueDate(DiaryId, DiaryDate) Then Exit Sub
    End If
  End If
  'reset the mDirty
  mDirty = False
End Sub


'
' End General Methods
'
Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdEditRow_Click()
  mnuEditEditRow_Click
End Sub

Private Sub Form_Activate()
  Form_Resize
  SSTabResize
  
End Sub

Private Sub Form_Initialize()
   'Initialzie the LawArea Dataset
   Set LawArea = NewGenericDataset(DatabaseName.INSTANCELAWAREA_VIEW)
   'Initialzie the LawError Dataset
   Set LawError = NewGenericDataset(DatabaseName.INSTANCELAWERROR_VIEW)
   'Initialize the Reference Dataset for documents
   Set mLetter = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
   'Initialzie the Payment Dataset
   Set Payment = NewGenericDataset(DatabaseName.CLAIMPAYMENT_VIEW)
   'Initialzie the DeductibleReimburse Dataset
   Set mDeductible = NewGenericDataset(DatabaseName.DEDUCTIBLEREIMBURSE_VIEW)
   
   'Initialize the InstanceLawyer Dataset
   Set InstanceLawyer = NewGenericDataset(DatabaseName.INSTANCELAWYER_VIEW)
   'Initialize the ClaimReserve Dataset
   Set CLAIMRESERVE = NewGenericDataset(DatabaseName.CLAIMRESERVE_VIEW)
   'Initialize Conctact Dataset
   Set ContactData = NewGenericDataset(DatabaseName.CONTACT_VIEW)
    
   'Initialize DispositionData dataset
   Set DispositionData = LawSession.NewDataProvider
   DispositionData.Source = DatabaseName.LAWDISPOSITION_TABLE
   
   'Initialize Event dataset
   Set mReferenceView = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
      
   'Initialize Incident dataset
   Set IncidentData = NewGenericDataset(DatabaseName.INCIDENT_VIEW)
   
   'Initialize InstancePayment dataset for the Unallocated tab
   Set InstancePaymentData = NewGenericDataset(DatabaseName.INSTANCEPAYMENT_VIEW)
   
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
  
  ' Custom Ctrl-Shift hotkeys - Inserts
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 And (Shift And vbAltMask) = 0 Then
    Select Case KeyCode
      Case vbKeyD 'Deductible
        mnuFileNewDetail_Click DeductibleTab
      Case vbKeyE 'Event
        mnuFileNewDetail_Click EventTab
      Case vbKeyT 'Tortfeasor
        mnuFileNewDetail_Click LawyerTab
      Case vbKeyP 'Payment
        mnuFileNewDetail_Click PaymentTab
      Case vbKeyR 'Reserve
        mnuFileNewDetail_Click ReserveTab
      Case vbKeyO 'Contact
        mnuFileNewDetail_Click ContactTab
      Case vbKeyL 'Letter
        mnuFileNewDetail_Click DocumentTab
    End Select
    Exit Sub
  End If
  
  
  Select Case KeyCode
    Case vbKeyInsert
      ' Custom insert key
      mnuFileNewDetail_Click SSTab.Tab
    Case vbKeyF1
      FormUtil.ShowHelp , Me.hWnd
  End Select
End Sub

Public Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  'Ensure first tab is open.
  If SSTab.Tab = mTab Then
    SSTab_Click mTab
  Else
    SSTab.Tab = mTab
  End If
  
    ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrInsert.ButtonHeight + 48
  
  ' Setup Toolbar.
  FormUtil.SizeSingleToolbar tbrInsert, 0
  FormUtil.SizeSingleToolbar tbrEdit, tbrInsert.Left + tbrInsert.Width
  FormUtil.SizeSingleToolbar tbrBrowse, tbrEdit.Left + tbrEdit.Width
  
  ' Custom Menu Shortcut Adjustments
  Me.KeyPreview = True
  
  mnuViewPrevious.Caption = mnuViewPrevious.Caption + Chr(9) + "Ctrl+<"
  mnuViewNext.Caption = mnuViewNext.Caption + Chr(9) + "Ctrl+>"
  
  mnuFileNewDetail(DeductibleTab).Caption = mnuFileNewDetail(DeductibleTab).Caption + vbTab + "Ctrl+Shift+D"
  mnuFileNewDetail(EventTab).Caption = mnuFileNewDetail(EventTab).Caption + vbTab + "Ctrl+Shift+E"
  mnuFileNewDetail(LawyerTab).Caption = mnuFileNewDetail(LawyerTab).Caption + vbTab + "Ctrl+Shift+T"
  mnuFileNewDetail(PaymentTab).Caption = mnuFileNewDetail(PaymentTab).Caption + vbTab + "Ctrl+Shift+P"
  mnuFileNewDetail(ReserveTab).Caption = mnuFileNewDetail(ReserveTab).Caption + vbTab + "Ctrl+Shift+R"
  mnuFileNewDetail(ContactTab).Caption = mnuFileNewDetail(ContactTab).Caption + vbTab + "Ctrl+Shift+O"
  mnuFileNewDetail(DocumentTab).Caption = mnuFileNewDetail(DocumentTab).Caption + vbTab + "Ctrl+Shift+L"
    
  'Hack for right aligned multi line label:
  lblCoverageCounselLabel.Caption = "Coverage" & vbCrLf & "Counsel"
  
  'Setup Form Manager
  FormMgr.Load Me
  
  FormUtil.EditLoadImages Me
    
  ' Refresh form with data
  FormRefresh
  
  FormUtil.TabControlEnable SSTab
  optStatus(1).Value = True
End Sub

Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub fraClaim_DblClick()
  FormUtil.ShowByPrimaryKey New frmClaimEdit, mClaimHandle, FormMgr
End Sub


Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
    Case vbKeyDelete
      mnuEditDeleteRow_Click
      KeyCode = 0
  End Select
End Sub

'ver 2.00.0304 (jjm) WILMIC
Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub

Private Sub mnuChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

'ver 2.00.0304 (jjm) WILMIC
Private Sub mnuEditBrowseRow_Click()
  Select Case SSTab.Tab
    Case DeductibleTab  'Deductible
    
    Case EventTab 'Event
    
    Case LawyerTab  'Lawyer
      Dispatcher.BrowseLawyerOpen ListItemText(lvView(LawyerTab).SelectedItem, DatabaseName.LAWYER_KEY)
    Case PaymentTab  'Payment
    
    Case ReserveTab  'Reserve
      Dispatcher.BrowseClaimReserveOpen ListItemText(lvView(ReserveTab).SelectedItem, DatabaseName.CLAIMRESERVE_KEY)
    Case ContactTab  'Contact
    
    Case DocumentTab  'Letter
    
    Case LawAreaTab  'Law Area
    
    Case LawErrorTab  'Law Error
  
  End Select

End Sub

Private Sub mnuEditClaim_Click()
  FormUtil.ShowByPrimaryKey New frmClaimEdit, mClaimHandle, FormMgr
End Sub

Private Sub mnuEditDeleteRow_Click()
  If MsgBox("Are you sure you want to delete the row?", vbYesNo, "Confirm Row Delete") = vbNo Then
    Exit Sub
  End If

  If lvView(SSTab.Tab).ListItems.Count < 1 Then
    MsgBox "There are no entries to delete."
    Exit Sub
  End If

  Select Case SSTab.Tab
    Case ContactTab
      'ver 2.10.0326
      Factory.ContactService.DeleteContact ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.CONTACT_KEY)
    Case EventTab
  End Select
  
  'ver 2.71.00 (jjm) 2005 02 01
  'Set dirty so the user will get diary suspend prompt
   mDirty = True
  
  FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case DeductibleTab  'Deductible
      FormUtil.ShowByListView frmDeductibleReimburseEdit, FormMgr, lvView(DeductibleTab)
    Case EventTab 'Event
      If Factory.EventService.IsEventTypeInGroup(ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.EVENTTYPE_KEY), DatabaseName.EVENT_TABLE) Then
        FormUtil.ShowByPrimaryKey New frmEventEdit, ListItemText(lvView(EventTab).SelectedItem, DatabaseName.EVENT_KEY), FormMgr, , DatabaseName.EVENT_TABLE
      Else
        FormUtil.ShowEventProperties ListItemText(lvView(EventTab).SelectedItem, DatabaseName.EVENT_KEY)
      End If
    Case LawyerTab  'Lawyer
      FormUtil.ShowByPrimaryKey New frmPersonEdit, ListItemText(lvView(LawyerTab).SelectedItem, DatabaseName.LAWYER_KEY), FormMgr
    Case PaymentTab  'Payment
      FormUtil.ShowByListView New frmClaimPaymentEdit, FormMgr, lvView(PaymentTab)
    Case ReserveTab  'Reserve
      FormUtil.ShowByListView New frmClaimReserveEdit, FormMgr, lvView(ReserveTab)
    Case ContactTab  'Contact
      FormUtil.ShowByListView New frmContactEdit, FormMgr, lvView(ContactTab)
    Case DocumentTab  'Letter
      FormUtil.ShowByListView New frmEventDocumentEdit, FormMgr, lvView(DocumentTab)
    Case LawAreaTab  'Law Area
      mnuFileNewDetail_Click LawAreaTab
    Case LawErrorTab  'Law Error
      mnuFileNewDetail_Click LawErrorTab
    Case IncidentTab
      FormUtil.ShowByListView New frmIncidentOpen, FormMgr, lvView(IncidentTab)
    Case UnallocatedTab
      FormUtil.ShowByListView New frmInstancePaymentEdit, FormMgr, lvView(UnallocatedTab)
  End Select
End Sub

Private Sub mnuEditEventRow_Click(Index As Integer)
  Dim Handles(0) As String
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  
  Handles(0) = ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.EVENT_KEY)
'  Factory.FolderParameterService.ParameterLoad ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.FOLDER_KEY), Index, ParameterNames, ParameterValues, True

  If Index = lawActionComplete Then
    If MsgBox("Are you sure you want to complete item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
    Else
      Exit Sub
    End If
  ElseIf Index = lawActionVoid Then
    If MsgBox("Are you sure you want to void item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetVoid Handles, ParameterNames, ParameterValues
    Else
      Exit Sub
    End If
  ElseIf Index = 2 Then
    If MsgBox("Are you sure you want to set the item to a pending status?", vbYesNo, "Set Event Status") = vbYes Then
      Factory.EventService.SetPending Handles, ParameterNames, ParameterValues
    End If
  End If
  
  'Get source type of event
  Dim sourceTypeId As String
  'First try to get the value from the list view. It may or may not be in the view control.
  sourceTypeId = ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.SOURCETYPE_KEY)
  'if not, pull the sourceTypeId from the database
  If sourceTypeId = vbNullString Then
    sourceTypeId = DataUtil.FindHandle(DatabaseName.EVENT_VIEW, DatabaseName.EVENT_KEY, Handles(0), DatabaseName.SOURCETYPE_KEY)
  End If
  
  'Mantis 2626 - ver 3.10 (jjm)
  'Only trigger suspend diary (setting mDirty = true) if the Event is
  'not a claim payment or deductible reimburse
  If sourceTypeId <> DatabaseName.CLAIMPAYMENT_TABLE And _
  sourceTypeId <> DatabaseName.DEDUCTIBLEREIMBURSE_TABLE Then
    'ver 2.71.00 (jjm) 2005 02 01
    'Set dirty so the user will get diary suspend prompt
     mDirty = True
  End If
  FormRefresh


End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuEditNarrative_Click()
  Factory.ClaimService.ShowNarrative mClaimHandle
  'ver 2.71.00 (jjm) 2005 02 01
  'No form refresh call so call suspend diary directly
  SuspendDiary
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNew_Click()
  BrowseNewClaim
End Sub

Private Sub mnuFileNewDetail_Click(Index As Integer)
  Select Case Index
    Case DeductibleTab  'mDeductible
      Dim frmDed As New frmDeductibleReimburseEdit
      FormMgr.Add frmDed
      frmDed.ShowNew mClaimHandle
    
    Case EventTab  'Event
      '2.71.01 (jjm) 2005 03 29 - form interface bug - removed the key
      Dim fEvent As New frmEventEdit
      FormMgr.Add fEvent ', DatabaseName.EVENT_TABLE
      fEvent.ShowNew DatabaseName.CLAIMEVENT_RULE, mEventHandle, mClaimNumber & " " & lblDataField(3).Caption
    Case LawyerTab  'Tortfeasor
      'No need to add a lawyer from this location
    Case PaymentTab  'Payment
      FormUtil.ShowNew New frmClaimPaymentEdit, FormMgr, mClaimHandle
    Case ReserveTab  'Reserve
      Dispatcher.BrowseClaimReserveNew mClaimHandle
    Case ContactTab  'Contact
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowNewByClaim mClaimHandle
    Case DocumentTab  'Document
      Dim fLetter As New frmEventDocumentEdit
      FormMgr.Add fLetter, "New" & DatabaseName.EVENT_TABLE & "Doc"
      fLetter.ShowNew DatabaseName.CLAIM_DOCUMENT, mEventHandle, lblDataField(3).Caption, , , mClaimHandle
    Case LawAreaTab
      FormUtil.ShowByPrimaryKey New frmClaimEdit, mClaimHandle, FormMgr, 2
    Case LawErrorTab
      FormUtil.ShowByPrimaryKey New frmClaimEdit, mClaimHandle, FormMgr, 3
    Case IncidentTab
      Dim fIncident As New frmIncidentEdit
      FormMgr.Add fIncident
      fIncident.ShowNew
    Case UnallocatedTab
      FormUtil.ShowNew New frmInstancePaymentEdit, FormMgr, mClaimHandle
  End Select
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0 'Firm
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
    Case 1 'Policy
      FormUtil.ShowByPrimaryKey New frmPolicyOpen, mPolicyHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.CLAIM_TABLE, mClaimHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.CLAIM_REF, DatabaseName.CLAIM_TABLE, mClaimHandle, mClaimNumber & " " & lblDataField(3).Caption
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  
  'ver 2.71.00 (jjm) 2005 02 01
  'Set dirty so the user will get diary suspend prompt
   mDirty = True
  
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub
Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuPopupBrowse_Click()
  mnuEditBrowseRow_Click
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupEvent_Click(Index As Integer)
  mnuEditEventRow_Click Index
End Sub

Private Sub mnuPopupMerge_Click()
  mnuToolsMerge_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNewDetail_Click mTab
End Sub

Private Sub mnuPopupWebMerge_Click()
  mnuToolsWebMerge_Click
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.CLAIM_TABLE, FormMgr
End Sub

Private Sub mnuToolsClaim_Click()
  Dispatcher.BrowseClaimOpen mClaimHandle
End Sub

Private Sub mnuToolsFirm_Click()
  Dispatcher.BrowseFirmOpen mFirmHandle
End Sub

Private Sub mnuToolsMerge_Click()
  Dim pointer As New WaitPointer, Handles(0) As String
  pointer.Default

  'Handles(0) = lvView(DocumentTab).SelectedItem.ListSubItems(DatabaseName.EVENT_KEY)
  Handles(0) = ListItemText(lvView(DocumentTab).SelectedItem, DatabaseName.EVENT_KEY)
  ' Print Envelopes
  Factory.EventRecipientService.PrintEnvelope Handles(0)

  ' Perform Document Merge
  Factory.EventService.DocumentMerge Handles
  
  'ver 2.71.00 (jjm) 2005 02 01
  'Set dirty so the user will get diary suspend prompt
   mDirty = True
  
  FormRefresh
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWebMerge_Click()
  Factory.EventService.WebMergeByView lvView(DocumentTab)
End Sub

Private Sub mnuViewNext_Click()
  ' No validate on Open forms
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  ' No validate on Open forms
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
'  Dim pointer As New WaitPointer
'  pointer.pointer = vbHourglass
  
'  SetHotKeys
End Sub

Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  picBody_Resize
End Sub

Private Sub optStatus_Click(Index As Integer)
  mReferenceView.DataProvider.filter = EventFilter
  
  SSTabRefresh
  If Not ActiveControl Is Nothing Then
    optStatus(Index).SetFocus
  End If
End Sub

Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub


'
' Implement Lazy Initialization
'
Private Sub SSTab_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab
  mTab = SSTab.Tab
  
  Dim Source As DataProvider
  Set Source = ListViewDataProvider(mTab)
  If Source Is Nothing Then Exit Sub

  ' Ensure list view is loaded.
  ListViewLoad lvView(mTab), Source
  
  sbStatusBar.Panels(1).Text = str(lvView(mTab).ListItems.Count) + " object(s)"
  sbStatusBar.Panels(2).Text = Source.Source
  
  MenuRefresh
  
  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub

Private Sub tbrAction_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileWorkflow_Click 0
End Sub

Private Sub tbrAction_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileWorkflow_Click ButtonMenu.Tag
End Sub

Private Sub tbrBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuToolsClaim_Click
End Sub

Private Sub tbrBrowse_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
  Case "Claim"
    mnuToolsClaim_Click
  Case "Firm"
    mnuToolsFirm_Click
  End Select
End Sub

Private Sub tbrEdit_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuEditClaim_Click
End Sub

Private Sub tbrEdit_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
    Case "Claim"
      mnuEditClaim_Click
    Case "Narrative"
      mnuEditNarrative_Click
  End Select
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuFileNewDetail_Click SSTab.Tab
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mnuFileNewDetail_Click ButtonMenu.Index - 1
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

