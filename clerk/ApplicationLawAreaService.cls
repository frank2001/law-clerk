VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ApplicationLawAreaService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const ServiceName = "ApplicationLawAreaService"

Private mClient As SoapClient30
Private mDataProvider As DataProvider
Private mHandleNames(1) As String


'
' Initialize Web Service
'
Public Sub Init(WSDLFile As String, EndPointURL As String, Session As Session)
  Set mDataProvider = Session.NewDataProvider(DatabaseName.APPLICATIONLAWAREA_TABLE)
  mHandleNames(0) = DatabaseName.APPLICATION_KEY
  mHandleNames(1) = DatabaseName.LAWAREA_KEY
  
On Error GoTo Exception
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Sub


'
' Data Access
'
Public Function Find(ApplicationHandle As String, LawAreaHandle As String, Optional Column As String) As Collection
  Dim Handles(1) As String
  Handles(0) = ApplicationHandle
  Handles(1) = LawAreaHandle
  Set Find = mDataProvider.FindMulti(mHandleNames, Handles)
End Function


'
' Create an application.
'
Public Function CreateApplicationLawArea( _
                          ApplicationId As String, _
                          LawAreaId As String, _
                          LawAreaPercent As Double) As Variant
    
On Error GoTo ErrorHandler
  
  CreateApplicationLawArea = mClient.Create( _
      ApplicationId, LawAreaId, LawAreaPercent)
  Exit Function
  
ErrorHandler:
'  MsgBox "Fault: " & mClient.FaultString, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
  
End Function


'
' Write an application.
'
Public Function WriteApplicationLawArea( _
                      ApplicationHandle As String, _
                      LawAreaHandle As String, _
                      LawAreaId As String, _
                      LawAreaPercent As Double) As Boolean
    
On Error GoTo ErrorHandler
  
  Call mClient.Write(ApplicationHandle, _
      LawAreaHandle, LawAreaId, LawAreaPercent)
  WriteApplicationLawArea = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Fault: " & mClient.FaultString, vbExclamation
  WriteApplicationLawArea = False
End Function

'
' Delete an ApplicationLawArea
'
Public Function DeleteApplicationLawArea( _
    ApplicationId As String, LawAreaId As String) As Boolean

On Error GoTo ErrorHandler
  Call mClient.Delete(ApplicationId, LawAreaId)
  DeleteApplicationLawArea = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
'  MsgBox "Fault: " & mClient.FaultString, vbExclamation
  DeleteApplicationLawArea = False
End Function
