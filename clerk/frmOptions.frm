VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMainOptionsDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Options"
   ClientHeight    =   5055
   ClientLeft      =   30
   ClientTop       =   330
   ClientWidth     =   5580
   Icon            =   "frmOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   5580
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "Options"
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   4248
      TabIndex        =   32
      Top             =   4584
      Width           =   1200
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   336
      Left            =   2964
      TabIndex        =   31
      Tag             =   "Cancel"
      Top             =   4584
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   1680
      TabIndex        =   30
      Tag             =   "OK"
      Top             =   4584
      Width           =   1200
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3828.617
      ScaleMode       =   0  'User
      ScaleWidth      =   5733.383
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         Height          =   2029
         Left            =   506
         TabIndex        =   17
         Tag             =   "Sample 4"
         Top             =   504
         Width           =   2039
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   2
      Left            =   -20000
      ScaleHeight     =   3828.617
      ScaleMode       =   0  'User
      ScaleWidth      =   5733.383
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Sample 3"
         Height          =   2029
         Left            =   407
         TabIndex        =   16
         Tag             =   "Sample 3"
         Top             =   405
         Width           =   2039
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3828.617
      ScaleMode       =   0  'User
      ScaleWidth      =   5733.383
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame Frame1 
         Caption         =   "Editing Options"
         Height          =   852
         Left            =   360
         TabIndex        =   18
         Top             =   240
         Width           =   2772
         Begin VB.CheckBox Check1 
            Caption         =   "Tab on Enter keystroke"
            Height          =   252
            Left            =   120
            TabIndex        =   19
            Top             =   360
            Value           =   1  'Checked
            Width           =   2052
         End
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4296
      Left            =   108
      TabIndex        =   33
      Top             =   132
      Width           =   5340
      _ExtentX        =   9419
      _ExtentY        =   7567
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   8
      TabHeight       =   420
      TabCaption(0)   =   "&General"
      TabPicture(0)   =   "frmOptions.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSample1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraSample1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame6"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "&Edit"
      TabPicture(1)   =   "frmOptions.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame5"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "En&velope"
      TabPicture(2)   =   "frmOptions.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame4"
      Tab(2).Control(1)=   "Frame2"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "&Search"
      TabPicture(3)   =   "frmOptions.frx":0060
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame7"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Advanced"
      TabPicture(4)   =   "frmOptions.frx":007C
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame8"
      Tab(4).ControlCount=   1
      Begin VB.Frame Frame8 
         Caption         =   "INI Conversion Utilities"
         Height          =   1305
         Left            =   -74835
         TabIndex        =   37
         Top             =   330
         Width           =   4956
         Begin VB.CommandButton cmdFolderINI 
            Caption         =   "Folder.ini..."
            Height          =   288
            Left            =   3792
            TabIndex        =   38
            Top             =   900
            Width           =   1028
         End
         Begin VB.Label Label1 
            Caption         =   $"frmOptions.frx":0098
            Height          =   612
            Left            =   132
            TabIndex        =   39
            Top             =   252
            Width           =   4656
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Advance Search Options"
         Height          =   1695
         Left            =   -74820
         TabIndex        =   34
         Top             =   336
         Width           =   4956
         Begin VB.CheckBox chkDebugSearch 
            Caption         =   "Display where clause of SQL when using advanced searches"
            Height          =   252
            Left            =   150
            TabIndex        =   36
            Top             =   288
            Width           =   4692
         End
         Begin VB.CheckBox chkDefaultToFirmPolicyContact 
            Caption         =   "Default to Firm Policy Contact only on Contact Searches"
            Height          =   252
            Left            =   144
            TabIndex        =   35
            Top             =   600
            Width           =   4692
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Search"
         Height          =   852
         Left            =   180
         TabIndex        =   29
         Top             =   2496
         Width           =   4956
         Begin VB.OptionButton optSearch 
            Caption         =   "Remember the last search by field used"
            Height          =   252
            Index           =   1
            Left            =   120
            TabIndex        =   7
            Top             =   492
            Width           =   4620
         End
         Begin VB.OptionButton optSearch 
            Caption         =   "Always use the default search by field"
            Height          =   252
            Index           =   0
            Left            =   120
            TabIndex        =   6
            Top             =   252
            Value           =   -1  'True
            Width           =   4620
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Text Field Behavior"
         Height          =   1032
         Left            =   -74820
         TabIndex        =   28
         Top             =   336
         Width           =   4956
         Begin VB.CheckBox chkEnterAsTab 
            Caption         =   "Enter key moves to next field"
            Height          =   252
            Left            =   144
            TabIndex        =   9
            Top             =   288
            Width           =   3252
         End
         Begin VB.CheckBox chkTextSelect 
            Caption         =   "Select the text on a field when you enter it"
            Height          =   252
            Left            =   144
            TabIndex        =   10
            Top             =   588
            Width           =   4650
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Envelope Printer"
         Height          =   948
         Left            =   -74832
         TabIndex        =   26
         Top             =   336
         Width           =   4956
         Begin VB.ComboBox cboPrinters 
            Height          =   288
            ItemData        =   "frmOptions.frx":0162
            Left            =   120
            List            =   "frmOptions.frx":0164
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   528
            Width           =   4716
         End
         Begin VB.Label Label3 
            Caption         =   "This selects which printer will be used for printing envelopes."
            Height          =   336
            Left            =   132
            TabIndex        =   27
            Top             =   252
            Width           =   4656
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "View"
         Height          =   684
         Left            =   180
         TabIndex        =   24
         Top             =   3420
         Width           =   4956
         Begin VB.TextBox txtItemsPerView 
            Height          =   288
            Left            =   2784
            TabIndex        =   8
            Top             =   252
            Width           =   564
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Maximum number of Items per view:"
            Height          =   192
            Left            =   168
            TabIndex        =   25
            Top             =   300
            Width           =   2520
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Default Envelope Settings"
         Height          =   1212
         Left            =   -74832
         TabIndex        =   22
         Top             =   1356
         Width           =   4956
         Begin VB.CommandButton cmdEnvelopeSettings 
            Caption         =   "Settings..."
            Height          =   288
            Left            =   3792
            TabIndex        =   12
            Top             =   804
            Width           =   1028
         End
         Begin VB.Label lblEnvelopeSettings 
            Caption         =   $"frmOptions.frx":0166
            Height          =   612
            Left            =   132
            TabIndex        =   23
            Top             =   252
            Width           =   4656
         End
      End
      Begin VB.Frame fraSample1 
         Caption         =   "Home Page"
         Height          =   1152
         Index           =   1
         Left            =   180
         TabIndex        =   21
         Tag             =   "Sample 1"
         Top             =   336
         Width           =   4956
         Begin VB.CommandButton cmdFolderModify 
            Caption         =   "&Modify..."
            Height          =   288
            Left            =   4044
            TabIndex        =   3
            Top             =   732
            Width           =   780
         End
         Begin VB.TextBox txtFolder 
            Height          =   288
            Left            =   348
            TabIndex        =   2
            Top             =   732
            Width           =   3552
         End
         Begin VB.OptionButton optHome 
            Caption         =   "Use Folder:"
            Height          =   252
            Index           =   1
            Left            =   120
            TabIndex        =   1
            Top             =   492
            Width           =   1392
         End
         Begin VB.OptionButton optHome 
            Caption         =   "Remember Last Page"
            Height          =   252
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Top             =   252
            Value           =   -1  'True
            Width           =   2652
         End
      End
      Begin VB.Frame fraSample1 
         Caption         =   "Web View"
         Height          =   864
         Index           =   0
         Left            =   180
         TabIndex        =   20
         Tag             =   "Sample 1"
         Top             =   1560
         Width           =   4956
         Begin VB.OptionButton optFolders 
            Caption         =   "Enable Web content in &folders"
            Height          =   252
            Index           =   0
            Left            =   120
            TabIndex        =   4
            Top             =   252
            Value           =   -1  'True
            Width           =   2652
         End
         Begin VB.OptionButton optFolders 
            Caption         =   "Use Windows &classic folders"
            Height          =   252
            Index           =   1
            Left            =   120
            TabIndex        =   5
            Top             =   492
            Width           =   2652
         End
      End
   End
End
Attribute VB_Name = "frmMainOptionsDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0429 (jjm)
Option Explicit


Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdEnvelopeSettings_Click()
  Dim fOptions As New frmEnvelopeOptions
    
  ' Hide unwanted fields
  fOptions.lblLabel(1).Visible = False
  fOptions.txtDeliveryPoint.Visible = False
   
  ' Load the saved defaults
  CommonDialogUtil.SetFont fOptions.dlgDeliveryFont, LocaleEnvelopeFont
  fOptions.dlgDeliveryFont.Color = LocaleEnvelopeForeColor
  fOptions.chkPrintBarcode.Value = LocaleEnvelopeBarCode
  fOptions.PaperSize = LocaleEnvelopePaperSize
  If Not LocaleEnvelopeCurrentY = 0 Then
    fOptions.txtDeliveryTop = FormatNumber((LocaleEnvelopeCurrentY / TwipsPerInch), 1)
  End If
  If Not LocaleEnvelopeCurrentX = 0 Then
    fOptions.txtDeliveryLeft = FormatNumber((LocaleEnvelopeCurrentX / TwipsPerInch), 1)
  End If
  
  ' Display the form
  fOptions.Show vbModal
  If Not fOptions.Ok Then
    Exit Sub
  End If
  
  ' Save new settings
  Set LocaleEnvelopeFont = CommonDialogUtil.GetFont(fOptions.dlgDeliveryFont)
  LocaleEnvelopeForeColor = fOptions.dlgDeliveryFont.Color
  LocaleEnvelopeBarCode = fOptions.chkPrintBarcode.Value
  LocaleEnvelopePaperSize = fOptions.PaperSize
  LocaleEnvelopeCurrentY = fOptions.GetDeliveryTopMargin
  LocaleEnvelopeCurrentX = fOptions.GetDeliveryLeftMargin
  
  Unload fOptions
    
  ' Write settings to ini
  IniMain.WriteFile "Envelope", UrlName.BARCODE_INI, str(LocaleEnvelopeBarCode)
  IniMain.WriteFile "Envelope", UrlName.PAPERSIZE_INI, str(LocaleEnvelopePaperSize)
  IniMain.WriteFile "Envelope", UrlName.CURRENTX_INI, str(LocaleEnvelopeCurrentX)
  IniMain.WriteFile "Envelope", UrlName.CURRENTY_INI, str(LocaleEnvelopeCurrentY)
  IniMain.WriteFile "Envelope", UrlName.FORECOLOR_INI, str(LocaleEnvelopeForeColor)
  ' All below are individual settings for the LocaleEnvelopeFont object
  IniMain.WriteFile "Envelope", UrlName.FONTNAME_INI, LocaleEnvelopeFont.Name
  IniMain.WriteFile "Envelope", UrlName.FONTSIZE_INI, LocaleEnvelopeFont.Size
  IniMain.WriteFile "Envelope", UrlName.FONTBOLD_INI, IIf(LocaleEnvelopeFont.Bold, "True", "False")
  IniMain.WriteFile "Envelope", UrlName.FONTITALIC_INI, IIf(LocaleEnvelopeFont.Italic, "True", "False")
  IniMain.WriteFile "Envelope", UrlName.FONTUNDERLINE_INI, IIf(LocaleEnvelopeFont.Underline, "True", "False")
  IniMain.WriteFile "Envelope", UrlName.FONTSTRIKETHROUGH_INI, IIf(LocaleEnvelopeFont.Strikethrough, "True", "False")
  
End Sub

Private Sub cmdFolderINI_Click()
   Dim fFolderIni As New frmConverter
   fFolderIni.Show vbModal, Me
   Unload fFolderIni
End Sub

Private Sub cmdFolderModify_Click()
  Dim fFolder As New frmFolderBrowseDialog, Path As String
  optHome(1).Value = True
  
  Path = txtFolder.Text
  If fFolder.ShowSelect(, Path) Then
    txtFolder.Text = fFolder.FolderPath
  End If
  Unload fFolder
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp HelpName.OPTIONS_TOPIC, Me.hWnd
End Sub

Private Sub cmdOk_Click()
  ' Update global variables
  LocaleEnterAsTab = chkEnterAsTab.Value
  LocaleEnableWebContent = optFolders(0).Value
  LocaleTextSelectOnFocus = chkTextSelect.Value
  LocaleItemsPerView = GetInteger(txtItemsPerView.Text)
  LocaleRememberLastSearch = optSearch(1).Value
  LocaleDefaultToFirmPolicyContact = chkDefaultToFirmPolicyContact.Value
  LocaleDebugAdvancedSearch = chkDebugSearch.Value
  
  ' Save settings
  IniMain.WriteFile UrlName.MAIN_INI, UrlName.ENTERASTAB_INI, IIf(LocaleEnterAsTab, "True", "False")
  IniMain.WriteFile UrlName.MAIN_INI, UrlName.ENABLEWEBCONTENT_INI, IIf(LocaleEnableWebContent, "True", "False")
  IniMain.WriteFile UrlName.MAIN_INI, UrlName.TEXTSELECTONFOCUS_INI, IIf(LocaleTextSelectOnFocus, "True", "False")
  IniMain.WriteFile UrlName.MAIN_INI, UrlName.ITEMSPERVIEW_INI, txtItemsPerView.Text
  IniMain.WriteFile UrlName.MAIN_INI, UrlName.REMEMBERLASTSEARCH_INI, IIf(LocaleRememberLastSearch, "True", "False")
  IniMain.WriteFile UrlName.MAIN_INI, UrlName.DEFAULTTOFIRMPOLICYCONTACT_INI, IIf(LocaleDefaultToFirmPolicyContact, "True", "False")
  IniMain.WriteFile UrlName.MAIN_INI, UrlName.DEBUGADVANCEDSEARCH_INI, IIf(LocaleDebugAdvancedSearch, "True", "False")
  
  IniForm.WriteFile UrlName.FRMMAIN_INI, UrlName.HOME_INI, IIf(optHome(0).Value, 0, 1)
  If optHome(1).Value = True Then
    IniForm.WriteFile UrlName.FRMMAIN_INI, UrlName.FOLDER_INI, txtFolder.Text
    IniForm.WriteFile UrlName.FRMMAIN_INI, UrlName.FOLDERKEY_INI, Factory.PreferenceService.FindFolder(SystemFolders_TreeName, txtFolder.Text)
  End If

  'Set the envelope printer and default envelope size. Then record to Ini
  DefaultEnvelopePrinter = cboPrinters.Text
  IniMain.WriteFile UrlName.MISC_INI, UrlName.ENVELOPEPRINTER_INI, DefaultEnvelopePrinter

  Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  Dim I As Integer
  
  chkEnterAsTab.Value = IIf(LocaleEnterAsTab, 1, 0)
  chkTextSelect.Value = IIf(LocaleTextSelectOnFocus, 1, 0)
  chkDefaultToFirmPolicyContact = IIf(LocaleDefaultToFirmPolicyContact, 1, 0)
  chkDebugSearch = IIf(LocaleDebugAdvancedSearch, 1, 0)
  
  If LocaleEnableWebContent = True Then
    optFolders(0).Value = True
  Else
    optFolders(1).Value = True
  End If
  
  optHome(GetInteger(IniForm.ReadFile(UrlName.FRMMAIN_INI, UrlName.HOME_INI))).Value = True
  txtFolder.Text = IniForm.ReadFile(UrlName.FRMMAIN_INI, UrlName.FOLDER_INI)
  
  txtItemsPerView.Text = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.ITEMSPERVIEW_INI)
  optSearch(IIf(LocaleRememberLastSearch, 1, 0)).Value = True
  
  'Load printer combo box
  For I = 0 To Printers.Count - 1
    cboPrinters.AddItem Printers(I).DeviceName
  Next
  
  'set the printer in combobox to the default envelope printer
  For I = 0 To Printers.Count - 1
    If Printers(I).DeviceName = DefaultEnvelopePrinter Then
      cboPrinters.ListIndex = I
      Exit For
    End If
  Next
  
End Sub


Private Sub txtFolder_Change()
  FormUtil.EditTextSelect txtFolder
End Sub

Private Sub txtItemsPerView_Validate(Cancel As Boolean)
  If Not (GetInteger(txtItemsPerView.Text) <= 1000 And _
          GetInteger(txtItemsPerView.Text) > -1 And _
          GetInteger(txtItemsPerView.Text) = GetDouble(txtItemsPerView.Text)) Then
    MsgBox "The maximum number of items per view must a whole number between 0 and 1000"
    txtItemsPerView.SetFocus
    SelAll txtItemsPerView
  End If
End Sub
