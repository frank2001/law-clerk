VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MemorySession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Description = "Establishes a connection to a data source."
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"


' Session
'
' The Session class establishes a Connection with a data provider.
'
' This session object uses a MemoryRecordset which provides all data.

Option Explicit
'Implements Session

Private ds As MemoryDataProvider
Private mSQLDialect As New SQLDialect



Private Function Session_NewDataProvider(Optional Source As String) As DataProvider
  Set Session_NewDataProvider = New MemoryDataProvider
End Function


Private Property Get Session_DateFormat() As String
End Property

Private Function Session_SharedRecordsetSupport() As Boolean
  Session_SharedRecordsetSupport = False
End Function

Private Function Session_GetSharedRecordset(Source As String, Key As Collection) As Variant
End Function

Private Function Session_Authenticate(Form As Object) As Boolean
End Function


Private Property Get Session_Session() As String
End Property

Private Property Get Session_SQLDialect() As SQLDialect
  Set Session_SQLDialect = mSQLDialect
End Property

Private Property Get Session_Connection() As String
End Property

