VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QuoteDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
Option Explicit

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer

Private Const DEFAULT_INDEX = UrlName.QUOTESEARCH_FIRMLOCATOR_INDEX

Private Sub Class_Initialize()
  ListIndex = DEFAULT_INDEX
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.QUOTE_VIEW
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in application identifier field only"
  frm.cboIn.AddItem "in firm identifier field only"
  frm.cboIn.AddItem "in firm entity locator field only"
  frm.cboIn.AddItem "in quote identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, DEFAULT_INDEX)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Select Case ListIndex
      Case UrlName.QUOTESEARCH_APPLICATIONID_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.APPLICATION_KEY, Words)
      Case UrlName.QUOTESEARCH_QUOTEID_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.QUOTE_KEY, Words)
      Case UrlName.QUOTESEARCH_FIRMID_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.FIRM_KEY, Words)
      Case UrlName.QUOTESEARCH_FIRMLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.FIRMENTITYLOCATOR_FIELD, Words)
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function
