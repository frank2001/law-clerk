VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FirmInsurerService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0408 (jjm)

Option Explicit
Private Const ServiceName = "FirmInsurerService"

Private mClient As SoapClient30
Private mDataProvider As DataProvider
Private mHandleNames(2) As String

'
' Initialize Web Service
'
Private Sub Class_Initialize()
  ' Set the Dataprovider 'ver 2.10.0410
  Set mDataProvider = LawSession.NewDataProvider(DatabaseName.FIRMINSURER_TABLE)
  
  ' Set HandleNames
  mHandleNames(0) = DatabaseName.FIRM_KEY
  mHandleNames(1) = DatabaseName.INSURER_KEY
  mHandleNames(2) = DatabaseName.EFFECTIVEDATE_FIELD
  
On Error GoTo Exception
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub

Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName

End Sub

'
' Data Access
'
Public Function Find(firmHandle As String, InsurerHandle As String, EffectiveHandle As Date, Optional Column As String) As Collection
  Dim Handles(2) As Variant
  Handles(0) = firmHandle
  Handles(1) = InsurerHandle
  Handles(2) = EffectiveHandle
  'MsgBox FormatDateTime(EffectiveHandle, )
  Set Find = mDataProvider.FindMulti(mHandleNames, Handles, Column)
End Function


'
' Interface to server
'

'
' Create a FirmInsurer record.
'
Public Function CreateFirmInsurer( _
    FirmId As String, insurerId As String, EffectiveDate As Date, _
    ExpirationDate As Date, PerClaimLimit As Double, AggregateLimit As Double, _
    Deductible As Double, Premium As Double, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
    
  CreateFirmInsurer = mClient.Create( _
      FirmId, insurerId, EffectiveDate, ExpirationDate, _
      PerClaimLimit, AggregateLimit, Deductible, Premium, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  Exit Function
  
ErrorHandler:
  'MsgBox "Fault: " & mClient.FaultString, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
  
End Function

'
' Write an application.
'
'ver 2.10.0408 (jjm) added IsurerID as a parameter and created SERVICE11
Public Function WriteFirmInsurer( _
    firmHandle As String, InsurerHandle As String, EffectiveHandle As Date, _
    insurerId As String, _
    EffectiveDate As Date, ExpirationDate As Date, _
    PerClaimLimit As Double, AggregateLimit As Double, _
    Deductible As Double, Premium As Double, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo ErrorHandler
    Call mClient.Write( _
        firmHandle, InsurerHandle, EffectiveHandle, insurerId, EffectiveDate, ExpirationDate, _
        PerClaimLimit, AggregateLimit, Deductible, Premium, _
        AttributeNames, AttributeValues, ParameterNames, ParameterValues)
    WriteFirmInsurer = True
    Exit Function
    
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Fault: " & mClient.FaultString, vbExclamation
  WriteFirmInsurer = False
End Function

'
' Delete an FirmInsurer
'
Public Function DeleteFirmInsurer(FirmId As String, insurerId As String, EffectiveDate As Date, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler
  Call mClient.Delete(FirmId, insurerId, EffectiveDate, ParameterNames, ParameterValues)
  DeleteFirmInsurer = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Fault: " & mClient.FaultString, vbExclamation
  DeleteFirmInsurer = False
End Function


'Test to see if the record is a true record or a virtual one created by the FirmInsurerView
Public Function validateInsurerRecord(insurerId As String) As Boolean
  Dim Result As Boolean
  Result = Not (insurerId = "WILMIC__")
  'Hack for WILMIC's FirmInsurerView. The value "WILMIC__" is only found in the view.
  If Not Result Then
    MsgBox "You can not edit previous insurer information for WILMIC"
  End If
  validateInsurerRecord = Result
End Function

