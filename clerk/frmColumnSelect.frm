VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmColumnSettingsDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Column Settings"
   ClientHeight    =   5115
   ClientLeft      =   30
   ClientTop       =   315
   ClientWidth     =   5070
   Icon            =   "frmColumnSelect.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5115
   ScaleWidth      =   5070
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   3696
      TabIndex        =   13
      Top             =   4656
      Width           =   1200
   End
   Begin VB.CommandButton cmdAdvanced 
      Caption         =   "Ad&vanced"
      Height          =   336
      Left            =   3672
      TabIndex        =   10
      Top             =   3924
      Width           =   1200
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select &All"
      Height          =   336
      Index           =   0
      Left            =   3672
      TabIndex        =   5
      Top             =   2408
      Width           =   1200
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select &None"
      Height          =   336
      Index           =   1
      Left            =   3672
      TabIndex        =   6
      Top             =   2812
      Width           =   1200
   End
   Begin VB.Frame fraLine 
      Height          =   36
      Left            =   120
      TabIndex        =   15
      Top             =   4464
      Width           =   4788
   End
   Begin VB.ComboBox cboItem 
      Height          =   288
      Left            =   156
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   3960
      Width           =   2124
   End
   Begin VB.CommandButton cmdProperties 
      Caption         =   "&Properties"
      Height          =   336
      Left            =   3672
      TabIndex        =   7
      Top             =   3216
      Width           =   1200
   End
   Begin VB.CommandButton cmdHide 
      Caption         =   "H&ide"
      Height          =   336
      Left            =   3672
      TabIndex        =   4
      Top             =   2004
      Width           =   1200
   End
   Begin VB.CommandButton cmdShow 
      Caption         =   "&Show"
      Height          =   336
      Left            =   3672
      TabIndex        =   3
      Top             =   1600
      Width           =   1200
   End
   Begin MSComctlLib.ListView lvColumn 
      Height          =   2772
      Left            =   168
      TabIndex        =   0
      Top             =   792
      Width           =   3348
      _ExtentX        =   5900
      _ExtentY        =   4895
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   2376
      TabIndex        =   12
      Top             =   4656
      Width           =   1200
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   1056
      TabIndex        =   11
      Top             =   4656
      Width           =   1200
   End
   Begin VB.CommandButton cmdItemDown 
      Caption         =   "Move &Down"
      Height          =   336
      Left            =   3672
      TabIndex        =   2
      Top             =   1196
      Width           =   1200
   End
   Begin VB.CommandButton cmdItemUp 
      Caption         =   "Move &Up"
      Height          =   336
      Left            =   3672
      TabIndex        =   1
      Top             =   792
      Width           =   1200
   End
   Begin VB.Label lblInstructions 
      AutoSize        =   -1  'True
      Caption         =   "Select the column you would like highlighted:"
      Height          =   192
      Index           =   1
      Left            =   144
      TabIndex        =   8
      Top             =   3696
      Width           =   3156
   End
   Begin VB.Label lblInstructions 
      Caption         =   $"frmColumnSelect.frx":000C
      Height          =   588
      Index           =   0
      Left            =   204
      TabIndex        =   14
      Top             =   108
      Width           =   4680
   End
End
Attribute VB_Name = "frmColumnSettingsDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 1.90.0203 (jjm)

Option Explicit

Private mOk As Boolean
Private mDirty As Boolean
Private mFolderName As String
Private mSource As String
Private mSaveAsDefault As Boolean

'
' Used with refresh columns
'
Public Sub AddItem(Index As Integer, Key As String, Text As String, Optional Alignment As Integer, Optional Width As Integer = 1440, Optional Position As Integer, Optional Tag As String, Optional Required As Boolean)
  cboItem.AddItem Text
  
  With lvColumn.ListItems.Add(, Key, Text)
    .Tag = Tag
  
    Dim SortKey As String
    SortKey = IIf(Position = 0, "B" & Text, "A" & Format(Position, "000"))
    .ListSubItems.Add , "SortKey", SortKey
    .ListSubItems.Add , "Index", Index
    .ListSubItems.Add , "Alignment", Alignment
    .ListSubItems.Add , "Width", Width
    .ListSubItems.Add , "Required", Required Or Right(Key, 2) = "Id"
    .Checked = (Position > 0) Or .ListSubItems("Required")
  End With
End Sub


'
' Refresh which buttons are enabled.
'
Public Sub ButtonRefresh()
  With lvColumn.SelectedItem
    cmdItemUp.Enabled = (.Index > 1)
    cmdItemDown.Enabled = (.Index < lvColumn.ListItems.Count)
    cmdShow.Enabled = Not .Checked
    cmdHide.Enabled = .Checked And Not .ListSubItems("Required")
  End With
End Sub


'
' Refresh list from dataset
'
Public Sub ColumnRefresh()
  Dim mDataSet As Dataset
  Set mDataSet = NewGenericDataset(mSource)
  mDataSet.DataProvider.OpenRecordset 1
  
  Dim mCollection As Collection
  Set mCollection = mDataSet.DataProvider.Collection
  
  If mCollection Is Nothing Then
    MsgBox "Cannot refresh columns. There are no records available in the " & _
      mDataSet.DataProvider.Source & " table."
    Exit Sub
  End If
  
  'Working summary strings to create list for message
  Dim AddSummary As String
  Dim RemoveSummary As String
  
  ' Scan through collection.
  Dim item As Variant
  For Each item In mCollection(1)
    If Not Contains(lvColumn.ListItems, item.Name) Then
      AddSummary = AddSummary & vbTab & item.Name & vbCrLf
      AddItem lvColumn.ListItems.Count + 1, item.Name, item.Name
    End If
  Next item
  
  ' Scan through list items to see if any should be removed.
  ' Must store keys to remove because we can't do it within a for each.
  Dim Remove As New Collection
  For Each item In lvColumn.ListItems
    'HACK: Keep any column that begins with ~ Bug#1648 ... And Left$(item.Key, 1) <> "~"
    If Not Contains(mCollection(1), item.Key) Then
      RemoveSummary = RemoveSummary & vbTab & item.Key & vbCrLf
      Remove.Add item
    End If
  Next item
  For Each item In Remove
    lvColumn.ListItems.Remove item.Index
  Next item
  
  If AddSummary = vbNullString And RemoveSummary = vbNullString Then
    MsgBox "Your columns are already in sync with the datasource."
  Else
    ErrorUtil.DisplayMessage "The columns have been synchronized with the database. Click on Details for more information.", _
                              "ADDED:" & vbCrLf & AddSummary & vbCrLf & _
                              "REMOVED:" & vbCrLf & RemoveSummary
  End If
  
  mDirty = True
End Sub


Public Property Get Dirty() As Boolean
  Dirty = mDirty
End Property

Public Property Let FolderName(vData As String)
  mFolderName = vData
End Property


Public Sub KillPersonal()
  Dim FileName As String
  FileName = FolderIniFileName(mFolderName, True)
  If FileUtil.IsFile(FileName) Then
    FileSystem.Kill FileName
  End If
End Sub


Public Property Get Ok() As Boolean
  Ok = mOk
End Property


Public Sub RestoreDefault()
  KillPersonal
  
  lvColumn.ListItems.Clear
  Dim Ini As New IniFile
  Ini.FileName = FolderIniFileActive(mFolderName)
  LoadItems Ini
  mDirty = False
End Sub


'
' Method requires that the section have at least one column.
'
Public Sub LoadItems(Ini As IniFile)
  ListViewModule.DeserializeColumnSettings Me, Ini, mFolderName
  
  lvColumn.SortKey = 1
  lvColumn.Sorted = True
  
  If lvColumn.ListItems.Count = 0 Then
    MsgBox "There are no current settings saved. Please refresh the columns in the advanced column settings window.", vbCritical, "Column Settings Error"
    Exit Sub
  End If
  lvColumn.ListItems(1).Selected = True

  ButtonRefresh
  lvColumn_ItemClick Nothing
  cboItem.ListIndex = 0
End Sub

Public Property Get SaveAsDefault() As Boolean
  SaveAsDefault = mSaveAsDefault
End Property

Public Property Let Source(vData As String)
  mSource = vData
End Property


'
' Swap simply swaps the SortKey of the selected item with the
' desired index
'
Private Sub Swap(Index As Integer)
  lvColumn.Sorted = False
  
  Dim Selected As ListItem
  Set Selected = lvColumn.SelectedItem
  
  Dim item As ListItem
  Set item = lvColumn.ListItems(Index)
  
  Dim SortKey As String
  SortKey = Selected.ListSubItems("SortKey")
  Selected.ListSubItems("SortKey") = item.ListSubItems("SortKey")
  item.ListSubItems("SortKey") = SortKey
  
  lvColumn.Sorted = True
  lvColumn.ListItems(Index).Selected = True
End Sub


Private Sub cmdAdvanced_Click()
  Dim Advanced As New frmColumnSettingsAdvancedDialog
  Set Advanced.ColumnSettingsDialog = Me
  Advanced.chkDefault.Value = IIf(mSaveAsDefault, 1, 0)
  Advanced.Show vbModal
  If Advanced.Ok Then
    mSaveAsDefault = Advanced.chkDefault
    'ver 1.90.0203 (jjm)
    If cboItem.ListCount > 0 And cboItem.ListIndex = -1 Then
      cboItem.ListIndex = 0
    End If
  End If
  Unload Advanced
  cmdOK.SetFocus
End Sub

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp HelpName.COLUMNSETTINGS_TOPIC
End Sub

Private Sub cmdHide_Click()
  lvColumn.SelectedItem.Checked = False
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdItemDown_Click()
  Swap lvColumn.SelectedItem.Index + 1
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdItemUp_Click()
  Swap lvColumn.SelectedItem.Index - 1
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdOk_Click()
  mOk = True
  Hide
End Sub

Private Sub cmdProperties_Click()
  Dim fProperties As New frmColumnPropertiesDialog
  Dim old As String
  fProperties.ShowListItem lvColumn.SelectedItem
  old = lvColumn.SelectedItem.Text
  fProperties.Show vbModal
  If fProperties.Dirty Then
    mDirty = True
    'Update the combo box if the column name has been changed
    Dim I As Integer
    For I = 0 To cboItem.ListCount
      If cboItem.List(I) = old Then
        cboItem.List(I) = lvColumn.SelectedItem.Text
        Exit For
      End If
    Next I
  End If
  lvColumn.SetFocus
  Unload fProperties
End Sub

Private Sub cmdSelect_Click(Index As Integer)
  Dim item As ListItem
  For Each item In lvColumn.ListItems
    If Not item.ListSubItems("Required") Then
      item.Checked = Index = 0
    End If
  Next item
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub cmdShow_Click()
  lvColumn.SelectedItem.Checked = True
  ButtonRefresh
  lvColumn.SetFocus
  mDirty = True
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  'The window standard for viewing properties
  If (KeyCode = vbKeyReturn) And (Shift = vbAltMask) Then
    cmdProperties_Click
    Exit Sub
  End If
  
  'Call for help
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  mDirty = False
End Sub

Private Sub lvColumn_DblClick()
  cmdProperties_Click
End Sub

Private Sub lvColumn_ItemCheck(ByVal item As MSComctlLib.ListItem)
  If Not item.Checked And item.ListSubItems("Required") Then
    Beep
    'MsgBox "The column you have selected is required. To hide the column set the column width to 0 under properties.", vbOKOnly, "Required Column"
    Select Case MsgBox("The column you have selected is required. To hide the column set the column width to 0." & Chr(10) & _
           "Would you like to set that column width to zero?", vbYesNo, "Required Column")
        Case vbYes
          item.ListSubItems("Width") = 0
          
    End Select
    item.Checked = True
  End If
  ButtonRefresh
End Sub

Private Sub lvColumn_ItemClick(ByVal item As MSComctlLib.ListItem)
  ButtonRefresh
End Sub

Public Property Get Column() As ListView
  Set Column = lvColumn
End Property
