VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFirmDiaryOpen 
   Caption         =   "Firm Diary"
   ClientHeight    =   7944
   ClientLeft      =   48
   ClientTop       =   1128
   ClientWidth     =   8520
   Icon            =   "frmFirmDiary.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7944
   ScaleWidth      =   8520
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   7290
      Left            =   0
      ScaleHeight     =   7296
      ScaleWidth      =   8520
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   384
      Width           =   8520
      Begin VB.Frame fraNote 
         Caption         =   "Firm Note"
         Height          =   1272
         Left            =   75
         TabIndex        =   73
         Top             =   2895
         Width           =   4164
         Begin VB.TextBox txtNote 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   996
            Left            =   72
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   74
            TabStop         =   0   'False
            Top             =   195
            Width           =   4008
         End
      End
      Begin VB.Frame fraFirm 
         Caption         =   "Firm"
         Height          =   2775
         Left            =   72
         TabIndex        =   50
         Top             =   72
         Width           =   4188
         Begin VB.Label Label26 
            AutoSize        =   -1  'True
            Caption         =   "Website:"
            Height          =   195
            Left            =   120
            TabIndex        =   88
            Top             =   2445
            Width           =   630
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "entityURL"
            DataField       =   "entityURL"
            DataMember      =   "Entity"
            Height          =   195
            Index           =   14
            Left            =   990
            TabIndex        =   87
            Top             =   2445
            UseMnemonic     =   0   'False
            Width           =   705
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "stateBarNumber"
            DataField       =   "stateBarNumber"
            DataMember      =   "Firm"
            Height          =   195
            Index           =   11
            Left            =   996
            TabIndex        =   80
            Top             =   2190
            Width           =   1140
         End
         Begin VB.Label Label24 
            AutoSize        =   -1  'True
            Caption         =   "State Bar:"
            Height          =   195
            Left            =   120
            TabIndex        =   79
            Top             =   2190
            Width           =   705
         End
         Begin VB.Label Label16 
            Caption         =   "Contact:"
            Height          =   192
            Left            =   120
            TabIndex        =   70
            Top             =   288
            Width           =   732
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "state"
            DataField       =   "state"
            DataMember      =   "Location"
            Height          =   192
            Index           =   7
            Left            =   2712
            TabIndex        =   69
            Top             =   1488
            UseMnemonic     =   0   'False
            Width           =   348
         End
         Begin VB.Label Label21 
            Caption         =   "Zip:"
            Height          =   195
            Left            =   3105
            TabIndex        =   68
            Top             =   1485
            Width           =   255
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "(608) 256-5680"
            DataField       =   "telephone"
            DataMember      =   "Location"
            Height          =   192
            Index           =   10
            Left            =   996
            TabIndex        =   67
            Top             =   1728
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "zip"
            DataField       =   "zip"
            DataMember      =   "Location"
            Height          =   192
            Index           =   8
            Left            =   3444
            TabIndex        =   66
            Top             =   1488
            UseMnemonic     =   0   'False
            Width           =   204
         End
         Begin VB.Label Label10 
            Caption         =   "State:"
            Height          =   192
            Left            =   2256
            TabIndex        =   65
            Top             =   1488
            Width           =   408
         End
         Begin VB.Label Label9 
            Caption         =   "Fax:"
            Height          =   192
            Left            =   2256
            TabIndex        =   64
            Top             =   1728
            Width           =   336
         End
         Begin VB.Label Label7 
            Caption         =   "Telephone:"
            Height          =   192
            Left            =   120
            TabIndex        =   63
            Top             =   1728
            Width           =   864
         End
         Begin VB.Label Label6 
            Caption         =   "City:"
            Height          =   192
            Left            =   120
            TabIndex        =   62
            Top             =   1488
            Width           =   360
         End
         Begin VB.Label Label4 
            Caption         =   "Address:"
            Height          =   192
            Left            =   120
            TabIndex        =   61
            Top             =   768
            Width           =   720
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "entityName"
            DataField       =   "entityName"
            DataMember      =   "PersonEntity"
            Height          =   195
            Index           =   0
            Left            =   1005
            TabIndex        =   60
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   795
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "addressLine2"
            DataField       =   "addressLine2"
            DataMember      =   "Location"
            Height          =   192
            Index           =   4
            Left            =   996
            TabIndex        =   59
            Top             =   1008
            UseMnemonic     =   0   'False
            Width           =   984
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "addressLine3"
            DataField       =   "addressLine3"
            DataMember      =   "Location"
            Height          =   192
            Index           =   5
            Left            =   996
            TabIndex        =   58
            Top             =   1248
            UseMnemonic     =   0   'False
            Width           =   984
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "city"
            DataField       =   "city"
            DataMember      =   "Location"
            Height          =   192
            Index           =   6
            Left            =   996
            TabIndex        =   57
            Top             =   1488
            UseMnemonic     =   0   'False
            Width           =   240
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "addressLine1"
            DataField       =   "addressLine1"
            DataMember      =   "Location"
            Height          =   192
            Index           =   3
            Left            =   996
            TabIndex        =   56
            Top             =   768
            UseMnemonic     =   0   'False
            Width           =   984
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "(608) 256-5680"
            DataField       =   "fax"
            DataMember      =   "Location"
            Height          =   192
            Index           =   9
            Left            =   2712
            TabIndex        =   55
            Top             =   1728
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "entityName"
            DataField       =   "entityName"
            DataMember      =   "Entity"
            Height          =   195
            Index           =   2
            Left            =   1005
            TabIndex        =   54
            Top             =   525
            UseMnemonic     =   0   'False
            Width           =   795
         End
         Begin VB.Label Label3 
            Caption         =   "Firm:"
            Height          =   192
            Left            =   120
            TabIndex        =   53
            Top             =   528
            Width           =   792
         End
         Begin VB.Label Label2 
            Caption         =   "E-Mail:"
            Height          =   192
            Left            =   120
            TabIndex        =   52
            Top             =   1968
            Width           =   552
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "email"
            DataField       =   "email"
            DataMember      =   "Location"
            Height          =   192
            Index           =   1
            Left            =   996
            TabIndex        =   51
            Top             =   1968
            UseMnemonic     =   0   'False
            Width           =   396
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Status"
         Height          =   2340
         Left            =   4332
         TabIndex        =   27
         Top             =   72
         Width           =   4128
         Begin VB.Label lblFirmDissolved 
            AutoSize        =   -1  'True
            Caption         =   "Firm Dissolved"
            Height          =   192
            Left            =   156
            TabIndex        =   84
            Top             =   2112
            Visible         =   0   'False
            Width           =   1080
         End
         Begin VB.Label Label20 
            AutoSize        =   -1  'True
            Caption         =   "Premium:"
            Height          =   192
            Left            =   156
            TabIndex        =   83
            Top             =   1848
            Width           =   672
         End
         Begin VB.Label lblNotOffering 
            AutoSize        =   -1  'True
            Caption         =   "Not Offering"
            Height          =   192
            Left            =   3108
            TabIndex        =   82
            Top             =   2112
            Visible         =   0   'False
            Width           =   840
         End
         Begin VB.Label lblDontSolicit 
            AutoSize        =   -1  'True
            Caption         =   "Don't Solicit"
            Height          =   192
            Left            =   1716
            TabIndex        =   81
            Top             =   2112
            Visible         =   0   'False
            Width           =   840
         End
         Begin VB.Label lblClaimCount 
            AutoSize        =   -1  'True
            Caption         =   "totalClaimCount"
            DataField       =   "totalClaimCount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            Height          =   192
            Left            =   768
            TabIndex        =   49
            Top             =   492
            UseMnemonic     =   0   'False
            Width           =   1116
         End
         Begin VB.Label Label18 
            AutoSize        =   -1  'True
            Caption         =   "Claims"
            Height          =   192
            Left            =   120
            TabIndex        =   48
            Top             =   252
            Width           =   492
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "insureStatusId"
            DataField       =   "insureStatusId"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   13
            Left            =   1188
            TabIndex        =   47
            Top             =   1644
            UseMnemonic     =   0   'False
            Width           =   1008
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Insure Status:"
            Height          =   192
            Left            =   156
            TabIndex        =   46
            Top             =   1644
            Width           =   948
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Payment"
            Height          =   192
            Left            =   2244
            TabIndex        =   45
            Top             =   732
            Width           =   636
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "totalPremium"
            DataField       =   "totalPremium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   44
            Left            =   1188
            TabIndex        =   44
            Top             =   1860
            UseMnemonic     =   0   'False
            Width           =   936
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "0"
            DataField       =   "grossIncurredALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   195
            Index           =   40
            Left            =   2130
            TabIndex        =   43
            Top             =   930
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "0"
            DataField       =   "grossIncurredIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   41
            Left            =   2136
            TabIndex        =   42
            Top             =   1104
            UseMnemonic     =   0   'False
            Width           =   84
         End
         Begin VB.Label lblPaymentPremium 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "0.00"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "ContactFirm"
            Height          =   192
            Left            =   3768
            TabIndex        =   41
            Top             =   1308
            UseMnemonic     =   0   'False
            Width           =   288
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "insureDuration"
            DataField       =   "insureDuration"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   21
            Left            =   3084
            TabIndex        =   40
            Top             =   1644
            UseMnemonic     =   0   'False
            Width           =   1032
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "0"
            DataField       =   "paymentIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   43
            Left            =   3972
            TabIndex        =   39
            Top             =   1092
            UseMnemonic     =   0   'False
            Width           =   84
         End
         Begin VB.Label lblIncurredPremium 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "0.00"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "ContactFirm"
            Height          =   192
            Left            =   1932
            TabIndex        =   38
            Top             =   1308
            UseMnemonic     =   0   'False
            Width           =   288
         End
         Begin VB.Label Label46 
            AutoSize        =   -1  'True
            Caption         =   "Years:"
            Height          =   192
            Left            =   2160
            TabIndex        =   37
            Top             =   1644
            Width           =   468
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Count:"
            Height          =   192
            Left            =   252
            TabIndex        =   36
            Top             =   492
            Width           =   444
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Incurred"
            Height          =   192
            Left            =   252
            TabIndex        =   35
            Top             =   732
            Width           =   576
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "ALAE"
            Height          =   192
            Left            =   396
            TabIndex        =   34
            Top             =   924
            Width           =   408
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "/ Premium"
            Height          =   192
            Left            =   396
            TabIndex        =   33
            Top             =   1308
            Width           =   744
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Indemnity"
            Height          =   192
            Left            =   396
            TabIndex        =   32
            Top             =   1116
            Width           =   684
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "ALAE"
            Height          =   192
            Left            =   2400
            TabIndex        =   31
            Top             =   924
            Width           =   408
         End
         Begin VB.Label Lable96 
            AutoSize        =   -1  'True
            Caption         =   "/ Premium"
            Height          =   192
            Left            =   2400
            TabIndex        =   30
            Top             =   1308
            Width           =   744
         End
         Begin VB.Label Label19 
            AutoSize        =   -1  'True
            Caption         =   "Indemnity"
            Height          =   192
            Left            =   2400
            TabIndex        =   29
            Top             =   1116
            Width           =   684
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "0"
            DataField       =   "paymentALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   42
            Left            =   3972
            TabIndex        =   28
            Top             =   900
            UseMnemonic     =   0   'False
            Width           =   84
         End
      End
      Begin VB.Frame fraInsurer 
         Caption         =   "Insurer"
         Height          =   1392
         Left            =   4332
         TabIndex        =   13
         Top             =   2436
         Width           =   4152
         Begin VB.Label Label25 
            AutoSize        =   -1  'True
            Caption         =   "Next Effective Date:"
            Height          =   192
            Left            =   144
            TabIndex        =   86
            Top             =   1104
            Width           =   1392
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "nextEffectiveDate"
            DataField       =   "nextEffectiveDate"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   12
            Left            =   1728
            TabIndex        =   85
            Top             =   1104
            UseMnemonic     =   0   'False
            Width           =   1248
         End
         Begin VB.Label Label22 
            Caption         =   "/"
            Height          =   192
            Left            =   960
            TabIndex        =   26
            Top             =   792
            Width           =   60
         End
         Begin VB.Label Label17 
            Caption         =   "-"
            Height          =   192
            Left            =   2892
            TabIndex        =   25
            Top             =   288
            Width           =   60
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "expirationDate"
            DataField       =   "expirationDate"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   28
            Left            =   3012
            TabIndex        =   24
            Top             =   288
            UseMnemonic     =   0   'False
            Width           =   1044
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "premium"
            DataField       =   "premium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   26
            Left            =   3348
            TabIndex        =   23
            Top             =   792
            UseMnemonic     =   0   'False
            Width           =   624
         End
         Begin VB.Label Label55 
            Caption         =   "Premium"
            Height          =   192
            Left            =   3336
            TabIndex        =   22
            Top             =   588
            Width           =   708
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "perClaimDeductible"
            DataField       =   "perClaimDeductible"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   25
            Left            =   1644
            TabIndex        =   21
            Top             =   792
            UseMnemonic     =   0   'False
            Width           =   1428
         End
         Begin VB.Label Label50 
            Caption         =   "Deductible"
            Height          =   192
            Left            =   2304
            TabIndex        =   20
            Top             =   588
            Width           =   864
         End
         Begin VB.Label lbldatafield 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "perClaimLimit"
            DataField       =   "perClaimLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   27
            Left            =   -72
            TabIndex        =   19
            Top             =   792
            UseMnemonic     =   0   'False
            Width           =   972
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "aggregateLimit"
            DataField       =   "aggregateLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            DataMember      =   "Firm"
            Height          =   192
            Index           =   24
            Left            =   1068
            TabIndex        =   18
            Top             =   792
            UseMnemonic     =   0   'False
            Width           =   1080
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            DataMember      =   "Firm"
            Height          =   192
            Index           =   29
            Left            =   2016
            TabIndex        =   17
            Top             =   288
            UseMnemonic     =   0   'False
            Width           =   948
         End
         Begin VB.Label lbldatafield 
            AutoSize        =   -1  'True
            Caption         =   "entityLocator"
            DataField       =   "entityLocator"
            DataMember      =   "InsurerEntity"
            Height          =   195
            Index           =   30
            Left            =   1020
            TabIndex        =   16
            Top             =   285
            UseMnemonic     =   0   'False
            Width           =   915
         End
         Begin VB.Label Label36 
            Caption         =   "Insurer Id:"
            Height          =   192
            Left            =   132
            TabIndex        =   15
            Top             =   288
            Width           =   696
         End
         Begin VB.Label Label23 
            Caption         =   "Limits"
            Height          =   192
            Left            =   744
            TabIndex        =   14
            Top             =   588
            Width           =   612
         End
      End
      Begin MSComctlLib.ImageList imlToolbarIcons 
         Left            =   4548
         Top             =   6156
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   9
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":000C
               Key             =   "New"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":011E
               Key             =   "Open"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":0230
               Key             =   "Print"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":0342
               Key             =   "Copy"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":0454
               Key             =   "Delete"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":0566
               Key             =   "View Large Icons"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":0678
               Key             =   "View Small Icons"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":078A
               Key             =   "View List"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFirmDiary.frx":089C
               Key             =   "View Details"
            EndProperty
         EndProperty
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   2910
         Left            =   75
         TabIndex        =   71
         Top             =   4275
         Width           =   8370
         _ExtentX        =   14774
         _ExtentY        =   5144
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   10
         TabHeight       =   420
         TabCaption(0)   =   "Event"
         TabPicture(0)   =   "frmFirmDiary.frx":09AE
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "dtpSince"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lvView(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "chkGroup(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "chkGroup(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkGroup(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkGroup(3)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkGroup(4)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkGroup(5)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cboStatus"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Lawyer"
         TabPicture(1)   =   "frmFirmDiary.frx":09CA
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Insurer"
         TabPicture(2)   =   "frmFirmDiary.frx":09E6
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lvView(2)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Claim"
         TabPicture(3)   =   "frmFirmDiary.frx":0A02
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lvView(3)"
         Tab(3).ControlCount=   1
         Begin VB.CommandButton cmdEditRow 
            Caption         =   "Hidden Edit Row"
            Default         =   -1  'True
            Height          =   312
            Left            =   5172
            TabIndex        =   75
            TabStop         =   0   'False
            Top             =   -492
            Width           =   1416
         End
         Begin VB.ComboBox cboStatus 
            Height          =   315
            ItemData        =   "frmFirmDiary.frx":0A1E
            Left            =   6720
            List            =   "frmFirmDiary.frx":0A2E
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   2088
            Width           =   1548
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Note"
            Height          =   192
            Index           =   5
            Left            =   6720
            TabIndex        =   9
            Tag             =   "Note"
            Top             =   1704
            Width           =   1548
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Billing"
            Height          =   192
            Index           =   4
            Left            =   6720
            TabIndex        =   8
            Tag             =   "Billing"
            Top             =   1452
            Width           =   1548
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Und Status"
            Height          =   192
            Index           =   3
            Left            =   6720
            TabIndex        =   5
            Tag             =   "UnderwritingStatus"
            Top             =   648
            Width           =   1416
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Document"
            Height          =   192
            Index           =   2
            Left            =   6720
            TabIndex        =   7
            Tag             =   "Document"
            Top             =   1188
            Width           =   1548
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Underwriting"
            Height          =   192
            Index           =   0
            Left            =   6720
            TabIndex        =   4
            Tag             =   "Underwriting"
            Top             =   384
            Width           =   1416
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Phone && Visit"
            Height          =   192
            Index           =   1
            Left            =   6720
            TabIndex        =   6
            Tag             =   "Visit"
            Top             =   912
            Width           =   1524
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2448
            Index           =   1
            Left            =   -74916
            TabIndex        =   1
            Tag             =   "FirmFirmLawyer"
            Top             =   360
            Width           =   8172
            _ExtentX        =   14415
            _ExtentY        =   4318
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2448
            Index           =   2
            Left            =   -74916
            TabIndex        =   2
            Tag             =   "FirmFirmInsurerView"
            Top             =   360
            Width           =   8172
            _ExtentX        =   14415
            _ExtentY        =   4318
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2448
            Index           =   3
            Left            =   -74916
            TabIndex        =   3
            Tag             =   "FirmClaim"
            Top             =   360
            Width           =   8148
            _ExtentX        =   14372
            _ExtentY        =   4318
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   2448
            Index           =   0
            Left            =   84
            TabIndex        =   0
            Tag             =   "FirmEvent"
            Top             =   360
            Width           =   6456
            _ExtentX        =   11367
            _ExtentY        =   4318
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin MSComCtl2.DTPicker dtpSince 
            Height          =   276
            Left            =   6720
            TabIndex        =   11
            Top             =   2496
            Width           =   1404
            _ExtentX        =   2477
            _ExtentY        =   466
            _Version        =   393216
            Format          =   79888385
            CurrentDate     =   37125
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   288
      Left            =   0
      TabIndex        =   72
      Top             =   7656
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   508
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6541
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "11/26/2013"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "9:56 AM"
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   76
      Top             =   0
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   677
      BandCount       =   1
      _CBWidth        =   8520
      _CBHeight       =   384
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   336
         ScaleWidth      =   8424
         TabIndex        =   77
         TabStop         =   0   'False
         Top             =   24
         Width           =   8424
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   24
            TabIndex        =   78
            Top             =   24
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   16777215
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   324
      Top             =   864
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   -108
      Top             =   4260
      _ExtentX        =   699
      _ExtentY        =   699
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNewFirm 
            Caption         =   "&Firm"
         End
         Begin VB.Menu mnuSeparator38262 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Event"
            Index           =   0
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Lawyer"
            Enabled         =   0   'False
            Index           =   1
            Visible         =   0   'False
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Insurer"
            Index           =   2
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator34121 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileEvent 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileEvent 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator342 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuFileSeperator001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditFirm 
         Caption         =   "&Firm"
      End
      Begin VB.Menu mnuSeparator922 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEditRow 
         Caption         =   "&Edit Row"
      End
      Begin VB.Menu mnuEditBrowseRow 
         Caption         =   "&Browse Row"
      End
      Begin VB.Menu mnuEditDeleteRow 
         Caption         =   "&Delete Row"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuSeperator369 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Complete Row"
         Index           =   0
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Void Row"
         Index           =   1
      End
      Begin VB.Menu mnuEditEventRow 
         Caption         =   "&Pend Row"
         Index           =   2
      End
      Begin VB.Menu mnuSeparator326 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditExport 
         Caption         =   "Expor&t Row..."
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "&Previous"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "&Next"
      End
      Begin VB.Menu mnuSperator6546 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewTabMenu 
         Caption         =   "&Tab"
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Event"
            Index           =   0
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Lawyer"
            Index           =   1
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Insurer"
            Index           =   2
         End
         Begin VB.Menu mnuViewTab 
            Caption         =   "&Claim"
            Index           =   3
         End
      End
      Begin VB.Menu mnuSeperator997 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuSeperator998 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeparator3841 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsBrowse 
         Caption         =   "&Browse Firm"
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuToolsBrowseWebsite 
         Caption         =   "Browse &Website"
      End
      Begin VB.Menu mnuSeparater3842 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSeperator3843 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "Web Merge"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeparator738 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupEdit 
         Caption         =   "&Edit"
      End
      Begin VB.Menu mnuPopupBrowse 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSeperator655 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSeperator656 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Complete"
         Index           =   0
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Void"
         Index           =   1
      End
      Begin VB.Menu mnuPopupEvent 
         Caption         =   "&Pend"
         Index           =   2
      End
      Begin VB.Menu mnuSeperator423 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupExport 
         Caption         =   "Expor&t"
      End
   End
End
Attribute VB_Name = "frmFirmDiaryOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.60.00 (jjm) 2004 03 29
'previous ver 2.40.1124 (jjm)
'previous ver 2.30.1014 (jjm)
'previous ver 2.20.0610 (jjm)
'previous ver 2.10.0408 (jjm)
Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
  
Private mFirmHandle As String
Private mOrganizationHandle As String
Private mEventHandle As String

Private mTab As Integer

Private FormMgr As New FormManager

Private mListIndex As Integer
Private mListView As ListView
Private mListViewType As String

Private mReference As Dataset
Private mReferenceView As Dataset
'Private ContactFirmData As Dataset
Private mFirmLawyer As Dataset
Private mClaim As Dataset
Private mFirmInsurer As Dataset

' Custom constants
Const LAWYER_AFFILIATED = "empty(isFormer)"

Const LBL_GROSSINCURREDALAE_INDEX = 40
Const LBL_GROSSINCURREDINDEMNITY_INDEX = 41
Const LBL_PAYMENTALAE_INDEX = 42
Const LBL_PAYMENTINDEMNITY_INDEX = 43
Const LBL_TOTALPREMIUM_INDEX = 44

Const DATAMEMBER_ENTITY = "Entity"
Const DATAMEMBER_FIRM = "Firm"
Const DATAMEMBER_INSURERENTITY = "InsurerEntity"
Const DATAMEMBER_LOCATION = "Location"
Const DATAMEMBER_PERSONENTITY = "PersonEntity"

Const TAB_EVENT = 0
Const TAB_LAWYER = 1
Const TAB_FIRMINSURER = 2
Const TAB_CLAIM = 3


'
' General Methods
'

Public Function EventFilter() As String
  
  'ver 2.60.00 (jjm) 2004 03 29
  'Filter for the eventTypeGroups
  Dim I As Integer
  Dim RuleIds As String
  Dim OrFilter As New filter
  'create a predicate containing a list of ruleIds.
  For I = 0 To chkGroup.Count - 1
    If chkGroup(I).Value = 1 Then
      RuleIds = IIf(RuleIds = vbNullString, vbNullString, RuleIds & " or ") & DatabaseName.RULE_KEY & " = '" & chkGroup(I).Tag & "'"
    End If
  Next I
  'use the ruleIds predicate to query the EventTypeRule table to get a
  'list of desired eventTypeIds
  If RuleIds <> vbNullString Then
    Dim ETRDataset As Dataset, EventTypes As collection
    Set ETRDataset = NewGenericDataset(DatabaseName.EVENTTYPERULE_TABLE)
    ETRDataset.DataProvider.filter = RuleIds
    'Set so it only returns eventTypeId fields
    ETRDataset.DataProvider.Column = DatabaseName.EVENTTYPE_KEY
    'fire the query
    ETRDataset.DataProvider.OpenRecordset
    'collect the events
    Set EventTypes = ETRDataset.DataProvider.collection
    'test for returned events
    If Not EventTypes Is Nothing Then
      Dim j As Integer, etPredicate As String
      For j = 1 To EventTypes.Count
        'build EventType predicate
        OrFilter.AddOr DatabaseName.EVENTTYPE_KEY & " = '" & GetDataFromCollection(EventTypes(j), DatabaseName.EVENTTYPE_KEY) & "'"
      Next j
    End If
  End If

  
  'ver 2.20.0610 (jjm)
  Dim StatusFilter As String
  StatusFilter = DatabaseName.CREATEDATE_FIELD & " >= " & LawDialect.DateFormat(dtpSince)
  If cboStatus.ListIndex < 1 Then 'Complete/Pending
    StatusFilter = StatusFilter & " and empty(" & DatabaseName.VOIDDATE_FIELD & ")"
  ElseIf cboStatus.ListIndex = 1 Then 'Completed
    StatusFilter = StatusFilter & " and exists(" & DatabaseName.COMPLETEDATE_FIELD & ") and empty(" & DatabaseName.VOIDDATE_FIELD & ")"
  ElseIf cboStatus.ListIndex = 2 Then 'Pending
    StatusFilter = StatusFilter & " and empty(" & DatabaseName.COMPLETEDATE_FIELD & ") and empty(" & DatabaseName.VOIDDATE_FIELD & ")"
  Else 'voided
    StatusFilter = StatusFilter & " and exists(" & DatabaseName.VOIDDATE_FIELD & ")"
  End If

  'ver 3.00.00
  EventFilter = DatabaseName.REFERENCEEVENTID_FIELD & " = '" & mEventHandle & "' and " & _
                StatusFilter & _
                IIf(OrFilter = vbNullString, vbNullString, " and (" & OrFilter & ")")
                
  'MsgBox EventFilter
  'Debug.Print EventFilter
End Function
  


'
' Called when a child is unloaded.  May have affected firm and detail so
' refresh all.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
      Exit Sub
    Case fimNew
       'new click
      Exit Sub
    Case fimSelect
'      mPolicyHandle = ListItemText(fInterface.Child.lvView.SelectedItem, databasename.POLICY_KEY)
'      mFirmHandle = ListItemText(fInterface.Child.lvView.SelectedItem, databasename.FIRM_KEY)
    Case fimSave
      If fInterface.Key = DatabaseName.EVENT_KEY & "New" Then
        Dim Handles(0) As String
        Dim ParameterNames() As String
        Dim ParameterValues() As String
        Dim vFolder As String
        Handles(0) = fInterface.Child.EventHandle
        vFolder = mReferenceView.DataProvider.Find(DatabaseName.EVENT_KEY, Handles(0))(DatabaseName.FOLDER_KEY)
'        Factory.FolderParameterService.ParameterLoad _
'          vFolder, _
'          lawActionComplete, ParameterNames, ParameterValues, True
                
        'Automatically Complete the items
        Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
      End If
  End Select
  
  FormRefresh
  Show
End Sub

'
' Called when the form is loaded from the FormInterface
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub

'
' Called when a parent is unloaded and the interface is still valid
'
Public Sub FormParentUnload()
  Set mListView = Nothing
  FormRefresh
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass

  Dim FirmDataFields As collection, EntityDatafields As collection
  Dim InsurerDatafields As collection, LocationDatafields As collection
  Dim OrganizationDatafields As collection, PersonDatafields As collection
  
  'Get the data
  Set FirmDataFields = Factory.FirmService.Find(mFirmHandle)
  Set EntityDatafields = Factory.EntityService.Find(mFirmHandle)
  Set OrganizationDatafields = Factory.OrganizationService.Find(mFirmHandle, DatabaseName.MAINPERSONID_FIELD)
  Set PersonDatafields = Factory.EntityService.Find(GetDataFromCollection(OrganizationDatafields, DatabaseName.MAINPERSONID_FIELD), DatabaseName.ENTITYNAME_FIELD)
  Set LocationDatafields = Factory.LocationService.Find(GetDataFromCollection(EntityDatafields, DatabaseName.MAINLOCATIONID_FIELD))
  Set InsurerDatafields = Factory.EntityService.Find(GetDataFromCollection(FirmDataFields, DatabaseName.LATESTINSURERID_FIELD))

  'Set the data
  SetFields lbldatafield, FirmDataFields, DATAMEMBER_FIRM
  SetFields lbldatafield, EntityDatafields, DATAMEMBER_ENTITY
  SetFields lbldatafield, LocationDatafields, DATAMEMBER_LOCATION
  SetFields lbldatafield, InsurerDatafields, DATAMEMBER_INSURERENTITY
  SetFields lbldatafield, PersonDatafields, DATAMEMBER_PERSONENTITY
  
  Caption = "Firm Diary - " & GetDataFromCollection(EntityDatafields, DatabaseName.ENTITYLOCATOR_FIELD) & " ( " & mFirmHandle & " )"
  
  'reset label fields if data was not populated
  Dim I As Integer
  For I = 40 To 44
    If lbldatafield(I).Caption = vbNullString Then
      lbldatafield(I).Caption = "0"
    End If
  Next I
  
  '2.30.1014 - changed "0.00" to "$0.00"
  If Not lbldatafield(LBL_GROSSINCURREDALAE_INDEX).Caption = vbNullString And _
     Not lbldatafield(LBL_GROSSINCURREDINDEMNITY_INDEX).Caption = vbNullString And _
     Not GetDouble(lbldatafield(LBL_TOTALPREMIUM_INDEX).Caption) = 0 Then
     
    lblIncurredPremium.Caption = Format((CCur(lbldatafield(LBL_GROSSINCURREDALAE_INDEX).Caption) + CCur(lbldatafield(LBL_GROSSINCURREDINDEMNITY_INDEX).Caption)) / CCur(lbldatafield(LBL_TOTALPREMIUM_INDEX).Caption), "##0.00")
  Else
    lblIncurredPremium.Caption = "0.00"
  End If

  If Not lbldatafield(LBL_PAYMENTALAE_INDEX).Caption = vbNullString And _
     Not lbldatafield(LBL_PAYMENTINDEMNITY_INDEX).Caption = vbNullString And _
     Not GetDouble(lbldatafield(LBL_TOTALPREMIUM_INDEX).Caption) = 0 Then
     
    lblPaymentPremium.Caption = Format((CCur(lbldatafield(LBL_PAYMENTALAE_INDEX).Caption) + CCur(lbldatafield(LBL_PAYMENTINDEMNITY_INDEX).Caption)) / CCur(lbldatafield(LBL_TOTALPREMIUM_INDEX).Caption), "##0.00")
  Else
    lblPaymentPremium.Caption = "0.00"
  End If
  
  mEventHandle = GetDataFromCollection(EntityDatafields, DatabaseName.EVENT_KEY)
  mnuToolsNotepad.Enabled = mEventHandle <> vbNullString
  If (mEventHandle = vbNullString) Then
    txtNote.Text = vbNullString
  Else
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
  End If
  
  'ver 2.00.0116 (jjm)
  'Refresh Flags
  'This relies on the ReferenceView
  Dim references As collection
'  mReferenceView.DataProvider.Filter = DatabaseName.REFERENCETABLE_FIELD & " = '" & DatabaseName.FIRM_TABLE & "' and """ & DatabaseName.REFERENCEID1_FIELD & " = '" & mFirmHandle & "'"
  mReferenceView.DataProvider.filter = DatabaseName.REFERENCEEVENTID_FIELD & " = '" & mEventHandle & "'"
  mReferenceView.DataProvider.OpenRecordset
  Set references = mReferenceView.DataProvider.collection
  lblDontSolicit.Visible = False
  lblFirmDissolved.Visible = False
  lblNotOffering.Visible = False
  'ver 2.01.0408 (jjm)
  If Not references Is Nothing Then
    Dim n As Integer
    For n = 1 To references.Count
      If references(n)(DatabaseName.VOIDDATE_FIELD) = vbNullString Then
        Select Case references(n)(DatabaseName.EVENTTYPE_KEY)
          Case DatabaseName.DONTSOLICIT_TYPE
            lblDontSolicit.Visible = True
          Case DatabaseName.FIRMDISSOLVED_TYPE
            lblFirmDissolved.Visible = True
          Case DatabaseName.NOTOFFERING_TYPE
            lblNotOffering.Visible = True
        End Select
      End If
    Next n
  End If
  
  mReferenceView.DataProvider.filter = EventFilter
  mFirmLawyer.DataProvider.filter = DatabaseName.LATESTFIRMID_FIELD & " = '" & mFirmHandle & "'"  '& " and " & LAWYER_AFFILIATED
  mFirmInsurer.DataProvider.filter = DatabaseName.FIRM_KEY & " = '" & mFirmHandle & "'"
  mClaim.DataProvider.filter = DatabaseName.FIRM_KEY & " = '" & mFirmHandle & "'"
   
  FormUtil.NavigationRefresh Me, mListView, mListIndex
 
  ' Refresh tab.
  SSTabRefresh

  'ver 2.40.1209 (jjm)
  'Claim Count
  Dim Claims As New collection
  mClaim.DataProvider.OpenRecordset
  Set Claims = mClaim.DataProvider.collection
  lblClaimCount.Caption = Claims.Count
        
End Sub

'
' Set list index of underlying view.  Used to navigate and set mFirmHandle.
'
Private Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListViewType
    Case "Firm"
      mFirmHandle = ListItemText(mListView.SelectedItem, DatabaseName.FIRM_KEY)
    Case "Event"
      'Find the firm's eventId
      Dim FirmEventHandle As String
      FirmEventHandle = Factory.ReferenceService.FindReferenceHandleByEventType(ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY), "Firm")
      If FirmEventHandle = vbNullString Then
        Unload Me
        Exit Property
      End If
      
      mFirmHandle = GetDataFromCollection(DataUtil.GenericFind(DatabaseName.ENTITY_TABLE, DatabaseName.EVENT_KEY, FirmEventHandle), DatabaseName.ENTITY_KEY)
  End Select
End Property

Private Function ListViewDataProvider(Index As Integer) As DataProvider
  Select Case Index
  Case 0
    Set ListViewDataProvider = mReferenceView.DataProvider
  Case 1
    Set ListViewDataProvider = mFirmLawyer.DataProvider
  Case 2
    Set ListViewDataProvider = mFirmInsurer.DataProvider
  Case 3
    Set ListViewDataProvider = mClaim.DataProvider
  End Select
End Function

Private Sub MenuRefresh()
  'Disable and enable Edit menus when Document Tab is selected
  Dim IsEventTab, IsBrowserTab, IsLawyerTab, IsInsurertab, IsClaimTab As Boolean
  IsEventTab = (SSTab.Tab = TAB_EVENT)
  IsInsurertab = (SSTab.Tab = TAB_FIRMINSURER)
  IsClaimTab = (SSTab.Tab = TAB_CLAIM)
  IsLawyerTab = SSTab.Tab = TAB_LAWYER
  
  IsBrowserTab = IsLawyerTab Or IsClaimTab 'lawyer or claim tab
   
  'Delete menus
  mnuEditDeleteRow.Enabled = IsInsurertab
  mnuPopupDelete.Enabled = IsInsurertab
  
  'Void and Complete menus
  mnuEditEventRow(0).Enabled = IsEventTab
  mnuEditEventRow(1).Enabled = IsEventTab
  mnuPopupEvent(0).Enabled = IsEventTab
  mnuPopupEvent(1).Enabled = IsEventTab
  
  'Browser menus
  mnuEditBrowseRow.Enabled = IsBrowserTab
  mnuPopupBrowse.Enabled = IsBrowserTab
  
  'New Menus
  mnuPopupNew.Enabled = Not IsLawyerTab
End Sub

Public Sub ShowByListView(View As ListView, Optional ListViewType As String = "Firm")
  If View.ListItems.Count = 0 Then
    MsgBox "There are no firms to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListViewType = ListViewType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

'
' Show by primary key.
'
Public Sub ShowByPrimaryKey(firmId As String, Optional Index As Integer)
  mFirmHandle = firmId
  mTab = Index
  Show
End Sub

'
' Refresh contents of tab.
'
Public Sub SSTabRefresh()
  Dim Source As DataProvider, View As ListView, Index As Variant
  Set View = lvView(mTab)
  Set Source = ListViewDataProvider(mTab)
  
  With View
    'Ensure first tab is open.
    If SSTab.Tab = mTab Then
      If .ListItems.Count > 0 Then
        Index = IIf(.SelectedItem.Key = "", .SelectedItem.Index, .SelectedItem.Key)
        .Visible = False
        .SelectedItem.Selected = False
        ListViewLoad View, Source
        On Error Resume Next
        .ListItems(Index).Selected = True
        .Visible = True
        .SetFocus
      Else
        ListViewLoad View, Source, True
      End If
    Else
      SSTab.Tab = mTab
    End If
    
    MenuRefresh
     
    sbStatusBar.Panels(1).Text = str(.ListItems.Count) + " object(s)"
    sbStatusBar.Panels(2).Text = Source.Source
  End With
End Sub

Private Sub SSTabResize()
  Dim Index As Integer
  Index = SSTab.Tab
  
  lvView(Index).Height = Max(SSTab.Height - lvView(Index).Top - lvView(Index).Left, 0)
  lvView(Index).Width = Max(SSTab.Width - IIf(Index = 0, 1744, 0) - lvView(Index).Left * 2, 0)

  If Index = 0 Then
    chkGroup(0).Left = SSTab.Width - 1644
    chkGroup(1).Left = SSTab.Width - 1644
    chkGroup(2).Left = SSTab.Width - 1644
    chkGroup(3).Left = SSTab.Width - 1644
    chkGroup(4).Left = SSTab.Width - 1644
    chkGroup(5).Left = SSTab.Width - 1644
  
    cboStatus.Left = SSTab.Width - 1644
    dtpSince.Left = SSTab.Width - 1644
  End If

End Sub
'
' End General
'
'
'
'
' Begin Controls
'

Private Sub cboStatus_Click()
  ' Can be called from Form_Load.  If so, don't refresh.
  If Not ActiveControl Is Nothing Then chkGroup_Click 0
End Sub

Private Sub chkGroup_Click(Index As Integer)
  mReferenceView.DataProvider.filter = EventFilter
  SSTabRefresh
End Sub

Private Sub cmdEditRow_Click()
    If lvView(SSTab.Tab).ListItems.Count < 1 Then Exit Sub
    mnuEditEditRow_Click
End Sub

Private Sub dtpSince_CloseUp()
  chkGroup_Click 0
End Sub

Private Sub Form_Initialize()
   'Initialize Event dataset
   Set mReferenceView = New GenericDataset
   Set mReferenceView.Session = LawSession
   mReferenceView.DataProvider.Source = DatabaseName.REFERENCEVIEW_TABLE
   
   'Initialize Reference dataset
   Set mReference = New ReferenceDataset
   Set mReference.Session = LawSession
   
   'Initialize Firm Lawyer dataset 'ver 2.10.0326 (jjm)
   Set mFirmLawyer = NewGenericDataset(DatabaseName.LAWYER_VIEW)
   
   'Initialize claim dataset 'ver 2.40.0105
   Set mClaim = NewGenericDataset(DatabaseName.CLAIM_VIEW)
   
   'Initialize FirmInsurer dataset
   Set mFirmInsurer = NewGenericDataset(DatabaseName.FIRMINSURER_VIEW)
   
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If

  ' Custom insert key
  Select Case KeyCode
    Case vbKeyInsert
      mnuFileNew_Click SSTab.Tab
  End Select
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  dtpSince.Value = CDate("1/1/1986")
  
  'Standard setup for 'Open' forms.
  FormMgr.Load Me
  FormUtil.EditLoadImages Me
  FormUtil.MenuLoad Me
  
  'Default is Complete/Pending.
  cboStatus.ListIndex = 0
  
  TabControlEnable SSTab
  
  mnuEditEventRow(0).Enabled = (SSTab.Tab = 0)
  mnuEditEventRow(1).Enabled = (SSTab.Tab = 0)

  ' Refresh data to form
  FormRefresh
End Sub


Private Sub Form_Paint()
  SSTabResize
End Sub

Private Sub Form_Resize()
  picBody.Height = Max(ScaleHeight - picBody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub fraFirm_DblClick()
  FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mFirmHandle, FormMgr
End Sub

Private Sub fraInsurer_DblClick()
  Dim fInsurer As New frmFirmInsurerEdit
  fInsurer.ShowByPrimaryKey mFirmHandle, lbldatafield(30), lbldatafield(29)
End Sub

Private Sub fraNote_DblClick()
  If mnuToolsNotepad.Enabled Then
    mnuToolsNotepad_Click
  Else
    MsgBox "Please add initial note using the Edit, Firm menu."
  End If
End Sub





Private Sub lvView_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ListViewSort lvView(Index), ColumnHeader, ListViewDataProvider(Index)
End Sub

Private Sub lvview_DblClick(Index As Integer)
  If lvView(Index).ListItems.Count <> 0 Then mnuEditEditRow_Click
End Sub

Private Sub lvView_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case KeyCode
    Case vbKeyLeft:
      If SSTab.Tab = 0 Then
        SSTab.Tab = SSTab.Tabs - 1
      Else
        SSTab.Tab = SSTab.Tab - 1
      End If
      KeyCode = 0
    Case vbKeyRight:
      If SSTab.Tab = SSTab.Tabs - 1 Then
        SSTab.Tab = 0
      Else
        SSTab.Tab = SSTab.Tab + 1
      End If
      KeyCode = 0
  End Select
End Sub
Private Sub lvView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView(mTab).hWnd, X, Y) And ListViewModule.SelectedCount(lvView(mTab).hWnd) > 0 Then
    lvView(mTab).SelectedItem.Selected = False
  End If
  If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView(mTab).hWnd) = 0 Then
       'For list view popup
       'MenuListItem
     Else
       'For item popup
       PopupMenu mnuPopup, , , , mnuPopupEdit
     End If
  End If
End Sub


Private Sub mnuEditBrowseRow_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Select Case SSTab.Tab
    Case TAB_LAWYER  'lawyer
      Dispatcher.BrowseLawyerOpen ListItemText(lvView(TAB_LAWYER).SelectedItem, DatabaseName.LAWYER_KEY)
    Case TAB_CLAIM
      Dispatcher.BrowseClaimOpen ListItemText(lvView(TAB_CLAIM).SelectedItem, DatabaseName.CLAIM_KEY)
  End Select
End Sub

Private Sub mnuEditDeleteRow_Click()
  Select Case SSTab.Tab
    'ver 2.10.0410 (jjm)
    Case TAB_FIRMINSURER
      With lvView(TAB_FIRMINSURER)
        Call Factory.FirmInsurerService.DeleteFirmInsurer( _
            ListItemText(.SelectedItem, DatabaseName.FIRM_KEY), _
            ListItemText(.SelectedItem, DatabaseName.INSURER_KEY), _
            ListItemText(.SelectedItem, "effectiveDate"), _
            vbNullStringArray, vbNullStringArray)
      End With
  End Select
  FormRefresh
End Sub

Private Sub mnuEditEditRow_Click()
  Select Case SSTab.Tab
    Case 0 'Event
      Dim EventHandle As String
      Dim EventGroupHandle As String
      
      EventHandle = ListItemText(lvView(0).SelectedItem, DatabaseName.EVENT_KEY)
      EventGroupHandle = ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.EVENTTYPERULEID_FIELD)
      
      If EventGroupHandle = "Event" Then
        FormUtil.ShowByPrimaryKey New frmEventEdit, EventHandle, FormMgr
      ElseIf EventGroupHandle = DatabaseName.FIRM_DOCUMENT Then
        FormUtil.ShowByPrimaryKey New frmEventDocumentEdit, EventHandle, FormMgr
      Else
        FormUtil.ShowEventProperties ListItemText(lvView(SSTab.Tab).SelectedItem, DatabaseName.EVENT_KEY)
      End If
      
    Case TAB_LAWYER   'Lawyer
      FormUtil.ShowByListView New frmFirmLawyerEdit, FormMgr, lvView(TAB_LAWYER)
    Case TAB_FIRMINSURER   'Insurer
      FormUtil.ShowByListView New frmFirmInsurerEdit, FormMgr, lvView(TAB_FIRMINSURER)
    Case TAB_CLAIM:  'Claim
      FormUtil.ShowByListView New frmClaimOpen, FormMgr, lvView(TAB_CLAIM)
  End Select
End Sub

Private Sub mnuEditEventRow_Click(Index As Integer)
  Dim Handles(0) As String
  Dim ParameterNames() As String
  Dim ParameterValues() As String
   
  Handles(0) = ListItemText(lvView(0).SelectedItem, DatabaseName.EVENT_KEY)
'  Factory.FolderParameterService.ParameterLoad ListItemText(lvView(0).SelectedItem, DatabaseName.FOLDER_KEY), Index, ParameterNames, ParameterValues, True
  If Index = lawActionComplete Then
    If MsgBox("Are you sure you want to complete item?", vbYesNo, "Complete Event") = vbYes Then
      Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
    End If
  ElseIf Index = lawActionVoid Then
    If MsgBox("Are you sure you want to void item?", vbYesNo, "Void Event") = vbYes Then
      Factory.EventService.SetVoid Handles, ParameterNames, ParameterValues
    End If
  ElseIf Index = 2 Then
    If MsgBox("Are you sure you want to set the item to a pending status?", vbYesNo, "Set Event Status") = vbYes Then
      Factory.EventService.SetPending Handles, ParameterNames, ParameterValues
    End If
  End If
  
  FormRefresh
End Sub

Private Sub mnuEditFirm_Click()
  FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mFirmHandle, FormMgr
End Sub

Private Sub mnuEditExport_Click()
  Dim ExportDlg As New frmExportDialog
  Dim Index As Integer
  Index = ExportDlg.ShowExport(Me)
  
  If Index > -1 Then
    FormUtil.ExportItem lvView(SSTab.Tab), Index, dlgSave
  End If
End Sub


Private Sub mnuFileNew_Click(Index As Integer)
  Select Case Index
    Case 0  'Event
      Dim fEvent As New frmEventEdit
      FormMgr.Add fEvent, DatabaseName.EVENT_KEY & "New"
      fEvent.ShowNew DatabaseName.FIRMEVENT_RULE, mEventHandle, lbldatafield(2)
    'Case 1  'Lawyer
    '  FormUtil.ShowNew New frmFirmLawyerEdit, FormMgr, mFirmHandle
    Case 2  'Insurer
      FormUtil.ShowNew New frmFirmInsurerEdit, FormMgr, mFirmHandle
  End Select
End Sub




Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.FIRM_TABLE, mFirmHandle
  End If
  'FormRefresh
End Sub

Private Sub mnuPopupBrowse_Click()
  mnuEditBrowseRow_Click
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDeleteRow_Click
End Sub

Private Sub mnuPopupEdit_Click()
  mnuEditEditRow_Click
End Sub

Private Sub mnuPopupEvent_Click(Index As Integer)
  mnuEditEventRow_Click Index
End Sub

Private Sub mnuPopupExport_Click()
  mnuEditExport_Click
End Sub

Private Sub mnuPopupNew_Click()
  mnuFileNew_Click SSTab.Tab
End Sub

Private Sub mnuToolsBrowse_Click()
  Dispatcher.BrowseFirmOpen mFirmHandle
End Sub

Private Sub mnuToolsBrowseWebsite_Click()
  Dispatcher.BrowseEntityURL mFirmHandle
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuFileEvent_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.FIRM_REF, DatabaseName.FIRM_TABLE, mFirmHandle, lbldatafield(2).Caption
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub


Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub


Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr
End Sub


Private Sub mnuToolsWebMerge_Click()
  Factory.EventService.WebMergeByView lvView(0)
End Sub

Private Sub mnuViewChooseColumns_Click()
  ChooseColumns lvView(SSTab.Tab), ListViewDataProvider(SSTab.Tab), False
End Sub

Private Sub mnuViewNext_Click()
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

Private Sub mnuViewPrevious_Click()
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView(SSTab.Tab), lvView(SSTab.Tab).Tag
End Sub

Private Sub mnuViewTab_Click(Index As Integer)
  SSTab.Tab = Index
End Sub

Private Sub picBody_Resize()
  SSTab.Height = Max(picBody.ScaleHeight - SSTab.Top, 0)
  SSTab.Width = Max(picBody.ScaleWidth - (SSTab.Left * 2), 0)
  SSTabResize
End Sub


'
' User clicks on new tab.  Because method can be called from Form_Load,
' we must check ActiveControl before doing anything that needs the form
' to be loaded.
'
Private Sub SSTab_Click(PreviousTab As Integer)
  TabControlEnable SSTab
  mTab = SSTab.Tab
  SSTabRefresh

  ' Can be called from Form_Load through FormRefresh.
  If ActiveControl Is Nothing Then
    lvView(mTab).TabIndex = 0
  Else
    SSTabResize
    lvView(mTab).SetFocus
  End If
End Sub

Private Sub txtDataField_DblClick(Index As Integer)
  mnuToolsNotepad_Click
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub
