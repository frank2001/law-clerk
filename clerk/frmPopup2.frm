VERSION 5.00
Begin VB.Form frmPopup2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   480
   ClientLeft      =   120
   ClientTop       =   744
   ClientWidth     =   4020
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   480
   ScaleWidth      =   4020
   StartUpPosition =   3  'Windows Default
   Begin VB.Menu mnuPopups 
      Caption         =   "General"
      Begin VB.Menu mnuPopupCoverage 
         Caption         =   "Coverage"
         Begin VB.Menu mnuCoverageOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeparator38262 
            Caption         =   "-"
         End
         Begin VB.Menu mnuCoverageExploreMenu 
            Caption         =   "E&xplore"
            Begin VB.Menu mnuCoverageExplore 
               Caption         =   "&Application"
               Index           =   0
            End
            Begin VB.Menu mnuCoverageExplore 
               Caption         =   "Application &Limit"
               Index           =   1
            End
            Begin VB.Menu mnuCoverageExplore 
               Caption         =   "Application &Rating"
               Index           =   2
            End
            Begin VB.Menu mnuCoverageExplore 
               Caption         =   "&Firm"
               Index           =   3
            End
            Begin VB.Menu mnuCoverageExplore 
               Caption         =   "&Policy"
               Index           =   4
            End
         End
         Begin VB.Menu mnuSeparator622 
            Caption         =   "-"
         End
         Begin VB.Menu mnuCoverageProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuEvent 
         Caption         =   "Event"
         Begin VB.Menu mnuEventOpen 
            Caption         =   "mnuEventOpen"
            Index           =   0
         End
         Begin VB.Menu mnuSeparator322 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEventWorkflow 
            Caption         =   "&Complete..."
            Index           =   0
         End
         Begin VB.Menu mnuEventWorkflow 
            Caption         =   "&Void..."
            Index           =   1
         End
         Begin VB.Menu mnuSeparator433 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEventCut 
            Caption         =   "Cu&t"
            Shortcut        =   ^X
         End
         Begin VB.Menu mnuEventPaste 
            Caption         =   "&Paste"
            Shortcut        =   ^V
         End
         Begin VB.Menu mnuEventMove 
            Caption         =   "&Move to Folder..."
            Shortcut        =   ^M
         End
         Begin VB.Menu mnuSeparator434 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEventProperties 
            Caption         =   "P&roperties..."
         End
      End
      Begin VB.Menu mnuGLBatch 
         Caption         =   "GLBatch"
         Begin VB.Menu mnuGLBatchOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuGLBatchOpen 
            Caption         =   "Expor&t"
            Index           =   2
         End
         Begin VB.Menu mnuSeparator5632 
            Caption         =   "-"
         End
         Begin VB.Menu mnuGLBatchExploreMenu 
            Caption         =   "&Explore"
            Begin VB.Menu mnuGLBatchExplore 
               Caption         =   "G/L Entry"
               Index           =   1
            End
         End
      End
      Begin VB.Menu mnuGLEntry 
         Caption         =   "GLEntry"
         Begin VB.Menu mnuGLEntryEventOpen 
            Caption         =   "mnuEventOpen"
            Index           =   0
         End
         Begin VB.Menu mnuSeparator342 
            Caption         =   "-"
         End
         Begin VB.Menu mnuGLEntryExploreMenu 
            Caption         =   "&Explore"
            Begin VB.Menu mnuGLEntryExplore 
               Caption         =   "G/L Transaction"
               Index           =   1
            End
         End
      End
      Begin VB.Menu mnuPopupSelect 
         Caption         =   "Select"
         Begin VB.Menu mnuSelect 
            Caption         =   "&Select"
         End
         Begin VB.Menu mnuSelectNew 
            Caption         =   "&New"
         End
         Begin VB.Menu mnuSelectEdit 
            Caption         =   "&Edit"
         End
      End
      Begin VB.Menu mnuPopupPerson 
         Caption         =   "Person"
         Begin VB.Menu mnuPopupPersonOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuPopupPersonOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuPopupPersonOpen 
            Caption         =   "&Browse"
            Enabled         =   0   'False
            Index           =   3
         End
         Begin VB.Menu mnuSeperator967 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupPersonNewMenu 
            Caption         =   "&New"
            Begin VB.Menu mnuPopupPersonNew 
               Caption         =   "&Person"
               Enabled         =   0   'False
               Index           =   1
            End
         End
         Begin VB.Menu mnuPopupPersonExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuPopupPersonExplore 
               Caption         =   "&Contact"
               Index           =   1
            End
            Begin VB.Menu mnuPopupPersonExplore 
               Caption         =   "&Lawyer"
               Index           =   2
            End
         End
         Begin VB.Menu mnuSeperator966 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupPersonSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuPopupSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuPopupPersonProperties 
            Caption         =   "P&roperties"
         End
      End
   End
   Begin VB.Menu mnuMaintenancePopups 
      Caption         =   "Maintenance"
      Begin VB.Menu mnuContactType 
         Caption         =   "Contact Type"
         Begin VB.Menu mnuContactTypeOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator939 
            Caption         =   "-"
         End
         Begin VB.Menu mnuContactTypeProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuEndorsementType 
         Caption         =   "Endorsement Type"
         Begin VB.Menu mnuEndorsementTypeOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator938 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEndorsementTypeProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuEFTDepositFinancialInst 
         Caption         =   "EFT Deposit Financial Inst"
         Begin VB.Menu mnuEFTDepositFinancialInstOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator301 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEFTDepositFinancialInstProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuEFTIndividual 
         Caption         =   "EFT Individual"
         Begin VB.Menu mnuEFTIndividualOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator1000 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEFTIndividualNew 
            Caption         =   "&New"
         End
         Begin VB.Menu mnuSeperator1001 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEFTIndividualProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuEFTTransactionCode 
         Caption         =   "EFT Transaction Code"
         Begin VB.Menu mnuEFTTransactionCodeOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator302 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEFTTransactionProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuGLAccount 
         Caption         =   "GL Account"
         Begin VB.Menu mnuGLAccountOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator303 
            Caption         =   "-"
         End
         Begin VB.Menu mnuGLAccountProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuGLAccountGroup 
         Caption         =   "GL Account Group"
         Begin VB.Menu mnuGLAccountGroupOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator305 
            Caption         =   "-"
         End
         Begin VB.Menu mnuGLAccountGroupProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuEventType 
         Caption         =   "Event Type"
         Begin VB.Menu mnuEventTypeOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeparator8722 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEventTypeExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuEventTypeExplore 
               Caption         =   "Group"
               Index           =   1
            End
         End
         Begin VB.Menu mnuSeperator306 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEventTypeProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuEventTypeGroup 
         Caption         =   "Event Type Group"
         Begin VB.Menu mnuEventTypeGroupOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
      End
      Begin VB.Menu mnuFiscalCalendar 
         Caption         =   "FiscalCalendar"
         Begin VB.Menu mnuFiscalCalendarOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator310 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFiscalCalendarProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuInsurer 
         Caption         =   "Insurer"
         Begin VB.Menu mnuInsurerOpen 
            Caption         =   "&Open"
            Index           =   1
         End
         Begin VB.Menu mnuInsurerOpen 
            Caption         =   "&Edit"
            Index           =   2
         End
         Begin VB.Menu mnuSeperator333 
            Caption         =   "-"
         End
         Begin VB.Menu mnuInsurerProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuLawArea 
         Caption         =   "Law Area"
         Begin VB.Menu mnuLawAreaOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator311 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLawAreaProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuLawDisposition 
         Caption         =   "Law Disposition"
         Begin VB.Menu mnuLawDispositionOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator312 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLawDispositionProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuLawError 
         Caption         =   "Law Error"
         Begin VB.Menu mnuLawErrorOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator313 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLawErrorProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuLocationType 
         Caption         =   "Location Type"
         Begin VB.Menu mnuLocationTypeOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator344 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLocationTypeProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuRatingBook 
         Caption         =   "RatingBook"
         Begin VB.Menu mnuRatingBookOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeparator3422 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingBookExploreMenu 
            Caption         =   "Exp&lore"
            Begin VB.Menu mnuRatingBookExplore 
               Caption         =   "&Calc"
               Index           =   1
            End
            Begin VB.Menu mnuRatingBookExplore 
               Caption         =   "&Metric"
               Index           =   2
            End
            Begin VB.Menu mnuRatingBookExplore 
               Caption         =   "&Rate"
               Index           =   3
            End
         End
         Begin VB.Menu mnuSeperator314 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingBookProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuRatingBookMetric 
         Caption         =   "Rating Book Metric"
         Begin VB.Menu mnuRatingBookMetricOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator345 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingBookMetricProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuRatingBookMetricCalc 
         Caption         =   "Rating Book Metric Calc"
         Begin VB.Menu mnuRatingBookMetricCalcOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator315 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRaingBookMetricCalcProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuRatingBookRate 
         Caption         =   "Rating Book Rate"
         Begin VB.Menu mnuRatingBookRateOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator316 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingBookRateProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuRatingMetric 
         Caption         =   "Rating Metric"
         Begin VB.Menu mnuRatingMetricOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSperator317 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingMetricProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuRatingRate 
         Caption         =   "Rating Rate"
         Begin VB.Menu mnuRatingRateOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator318 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRatingRateProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuReinsuranceTreaty 
         Caption         =   "Reinsurance Treaty"
         Begin VB.Menu mnuReinsuranceTreatyOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator319 
            Caption         =   "-"
         End
         Begin VB.Menu mnuReinsuranceTreatyProperties 
            Caption         =   "&Properties"
         End
      End
      Begin VB.Menu mnuTreatyLayer 
         Caption         =   "Treaty Layer"
         Begin VB.Menu mnuTreatyLayerOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator369 
            Caption         =   "-"
         End
         Begin VB.Menu mnuTreatyLayerProperties 
            Caption         =   "&Properties"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuQuestion 
         Caption         =   "Question"
         Begin VB.Menu mnuQuestionOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator320 
            Caption         =   "-"
         End
         Begin VB.Menu mnuQuestionProperties 
            Caption         =   "&Properties"
         End
      End
   End
   Begin VB.Menu mnuSystemPopups 
      Caption         =   "System"
      Begin VB.Menu mnuFolder 
         Caption         =   "Folder"
         Begin VB.Menu mnuFolderOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
      End
      Begin VB.Menu mnuListener 
         Caption         =   "Listener"
         Begin VB.Menu mnuListenerOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
         Begin VB.Menu mnuSeperator999 
            Caption         =   "-"
         End
         Begin VB.Menu mnuListenerExploreMenu 
            Caption         =   "&Exp&lore"
            Index           =   2
            Begin VB.Menu mnuListenerExplore 
               Caption         =   "&Parameter"
               Index           =   1
            End
         End
      End
      Begin VB.Menu mnuListenerParameter 
         Caption         =   "Listener Parameter"
         Begin VB.Menu mnuListenerParameterOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
      End
      Begin VB.Menu mnuListenerAction 
         Caption         =   "Listener Action"
         Begin VB.Menu mnuListenerActionOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
      End
      Begin VB.Menu mnuParameter 
         Caption         =   "Parameter"
         Begin VB.Menu mnuParameterOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
      End
      Begin VB.Menu mnuPublisher 
         Caption         =   "Publisher"
         Begin VB.Menu mnuPublisherOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
      End
      Begin VB.Menu mnuPublisherListener 
         Caption         =   "Publisher Listener"
         Begin VB.Menu mnuPublisherListenerOpen 
            Caption         =   "&Edit"
            Index           =   1
         End
      End
   End
End
Attribute VB_Name = "frmPopup2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.21.0708 jjm
'ver 2.00.0205 jjm

Option Explicit

Private mParent As Form
Private mParentInterface As FormInterface

Private mEventDataset As Dataset



'
' FormInterface Interface
'

'
' Save Parent
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set mParentInterface = ParentInterface
  Set mParent = ParentInterface.Parent
End Sub

'
' When parent is unloaded, remove reference to parent so it can terminate.
'
Public Sub FormParentUnload()
  Set mParent = Nothing
End Sub


Public Sub Form_Initialize()
  Set mEventDataset = New EventDataset
  Set mEventDataset.Session = LawSession
End Sub


Public Sub mnuContactTypeOpen_Click(Index As Integer)
  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.CONTACTTYPE_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.CONTACTTYPE_KEY) & _
            "&key=" & Util.EncodeURL(ListItemText(mParent.lvView.SelectedItem, DatabaseName.CONTACTTYPE_KEY)), True
    End Select
  Next item
End Sub

Public Sub mnuContactTypeProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.CONTACTTYPE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.CONTACTTYPE_KEY)
  End If
End Sub

Private Sub mnuCoverageExplore_Click(Index As Integer)
  Select Case Index
    Case 0 'Application
      mParent.FolderLoad DatabaseName.APPLICATION_TABLE, , 0, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
    Case 1 'ApplicationLimit
      mParent.FolderLoad DatabaseName.APPLICATIONLIMIT_TABLE, , 1, ListItemText(mParent.lvView.SelectedItem, DatabaseName.APPLICATIONLIMIT_KEY)
    Case 2 'ApplicationRating
      mParent.FolderLoad DatabaseName.QUOTE_TABLE, , 1, ListItemText(mParent.lvView.SelectedItem, DatabaseName.QUOTE_KEY)
    Case 3 'Firm
      mParent.FolderLoad DatabaseName.FIRM_TABLE, , 1, ListItemText(mParent.lvView.SelectedItem, DatabaseName.FIRM_KEY)
    Case 4 'Policy
      mParent.FolderLoad DatabaseName.POLICY_TABLE, , 3, ListItemText(mParent.lvView.SelectedItem, DatabaseName.POLICY_KEY)
  End Select
End Sub

Public Sub mnuCoverageOpen_Click(Index As Integer)
  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Edit
        FormUtil.ShowByListView New frmCoverageEdit, mParent.FormMgr, mParent.lvView
    End Select
  Next item
End Sub


Public Sub mnuCoverageProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.COVERAGE_VIEW, ListItemText(mParent.lvView.SelectedItem, DatabaseName.COVERAGE_KEY)
  End If
End Sub

Public Sub mnuEFTDepositFinancialInstOpen_Click(Index As Integer)
  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.EFTDEPOSITFINANCIALINST_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.EFTDEPOSITFINANCIALINST_KEY) & _
            "&key=" & Util.EncodeURL(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTDEPOSITFINANCIALINST_KEY)), True
    End Select
  Next item

End Sub

Public Sub mnuEFTDepositFinancialInstProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.EFTDEPOSITFINANCIALINST_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTDEPOSITFINANCIALINST_KEY)
  End If

End Sub


Public Sub mnuEFTIndividualNew_Click()
  Dispatcher.ShowNew DatabaseName.EFTINDIVIDUAL_TABLE
End Sub

Public Sub mnuEFTIndividualOpen_Click(Index As Integer)
  Select Case Index
    Case 1
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.EFTINDIVIDUAL_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.EFTINDIVIDUAL_KEY) & _
            "&key=" & Util.EncodeURL(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTINDIVIDUAL_KEY)), True
      
      
      'FormUtil.ShowByPrimaryKey New frmEFTIndividualEdit, ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTINDIVIDUAL_KEY), mParent.FormMgr
      'Dim fEftIndividualEdit As New frmEFTIndividualEdit
      'mParent.FormMgr.Load fEftIndividualEdit
      'fEftIndividualEdit.ShowByPrimaryKey ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTINDIVIDUAL_KEY)
  End Select
End Sub

Public Sub mnuEFTIndividualProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.EFTINDIVIDUAL_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTINDIVIDUAL_KEY)
  End If
End Sub

Public Sub mnuEFTTransactionCodeOpen_Click(Index As Integer)
  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.EFTTRANSACTIONCODE_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.EFTTRANSACTIONCODE_KEY) & _
            "&key=" & Util.EncodeURL(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTTRANSACTIONCODE_KEY)), True
    End Select
  Next item
End Sub

Public Sub mnuEFTTransactionProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.EFTTRANSACTIONCODE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.EFTTRANSACTIONCODE_KEY)
  End If
End Sub

Public Sub mnuEndorsementTypeOpen_Click(Index As Integer)
  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.ENDORSEMENTTYPE_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.ENDORSEMENTTYPE_KEY) & _
            "&key=" & Util.EncodeURL(ListItemText(mParent.lvView.SelectedItem, DatabaseName.ENDORSEMENTTYPE_KEY)), True
    End Select
  Next item
End Sub

Public Sub mnuEndorsementTypeProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.ENDORSEMENTTYPE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.ENDORSEMENTTYPE_KEY)
  End If

End Sub

Public Sub mnuEventCut_Click()
  mParent.mnuEditCut_Click
End Sub

Public Sub mnuEventMoveToFolder_Click()
End Sub

Public Sub mnuEventMove_Click()
  mParent.mnuEditMove_Click
End Sub

'See FormUtil.MenuEventLoad for the dynamic building of the workflow event menus
Public Sub mnuEventOpen_Click(Index As Integer)
  Dim Hourglass As New WaitPointer
  Hourglass.Default

  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  

  Dim TestType As String
  'Get the first event type in the collection
  TestType = ListItemText(Selection(1), DatabaseName.EVENTTYPE_KEY)
  
  Dim EventHandle As String
  Dim EventTypeHandle As String
  For Each item In Selection
    
    mParent.lvView.SelectedItem = item
    EventHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)
    EventTypeHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENTTYPE_KEY)
    
    If ListItemText(item, DatabaseName.EVENTTYPE_KEY) <> TestType Then
      Exit For
    End If
    
    Select Case mnuEventOpen(Index).Tag
      Case DatabaseName.APPLICATION_TABLE
        'test to see if the Application is a policy application or an endorsement application
        'The AppEdit rule is also used in frmApplicationEdit for selecting the application types
        If Factory.EventService.IsEventTypeInGroup(EventTypeHandle, DatabaseName.APPENDORSEMENTEDIT_RULE) Then
          FormUtil.ShowByEventListView mParent, New frmEndorsementApplicationOpen
        Else
          FormUtil.ShowByEventListView mParent, New frmApplicationOpen
        End If
      
      Case DatabaseName.APPLICATIONLIMIT_TABLE
        'FormUtil.ShowByPrimaryKey New frmApplicationLimitEdit, mParent.FormMgr, Factory.ReferenceService.ReferenceId1(EventHandle, DatabaseName.APPLICATIONLIMIT_TABLE)
        MsgBox "ApplicationLimit hook not enabled. Please contact the network administrator"
      
      Case DatabaseName.QUOTE_TABLE
        FormUtil.ShowByEventListView mParent, New frmQuoteEdit
      
      Case DatabaseName.QUOTEBOOK_TABLE
        FormUtil.ShowByEventListView mParent, New frmQuoteEdit
      
      Case DatabaseName.CLAIM_TABLE
        FormUtil.ShowByEventListView mParent, New frmClaimOpen
      
      Case DatabaseName.CLAIMRESERVE_TABLE
        'MsgBox "ToDo: Hook up ClaimReserve Browse"
        Dispatcher.BrowseClaimReserveOpen Factory.ReferenceService.FindReferenceHandleBySourceType(EventHandle, DatabaseName.CLAIMRESERVE_TABLE)
      
      Case DatabaseName.CLAIMPAYMENT_TABLE
        FormUtil.ShowByEventListView mParent, New frmClaimPaymentEdit
      
      Case DatabaseName.COVERAGE_TABLE
        FormUtil.ShowByEventListView mParent, New frmCoverageEdit
        
      Case DatabaseName.DEDUCTIBLEREIMBURSE_TABLE
        FormUtil.ShowByEventListView mParent, New frmDeductibleReimburseEdit
      
      Case DatabaseName.FIRM_TABLE
        FormUtil.ShowByEventListView mParent, New frmFirmDiaryOpen
      
      Case DatabaseName.POLICY_TABLE
        FormUtil.ShowByEventListView mParent, New frmPolicyOpen
      
      Case DatabaseName.POLICYACCOUNTING_TABLE:
        FormUtil.ShowByEventListView mParent, New frmPolicyAccountingEdit
      
      Case "Notepad"
        FormUtil.ShowByPrimaryKey New frmNotepad, EventHandle
      
      Case DatabaseName.ENDORSEMENT_TABLE, _
           DatabaseName.FIRM_DOCUMENT, _
           DatabaseName.CLAIM_DOCUMENT, _
           DatabaseName.INCIDENTDOCUMENT_RULE, _
           DatabaseName.DOCUMENT_SOURCETYPE
        Dim fForm As New frmEventDocumentEdit
        fForm.ShowByPrimaryKey EventHandle
      
      
      Case DatabaseName.INCIDENT_TABLE
        FormUtil.ShowByEventListView mParent, New frmIncidentOpen
        
      Case DatabaseName.RECEIPT_TABLE
        Dispatcher.BrowseReceipt EventHandle
     
      'Hacks:
      Case "BrowseClaim"
        MsgBox "ToDo: Hook up claim browse by event"
      'ver 2.21.0708 jjm
      Case "BrowseWorksheet"
        Dispatcher.BrowseClaimWorksheet EventHandle
      Case DatabaseName.ONLINEAPP_TYPE
        FormUtil.ShowByListView New frmEventOnlineAppEdit, mParent.FormMgr, mParent.lvView
      Case "BrowseOnlineApp"
        Dim eventLocator As String
        eventLocator = ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENTLOCATOR_FIELD)
        If (eventLocator = vbNullString) Then
          MsgBox "The eventLocator field was not found or no value exists. This field is needed to retrieve the online application number. Please add this field via choose columns."
        End If
        OnlineAppUtil.browseOnlineApplication eventLocator
    End Select
  Next item
End Sub

Public Sub mnuEventPaste_Click()
  mParent.mnuEditPaste_Click
End Sub

Public Sub mnuEventProperties_Click()
  FormUtil.ShowEventProperties ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)
End Sub

Public Sub mnuEventTypeExplore_Click(Index As Integer)
  ' Set id before selecting folder as the list view will change.
  Dim EventTypeHandle As String
  EventTypeHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENTTYPE_KEY)
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.EVENTTYPERULE_TABLE, , 1, EventTypeHandle
  End Select
End Sub

Public Sub mnuEventTypeGroupOpen_Click(Index As Integer)
  Dim EventTypeHandle As String, EventGroupHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    
    EventTypeHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENTTYPE_KEY)
    EventGroupHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RULE_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.EVENTTYPERULE_TABLE) & _
            "&key=" & DatabaseName.EVENTTYPE_KEY & _
            "&key=" & Util.EncodeURL(EventTypeHandle) & _
            "&key=" & DatabaseName.RULE_KEY & _
            "&key=" & Util.EncodeURL(EventGroupHandle), True
    End Select
  Next item
End Sub

Public Sub mnuEventTypeOpen_Click(Index As Integer)
  Dim EventTypeHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    EventTypeHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENTTYPE_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.EVENTTYPE_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.EVENTTYPE_KEY) & _
            "&key=" & Util.EncodeURL(EventTypeHandle), True
    End Select
  Next item
End Sub

Public Sub mnuEventTypeProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.EVENTTYPE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENTTYPE_KEY)
  End If

End Sub

Public Sub mnuEventWorkflow_Click(Index As Integer)
  mParent.mnuFileWorkflow_Click Index
End Sub


Public Sub mnuFiscalCalendarOpen_Click(Index As Integer)
  Dim YearHandle As String, PeriodHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    YearHandle = ListItemText(mParent.lvView.SelectedItem, "fiscalYear")
    PeriodHandle = ListItemText(mParent.lvView.SelectedItem, "fiscalPeriod")
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.FISCALCALENDAR_TABLE) & _
            "&key=fiscalYear" & _
            "&key=" & Util.EncodeURL(YearHandle) & _
            "&key=fiscalPeriod" & _
            "&key=" & Util.EncodeURL(PeriodHandle), True
    End Select
  Next item

End Sub

Public Sub mnuFiscalCalendarProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.FISCALCALENDAR_TABLE, ListItemText(mParent.lvView.SelectedItem, "fiscalYear")
  End If

End Sub

Public Sub mnuFolderOpen_Click(Index As Integer)
  Dim FolderHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    
    FolderHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.FOLDER_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.FOLDER_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.FOLDER_KEY) & _
            "&key=" & Util.EncodeURL(FolderHandle), True
    End Select
  Next item

End Sub

Public Sub mnuGLAccountOpen_Click(Index As Integer)
  Dim GLAccountHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    
    GLAccountHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.GLACCOUNT_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.GLACCOUNT_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.GLACCOUNT_KEY) & _
            "&key=" & Util.EncodeURL(GLAccountHandle), True
    End Select
  Next item
End Sub

'
' GLBatch
'

Public Sub mnuGLBatchExplore_Click(Index As Integer)
' Must save firm id since setting folder name creates a new selected item.
  Dim GLBatchHandle As String
  GLBatchHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.GLBATCH_KEY)
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.GLENTRY_TABLE, , 1, GLBatchHandle
  End Select
End Sub

Public Sub mnuGLBatchOpen_Click(Index As Integer)
  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1  ' Open
        'keep following URL in sync with FormUtil.ExportItem(3)
        Browse HttpServer & WebUtil & "glbatchexport.custom?display=text/plain&key=" & _
               EncodeURL(ListItemText(mParent.lvView.SelectedItem, DatabaseName.GLBATCH_KEY))
      Case 2  ' Export
        ' 2.00.0205 (jjm)
        Dim FileName As String
        FileName = IniForm.ReadFile("GLBatch", "file")
        If FormUtil.ExportItem(mParent.lvView, 3, mParent.dlgSave, FileName) Then
          IniForm.WriteFile "GLBatch", "file", FileName
        End If
    End Select
  Next item
  
End Sub

Private Sub mnuGLEntryEventOpen_Click(Index As Integer)
  'ver 2.00.0106 (jjm)
  'mnuEventOpen is the same as GLEntryEventOpen. When it is being setup
  'everything is loaded to mnuEventOpen and then copied to mnuGLEntryEventOpen.
  
  mnuEventOpen_Click Index
End Sub

Public Sub mnuGLEntryExplore_Click(Index As Integer)
' Must save firm id since setting folder name creates a new selected item.
  Dim GLEntryHandle As String
  GLEntryHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, "glEntryId")
  
  Select Case Index
    Case 1
      mParent.FolderLoad DatabaseName.GLTRANSACTION_TABLE, , 0, GLEntryHandle
  End Select
End Sub

Public Sub mnuGLAccountGroupOpen_Click(Index As Integer)
  Dim GLAccountGroupHandle As String, EventGroupHandle As String
    Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    GLAccountGroupHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.GLACCOUNT_KEY)
    EventGroupHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RULE_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.GLACCOUNT_RULE) & _
            "&key=" & Util.EncodeURL(DatabaseName.GLACCOUNT_KEY) & _
            "&key=" & Util.EncodeURL(GLAccountGroupHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.RULE_KEY) & _
            "&key=" & Util.EncodeURL(EventGroupHandle), True
    End Select
  Next item
End Sub

Public Sub mnuInsurerOpen_Click(Index As Integer)
  Dim InsurerHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    Dim Handle As String
    mParent.lvView.SelectedItem = item
    Handle = ListItemText(item, DatabaseName.INSURER_KEY)
    
    Select Case Index
      Case 1 'Open
        FormUtil.ShowByPrimaryKey New frmOrganizationOpen, Handle, mParent.FormMgr
      Case 2 'Edit
        FormUtil.ShowByPrimaryKey New frmOrganizationEdit, Handle, mParent.FormMgr
      Case 3 'Browse
    End Select
  Next item
End Sub

Public Sub mnuInsurerProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.INSURER_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.INSURER_KEY)
  End If

End Sub

Public Sub mnuLawAreaOpen_Click(Index As Integer)
  Dim LawAreaHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    LawAreaHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWAREA_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.LAWAREA_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LAWAREA_KEY) & _
            "&key=" & Util.EncodeURL(LawAreaHandle), True
    End Select
  Next item

End Sub

Public Sub mnuLawAreaProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.LAWAREA_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWAREA_KEY)
  End If

End Sub

Public Sub mnuLawDispositionOpen_Click(Index As Integer)
  Dim LawDispositionHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    LawDispositionHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWDISPOSITION_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.LAWDISPOSITION_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LAWDISPOSITION_KEY) & _
            "&key=" & Util.EncodeURL(LawDispositionHandle), True
    End Select
  Next item
End Sub

Public Sub mnuLawDispositionProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.LAWDISPOSITION_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWDISPOSITION_KEY)
  End If
End Sub

Public Sub mnuLawErrorOpen_Click(Index As Integer)
  Dim LawErrorHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item

    LawErrorHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWERROR_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.LAWERROR_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LAWERROR_KEY) & _
            "&key=" & Util.EncodeURL(LawErrorHandle), True
    End Select
  Next item
End Sub

Public Sub mnuLawErrorProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.LAWERROR_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LAWERROR_KEY)
  End If

End Sub

Public Sub mnuListenerActionOpen_Click(Index As Integer)
  Dim ListenerHandle As String, ParameterHandle As String, ActionHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    ListenerHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LISTENER_KEY)
    ParameterHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.PARAMETER_KEY)
    ActionHandle = ListItemText(mParent.lvView.SelectedItem, "action")
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.SYSLISTENERACTIONPARAM_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LISTENER_KEY) & _
            "&key=" & Util.EncodeURL(ListenerHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.PARAMETER_KEY) & _
            "&key=" & Util.EncodeURL(ParameterHandle) & _
            "&key=action" & _
            "&key=" & Util.EncodeURL(ActionHandle), True
    End Select
  Next item

End Sub

Public Sub mnuListenerExplore_Click(Index As Integer)
  Dim ListenerHandle As String
  ListenerHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.LISTENER_KEY)

  Select Case Index
    Case 1 ' Parameter
      mParent.FolderLoad DatabaseName.SYSLISTENERPARAMETER_TABLE, , , ListenerHandle
  End Select

End Sub

Public Sub mnuListenerOpen_Click(Index As Integer)
  Dim ListenerHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item

    ListenerHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LISTENER_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.LISTENER_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LISTENER_KEY) & _
            "&key=" & Util.EncodeURL(ListenerHandle), True
    End Select
  Next item

End Sub

Public Sub mnuListenerParameterOpen_Click(Index As Integer)
  Dim ListenerHandle As String, ParameterHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    
    ListenerHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LISTENER_KEY)
    ParameterHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.PARAMETER_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.SYSLISTENERPARAMETER_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LISTENER_KEY) & _
            "&key=" & Util.EncodeURL(ListenerHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.PARAMETER_KEY) & _
            "&key=" & Util.EncodeURL(ParameterHandle), True
    End Select
  Next item

End Sub

Public Sub mnuLocationTypeOpen_Click(Index As Integer)
  Dim LocationTypeHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    LocationTypeHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LOCATIONTYPE_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.LOCATIONTYPE_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LOCATIONTYPE_KEY) & _
            "&key=" & Util.EncodeURL(LocationTypeHandle), True
    End Select
  Next item
End Sub

Public Sub mnuLocationTypeProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.LOCATIONTYPE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.LOCATIONTYPE_KEY)
  End If

End Sub

Public Sub mnuParameterOpen_Click(Index As Integer)
  Dim ParameterHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item

    ParameterHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.PARAMETER_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.SYSPARAMETER_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.PARAMETER_KEY) & _
            "&key=" & Util.EncodeURL(ParameterHandle), True
    End Select
  Next item

End Sub

Public Sub mnuPopupPersonExplore_Click(Index As Integer)
 Dim PersonHandle As String
  PersonHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.PERSON_KEY)
  
  Select Case Index
    Case 1 'Claim
      mParent.FolderLoad DatabaseName.CONTACT_TABLE, , UrlName.CONTACTSEARCH_PERSONID_INDEX, PersonHandle
    Case 2 'Lawyer
      mParent.FolderLoad DatabaseName.LAWYER_TABLE, , 3, PersonHandle
  End Select
End Sub

'
' Person
'
Public Sub mnuPopupPersonOpen_Click(Index As Integer)
  Dim Selection As collection, item As ListItem
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    Select Case Index
      Case 1 'Open
        FormUtil.ShowByListView New frmPersonOpen, mParent.FormMgr, mParent.lvView
      Case 2 'Edit
         FormUtil.ShowByListView New frmPersonEdit, mParent.FormMgr, mParent.lvView
      Case 3 'Browse
     End Select
  Next item
End Sub

Public Sub mnuPopupPersonProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.PERSON_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.PERSON_KEY)
  End If

End Sub

Public Sub mnuPopupSendToFolder_Click()
  mParent.mnuFileSendToFolder_Click
End Sub

Public Sub mnuPublisherListenerOpen_Click(Index As Integer)
  Dim ListenerHandle As String, PublisherHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    ListenerHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.LISTENER_KEY)
    PublisherHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.PUBLISHER_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.SYSPUBLISHERLISTENER_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.LISTENER_KEY) & _
            "&key=" & Util.EncodeURL(ListenerHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.PUBLISHER_KEY) & _
            "&key=" & Util.EncodeURL(PublisherHandle), True
    End Select
  Next item
End Sub

Public Sub mnuPublisherOpen_Click(Index As Integer)
  Dim PublisherHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    PublisherHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.PUBLISHER_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.SYSPUBLISHER_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.PUBLISHER_KEY) & _
            "&key=" & Util.EncodeURL(PublisherHandle), True
    End Select
  Next item

End Sub

Public Sub mnuQuestionOpen_Click(Index As Integer)
  Dim QuestionHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    QuestionHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.QUESTION_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.QUESTION_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.QUESTION_KEY) & _
            "&key=" & Util.EncodeURL(QuestionHandle), True
    End Select
  Next item
End Sub

Public Sub mnuQuestionProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.QUESTION_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.QUESTION_KEY)
  End If

End Sub

Public Sub mnuRatingBookExplore_Click(Index As Integer)
  Dim RatingBookHandle As String
  RatingBookHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGBOOK_KEY)

  Select Case Index
    Case 1 'Calc
      mParent.FolderLoad DatabaseName.RATINGBOOKMETRICCALC_TABLE, , , RatingBookHandle
    Case 2 'Metric
      mParent.FolderLoad DatabaseName.RATINGBOOKMETRIC_TABLE, , , RatingBookHandle
    Case 3 'Rate
      mParent.FolderLoad DatabaseName.RATINGBOOKRATE_TABLE, , , RatingBookHandle
  
  End Select
End Sub

Public Sub mnuRatingBookMetricCalcOpen_Click(Index As Integer)
  Dim RatingBookHandle As String, RatingMetricHandle As String, RatingBookMetricCalcHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item

    RatingBookHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGBOOK_KEY)
    RatingMetricHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGMETRIC_KEY)
    RatingBookMetricCalcHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGMETRICIDCALC_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.RATINGBOOKMETRICCALC_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGBOOK_KEY) & _
            "&key=" & Util.EncodeURL(RatingBookHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGMETRIC_KEY) & _
            "&key=" & Util.EncodeURL(RatingMetricHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGMETRICIDCALC_KEY) & _
            "&key=" & Util.EncodeURL(RatingBookMetricCalcHandle), True
    End Select
  Next item

End Sub

Public Sub mnuRatingBookMetricOpen_Click(Index As Integer)
  Dim BookHandle As String, MetricHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    
    BookHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGBOOK_KEY)
    MetricHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGMETRIC_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.RATINGBOOKMETRIC_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGBOOK_KEY) & _
            "&key=" & Util.EncodeURL(BookHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGMETRIC_KEY) & _
            "&key=" & Util.EncodeURL(MetricHandle), True
    End Select
  Next item

End Sub


Private Sub mnuRatingBookRateOpen_Click(Index As Integer)
  Dim BookHandle As String, RateHandle As String, NumericIndex As String
  Dim Selection As collection, item As ListItem

  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
    
    BookHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGBOOK_KEY)
    RateHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGRATE_KEY)
    NumericIndex = ListItemText(mParent.lvView.SelectedItem, DatabaseName.NUMERICINDEX_FIELD)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.RATINGBOOKRATE_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGBOOK_KEY) & _
            "&key=" & Util.EncodeURL(BookHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGRATE_KEY) & _
            "&key=" & Util.EncodeURL(RateHandle) & _
            "&key=" & Util.EncodeURL(DatabaseName.NUMERICINDEX_FIELD) & _
            "&key=" & Util.EncodeURL(NumericIndex), True
    End Select
  Next item
End Sub

Public Sub mnuRatingBookOpen_Click(Index As Integer)
  Dim RatingBookHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item

    RatingBookHandle = ListViewModule.ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGBOOK_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.RATINGBOOK_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGBOOK_KEY) & _
            "&key=" & Util.EncodeURL(RatingBookHandle), True
    End Select
  Next item
End Sub

Public Sub mnuRatingBookProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.RATINGBOOK_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGBOOK_KEY)
  End If

End Sub

Public Sub mnuRatingBookRateProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.RATINGBOOKRATE_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGBOOK_KEY)
  End If

End Sub

Public Sub mnuRatingMetricOpen_Click(Index As Integer)
  Dim MetricHandle As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    MetricHandle = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGMETRIC_KEY)
    Select Case Index
      Case 1 'Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.RATINGMETRIC_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGMETRIC_KEY) & _
            "&key=" & Util.EncodeURL(MetricHandle), True
    End Select
  Next item

End Sub

Private Sub mnuRatingRateOpen_Click(Index As Integer)
  Dim RatingRate As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    RatingRate = ListItemText(mParent.lvView.SelectedItem, DatabaseName.RATINGRATE_KEY)
    Select Case Index
      Case 1 ' Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.RATINGRATE_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.RATINGRATE_KEY) & _
            "&key=" & Util.EncodeURL(RatingRate), True
    End Select
  Next item
End Sub

Public Sub mnuReinsuranceTreatyOpen_Click(Index As Integer)
  Dim ReinsuranceTreaty As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    ReinsuranceTreaty = ListItemText(mParent.lvView.SelectedItem, DatabaseName.REINSURANCETREATY_KEY)
    Select Case Index
      Case 1 ' Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.REINSURANCETREATY_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.REINSURANCETREATY_KEY) & _
            "&key=" & Util.EncodeURL(ReinsuranceTreaty), True
    End Select
  Next item

End Sub

Public Sub mnuReinsuranceTreatyProperties_Click()
  If Not FormUtil.ShowEventProperties(ListItemText(mParent.lvView.SelectedItem, DatabaseName.EVENT_KEY)) Then
    FormUtil.ShowProperties DatabaseName.REINSURANCETREATY_TABLE, ListItemText(mParent.lvView.SelectedItem, DatabaseName.REINSURANCETREATY_KEY)
  End If

End Sub

Private Sub mnuTreatyLayerOpen_Click(Index As Integer)
  Dim TreatyLayer As String
  Dim Selection As collection, item As ListItem
  
  Set Selection = ListViewModule.ListViewSelected(mParent.lvView)
  
  For Each item In Selection
    mParent.lvView.SelectedItem = item
  
    TreatyLayer = ListItemText(mParent.lvView.SelectedItem, DatabaseName.TREATYLAYER_KEY)
    Select Case Index
      Case 1 ' Edit
        Browse HttpServer & WebService & "genericopen" & _
            "?key=" & Util.EncodeURL(DatabaseName.TREATYLAYER_TABLE) & _
            "&key=" & Util.EncodeURL(DatabaseName.TREATYLAYER_KEY) & _
            "&key=" & Util.EncodeURL(TreatyLayer), True
    End Select
  Next item
End Sub

Public Sub mnuSelect_Click()
  mParent.cmdSelect_Click
End Sub

Public Sub mnuSelectEdit_Click()
  mParent.cmdEdit_Click
End Sub

Public Sub mnuSelectNew_Click()
  mParent.mnuFileNew_Click 0
End Sub

