VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "View"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Default information about a folder."
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Dataset"
'ver 2.10.0331 (jjm)
Option Explicit

Private mPopupMenuName As String
Private mPopupMenuForm As Form

Private mDefaultMenuCaption As String
Private mDefaultMenuName As String
Private mDefaultMenuIndex As Integer

Private mOpenMenuName As String
Private mPropertiesMenuName As String
Private mExploreMenuName As String

Private mNewItemName As String

Private mDataSet As Dataset
Private mDatasetName As String

Private mIsWorkflow As Boolean
Private mmIsSearchOnly As Boolean
Private mIsMaintenance As Boolean

Private mPreferenceNote As String

'local variable(s) to hold property value(s)
Private mKey As String 'local copy

Private mColumn As String


Public Property Let Key(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Key = 5
    mKey = vData
End Property


Public Property Get Key() As String
Attribute Key.VB_UserMemId = 0
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Key
    Key = mKey
End Property


Public Property Let mIsSearchOnly(ByVal vData As Boolean)
    mmIsSearchOnly = vData
End Property

Public Property Get mIsSearchOnly() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Words
    mIsSearchOnly = mmIsSearchOnly
End Property

Public Property Let IsWorkflow(ByVal vData As Boolean)
    mIsWorkflow = vData
End Property

Public Property Get IsWorkflow() As Boolean
    IsWorkflow = mIsWorkflow
End Property

'
' Dataset
'

Public Property Set Dataset(ByVal vData As Dataset)
    Set mDataSet = vData
End Property


Public Property Get Dataset() As Dataset
    Set Dataset = mDataSet
End Property

Public Property Let DatasetName(ByVal vData As String)
  mDatasetName = vData
End Property


Public Property Get DatasetName() As String
  DatasetName = mDatasetName
End Property

'
' Popup Menu Form
'

Public Property Set PopupMenuForm(vData As Form)
  Set mPopupMenuForm = vData
End Property

Public Property Get PopupMenuForm() As Form
  Set PopupMenuForm = mPopupMenuForm
End Property


'
' Popup Menu
'

Public Property Let PopupMenuName(vData As String)
  mPopupMenuName = vData
End Property


Public Property Get PopupMenuName() As String
  PopupMenuName = mPopupMenuName
End Property


Public Property Get PopupMenu() As Menu
  If (mPopupMenuForm Is Nothing Or mPopupMenuName = "") Then
    Exit Property
  End If
  Set PopupMenu = mPopupMenuForm.Controls(mPopupMenuName)
End Property


'
' Default Menu
'

Public Property Let DefaultMenuName(vData As String)
  Dim Paren As Integer
   
  Paren = InStr(vData, "(")
  
  If Paren = 0 Then
    mDefaultMenuName = vData
    mDefaultMenuIndex = -1
    Exit Property
  End If
  
  mDefaultMenuName = Mid(vData, 1, Paren - 1)
  If Mid(vData, Paren + 1, 1) <> """" Then
    mDefaultMenuIndex = CInt(Mid(vData, Paren + 1, 1))
    Exit Property
  End If
  
  mDefaultMenuCaption = Mid(vData, Paren + 2, Len(vData) - 3 - Paren) '3 is for the two " and the last )
     
End Property


Public Property Get DefaultMenuName() As String
  DefaultMenuName = mDefaultMenuName
End Property

Public Property Get DefaultMenuIndex() As Integer
  'ver 2.10.0331 (jjm)
  If mDefaultMenuCaption = vbNullString Then
    DefaultMenuIndex = mDefaultMenuIndex
  Else
    Dim c As Control
    For Each c In mPopupMenuForm.Controls(mDefaultMenuName)
      If mDefaultMenuCaption = Replace(c.Caption, "&", "") Then
          DefaultMenuIndex = c.Index
        Exit Property
      End If
    Next c
  End If
End Property


Public Property Get DefaultMenu() As Menu
  If (mPopupMenuForm Is Nothing Or mDefaultMenuName = "") Then
    Exit Property
  End If
  
  ' VB does not short circut IIf function.  Must do if then else to
  ' keep from evaluating both.
  
  'Sets the menu if there is no index
  If mDefaultMenuIndex < 0 Then
    Set DefaultMenu = mPopupMenuForm.Controls(mDefaultMenuName)
    Exit Property
  End If

  'Sets the menu if the caption is not set
  If mDefaultMenuCaption = vbNullString Then
    Set DefaultMenu = mPopupMenuForm.Controls(mDefaultMenuName)(mDefaultMenuIndex)
    Exit Property
  End If
  
  'Cycles through the controls of the menu for the caption
  Dim c As Control
  For Each c In mPopupMenuForm.Controls(mDefaultMenuName)
    If mDefaultMenuCaption = Replace(c.Caption, "&", "") Then
      Set DefaultMenu = c
      Exit Property
    End If
  Next c
  

End Property


'
' Open Menu Control Array
'

Public Property Let OpenMenuName(vData As String)
  mOpenMenuName = vData
End Property


Public Property Get OpenMenuName() As String
  OpenMenuName = mOpenMenuName
End Property

Public Property Get OpenMenu() As Object
  If (mPopupMenuForm Is Nothing Or mOpenMenuName = "") Then
    Exit Property
  End If
  Set OpenMenu = mPopupMenuForm.Controls(mOpenMenuName)
End Property


'
' Explore Menu Control Array
'

Public Property Let ExploreMenuName(vData As String)
  mExploreMenuName = vData
End Property

Public Property Get ExploreMenuName() As String
  ExploreMenuName = mExploreMenuName
End Property

Public Property Get ExploreMenu() As Object
  If (mPopupMenuForm Is Nothing Or mExploreMenuName = "") Then
    Exit Property
  End If
  Set ExploreMenu = mPopupMenuForm.Controls(mExploreMenuName)
End Property


'
' Item for Ctrl-N or Insert
'

Public Property Let NewItemName(vData As String)
  mNewItemName = vData
End Property

Public Property Get NewItemName() As String
  NewItemName = mNewItemName
End Property



'
' Properties Menu
'
'
'Public Property Let PropertiesMenuName(vData As String)
'  mPropertiesMenuName = vData
'End Property
'
'
'Public Property Get PropertiesMenuName() As String
'  PropertiesMenuName = mPropertiesMenuName
'End Property
'
'Public Property Get PropertiesMenu() As Object
'  If (mPopupMenuForm Is Nothing Or mPropertiesMenuName = "") Then
'    Exit Property
'  End If
'  Set PropertiesMenu = mPopupMenuForm.Controls(mPropertiesMenuName)
'End Property




Public Property Let PreferenceNote(vData As String)
  mPreferenceNote = vData
End Property


Public Property Get PreferenceNote() As String
  PreferenceNote = mPreferenceNote
End Property

