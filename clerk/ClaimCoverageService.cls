VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClaimCoverageService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Private mDataSet As Dataset


Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.CLAIMCOVERAGE_TABLE)
End Sub


Public Function Find(claimHandle As String, coverageHandle As String, Optional Column As String) As Collection
  Dim Handles(1) As String
  Handles(0) = claimHandle
  Handles(1) = coverageHandle
  Dim Names(1) As String
  Names(0) = DatabaseName.CLAIM_KEY
  Names(1) = DatabaseName.COVERAGE_KEY

  Set Find = mDataSet.DataProvider.FindMulti(Names, Handles, Column)
End Function

Public Function FindByCoverageType(claimHandle As String, coverageTypeHandle As String, Optional Column As String) As Collection
  Dim Handles(1) As String
  Handles(0) = claimHandle
  Handles(1) = coverageTypeHandle
  Dim Names(1) As String
  Names(0) = DatabaseName.CLAIM_KEY
  Names(1) = DatabaseName.COVERAGETYPE_KEY

  Set FindByCoverageType = mDataSet.DataProvider.FindMulti(Names, Handles, Column)
End Function


