VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DeductibleReimburseService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "DeductibleReimburseService"
Private mDataSet As Dataset
'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.DEDUCTIBLEREIMBURSE_TABLE)
  
  'Setup SOAP
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.DEDUCTIBLEREIMBURSE_KEY, Handle, Column)
End Function

'
' Interface to server
'


' parameterOrder='deductibleReimburseId claimId eventTypeId reimburseDate deductibleReimburse attributeNames attributeValues parameterNames parameterValues'>


'
' Create.
'
Public Function CreateDeductibleReimburse( _
    DeductibleReimburseId As String, _
    ClaimId As String, EventTypeId As String, _
    ReimburseDate As Date, DeductibleReimburse As Double, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Variant
    
On Error GoTo Exception
  
  CreateDeductibleReimburse = Client.Create( _
      DeductibleReimburseId, _
      ClaimId, EventTypeId, _
      ReimburseDate, DeductibleReimburse, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  Exit Function
  
Exception:
  MsgBox "Deductible Reimburse Create Service Error: " & Err.Description, vbExclamation
End Function

'
' Write an application.
'
Public Function WriteDeductibleReimburse( _
    DeductibleReimburseHandle As String, _
    ClaimId As String, EventTypeId As String, _
    ReimburseDate As Date, DeductibleReimburse As Double, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo Exception
  
  Call Client.Write( _
      DeductibleReimburseHandle, _
      ClaimId, EventTypeId, _
      ReimburseDate, DeductibleReimburse, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  WriteDeductibleReimburse = True
  Exit Function
  
Exception:
  MsgBox "Deductible Reimburse Write Service Error: " & Err.Description, vbExclamation
  WriteDeductibleReimburse = False
End Function

'
' Delete an DeductibleReimburse
'
Public Function DeleteDeductibleReimburse( _
    DeductibleReimburseHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
    
On Error GoTo Exception

  Call Client.Delete(DeductibleReimburseHandle, ParameterNames, ParameterValues)
  DeleteDeductibleReimburse = True
  Exit Function
  
Exception:
  MsgBox "Deductible Reimburse Error: " & Err.Description, vbExclamation
  DeleteDeductibleReimburse = False
End Function
