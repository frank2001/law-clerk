VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TimeService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const WSDLFile = "TimeService"

Private Const SERVICE = "TimeServiceService"

Private mClient As SoapClient30
Private mServiceName As String

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mClient = New SoapClient30
    
  Dim URL As String
  URL = HttpServer & WebComponent & WSDLFile & ".jws?wsdl"
  
  '
  ' Load the list of server versions supported
  '
  Dim ServiceList(0) As String
  ServiceList(0) = SERVICE

  '
  ' Cycle through the list of service versions and see if they are
  ' compatible with the middle tier.
  '
  Dim I As Integer
  For I = 0 To SafeUBound(ServiceList)
    Set mClient = Factory.ServiceMSSoapInit(URL, ServiceList(I))
    If Not mClient Is Nothing Then
      mServiceName = ServiceList(I)
      Exit For
    End If
  Next I
  
  If mClient Is Nothing Then
    MsgBox "TimeService Error:  Unable to connect to TimeService on server (" & HttpServer & "). This is most likely because this server does not yet support the TimeService. Time related server and client features may not work.", vbCritical, "Error"
    Exit Sub
  End If
    
  'mClient.mssoapinit HttpServer &  WebComponent & ServiceName & ".wsdl", "LawyerService"
  'mClient.ConnectorProperty("EndPointURL") = HttpServer &  WebService & LawSession.Session
End Sub

'
' Interface to server
'

'
' This function will return the difference in time between the time sent
' and the server time. If the return value is zero then there is no difference
'
Public Function CompareTime(timeDate As Date, timeString As String) As Long
    
On Error GoTo ErrorHandler

  CompareTime = mClient.Time(timeDate, timeString)
  Exit Function
  
ErrorHandler:
  'MsgBox mServiceName & " Fault: " & mClient.FaultString, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
End Function


'
' Get the server time zone
'
Public Function GetServerTimezone() As String
On Error GoTo ErrorHandler
  GetServerTimezone = mClient.timeZone
  Exit Function
ErrorHandler:
  MsgBox mServiceName & " Fault: " & mClient.FaultString, vbExclamation
End Function
