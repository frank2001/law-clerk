VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OrganizationDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'ver 2.10.0421 (jjm)
'previous ver 2.00.0310 (jjm)
Option Explicit

' Organization

Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer


Private Sub Class_Initialize()
  ListIndex = 1
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.ORGANIZATION_VIEW
'  mDataProvider.Source = DatabaseName.ORGANIZATION_TABLE
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in entity locator field only"
  frm.cboIn.AddItem "in entity name field only"
  frm.cboIn.AddItem "in organization identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, UrlName.ORGANIZATIONSEARCH_ENTITYLOCATOR_INDEX)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
 Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Select Case ListIndex
      Case UrlName.ORGANIZATIONSEARCH_ENTITYLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ENTITYLOCATOR_FIELD, Words)
      Case UrlName.ORGANIZATIONSEARCH_ENTITYNAME_INDEX
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.ENTITYNAME_FIELD, Words)
      Case UrlName.ORGANIZATIONSEARCH_ORGANIZATIONID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ORGANIZATION_KEY, Words)
    End Select
  End If
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

