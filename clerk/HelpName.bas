Attribute VB_Name = "HelpName"
Option Explicit

'Module to hold all of the help constants

Public Const ADVANCEDCOLUMNSETTINGS_TOPIC As Long = 261
Public Const ADVANCEDFIND_TOPIC As Long = 381

Public Const COLUMNSETTINGS_TOPIC As Long = 260
Public Const COLUMNPROPERTIES_TOPIC As Long = 262

Public Const ENVELOPEOPTIONS_TOPIC As Long = 420
Public Const ENVELOPEPRINTING_TOPIC As Long = 421
Public Const ENVELOPEWINDOW_TOPIC As Long = 422
Public Const ENVELOPESELECTINGPRINTER_TOPIC As Long = 423

Public Const EXPLORER_TOPIC As Long = 383           'Not currently used

Public Const FOLDERPROPERTIES_TOPIC As Long = 276

Public Const LOGIN_TOPIC As Long = 370

Public Const NAVIGATINGEXPLORER_TOPIC As Long = 380 'Not currently used

Public Const OPTIONS_TOPIC As Long = 382
Public Const ORGANIZINGFOLDERS_TOPIC As Long = 274

