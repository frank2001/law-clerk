VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Begin VB.Form frmToolbarClicker 
   Caption         =   "Form1"
   ClientHeight    =   7956
   ClientLeft      =   60
   ClientTop       =   348
   ClientWidth     =   7212
   LinkTopic       =   "Form1"
   ScaleHeight     =   7956
   ScaleWidth      =   7212
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List1 
      Height          =   5808
      Left            =   72
      TabIndex        =   4
      Top             =   1752
      Width           =   6636
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4896
      Top             =   1272
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   24
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmToolbarClicker.frx":0000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   0
      Top             =   384
      Width           =   7212
      _ExtentX        =   12721
      _ExtentY        =   593
      BandCount       =   1
      _CBWidth        =   7212
      _CBHeight       =   336
      _Version        =   "6.7.8988"
      Child1          =   "picNavigation"
      MinHeight1      =   288
      Width1          =   2352
      NewRow1         =   0   'False
      Begin VB.PictureBox picNavigation 
         BorderStyle     =   0  'None
         Height          =   288
         Left            =   24
         ScaleHeight     =   288
         ScaleWidth      =   7116
         TabIndex        =   1
         Top             =   24
         Width           =   7116
         Begin MSComctlLib.Toolbar Toolbar2 
            Height          =   324
            Left            =   2160
            TabIndex        =   2
            Top             =   0
            Width           =   1452
            _ExtentX        =   2561
            _ExtentY        =   572
            ButtonWidth     =   1122
            ButtonHeight    =   487
            Appearance      =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Two"
                  Style           =   5
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   324
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Width           =   1464
            _ExtentX        =   2582
            _ExtentY        =   572
            ButtonWidth     =   1143
            ButtonHeight    =   487
            Appearance      =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "One"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Text            =   "one"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
   End
   Begin ComCtl3.CoolBar CoolBar1 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   7212
      _ExtentX        =   12721
      _ExtentY        =   677
      _CBWidth        =   7212
      _CBHeight       =   384
      _Version        =   "6.7.8988"
      Child1          =   "picMenu"
      MinWidth1       =   792
      MinHeight1      =   336
      Width1          =   3504
      NewRow1         =   0   'False
      Child2          =   "picEvent"
      MinWidth2       =   996
      MinHeight2      =   336
      Width2          =   996
      FixedBackground2=   0   'False
      NewRow2         =   0   'False
      Child3          =   "picUpDown"
      MinWidth3       =   600
      MinHeight3      =   324
      Width3          =   948
      NewRow3         =   0   'False
      Begin VB.PictureBox picEvent 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   3660
         ScaleHeight     =   336
         ScaleWidth      =   2508
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   24
         Width           =   2508
         Begin MSComctlLib.Toolbar tbrAction 
            Height          =   276
            Left            =   0
            TabIndex        =   13
            Top             =   12
            Width           =   1044
            _ExtentX        =   1842
            _ExtentY        =   487
            ButtonWidth     =   1397
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Action"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "0"
                        Text            =   "Complete"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Tag             =   "1"
                        Text            =   "Void"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Object.Tag             =   "2"
                        Text            =   "Reopen"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   132
         ScaleHeight     =   336
         ScaleWidth      =   3348
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   24
         Width           =   3348
         Begin MSComctlLib.Toolbar tbrBrowse 
            Height          =   552
            Left            =   1836
            TabIndex        =   9
            Top             =   12
            Width           =   1056
            _ExtentX        =   1863
            _ExtentY        =   974
            ButtonWidth     =   1545
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Browse"
                  ImageIndex      =   1
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Key             =   "Application"
                        Text            =   "Application"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrEdit 
            Height          =   552
            Left            =   996
            TabIndex        =   10
            Top             =   12
            Width           =   744
            _ExtentX        =   1312
            _ExtentY        =   974
            ButtonWidth     =   1080
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Edit"
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Application"
                        Text            =   "Application"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Contact"
                        Text            =   "Contact"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Firm"
                        Text            =   "Firm"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrInsert 
            Height          =   276
            Left            =   0
            TabIndex        =   11
            Top             =   12
            Width           =   888
            _ExtentX        =   1566
            _ExtentY        =   487
            ButtonWidth     =   1143
            ButtonHeight    =   487
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "New"
                  Object.ToolTipText     =   "Click down arrow to add detail to the application."
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   5
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Lawyer"
                        Text            =   "Lawyer"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "LawArea"
                        Text            =   "Law Area"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Limit"
                        Text            =   "Limit"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Insurer"
                        Text            =   "Insurer"
                     EndProperty
                     BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Question"
                        Text            =   "Question"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picUpDown 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   324
         Left            =   6348
         ScaleHeight     =   324
         ScaleWidth      =   792
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   24
         Width           =   792
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   0
            TabIndex        =   7
            Top             =   12
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   402
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
End
Attribute VB_Name = "frmToolbarClicker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

'This is the standard way to use bitmap images in LAW. The bitmaps must be 256
'color bitmaps. Anything higher will result in a conflicting mask color. Bitmaps
'are to be captured using the 256 color display settings on the computer.

ImageList1.ListImages.Add 1, "forward", LoadPicture(App.Path & "\forward.bmp")
ImageList1.ListImages.Add 2, "back", LoadPicture(App.Path & "\back.bmp")
ImageList1.UseMaskColor = True

Toolbar1.ImageList = ImageList1
Toolbar1.Buttons(1).Image = "back"
Toolbar2.ImageList = ImageList1
Toolbar2.Buttons(1).Image = "forward"

  
End Sub

Private Sub tbrInsert_ButtonClick(ByVal Button As MSComctlLib.Button)
  List1.AddItem "New_ButtonClick"
End Sub

Private Sub tbrInsert_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  List1.AddItem "New_ButtonDropDown"
End Sub

Private Sub tbrInsert_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  List1.AddItem "New_ButtonMenuClick"
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  List1.AddItem "One_buttonClick"
End Sub

Private Sub Toolbar1_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  List1.AddItem "One_buttonDropDown"
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  List1.AddItem "One_buttonMenuClick"
End Sub

Private Sub Toolbar1_Click()
  'List1.AddItem "ToolbarOne_buttonClick"
End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
  List1.AddItem "two_buttonClick"
End Sub

Private Sub Toolbar2_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  List1.AddItem "two_buttonDropDown"
End Sub

Private Sub Toolbar2_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  List1.AddItem "two_buttonMenuClick"
End Sub

Private Sub Toolbar2_Click()
  'List1.AddItem "Toolbartwo_buttonClick"
End Sub


