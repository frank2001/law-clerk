VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmGLJournalEntry 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "WILMIC- G/L Journal Entry"
   ClientHeight    =   6168
   ClientLeft      =   120
   ClientTop       =   696
   ClientWidth     =   7644
   Icon            =   "frmGLJournalEntry.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6168
   ScaleWidth      =   7644
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6180
      Left            =   0
      ScaleHeight     =   6180
      ScaleWidth      =   7644
      TabIndex        =   0
      Top             =   0
      Width           =   7644
      Begin VB.TextBox txtDataField 
         Height          =   288
         Index           =   0
         Left            =   864
         TabIndex        =   32
         Text            =   "00101"
         Top             =   144
         Width           =   1056
      End
      Begin VB.TextBox txtDataField 
         Height          =   288
         Index           =   1
         Left            =   2592
         TabIndex        =   31
         Text            =   "Test"
         Top             =   144
         Width           =   2436
      End
      Begin VB.VScrollBar VScroll1 
         Height          =   288
         Left            =   1920
         TabIndex        =   30
         Top             =   144
         Width           =   228
      End
      Begin VB.TextBox txtDataField 
         Height          =   288
         Index           =   2
         Left            =   864
         TabIndex        =   29
         Text            =   "0002"
         Top             =   648
         Width           =   1056
      End
      Begin VB.TextBox txtDataField 
         Height          =   288
         Index           =   3
         Left            =   2580
         TabIndex        =   28
         Text            =   "Law System Import"
         Top             =   648
         Width           =   2436
      End
      Begin VB.VScrollBar VScroll2 
         Height          =   288
         Left            =   1920
         TabIndex        =   27
         Top             =   648
         Width           =   228
      End
      Begin VB.ComboBox cboDataField 
         Height          =   288
         Index           =   0
         ItemData        =   "frmGLJournalEntry.frx":000C
         Left            =   2760
         List            =   "frmGLJournalEntry.frx":0034
         TabIndex        =   25
         Top             =   996
         Width           =   756
      End
      Begin VB.ComboBox cboDataField 
         Height          =   288
         Index           =   1
         Left            =   4260
         TabIndex        =   24
         Text            =   "Combo1"
         Top             =   996
         Width           =   756
      End
      Begin VB.TextBox txtDataField 
         Height          =   288
         Index           =   4
         Left            =   864
         TabIndex        =   23
         Text            =   "Text1"
         Top             =   1332
         Width           =   936
      End
      Begin VB.CheckBox chkAutoReversal 
         Caption         =   "Auto Reversal"
         Height          =   198
         Left            =   204
         TabIndex        =   22
         Top             =   1812
         Width           =   1356
      End
      Begin VB.PictureBox Picture3 
         Height          =   384
         Left            =   3000
         ScaleHeight     =   336
         ScaleWidth      =   1968
         TabIndex        =   19
         Top             =   1680
         Width           =   2016
         Begin VB.OptionButton optNormal 
            Caption         =   "Normal"
            Height          =   216
            Left            =   84
            TabIndex        =   21
            Top             =   84
            Value           =   -1  'True
            Width           =   996
         End
         Begin VB.OptionButton optQuick 
            Caption         =   "Quick"
            Height          =   252
            Left            =   1224
            TabIndex        =   20
            Top             =   48
            Width           =   1476
         End
      End
      Begin VB.PictureBox Picture2 
         Height          =   1548
         Left            =   5160
         ScaleHeight     =   1500
         ScaleWidth      =   2244
         TabIndex        =   8
         Top             =   540
         Width           =   2292
         Begin VB.Label Label2 
            Caption         =   "Entries:"
            Height          =   204
            Left            =   84
            TabIndex        =   18
            Top             =   84
            Width           =   684
         End
         Begin VB.Label Label3 
            Caption         =   "Debits:"
            Height          =   204
            Left            =   84
            TabIndex        =   17
            Top             =   336
            Width           =   684
         End
         Begin VB.Label Label7 
            Caption         =   "Credits:"
            Height          =   204
            Left            =   84
            TabIndex        =   16
            Top             =   588
            Width           =   684
         End
         Begin VB.Label Label9 
            Caption         =   "Type:"
            Height          =   204
            Left            =   84
            TabIndex        =   15
            Top             =   972
            Width           =   684
         End
         Begin VB.Label Label10 
            Caption         =   "Status:"
            Height          =   204
            Left            =   84
            TabIndex        =   14
            Top             =   1224
            Width           =   684
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "entries"
            Height          =   192
            Index           =   1
            Left            =   1476
            TabIndex        =   13
            Top             =   84
            UseMnemonic     =   0   'False
            Width           =   480
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "debits"
            Height          =   192
            Index           =   2
            Left            =   1512
            TabIndex        =   12
            Top             =   336
            UseMnemonic     =   0   'False
            Width           =   444
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "credits"
            Height          =   192
            Index           =   3
            Left            =   1476
            TabIndex        =   11
            Top             =   588
            UseMnemonic     =   0   'False
            Width           =   480
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "type"
            Height          =   192
            Index           =   4
            Left            =   1644
            TabIndex        =   10
            Top             =   972
            UseMnemonic     =   0   'False
            Width           =   312
         End
         Begin VB.Label lblDataField 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "status"
            Height          =   192
            Index           =   5
            Left            =   1536
            TabIndex        =   9
            Top             =   1224
            UseMnemonic     =   0   'False
            Width           =   420
         End
      End
      Begin VB.Frame Frame1 
         Height          =   24
         Left            =   180
         TabIndex        =   7
         Top             =   540
         Width           =   4872
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   336
         Left            =   204
         TabIndex        =   4
         Top             =   5664
         Width           =   1200
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Delete"
         Height          =   336
         Left            =   1452
         TabIndex        =   3
         Top             =   5664
         Width           =   1200
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "C&lear"
         Height          =   336
         Left            =   2700
         TabIndex        =   2
         Top             =   5664
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "&Close"
         Height          =   336
         Left            =   6264
         TabIndex        =   1
         Top             =   5664
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   2868
         Left            =   204
         TabIndex        =   5
         Top             =   2232
         Width           =   7260
         _ExtentX        =   12806
         _ExtentY        =   5059
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "G/L"
         TabPicture(0)   =   "frmGLJournalEntry.frx":0068
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lvGL"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         Begin MSComctlLib.ListView lvGL 
            Height          =   2412
            Left            =   84
            TabIndex        =   6
            Top             =   324
            Width           =   7068
            _ExtentX        =   12467
            _ExtentY        =   4255
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
      Begin MSComCtl2.DTPicker dtpDataField 
         Height          =   288
         Left            =   864
         TabIndex        =   26
         Top             =   996
         Width           =   1044
         _ExtentX        =   1842
         _ExtentY        =   508
         _Version        =   393216
         Format          =   43974657
         CurrentDate     =   37139
      End
      Begin VB.Label lblDate 
         Caption         =   "Date"
         Height          =   204
         Left            =   204
         TabIndex        =   46
         Top             =   1080
         Width           =   552
      End
      Begin VB.Label Label4 
         Caption         =   "Year"
         Height          =   204
         Left            =   3768
         TabIndex        =   45
         Top             =   1080
         Width           =   516
      End
      Begin VB.Label Label5 
         Caption         =   "Source"
         Height          =   204
         Left            =   204
         TabIndex        =   44
         Top             =   1416
         Width           =   540
      End
      Begin VB.Label Label6 
         Caption         =   "Period"
         Height          =   204
         Left            =   2208
         TabIndex        =   43
         Top             =   1080
         Width           =   660
      End
      Begin VB.Label lblDataField 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Law System"
         Height          =   288
         Index           =   0
         Left            =   2016
         TabIndex        =   42
         Top             =   1332
         UseMnemonic     =   0   'False
         Width           =   3000
      End
      Begin VB.Label Label1 
         Caption         =   "Entry Mode"
         Height          =   204
         Left            =   2016
         TabIndex        =   41
         Top             =   1812
         Width           =   876
      End
      Begin VB.Label Label8 
         Caption         =   "Debits"
         Height          =   204
         Left            =   204
         TabIndex        =   40
         Top             =   5268
         Width           =   600
      End
      Begin VB.Label Label11 
         Caption         =   "Credits"
         Height          =   204
         Left            =   2448
         TabIndex        =   39
         Top             =   5268
         Width           =   600
      End
      Begin VB.Label Label12 
         Caption         =   "Out Of Balance By"
         Height          =   204
         Left            =   4860
         TabIndex        =   38
         Top             =   5268
         Width           =   1092
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "debits"
         Height          =   288
         Left            =   852
         TabIndex        =   37
         Top             =   5232
         UseMnemonic     =   0   'False
         Width           =   1416
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "credits"
         Height          =   288
         Left            =   3060
         TabIndex        =   36
         Top             =   5232
         UseMnemonic     =   0   'False
         Width           =   1416
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "out  of balance"
         Height          =   288
         Left            =   6048
         TabIndex        =   35
         Top             =   5232
         UseMnemonic     =   0   'False
         Width           =   1416
      End
      Begin VB.Label lblEntry 
         Caption         =   "Entry"
         Height          =   204
         Left            =   204
         TabIndex        =   34
         Top             =   732
         Width           =   552
      End
      Begin VB.Label Label16 
         Caption         =   "Batch"
         Height          =   204
         Left            =   204
         TabIndex        =   33
         Top             =   228
         Width           =   552
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
   End
   Begin VB.Menu mnuBatch 
      Caption         =   "&Batch"
   End
   Begin VB.Menu mnuEntry 
      Caption         =   "E&ntry"
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
   End
End
Attribute VB_Name = "frmGLJournalEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub
Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  FormResize
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
End Sub

