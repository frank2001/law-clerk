VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmClaimPaymentEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Payment"
   ClientHeight    =   5640
   ClientLeft      =   120
   ClientTop       =   744
   ClientWidth     =   7896
   Icon            =   "frmClaimPaymentEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   7896
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5208
      Left            =   0
      ScaleHeight     =   5208
      ScaleWidth      =   7896
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   384
      Width           =   7896
      Begin VB.CommandButton cmdVendor 
         Caption         =   "Vendor..."
         Enabled         =   0   'False
         Height          =   288
         Left            =   6768
         TabIndex        =   21
         Top             =   816
         Width           =   1000
      End
      Begin VB.CommandButton cmdClaim 
         Caption         =   "C&laim"
         Height          =   288
         Left            =   6768
         TabIndex        =   20
         Top             =   84
         Width           =   1000
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   4140
         TabIndex        =   17
         Top             =   4860
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   5364
         TabIndex        =   18
         Top             =   4860
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   6600
         TabIndex        =   19
         Top             =   4860
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3450
         Left            =   45
         TabIndex        =   50
         Top             =   1320
         Width           =   7815
         _ExtentX        =   13780
         _ExtentY        =   6075
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmClaimPaymentEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label3"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label4"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label6"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Label17"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lvGL"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtpDataField(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtDataField(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboEventTypeName"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtDataField(13)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtDataField(30)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "Frame1"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "cboPayee"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "&Misc"
         TabPicture(1)   =   "frmClaimPaymentEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Label14"
         Tab(1).Control(1)=   "Label16"
         Tab(1).Control(2)=   "txtDataField(10)"
         Tab(1).Control(3)=   "optYesNo(1)"
         Tab(1).Control(4)=   "optYesNo(0)"
         Tab(1).ControlCount=   5
         TabCaption(2)   =   "N&ote"
         TabPicture(2)   =   "frmClaimPaymentEdit.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "txtNote"
         Tab(2).Control(1)=   "cmdClearNotepad"
         Tab(2).ControlCount=   2
         TabCaption(3)   =   "&Advanced"
         TabPicture(3)   =   "frmClaimPaymentEdit.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Label2"
         Tab(3).Control(1)=   "Label11"
         Tab(3).Control(2)=   "Label12"
         Tab(3).Control(3)=   "Label15"
         Tab(3).Control(4)=   "Label18(0)"
         Tab(3).Control(5)=   "Label19"
         Tab(3).Control(6)=   "Label21(0)"
         Tab(3).Control(7)=   "Label22"
         Tab(3).Control(8)=   "Label23"
         Tab(3).Control(9)=   "Label5"
         Tab(3).Control(10)=   "Label21(2)"
         Tab(3).Control(11)=   "Label21(1)"
         Tab(3).Control(12)=   "Label18(1)"
         Tab(3).Control(13)=   "Label20"
         Tab(3).Control(14)=   "txtDataField(11)"
         Tab(3).Control(15)=   "txtDataField(14)"
         Tab(3).Control(16)=   "txtDataField(15)"
         Tab(3).Control(17)=   "txtDataField(16)"
         Tab(3).Control(18)=   "txtDataField(18)"
         Tab(3).Control(19)=   "txtDataField(17)"
         Tab(3).Control(20)=   "txtDataField(20)"
         Tab(3).Control(21)=   "txtDataField(21)"
         Tab(3).Control(21).Enabled=   0   'False
         Tab(3).Control(22)=   "txtDataField(3)"
         Tab(3).Control(23)=   "txtBalance"
         Tab(3).Control(23).Enabled=   0   'False
         Tab(3).Control(24)=   "txtDeductibleDirect"
         Tab(3).Control(24).Enabled=   0   'False
         Tab(3).Control(25)=   "txtDataField(19)"
         Tab(3).ControlCount=   26
         Begin VB.ComboBox cboPayee 
            Height          =   288
            ItemData        =   "frmClaimPaymentEdit.frx":007C
            Left            =   1200
            List            =   "frmClaimPaymentEdit.frx":007E
            TabIndex        =   3
            Text            =   "cboPayee"
            Top             =   1297
            Width           =   2436
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74925
            TabIndex        =   65
            Top             =   3045
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   2640
            Left            =   -74940
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   64
            Top             =   300
            Width           =   7575
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "paymentAmount"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   19
            Left            =   -74796
            Locked          =   -1  'True
            TabIndex        =   59
            Text            =   "paymentAmount"
            Top             =   1512
            Width           =   1200
         End
         Begin VB.Frame Frame1 
            Caption         =   "Paid by:"
            Height          =   1200
            Left            =   6396
            TabIndex        =   56
            Top             =   1680
            Width           =   1284
            Begin VB.OptionButton optPaidBy 
               Caption         =   "Refund"
               Height          =   216
               Index           =   2
               Left            =   108
               TabIndex        =   60
               Top             =   840
               Width           =   1008
            End
            Begin VB.OptionButton optPaidBy 
               Caption         =   "Insured"
               Height          =   216
               Index           =   1
               Left            =   108
               TabIndex        =   58
               Top             =   576
               Width           =   828
            End
            Begin VB.OptionButton optPaidBy 
               Caption         =   "WILMIC"
               Height          =   216
               Index           =   0
               Left            =   108
               TabIndex        =   57
               Top             =   300
               Value           =   -1  'True
               Width           =   915
            End
         End
         Begin VB.TextBox txtDeductibleDirect 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            Height          =   288
            Left            =   -73548
            Locked          =   -1  'True
            TabIndex        =   54
            TabStop         =   0   'False
            Text            =   "deductibleDirect"
            Top             =   2952
            Width           =   1200
         End
         Begin VB.TextBox txtBalance 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000B&
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            Height          =   288
            Left            =   -72300
            Locked          =   -1  'True
            TabIndex        =   53
            TabStop         =   0   'False
            Top             =   1512
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "deductibleOverpay"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   3
            Left            =   -72300
            Locked          =   -1  'True
            TabIndex        =   51
            Text            =   "deductibleOverpay"
            Top             =   2952
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            Height          =   288
            Index           =   30
            Left            =   6468
            TabIndex        =   5
            Text            =   "custom amount"
            Top             =   1284
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000B&
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            Height          =   288
            Index           =   21
            Left            =   -71052
            Locked          =   -1  'True
            TabIndex        =   46
            TabStop         =   0   'False
            Top             =   2952
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "paymentOther"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   20
            Left            =   -72300
            Locked          =   -1  'True
            TabIndex        =   12
            Text            =   "paymentOther"
            Top             =   840
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "paymentIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   17
            Left            =   -73548
            Locked          =   -1  'True
            TabIndex        =   11
            Text            =   "paymentIndemnity"
            Top             =   840
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "paymentALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   18
            Left            =   -74796
            Locked          =   -1  'True
            TabIndex        =   10
            Text            =   "paymentALAE"
            Top             =   840
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "deductibleAdvance"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   16
            Left            =   -74796
            Locked          =   -1  'True
            TabIndex        =   15
            Text            =   "deductibleAdvance"
            Top             =   2952
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "deductibleDirect"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   15
            Left            =   -73548
            Locked          =   -1  'True
            TabIndex        =   16
            Text            =   "deductibleDirect"
            Top             =   1512
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "deductibleALAE"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   14
            Left            =   -74796
            Locked          =   -1  'True
            TabIndex        =   13
            Text            =   "deductibleALAE"
            Top             =   2352
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            DataField       =   "deductibleIndemnity"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "Amount"
            Height          =   288
            Index           =   11
            Left            =   -73548
            Locked          =   -1  'True
            TabIndex        =   14
            Text            =   "deductibleIndemnity"
            Top             =   2352
            Width           =   1200
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "referenceNumber"
            DataMember      =   "Disbursement"
            Height          =   288
            Index           =   13
            Left            =   5736
            TabIndex        =   1
            Text            =   "referenceNumber"
            Top             =   612
            Width           =   1212
         End
         Begin VB.ComboBox cboEventTypeName 
            Height          =   288
            ItemData        =   "frmClaimPaymentEdit.frx":0080
            Left            =   3696
            List            =   "frmClaimPaymentEdit.frx":0082
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   1284
            Width           =   2724
         End
         Begin VB.OptionButton optYesNo 
            Caption         =   "No"
            Enabled         =   0   'False
            Height          =   192
            Index           =   0
            Left            =   -73860
            TabIndex        =   9
            Top             =   1944
            Width           =   588
         End
         Begin VB.OptionButton optYesNo 
            Caption         =   "Yes"
            Enabled         =   0   'False
            Height          =   192
            Index           =   1
            Left            =   -74772
            TabIndex        =   8
            Top             =   1932
            Width           =   816
         End
         Begin VB.TextBox txtDataField 
            BackColor       =   &H80000016&
            DataField       =   "payeeAddress"
            DataMember      =   "Event"
            Enabled         =   0   'False
            Height          =   768
            Index           =   10
            Left            =   -74916
            Locked          =   -1  'True
            TabIndex        =   7
            Top             =   588
            Width           =   5088
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "checkNumber"
            DataMember      =   "Disbursement"
            Height          =   288
            Index           =   0
            Left            =   84
            TabIndex        =   2
            Text            =   "checkNumber"
            Top             =   1284
            Width           =   1068
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmClaimPaymentEdit.frx":0084
            DataField       =   "eventDate"
            DataMember      =   "Event"
            Height          =   288
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Top             =   588
            Width           =   1212
            _ExtentX        =   2138
            _ExtentY        =   508
            _Version        =   393216
            Format          =   165085185
            CurrentDate     =   37116
         End
         Begin MSComctlLib.ListView lvGL 
            Height          =   1572
            Left            =   1800
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "ColumnFixed"
            Top             =   1704
            Width           =   4512
            _ExtentX        =   7959
            _ExtentY        =   2773
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "glAccountId"
               Text            =   "Account"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "transactionName"
               Text            =   "Name"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "transactionReference"
               Text            =   "Reference"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "transactionAmount"
               Text            =   "Amount"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Debit"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Credit"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label Label20 
            AutoSize        =   -1  'True
            Caption         =   "Insured:"
            Height          =   192
            Left            =   -73548
            TabIndex        =   63
            Top             =   1296
            Width           =   564
         End
         Begin VB.Label Label18 
            AutoSize        =   -1  'True
            Caption         =   "WILMIC:"
            Height          =   192
            Index           =   1
            Left            =   -74796
            TabIndex        =   62
            Top             =   1296
            Width           =   588
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            Caption         =   "Balance:"
            Height          =   192
            Index           =   1
            Left            =   -72300
            TabIndex        =   61
            Top             =   1296
            Width           =   636
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            Caption         =   "Direct:"
            Height          =   192
            Index           =   2
            Left            =   -73548
            TabIndex        =   55
            Top             =   2736
            Width           =   456
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Overpay:"
            Height          =   192
            Left            =   -72300
            TabIndex        =   52
            Top             =   2736
            Width           =   660
         End
         Begin VB.Label Label17 
            Caption         =   "Amount:"
            Height          =   192
            Left            =   6468
            TabIndex        =   49
            Top             =   1032
            Width           =   1092
         End
         Begin VB.Label Label23 
            AutoSize        =   -1  'True
            Caption         =   "Deductible"
            Height          =   192
            Left            =   -74796
            TabIndex        =   48
            Top             =   1896
            Width           =   780
         End
         Begin VB.Label Label22 
            AutoSize        =   -1  'True
            Caption         =   "Expense"
            Height          =   192
            Left            =   -74796
            TabIndex        =   47
            Top             =   360
            Width           =   636
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            Caption         =   "Balance:"
            Height          =   192
            Index           =   0
            Left            =   -71052
            TabIndex        =   45
            Top             =   2736
            Width           =   636
         End
         Begin VB.Label Label19 
            AutoSize        =   -1  'True
            Caption         =   "Advanced:"
            Height          =   192
            Left            =   -74796
            TabIndex        =   37
            Top             =   2736
            Width           =   780
         End
         Begin VB.Label Label18 
            AutoSize        =   -1  'True
            Caption         =   "Other:"
            Height          =   192
            Index           =   0
            Left            =   -72300
            TabIndex        =   34
            Top             =   624
            Width           =   420
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "ALAE:"
            Height          =   192
            Left            =   -74796
            TabIndex        =   31
            Top             =   624
            Width           =   444
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Indemnity:"
            Height          =   192
            Left            =   -73548
            TabIndex        =   33
            Top             =   624
            Width           =   720
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "ALAE:"
            Height          =   192
            Left            =   -74796
            TabIndex        =   35
            Top             =   2136
            Width           =   444
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Indemnity:"
            Height          =   192
            Left            =   -73548
            TabIndex        =   36
            Top             =   2136
            Width           =   720
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Invoice:"
            Height          =   192
            Index           =   1
            Left            =   5016
            TabIndex        =   41
            Top             =   660
            Width           =   552
         End
         Begin VB.Label Label16 
            Caption         =   "If Indemnity Payment: Is the underlying claim a Personal Injury (PI) action? AND is Payee and Individual?"
            Height          =   192
            Left            =   -74892
            TabIndex        =   40
            Top             =   1620
            Width           =   7476
         End
         Begin VB.Label Label14 
            Caption         =   "New Payer Address"
            Height          =   192
            Left            =   -74916
            TabIndex        =   39
            Top             =   360
            Width           =   2256
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Accounting Records:"
            Height          =   192
            Left            =   120
            TabIndex        =   28
            Top             =   1740
            Width           =   1488
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Payment type:"
            Height          =   192
            Left            =   3708
            TabIndex        =   27
            Top             =   1032
            Width           =   1092
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Payee:"
            Height          =   192
            Left            =   1200
            TabIndex        =   26
            Top             =   1032
            Width           =   516
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Check no:"
            Height          =   192
            Index           =   0
            Left            =   84
            TabIndex        =   25
            Top             =   1032
            Width           =   708
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Payment Date:"
            Height          =   192
            Left            =   120
            TabIndex        =   24
            Top             =   336
            Width           =   1092
            WordWrap        =   -1  'True
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   1176
         Top             =   3912
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   1428
         Top             =   3864
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.CommandButton cmdEnter 
         Caption         =   "Enter"
         Height          =   288
         Left            =   300
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   3876
         Visible         =   0   'False
         Width           =   1056
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "firmName"
         DataField       =   "firmName"
         DataMember      =   "Instance"
         Height          =   195
         Index           =   0
         Left            =   210
         TabIndex        =   44
         Top             =   135
         UseMnemonic     =   0   'False
         Width           =   660
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "claimant"
         DataField       =   "claimant"
         DataMember      =   "Instance"
         Height          =   192
         Index           =   1
         Left            =   204
         TabIndex        =   43
         Top             =   420
         UseMnemonic     =   0   'False
         Width           =   600
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "entityName (Vendor)"
         DataField       =   "entityName"
         DataMember      =   "Vendor"
         Height          =   195
         Index           =   2
         Left            =   210
         TabIndex        =   42
         Top             =   870
         UseMnemonic     =   0   'False
         Width           =   1440
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   7896
      _ExtentX        =   13928
      _ExtentY        =   677
      BandCount       =   1
      _CBWidth        =   7896
      _CBHeight       =   384
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   336
         ScaleWidth      =   7800
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   24
         Width           =   7800
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   38
            Top             =   60
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpenClaim 
            Caption         =   "&Claim"
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator834 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileCompleteClose 
         Caption         =   "Complete and c&lose..."
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator382 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator990 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
      End
      Begin VB.Menu mnuSeperator989 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator951 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmClaimPaymentEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.10.0430 (jjm)
'previous ver 1.90.0124 (jjm)

Option Explicit


Const EVENTTYPENAMELISTINDEX_PARAM = "cboEventTypeIndex"

Private mClaimPaymentHandle As String
Private mClaimHandle As String
Private mEventHandle As String
Private mEventTypeHandle As String
Private mEventStatus As String
Private GLTransaction As Dataset

Private mNew As Boolean
Private mDatafields As collection
Private mDataSet As Dataset
Private mListIndex As Integer
Private mListView As ListView
Private mListItemType As String
Private mDirty As Boolean

Private mEventTypes As collection
Private mEventTypeNameListIndex As Integer

' Parameters for creating new claim payment.
Private mParameterNames() As String
Private mParameterValues() As String

Private FormMgr As New FormManager

Private Const TXT_PAYMENTAMOUNT_INDEX = 30
Private Const TXT_WILMICPAYMENTAMOUNT_INDEX = 19 'on the advanced tab
Private Const TXT_DEDUCTIBLEDIRECT_INDEX = 15
Private Const TXT_BALANCE_INDEX = 21


Private Const CURRENCY_FORMAT = "#,###.00;(#,###.00)"

'
' Close and unload the form without saving
'
Private Sub Cancel()
  mDirty = False
  Unload Me
End Sub

Private Sub ClaimRefresh()
  ClearDataField lblDataField, "Instance"
  
  If mClaimHandle = vbNullString Then
    Exit Sub
  End If
  
  SetFields lblDataField, Factory.InstanceService.Find(mClaimHandle), "Instance"
  Caption = IIf(mNew, "New", "Edit") & " Payment (" & mClaimHandle & ")"
  
  '2626 - Mantis Update payee dropdown options
  Dim contactDataset As Dataset
  Set contactDataset = NewGenericDataset(DatabaseName.CONTACT_VIEW)
  contactDataset.DataProvider.Column = DatabaseName.PERSONENTITYNAME_FIELD & "," & DatabaseName.LOCATIONENTITYNAME_FIELD
  contactDataset.DataProvider.filter = LawDialect.EqualString(DatabaseName.CLAIM_KEY, mClaimHandle)
  contactDataset.DataProvider.OpenRecordset
  
  Dim ContactCollection As collection
  Set ContactCollection = contactDataset.DataProvider.collection
  
  If Not ContactCollection Is Nothing Then
  
    Dim distinctNameSet As New collection
    Dim I, Count As Integer
    Dim testName As String
    
    Count = ContactCollection.Count
    For I = 1 To Count
      'Default name to use is ContactView.locationEntityName
      testName = GetDataFromCollection(ContactCollection.item(I), DatabaseName.LOCATIONENTITYNAME_FIELD)
      'If locationEntityName value does not exist, use personEntityName
      If testName = vbNullString Then
        testName = GetDataFromCollection(ContactCollection.item(I), DatabaseName.PERSONENTITYNAME_FIELD)
      End If
      
      'Distinct values
      If Not Contains(distinctNameSet, testName) Then
        distinctNameSet.Add testName, testName
      End If
    Next I
    
    Util.BubbleSortStringCollection distinctNameSet
       
    Dim j As Integer
    For j = 1 To distinctNameSet.Count
      cboPayee.AddItem distinctNameSet.item(j)
    Next j
    'FormUtil.ComboBoxLoad contactDataset.DataProvider.collection, cboPayee, DatabaseName.PERSONENTITYNAME_FIELD
  End If
  
End Sub


'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub


Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form selected
    Case fimSelect
      mClaimHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.CLAIM_KEY)
      ClaimRefresh
      Dirty = True
  End Select
  Show
  dtpDataField(0).SetFocus
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormPrepare(PaymentAmount As Double, DirectAmount As Double)
  If txtDataField(TXT_PAYMENTAMOUNT_INDEX).Locked Then
    Exit Sub
  End If
  
  Dim xmlHttp As xmlHttp
  Dim xmlUrl As String
  xmlUrl = HttpServer & WebUtil & "claimpaymentprepare?key=" & mClaimHandle & _
                                                        "&key=" & EncodeURL(mEventTypeHandle) & _
                                                        "&key=" & Format(PaymentAmount, "############.00") & _
                                                        "&key=" & Format(DirectAmount, "############.00") & "&source=text/xml"
  Set xmlHttp = XMLUtil.HttpGet(xmlUrl)
      
  'MsgBox xmlHttp.responseText
  Dim Model As collection
  Set Model = XMLUtil.RowsetOLD(xmlHttp.responseXML.documentElement, "claimpayment")
  If Model Is Nothing Then
    ErrorUtil.DisplayXMLHttpError xmlHttp, xmlUrl
    'MsgBox "Error: Service was unabled to be called. " & xmlUrl, vbCritical
    Exit Sub
  End If

  SetFields Me.txtDataField, Model(1), "Amount"
End Sub

Private Sub FormRefresh()
  mEventStatus = vbNullString
  
  cmdClaim.Enabled = mClaimPaymentHandle = vbNullString
  
  'clear everything
  ClearDataField txtDataField
  ClearDataField dtpDataField
  ClearDataField lblDataField
  
  txtDeductibleDirect.Text = vbNullString
   
  If (mNew) Then
    mEventHandle = vbNullString
    cboEventTypeName.ListIndex = mEventTypeNameListIndex
    cboPayee = vbNullString
    dtpDataField(0).Value = Today

    Caption = "New Claim Payment (" & mClaimHandle & ")"
    txtNote.Text = vbNullString
  Else
    Dim DataField As collection
    Set DataField = Factory.ClaimPaymentService.Find(mClaimPaymentHandle)
        
    If DataField Is Nothing Then
      MsgBox "No Data Found. Form: ClaimPaymentEdit", vbCritical
      Unload Me
      Exit Sub
    End If
    
    'FormPrepare may not be called on edit
    SetFields txtDataField, DataField, "Amount"
        
    mClaimHandle = DataField(DatabaseName.CLAIM_KEY)
    
    'get the handle
    Dim disbursementHandle As String
    disbursementHandle = DataField(DatabaseName.DISBURSEMENT_KEY)
    ' get the disbursement data
    Dim DisbursementData As collection
    Set DisbursementData = GenericFind(DatabaseName.DISBURSEMENT_TABLE, DatabaseName.DISBURSEMENT_KEY, disbursementHandle)
    ' load the data into the form
    SetFields txtDataField, DisbursementData, "Disbursement"
    
    'manually set the payee dropdown
    cboPayee.Text = GetDataFromCollection(DisbursementData, DatabaseName.PAYEE_FIELD)
        
    mEventHandle = DataField(DatabaseName.EVENT_KEY)
    
    Dim EventData As collection
    Set EventData = Factory.EventService.Find(mEventHandle)
    SetFields dtpDataField, EventData, "Event" 'sets the payment date, aka eventDate
    txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    mEventStatus = EventData(DatabaseName.EVENTSTATUS_FIELD)
    
    'Backward Compatability Hack: manually set the paymentAmount field. This is stored in eventAmount, however the FormPrepare returns a "paymentAmount" node.
    txtDataField(TXT_WILMICPAYMENTAMOUNT_INDEX).Text = GetDataFromCollection(EventData, DatabaseName.EVENTAMOUNT_FIELD)
    
    
    'set the dropdown list
    mEventTypeHandle = GetDataFromCollection(EventData, DatabaseName.EVENTTYPE_KEY)
    cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, mEventTypeHandle) - 1
    
    Caption = "Claim Payment (" & mClaimHandle & ")"
  End If
    
  ClaimRefresh
  
  
  'Filters
  
  ' ver 1.90.1226
  ' corrected filter to DatabaseName.EVENT_KEY
  GLTransaction.DataProvider.filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle)
  ListViewLoad lvGL, GLTransaction.DataProvider, True
  
  Dim item As ListItem
  Dim transactionAmount As Double
  For Each item In lvGL.ListItems
    transactionAmount = Val(item.ListSubItems(DatabaseName.TRANSACTIONAMOUNT_FIELD))
    If transactionAmount < 0 Then
      item.SubItems(5) = Format(0 - transactionAmount, CURRENCY_FORMAT)
    Else
      item.SubItems(4) = Format(transactionAmount, CURRENCY_FORMAT)
    End If
  Next item
  
  ' Save amount before PaidByRefresh.
  txtDataField(TXT_PAYMENTAMOUNT_INDEX).Text = GetDouble(txtDataField(TXT_WILMICPAYMENTAMOUNT_INDEX)) _
                                              + GetDouble(txtDataField(TXT_DEDUCTIBLEDIRECT_INDEX))
  
  ' Set locking of txtDataField(TXT_PAYMENTAMOUNT_INDEX).
  txtDataField(TXT_PAYMENTAMOUNT_INDEX).Locked = mEventStatus = "Complete"
  
  ' Update who paid
  PaidByRefresh
  
  '
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  '
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' After refresh, record is not changed if existing, changed if new
  Dirty = False
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.CLAIMPAYMENT_TABLE
      mClaimPaymentHandle = ListItemText(mListView.SelectedItem, DatabaseName.CLAIMPAYMENT_KEY)
    Case DatabaseName.EVENT_TABLE
      mClaimPaymentHandle = FindHandle(DatabaseName.CLAIMPAYMENT_TABLE, _
                                       DatabaseName.EVENT_KEY, _
                                       ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY), _
                                       DatabaseName.CLAIMPAYMENT_KEY)
  End Select
End Property

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  
  ' Start Custom Code
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  If Val(txtDataField(TXT_BALANCE_INDEX)) <> 0 Or Val(txtBalance.Text) <> 0 Then
    MsgBox "Balance must be 0 in order to save!  See advanced tab.", vbExclamation + vbOKOnly, "Balance Error"
    Save = False
    Exit Function
  End If
  
  Dim mDataControl As New collection
  AddTextControl txtDataField, mDataControl
  AddValueControl dtpDataField, mDataControl
  
    
  If mNew Then
    mClaimPaymentHandle = Factory.ClaimPaymentService.CreateClaimPayment( _
        vbNullString, mClaimHandle, mEventTypeHandle, _
        GetDate(GetDataFromCollection(mDataControl, DatabaseName.EVENTDATE_FIELD)), _
        vbNullString, _
        GetDataFromCollection(mDataControl, DatabaseName.REFERENCENUMBER_FIELD), _
        GetDataFromCollection(mDataControl, DatabaseName.CHECKNUMBER_FIELD), _
        cboPayee.Text, _
        GetDouble(GetDataFromCollection(mDataControl, DatabaseName.PAYMENTALAE_FIELD)), _
        GetDouble(GetDataFromCollection(mDataControl, DatabaseName.PAYMENTINDEMNITY_FIELD)), _
        GetDouble(txtDataField(TXT_PAYMENTAMOUNT_INDEX)), _
        GetDouble(GetDataFromCollection(mDataControl, DatabaseName.DEDUCTIBLEALAE_FIELD)), _
        GetDouble(GetDataFromCollection(mDataControl, DatabaseName.DEDUCTIBLEINDEMNITY_FIELD)), _
        GetDouble(GetDataFromCollection(mDataControl, DatabaseName.DEDUCTIBLEADVANCE_FIELD)), _
        GetDouble(GetDataFromCollection(mDataControl, DatabaseName.DEDUCTIBLEDIRECT_FIELD)), _
        txtNote.Text)
    Save = mClaimPaymentHandle <> vbNullString
    If Save Then
      mNew = False
    End If
  Else
    Save = Factory.ClaimPaymentService.WriteClaimPayment( _
        mClaimPaymentHandle, mClaimHandle, mEventTypeHandle, _
        GetDate(mDataControl(DatabaseName.EVENTDATE_FIELD)), _
        vbNullString, _
        mDataControl(DatabaseName.REFERENCENUMBER_FIELD), _
        mDataControl(DatabaseName.CHECKNUMBER_FIELD), _
        cboPayee.Text, _
        GetDouble(mDataControl(DatabaseName.PAYMENTALAE_FIELD)), _
        GetDouble(mDataControl(DatabaseName.PAYMENTINDEMNITY_FIELD)), _
        GetDouble(txtDataField(TXT_PAYMENTAMOUNT_INDEX)), _
        GetDouble(mDataControl(DatabaseName.DEDUCTIBLEALAE_FIELD)), _
        GetDouble(mDataControl(DatabaseName.DEDUCTIBLEINDEMNITY_FIELD)), _
        GetDouble(mDataControl(DatabaseName.DEDUCTIBLEADVANCE_FIELD)), _
        GetDouble(mDataControl(DatabaseName.DEDUCTIBLEDIRECT_FIELD)), _
        txtNote.Text)
  End If

  If Save Then
    FormRefresh
  Else
    Dirty = True
  End If
  
  'End custom code
End Function
          
Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.CLAIMPAYMENT_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no law areas to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(ClaimPaymentId As String)
  mClaimPaymentHandle = ClaimPaymentId
  Show
End Sub


Public Sub ShowNew(Optional vClaimHandle As String)
  mClaimHandle = vClaimHandle
  mNew = True
  Show
  
  If mClaimHandle = vbNullString Then
    cmdClaim_Click
  End If
End Sub

' ver 1.90.1226 (srd)
Private Sub PaidByRefresh()
    If (GetDouble(txtDataField(TXT_DEDUCTIBLEDIRECT_INDEX)) <> 0 And _
        GetDouble(txtDataField(TXT_WILMICPAYMENTAMOUNT_INDEX)) <> 0) Then
      optPaidBy(2) = True  'Insured
    ElseIf (GetDouble(txtDataField(TXT_DEDUCTIBLEDIRECT_INDEX)) <> 0) Then
      optPaidBy(1) = True  'Refund
    Else
      optPaidBy(0) = True  'WILMIC
    End If
End Sub

Private Sub BalanceRefresh()
  Dim Amount As Double
  Amount = GetDouble(txtDataField(TXT_PAYMENTAMOUNT_INDEX))

  
  If optPaidBy(0) Then
    FormPrepare Amount, 0
  ElseIf optPaidBy(1) Then
    FormPrepare 0, Amount
  Else
    FormPrepare Amount, 0 - Amount
  End If
  
  Dim Balance As Double
  ' Expense Balance is expenses (ALAE, Indemnity, Other) - (Cash + Direct) - ( deductible ALAE, Indemnity, Advanced)
  Balance = (GetDouble(txtDataField(18)) + GetDouble(txtDataField(17)) + GetDouble(txtDataField(20))) - _
            (GetDouble(txtDataField(TXT_WILMICPAYMENTAMOUNT_INDEX)) + GetDouble(txtDataField(TXT_DEDUCTIBLEDIRECT_INDEX)))
  txtBalance.Text = Balance
  
  ' Copy Direct and OVerpay
  txtDeductibleDirect = txtDataField(TXT_DEDUCTIBLEDIRECT_INDEX) 'changing to be updated on Prepare
  
  ' Deductible Balance
  Balance = (GetDouble(txtDataField(14)) + GetDouble(txtDataField(11))) - _
            (GetDouble(txtDataField(16)) + GetDouble(txtDataField(TXT_DEDUCTIBLEDIRECT_INDEX)) - GetDouble(txtDataField(3)))
  
  txtDataField(TXT_BALANCE_INDEX).Text = Balance
End Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' END GENERAL METHODS
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Private Sub cboEventTypeName_Click()
  If cboEventTypeName.ListIndex < 0 Then
    mEventTypeHandle = vbNullString
  Else
    mEventTypeHandle = mEventTypes(cboEventTypeName.ListIndex + 1)(DatabaseName.EVENTTYPE_KEY)
  End If
End Sub

Private Sub cboPayee_Change()
  Dirty = True
End Sub

Private Sub cboPayee_Click()
  Dirty = True
End Sub

'
' End General
'

Private Sub cmdCancel_Click()
  Cancel
End Sub

Private Sub cmdClaim_Click()
  Dispatcher.ShowSearch DatabaseName.CLAIM_TABLE, FormMgr, True
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSave_Click()
  If Save Then
    Unload Me
  End If
End Sub

Private Sub cmdSaveNew_Click()
  If Save Then mnuFileNew_Click
End Sub

Private Sub cmdVendor_Click()
' was never enabled. Code remains commented out and disabled the button for the 3.0 release
'  Dispatcher.ShowSearch DatabaseName.VENDOR_TABLE, FormMgr, True
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
  Set mDataSet = New ClaimPaymentDataset
  Set mDataSet.Session = LawSession
 
  Set GLTransaction = NewGenericDataset(DatabaseName.GLTRANSACTION_VIEW)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If


End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  Set mEventTypes = Factory.EventTypeService.SourceCollection(DatabaseName.CLAIMPAYMENT_TABLE)
  FormUtil.ComboBoxLoad mEventTypes, cboEventTypeName, DatabaseName.EVENTTYPENAME_FIELD
  
  FormUtil.WriteIniFile Name, EVENTTYPENAMELISTINDEX_PARAM, 0
  
  mEventTypeNameListIndex = FormUtil.ReadIniFile(Name, EVENTTYPENAMELISTINDEX_PARAM)
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  'Default tab open to payment
  SSTab1.Tab = 0
  
  FormRefresh
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

'Implement a complete and close menu option
Private Sub mnuFileCompleteClose_Click()
  'Call the regular workflow menu
  mnuFileWorkflow_Click FormUtil.lawActionComplete
  'Cancel and unload the form. Then exit the form.
  mnuFileExit_Click
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileOpenClaim_Click()
  FormUtil.ShowByPrimaryKey New frmClaimOpen, mClaimHandle, FormMgr
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowEventProperties mEventHandle
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mClaimPaymentHandle = vbNullString
  FormRefresh
End Sub

Private Sub mnuFileSave_Click()
  '2.70.00 (jjm) 2004 11 08
  'Fix the File Save bug with amount not saving due to the lost focus
  'code not being called (REF #186)
  If ActiveControl = txtDataField(TXT_PAYMENTAMOUNT_INDEX) Then
    txtDataField_LostFocus TXT_PAYMENTAMOUNT_INDEX
  End If
  Save
End Sub

Private Sub mnuHelpAbout_Click()
  'MsgBox ActiveControl.Name
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.CLAIMPAYMENT_TABLE, lawActionNew, _
      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub



' ver 1.90.1226 (srd)
Private Sub optPaidBy_Click(Index As Integer)
  BalanceRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
  Select Case Index
    Case 2
      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, txtDataField(Index).Text) - 1
  End Select
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtDataField_LostFocus(Index As Integer)
  Select Case Index
    Case TXT_PAYMENTAMOUNT_INDEX
      BalanceRefresh
  End Select
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
