VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmApplicationEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Application"
   ClientHeight    =   4305
   ClientLeft      =   7410
   ClientTop       =   4995
   ClientWidth     =   5250
   Icon            =   "frmApplicationEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   5250
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4236
      Left            =   0
      ScaleHeight     =   4230
      ScaleWidth      =   5250
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   0
      Width           =   5256
      Begin VB.TextBox txtDataField 
         DataField       =   "firmName"
         Height          =   288
         Index           =   3
         Left            =   120
         TabIndex        =   20
         Text            =   "firmName"
         Top             =   180
         Width           =   3420
      End
      Begin VB.CommandButton cmdFirm 
         Caption         =   "F&irm"
         Height          =   288
         Left            =   3636
         TabIndex        =   21
         Top             =   180
         Width           =   1000
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   2676
         TabIndex        =   18
         Top             =   3828
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   3960
         TabIndex        =   19
         Top             =   3828
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   3048
         Left            =   72
         TabIndex        =   22
         Top             =   648
         Width           =   5088
         _ExtentX        =   8969
         _ExtentY        =   5371
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmApplicationEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label14"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label13"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label3"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label2(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblDataField(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Label4"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtpDataField(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtpDataField(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtDataField(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtDataField(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboDataField(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtDataField(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmApplicationEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).Control(1)=   "cmdClearNotepad"
         Tab(1).ControlCount=   2
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74928
            TabIndex        =   17
            Top             =   2640
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   2244
            Left            =   -74940
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   16
            Top             =   300
            Width           =   4956
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "applicationTypeId"
            Height          =   288
            Index           =   0
            Left            =   3300
            TabIndex        =   15
            TabStop         =   0   'False
            Text            =   "applicationTypeId"
            Top             =   24
            Visible         =   0   'False
            Width           =   744
         End
         Begin VB.ComboBox cboDataField 
            DataField       =   "eventTypeId"
            DataMember      =   "Event"
            Height          =   315
            Index           =   0
            ItemData        =   "frmApplicationEdit.frx":0044
            Left            =   1596
            List            =   "frmApplicationEdit.frx":0046
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Tag             =   "ApplicationEditEventTypeName"
            Top             =   420
            Width           =   3384
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "staffCount"
            Height          =   300
            Index           =   1
            Left            =   1596
            TabIndex        =   9
            Text            =   "staffCount"
            Top             =   1752
            Width           =   1836
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "policyTerm"
            Height          =   300
            Index           =   2
            Left            =   1584
            TabIndex        =   7
            Text            =   "policyTerm"
            Top             =   1392
            Width           =   1836
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmApplicationEdit.frx":0048
            DataField       =   "receiveDate"
            Height          =   288
            Index           =   0
            Left            =   1584
            TabIndex        =   3
            Top             =   744
            Width           =   1404
            _ExtentX        =   2487
            _ExtentY        =   503
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   105840641
            CurrentDate     =   37088
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmApplicationEdit.frx":0053
            DataField       =   "effectiveDate"
            Height          =   288
            Index           =   1
            Left            =   1596
            TabIndex        =   5
            Top             =   1056
            Width           =   1404
            _ExtentX        =   2487
            _ExtentY        =   503
            _Version        =   393216
            Format          =   105840641
            CurrentDate     =   37088
         End
         Begin VB.Label Label4 
            Caption         =   "Application:"
            Height          =   216
            Left            =   120
            TabIndex        =   0
            Top             =   432
            Width           =   1464
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "applicationStatus"
            DataField       =   "applicationStatus"
            Height          =   192
            Index           =   0
            Left            =   1584
            TabIndex        =   13
            Top             =   2292
            UseMnemonic     =   0   'False
            Width           =   1236
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Effective Date:"
            Height          =   192
            Index           =   8
            Left            =   120
            TabIndex        =   4
            Top             =   1104
            Width           =   1032
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Staff:"
            Height          =   192
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   1800
            Width           =   348
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Date &Rec'd:"
            Height          =   192
            Left            =   120
            TabIndex        =   2
            Top             =   792
            Width           =   852
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "&Policy Term:"
            Height          =   192
            Left            =   120
            TabIndex        =   6
            Top             =   1452
            Width           =   900
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Application Status:"
            Height          =   192
            Left            =   120
            TabIndex        =   12
            Top             =   2292
            Width           =   1320
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   276
      TabIndex        =   14
      Top             =   780
      Width           =   1056
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Application"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator3872 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Contact (Firm)"
            Index           =   0
         End
         Begin VB.Menu mnuFileNewOther 
            Caption         =   "&Firm"
            Index           =   1
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Application"
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuS4r324 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Firm"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparatro2823 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact (Firm)"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Firm"
         Index           =   1
      End
      Begin VB.Menu mnuSeparator8371 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator86732 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&New Application Folder Location..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator365 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmApplicationEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.11.0603 MR (jjm)
'previous ver 2.10.0417 (jjm)
'previous version was ancient
Option Explicit

Private mApplicationHandle As String
Private mFirmHandle As String
Private mEventHandle As String
Private mNew As Boolean
Private mDatafields As Collection
Private mDataSet As Dataset
Private mAppTypes As Collection

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mCancel As Boolean

' Internal Id Fields
Private mFirmDataset As Dataset

Private mLeft As Integer
Private mTop As Integer

Private mParameterNames() As String
Private mParameterValues() As String

' Form Manager
Private FormMgr As New FormManager

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Child is either a Firm
' select (Firm) from cmdFirm or a new firm (NewFirm) from mnuFileNewFirm.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form requested new firm.
    Case fimNew
      mnuFileNewOther_Click 1
      Exit Sub
    
    ' Select form selected
    Case fimSelect
      mFirmHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRM_KEY)
      Prepare
      
    ' New Firm has been saved via mnuFileNewFirm.
    Case fimSave
      If fInterface.Key = DatabaseName.FIRM_TABLE & "Edit" Then
        mFirmHandle = fInterface.Child.organizationHandle
        Prepare
      End If
  End Select
  Show
  
  'test to to see if cboDatafield is enabled. When a tab is selected all elements
  'not on the tab are disabled. If you try to set focus to a disabled element the
  'vb will crash.
  If (cboDataField(0).Enabled) Then cboDataField(0).SetFocus
End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub


Private Sub FormRefresh()
  
  If (mNew) Then
    Caption = "New Application"
    
    lbldatafield(0) = ""
    txtDataField(3) = ""
    'dtpDataField(0) = Today
    ' Defaults in case prepare doesn't work
    txtDataField(1) = "0"
    txtDataField(2) = "12"
    dtpDataField(1) = Today
    
    Prepare
         
    dtpDataField(0) = Today
    mEventHandle = vbNullString

    FormUtil.GetComboDefault cboDataField(0)
    
  Else
    ' ver 2.20.0605 (jjm)
    ' Set the default date, this will be overwritten by any data if it exists
    dtpDataField(0) = Today
  
    Set mDatafields = Factory.ApplicationService.Find(mApplicationHandle)
    SetFields txtDataField, mDatafields
    SetFields dtpDataField, mDatafields
    SetFields lbldatafield, mDatafields
        
    mApplicationHandle = GetDataFromCollection(mDatafields, DatabaseName.APPLICATION_KEY, True)
    mFirmHandle = GetDataFromCollection(mDatafields, DatabaseName.FIRM_KEY, True)
    Me.Caption = "Application Edit (" & mApplicationHandle & ")"
    
    mEventHandle = GetDataFromCollection(mDatafields, DatabaseName.EVENT_KEY, True)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
        
    'Load the EventType
    Dim EventData As Collection
    Set EventData = Factory.EventService.Find(mEventHandle, DatabaseName.EVENTTYPE_KEY)
    Dim I As Integer
    If Not EventData Is Nothing Then
      If Not mAppTypes Is Nothing Then
        For I = 1 To mAppTypes.Count
          If mAppTypes(I)(DatabaseName.EVENTTYPE_KEY) = EventData(DatabaseName.EVENTTYPE_KEY) Then
            cboDataField(0).ListIndex = I - 1
            Exit For
          End If
        Next I
      End If
    Else
      cboDataField(0).ListIndex = -1
    End If
  End If
  mnuFileOpen.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  
  Dirty = False
End Sub

Public Sub Prepare()
'
'  test code
'
'Dim test As Collection
'Set test = Factory.ApplicationService.getExplicit

  If mFirmHandle = vbNullString Then Exit Sub

'  'OLD WAY
'  Dim xmlHttp As xmlHttp, Model As Collection
'  Dim xmlUrl As String
'  xmlUrl = HttpServer &  WebUtil & "applicationprepare?key=" & mFirmHandle & "&source=text/xml"
'  Set xmlHttp = XMLUtil.HttpGet(xmlUrl)
'  Set Model = XMLUtil.Rowset(xmlHttp.responseXML.documentElement, "application")
'
'  If Model Is Nothing Then
'    MsgBox "Error: Service was unabled to be called. " & xmlUrl, vbCritical
'    Exit Sub
'  End If
  
  Dim Model As Collection
  Set Model = Factory.ApplicationService.applicationPrepare(mFirmHandle)
  
  SetFields txtDataField, Model
  SetFields dtpDataField, Model
  SetFields lbldatafield, Model
  txtDataField(2).Text = "12"
  
End Sub


Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Start custom Code
  
  ' Client side duplication.
  If mNew And Factory.ContactService.FindByOrganization(mFirmHandle, "Firm") Is Nothing Then
    Select Case MsgBox("You can not save a new application without creating a firm contact.  Would you like to create a new firm contact now?", vbYesNo)
      Case vbYes
        mnuFileNewOther_Click 0
        Exit Function
      Case Else
        Exit Function
    End Select
  End If
  
  '
  ' Save Data
  '
  
  'Get the eventTypeId
  Dim mDataControls As New Collection
  Dim ApplicationTypeId As String
  Dim EventTypeId As String
  Dim I As Integer
  
  AddValueControl dtpDataField, mDataControls
  AddTextControl txtDataField, mDataControls
  
  If Not mAppTypes Is Nothing Then
    For I = 1 To mAppTypes.Count
      If mAppTypes(I)(DatabaseName.EVENTTYPENAME_FIELD) = cboDataField(0).Text Then
        EventTypeId = GetDataFromCollection(mAppTypes(I), DatabaseName.EVENTTYPE_KEY)
        Exit For
      End If
    Next I
  End If
  
  
  If (mNew) Then
    'ver 2.11.0603 (jjm)
    'This is only need to be passed on a new application
    Dim folderPath As String
    folderPath = IniMain.ReadFile("application", "applicationfolder")
      
    mApplicationHandle = Factory.ApplicationService.CreateApplication( _
              EventTypeId, _
              mFirmHandle, _
              vbNullString, _
              GetDataFromCollection(mDataControls, DatabaseName.FIRMNAME_FIELD), _
              GetDate(mDataControls(DatabaseName.EFFECTIVEDATE_FIELD)), _
              GetDouble(GetDataFromCollection(mDataControls, DatabaseName.STAFFCOUNT_FIELD)), _
              GetDouble(GetDataFromCollection(mDataControls, DatabaseName.POLICYTERM_FIELD)), _
              GetDate(mDataControls(DatabaseName.RECEIVEDATE_FIELD)), _
              txtNote.Text, folderPath)
    save = mApplicationHandle <> vbNullString
  Else
    save = Factory.ApplicationService.WriteApplication(mApplicationHandle, _
              EventTypeId, _
              GetDataFromCollection(mDataControls, DatabaseName.FIRMNAME_FIELD), _
              GetDate(GetDataFromCollection(mDataControls, DatabaseName.EFFECTIVEDATE_FIELD)), _
              GetDouble(GetDataFromCollection(mDataControls, DatabaseName.STAFFCOUNT_FIELD)), _
              GetDouble(GetDataFromCollection(mDataControls, DatabaseName.POLICYTERM_FIELD)), _
              GetDate(mDataControls(DatabaseName.RECEIVEDATE_FIELD)), _
              txtNote.Text)
  End If
  
  If save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  Dirty = Not save
End Function

Public Sub ShowNew(Optional firmHandle As String)
  mFirmHandle = firmHandle
  mNew = True
  Show
  
  Dirty = False
  
  If mFirmHandle = vbNullString Then
    cmdFirm.SetFocus
    cmdFirm_Click
  Else
    cboDataField(0).SetFocus
  End If
End Sub

Public Sub ShowByPrimaryKey(ApplicationId As String)
  mApplicationHandle = ApplicationId
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Open up a select form for firm.  The response to the select
' is processed under FormChildUnload.
'
Public Sub cmdFirm_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True, , , , "Select a firm for the application"
End Sub

Private Sub cmdSave_Click()
  Dim WasNew As Boolean
  WasNew = mNew
  If save Then
    FormMgr.Message = fimSave
    
    'Save the default for the next combo box type
    FormUtil.SetComboDefault cboDataField(0)
    
    If TypeOf FormMgr.Parent Is frmApplicationOpen Then
      Unload Me
      Exit Sub
    End If
    
    Unload Me
    
    If WasNew Then
      ' DON'T register FormMgr.
      FormUtil.ShowByPrimaryKey New frmApplicationOpen, mApplicationHandle
    End If
  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_Initialize()
  Set mDataSet = New ApplicationDataset
  Set mDataSet.Session = LawSession
   
  Set mFirmDataset = New FirmViewDataset
  Set mFirmDataset.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyF
        mnuFileNewOther_Click 1
        KeyCode = 0
    End Select
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  'Custom Accelerators
  KeyPreview = True
  mnuFileNewOther(1).Caption = mnuFileNewOther(1).Caption + vbTab + "Ctrl+Shift+F"
  
  FormUtil.EditLoad Me
  FormMgr.Load Me
 
  Set mAppTypes = Factory.EventTypeService.GroupCollection(DatabaseName.APPEDIT_RULE)
  
  FormUtil.ComboBoxLoad mAppTypes, cboDataField(0), "eventTypeName"
   
'  Factory.FolderParameterService.ParameterLoad DatabaseName.APPLICATION_TABLE, lawActionNew, mParameterNames, mParameterValues
  
  FormRefresh
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub Form_Paint()
  Form_Resize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = True
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Resize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  If mFirmHandle = vbNullString Then
    MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
    Exit Sub
  End If
  Select Case Index
    Case 0
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowByFirm mFirmHandle, "Firm"
    Case 1
      FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mFirmHandle, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not save(True) Then Exit Sub
  mNew = True
  mApplicationHandle = vbNullString
  mFirmHandle = vbNullString
  FormRefresh
  cmdFirm.SetFocus
  cmdFirm_Click
End Sub

Private Sub mnuFileNewOther_Click(Index As Integer)
  Select Case Index
    Case 0
      If mFirmHandle = vbNullString Then
        MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
        Exit Sub
      End If
      Dim Contacts As Collection
      Set Contacts = Factory.ContactService.FindByOrganization(mFirmHandle, "Firm")
      If Contacts Is Nothing Then
        Dim fContact As New frmContactEdit
        FormMgr.Add fContact
        fContact.ShowNewByOrganization mFirmHandle, GetDataFromCollection(Factory.EntityService.Find(mFirmHandle, DatabaseName.MAINLOCATIONID_FIELD), DatabaseName.MAINLOCATIONID_FIELD)
        'fContact.ShowNewByFirm mFirmHandle, Factory.FirmService.Find(mFirmHandle, """locationId""")(DatabaseName.LOCATION_KEY)
      Else
        MsgBox "Contact record already exists for this firm."
        Exit Sub
      End If
    Case 1
      FormUtil.ShowNew New frmOrganizationEdit, FormMgr, , DatabaseName.FIRM_TABLE & "Edit"
  End Select
End Sub

'
' Check for changes and open application
'
Private Sub mnuFileOpen_Click()
  FormUtil.ShowByPrimaryKey New frmApplicationOpen, mApplicationHandle, FormMgr
End Sub

Private Sub mnuFileOpenOther_Click(Index As Integer)
  Select Case Index
    Case 0
      If mFirmHandle = vbNullString Then
        MsgBox "No firm has been associated with this application.  To insert a new firm, use {File}{New}{Firm}."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.APPLICATION_TABLE, mApplicationHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.APPLICATION_REF, DatabaseName.APPLICATION_TABLE, mApplicationHandle, lbldatafield(2).Caption
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub


Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  
  Dim fFolderList As New frmFolderListDialog
  'read files from ini and load into the list view
  With fFolderList.lvFolder
    .ListItems.Add , "Application", "Application"
    .ListItems("Application").ListSubItems.Add , "Location", IniMain.ReadFile("application", "applicationfolder")
  End With
  
  ListViewModule.AutosizeWidth fFolderList.lvFolder
  fFolderList.Show vbModal, Me
  
  If fFolderList.Ok Then
    'write folders to ini
    IniMain.WriteFile "application", "applicationfolder", fFolderList.lvFolder.ListItems("Application").ListSubItems("Location")
  End If
  
  Unload fFolderList

End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub


Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
'  If Index = 0 Then
'    Select Case txtDataField(0).Text
'      Case "N": cboDataField(0).ListIndex = 0
'      Case "R": cboDataField(0).ListIndex = 1
'    End Select
'  End If
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
