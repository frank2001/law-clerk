VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InstanceService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'ver 2.30.1007
'previous ver 2.30.0904 (jjm)
Option Explicit

Private mDataSet As Dataset
Private mLawArea As Dataset
Private mLawError As Dataset

'
' Initialize service
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.INSTANCE_TABLE)
  Set mLawArea = NewGenericDataset(DatabaseName.INSTANCELAWAREA_TABLE)
  Set mLawError = NewGenericDataset(DatabaseName.INSTANCELAWERROR_TABLE)
End Sub



Public Function Find(InstanceHandle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.INSTANCE_KEY, InstanceHandle, Column)
End Function

Public Function FindByEvent(EventHandle As String, Optional Column As String) As Collection
  Set FindByEvent = mDataSet.DataProvider.Find(DatabaseName.EVENT_KEY, EventHandle, Column)
End Function


Public Function LawAreaCollection(InstanceHandle As String) As Collection
  mLawArea.DataProvider.Column = DatabaseName.LAWAREA_KEY & "," & DatabaseName.LAWAREANAME_FIELD
  mLawArea.DataProvider.Filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, InstanceHandle)
  mLawArea.DataProvider.Sort = DatabaseName.LAWAREASEQ_FIELD
  mLawArea.DataProvider.OpenRecordset
  Set LawAreaCollection = mLawArea.DataProvider.Collection
End Function



Public Function LawErrorCollection(InstanceHandle As String) As Collection
  mLawError.DataProvider.Column = DatabaseName.LAWERROR_KEY & ", " & DatabaseName.LAWERRORNAME_FIELD
  mLawError.DataProvider.Filter = LawDialect.EqualString(DatabaseName.INSTANCE_KEY, InstanceHandle)
  mLawError.DataProvider.Sort = DatabaseName.LAWERRORSEQ_FIELD
  mLawError.DataProvider.OpenRecordset
  Set LawErrorCollection = mLawError.DataProvider.Collection
End Function

'ver 2.70 (jjm) 2004 11 22 '3.0 Moved here from incident service
Public Function GetHandleByEvent(EventHandle As String) As String
  If EventHandle = vbNullString Then
    Exit Function
  End If
  
  mDataSet.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, EventHandle)
  mDataSet.DataProvider.Column = DatabaseName.INCIDENT_KEY
  mDataSet.DataProvider.OpenRecordset
  If Not mDataSet.DataProvider.Collection Is Nothing Then
    GetHandleByEvent = mDataSet.DataProvider.Collection(1)(DatabaseName.INSTANCE_KEY)
  End If
End Function

