Attribute VB_Name = "LocationReferenceUtil"
Option Explicit


'
' This is a utility  module is for the Location Reference list
'


' Do the intial setup of the list: Headers, columns, etc...
Public Sub ListInit(lvList As ListView)
  'Setup the listview. The column headers will be: Type | Name | Id
  lvList.ColumnHeaders.Add , "Type", "Type"
  lvList.ColumnHeaders.Add , "Name", "Name"
  lvList.ColumnHeaders.Add , "Id", "Id"
  lvList.ColumnHeaders("Type").Width = 860
  lvList.ColumnHeaders("Name").Width = lvList.Width - (1400 + 860)
End Sub



' Sub to load the refrence list view. The reference list view displays all
' items that share the same locationId
' These items can be either a Firm, Person, or a Contact
Public Sub ListRefresh(lvList As ListView, LocationHandle As String)
  lvList.ListItems.Clear
  ListAdd LocationHandle, lvList, DatabaseName.ENTITY_TABLE, DatabaseName.ENTITY_KEY, DatabaseName.ENTITYNAME_FIELD, DatabaseName.MAINLOCATIONID_FIELD
  'ListAdd LocationHandle, lvList, DatabaseName.PERSON_TABLE, DatabaseName.PERSON_KEY, DatabaseName.ENTITYNAME_FIELD, DatabaseName.MAINLOCATIONID_FIELD
  ListAdd LocationHandle, lvList, DatabaseName.CONTACT_TABLE, DatabaseName.CONTACT_KEY, DatabaseName.CONTACTSALUTATION_FIELD, DatabaseName.LOCATION_KEY
End Sub



'Internal sub to add items from data
Private Sub ListAdd(LocationHandle As String, lvList As ListView, Source As String, TableId As String, NameField As String, ForeignLocationKey)
  Dim RefDataset As Dataset
  Dim Refs As New Collection
  Dim I As Integer
  Dim j As Integer
    
  Set RefDataset = NewGenericDataset(Source)
  RefDataset.DataProvider.Filter = ForeignLocationKey & " = '" & LocationHandle & "'"
  RefDataset.DataProvider.OpenRecordset
  If Not RefDataset.DataProvider.Collection Is Nothing Then
    Set Refs = RefDataset.DataProvider.Collection
    For I = 1 To Refs.Count
      lvList.ListItems.Add , , Source
      j = lvList.ListItems.Count
      lvList.ListItems(j).ListSubItems.Add , "Name", Refs(I)(NameField)
      lvList.ListItems(j).ListSubItems.Add , "Id", Refs(I)(TableId)
    Next I
  End If
End Sub


Public Sub ListDblClick(lvList As ListView, FormMgr As FormManager)
  Dim handle As String
  handle = ListItemText(lvList.SelectedItem, "Id")

  Select Case lvList.SelectedItem.Text
    Case "Contact"
      FormUtil.ShowByPrimaryKey New frmContactEdit, handle, FormMgr
    Case "Entity"
      MsgBox "ToDo: Test this function"
      Dim orgTest As Collection
      Set orgTest = Factory.OrganizationService.Find(handle, DatabaseName.ORGANIZATION_KEY)
      If orgTest Is Nothing Then
        FormUtil.ShowByPrimaryKey New frmPersonOpen, handle, FormMgr
      Else
        FormUtil.ShowByPrimaryKey New frmOrganizationOpen, handle, FormMgr
      End If
  End Select

End Sub
