VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmNotepad 
   Caption         =   "Notepad"
   ClientHeight    =   3225
   ClientLeft      =   225
   ClientTop       =   825
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3225
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog dlgOpenSave 
      Left            =   3672
      Top             =   1572
      _ExtentX        =   688
      _ExtentY        =   688
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "txt"
      Filter          =   "All files (*.*)|*.*|Text files (*.txt)|*.txt"
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   3612
      Top             =   672
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNotepad.frx":0000
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNotepad.frx":0112
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNotepad.frx":0224
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNotepad.frx":0336
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNotepad.frx":0448
            Key             =   "Cut"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNotepad.frx":055A
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNotepad.frx":066C
            Key             =   "Paste"
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtNotepad 
      Height          =   1920
      Left            =   192
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   564
      Width           =   3384
   End
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   1111
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   "New"
            Object.ToolTipText     =   "New"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Open"
            Object.ToolTipText     =   "Open"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            Object.ToolTipText     =   "Save"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Print"
            Object.ToolTipText     =   "Print"
            ImageKey        =   "Print"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Cut"
            Object.ToolTipText     =   "Cut"
            ImageKey        =   "Cut"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Copy"
            Object.ToolTipText     =   "Copy"
            ImageKey        =   "Copy"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Paste"
            Object.ToolTipText     =   "Paste"
            ImageKey        =   "Paste"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   2955
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   2593
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "3/30/2009"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            TextSave        =   "10:51 PM"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Enabled         =   0   'False
         Shortcut        =   ^N
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open..."
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFileSaveAs 
         Caption         =   "Save &As..."
      End
      Begin VB.Menu mnuSeparator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePrintSetup 
         Caption         =   "Print Set&up..."
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "&Print..."
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSend 
         Caption         =   "Sen&d..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditUndo 
         Caption         =   "&Undo"
      End
      Begin VB.Menu mnuEditBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditCut 
         Caption         =   "Cu&t"
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditCopy 
         Caption         =   "&Copy"
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "&Paste"
         Shortcut        =   ^V
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "&Status Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About  LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmNotepad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mHandle As String
Private mDirty As Boolean
Private FormMgr As New FormManager



Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

Public Sub FormParentUnload()
End Sub


Public Property Let Handle(vData As String)
  mHandle = vData
End Property


Public Function save() As Boolean
  Dim Note As String
  Note = txtNotepad.Text
  
  If Not Factory.EventService.SetNote(mHandle, Note) Then
    Exit Function
  End If
      
  ' Assign to self to disable undo.
  txtNotepad.Text = vbNullString
  txtNotepad.Text = Note
  mDirty = False
  save = True
End Function


Public Sub ShowByPrimaryKey(Handle As String)
  mHandle = Handle
  Show
End Sub


Private Sub Form_Load()
  Dim DataField As Collection, Source As String
  
  Set DataField = Factory.EventService.Find(mHandle, DatabaseName.EVENTNOTE_FIELD & ", " & DatabaseName.EVENTNAME_FIELD & "," & DatabaseName.EVENTTYPERULEID_FIELD)
  txtNotepad.Text = GetDataFromCollection(DataField, DatabaseName.EVENTNOTE_FIELD)
  Caption = GetDataFromCollection(DataField, DatabaseName.EVENTTYPENAME_FIELD)
  Source = GetDataFromCollection(DataField, DatabaseName.EVENTTYPERULEID_FIELD)
  
  Caption = Caption & " - " & Source & " Notepad"
  sbStatusBar.Panels(1).Text = Source & " - " & mHandle
  
  mDirty = False
  
  FormUtil.DeserializePosition Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If mDirty Then
    Select Case MsgBox("Save changes to note?", vbYesNoCancel + vbExclamation, "Notepad")
      Case vbCancel
        Cancel = True
      Case vbYes
        If Not save Then Cancel = True
    End Select
  End If
End Sub

Private Sub Form_Resize()
  txtNotepad.Top = Max(IIf(tbToolBar.Visible, tbToolBar.Top + tbToolBar.Height, 0), 0)
  txtNotepad.Height = Max(ScaleHeight - IIf(sbStatusBar.Visible, sbStatusBar.Height, 0) - IIf(tbToolBar.Visible, tbToolBar.Height, 0), 0)
  txtNotepad.Left = 0
  txtNotepad.Width = ScaleWidth
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuHelpAbout_Click()
  MsgBox "Version " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuViewRefresh_Click()
  'ToDo: Add 'mnuViewRefresh_Click' code.
  MsgBox "Add 'mnuViewRefresh_Click' code."
End Sub

Private Sub mnuViewStatusBar_Click()
  mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
  sbStatusBar.Visible = mnuViewStatusBar.Checked
  Form_Resize
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  tbToolBar.Visible = mnuViewToolbar.Checked
  Form_Resize
End Sub

Private Sub mnuEditPaste_Click()
  txtNotepad.SelText = Clipboard.GetText
End Sub

Private Sub mnuEditCopy_Click()
  Clipboard.Clear
  Clipboard.SetText txtNotepad.SelText
End Sub

Private Sub mnuEditCut_Click()
  Clipboard.Clear
  Clipboard.SetText txtNotepad.SelText
  txtNotepad.SelText = ""
End Sub

Private Sub mnuEditUndo_Click()
  SendMessage txtNotepad.hWnd, EM_UNDO, ByVal CLng(0), ByVal CLng(0)
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileSend_Click()
  'ToDo: Add 'mnuFileSend_Click' code.
  MsgBox "Add 'mnuFileSend_Click' code."
End Sub

Private Sub mnuFilePrint_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Printer.Print txtNotepad
  Printer.EndDoc
End Sub

Private Sub mnuFilePrintSetup_Click()
  Dim PrintFlags As Long
  PrintFlags = cdlPDPrintSetup
  ShowPrinter Me, PrintFlags
End Sub

Private Sub mnuFileSaveAs_Click()
On Error GoTo Exception
  With dlgOpenSave
    .DialogTitle = "Save Note As"
    .flags = cdlOFNOverwritePrompt Or cdlOFNHideReadOnly Or cdlOFNPathMustExist
    .ShowSave
    On Error GoTo 0
    WriteTextFile txtNotepad.Text, .FileName
  End With
Exception:
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuFileOpen_Click()
On Error GoTo Exception
  With dlgOpenSave
    .DialogTitle = "Open"
    .flags = cdlOFNHideReadOnly Or cdlOFNPathMustExist Or cdlOFNFileMustExist
    .ShowSave
    On Error GoTo 0
    txtNotepad.Text = ReadTextFile(.FileName)
  End With
Exception:
End Sub


Private Sub mnuFileNew_Click()
  'ToDo: Add 'mnuFileNew_Click' code.
  MsgBox "Add 'mnuFileNew_Click' code."
End Sub


Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
  On Error Resume Next
  Select Case Button.Key
    Case "New"
      'ToDo: Add 'New' button code.
      MsgBox "Add 'New' button code."
    Case "Open"
      mnuFileOpen_Click
    Case "Save"
      mnuFileSave_Click
    Case "Print"
      mnuFilePrint_Click
    Case "Cut"
      mnuEditCut_Click
    Case "Copy"
      mnuEditCopy_Click
    Case "Paste"
      mnuEditPaste_Click
  End Select
End Sub

Private Sub txtNotepad_Change()
  mDirty = True
End Sub
