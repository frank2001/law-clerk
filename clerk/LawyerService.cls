VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LawyerService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const WSDLFile = "LawyerService"

Private Const SERVICE = "LawyerServiceService"

Private mClient As SoapClient30
Private mDataSet As Dataset
Private mLawyerStateBar As Dataset
'Private mServiceName As String



'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.LAWYER_TABLE)
  Set mLawyerStateBar = NewGenericDataset(DatabaseName.LAWYERSTATEBAR_TABLE)
    
  
  Set mClient = New SoapClient30
    
  Dim url As String
  url = HttpServer & WebComponent & WSDLFile & ".jws?wsdl"
  
  Set mClient = Factory.ServiceMSSoapInit(url, SERVICE)
  
  If mClient Is Nothing Then
    MsgBox "LawyerService Error:  Unable to connect to the server (" & HttpServer & ").  You will not be able to save changes.", vbCritical, "Error"
    Exit Sub
  End If
    
  'mClient.mssoapinit HttpServer &  WebComponent & ServiceName & ".wsdl", "LawyerService"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & WSDLFile & ".jws" & LawSession.Session

End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.LAWYER_KEY, Handle, Column)
End Function


Public Function FindLawyerStateBar(LawyerHandle As String, StateBarHandle As String) As Collection
  Dim Handles(1) As String
  Dim HandleNames(1) As String
  Handles(0) = LawyerHandle
  Handles(1) = StateBarHandle
  HandleNames(0) = DatabaseName.LAWYER_KEY
  HandleNames(1) = DatabaseName.STATEBAR_KEY
  Set FindLawyerStateBar = mLawyerStateBar.DataProvider.FindMulti(HandleNames, Handles)
End Function


'
' Interface to server
'

'
' Create a Lawyer.
'
'ver 2.00.0310 (jjm) wilmic ' added firm handle to save


Public Function CreateLawyer(PersonLocator As String, _
    DisplayName As String, PositionId As String, RetroactiveDate As Date, _
    inPracticeDuration As Double, Uninsurable As Boolean, _
    BarAdmittedDate As Date, stateBarNumber As String, _
    Salutation As String, Title As String, _
    FirstName As String, MiddleName As String, _
    LastName As String, Suffix As String, _
    contactTypeId As String, PersonEmail As String, HomeEmail As String, _
    WorkTelephone As String, MobileTelephone As String, HomeTelephone As String, _
    firmId As String, eventNote As String, _
    LocationHandle As String) As Variant
    
    
    
    
    
    
On Error GoTo ErrorHandler

  CreateLawyer = mClient.Create( _
      PersonLocator, DisplayName, PositionId, _
      RetroactiveDate, _
      inPracticeDuration, Uninsurable, _
      BarAdmittedDate, stateBarNumber, _
      Salutation, Title, _
      FirstName, MiddleName, _
      LastName, Suffix, _
      contactTypeId, PersonEmail, HomeEmail, _
      WorkTelephone, MobileTelephone, HomeTelephone, _
      firmId, eventNote, _
      LocationHandle)
      
    
  Exit Function
  
ErrorHandler:
'  MsgBox SERVICE & " Fault: " & mClient.FaultString, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient, SERVICE
End Function

'
' Write a Lawyer
'
'ver 2.00.0310 (jjm) wilmic ' added firm handle to save
Public Function WriteLawyer( _
    LawyerHandle As String, PersonLocator As String, _
    DisplayName As String, PositionId As String, RetroactiveDate As Date, _
    inPracticeDuration As Double, Uninsurable As Boolean, _
    BarAdmittedDate As Date, stateBarNumber As String, _
    Salutation As String, Title As String, _
    FirstName As String, MiddleName As String, _
    LastName As String, Suffix As String, _
    contactTypeId As String, PersonEmail As String, HomeEmail As String, _
    WorkTelephone As String, MobileTelephone As String, HomeTelephone As String, _
    firmId As String, eventNote As String, _
    LocationHandle, _
    UpgradeToLawyer As Boolean) As Boolean
    
On Error GoTo ErrorHandler
  Call mClient.Write( _
      LawyerHandle, PersonLocator, DisplayName, PositionId, _
      RetroactiveDate, _
      inPracticeDuration, Uninsurable, _
      BarAdmittedDate, stateBarNumber, _
      Salutation, Title, _
      FirstName, MiddleName, _
      LastName, Suffix, _
      contactTypeId, PersonEmail, HomeEmail, _
      WorkTelephone, MobileTelephone, HomeTelephone, _
      firmId, eventNote, _
      LocationHandle, _
      UpgradeToLawyer)

  WriteLawyer = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient, SERVICE
  'MsgBox SERVICE & " Fault: " & mClient.FaultString, vbExclamation
  WriteLawyer = False
End Function

'
' Delete an Lawyer
'
Public Function DeleteLawyer( _
    LawyerHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler

  Call mClient.Delete(LawyerHandle, ParameterNames, ParameterValues)
  DeleteLawyer = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient, SERVICE
'  MsgBox "Fault: " & mClient.FaultString, vbExclamation
  DeleteLawyer = False
End Function


