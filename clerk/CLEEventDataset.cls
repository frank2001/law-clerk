VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLEEventDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
'ver 2.00.0310 (jjm)
'CLEEvent Dataset

Option Explicit
Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private ListIndex As Integer
Private Words As String

Private Sub CLEEventSearchFilter(frm As frmGenericSearch, newFilter As Filter)
  'Event Name
  If frm.chkCLEEvent(0).Value = Checked Then
    newFilter.Add LawDialect.UpperCase("eventName") & "like '%" & UCase(frm.txtCLEEvent(0)) & "%'"
  End If
  
  'Topic
  If frm.chkCLEEvent(1).Value = Checked Then
    newFilter.Add LawDialect.UpperCase("eventTopic") & "like '%" & UCase(frm.txtCLEEvent(1)) & "%'"
  End If
  
  'Location
  If frm.chkCLEEvent(2).Value = Checked Then
    newFilter.Add LawDialect.UpperCase("eventLocation") & "like '%" & UCase(frm.txtCLEEvent(2)) & "%'"
  End If
  
  'Event Date
  If frm.chkCLEEvent(3).Value = vbChecked Then
    newFilter.Add """eventDate"" > " & LawDialect.DateFormat(frm.dtpFromEventDate.Value) & " and ""eventDate"" < " & LawDialect.DateFormat(frm.dtpToEventDate.Value)
  End If

End Sub

Private Sub Class_Initialize()
  ListIndex = 1
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.CLEEVENT_TABLE
'  mDataProvider.Sort = """cleEventId"""
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property


Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in attending staff field only"
  frm.cboIn.AddItem "in cle event identifier field only"
  frm.cboIn.AddItem "in event name field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, 1)
End Sub


Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  If Not Words = "" Then
    Words = Replace(Words, "'", "''") 'Translate to SQL
    Select Case ListIndex
      Case 0
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.ATTENDINGSTAFF_FIELD, Words)
      Case 1
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.CLEEVENT_KEY, Words)
      Case 2
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.EVENTNAME_FIELD, Words)
    End Select
  End If
  
  If TypeOf frm Is frmGenericSearch Then
    CLEEventSearchFilter frm, newFilter
  End If
  
  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function

