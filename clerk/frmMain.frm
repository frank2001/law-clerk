VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Begin VB.Form frmMain 
   Caption         =   "LAW Clerk"
   ClientHeight    =   9084
   ClientLeft      =   3240
   ClientTop       =   2496
   ClientWidth     =   13056
   Icon            =   "frmMain.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   9084
   ScaleWidth      =   13056
   Begin VB.Timer tmrFolderWatch 
      Enabled         =   0   'False
      Left            =   3360
      Top             =   6960
   End
   Begin VB.Timer tmrMenu 
      Enabled         =   0   'False
      Left            =   8088
      Top             =   6288
   End
   Begin MSComctlLib.ImageList imlTVFolders 
      Left            =   1860
      Top             =   6105
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   255
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   4740
      Top             =   5955
      _ExtentX        =   699
      _ExtentY        =   699
      _Version        =   393216
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   13056
      _ExtentX        =   23029
      _ExtentY        =   593
      BandCount       =   4
      _CBWidth        =   13056
      _CBHeight       =   336
      _Version        =   "6.7.9782"
      Child1          =   "picStart"
      MinHeight1      =   288
      Width1          =   1308
      NewRow1         =   0   'False
      Child2          =   "picNavigation"
      MinHeight2      =   288
      Width2          =   3492
      NewRow2         =   0   'False
      Child3          =   "picToolBar"
      MinWidth3       =   4008
      MinHeight3      =   264
      Width3          =   4008
      NewRow3         =   0   'False
      Child4          =   "picFolderWatch"
      MinHeight4      =   288
      Width4          =   7008
      NewRow4         =   0   'False
      Begin VB.PictureBox picFolderWatch 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   288
         Left            =   9168
         ScaleHeight     =   288
         ScaleWidth      =   3816
         TabIndex        =   41
         Top             =   24
         Width           =   3816
         Begin MSComctlLib.Toolbar tbrFolderWatch 
            Height          =   312
            Left            =   0
            TabIndex        =   42
            Top             =   0
            Width           =   6000
            _ExtentX        =   10583
            _ExtentY        =   550
            ButtonWidth     =   3344
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            ImageList       =   "imlFolderWatch"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "No Folder Selected"
                  ImageIndex      =   1
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Enabled         =   0   'False
                        Text            =   "Go To Folder"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "select"
                        Text            =   "Select Folder"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picNavigation 
         BorderStyle     =   0  'None
         Height          =   288
         Left            =   1464
         ScaleHeight     =   288
         ScaleWidth      =   3336
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   24
         Width           =   3336
         Begin MSComctlLib.Toolbar tbrForward 
            Height          =   312
            Left            =   1308
            TabIndex        =   36
            Top             =   0
            Width           =   1368
            _ExtentX        =   2413
            _ExtentY        =   550
            ButtonWidth     =   1715
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Forward"
                  Style           =   5
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrBack 
            Height          =   312
            Left            =   96
            TabIndex        =   37
            Top             =   12
            Width           =   1284
            _ExtentX        =   2265
            _ExtentY        =   550
            ButtonWidth     =   1228
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Back"
                  Style           =   5
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar tbrUpOneLevel 
            Height          =   264
            Left            =   2592
            TabIndex        =   38
            Top             =   0
            Width           =   792
            _ExtentX        =   1397
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "UpOneLevel"
                  Object.ToolTipText     =   "Up"
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picStart 
         BorderStyle     =   0  'None
         Height          =   288
         Left            =   132
         ScaleHeight     =   288
         ScaleWidth      =   1152
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   24
         Width           =   1152
         Begin MSComctlLib.Toolbar tbrStart 
            Height          =   312
            Left            =   0
            TabIndex        =   29
            Top             =   36
            Width           =   1128
            _ExtentX        =   1990
            _ExtentY        =   550
            ButtonWidth     =   1228
            ButtonHeight    =   550
            Style           =   1
            TextAlignment   =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Start"
                  Object.ToolTipText     =   "Click here to begin"
                  ImageIndex      =   10
               EndProperty
            EndProperty
         End
      End
      Begin VB.PictureBox picToolBar 
         BorderStyle     =   0  'None
         Height          =   264
         Left            =   4980
         ScaleHeight     =   264
         ScaleWidth      =   4008
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   36
         Width           =   4008
         Begin MSComctlLib.Toolbar tbToolBar 
            Height          =   264
            Left            =   0
            TabIndex        =   30
            Top             =   0
            Width           =   8676
            _ExtentX        =   15304
            _ExtentY        =   466
            ButtonWidth     =   487
            ButtonHeight    =   466
            Wrappable       =   0   'False
            Style           =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   13
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "New"
                  Object.ToolTipText     =   "New"
                  ImageKey        =   "New"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Open"
                  Object.ToolTipText     =   "Open"
                  ImageKey        =   "Open"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Print"
                  Object.ToolTipText     =   "Print"
                  ImageKey        =   "Print"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Move"
                  Object.ToolTipText     =   "Move to Folder"
                  ImageKey        =   "Copy"
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Delete"
                  Object.ToolTipText     =   "Delete"
                  ImageKey        =   "Delete"
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode0"
                  Object.ToolTipText     =   "View Large Icons"
                  ImageKey        =   "View Large Icons"
                  Style           =   2
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode1"
                  Object.ToolTipText     =   "View Small Icons"
                  ImageKey        =   "View Small Icons"
                  Style           =   2
               EndProperty
               BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode2"
                  Object.ToolTipText     =   "View List"
                  ImageKey        =   "View List"
                  Style           =   2
               EndProperty
               BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "lvMode3"
                  Object.ToolTipText     =   "View Details"
                  ImageKey        =   "View Details"
                  Style           =   2
               EndProperty
               BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdDefault 
      Caption         =   "Default"
      Height          =   336
      Left            =   1800
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   5580
      Width           =   1560
   End
   Begin VB.CommandButton cmdMenu 
      Caption         =   "Alt &M"
      CausesValidation=   0   'False
      Height          =   336
      Left            =   120
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   5580
      Width           =   1500
   End
   Begin VB.PictureBox picSplitter 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      FillColor       =   &H00808080&
      Height          =   4800
      Left            =   10872
      ScaleHeight     =   2084.849
      ScaleMode       =   0  'User
      ScaleWidth      =   312
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   156
      Visible         =   0   'False
      Width           =   48
   End
   Begin VB.PictureBox picListView 
      BackColor       =   &H80000005&
      Enabled         =   0   'False
      HasDC           =   0   'False
      Height          =   3090
      Left            =   12
      ScaleHeight     =   3048
      ScaleWidth      =   9456
      TabIndex        =   16
      TabStop         =   0   'False
      Tag             =   "Firm"
      Top             =   2280
      Visible         =   0   'False
      Width           =   9504
      Begin VB.PictureBox picSearchStatus 
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         Height          =   240
         Left            =   1920
         ScaleHeight     =   240
         ScaleWidth      =   4992
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   420
         Visible         =   0   'False
         Width           =   4992
         Begin VB.Label lblSearchStatus 
            AutoSize        =   -1  'True
            BackColor       =   &H80000009&
            BackStyle       =   0  'Transparent
            Caption         =   "Search Status"
            Height          =   192
            Left            =   0
            TabIndex        =   21
            Top             =   0
            Width           =   996
         End
      End
      Begin MSComctlLib.ListView lvView 
         Height          =   4152
         Left            =   1416
         TabIndex        =   17
         Top             =   0
         Width           =   5856
         _ExtentX        =   10351
         _ExtentY        =   7324
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         _Version        =   393217
         Icons           =   "imlIcons"
         SmallIcons      =   "imlSmallIcons"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.Label lblListView 
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "This text should be replaced with the folders ""note"" information from PreferenceEntry."
         Height          =   1575
         Index           =   2
         Left            =   90
         TabIndex        =   40
         Top             =   2415
         Width           =   1600
      End
      Begin VB.Label lblListView 
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Double click or press Enter on an item to view or edit the information. "
         Height          =   855
         Index           =   1
         Left            =   90
         TabIndex        =   39
         Top             =   720
         Width           =   1605
      End
      Begin VB.Label lblListView 
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Table Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   90
         TabIndex        =   18
         Top             =   0
         Width           =   1605
         WordWrap        =   -1  'True
      End
   End
   Begin VB.PictureBox picView 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2004
      Left            =   0
      ScaleHeight     =   2004
      ScaleWidth      =   13056
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   336
      Width           =   13050
      Begin VB.PictureBox picMain 
         BackColor       =   &H80000005&
         Height          =   4560
         Left            =   3144
         ScaleHeight     =   4512
         ScaleWidth      =   7524
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "WILMIC"
         Top             =   1140
         Visible         =   0   'False
         Width           =   7572
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Press Alt-M for the Start Menu or click the start button in the upper left hand corner."
            Height          =   384
            Index           =   3
            Left            =   504
            TabIndex        =   22
            Top             =   1260
            Width           =   4428
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblWelcolme 
            BackColor       =   &H80000005&
            Caption         =   "Welcome to the WILMIC implementation of the LAW System."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   852
            Left            =   216
            TabIndex        =   15
            Top             =   276
            Width           =   2892
         End
         Begin VB.Label lblVersion 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Version: 1.79.0719"
            Height          =   192
            Left            =   5316
            TabIndex        =   14
            Top             =   276
            Width           =   1284
         End
      End
      Begin VB.Frame fraSearch 
         Enabled         =   0   'False
         Height          =   1260
         Left            =   2172
         TabIndex        =   10
         Top             =   0
         Visible         =   0   'False
         Width           =   7812
         Begin VB.PictureBox picWords 
            BackColor       =   &H80000005&
            Height          =   900
            Left            =   0
            ScaleHeight     =   852
            ScaleWidth      =   7524
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   360
            Width           =   7572
            Begin VB.Frame fraLookFor 
               BackColor       =   &H80000005&
               BorderStyle     =   0  'None
               Height          =   684
               Left            =   0
               TabIndex        =   32
               Top             =   0
               Visible         =   0   'False
               Width           =   4116
               Begin VB.ComboBox txtWords 
                  Height          =   288
                  Left            =   1500
                  TabIndex        =   1
                  Text            =   "Combo1"
                  Top             =   0
                  Width           =   2604
               End
               Begin VB.ComboBox cboIn 
                  Height          =   288
                  ItemData        =   "frmMain.frx":09CA
                  Left            =   1500
                  List            =   "frmMain.frx":09CC
                  Style           =   2  'Dropdown List
                  TabIndex        =   2
                  Tag             =   "Firm"
                  Top             =   348
                  Width           =   2604
               End
               Begin VB.Label Label1 
                  Alignment       =   1  'Right Justify
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "Search f&or:"
                  Height          =   192
                  Left            =   120
                  TabIndex        =   33
                  Top             =   108
                  Width           =   768
               End
            End
            Begin VB.CommandButton cmdSearchNow 
               Caption         =   "Search Now"
               Height          =   288
               Left            =   4400
               TabIndex        =   3
               Top             =   144
               Width           =   1080
            End
            Begin VB.CommandButton cmdAdvancedSearch 
               Caption         =   "&Advanced"
               Height          =   288
               Index           =   0
               Left            =   4400
               TabIndex        =   4
               Top             =   492
               Width           =   1092
            End
         End
         Begin VB.Label lblLabel 
            Caption         =   "Search"
            Height          =   252
            Index           =   5
            Left            =   120
            TabIndex        =   12
            Top             =   120
            Width           =   1692
         End
      End
      Begin VB.Frame fraFolders 
         Height          =   1836
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   2340
         Begin VB.PictureBox picRemoveFolders 
            BorderStyle     =   0  'None
            Height          =   192
            Left            =   1968
            Picture         =   "frmMain.frx":09CE
            ScaleHeight     =   192
            ScaleWidth      =   192
            TabIndex        =   23
            Top             =   144
            Visible         =   0   'False
            Width           =   192
         End
         Begin VB.PictureBox picFolders 
            Height          =   1284
            Left            =   0
            ScaleHeight     =   1236
            ScaleWidth      =   2304
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   360
            Width           =   2352
            Begin MSComctlLib.TreeView tvFolders 
               Height          =   1068
               Left            =   0
               TabIndex        =   0
               Top             =   0
               Width           =   1836
               _ExtentX        =   3239
               _ExtentY        =   1884
               _Version        =   393217
               HideSelection   =   0   'False
               Indentation     =   127
               LabelEdit       =   1
               LineStyle       =   1
               Style           =   7
               Appearance      =   0
            End
         End
         Begin VB.Label lblLabel 
            Caption         =   "Folders"
            Height          =   192
            Index           =   0
            Left            =   120
            TabIndex        =   9
            Top             =   120
            Width           =   744
         End
      End
      Begin VB.Image imgSplitter 
         Height          =   5748
         Left            =   2076
         MousePointer    =   9  'Size W E
         Top             =   108
         Width           =   48
      End
   End
   Begin VB.Timer tmrSearch 
      Enabled         =   0   'False
      Interval        =   750
      Left            =   8085
      Top             =   5745
   End
   Begin VB.Timer tmrFolderList 
      Enabled         =   0   'False
      Interval        =   750
      Left            =   8040
      Top             =   5310
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   276
      Left            =   0
      TabIndex        =   5
      Top             =   8808
      Width           =   13056
      _ExtentX        =   23029
      _ExtentY        =   466
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15409
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "3/13/2013"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1757
            MinWidth        =   1764
            TextSave        =   "1:37 PM"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox dlgCommonDialog 
      Height          =   384
      Left            =   9660
      ScaleHeight     =   336
      ScaleWidth      =   912
      TabIndex        =   34
      Top             =   8472
      Width           =   960
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   7488
      Top             =   5292
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0C4C
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0D5E
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0E70
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0F82
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1094
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":11A6
            Key             =   "View Large Icons"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":12B8
            Key             =   "View Small Icons"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":13CA
            Key             =   "View List"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":14DC
            Key             =   "View Details"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":15EE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlIcons 
      Left            =   1155
      Top             =   6030
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":194A
            Key             =   "book"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlSmallIcons 
      Left            =   5910
      Top             =   6810
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   13
      ImageHeight     =   13
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1C64
            Key             =   "closed"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1DD6
            Key             =   "cylinder"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1F48
            Key             =   "leaf"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":20BA
            Key             =   "open"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":222C
            Key             =   "smlBook"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":24DE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlArrows 
      Left            =   6600
      Top             =   6000
      _ExtentX        =   826
      _ExtentY        =   826
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2790
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlArrowsDisabled 
      Left            =   6000
      Top             =   6000
      _ExtentX        =   826
      _ExtentY        =   826
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlFolderWatch 
      Left            =   2640
      Top             =   6840
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   255
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2AE4
            Key             =   "Normal"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":30C6
            Key             =   "Alert"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblStart 
      AutoSize        =   -1  'True
      BackColor       =   &H80000018&
      Caption         =   "Click here to begin..."
      Height          =   195
      Left            =   4080
      TabIndex        =   31
      Top             =   5490
      Visible         =   0   'False
      Width           =   1425
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNewFolder 
            Caption         =   "Folder"
         End
         Begin VB.Menu mnuFileNewItem 
            Caption         =   "mnuFileNewItem"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator9832 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "mnuFileNew"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "mnuFileOpen"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileExploreMenu 
         Caption         =   "Exp&lore"
         Begin VB.Menu mnuFileExplore 
            Caption         =   "mnuFileExplore"
            Index           =   0
         End
      End
      Begin VB.Menu mnuSeparator8347 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "&Print..."
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFilePrintSetup 
         Caption         =   "Print Set&up..."
      End
      Begin VB.Menu mnuSeparator8723 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuFileRename 
         Caption         =   "Rena&me"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d to"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuFileBar4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileImport 
         Caption         =   "&Import..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuFileExport 
         Caption         =   "Expor&t..."
      End
      Begin VB.Menu mnuFileBar6 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileLogOff 
         Caption         =   "&Log Off..."
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditCut 
         Caption         =   "Cu&t"
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "&Paste"
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator3343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditRotate 
         Caption         =   "Rotate"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSeparator2343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditSelectAll 
         Caption         =   "Select &All"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuEditInvertSelection 
         Caption         =   "&Invert Selection"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewFolders 
         Caption         =   "&Folders"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "Status &Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewGoToMenu 
         Caption         =   "G&o To"
         Begin VB.Menu mnuViewGoToBack 
            Caption         =   "&Back"
         End
         Begin VB.Menu mnuViewGoToForward 
            Caption         =   "&Forward"
         End
         Begin VB.Menu mnuViewGoToUpOneLevel 
            Caption         =   "&Up One Level"
         End
         Begin VB.Menu mnuSeparator34211 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewGoToHome 
            Caption         =   "&Home Folder"
         End
         Begin VB.Menu mnuSeparator82112 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewGoToStartMenu 
            Caption         =   "Start &Menu"
         End
         Begin VB.Menu mnuGoToOpen 
            Caption         =   "&Open Menu"
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuGoToExplore 
            Caption         =   "Exp&lore Menu"
            Shortcut        =   ^L
         End
         Begin VB.Menu mnuSeparator3843 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewGoToFolder 
            Caption         =   "Choose &Folder..."
            Shortcut        =   {F7}
         End
      End
      Begin VB.Menu mnuViewBar33 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "Lar&ge Icons"
         Index           =   0
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "S&mall Icons"
         Index           =   1
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&List"
         Index           =   2
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&Details"
         Index           =   3
      End
      Begin VB.Menu mnuViewBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewArrangeIcons 
         Caption         =   "Arrange &Icons"
         Begin VB.Menu mnuArrangeIcon 
            Caption         =   "mnuArrangeIcon"
            Index           =   0
         End
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuViewBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeparator3467 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewWebBrowser 
         Caption         =   "&Browse"
      End
      Begin VB.Menu mnuSeparator8343 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsEnvelope 
         Caption         =   "&Envelope..."
      End
      Begin VB.Menu mnuToolsMerge 
         Caption         =   "&Merge..."
      End
      Begin VB.Menu mnuToolsWebMerge 
         Caption         =   "&Web Merge..."
      End
      Begin VB.Menu mnuToolsReport 
         Caption         =   "&Report..."
      End
      Begin VB.Menu mnuSeparator3833 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsServiceMenu 
         Caption         =   "&Service"
         Begin VB.Menu mnuToolsService 
            Caption         =   "&Claim Warehouse (Manual Monthly)"
            Index           =   1
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "&Deductible Due Update... (Manual as Required)"
            Index           =   2
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "&Firm Insure Status... (Auto Daily)"
            Index           =   3
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "Firm Last &Insurer...  (Auto Daily)"
            Index           =   4
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "&Lawyer In Practice Duration... (Auto Daily)"
            Index           =   5
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "Lawyer Insure &Status... (Auto Daily)"
            Index           =   6
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "Loss &Triangle (Manual as Required)"
            Index           =   7
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "Monthly &EFT Accounting... (Manual Monthly)"
            Index           =   8
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "&Quarterly Payment Plan Cycle... (Manual Daily)"
            Index           =   9
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "Initialize &Underwriting Crosstab (Auto Daily)"
            Index           =   10
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "CL&E Lawyer (Auto Daily)"
            Index           =   11
         End
         Begin VB.Menu mnuToolsService 
            Caption         =   "&Retrieve Online Applications (Manual)"
            Index           =   12
         End
         Begin VB.Menu mnuSeparator98374 
            Caption         =   "-"
         End
         Begin VB.Menu mnuToolsServiceManager 
            Caption         =   "&Manager"
         End
      End
      Begin VB.Menu mnuSeparator34157 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsEFT 
         Caption         =   "E&FT"
         Begin VB.Menu mnuToolsEFTExportEFTTransmissionFile 
            Caption         =   "Expor&t EFT Transmission File"
         End
      End
      Begin VB.Menu mnuSeperator8088 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsOrganizeFolders 
         Caption         =   "Or&ganize Folders..."
      End
      Begin VB.Menu mnuToolsWorkflowOptionsMenu 
         Caption         =   "Wor&kflow"
         Enabled         =   0   'False
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&Complete..."
            Index           =   0
         End
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&Void..."
            Index           =   1
         End
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&Reopen"
            Enabled         =   0   'False
            Index           =   2
         End
         Begin VB.Menu mnuToolsWorkflowOptions 
            Caption         =   "&New"
            Index           =   3
         End
      End
      Begin VB.Menu mnuSeparator3432 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsOptions 
         Caption         =   "&Options..."
      End
      Begin VB.Menu mnuToolsPreferences 
         Caption         =   "&Preferences..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "&Search For Help On..."
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ver 2.60.00 (jjm) 2004 03 30
' previous ver 2.60.00 (jjm) 2004 03 09
' previous ver 2.50.00 (jjm) 2004 02 19
' previous ver 2.20.0603 (jjm)
' previous ver 2.10.0507 (jjm)
' previous ver 2.10.0420 (jjm) core
' previous ver 2.00.0317 (jjm) Core
'
' Main Form
'
'
' The main form can be one of 3 form types set by the the following
' global constants from MainModule:
'
' ftView - Folder list and main window
' ftAdvancedSearch - Look in list, advanced search and a list view
' ftSelect - Advanced search and list view pinned to a folder
'
 
Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

' Form Arrangement for ftView
Const faMain = 1
Const faListView = 2
  
' Classes for accessing data
Private mView As View

Public Views As ViewItems
  
'local variable(s) to hold property value(s)
Private mFolderKey As String
Private mArrangement As Integer

Private lastSearch As String
Public TxtWordsIndex As Integer
Private lastCount As Integer

Private mbMoving As Boolean
Const sglSplitLimit = 1000

Public FormMgr As New FormManager
Private Popup As frmPopup

' Accessed from Popup for event paste.
Public Popup2 As frmPopup2

Private GotoMenu As frmGoTo

Private mShowPopup As Boolean
Private mUnloaded As Boolean

Private mWordLimit As Integer
Private mWordCache As New Collection

Private mSequence As Sequence


Private mFolderWatchHandle As String 'Folder Watch Utility
Private mFolderName As String 'Folder Watch Utilitycache the folder name


'used for spacing the webcontent
Private Const WEB_SPACER = 480
Private Const WEB_LABEL_NAME = 0
Private Const WEB_LABEL_GENERAL = 1
Private Const WEB_LABEL_NOTE = 2

Private Sub AddViewByKey(Key As String)
  'Load a view
  ' Get map values
  
  'Local variables to store data
  Dim Source As String
  Dim SourceDataset As Dataset, filter As New filter, IsWorkflow As Boolean
  Dim DataType As String
  Dim IsSearchOnly As Boolean
  Dim PopupMenuName As String, PopupDefault As String, OpenMenu As String
  Dim PopupExplore As String, PopupNew As String
  Dim PreferenceNote As String
    
  'If not from ini load from system folders xml
  Dim map As MSXML2.IXMLDOMNode
  'Gather info From server
  Set map = PreferenceUtil.GetNodeMap(SystemFolders_TreeName, Key)
  
  'Store Data
  Source = PreferenceUtil.getMapEntryValue(map, "source")
  
  'If the source is null then there is no need to add a view.
  If Source = vbNullString Then
    Exit Sub
  End If
  
  'gather rest of data
  DataType = PreferenceUtil.getMapEntryValue(map, "datatype")
  filter.Add PreferenceUtil.getMapEntryValue(map, "filter")
  IsSearchOnly = (PreferenceUtil.getMapEntryValue(map, "search") = "1")
  PopupMenuName = PreferenceUtil.getMapEntryValue(map, "popup")
  PopupDefault = PreferenceUtil.getMapEntryValue(map, "popup.default")
  OpenMenu = PreferenceUtil.getMapEntryValue(map, "popup.open")
  PopupExplore = PreferenceUtil.getMapEntryValue(map, "popup.explore")
  PopupNew = PreferenceUtil.getMapEntryValue(map, "new")
  PreferenceNote = PreferenceUtil.getMapEntryValue(map, "note")

      
  'Process Info
  Set SourceDataset = NewDataset(DataType, Source)
      
  'Sets the filter so that the only events with the matching
  'folderId are displayed on that workflow folder/view
  IsWorkflow = (DataType = DatabaseName.EVENT_TABLE)
  SourceDataset.DataProvider.BaseFilter = filter.filter
    
  'Add the item
  If PopupMenuName = vbNullString Then
    Views.Add Key, SourceDataset, IsWorkflow, IsSearchOnly
    Exit Sub
  End If
  
  Views.Add Key, SourceDataset, IsWorkflow, IsSearchOnly, GetForm(PopupMenuName), _
            GetControlName(PopupMenuName), PopupDefault, OpenMenu, PopupExplore, PopupNew, PreferenceNote
              
End Sub
' Just return the form type
Public Property Get Arrangement() As Integer
  Arrangement = mArrangement
End Property

' Only ftView has different arrangements.
Public Property Let Arrangement(ByVal vData As Integer)
  If mArrangement = vData Then
    Exit Property
  End If
  mArrangement = vData
    
  '  If we are not on a View form type, then there can be no arrangement change
  Select Case mArrangement
    Case faMain
      picMain.Enabled = True
      picMain.Visible = True
      fraSearch.Visible = False
      fraSearch.Enabled = False
      picListView.Visible = False
      picListView.Enabled = False
      lvView.ListItems.Clear
      lvView.ColumnHeaders.Clear
    
    Case faListView
      fraSearch.Visible = True
      fraSearch.Enabled = True
      picListView.Visible = True
      picListView.Enabled = True
      picMain.Visible = False
      cmdSearchNow.Default = True
      'cmdSearchNow, cmdAdvancedSearch - left value was originall set to 4200px
   End Select
  
  picView_Resize
   
End Property

' Folder configures for the current folder
Public Property Get FolderKey() As String
  FolderKey = mFolderKey
End Property


' Folder configures for the current folder
Public Property Let FolderKey(Key As String)
  ' Turn off any timer.
  tmrFolderList.Enabled = False
  
  'Exit if the folder is already set
  If mFolderKey = Key Then Exit Property
    
  'See if the View is loaded into views, if not load it.
  If Not Views.ContainsKey(Key) Then
    AddViewByKey Key
  End If
  
  mFolderKey = Key
  Set mView = Views(Key)
  
  ' check that folder is selected.
  If Not Contains(tvFolders.Nodes, Key) Then
    MsgBox "Folder " & Key & " no longer exists."
    If Not Key = "ROOT" Then
      FolderKey = "ROOT"
    End If
    Exit Property
  End If
  
  ' Hook in sequence
  If mSequence.Selected <> mFolderKey Then
    mSequence.AddItem FolderKey
    SequenceRefresh
  End If
  
  ' Disable Up One level when root is selected
  mnuViewGoToUpOneLevel.Enabled = Not (tvFolders.Nodes(Key).Parent Is Nothing)
  tbrUpOneLevel.Buttons(1).Enabled = mnuViewGoToUpOneLevel.Enabled
  
  
  tvFolders.Nodes(Key).Selected = True
  
  
  
  Caption = tvFolders.SelectedItem.Text & " (" & HttpServer & " v" & MalcolmVersion & ") - " & "Clerk " & LAWVersion
  
  ' Reset folder dependent menus
  sbStatusBar.Panels(2).Text = ""
  
  If mView Is Nothing Then
    MenuRefresh False
    Arrangement = faMain
    Exit Property
  End If
  
  Arrangement = faListView
  
  lblListView(WEB_LABEL_NAME).Caption = tvFolders.SelectedItem.Text
  sbStatusBar.Panels(2).Text = mView.Dataset.DataProvider.Source
  
  lblListView(WEB_LABEL_NOTE).Caption = mView.PreferenceNote
  
    
  ' Setup listview.
  lvView.Tag = Key
  lvView.Visible = False
  lvView.ColumnHeaders.Clear
  lvView.ListItems.Clear
  ListViewModule.DeserializeColumnHeaders lvView, Key
    
  ' Set toolbar
  tbToolBar.Buttons("lvMode" & lvView.View).Value = tbrPressed
         
  mView.Dataset.LoadSearch Me
  LoadView
    
  lvView.Visible = True

  lvView.Refresh

End Property

'
' Load folder.
'
Public Sub FolderLoad(FolderHandle As String, Optional DefaultMenuName, Optional ListIndex, Optional SearchText)

On Error GoTo Exception

  If Not IsMissing(DefaultMenuName) Then
    If Not Views.ContainsKey(FolderHandle) Then
      AddViewByKey FolderHandle
    End If
    Views(FolderHandle).DefaultMenuName = DefaultMenuName
  End If
  FolderKey = FolderHandle
  If Not IsMissing(ListIndex) Then cboIn.ListIndex = ListIndex
  If txtWords.Visible Then
    If Not IsMissing(SearchText) Then
      txtWords = SearchText
      SearchNow
      If lvView.ListItems.Count = 0 Then
        txtWords.SetFocus
      Else
        lvView.SetFocus
      End If
    Else
      txtWords.SetFocus
    End If
  ElseIf lvView.Visible Then
    lvView.SetFocus
  End If
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplayMessage "Unable to load folder. Error# " & Err.Number, Err.Description, "Folder Load Error", True
  Err.Clear
End Sub

'
' If a child is unloaded, find last form of same name.  Load that first, then
' load the main form.
'
Public Sub FormChildUnload(ByRef vData As FormInterface)
  Dim LastForm As Form
  Set LastForm = FindLastForm(vData.Child)
  If LastForm Is Nothing Then
    Show
    Refresh
  Else
    LastForm.Show
  End If
End Sub

'
' An interface is set when using frmMain as a select
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Do nothing if a parent form is unloaded
'
Public Sub FormParentUnload()
End Sub

'
' Handles calls for inserting new records.
'
Public Sub FileNew(Source As String)
  ' Get handles if we are on a view and the list view has a selected item.
  Dim firmHandle As String, ApplicationHandle As String
  Dim claimHandle As String, EventHandle As String, LawyerHandle As String, PolicyHandle As String
  Dim CLEEventHandle As String
  If Not mView Is Nothing And ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    ApplicationHandle = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
    claimHandle = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.CLAIM_KEY)
    EventHandle = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.EVENT_KEY)
    firmHandle = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.FIRM_KEY)
    LawyerHandle = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.LAWYER_KEY)
    PolicyHandle = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.POLICY_KEY)
  End If
  
  'Test for special case new items
  Select Case Source
    Case DatabaseName.APPLICATION_TABLE
      Dispatcher.ShowNew Source, firmHandle, FormMgr
    Case DatabaseName.APPLICATIONLAWYER_TABLE
      If ApplicationHandle = vbNullString Then
        FolderLoad DatabaseName.APPLICATION_TABLE, MenuName.APPLICATION_NEW_LAWYER
      Else
        Dispatcher.ShowNew Source, ApplicationHandle, FormMgr
      End If
    Case DatabaseName.CLAIMRESERVE_TABLE
      If claimHandle = vbNullString Then
        MsgBox "Must be on a claim to do a new claim reserve"
      Else
        Browse HttpServer & WebService & "claimreservenew?key=&key=" + claimHandle + "&action=create"
      End If
    Case DatabaseName.DEDUCTIBLEREIMBURSE_TABLE
      Dispatcher.ShowNew Source, claimHandle, FormMgr
    Case "EndorsementApplication"
      Dim fEAEdit As New frmEndorsementApplicationEdit
      FormMgr.Add fEAEdit
      fEAEdit.ShowNew PolicyHandle, firmHandle
    Case DatabaseName.FIRMINSURER_TABLE
      If firmHandle = vbNullString Then
        FolderLoad DatabaseName.FIRM_TABLE, MenuName.FIRM_NEW_INSURER
      Else
        Dispatcher.ShowNew Source, firmHandle, FormMgr
      End If
    Case DatabaseName.LAWYER_TABLE
      Dispatcher.ShowNew Source, LawyerHandle, FormMgr
    
    Case DatabaseName.POLICYACCOUNTING_TABLE
      Dim fPolicyAcctg As New frmPolicyAccountingEdit
      FormMgr.Add fPolicyAcctg
      fPolicyAcctg.ShowNew PolicyHandle, "PolPay-MC"
    
    Case "FirmDocument"
      Dim fLetter As New frmEventDocumentEdit
      FormMgr.Add fLetter
      fLetter.ShowNew DatabaseName.FIRM_DOCUMENT, EventHandle, , , firmHandle, , IIf(mView.NewItemName = "FirmDocument", mFolderKey, vbNullString)
    
    'Call Standard ShowNew
    Case Else
      Dispatcher.ShowNew Source, , FormMgr
  End Select
  


End Sub

' Helper sub of FileNewMenuLoad - Startup Sub
Private Sub FileNewLoad(Caption As String, Index As Integer, Optional Shortcut As String, Optional Enabled As Boolean)
  Load mnuFileNew(Index)
  mnuFileNew(Index).Caption = Caption & IIf(Shortcut = vbNullString, "", vbTab & Shortcut)
  mnuFileNew(Index).Enabled = Enabled
  mnuFileNew(Index).Tag = Replace(Caption, " ", "")
End Sub

'Sub to Load the File New menu on startup
Private Sub FileNewMenuLoad()
  FileNewLoad "Application", 100, "Ctrl+Shift+A", True
  'FileNewLoad "Application Law Area", 110, , True
  FileNewLoad "Application Lawyer", 120, , True
  'FileNewLoad "Application Limit", 130, , True
  'FileNewLoad "Application Question", 140, , True
  'FileNewLoad "Application Type", 160, , True
  FileNewLoad "Claim", 170, , True
  FileNewLoad "Claim Payment", 190, , True
  FileNewLoad "Claim Reserve", 200, , True
  FileNewLoad "CLE Event", 225, , True
  FileNewLoad "Contact", 235, , True
  FileNewLoad "Contact Type", 240, , True
  FileNewLoad "Deductible Reimburse", 250, , True
  FileNewLoad "EFT Deposit Financial Inst", 253, , True
  FileNewLoad "EFT Individual", 255, , True
  FileNewLoad "Endorsement Application", 260, , True
  FileNewLoad "Endorsement Type", 270, , True
  FileNewLoad "Event Type", 280, , True
  FileNewLoad "Firm", 290, "Ctrl+Shift+F", True
  FileNewLoad "Firm Insurer", 300, , True
  FileNewLoad "Firm Lawyer", 310, , True
  FileNewLoad "Fiscal Calendar", 320, , True
  FileNewLoad "GL Account", 330, , True
  FileNewLoad "GL Account Group", 335, , True
  FileNewLoad "GL Batch", 340, , True
  FileNewLoad "Incident", 350, , True
  FileNewLoad "Insurer", 360, , True
  FileNewLoad "Law Area", 380, , True
  FileNewLoad "Law Disposition", 390, , True
  FileNewLoad "Law Error", 400, , True
  FileNewLoad "Lawyer", 410, "Ctrl+Shift+L", True
  FileNewLoad "Location", 420, "Ctrl+Shift+O", True
  FileNewLoad "Location Type", 430, , True
  FileNewLoad "Person", 440, , True
  FileNewLoad "Policy Accounting", 460, , True
  FileNewLoad "Question", 470, , True
  FileNewLoad "Rating Book", 490, , True
  FileNewLoad "Rating Book Metric", 500, , True
  FileNewLoad "Rating Book Metric Calc", 510, , True
  FileNewLoad "Rating Book Rate", 520, , True
  FileNewLoad "Rating Metric", 540, , True
  FileNewLoad "Rating Rate", 550, , True
  FileNewLoad "Treaty Layer", 600, , True
  mnuFileNew(0).Visible = False
End Sub

Private Sub FrameRefresh()
  fraFolders.Width = picSplitter.Left - fraFolders.Left - picView.Left
  picFolders.Width = fraFolders.Width
  picView_Resize
  picSplitter.Visible = False
  mbMoving = False
End Sub

Private Function GetControlName(Name As String) As String
  GetControlName = Mid(Name, InStr(Name, ".") + 1)
End Function


Private Function GetForm(Name As String) As Form
  If Left(Name, 6) = "Popup." Then
    Set GetForm = Popup
  Else
    If Left(Name, 7) = "Popup2." Then
      Set GetForm = Popup2
    End If
  End If
End Function


'
' Update ListView after a search.
'
Public Sub ListViewResult()
  Dim IsCount As Boolean
  IsCount = (lvView.ListItems.Count > 0)
  picSearchStatus.Visible = Not IsCount
  
  ' Can not just check picSerachStatus.Visible since it may not
  ' be visible yet, even though it was set.
  If IsCount Then
    ' Undo any selected item
    lvView.SelectedItem.Selected = False
  Else
    If fraSearch.Visible Then
      lblSearchStatus.Caption = "Search is complete.  There are no results to display."
    Else
      lblSearchStatus.Caption = "There is nothing to display."
    End If
  End If
     
  'Should not be needed here - LOAD VIEW will already take care of this
  'MenuViewRefresh
End Sub


Public Sub LoadView()
  MenuArrangeIconLoad
  
  Dim SearchOnly As Boolean
  SearchOnly = mView.mIsSearchOnly
  
  fraSearch.Visible = SearchOnly
  mnuEditRotate.Enabled = SearchOnly
  
  If SearchOnly Then
    lastSearch = ""
    If mView.Dataset.DataProvider.isOpen Then
      ' Dataset is already open
      picSearchStatus.Visible = False
      mView.Dataset.DataProvider.SetListView lvView
      ListViewResult
    Else
      ' Setup list view for searching.
      lvView.ListItems.Clear
      picListView_Resize
      lblSearchStatus.Caption = "Enter your search criteria to begin."
      picSearchStatus.Visible = True
    End If
  Else
    picSearchStatus.Visible = False
    If mView.Dataset.DataProvider.isOpen Then
      'Ref 169 - Refresh the dataprovider
      mView.Dataset.DataProvider.OpenRecordset
      mView.Dataset.DataProvider.SetListView lvView
    Else
      Dim pointer As New WaitPointer
      pointer.pointer = vbHourglass
      'Ref 169 - always refresh
      'ListViewLoad lvView, mView.Dataset.DataProvider, False, True
      ListViewLoad lvView, mView.Dataset.DataProvider, True, True
      
    End If
    ListViewResult
  End If
  
  'ver 2.75.00 (jjm) moved to end of sub fix bug with count not always being correctly set
  MenuViewRefresh
  
  picView_Resize
End Sub

Public Sub MenuSubItem(Optional X, Optional Y)
  Popup.mnuListViewPaste(0).Visible = (Not EventBuffer Is Nothing And mView.Dataset.DataProvider.Source = "EventView")
  Popup.mnuListViewPaste(1).Visible = (Not EventBuffer Is Nothing And mView.Dataset.DataProvider.Source = "EventView")
  PopupMenu Popup.mnuListView, , X, Y
End Sub

Public Sub MenuListItem(Optional X, Optional Y)
  MenuViewRefresh
  
  On Error Resume Next
  PopupMenu mView.PopupMenu, , X, Y, mView.DefaultMenu
  ' Either default menu doesn't exist or control array doesn't exist, or the default
  ' menu is a sub menu.
  If Err = 427 Or Err = 490 Then
    PopupMenu mView.PopupMenu, , X, Y
  ElseIf Err > 0 Then
    'MsgBox Err.Description
  End If
  On Error GoTo 0
  MenuViewRefresh
End Sub

'Refresh menu
Public Sub MenuRefresh(IsView As Boolean)
  mnuFileNewItem.Enabled = IsView
  mnuViewRefresh.Enabled = IsView
  mnuViewArrangeIcons.Enabled = IsView
  mnuViewChooseColumns.Enabled = IsView
  mnuViewSaveSettings.Enabled = IsView
  mnuToolsWorkflowOptionsMenu.Enabled = IsView
  mnuListViewMode(0).Enabled = IsView
  mnuListViewMode(1).Enabled = IsView
  mnuListViewMode(2).Enabled = IsView
  mnuListViewMode(3).Enabled = IsView
  
  If IsView Then
    'Load dynamic menu
    MenuViewRefresh
  Else
    MenuViewDisable
  End If
End Sub

'Disable the entire view. Used when no menu will be displayed.
Public Sub MenuViewDisable()
  mnuFileOpenMenu.Enabled = False
  mnuFileExploreMenu.Enabled = False
  mnuFileNewItem.Visible = False
  mnuFilePrint.Enabled = False
  mnuFileExport.Enabled = False
  mnuFileSendTo.Enabled = False
  mnuFileWorkflow(0).Enabled = False
  mnuFileWorkflow(1).Enabled = False
  
  mnuEditCut.Enabled = False
  mnuEditPaste.Enabled = False
  mnuEditMove.Enabled = False
  mnuEditInvertSelection.Enabled = False
  mnuEditSelectAll.Enabled = False
End Sub

'
' Setup menus. Also loads the right click menu on the fly.
'
Public Sub MenuViewRefresh()
  Dim Count As Integer, IsCount, Selected As Integer, IsSelected, Workflow As Boolean
  Count = lvView.ListItems.Count
  IsCount = Count > 0
  Selected = ListViewModule.SelectedCount(lvView.hWnd)
  IsSelected = Selected > 0
  Workflow = mView.IsWorkflow
  
  ' set arrange check
'  If (lvView.ColumnHeaders.Count > 0) Then
'    mnuArrangeIcon(lvView.SortKey + 1).Checked = True
'    Popup.mnuListViewArrangeIcon(lvView.SortKey + 1).Checked = True
'  End If
  
  ' If an item selected, copy over popup open and explore.
  If IsSelected Then
    sbStatusBar.Panels(1).Text = Selected & " of " & Count & " object(s) selected"
    
    If mView.OpenMenuName = "mnuEventOpen" Then
      FormUtil.MenuEventLoad Popup2, ListItemText(lvView.SelectedItem, DatabaseName.EVENT_KEY)
    ElseIf mView.OpenMenuName = "mnuGLEntryEventOpen" Then
      FormUtil.MenuEventLoad Popup2, ListItemText(lvView.SelectedItem, DatabaseName.EVENT_KEY)
      MenuCopy Popup2.mnuEventOpen, Popup2.mnuGLEntryEventOpen
    End If
    
    ' Copy over Open and Explore
    FormUtil.MenuCopy mView.OpenMenu, mnuFileOpen
    FormUtil.MenuCopy mView.ExploreMenu, mnuFileExplore
  Else
    sbStatusBar.Panels(1).Text = Count & " object(s)"
  End If
  
  mnuFileOpenMenu.Enabled = IsSelected And Not mnuFileOpen(0).Visible
  mnuFileExploreMenu.Enabled = IsSelected And Not mnuFileExplore(0).Visible
  
  'Setup File -> New -> menu item
  mnuFileNewItem.Visible = (mView.NewItemName <> vbNullString)
  If mnuFileNewItem.Visible Then
    mnuFileNewItem.Caption = mView.NewItemName
  End If
  
  mnuFilePrint.Enabled = IsCount
  mnuFileExport.Enabled = IsSelected
  mnuFileSendTo.Enabled = IsSelected And Not Workflow
    
  mnuFileWorkflow(0).Enabled = Workflow
  mnuFileWorkflow(1).Enabled = Workflow
    
  mnuEditCut.Enabled = IsSelected And Workflow
  mnuEditPaste.Enabled = Not EventBuffer Is Nothing And Workflow
  mnuEditMove.Enabled = IsSelected And Workflow
  
  mnuEditInvertSelection.Enabled = IsCount
  mnuEditSelectAll.Enabled = IsCount
  
End Sub

'
' Helper function for adding a new view.
'
Public Sub NewView(Key As String, Source As String, _
      Optional FolderType As String, Optional mIsSearchOnly As Boolean)
    
  Dim SourceDataset As Dataset, filter As New filter, IsWorkflow As Boolean
  Dim DataType As String
  Dim map As MSXML2.IXMLDOMNode
  Set map = PreferenceUtil.GetNodeMap(SystemFolders_TreeName, Key)
  
  DataType = PreferenceUtil.getMapEntryValue(map, "datatype")
  
  Set SourceDataset = NewDataset(DataType, Source)
  IsWorkflow = (DataType = DatabaseName.EVENT_TABLE)
    
  filter.Add PreferenceUtil.getMapEntryValue(map, "filter")
  If IsWorkflow Then
    filter.Add """" & DatabaseName.FOLDER_KEY & """ = '" & Key & "'"
  End If
  
  'ver 2.00.0219 (jjm)
  SourceDataset.DataProvider.BaseFilter = filter.filter
  
  Dim MenuPopup As String, PopupDefault As String, OpenMenu As String
  MenuPopup = PreferenceUtil.getMapEntryValue(map, "popup")
  If MenuPopup = vbNullString Then
    Views.Add Key, SourceDataset, IsWorkflow, mIsSearchOnly
    Exit Sub
  End If
  Views.Add Key, SourceDataset, IsWorkflow, mIsSearchOnly, GetForm(MenuPopup), GetControlName(MenuPopup), PreferenceUtil.getMapEntryValue(map, "popup.default"), PreferenceUtil.getMapEntryValue(map, "popup.open"), PreferenceUtil.getMapEntryValue(map, "popup.explore"), PreferenceUtil.getMapEntryValue(map, "new")
 
End Sub

Private Sub MenuArrangeIconLoad()
  Dim ColumnCount, I As Integer
  ColumnCount = lvView.ColumnHeaders.Count
  
  mnuViewArrangeIcons.Enabled = ColumnCount > 0
  Popup.mnuListViewArrangeIcons.Enabled = ColumnCount > 0
  
  MenuUnload mnuArrangeIcon
  For I = 1 To ColumnCount
    Load mnuArrangeIcon(I)
    mnuArrangeIcon(I).Caption = lvView.ColumnHeaders(I).Text
  Next I
  
  If mnuViewArrangeIcons.Enabled Then
    mnuArrangeIcon(0).Visible = False
  End If
  
  MenuCopy mnuArrangeIcon, Popup.mnuListViewArrangeIcon
End Sub

Public Property Get View() As View
  Set View = mView
End Property

'
' Refresh underlying data for a folder.  Called when we know the underlying data
' changes and we may not be on the foler
'
Public Sub RefreshFolder(Key As String)
  Views(Key).Dataset.DataProvider.OpenRecordset
End Sub


Public Sub SearchNow()
  ' Show hourglass
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  'If txtWords <> vbNullString Then
    ComboBoxUtil.AddText txtWords, True, mWordLimit
    ComboBoxUtil.Serialize txtWords, cboIn.Text, IniSearch, mWordCache
  'End If
  
  With lvView
    .ListItems.Clear
    lblSearchStatus.Caption = "Searching..."
    picSearchStatus.Visible = True
    lblSearchStatus.Refresh
  
    ' lastSearch is used for tmrSearch
    lastSearch = txtWords.Text
  
    ' Setup column, sort and find
    If .ColumnHeaders.Count > 0 Then
      mView.Dataset.DataProvider.Column = LawDialect.Column(lvView.ColumnHeaders)
    End If
    mView.Dataset.DataProvider.Sort = LawDialect.Sort(lvView)
      
    mView.Dataset.FindByForm Me
  
    ' if results.
    If Not .SelectedItem Is Nothing Then
      SetDefault
      .SelectedItem.Selected = False
    End If
  End With
  
  ListViewResult
End Sub

'
' Set default command button
'
Private Sub SetDefault()
  cmdDefault.Default = True
End Sub

Private Sub SequenceRefresh()
  Dim Back, Forward As Boolean
  If Not mSequence Is Nothing Then
    With mSequence
      Back = .ListIndex < .ListCount - 1
      Forward = .ListIndex > 0
    End With
  End If
  
  'cmdBack.Enabled = Back
  mnuViewGoToBack.Enabled = Back
  tbrBack.Buttons(1).Enabled = Back
  tbrBack.Buttons(1).Value = tbrUnpressed
  
  'cmdForward.Enabled = Forward
  mnuViewGoToForward.Enabled = Forward
  tbrForward.Buttons(1).Enabled = Forward
  tbrForward.Buttons(1).Value = tbrUnpressed
End Sub
'
' End General Methods
'
'
'
'
' Control Methods
'


Private Sub cboIn_Click()
  txtWords_GotFocus
End Sub

Private Sub cboIn_KeyDown(KeyCode As Integer, Shift As Integer)
  'Trap F6
  If Me.View.Dataset Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub

Private Sub cbrCoolBar_Resize()
  Form_Resize
End Sub

Private Sub cmdAdvancedSearch_Click(Index As Integer)
  mnuAdvancedFind_Click
End Sub

Private Sub cmdAdvancedSearch_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  'Trap F6
  If Me.View.Dataset Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub

Private Sub cmdDefault_Click()
  If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then Exit Sub

On Error GoTo Exception

  ' Call default menu if available.
  If Not mView.DefaultMenuName = "" Then
    If mView.DefaultMenuIndex < 0 Then
      CallByName mView.PopupMenuForm, mView.DefaultMenuName & "_Click", VbMethod
    Else
      CallByName mView.PopupMenuForm, mView.DefaultMenuName & "_Click", VbMethod, mView.DefaultMenuIndex
    End If
  End If
  Exit Sub
  
'ver 2.60.00 (jjm) 2004 03 30
Exception:
  MsgBox "Unable to call menu. Check the default popup menu settings in the folder properties." & _
      vbCrLf & "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub

Private Sub cmdMenu_Click()
  mnuViewGoToStartMenu_Click
'  If ActiveControl Is cmdMenu And Not mLastControl Is Nothing And mLastControl.Visible Then
'    mLastControl.SetFocus
'  End If
End Sub


Private Sub cmdSearchNow_Click()
  mnuViewRefresh_Click
  If lvView.ListItems.Count > 0 Then
    lvView.ListItems(1).Selected = True
    MenuViewRefresh
  End If
End Sub


Private Sub cmdSearchNow_KeyDown(KeyCode As Integer, Shift As Integer)
  'Trap F6
  If Me.View.Dataset Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub


'
' Trap for custom Ctrl+Shift Keycodes
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If (Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0 Then
    Select Case KeyCode
      Case vbKeyA
        FileNew "Application"
        KeyCode = 0
        Shift = 0
      Case vbKeyF
        FileNew "Firm"
        KeyCode = 0
        Shift = 0
      Case vbKeyL
        FileNew "Lawyer"
        KeyCode = 0
        Shift = 0
      Case vbKeyO
        FileNew "Location"
        KeyCode = 0
        Shift = 0
      Case vbKeyP
        FileNew "Person"
        KeyCode = 0
        Shift = 0
    End Select
    Exit Sub
  End If
  If (Shift And vbAltMask) > 0 And Not (Shift And vbShiftMask) And Not (Shift And vbCtrlMask) Then
    Select Case KeyCode
      Case vbKeyLeft
        If mnuViewGoToBack.Enabled Then
          mnuViewGoToBack_Click
        End If
        Shift = 0
        KeyCode = 0
      Case vbKeyRight
        If mnuViewGoToForward.Enabled Then
          mnuViewGoToForward_Click
        End If
        Shift = 0
        KeyCode = 0
      Case vbKeyHome
        mnuViewGoToHome_Click
        Shift = 0
        KeyCode = 0
      Case vbKeyM
        Shift = 0
        KeyCode = 0
        mShowPopup = True
    End Select
    Exit Sub
  End If
  If ((Shift And vbAltMask) = 0 And (Shift And vbShiftMask) = 0) Then
    Select Case KeyCode
      Case vbKeyDelete
        mnuFile_Click
        If mnuFileDelete.Visible And mnuFileDelete.Enabled Then
          mnuFileDelete_Click
        End If
    End Select
  End If
  Select Case KeyCode
    Case vbKeyInsert
      If mnuFileNewItem.Visible And mnuFileNewItem.Enabled Then
        mnuFileNewItem_Click
      End If
  End Select

End Sub

Private Sub Form_Load()
  If mUnloaded Then
    MsgBox Name & ": Illegal Load Attempt.  Please report."
    Unload Me
    Exit Sub
  End If
  
  ' Load form manager.
  FormMgr.Load Me
   
  ' Setup child forms.
  Set Popup = New frmPopup
  FormMgr.Add Popup
  Set Popup2 = New frmPopup2
  FormMgr.Add Popup2
  Set GotoMenu = New frmGoTo
  FormMgr.Add GotoMenu
   
  ' Load frame width
  picListView.Left = GetInteger(IniMain.ReadFile(UrlName.MISC_INI, "BodyLeft"))
  
  ' Load images for Tree View
  imlTVFolders.ListImages.Add 1, "TVFolderImage", PicDisplay.GetPicture(UrlName.TVFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 2, "TVOpenFolderImage", PicDisplay.GetPicture(UrlName.TVOPENFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 3, "LAWFolder", PicDisplay.GetPicture(UrlName.LAWFOLDER_IMAGE)
  
  ' Populuate tree and load folders.
  tvFolders.ImageList = imlTVFolders
  
  Set Views = New ViewItems
  'PopulateTree tvFolders
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  
  ' Get view settings from ini file.
  mnuViewFolders.Checked = IniMain.ReadFile(UrlName.MAIN_INI, "ViewFolders")
  mnuViewToolBar.Checked = IniMain.ReadFile(UrlName.MAIN_INI, "ViewToolbar")
  mnuViewStatusBar.Checked = IniMain.ReadFile(UrlName.MAIN_INI, "ViewStatusBar")
  mnuViewGoToStartMenu.Caption = mnuViewGoToStartMenu.Caption & vbTab & "Alt+M"
   
  ' position settings
  ' set defaults first
  Top = 0
  Left = 0
  Height = 7200
  Width = 9600
  ' get saved settings, if any.
  FormUtil.DeserializePosition Me
   
  ' Custom Menu Shortcut Adjustments
  KeyPreview = True
  mnuFileExit.Caption = mnuFileExit.Caption + vbTab + "Alt+F4"
  mnuViewGoToBack.Caption = mnuViewGoToBack.Caption + vbTab + "Alt+Left Arrow"
  mnuViewGoToForward.Caption = mnuViewGoToForward.Caption + vbTab + "Alt+Right Arrow"
  mnuViewGoToHome.Caption = mnuViewGoToHome.Caption + vbTab + "Alt+Home"
  
  'Customize log off menu item
  mnuFileLogOff.Caption = "Log O&ff " & UserName & "..."
    
  ' Initialize Controls
  Set mSequence = New Sequence
  mSequence.Size = 10
  SequenceRefresh
    
  FileNewMenuLoad
  
  ' Hide alt key shortcuts.
  cmdMenu.Left = -20000
  cmdDefault.Left = -20000
  
  'Load the images
  imlArrows.ListImages.Add 1, "Back", PicDisplay.GetPicture(UrlName.BACK_IMAGE)
  imlArrows.ListImages.Add 2, "Forward", PicDisplay.GetPicture(UrlName.FORWARD_IMAGE)
  imlArrows.ListImages.Add 3, "Up", PicDisplay.GetPicture(UrlName.UPONELEVEL_IMAGE)
  
  imlArrowsDisabled.ListImages.Add 1, "DisBack", PicDisplay.GetPicture(UrlName.BACKDISABLED_IMAGE)
  imlArrowsDisabled.ListImages.Add 2, "DisForward", PicDisplay.GetPicture(UrlName.FORWARDDISABLED_IMAGE)
  imlArrowsDisabled.ListImages.Add 3, "DisUp", PicDisplay.GetPicture(UrlName.UPONELEVELDISABLED_IMAGE)
  
  ' set the image list to the toolbar
  tbrBack.ImageList = imlArrows
  tbrForward.ImageList = imlArrows
  tbrUpOneLevel.ImageList = imlArrows
  
  tbrBack.DisabledImageList = imlArrowsDisabled
  tbrForward.DisabledImageList = imlArrowsDisabled
  tbrUpOneLevel.DisabledImageList = imlArrowsDisabled
  
  tbrBack.Buttons(1).Image = "Back"
  tbrForward.Buttons(1).Image = "Forward"
  tbrUpOneLevel.Buttons(1).Image = "Up"
  
  
  ' Setup screen.
  tbToolBar.Height = tbToolBar.ButtonHeight
  tbrStart.Height = tbrStart.ButtonHeight
  tbrForward.Height = tbrForward.ButtonHeight
  tbrBack.Height = tbrBack.ButtonHeight
  tbrUpOneLevel.Height = tbrUpOneLevel.ButtonHeight
  tbrFolderWatch.Height = tbrFolderWatch.ButtonHeight
  
  ' Setup Start Button cool bar band
  cbrCoolBar.Bands(1).MinHeight = tbrStart.Height + 24
  cbrCoolBar.Bands(1).MinWidth = tbrStart.ButtonWidth  ' + tbrBack.Width + tbrForward.Width
  cbrCoolBar.Bands(1).Width = tbrStart.ButtonWidth ' + tbrBack.Width + tbrForward.Width
  
  ' Setup Navigation cool bar Band.
  FormUtil.SizeSingleToolbar tbrBack, 0
  FormUtil.SizeSingleToolbar tbrForward, tbrBack.Left + tbrBack.Width
  FormUtil.SizeSingleToolbar tbrUpOneLevel, tbrForward.Left + tbrForward.Width
  cbrCoolBar.Bands(2).MinHeight = tbrBack.Height + 24
  cbrCoolBar.Bands(2).MinWidth = tbrBack.Width + tbrForward.Width + tbrUpOneLevel.Width
  cbrCoolBar.Bands(2).Width = tbrBack.Width + tbrForward.Width + tbrUpOneLevel.Width
  
  cbrCoolBar.Bands(3).MinHeight = tbToolBar.Height + 24
  
  ' Unload splash screen last
  If Not fSplash Is Nothing Then
    Unload fSplash
  End If
      
  ' Get any limit to the drop downs.
  ComboBoxUtil.SetDropDownHeight Me, txtWords, mWordLimit
  mWordLimit = Max(GetInteger(IniMain.ReadFile("WordList", "limit")), 20)
      
  lblWelcolme.Caption = "Welcome to the " & LocaleShortOrganization & " implementation of the LAW System."
  lblVersion.Caption = LAWVersion
  
  picSplitter.Left = GetInteger(IniMain.ReadFile(UrlName.MISC_INI, "LeftWidth"))
  If picSplitter.Left > sglSplitLimit Then
    FrameRefresh
  End If
'  lblStart.Move cbrCoolBar.Left + picStart.Left + 24, _
'        cbrCoolBar.Top + picStart.Top + cbrCoolBar.Bands(2).Height + 12
'  lblStart.Visible = True

 FolderWatchInit

End Sub

'
' Ensure any outstanding timer is disabled when leaving form.
'
Private Sub Form_LostFocus()
  tmrFolderList.Enabled = False
  tmrSearch.Enabled = False
End Sub

Private Sub Form_Resize()
  ' Do not resize when minimized.
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  fraFolders.Visible = mnuViewFolders.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  sbStatusBar.Visible = mnuViewStatusBar.Checked
  
  Dim StatusBar As Integer
  StatusBar = IIf(sbStatusBar.Visible, sbStatusBar.Height, 0)
  picView.Height = Max(Me.ScaleHeight - StatusBar - picView.Top, 0)
End Sub

'
' Uncomment MsgBox to test for proper form termination.  Terminate
' should NOT reference any controls or the form is reloaded.
'
Private Sub Form_Terminate()
  'MsgBox "Main_Terminate"
End Sub


Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  
  ComboBoxUtil.Flush IniSearch, mWordCache
  
  ' Close down form manager
  FormMgr.Unload
  
  ' Unload child forms
  Unload Popup
  Unload Popup2
  Unload GotoMenu
  
  ' See if we are saving previous folder.
  If GetInteger(IniForm.ReadFile(Name, "home")) = 0 Then
    IniForm.WriteFile Name, "folderkey", tvFolders.SelectedItem.Key
  End If
  
  'Save framesettings
  IniMain.WriteFile UrlName.MISC_INI, "LeftWidth", str(picSplitter.Left)
  
  mUnloaded = True
End Sub



Private Sub imgSplitter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  With imgSplitter
    picSplitter.Move .Left, .Top + picView.Top, .Width, .Height - 20
  End With
  picSplitter.Visible = True
  mbMoving = True
End Sub


Private Sub imgSplitter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim sglPos As Single
  
  If mbMoving Then
    sglPos = X + imgSplitter.Left
    If sglPos < sglSplitLimit Then
      picSplitter.Left = sglSplitLimit
    ElseIf sglPos > Me.Width - sglSplitLimit Then
      picSplitter.Left = Me.Width - sglSplitLimit
    Else
      picSplitter.Left = sglPos
    End If
  End If
End Sub

Private Sub imgSplitter_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  FrameRefresh
End Sub

Private Sub lblListView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  picListView_MouseUp Button, Shift, X, Y
End Sub



Public Sub lvView_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'  mnuArrangeIcon(lvView.SortKey + 1).Checked = False
'  Popup.mnuListViewArrangeIcon(lvView.SortKey + 1).Checked = False
  
  If lvView.ListItems.Count = 0 Then
    ListViewSort lvView, ColumnHeader
  Else
    Dim pointer As New WaitPointer
    pointer.pointer = vbHourglass
    ListViewSort lvView, ColumnHeader, mView.Dataset.DataProvider, True
  End If
  MenuViewRefresh
End Sub

Private Sub lvview_DblClick()
  cmdDefault_Click
End Sub

Private Sub lvView_GotFocus()
  If Not lvView.SelectedItem Is Nothing Then
    SetDefault
  End If
  
  ' MsgBox mnuFileOpenMenu.Visible
  tmrMenu.Interval = 1
  tmrMenu.Enabled = True
 
  ' Refresh full menu.  Don't do MenuViewRefresh as we
  ' need to enable the view set.
  MenuRefresh True
End Sub

Private Sub lvView_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF10 And (Shift And vbShiftMask) > 0 And Not mView.PopupMenu Is Nothing Then
    If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
      MenuSubItem picListView.Left + 24, picListView.Top + 24
    Else
      MenuListItem picListView.Left + 24, picListView.Top + 24
    End If
    KeyCode = 0
    Exit Sub
  End If

  'Trap F6
  If KeyCode = vbKeyF6 Then
    If (Shift And vbShiftMask) > 0 Then
      If txtWords.Visible Then txtWords.SetFocus
    Else
      If tvFolders.Visible Then tvFolders.SetFocus
    End If
  End If
End Sub

Private Sub lvView_KeyUp(KeyCode As Integer, Shift As Integer)
  MenuViewRefresh
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView.hWnd, X, Y) And ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    lvView.SelectedItem.Selected = False
  End If

'ver 2.60.00 (jjm) 2004 03 30
On Error GoTo Exception
  If Button = vbRightButton And (Not mView.PopupMenu Is Nothing) Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
       MenuSubItem
     Else
       MenuListItem
     End If
  End If
  MenuViewRefresh
  Exit Sub
  
'ver 2.60.00 (jjm) 2004 03 30
Exception:
  MsgBox "Unable to load popup menu. Check the folder properties settings." & vbCrLf & _
      "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub

Private Sub mnuAdvancedFind_Click()
  If Not mView.IsWorkflow Then
     Dispatcher.ShowSearch mView.Key, FormMgr
  End If
End Sub

Private Sub mnuArrangeIcon_Click(Index As Integer)
  Call lvView_ColumnClick(lvView.ColumnHeaders(Index))
End Sub

'
' Called from event popup.
'
Public Sub mnuEditCut_Click()
  Dim I, Count As Integer
  Set EventBuffer = New Collection
  EventBufferFolder = mFolderKey
  
  With lvView
    Count = .ListItems.Count
    For I = 1 To Count
      If .ListItems(I).Selected Then
        EventBuffer.Add ListItemText(.ListItems(I), DatabaseName.EVENT_KEY)
      End If
    Next I
  End With
  
  MenuViewRefresh
End Sub

Private Sub mnuEditInvertSelection_Click()
  Dim I, Count As Integer
  Count = lvView.ListItems.Count
  
  With lvView
    Count = .ListItems.Count
    For I = 1 To Count
      .ListItems(I).Selected = (Not .ListItems(I).Selected)
    Next I
  End With
  
  MenuViewRefresh
End Sub

Public Sub mnuEditMove_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowMove(tvFolders.SelectedItem.Key) Then
    Factory.EventService.MoveToFolder lvView, fForm.FolderKey
    mnuViewRefresh_Click
  End If
  Unload fForm
End Sub

'
' Called from popups.
'
Public Sub mnuEditPaste_Click()
  Dim Hourglass As New WaitPointer
  Hourglass.Default
    
  Factory.EventService.PasteToFolder mFolderKey
  
  ' Mark old folder as dirty and Refresh current
  RefreshFolder EventBufferFolder
  mnuViewRefresh_Click
  
  EventBufferFolder = ""
  Set EventBuffer = Nothing

  MenuViewRefresh
End Sub

Private Sub mnuEditRotate_Click()
  cboIn.ListIndex = (cboIn.ListIndex + 1) Mod cboIn.ListCount
  txtWords.SetFocus
End Sub

Private Sub mnuEditSelectAll_Click()
  ListViewModule.SelectAll lvView.hWnd
  MenuViewRefresh
End Sub


'
' Set Open/Explore menus
'
Public Sub mnuFile_Click()
  
  If ActiveControl Is lvView Then
    mnuFileProperties.Tag = vbNullString
    If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
      mnuFileProperties.Tag = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.EVENT_KEY)
    End If
    mnuFileProperties.Enabled = mnuFileProperties.Tag <> vbNullString
  End If
  MenuRefresh Not mView Is Nothing
End Sub


'
' Called from popups.
'
Public Sub mnuFileDelete_Click()
  PreferenceUtil.RemoveFolder tvFolders
  FolderKey = tvFolders.SelectedItem.Key
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileExplore_Click(Index As Integer)
  CallByName mView.PopupMenuForm, mView.ExploreMenuName & "_Click", VbMethod, Index
End Sub

Private Sub mnuFileExport_Click()
  Dim ExportDlg As New frmExportDialog
  Dim Index As Integer
  Index = ExportDlg.ShowExport(Me)
  If Index > -1 Then
    FormUtil.ExportItem lvView, Index, dlgSave, , mFolderKey
  End If
End Sub

Private Sub mnuFileLogOff_Click()
  If MsgBox("Are you sure you want to log off?", vbYesNo, "Log Off LAW") = vbYes Then
    LogOff Me
  End If
End Sub

Private Sub mnuFileNew_Click(Index As Integer)
  FileNew mnuFileNew(Index).Tag
End Sub

Public Sub mnuFileNewFolder_Click()
  Dim Node As Node, ViewFile As String
  Set Node = PreferenceUtil.AddFolder(tvFolders)
  If Node Is Nothing Then Exit Sub
  
  NewView Node.Key, "EventView", "Workflow"
  FolderKey = Node.Key
  tvFolders.StartLabelEdit
End Sub

Public Sub mnuFileNewItem_Click()
  FileNew Replace(mnuFileNewItem.Caption, " ", "")
End Sub

Public Sub mnuFileOpen_Click(Index As Integer)
  CallByName mView.PopupMenuForm, mView.OpenMenuName & "_Click", VbMethod, Index
End Sub


Private Sub mnuFilePrint_Click()
  PrintUtil.PrintDialog Me, lvView
End Sub

Private Sub mnuFilePrintSetup_Click()
  PrintUtil.PrintSetupDialog Me
End Sub

Public Sub mnuFileProperties_Click()
  ' trap for list view.
  If ActiveControl Is lvView Then
    If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
      FormUtil.ShowEventProperties mnuFileProperties.Tag
      Exit Sub
    End If
  End If
  
  ' If no list view trap, then just show folder properties.
  Dim fProperties As New frmFolderPropertiesEdit
  Load fProperties
  fProperties.ShowFolder mFolderKey
End Sub


Public Sub mnuFileRename_Click()
  If ActiveControl Is Nothing Then Exit Sub
  If ActiveControl Is tvFolders Then
    If Not tvFolders.SelectedItem Is Nothing Then
      tvFolders.StartLabelEdit
    End If
  End If
End Sub

'
' Called from various popups.
'
Public Sub mnuFileSendToFolder_Click()
  Dim Source As String, ruleId As String, ReferenceId As String, EventName As String
  'ver 2.00.0311 (jjm) Core - Select by type over folder key
  'Select Case IniFolder.ReadFile(mFolderKey, "data.type") 'mFolderKey
  Dim map As MSXML2.IXMLDOMNode
  Set map = PreferenceUtil.GetNodeMap(SystemFolders_TreeName, mFolderKey)
  Select Case PreferenceUtil.getMapEntryValue(map, "datatype")
    Case DatabaseName.APPLICATION_TABLE
      Source = DatabaseName.APPLICATION_TABLE
      ruleId = DatabaseName.APPLICATION_REF
      EventName = DatabaseName.FIRMNAME_FIELD
    Case DatabaseName.QUOTE_TABLE
      Source = DatabaseName.QUOTE_TABLE
      ruleId = DatabaseName.APPLICATIONRATING_REF
      EventName = DatabaseName.FIRMNAME_FIELD
    Case DatabaseName.FIRM_TABLE
      Source = DatabaseName.FIRM_TABLE
      ruleId = DatabaseName.FIRM_TABLE & "Reference"
      EventName = DatabaseName.FIRMNAME_FIELD
    Case DatabaseName.CLAIM_TABLE
      Source = DatabaseName.CLAIM_TABLE
      ruleId = DatabaseName.CLAIM_REF
      EventName = DatabaseName.CLAIMNAME_FIELD
  End Select
  
  ' If not a valid source for reference.
  If Source = vbNullString Then
    MsgBox "Send To Folder is not supported for this record type."
    Exit Sub
  End If
  
  ' Create form and show.
  Dim Form As New frmFolderBrowseDialog
  If Not Form.ShowSendTo(ruleId) Then
    Unload Form
    Exit Sub
  End If
  
  '
  ' Form is OK
  '
  Refresh
  Dim pointer As New WaitPointer
  pointer.Default
  
  Call Factory.EventService.Create( _
      Form.eventTypeId, _
      ruleId, _
      Form.FolderKey, _
      ListViewText(lvView, DatabaseName.EVENT_KEY), _
      ListViewText(lvView, EventName))
  
  Unload Form
End Sub

Public Sub mnuFileWorkflow_Click(Index As Integer)
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  If Not FolderUtil.WorkflowShow(mFolderKey, Index, ParameterNames, ParameterValues, True) Then
    Exit Sub
  End If
  
  '  Call Service
  Dim pointer As New WaitPointer
  pointer.Default
  Factory.EventService.SetViewWorkflow Index, lvView, DatabaseName.EVENT_KEY, ParameterNames, ParameterValues
  SearchNow
End Sub

Private Sub mnuGoToExplore_Click()
  If mnuFileExploreMenu.Enabled Then
    PopupMenu mnuFileExploreMenu, , picListView.Left + 24, picListView.Top + 24
  End If
End Sub

Private Sub mnuGoToOpen_Click()
  If mnuFileOpenMenu.Enabled Then
    PopupMenu mnuFileOpenMenu, , picListView.Left + 24, picListView.Top + 24
  End If
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpSearchForHelpOn_Click()
    Dim nRet As Integer

    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hWnd, App.HelpFile, 261, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuListViewMode_Click(Index As Integer)
  lvView.View = Index
  tbToolBar.Buttons("lvMode" & Index) = tbrPressed
End Sub

Private Sub mnuToolsEFTExportEFTTransmissionFile_Click()

  'Get the EFT filename
  Dim EFTFileHandle As String
  Dim EFTFileDataset As Dataset
  Dim EFTFileRow As Collection
  Set EFTFileDataset = NewGenericDataset(DatabaseName.EFTFILE_TABLE)
  EFTFileDataset.DataProvider.filter = LawDialect.ColumnIsEmpty(DatabaseName.ISCLOSED_FIELD)
  EFTFileDataset.DataProvider.OpenRecordset
  Set EFTFileRow = EFTFileDataset.DataProvider.Collection
  If Not EFTFileRow Is Nothing Then
    EFTFileHandle = EFTFileRow(1)(DatabaseName.EFTFILE_KEY)
  Else
    MsgBox "There are no open EFT Files available.", vbCritical
    Exit Sub
  End If
  
  'Setup and Display the form
  Dim fExport As New frmEFTFileExportDialog
  Load fExport
  fExport.txtFile.Text = EFTFileHandle
  fExport.txtPath.Text = IniMain.ReadFile("defaults", "exportefttransmissionfilepath")
  fExport.Show vbModal
    
  'If the form was canceled out then exit the sub
  If Not fExport.Ok Then
    Unload fExport
    Exit Sub
  End If
  
  'Set the params section of the url with the file name which is from the eftFileId field
  Dim Params As String
  Params = "key=" & EncodeURL(EFTFileHandle)
  
  'Call middle tier to get the eft file
  Dim xmlHttp As xmlHttp
  Set xmlHttp = HttpPost(HttpServer & WebUtil & "eftopen.custom" & "?content=text/plain", Params)
  
  'Check to make sure that file is valid
  'A valid eft file will have '1' as the first character in the file
  If Left(xmlHttp.responseText, 1) <> "1" Then
    MsgBox "Unable to export the eft file due to the following: " & vbCrLf & xmlHttp.responseText, vbCritical, "EFT Export Error"
    Exit Sub
  End If
  
  'Write the eft file
  WriteTextFile xmlHttp.responseText, fExport.txtPath.Text & "\" & EFTFileHandle
  
  'Close the EFT File
  '
  'The default action of the following http call is to close the EFT FIle
  'To reopen an EFT File add the following parameter "&open=true" to the url.
  'This url can be called using http get or post.
  Set xmlHttp = HttpGet(HttpServer & WebUtil & "eftfile?key=" & EFTFileHandle)
  If InStr(xmlHttp.responseText, "OK") = 0 Then
    MsgBox "Error: EFT File " & EFTFileHandle & " was not successfully closed.", vbCritical, "EFT File Close Error"
  End If
            
  'Save the path as a law.ini setting
  IniMain.WriteFile "defaults", "exportefttransmissionfilepath", fExport.txtPath.Text
  
  Unload fExport
  
End Sub

'ver 2.10.0506
Private Sub mnuToolsEnvelope_Click()
  Dim pointer As New WaitPointer
  Dim Envelope As New MailLabel
  
  'Check to see if it is a document
  Dim EventGroup As String
  EventGroup = ListItemText(lvView.SelectedItem, DatabaseName.EVENTTYPERULEID_FIELD)
  If (EventGroup = DatabaseName.FIRM_DOCUMENT Or _
      EventGroup = DatabaseName.CLAIM_DOCUMENT Or _
      EventGroup = DatabaseName.ENDORSEMENT_TABLE Or _
      EventGroup = DatabaseName.INCIDENTDOCUMENT_RULE) And _
      ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
        
    Dim Handles() As String, I As Integer
    Handles = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
    
    ' Print Envelopes
    If SafeLBound(Handles) - SafeUBound(Handles) < 0 Then 'Need a better check
      If MsgBox("Would you like to print evelopes for all of the items selected?", vbYesNo) = vbYes Then
        pointer.Default
        For I = SafeLBound(Handles) To SafeUBound(Handles)
          Factory.EventRecipientService.PrintEnvelope Handles(I), True
        Next I
        Exit Sub
      End If
    End If
                    
    Dim Rows As Collection
    For I = SafeLBound(Handles) To SafeUBound(Handles)
      Set Rows = Factory.EventRecipientService.GetEnvelope(Handles(I), , True)
      If Not FormUtil.PrintEnvelope(CStr(Rows(1)("deliveryAddress"))) Then
        Exit Sub
      End If
    Next I
    Exit Sub
  End If
  
  'If there is a location handle then we can build
  'the address from the location table
  If ListItemText(lvView.SelectedItem, DatabaseName.LOCATION_KEY) <> vbNullString Or _
     ListItemText(lvView.SelectedItem, DatabaseName.MAINLOCATIONID_FIELD) <> vbNullString Then
    
    'Load handles
    Dim LocationHandles() As String
    Dim RecipientNames() As String
        
    'Set recipeint name depending on folder key
    Dim RecipientFieldName As String
    Select Case mView.Dataset.DataProvider.Source
      'Case DatabaseName.CONTACT_TABLE
      '  RecipientFieldName = DatabaseName.CONTACTNAME_FIELD
      Case DatabaseName.CONTACT_VIEW
        RecipientFieldName = DatabaseName.ENTITYNAME_FIELD
      Case DatabaseName.PERSON_TABLE
        RecipientFieldName = DatabaseName.ENTITYNAME_FIELD
      Case DatabaseName.PERSON_VIEW
        RecipientFieldName = DatabaseName.ENTITYNAME_FIELD
      Case DatabaseName.LAWYER_TABLE
        RecipientFieldName = DatabaseName.LAWYERENTITYNAME_FIELD
      Case DatabaseName.LAWYER_VIEW
        RecipientFieldName = DatabaseName.LAWYERENTITYNAME_FIELD
      Case DatabaseName.FIRM_TABLE
        RecipientFieldName = DatabaseName.FIRM_TABLE
      Case DatabaseName.FIRM_VIEW
        RecipientFieldName = DatabaseName.FIRM_TABLE
      Case DatabaseName.ANSWER_TABLE 'ver 2.10.0421 For printing mass envelopes
        RecipientFieldName = DatabaseName.CONTACTNAME_FIELD
    End Select
    
    
    ' Manually retrieve location list. This needs to be done because the location handle
    ' may be stored in a locationId or a mainLocationId field. If you could use only one field
    ' the following function would be used: ListViewModule.ListItemArray()
        
    Dim item As ListItem
    Dim ListCount, SelectCount, indexCounter As Integer
    Dim tmpLocation
    ListCount = lvView.ListItems.Count
    SelectCount = ListViewModule.SelectedCount(lvView.hWnd)
    ReDim LocationHandles(SelectCount - 1) As String
    indexCounter = 0
    Dim n As Integer
    For n = 1 To ListCount
      Set item = lvView.ListItems(n)
      If item.Selected Then
        tmpLocation = vbNullString
        'First try locationId, then try mainLocationId
        tmpLocation = ListItemText(item, DatabaseName.LOCATION_KEY)
        If tmpLocation = vbNullString Then
          tmpLocation = ListItemText(item, DatabaseName.MAINLOCATIONID_FIELD)
        End If
        LocationHandles(indexCounter) = tmpLocation
        indexCounter = indexCounter + 1
      End If
    Next n
    
    ReDim RecipientNames(0 To SafeUBound(LocationHandles))
    
    If RecipientFieldName = DatabaseName.FIRM_TABLE Then
      'Get the displayName of the contact based on the personId in the firm table
      Dim FirmHandles() As String
      Dim c As Integer
      FirmHandles = ListViewModule.ListItemArray(lvView, DatabaseName.FIRM_KEY)
      Dim PersonHandle As String
      For c = 0 To SafeUBound(FirmHandles)
        PersonHandle = GetDataFromCollection(Factory.OrganizationService.Find(FirmHandles(c), DatabaseName.MAINPERSONID_FIELD), DatabaseName.MAINPERSONID_FIELD)
        If PersonHandle <> vbNullString Then
          RecipientNames(c) = Factory.EntityService.Find(PersonHandle)(DatabaseName.ENTITYNAME_FIELD)
        End If
      Next c
    Else
      RecipientNames = ListViewModule.ListItemArray(lvView, RecipientFieldName)
    End If
               
    'Ask if the user wants to print all envelopes or preview each one
    If SafeLBound(LocationHandles) - SafeUBound(LocationHandles) < 0 Then 'Need a better check
      If MsgBox("Would you like to print evelopes for all of the items selected?", vbYesNo) = vbYes Then
        pointer.Default
        For n = 0 To SafeUBound(LocationHandles)
          Envelope.PrintEnvelope Factory.LocationService.GetLocationAddress(LocationHandles(n), RecipientNames(n)), LocaleEnvelopeBarCode = 1
        Next n
        Exit Sub
      End If
    End If

    For n = 0 To SafeUBound(LocationHandles)
      If Not FormUtil.PrintEnvelope(Factory.LocationService.GetLocationAddress(LocationHandles(n), RecipientNames(n))) Then
        Exit Sub
      End If
    Next n
    Exit Sub
  End If
  
  'Display the empty envelope dialog
  FormUtil.PrintEnvelope

End Sub

Private Sub mnuToolsMerge_Click()
  Dim pointer As New WaitPointer
  pointer.Default
  
  Dim Handles() As String, I As Integer
  Handles = ListViewModule.ListItemArray(lvView, DatabaseName.EVENT_KEY)
  
  ' Print Envelopes
  For I = 0 To SafeUBound(Handles)
    Factory.EventRecipientService.PrintEnvelope Handles(I)
  Next I
  
  ' Merge Documents
  Set pointer = Nothing
  Factory.EventService.DocumentMerge Handles, True
End Sub

Private Sub mnuToolsOptions_Click()
  Dim fOptions As New frmMainOptionsDialog
  fOptions.Show vbModal, Me
  Unload fOptions
  picListView_Resize
End Sub

Public Sub mnuToolsOrganizeFolders_Click()
  Dim fForm As New frmFolderOrganizeDialog
  fForm.ShowFolder mFolderKey
  Unload fForm
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  'FolderUtil.TreeViewLoadFolder tvFolders, systemfolders_treename
  If Contains(tvFolders.Nodes, mFolderKey) Then
    tvFolders.Nodes(mFolderKey).Selected = True
  End If
End Sub

Private Sub mnuToolsPreferences_Click()
  Dim fPref As New frmPreferencesEdit
  Load fPref
  fPref.Show
End Sub

Private Sub mnuToolsReport_Click()
  Dim fPrintDialog As New frmPrintDialog
  fPrintDialog.Show vbModal
  
  If Not fPrintDialog.mOk Then
    Exit Sub
  End If
  
  Dim QuoteBookHandle As String
  Dim QuoteData As Collection
  Dim Report As CRAXDRT.Report
  
  'Mantis 2184
  Dim ReportPath As String
  ReportPath = IniFolder.ReadFile(UrlName.CRYSTALREPORTS_INI, UrlName.PREMIUMQUOTEREPORTPATH_INI)
  
  If ReportPath = vbNullString Then
    MsgBox "The location of the Premium Quotation Report is not configured. Please add a " & _
           UrlName.PREMIUMQUOTEREPORTPATH_INI & " entry in the " & UrlName.CRYSTALREPORTS_INI & _
           " section in the following ini file:" & vbCrLf & IniFolder.FileName, vbCritical
    Exit Sub
  End If
  
  'ver 2.50.00 (jjm) 2004 02 19 'enabled multiple printing at one time
  Dim item As ListItem
  For Each item In lvView.ListItems
    'reset the objects
    QuoteBookHandle = vbNullString
    Set QuoteData = Nothing

    If item.Selected Then
      'Get the application rating handle
      
      'Test for handle in the ListView
      QuoteBookHandle = ListItemText(item, DatabaseName.QUOTEBOOK_KEY)
      
      'Find by reference if not found directly
      If QuoteBookHandle = vbNullString Then
        Dim EventHandle As String
        Dim quoteBookEventHandle As String
      
        EventHandle = ListItemText(item, DatabaseName.EVENT_KEY)
        quoteBookEventHandle = Factory.ReferenceService.FindReferenceHandleBySourceType(EventHandle, DatabaseName.QUOTEBOOK_TABLE)
        
        'Test for the actual QuoteBook Event
        QuoteBookHandle = DataUtil.FindHandle(DatabaseName.QUOTEBOOK_TABLE, DatabaseName.EVENT_KEY, _
                                              quoteBookEventHandle, _
                                              DatabaseName.QUOTEBOOK_KEY)
      End If
            
      'Check the Handle
      If (QuoteBookHandle = vbNullString) Then
        MsgBox "Unabled to find an application rating (QuoteBook) to print"
        'Exit Sub
      Else
        Set QuoteData = Factory.QuoteService.FindByMainQuoteBook(QuoteBookHandle)
        Set Report = GetCrystalReport( _
              ReportPath, _
              GetDataFromCollection(QuoteData, DatabaseName.APPLICATION_KEY), _
              QuoteBookHandle, _
              "AE", _
              5)
        On Error GoTo Exception
        Report.PrintOut
      End If
    End If
  Next item
  
  Exit Sub
  
Exception:
  
  Dim errorInfo As String
  errorInfo = errorInfo & "Error Source: " & Err.Source & vbCrLf
  If Not Report Is Nothing Then
    errorInfo = errorInfo & "DriverName: " & Report.driverName & vbCrLf
    errorInfo = errorInfo & "PrinterName: " & Report.PrinterName & vbCrLf
    errorInfo = errorInfo & "SQLQueryString: " & Report.SQLQueryString & vbCrLf
  End If

  ErrorUtil.DisplayMessage "Error: " & Err.Number & " " & Err.Description, errorInfo, "Report Error", False
  Err.Clear
End Sub

Private Sub mnuToolsService_Click(Index As Integer)
  Select Case Index
    Case 1 'Claims Warehouse
      Dim fClaimWarehouse As New frmDateSelectDialog
      fClaimWarehouse.Caption = "Claim Warehouse Update"
      fClaimWarehouse.lblDescription.Caption = "Select a period ending date."
      fClaimWarehouse.Show vbModal, Me
      
      If Not fClaimWarehouse.Ok Then
        Unload fClaimWarehouse
        Exit Sub
      End If
      
      Factory.ServiceService.InitService "Claim Warehouse", _
        "com.wilmic.script.ClaimWarehouse", _
        Format(fClaimWarehouse.dtpDate.Value, "yyyy-mm-dd"), _
        vbNullStringArray, vbNullStringArray, _
        5, True, True
      
      Unload fClaimWarehouse
      
    Case 2 'Deductible Due Update
      Dim fDeductibleUpdate As New frmDateSelectDialog
      fDeductibleUpdate.Caption = "Deductible Due Update"
      fDeductibleUpdate.lblDescription.Caption = "Select a period ending date."
      fDeductibleUpdate.Show vbModal, Me
      
      If Not fDeductibleUpdate.Ok Then
        Unload fDeductibleUpdate
        Exit Sub
      End If
    
      ' Use Main service to launch the DeductibleDue script with command
      ' line parameter for the date.
      Factory.ServiceService.InitService "Deductible Due", _
        "com.wilmic.script.DeductibleDue", _
        Format(fDeductibleUpdate.dtpDate.Value, "yyyy-mm-dd"), _
        vbNullStringArray, vbNullStringArray, _
        5, True, True
        
      Unload fDeductibleUpdate
    
    Case 3 'Firm Insure Status
      Dim fFirmInsureStatus As New frmDateSelectDialog
      fFirmInsureStatus.Caption = "Firm Insure Status Service"
      fFirmInsureStatus.lblDescription.Caption = "As of Date:"
      
      fFirmInsureStatus.Show vbModal, Me
      
      If Not fFirmInsureStatus.Ok Then
        Unload fFirmInsureStatus
        Exit Sub
      End If
      
      Factory.ServiceService.InitService "FirmInsureStatus", _
        "com.wilmic.service.FirmInsureStatus", _
        Format(fFirmInsureStatus.dtpDate.Value, "yyyy-mm-dd"), _
        vbNullStringArray, vbNullStringArray, _
        5, True, True
     
      Unload fFirmInsureStatus
      
    Case 4 'FirmLastInsurer
      If MsgBox("Continue and run the Firm Last Insurer Service?", vbYesNo, "Firm Last Insurer Service") <> vbYes Then
        Exit Sub
      End If
      
      Factory.ServiceService.InitService "FirmLastInsurer", _
        "com.wilmic.service.FirmSession", "FirmLastInsurer", _
        vbNullStringArray, vbNullStringArray, _
        5, True, True
      
    Case 5 'InPracticeDuration
      Dim fLawyerInPractice As New frmDateSelectDialog
      'set the default date
      Dim mDate As String
      mDate = DateSerial(Year(Today), Month(Today), 1)
      mDate = DateAdd("m", 5, mDate)
      mDate = DateAdd("d", -1, mDate)
      fLawyerInPractice.dtpDate = mDate
      fLawyerInPractice.Caption = "Lawyer In Practice Duration"
      fLawyerInPractice.lblDescription.Caption = "Select a date to run the lawyer in practice duration update."

      fLawyerInPractice.Show vbModal, Me

      If Not fLawyerInPractice.Ok Then
        Unload fLawyerInPractice
        Exit Sub
      End If
      
      Factory.ServiceService.InitService "LawyerInPractice", _
        "com.wilmic.service.InPracticeDuration", _
        Format(fLawyerInPractice.dtpDate.Value, "yyyy-mm-dd"), _
        vbNullStringArray, vbNullStringArray, _
        5, True, True
     
      Unload fLawyerInPractice
     
    Case 6 ' Lawyer Insure Status
      Dim fLawyerInsureStatus As New frmDateSelectDialog
      fLawyerInsureStatus.Caption = "Lawyer Insure Status Service"
      fLawyerInsureStatus.lblDescription.Caption = "As of Date:"
            
      fLawyerInsureStatus.Show vbModal, Me
      
      If Not fLawyerInsureStatus.Ok Then
        Unload fLawyerInsureStatus
        Exit Sub
      End If
      
      Factory.ServiceService.InitService "LawyerInsureStatus", _
        "com.wilmic.service.LawyerInsureStatus", _
        Format(fLawyerInsureStatus.dtpDate.Value, "yyyy-mm-dd"), _
        vbNullStringArray, vbNullStringArray, _
        5, True, True
     
      Unload fLawyerInsureStatus
      
    Case 7 ' Loss Triange
      Dim fLossTriangle As New frmDateSelectDialog
      fLossTriangle.Caption = "Loss Triangle"
      fLossTriangle.lblDescription.Caption = "Select a period ending date."
      fLossTriangle.Show vbModal, Me
      
      If Not fLossTriangle.Ok Then
        Unload fLossTriangle
        Exit Sub
      End If
      
      Factory.ServiceService.InitService "Loss Triangle", _
        "com.wilmic.script.LossTriangle", _
        Format(fLossTriangle.dtpDate.Value, "yyyy-mm-dd"), _
        vbNullStringArray, vbNullStringArray, _
        5, True, True
      
      Unload fLossTriangle
    Case 8 ' Monthly EFT accounting
      Dim fEFTAccounting As New frmEFTAccountingDialog
      fEFTAccounting.Show vbModal, Me
      If Not fEFTAccounting.mOk Then
        Unload fEFTAccounting
        Exit Sub
      End If
      
      Dim eParameterNames(3) As String
      Dim eParameterValues(3) As String
      eParameterNames(0) = "com.lawworkgroup.month"
      eParameterValues(0) = fEFTAccounting.GetMonthDiget
      eParameterNames(1) = "com.lawworkgroup.year"
      eParameterValues(1) = fEFTAccounting.cboYear
      eParameterNames(2) = "com.lawworkgroup.context.PolicyAccountingFolderName"
      eParameterValues(2) = fEFTAccounting.lblFolder.Caption
      eParameterNames(3) = "com.lawworkgroup.context.PolicyAccountingEventTypeId"
      eParameterValues(3) = fEFTAccounting.GetEventType

      Factory.ServiceService.InitService "PayplanAccounting", _
        "com.lawworkgroup.component.installment.spi.PayplanAccountingService", _
        vbNullString, _
        eParameterNames, eParameterValues, _
        5, True, True
      Unload fEFTAccounting
       
       
    Case 9 ' Quarterly Payment Plan Cycle
      Dim fPayplan As New frmPayplanDialog
      fPayplan.Show vbModal, Me
      If Not fPayplan.mOk Then
        Unload fPayplan
        Exit Sub
      End If
      
      Dim ParameterNames(1) As String
      Dim ParameterValues(1) As String
      ParameterNames(0) = "date"
      ParameterValues(0) = Format(fPayplan.dtpDate.Value, "yyyy-mm-dd")
      ParameterNames(1) = "InstallmentFolderName"
      ParameterValues(1) = fPayplan.lblFolder.Caption
      Factory.ServiceService.InitService "Payplan", _
        "com.lawworkgroup.component.installment.spi.PayplanCycleService", _
        vbNullString, _
        ParameterNames, ParameterValues, _
        5, True, True
      Unload fPayplan
    
    Case 10 ' Underwriting Crosstabs
      
      Factory.ServiceService.InitService "UnderwritingCrosstabs", _
        "com.wilmic.script.RefreshSnapshot", _
        vbNullString, vbNullStringArray, vbNullStringArray, _
        5, True, False
    
    Case 11 ' CLE Lawyer Advance
      
      Factory.ServiceService.InitService "LawyerCLEAdvance", _
        "com.wilmic.script.CLEEventLawyerAdvance", _
        vbNullString, vbNullStringArray, vbNullStringArray, _
        5, True, True
      
    Case 12 'Retrieve Online Applications
      OnlineAppUtil.retrieveOnlineApplications
      'exit early because it is not a true service
      Exit Sub
      
    Case Else
      Exit Sub
  End Select
  
  MsgBox "Service called!  Use service manager to monitor progress."
End Sub

Private Sub mnuToolsServiceManager_Click()
  Dispatcher.BrowseServiceManager
End Sub

Private Sub mnuToolsWebMerge_Click()
  Factory.EventService.WebMergeByView lvView
End Sub

Private Sub mnuToolsWorkflowOptions_Click(Index As Integer)
  FolderUtil.WorkflowShow mFolderKey, Index
End Sub

'
' Choose Columns.  Public because it is called from popup menu.
'

Public Sub mnuViewChooseColumns_Click()
  If ListViewModule.ColumnSettings(lvView, mView.Dataset.DataProvider.Source, lvView.Tag) Then
    LoadView
    lvView.Visible = True
  End If
End Sub

Private Sub mnuViewFolders_Click()
  mnuViewFolders.Checked = Not mnuViewFolders.Checked
  Form_Resize
  picView_Resize
  IniMain.WriteFile UrlName.MAIN_INI, "ViewFolders", mnuViewFolders.Checked
End Sub

Private Sub mnuViewGoToBack_Click()
  If mnuViewGoToBack.Enabled = False Then
    Exit Sub
  End If
  mSequence.ListIndex = mSequence.ListIndex + 1
  FolderKey = mSequence.Selected
  SequenceRefresh
End Sub

Private Sub mnuViewGoToForward_Click()
  If mnuViewGoToForward.Enabled = False Then
    Exit Sub
  End If
  mSequence.ListIndex = mSequence.ListIndex - 1
  FolderKey = mSequence.Selected
  SequenceRefresh
End Sub

Private Sub mnuViewGoToFolder_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowGoTo(mFolderKey) Then
    FolderLoad fForm.FolderKey
  End If
  Unload fForm
End Sub

Private Sub mnuViewGoToHome_Click()
  If mSequence Is Nothing Then
    Exit Sub
  End If
    
  Dim Count As Integer
  Count = mSequence.ListCount
  If Count > 0 Then
    mSequence.ListIndex = mSequence.ListCount - 1
    FolderKey = mSequence.Selected
    SequenceRefresh
  End If
End Sub

Private Sub mnuViewGoToStartMenu_Click()
  tbrStart.Buttons(1).Value = tbrPressed
  PopupMenu GotoMenu.mnuGoto, , cbrCoolBar.Left + picStart.Left + 24, _
        cbrCoolBar.Top + picStart.Top + cbrCoolBar.Bands(2).Height + 12
  tbrStart.Buttons(1).Value = tbrUnpressed
End Sub

Private Sub mnuViewGoToUpOneLevel_Click()
  'Hack to ensure that lvView does not have focus
  lvView.Visible = False
  FolderKey = tvFolders.Nodes.item(FolderKey).Parent.Key
End Sub

'
' Refresh - Public because called from popup
'
Public Sub mnuViewRefresh_Click()
  SearchNow
End Sub

'
' Serialize column headers
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView, lvView.Tag
End Sub

Private Sub mnuViewStatusBar_Click()
  mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
  Form_Resize
  IniMain.WriteFile UrlName.MAIN_INI, "ViewStatusbar", mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  Form_Resize
  IniMain.WriteFile UrlName.MAIN_INI, "ViewToolbar", mnuViewToolBar.Checked
End Sub

Private Sub mnuViewWebBrowser_Click()
  Browse HttpServer
End Sub


Private Sub picListView_KeyDown(KeyCode As Integer, Shift As Integer)
  lvView_KeyDown KeyCode, Shift
End Sub


Private Sub picListView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    MenuSubItem picListView.Left + X, picListView.Top + Y
  End If
End Sub

Private Sub picFolders_Resize()
  ' Do not resize when minimized
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If
      
  tvFolders.Height = picFolders.ScaleHeight
  tvFolders.Width = picFolders.ScaleWidth
End Sub


Private Sub picListView_Resize()
  ' Do not resize when minimized
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  lvView.Left = IIf(LocaleEnableWebContent = True, 1700, 0) 'original was 1500
  
  lvView.Height = Max(picListView.ScaleHeight - lvView.Top, 0)
  lvView.Width = Max(picListView.ScaleWidth - lvView.Left, 0)
  
  'determine general text top position
  lblListView(WEB_LABEL_GENERAL).Top = lblListView(WEB_LABEL_NAME).Height _
                                     + WEB_SPACER
  
  'determine preference note top position
  lblListView(WEB_LABEL_NOTE).Top = lblListView(WEB_LABEL_GENERAL).Top _
                                  + lblListView(WEB_LABEL_GENERAL).Height _
                                  + WEB_SPACER
  'vertically strech the note
  lblListView(WEB_LABEL_NOTE).Height = Max(picListView.ScaleHeight - lblListView(WEB_LABEL_NOTE).Top, 0)

  
  picSearchStatus.Top = lvView.Top + IIf(lvView.View = lvwReport, 252, 0)
  picSearchStatus.Left = lvView.Left
  picSearchStatus.Width = lvView.Width
End Sub

Private Sub picRemoveFolders_Click()
  mnuViewFolders_Click
End Sub

Private Sub picStart_Resize()
  tbrStart.Top = (picStart.Height - tbrStart.Height) / 2
  tbrBack.Top = (picNavigation.Height - tbrBack.Height) / 2
  tbrForward.Top = (picNavigation.Height - tbrForward.Height) / 2
End Sub

Private Sub picToolBar_Resize()
  tbToolBar.Top = (picToolBar.Height - tbToolBar.Height) / 2
End Sub

Private Sub picView_Resize()
  ' Do not resize on minimize
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If
  
  Dim BodyLeft As Integer
  If fraFolders.Visible Then
    fraFolders.Height = picView.Height
    picFolders.Height = Max(picView.Height - picFolders.Top, 0)
    imgSplitter.Left = fraFolders.Width
    imgSplitter.Height = picView.Height
    BodyLeft = imgSplitter.Left + imgSplitter.Width
  Else
    BodyLeft = 0
  End If
  
  Select Case mArrangement
  Case faMain
      picMain.Top = 96
      picMain.Left = BodyLeft
      picMain.Height = Max(picView.Height - picMain.Top, 0)
      picMain.Width = Max(picView.Width - picMain.Left, 0)
      fraLookFor.Visible = False
    Case faListView
      If fraSearch.Visible Then
        fraSearch.Left = BodyLeft
        fraSearch.Width = Max(picView.Width - fraSearch.Left, 0)
        picWords.Width = fraSearch.Width
        fraLookFor.Left = 224 'original was 24
        fraLookFor.Top = cmdSearchNow.Top
        fraLookFor.Visible = True
      Else
        fraLookFor.Visible = False
      End If
      
      picListView.Top = picView.Top + IIf(fraSearch.Visible, fraSearch.Height + 24, 96)
      picListView.Left = picView.Left + BodyLeft
      picListView.Width = Max(picView.Width - BodyLeft, 0)
      picListView.Height = Max(picView.Top + picView.Height - picListView.Top, 0)
  End Select
  
End Sub


Private Sub tbrBack_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuViewGoToBack_Click
End Sub

Private Sub tbrForward_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuViewGoToForward_Click
End Sub

Private Sub tbrStart_ButtonClick(ByVal Button As MSComctlLib.Button)
  cmdMenu_Click
End Sub

Private Sub tbrBack_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  Dim I As Integer
  tbrBack.Buttons(1).ButtonMenus.Clear
  For I = mSequence.ListIndex + 1 To mSequence.ListCount - 1
    tbrBack.Buttons(1).ButtonMenus.Add , , mSequence.List(I)
  Next I
 
End Sub

Private Sub tbrBack_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mSequence.ListIndex = ButtonMenu.Index + mSequence.ListIndex
  FolderKey = mSequence.Selected
  SequenceRefresh
  ' = ButtonMenu.Index + mSequence.ListIndex
  'lboList.ListIndex = ButtonMenu.Index + lboList.ListIndex
  'mSequence.ListIndex = lboList.ListIndex
  'SequenceRefresh
End Sub
Private Sub tbrForward_ButtonDropDown(ByVal Button As MSComctlLib.Button)
  Dim I As Integer
  tbrForward.Buttons(1).ButtonMenus.Clear
  For I = 0 To mSequence.ListIndex - 1
    tbrForward.Buttons(1).ButtonMenus.Add , , mSequence.List(I)
  Next I
End Sub

Private Sub tbrForward_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  mSequence.ListIndex = ButtonMenu.Index - 1
  FolderKey = mSequence.Selected
  SequenceRefresh
  'lboList.ListIndex = ButtonMenu.Index - 1
End Sub

Private Sub tbrUpOneLevel_ButtonClick(ByVal Button As MSComctlLib.Button)
  mnuViewGoToUpOneLevel_Click
End Sub

'
' This is a rare case where the ToolBar is the master for functionality.
' Usually, menus contain the functionality
'
Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "New"
      If mnuFileNewItem.Visible And mnuFileNewItem.Enabled Then
        mnuFileNewItem_Click
      End If
    Case "Open"
      cmdDefault_Click
    Case "Print"
      PrintUtil.PrintListView lvView, SelectedCount(lvView.hWnd) > 0
    Case "Move"
      If mnuEditMove.Visible And mnuEditMove.Enabled Then
        mnuEditMove_Click
      End If
    Case "Delete"
      If mnuFileDelete.Visible And mnuFileDelete.Enabled Then
        mnuFileDelete_Click
      End If
    Case "lvMode0"
      lvView.View = lvwIcon
      picListView_Resize
    Case "lvMode1"
      lvView.View = lvwSmallIcon
      picListView_Resize
    Case "lvMode2"
      lvView.View = lvwList
      picListView_Resize
    Case "lvMode3"
      lvView.View = lvwReport
      picListView_Resize
  End Select
End Sub


Private Sub tmrFolderList_Timer()
  tmrFolderList.Enabled = False
  If Not tvFolders.SelectedItem Is Nothing Then
    FolderKey = tvFolders.SelectedItem.Key
  End If
End Sub

Private Sub tmrSearch_Timer()
  tmrSearch.Enabled = False
  If Len(txtWords.Text) > 2 And lastSearch <> txtWords.Text Then
    cmdSearchNow_Click
  End If
End Sub

'
' Called after label edit AND the label was changed.
'
Private Sub tvFolders_AfterLabelEdit(Cancel As Integer, NewString As String)
  If Not DirNameSafe(NewString) Then
    Cancel = True
    Exit Sub
  End If
'''  If tvFolders.SelectedItem.Key = SystemFolders_RootKey Then
'''    SystemFolders_RootName = NewString
'''    PreferenceUtil.renameIniNode SystemFolders_TreeName, SystemPreferences_RootName
'''  Else
    Cancel = Not Factory.PreferenceService.RenameFolder(SystemFolders_TreeName, tvFolders.SelectedItem.Key, NewString)
'''  End If
End Sub

Private Sub tvFolders_GotFocus()
  mnuFileDelete.Visible = True
  mnuFileRename.Visible = True
  
  mnuFileProperties.Enabled = True
End Sub

Private Sub tvFolders_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF10 And (Shift And vbShiftMask) > 0 Then
    tmrFolderList_Timer
    PopupMenu Popup.mnuPopupFolderList, , picFolders.Left + picView.Left + 24, picFolders.Top + picView.Top + 24
    KeyCode = 0
  End If
  
  'Trap F6 for folders w/dataset.
  If KeyCode = vbKeyF6 Then
    tmrFolderList_Timer
    If (Shift And vbShiftMask) > 0 Then
      If picListView.Visible Then
        lvView.SetFocus
      End If
    Else
      If fraSearch.Visible Then
        txtWords.SetFocus
      ElseIf picListView.Visible Then
        lvView.SetFocus
      End If
    End If
  End If
End Sub

Private Sub tvFolders_LostFocus()
  mnuFileDelete.Visible = False
  mnuFileRename.Visible = False
  mnuFileProperties.Enabled = False
  
  'MsgBox ActiveControl.Name
End Sub

Private Sub tvFolders_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  tmrFolderList_Timer
  If Button = vbRightButton Then
    PopupMenu Popup.mnuPopupFolderList
  End If
End Sub
  
'
' Reset timer on any node click
'
Private Sub tvFolders_NodeClick(ByVal Node As MSComctlLib.Node)
  tmrFolderList.Enabled = False
  tmrFolderList.Enabled = True
End Sub

Private Sub txtWords_Click()
  cmdSearchNow.Default = True
End Sub

Private Sub txtWords_DropDown()
  tmrSearch.Enabled = False
End Sub

Private Sub txtWords_GotFocus()
  ComboBoxUtil.Deserialize txtWords, cboIn.Text, IniSearch, mWordCache
  cmdSearchNow.Default = True
End Sub

Private Sub txtWords_KeyDown(KeyCode As Integer, Shift As Integer)
  If ComboBoxUtil.IsDropped(txtWords) Then
    Exit Sub
  End If
  
  If (KeyCode = vbKeyDown Or KeyCode = vbKeyUp) And (Shift And vbShiftMask) = 0 And (Shift And vbAltMask) = 0 Then
    SendMessage lvView.hWnd, WM_KEYDOWN, KeyCode, 0
    KeyCode = 0
    Exit Sub
  End If

  'Trap F6
  If View.Dataset Is Nothing Then Exit Sub
  If KeyCode = vbKeyF6 And (Shift And vbShiftMask) > 0 Then
    tvFolders.SetFocus
  ElseIf KeyCode = vbKeyF6 And (Shift And vbShiftMask) = 0 Then
    lvView.SetFocus
  End If
End Sub

Private Sub txtWords_KeyPress(KeyAscii As Integer)
  If ComboBoxUtil.IsDropped(txtWords) Then
    Exit Sub
  End If
  
  tmrSearch.Enabled = False
  tmrSearch.Enabled = True
  cmdSearchNow.Default = True
End Sub






'
' FOLDER WATCH UTILITY
'

Public Property Let FolderWatchHandle(FolderId As String)
   mFolderWatchHandle = FolderId
  'start the monitoring when the folderId is not null
  If mFolderWatchHandle <> vbNullString Then
    mFolderName = Factory.PreferenceService.getNodeName(mFolderWatchHandle)
    If mFolderName = vbNullString Then
      MsgBox "There has been an error with loading the folder watch utility. It is possible that the folder previously being watched has been removed. Please select a new folder.", vbCritical
      mFolderWatchHandle = vbNullString
      Exit Property
    End If
        
    'Save folderId to ini
    IniMain.WriteFile UrlName.MAIN_INI, UrlName.FOLDERWATCHKEY_INI, mFolderWatchHandle
    Dim folderView As View
    'enable the GoTo Folder dropdown menu
    tbrFolderWatch.Buttons(1).ButtonMenus(1).Enabled = True
    FolderWatchRefresh
    tmrFolderWatch.Enabled = True
  End If
End Property

Public Property Get FolderWatchHandle() As String
  FolderWatchHandle = mFolderWatchHandle
End Property

Private Sub FolderWatchInit()
  'set the interval '1 second = 1,000 Milliseconds
  tmrFolderWatch.Interval = 60000 'checks every minute. Note: VB timers can't go longer than 65 seconds
  'Pull FolderWatchHandle from law.ini
  FolderWatchHandle = IniMain.ReadFile(UrlName.MAIN_INI, UrlName.FOLDERWATCHKEY_INI)
End Sub

Private Sub tbrFolderWatch_ButtonClick(ByVal Button As MSComctlLib.Button)
  'If the main button is pressed and no folder is selected show the folder select
  If FolderWatchHandle = vbNullString Then
    FolderWatchSelectFolder
    Exit Sub
  End If
  'If folder already exists then browse to that folder
  FolderKey = FolderWatchHandle
  
End Sub

Private Sub tbrFolderWatch_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Index
    Case 1:
      'Go To folder
      FolderKey = FolderWatchHandle
    Case 2:
      FolderWatchSelectFolder
  End Select
End Sub

Private Sub tmrFolderWatch_Timer()
  FolderWatchRefresh
End Sub


Private Sub FolderWatchSelectFolder()
  ' Create form and show.
  Dim FolderSelect As New frmFolderBrowseDialog
  If Not FolderSelect.ShowSelect Then
    Unload FolderSelect
    Exit Sub
  End If
  FolderWatchHandle = FolderSelect.FolderKey
End Sub


Private Sub FolderWatchRefresh()
  'See if the View is loaded into views, if not load it.
  If Not Views.ContainsKey(FolderWatchHandle) Then
    AddViewByKey FolderWatchHandle
  End If

  'call to check the folder
  Dim WatchDataset As Dataset
  
  Dim tmpView As View
  Set tmpView = Views(FolderWatchHandle)
  'validate configuration
  If tmpView Is Nothing Then
    Exit Sub
  End If
  
  Set WatchDataset = tmpView.Dataset
  
  WatchDataset.DataProvider.filter = vbNullString
  WatchDataset.DataProvider.OpenRecordset
  Dim results As Collection
  Set results = WatchDataset.DataProvider.Collection
  ' update count display
  With tbrFolderWatch.Buttons(1)
    If results Is Nothing Then
      .Image = 1
      .Caption = mFolderName
    Else
      .Image = 2
      .Caption = mFolderName & " (" & results.Count & ")"
    End If
  End With
End Sub

