Attribute VB_Name = "DatabaseName"
'2005 01 31
'2004 03 15

Option Explicit

'
' Tables and Keys
'
Public Const ANSWER_TABLE = "Answer"

Public Const APPLICATION_KEY = "applicationId"
Public Const APPLICATION_TABLE = "Application"
Public Const APPLICATION_REF = "ApplicationReference"
Public Const APPLICATION_VIEW = "ApplicationView"

Public Const APPLICATIONLAWAREA_TABLE = "ApplicationLawArea"
Public Const APPLICATIONLAWAREA_VIEW = "ApplicationLawAreaView"

Public Const APPLICATIONLAWYER_TABLE = "ApplicationLawyer"
Public Const APPLICATIONLAWYER_VIEW = "ApplicationLawyerView"

Public Const APPLICATIONLIMIT_KEY = "applicationLimitId"
Public Const APPLICATIONLIMIT_TABLE = "ApplicationLimit"
Public Const APPLICATIONLIMIT_VIEW = "ApplicationLimitView"

'Public Const APPLICATIONRATING_KEY = "applicationRatingId"
'Public Const APPLICATIONRATING_TABLE = "ApplicationRating"
Public Const APPLICATIONRATING_REF = "ApplicationRatingRef"
'Public Const APPLICATIONRATINGVIEW_TABLE = "ApplicationRatingView"

Public Const APPLICATIONQUESTION_TABLE = "ApplicationQuestion"
Public Const APPLICATIONQUESTION_VIEW = "ApplicationQuestionView"

Public Const ATTRIBUTE_KEY = "attributeId"
Public Const ATTRIBUTE_TABLE = "Attribute"

Public Const CLAIM_KEY = "claimId"
Public Const CLAIM_REF = "ClaimReference"
Public Const CLAIM_TABLE = "Claim"
Public Const CLAIM_VIEW = "ClaimView"
Public Const CLAIM_DOCUMENT = "ClaimDocument"

Public Const CLAIMCOVERAGE_TABLE = "ClaimCoverage"
Public Const CLAIMCOVERAGE_VIEW = "ClaimCoverageView"

'Public Const CLAIMENDORSEMENT_TABLE = "ClaimEndorsement"

'Public Const CLAIMLAWYER_TABLE = "ClaimLawyer"
'Public Const CLAIMLAWYER_VIEW = "ClaimLawyerView"
Public Const CLAIMLAWYERCOVERAGE_VIEW = "ClaimLawyerCoverageView"

Public Const CLAIMPAYMENT_KEY = "claimPaymentId"
Public Const CLAIMPAYMENT_TABLE = "ClaimPayment"
Public Const CLAIMPAYMENT_VIEW = "ClaimPaymentView"

'Public Const CLAIMLAWAREA_TABLE = "ClaimLawArea"
'Public Const CLAIMLAWAREA_VIEW = "ClaimLawAreaView"

'Public Const CLAIMLAWERROR_TABLE = "ClaimLawError"
'Public Const CLAIMLAWERROR_VIEW = "ClaimLawErrorView"

Public Const CLAIMRESERVE_TABLE = "ClaimReserve"
Public Const CLAIMRESERVE_KEY = "claimReserveId"
Public Const CLAIMRESERVE_VIEW = "ClaimReserveView"

Public Const CLEEVENT_KEY = "cleEventId"
Public Const CLEEVENT_TABLE = "CLEEvent"

Public Const CLEEVENTLAWYER_TABLE = "CLEEventLawyer"
Public Const CLEEVENTLAWYER_VIEW = "CLEEventLawyerView"

Public Const COMMUNICATIONTYPE_KEY = "communicationTypeId"
Public Const COMMUNICATIONTYPE_TABLE = "CommunicationType"

Public Const CONTACT_KEY = "contactId"
Public Const CONTACT_TABLE = "Contact"
Public Const CONTACT_VIEW = "ContactView"

Public Const CONTACTENTITY_KEY = "contactEntityId"

Public Const CONTACTTYPE_KEY = "contactTypeId"
Public Const CONTACTTYPE_TABLE = "ContactType"

Public Const COVERAGE_KEY = "coverageId"
Public Const COVERAGE_TABLE = "Coverage"
Public Const COVERAGE_VIEW = "CoverageView"
Public Const COVERAGE_REF = "CoverageReference"

Public Const COVERAGETYPE_KEY = "coverageTypeId"
Public Const COVERAGETYPE_TABLE = "CoverageType"

Public Const COVERAGELAWYER_TABLE = "CoverageLawyer"
Public Const COVERAGELAWYER_VIEW = "CoverageLawyerView"

Public Const DISBURSEMENT_KEY = "disbursementId"
Public Const DISBURSEMENT_TABLE = "Disbursement"
Public Const DEDUCTIBLEREIMBURSE_KEY = "deductibleReimburseId"
Public Const DEDUCTIBLEREIMBURSE_TABLE = "DeductibleReimburse"
Public Const DEDUCTIBLEREIMBURSE_REF = "DeductibleReimburseRef"
Public Const DEDUCTIBLEREIMBURSE_VIEW = "DeductibleReimburseView"

Public Const EFTFILE_TABLE = "EFTFile"
Public Const EFTFILE_KEY = "eftFileId"

Public Const EFTINDIVIDUAL_TABLE = "EFTIndividual"
Public Const EFTINDIVIDUAL_KEY = "eftIndividualId"

Public Const EFTDEPOSITFINANCIALINST_KEY = "eftDepositFinancialInstId"
Public Const EFTDEPOSITFINANCIALINST_TABLE = "EFTDepositFinancialInst"

Public Const EFTTRANSACTIONCODE_KEY = "eftTransactionCodeId"
Public Const EFTTRANSACTIONCODE_TABLE = "EFTTransactionCode"

Public Const ENDORSEMENT_REF = "EndorsementReference"
Public Const ENDORSEMENT_TABLE = "Endorsement"
Public Const ENDORSEMENT_VIEW = "EndorsementView"
Public Const ENDORSEMENT_KEY = "endorsementId"

Public Const ENDORSEMENTTYPE_KEY = "endorsementTypeId"
Public Const ENDORSEMENTTYPE_TABLE = "EndorsementType"

Public Const ENTITY_KEY = "entityId"
Public Const ENTITY_TABLE = "Entity"
Public Const ENTITY_VIEW = "EntityView"

Public Const ENTITYLOCATION_VIEW = "EntityLocationView"

Public Const EVENT_KEY = "eventId"
Public Const EVENT_TABLE = "Event"
Public Const EVENT_VIEW = "EventView"

Public Const EVENTATTRIBUTE_TABLE = "EventAttribute"

Public Const EVENTGROUP_TABLE = "EventGroup"

Public Const EVENTVIEW_TABLE = "EventView"

Public Const EVENTTYPE_KEY = "eventTypeId"
Public Const EVENTTYPE_TABLE = "EventType"
Public Const EVENTTYPERULE_TABLE = "EventTypeRule"
Public Const EVENTTYPERULE_VIEW = "EventTypeRuleView"

Public Const EVENTRECIPIENT_KEY = "eventRecipientId"
Public Const EVENTRECIPIENT_TABLE = "EventRecipient"
Public Const EVENTRECIPIENT_VIEW = "EventRecipientView"

Public Const EVENTREFERENCE_KEY = "referenceEventId"
Public Const EVENTREFERENCE_TABLE = "EventReference"
Public Const EVENTREFERENCE_VIEW = "EventReferenceView"

Public Const EVENTRULE_KEY = "eventRuleId"

Public Const FIRM_KEY = "firmId"
Public Const FIRM_TABLE = "Firm"
Public Const FIRM_REF = "FirmReference"
Public Const FIRM_DOCUMENT = "FirmDocument"
Public Const FIRM_VIEW = "FirmView"

Public Const FIRMINSURER_TABLE = "FirmInsurer"
Public Const FIRMINSURER_VIEW = "FirmInsurerView"

Public Const FIRMLAWYER_VIEW = "FirmLawyerView"
Public Const FIRMLAWYER_TABLE = "FirmLawyer"

Public Const FIRMPOLICY_TABLE = "FirmPolicy"
Public Const FIRMPOLICY_VIEW = "FirmPolicyView"

Public Const FISCALCALENDAR_TABLE = "FiscalCalendar"

Public Const FOLDER_TABLE = "SysFolder"

Public Const GLACCOUNT_KEY = "glAccountId"
Public Const GLACCOUNT_TABLE = "GLAccount"
Public Const GLACCOUNTGROUP_VIEW = "GLAccountGroupView"
Public Const GLBATCH_KEY = "glBatchId"
Public Const GLBATCH_TABLE = "GLBatch"
Public Const GLENTRY_KEY = "glEntryId"
Public Const GLENTRY_TABLE = "GLEntry"
Public Const GLTRANSACTION_KEY = "glTransactionId"
Public Const GLTRANSACTION_TABLE = "GLTransaction"
Public Const GLTRANSACTION_VIEW = "GLTransactionView"

Public Const GROUP_KEY = "groupId"

Public Const INCIDENT_KEY = "incidentId"
Public Const INCIDENT_TABLE = "Incident"
Public Const INCIDENT_VIEW = "IncidentView"

Public Const INSTALLMENT_KEY = "installmentId"
Public Const INSTALLMENT_TABLE = "Installment"
Public Const INSTALLMENT_VIEW = "InstallmentView"
Public Const INSTALLMENTCHILD_VIEW = "InstallmentChildView"

Public Const INSTALLMENTPOLICYPAYMENT_TABLE = "InstallmentPolicyPayment"
Public Const INSTALLMENTPOLICYPAYMENT_VIEW = "InstallmentPolicyPaymentView"

Public Const INSTANCE_KEY = "instanceId"
Public Const INSTANCE_TABLE = "Instance"
Public Const INSTANCE_VIEW = "InstanceView"
Public Const INSTANCELAWAREA_TABLE = "InstanceLawArea"
Public Const INSTANCELAWAREA_VIEW = "InstanceLawAreaView"
Public Const INSTANCELAWERROR_TABLE = "InstanceLawError"
Public Const INSTANCELAWERROR_VIEW = "InstanceLawErrorView"
Public Const INSTANCELAWYER_TABLE = "InstanceLawyer"
Public Const INSTANCELAWYER_VIEW = "InstanceLawyerView"
Public Const INSTANCEPAYMENT_KEY = "instancePaymentId"
Public Const INSTANCEPAYMENT_TABLE = "InstancePayment"
Public Const INSTANCEPAYMENT_VIEW = "InstancePaymentView"

Public Const INSURER_KEY = "insurerId"
Public Const INSURER_TABLE = "Insurer"
Public Const INSURER_VIEW = "InsurerView"

Public Const INVOICE_KEY = "invoiceId"
Public Const INVOICE_TABLE = "Invoice"
Public Const INVOICE_VIEW = "InvoiceView"

Public Const LAWAREA_KEY = "lawAreaId"
Public Const LAWAREA_TABLE = "LawArea"

Public Const LAWYERDESIGNATION_TABLE = "LawyerDesignation"
Public Const LAWYERDESIGNATION_KEY = "lawyerDesignationId"

Public Const LAWDISPOSITION_KEY = "lawDispositionId"
Public Const LAWDISPOSITION_TABLE = "LawDisposition"

Public Const LAWERROR_KEY = "lawErrorId"
Public Const LAWERROR_TABLE = "LawError"

Public Const LAWYER_TABLE = "Lawyer"
Public Const LAWYER_REF = "LawyerReference"
Public Const LAWYER_KEY = "lawyerId"
Public Const LAWYER_VIEW = "LawyerView"

Public Const LAWYERSTATEBAR_TABLE = "LawyerStateBar"

Public Const LISTENER_KEY = "listenerId"
Public Const LISTENER_TABLE = "SysListener"

Public Const LOCATION_KEY = "locationId"
Public Const LOCATION_TABLE = "Location"
Public Const LOCATION_REF = "LocationReference"
Public Const LOCATION_VIEW = "LocationView"

Public Const LOCATIONTYPE_KEY = "locationTypeId"
Public Const LOCATIONTYPE_TABLE = "LocationType"

Public Const ORGANIZATION_KEY = "organizationId"
Public Const ORGANIZATION_TABLE = "Organization"
Public Const ORGANIZATION_VIEW = "OrganizationView"

Public Const PARAMETER_KEY = "parameterId"

Public Const PERSON_KEY = "personId"
Public Const PERSON_TABLE = "Person"
Public Const PERSON_VIEW = "PersonView"
Public Const PERSON_REF = "PersonReference"

Public Const PERSONATTRIBUTE_TABLE = "PersonAttribute"
Public Const PERSONATTRIBUTE_VIEW = "PersonAttributeView"

Public Const POLICY_KEY = "policyId"
Public Const POLICY_REF = "PolicyReference"
Public Const POLICY_TABLE = "Policy"
Public Const POLICY_VIEW = "PolicyView"

Public Const POLICYACCOUNTING_KEY = "policyAccountingId"
Public Const POLICYACCOUNTING_TABLE = "PolicyAccounting"
Public Const POLICYACCOUNTING_VIEW = "PolicyAccountingView"

Public Const POLICYLAWYER_TABLE = "PolicyLawyer"
Public Const POLICYLAWYERCOVERAGEVIEW_TABLE = "PolicyLawyerCoverageView"

Public Const PREFERENCE_TABLE = "Preference"
Public Const PREFERENCE_KEY = "preferenceId"
Public Const PREFERENCEENTRY_TABLE = "PreferenceEntry"

Public Const PUBLISHER_KEY = "publisherId"

Public Const QUESTION_KEY = "questionId"
Public Const QUESTION_TABLE = "Question"

Public Const QUOTE_KEY = "quoteId"
Public Const QUOTE_TABLE = "Quote"
Public Const QUOTE_VIEW = "QuoteView"

Public Const QUOTECOVERAGE_KEY = "quoteCoverageId"
Public Const QUOTECOVERAGE_TABLE = "QuoteCoverage"
Public Const QUOTECOVERAGE_VIEW = "QuoteCoverageView"

Public Const QUOTEBOOK_KEY = "quoteBookId"
Public Const QUOTEBOOK_TABLE = "QuoteBook"

Public Const QUOTELAWYER_KEY = "quoteLawyerId"
Public Const QUOTELAWYER_TABLE = "QuoteLawyer"
Public Const QUOTELAWYER_VIEW = "QuoteLawyerView"

Public Const RATING_TABLE = "Rating"

Public Const RATINGBOOK_TABLE = "RatingBook"
Public Const RATINGBOOK_KEY = "ratingBookId"

Public Const RATINGBOOKMETRIC_TABLE = "RatingBookMetric"
Public Const RATINGBOOKMETRICCALC_TABLE = "RatingBookMetricCalc"
Public Const RATINGBOOKRATE_TABLE = "RatingBookRate"
Public Const RATINGLAWYER_TABLE = "RatingLawyer"
Public Const RatingLIMIT_TABLE = "RatingLimit"

Public Const RATINGMETRIC_TABLE = "RatingMetric"
Public Const RATINGMETRIC_KEY = "ratingMetricId"
Public Const RATINGMETRICIDCALC_KEY = "ratingMetricIdCalc"
Public Const RATINGRATE_TABLE = "RatingRate"
Public Const RATINGRATE_KEY = "ratingRateId"


' TEMP ENTIRES FOR OLD SYSTEM
Public Const REFERENCE_KEY = "referenceEventId"
Public Const REFERENCE_TABLE = "EventReference"
Public Const REFERENCEVIEW_TABLE = "EventReferenceView"

Public Const RECEIPT_KEY = "receiptId"
Public Const RECEIPT_TABLE = "Receipt"

Public Const REINSURANCETREATY_KEY = "reinsuranceTreatyId"
Public Const REINSURANCETREATY_TABLE = "ReinsuranceTreaty"

Public Const RULE_TABLE = "Rule"
Public Const RULE_KEY = "ruleId"

Public Const STATE_TABLE = "State"
Public Const STATE_KEY = "stateId"
Public Const STATECITY_VIEW = "StateCityView"

'Used with LawyerStateBar and Lawyer table
Public Const STATEBAR_KEY = "stateBarId"

Public Const SYSFOLDERACTIONPARAM_TABLE = "SysFolderActionParam"

Public Const SYSGROUPACTIONPARAM_TABLE = "SysGroupActionParam"

Public Const SYSLISTENER_TABLE = "SysListener"
Public Const SYSLISTENERACTIONPARAM_TABLE = "SysListenerActionParam"
Public Const SYSLISTENERPARAMETER_TABLE = "SysListenerParameter"

Public Const SYSPARAMETER_TABLE = "SysParameter"
Public Const SYSPUBLISHER_TABLE = "SysPublisher"
Public Const SYSPUBLISHERLISTENER_TABLE = "SysPublisherListener"

Public Const TABLECOLUMNTYPE_TABLE = "TableColumnType"
Public Const TREATYLAYER_TABLE = "TreatyLayer"
Public Const TREATYLAYER_KEY = "treatyLayerId"

Public Const VENDOR_TABLE = "Vendor"
Public Const VENDOR_Key = "vendorId"



'
' Field Names
'
Public Const ACCOUNTNAME_FIELD = "accountName"
Public Const ACCOUNTNUMBER_FIELD = "accountNumber"
Public Const ADDRESSLINE1_FIELD = "addressLine1"
Public Const ADDRESSLINE2_FIELD = "addressLine2"
Public Const ADDRESSLINE3_FIELD = "addressLine3"
Public Const ADJUSTER_FIELD = "adjuster"
Public Const AGGREGATELIMIT_FIELD = "aggregateLimit"
Public Const ANSWERAMOUNT_FIELD = "answerAmount"
Public Const ANSWERDATE_FIELD = "answerDate"
Public Const ANSWERNOTE_FIELD = "answerNote"
Public Const ATTENDINGSTAFF_FIELD = "attendingStaff"

Public Const BARADMITDATE_FIELD = "barAdmitDate"

Public Const CANCELEFFECTIVEDATE_FIELD = "cancelEffectiveDate"
Public Const CARRIERROUTE_FIELD = "carrierRoute"
Public Const CHECKNUMBER_FIELD = "checkNumber"
Public Const CITY_FIELD = "city"
Public Const CLAIMMADEDATE_FIELD = "claimMadeDate"
Public Const CLAIMANT_FIELD = "claimant"
Public Const CLAIMNAME_FIELD = "claimName"
Public Const CLAIMNUMBER_FIELD = "claimNumber"
Public Const CLAIMTYPE_FIELD = "claimType"
Public Const CLOSEDATE_FIELD = "closeDate"
Public Const COLUMN_FIELD = "column"
Public Const COMPANYNAME_FIELD = "companyName"
Public Const COMPLETEDATE_FIELD = "completeDate"
Public Const COMPLETEUSERID_FIELD = "completeUserId"
Public Const CONTACTNAME_FIELD = "contactName"
Public Const CONTACTSALUTATION_FIELD = "contactSalutation"
Public Const CONTACTTYPENAME_FIELD = "contactTypeName"
Public Const COUNTY_FIELD = "county"
Public Const COUNTRY_FIELD = "country"
Public Const COURT_FIELD = "court"
Public Const COURTCASENUM_FIELD = "courtCaseNum"
Public Const COVERAGEDATE_FIELD = "coverageDate"
Public Const COVERAGETYPENAME_FIELD = "coverageTypeName"
Public Const CREATEDATE_FIELD = "createDate"

Public Const DESCRIPTION_FIELD = "description"
Public Const DEDUCTIBLEALAE_FIELD = "deductibleALAE"
Public Const DEDUCTIBLEADVANCE_FIELD = "deductibleAdvance"
Public Const DEDUCTIBLEDIRECT_FIELD = "deductibleDirect"
Public Const DEDUCTIBLEINDEMNITY_FIELD = "deductibleIndemnity"
Public Const DEDUCTIBLEREIMBURSE_FIELD = "deductibleReimburse"
Public Const DEDUCTIBLEWRITEOFF_FIELD = "deductibleWriteOff"
Public Const DELIVERYADDRESS_FIELD = "deliveryAddress"
Public Const DELIVERYPOINTCODE_FIELD = "deliveryPointCode"
'Public Const DISPLAYNAME_FIELD = "displayName"
Public Const DOCUMENTFILENAME_FIELD = "documentFileName"
Public Const DUEDATE_FIELD = "dueDate"
Public Const DUEUSERID_FIELD = "dueUserId"


Public Const EFFECTIVEDATE_FIELD = "effectiveDate"
Public Const EMAIL_FIELD = "email"
Public Const ENTITYLOCATOR_FIELD = "entityLocator"
Public Const ENTITYNAME_FIELD = "entityName"
Public Const ENTITYURL_FIELD = "entityURL"
Public Const ENTRYDATE_FIELD = "entryDate"

'PreferenceEntry fields
Public Const ENTRYID_FIELD = "entryId"
Public Const ENTRYVALUE_FIELD = "entryValue"

Public Const ESTABLISHDATE_FIELD = "establishDate"
Public Const EVENTAMOUNT_FIELD = "eventAmount"
Public Const EVENTDATE_FIELD = "eventDate"
Public Const EVENTDESC_FIELD = "eventDesc"
Public Const EVENTGROUPID_FIELD = "eventGroupId"
Public Const EVENTLOCATOR_FIELD = "eventLocator"
Public Const EVENTNAME_FIELD = "eventName"
Public Const EVENTNOTE_FIELD = "eventNote"
Public Const EVENTSTATUS_FIELD = "eventStatus"
Public Const EVENTTYPELOCATOR_FIELD = "eventTypeLocator"
Public Const EVENTTYPENAME_FIELD = "eventTypeName"
Public Const EVENTTYPERULEID_FIELD = "eventTypeRuleId"
Public Const EXPIRATIONDATE_FIELD = "expirationDate"

Public Const FAX_FIELD = "fax"
Public Const FEDERALTAXNUMBER_FIELD = "federalTaxNumber"
Public Const FINANCECHARGE_FIELD = "financeCharge" 'Found in InvoiceView
Public Const FIRMENTITYLOCATOR_FIELD = "firmEntityLocator"
Public Const FIRMENTITYNAME_FIELD = "firmEntityName"
Public Const FIRMNAME_FIELD = "firmName"
Public Const FIRMSERVEDATE_FIELD = "firmServeDate"
Public Const FIRSTNAME_FIELD = "firstName"
 
Public Const FOLDER_KEY = "folderId"
Public Const FOLDERNAME_FIELD = "folderName"

Public Const GROUPNAME_FIELD = "groupName"

Public Const HOMEEMAIL_FIELD = "homeEmail"
Public Const HOMETELEPHONE_FIELD = "homeTelephone"

Public Const INCEPTIONDATE_FIELD = "inceptionDate"
Public Const INCIDENTNUMBER_FIELD = "incidentNumber"
Public Const INDIVIDUALNAME_FIELD = "individualName"
Public Const INPRACTICEDATE_FIELD = "inPracticeDate"
Public Const INPRACTICEDURATION_FIELD = "inPracticeduration"
Public Const INSTITUTIONNAME_FIELD = "institutionName"
Public Const INSTITUTIONCITY_FIELD = "institutionCity"
Public Const INSURERENTITYLOCATOR_FIELD = "insurerEntityLocator"
Public Const INSURERENTITYNAME_FIELD = "insurerEntityName"
Public Const INSUREDSERVEDATE_FIELD = "insuredServeDate"
Public Const INSURERSERVEDATE_FIELD = "insurerServeDate"
Public Const ISANSWER_FIELD = "isAnswer"
Public Const ISCLOSED_FIELD = "isClosed"
Public Const ISENVELOPE_FIELD = "isEnvelope"
Public Const ISFEEDISPUTE_FIELD = "isFeeDispute"
Public Const ISJUDGMENTCLAIMANT_FIELD = "isJudgmentClaimant"
Public Const ISPRENOTESENT_FIELD = "isPrenoteSent"
Public Const ISREINSURERREPORT_FIELD = "isReinsurerReport"
Public Const ISSUEDATE_FIELD = "issueDate"
Public Const ISTAIL_FIELD = "isTail"
Public Const ISUNINSURABLE_FIELD = "isUninsurable"
                                       
Public Const KNOWDATE_FIELD = "knowDate"
Public Const KNOWDATERANGE_FIELD = "knowDateRange"

Public Const LASTNAME_FIELD = "lastName"
Public Const LATECHARGE_FIELD = "lateCharge"  'Virtual field found in InvoiceView
Public Const LATESTAPPLICATIONID_FIELD = "latestApplicationId"
Public Const LATESTFIRMID_FIELD = "latestFirmId"
Public Const LATESTINSURERID_FIELD = "latestInsurerId"
Public Const LATESTPOLICYID_FIELD = "latestPolicyId"
Public Const LATESTPOLICYSTATUS_FIELD = "latestPolicyStatus"
Public Const LAWAREANAME_FIELD = "lawAreaName"
Public Const LAWAREAPERCENT_FIELD = "lawAreaPercent"
Public Const LAWAREASEQ_FIELD = "lawAreaSeq"
Public Const LAWDISPOSITIONNAME_FIELD = "lawDispositionName"
Public Const LAWERRORNAME_FIELD = "lawErrorName"
Public Const LAWERRORSEQ_FIELD = "lawErrorSeq"
Public Const LAWYERDESIGNATIONNAME_FIELD = "lawyerDesignationName"
Public Const LAWYERENTITYLOCATOR_FIELD = "lawyerEntityLocator"
Public Const LAWYERENTITYNAME_FIELD = "lawyerEntityName"
Public Const LAWYERNAME_FIELD = "lawyerName"
Public Const LAWYERPRINCIPALCOUNT_FIELD = "lawyerPrincipalCount"
Public Const LAWYEROFCOUNSELCOUNT_FIELD = "lawyerOfCounselCount"
Public Const LAWYEROTHERCOUNT_FIELD = "lawyerOtherCount"

Public Const LOCATIONTYPENAME_FIELD = "locationTypeName"
Public Const LOCATIONENTITYNAME_FIELD = "locationEntityName"

Public Const MAINCOUNTYNAME_FIELD = "mainCountyName"
Public Const MAINENTITYID_FIELD = "mainEntityId"
Public Const MAINLAWYERID_FIELD = "mainLawyerId"
Public Const MAINLOCATIONID_FIELD = "mainLocationId"
Public Const MAINPERSONID_FIELD = "mainPersonId"
Public Const MAINQUOTEBOOKID_FIELD = "mainQuoteBookId"
Public Const MAINQUOTECOVERAGEID_FIELD = "mainQuoteCoverageId"
Public Const MIDDLENAME_FIELD = "middleName"
Public Const MOBILETELEPHONE_FIELD = "mobileTelephone"

Public Const NEXTEFFECTIVEDATE_FIELD = "nextEffectiveDate"
Public Const NETINCURREDALAE_FIELD = "netIncurredALAE"
Public Const NETINCURREDINDEMNITY_FIELD = "netIncurredIndemnity"
Public Const NODENAME_FIELD = "nodeName"
Public Const NUMERICINDEX_FIELD = "numericIndex"

Public Const OCCURDATERANGE_FIELD = "occurDateRange"
Public Const OCCURDATE_FIELD = "occurDate"
Public Const OPENDATE_FIELD = "openDate"
Public Const ORGANIZATIONLEGALFORM_FIELD = "organizationLegalForm"
Public Const ORGANIZATIONENTITYLOCATOR_FIELD = "organizationEntityLocator"
Public Const ORGANIZATIONENTITYNAME_FIELD = "organizationEntityName"
Public Const OTHERINSTANCEID_FIELD = "otherInstanceId"

Public Const PAYEE_FIELD = "payee"
Public Const PAYMENTALAE_FIELD = "paymentALAE"
Public Const PAYMENTGENERALLEGAL_FIELD = "paymentGeneralLegal"
Public Const PAYMENTINDEMNITY_FIELD = "paymentIndemnity"
Public Const PERCLAIMDEDUCTIBLE_FIELD = "perClaimDeductible"
Public Const PERSONENTITYNAME_FIELD = "personEntityName"
'Public Const PERSONLOCATOR_FIELD = "personLocator"
Public Const PERCLAIMLIMIT_FIELD = "perClaimLimit"
Public Const PLACENAME_FIELD = "placeName"
Public Const PLEADINGCAPTION_FIELD = "pleadingCaption"
Public Const POLICYNUMBER_FIELD = "policyNumber"
Public Const POLICYTERM_FIELD = "policyTerm"
Public Const PREMIUM_FIELD = "premium"

Public Const QUOTENAME_FIELD = "quoteName"

Public Const RATINGNAME_FIELD = "ratingName"
Public Const RECEIVEDATE_FIELD = "receiveDate"
Public Const RECIPIENTNAME_FIELD = "recipientName"
Public Const RECIPIENTSEQ_FIELD = "recipientSeq"
Public Const RECIPIENTSALUTATION_FIELD = "recipientSalutation"
Public Const RECIPIENTTYPEID_FIELD = "recipientTypeId"

Public Const REFERENCEEVENTID_FIELD = "referenceEventId"
Public Const REFERENCEEVENTTYPEID_FIELD = "referenceEventTypeId"
Public Const REFERENCEEVENTTYPENAME_FIELD = "referenceEventTypeName"

Public Const REFERENCENUMBER_FIELD = "referenceNumber" 'Found in the Disbursement table

Public Const REFERENCESOURCETYPE_KEY = "referenceSourceTypeId" ' virtual view field found in EventReferenceView
Public Const REFERENCETABLE_FIELD = "referenceTable"

Public Const REINSURERREPORTDATE_FIELD = "reinsurerReportDate"
Public Const REOPENDATE_FIELD = "reOpenDate"
Public Const REPORTDATE_FIELD = "reportDate"
Public Const RETROACTIVEDATE_FIELD = "retroactiveDate"


Public Const SALUTATION_FIELD = "salutation"
'Public Const SOURCEGROUPID_FIELD = "sourceGroupId"

Public Const SOURCETYPE_KEY = "sourceTypeId"
Public Const SOURCETYPE_FIELD = "sourceType" 'used in the GLEntry table

Public Const STAFFCOUNT_FIELD = "staffCount"
Public Const STATEBARNUMBER_FIELD = "stateBarNumber"
Public Const STATE_FIELD = "state"
Public Const STATENAME_FIELD = "stateName"
Public Const SUFFIX_FIELD = "suffix"

Public Const TABLE_FIELD = "table"
Public Const TELEPHONE_FIELD = "telephone"
Public Const TITLE_FIELD = "title"
Public Const TRANSACTIONAMOUNT_FIELD = "transactionAmount"
Public Const TRANSACTIONTYPENAME_FIELD = "transactionTypeName"
Public Const TREATYYEAR_FIELD = "treatyYear"
Public Const TRIALDATERANGE_FIELD = "trialDateRange"
Public Const TYPE_FIELD = "type"

Public Const VOIDDATE_FIELD = "voidDate"

Public Const WORKTELEPHONE_FIELD = "workTelephone"

Public Const ZIP_FIELD = "zip"

'
' Event Groups
'

Public Const APPEDIT_RULE = "AppEdit"
Public Const APPENDORSEMENTEDIT_RULE = "AppEndorsementEdit"
Public Const APPLAWYERCOVERAGE_RULE = "AppLawyerCoverage"
Public Const APPNOLAWYER_RULE = "AppNoLawyer"
Public Const APPNOLIMIT_RULE = "AppNoLimit"
Public Const APPREISSUE_RULE = "AppReissue"
Public Const CLAIMEVENT_RULE = "ClaimEvent"
Public Const CLAIMTAIL_RULE = "ClaimTail"
Public Const COVERAGE_RULE = "Coverage"
Public Const CVGCHANGE_RULE = "CvgChange"
Public Const CVGLIMIT_RULE = "CvgLimit"
Public Const CVGPOLICYBINDING_RULE = "CvgPolicyBinding"
Public Const CVGPOLICYMISC_RULE = "CvgPolicyMisc"

Public Const DOCUMENT_RULE = "Document"

Public Const FIRMEVENT_RULE = "FirmEvent"

Public Const GLACCOUNT_RULE = "GLAccountGroup"

Public Const INCIDENTDOCUMENT_RULE = "IncidentDocument"
Public Const INCIDENTEVENT_RULE = "IncidentEvent"

Public Const MP_RULE = "MP"

Public Const OPENEXPIRATIONDATE_RULE = "OpenExpirationDate"

Public Const POLICYACCOUNTINGEFT_RULE = "PolicyAccountingEFT"

Public Const QP_RULE = "QP"



'
' EventTypes (Id's not locators)
'
Public Const ACCOUNTINGNOTE_TYPE = "AccountingNote"
Public Const CLAIMDIARY_TYPE = "ClaimDiary"
Public Const CLAIMCROSSREFERENCE_TYPE = "ClaimCrossReference"
Public Const CURRENTINSURED_TYPE = "CI"

Public Const DONTSOLICIT_TYPE = "DS"

Public Const FIRM_TYPE = "Firm"
Public Const FIRMDISSOLVED_TYPE = "FD"
Public Const FIRMPOLICYCONTACT_TYPE = "Firm"

Public Const NEVERINSURED_TYPE = "NI"
Public Const NPRACEXTCLAIMREPPER_TYPE = "NPracExtClaimRepPer"
Public Const NOTOFFERING_TYPE = "NO"

Public Const ONLINEAPP_TYPE = "OnlineApp"

Public Const POLICYLIMITSCLAIM_TYPE = "PolicyLimitsClaim"

Public Const PREVIOUSINSURED_TYPE = "PI"

Public Const REISSUEAPP_TYPE = "ReissueApp"

'
' Question types
'

Public Const GRIEVANCECOUNSEL_QUESTION = "GC"


'
' Attribute Names
'
Public Const FILENAME_ATTRIBUTE = "fileName"
Public Const ISMODIFY_ATTRIBUTE = "isModify"
Public Const SAVEFILENAME_ATTRIBUTE = "saveFileName"
Public Const TERM_ATTRIBUTE = "term"
Public Const EXPIRATIONDATE_ATTRIBUTE = "expirationDate"


'
' Coverage Types
'
Public Const GREIVANCECOUNSEL_COVERAGETYPE = "Cvg-GC"
Public Const DEFENDANTSREIMBURSEMENT_COVERAGETYPE = "Cvg-DF"

'
' Event Status types
'
Public Const PENDING_EVENTSTATUS = "Pending"



'
' LocationType
'
Public Const PRIMARY_LOCATIONTYPE = "Firm"



'
' Source Types
'
Public Const DOCUMENT_SOURCETYPE = "Document"



'
' Hack for 2791
'
Public Const POLICYEXTENSION_VIEW = "PolicyExtensionView"
Public Const NEWEXPIRATIONDATE_FIELD = "newExpirationDate"

Public Const APP_POLICYEXTENSION_TYPE = "PolicyExtension"
Public Const CVG_POLICYEXTENSION_TYPE = "Cvg-PE"

