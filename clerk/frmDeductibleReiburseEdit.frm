VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmDeductibleReimburseEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Deductible Edit"
   ClientHeight    =   4545
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   6810
   Icon            =   "frmDeductibleReiburseEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4545
   ScaleWidth      =   6810
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtDataField 
      DataField       =   "eventTypeId"
      BeginProperty DataFormat 
         Type            =   0
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   0
      EndProperty
      DataMember      =   "Event"
      Enabled         =   0   'False
      Height          =   288
      Index           =   0
      Left            =   5520
      TabIndex        =   15
      Top             =   180
      Visible         =   0   'False
      Width           =   936
   End
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4068
      Left            =   0
      ScaleHeight     =   4065
      ScaleWidth      =   6810
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   384
      Width           =   6810
      Begin VB.CommandButton cmdClaim 
         Caption         =   "&Claim"
         Height          =   288
         Left            =   5715
         TabIndex        =   5
         Top             =   168
         Width           =   1000
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   3045
         TabIndex        =   6
         Top             =   3660
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   4290
         TabIndex        =   7
         Top             =   3660
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         CausesValidation=   0   'False
         Height          =   336
         Left            =   5520
         TabIndex        =   8
         Top             =   3660
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   2685
         Left            =   60
         TabIndex        =   19
         Top             =   870
         Width           =   6660
         _ExtentX        =   11748
         _ExtentY        =   4736
         _Version        =   393216
         Style           =   1
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmDeductibleReiburseEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label5"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lvGL"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtpDataField(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtDataField(3)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cboEventTypeName"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmDeductibleReiburseEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "A&dvanced"
         TabPicture(2)   =   "frmDeductibleReiburseEdit.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).ControlCount=   0
         Begin VB.TextBox txtNote 
            Height          =   2235
            Left            =   -74916
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   4
            Top             =   324
            Width           =   6108
         End
         Begin VB.ComboBox cboEventTypeName 
            Height          =   315
            ItemData        =   "frmDeductibleReiburseEdit.frx":0060
            Left            =   1725
            List            =   "frmDeductibleReiburseEdit.frx":0062
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   615
            Width           =   3216
         End
         Begin VB.TextBox txtDataField 
            Alignment       =   1  'Right Justify
            DataField       =   "deductibleReimburse"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            Height          =   288
            Index           =   3
            Left            =   5100
            TabIndex        =   2
            Top             =   628
            Width           =   1392
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmDeductibleReiburseEdit.frx":0064
            DataField       =   "eventDate"
            DataMember      =   "Event"
            Height          =   285
            Index           =   0
            Left            =   135
            TabIndex        =   0
            Top             =   630
            Width           =   1404
            _ExtentX        =   2487
            _ExtentY        =   503
            _Version        =   393216
            Format          =   9961473
            CurrentDate     =   37118
         End
         Begin MSComctlLib.ListView lvGL 
            Height          =   1575
            Left            =   1635
            TabIndex        =   3
            Tag             =   "ColumnFixed"
            Top             =   1005
            Width           =   4875
            _ExtentX        =   8599
            _ExtentY        =   2778
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "glAccountId"
               Text            =   "Account"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "transactionName"
               Text            =   "Name"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "transactionReference"
               Text            =   "Reference"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "transactionAmount"
               Text            =   "Amount"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "~debit"
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Debit"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "~credit"
               Object.Tag             =   "#,###.00;(#,###.00)"
               Text            =   "Credit"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label lbllabel 
            Caption         =   "Accounting:"
            Height          =   252
            Left            =   228
            TabIndex        =   16
            Top             =   1056
            Width           =   1092
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Amount"
            Height          =   192
            Left            =   5568
            TabIndex        =   10
            Top             =   348
            Width           =   540
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   108
         Top             =   3480
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   564
         Top             =   3516
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "claimant"
         DataField       =   "claimant"
         DataMember      =   "Instance"
         Height          =   192
         Index           =   1
         Left            =   72
         TabIndex        =   18
         Top             =   516
         UseMnemonic     =   0   'False
         Width           =   600
      End
      Begin VB.Label lblDataField 
         AutoSize        =   -1  'True
         Caption         =   "firmName"
         DataField       =   "firmName"
         DataMember      =   "Instance"
         Height          =   195
         Index           =   0
         Left            =   75
         TabIndex        =   17
         Top             =   210
         UseMnemonic     =   0   'False
         Width           =   660
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   6810
      _ExtentX        =   12012
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   6810
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   6690
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   30
         Width           =   6690
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   13
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   288
      Left            =   960
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   3216
      Visible         =   0   'False
      Width           =   1056
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpenClaim 
            Caption         =   "&Claim"
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator342 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator876 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&davanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator986 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
      End
      Begin VB.Menu mnuSeparator3512 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmDeductibleReimburseEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ver 1.90.1226 (srd)

Option Explicit

Private mDeductibleReimburseHandle As String
Private mClaimHandle As String
Private mEventHandle As String

Private mNew As Boolean
Private mDataSet As Dataset
Private mListView As ListView
Private mListIndex As Integer
Private mListItemType As String

Private mGLTransaction As Dataset

Private mEventTypes As Collection

Private mParameterNames() As String
Private mParameterValues() As String

Private mDirty As Boolean
Public FormMgr As New FormManager

Private Const TXT_DEDUCTIBLEREIMBURSE_INDEX = 3
Private Const TXT_EVENTTYPEID_INDEX = 0

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, vData, mNew
  End If
End Property


Private Sub ClaimRefresh()
  ClearDataField lblDatafield, "Instance"
  If mClaimHandle = vbNullString Then
    Exit Sub
  End If
    
  SetFields lblDatafield, Factory.InstanceService.Find(mClaimHandle)
  Caption = IIf(mNew, "New", "Edit") & " Deductible (" & mClaimHandle & ")"
End Sub


Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub
      
    ' Select form selected
    Case fimSelect
      mClaimHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.CLAIM_KEY)
      ClaimRefresh
      Dirty = True
  End Select
  Show
  dtpDataField(0).SetFocus
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  ClearDataField txtDatafield
  ClearDataField lblDatafield
  txtNote.Text = vbNullString
  
  If (mNew) Then
    dtpDataField(0).Value = Today
    cboEventTypeName.ListIndex = IIf(cboEventTypeName.ListCount > 0, 0, -1)
    mEventHandle = vbNullString
    Caption = "New Deductible (" & mClaimHandle & ")"
    FormUtil.GetComboDefault cboEventTypeName
  Else
    Dim Datafields As Collection
    Set Datafields = Factory.DeductibleReimburseService.Find(mDeductibleReimburseHandle)
    If Datafields Is Nothing Then
      Unload Me
      Exit Sub
    End If
    
    mClaimHandle = Datafields(DatabaseName.CLAIM_KEY)
    mEventHandle = Datafields(DatabaseName.EVENT_KEY)
  
    SetFields txtDatafield, Datafields
    'SetFields dtpDataField, Datafields
    
    If (mEventHandle <> vbNullString) Then
      Dim eventRow As Collection
      Set eventRow = Factory.EventService.Find(mEventHandle, DatabaseName.EVENTTYPE_KEY & "," & DatabaseName.EVENTDATE_FIELD)
      SetFields txtDatafield, eventRow, "Event"
      SetFields dtpDataField, eventRow, "Event"
      'set seperately for String truncate issues
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If

    txtDatafield(TXT_DEDUCTIBLEREIMBURSE_INDEX).Text = GetDouble(GetDataFromCollection(Datafields, DatabaseName.DEDUCTIBLEREIMBURSE_FIELD)) _
                         + GetDouble(GetDataFromCollection(Datafields, DatabaseName.DEDUCTIBLEWRITEOFF_FIELD))
    
    Caption = "Edit Deductible (" & mClaimHandle & ")"
  End If
  
  ' Refresh any claim associated data.
  ClaimRefresh
  
  cmdClaim.Enabled = mNew
  
  'Notepad
  mnuToolsNotepad.Enabled = Not mNew
  
  ' Send To
  mnuFileSendToFolder.Enabled = Not mNew
  
  'GL update
  ' ver 1.90.1226
  ' corrected filter to DatabaseName.EVENT_KEY
  mGLTransaction.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENT_KEY, mEventHandle)
  ListViewLoad lvGL, mGLTransaction.DataProvider, True
  FormUtil.GLTransactionSummary lvGL.ListItems
  
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  Dirty = False
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


Public Property Let ListIndex(vData As Integer)
  mListIndex = vData
  mListView.SelectedItem.Selected = False
  mListView.ListItems(vData).Selected = True
  Select Case mListItemType
    Case DatabaseName.DEDUCTIBLEREIMBURSE_TABLE
      mDeductibleReimburseHandle = ListItemText(mListView.SelectedItem, DatabaseName.DEDUCTIBLEREIMBURSE_KEY)
    Case DatabaseName.EVENT_TABLE
      mDeductibleReimburseHandle = FindHandle(DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, _
                                   DatabaseName.EVENT_KEY, _
                                   ListItemText(mListView.SelectedItem, DatabaseName.EVENT_KEY), _
                                   DatabaseName.DEDUCTIBLEREIMBURSE_KEY)
  End Select
End Property


Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
  
  ' Custom Code
  
  Dim mDataControl As New Collection
  AddTextControl txtDatafield, mDataControl
  AddValueControl dtpDataField, mDataControl
  
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  ParameterNames = mParameterNames
  ParameterValues = mParameterValues
  AddParameter DatabaseName.DEDUCTIBLEREIMBURSE_TABLE & "EventNote", _
      IIf(txtNote.Text = vbNullString, " ", txtNote.Text), _
      ParameterNames, ParameterValues
  
  If GetDouble(GetDataFromCollection(mDataControl, DatabaseName.DEDUCTIBLEREIMBURSE_FIELD)) > _
      Factory.ClaimService.Find(mClaimHandle)(DatabaseName.DEDUCTIBLEADVANCE_FIELD) Then
    If MsgBox("This transaction will result in a negative balance upon completion. Do you wish to continue?", vbYesNo) = vbNo Then
      Save = False
      Exit Function
    End If
  End If
  
  
  
  If mNew Then
    mDeductibleReimburseHandle = Factory.DeductibleReimburseService.CreateDeductibleReimburse( _
        vbNullString, mClaimHandle, mDataControl(DatabaseName.EVENTTYPE_KEY), _
        GetDate(mDataControl(DatabaseName.EVENTDATE_FIELD)), GetDouble(mDataControl("deductibleReimburse")), _
        vbNullStringArray, vbNullStringArray, ParameterNames, ParameterValues)
    Save = mDeductibleReimburseHandle <> vbNullString
    If Save Then
      mNew = False
    End If
  Else
    Save = Factory.DeductibleReimburseService.WriteDeductibleReimburse( _
      mDeductibleReimburseHandle, _
      mClaimHandle, mDataControl(DatabaseName.EVENTTYPE_KEY), _
      GetDate(mDataControl(DatabaseName.EVENTDATE_FIELD)), GetDouble(mDataControl("deductibleReimburse")), _
      vbNullStringArray, vbNullStringArray, ParameterNames, ParameterValues)
  End If

  If Save Then
    FormRefresh
  Else
    Dirty = True
  End If
End Function
          
Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.DEDUCTIBLEREIMBURSE_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no deductibles to view or edit."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(DeductibleReimburseHandle As String)
  mDeductibleReimburseHandle = DeductibleReimburseHandle
  Show
End Sub


Public Sub ShowNew(Optional ClaimHandle As String)
  mClaimHandle = ClaimHandle
  mNew = True
  Show
  
  If mClaimHandle = vbNullString Then
    cmdClaim_Click
  End If
End Sub
'
'
' End General
'
'
'
'
' Control Methods
'

Private Sub cboEventTypeName_Click()
  If cboEventTypeName.ListIndex < 0 Then
    txtDatafield(TXT_EVENTTYPEID_INDEX).Text = vbNullString
  Else
    txtDatafield(TXT_EVENTTYPEID_INDEX).Text = mEventTypes(cboEventTypeName.ListIndex + 1)(DatabaseName.EVENTTYPE_KEY)
  End If
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClaim_Click()
  Dispatcher.ShowSearch DatabaseName.CLAIM_TABLE, FormMgr, True, , , , "Select a claim for the deductible"
End Sub

Private Sub cmdSave_Click()
  If Save Then
    FormUtil.SetComboDefault cboEventTypeName
    Unload Me
  End If
End Sub

Private Sub cmdSaveNew_Click()
  If Save Then
    mnuFileNew_Click
  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
  Set mDataSet = New DeductibleReimburseDataset
  Set mDataSet.Session = LawSession
  
  ' ver 1.90.1226
  ' commented out above 2 lines, changed to generic dataset.
  Set mGLTransaction = NewGenericDataset(DatabaseName.GLTRANSACTION_VIEW)
  
'  Factory.FolderParameterService.ParameterLoad DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, lawActionNew, mParameterNames, mParameterValues
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
 ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  '
  ' Populate EventTypeName Dropdown
  '
  Set mEventTypes = Factory.EventTypeService.SourceCollection(DatabaseName.DEDUCTIBLEREIMBURSE_TABLE)
  FormUtil.ComboBoxLoad mEventTypes, cboEventTypeName, DatabaseName.EVENTTYPENAME_FIELD
  
  FormResize
  FormRefresh
  TabControlEnable SSTab
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mDeductibleReimburseHandle = vbNullString
  FormRefresh
End Sub

Private Sub mnuFileNewMenu_Click()
  If Save(True) Then
    mNew = True
    FormRefresh
  End If
End Sub

Private Sub mnuFileOpenClaim_Click()
  FormUtil.ShowByPrimaryKey New frmClaimOpen, mClaimHandle, FormMgr
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.DEDUCTIBLEREIMBURSE_REF, DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, mEventHandle, "Claim Name Goes hear?"
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
  FormUtil.ShowEventDialog mEventHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, mDeductibleReimburseHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
  FolderUtil.WorkflowShow DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, lawActionNew, _
      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolBar.Checked = Not mnuViewToolBar.Checked
  cbrCoolBar.Visible = mnuViewToolBar.Checked
  FormResize
End Sub

Private Sub SSTab_Click(PreviousTab As Integer)
  TabControlEnable SSTab
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
  Select Case Index
    Case 0
      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, DatabaseName.EVENTTYPE_KEY, txtDatafield(Index).Text) - 1
  End Select
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDatafield(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
