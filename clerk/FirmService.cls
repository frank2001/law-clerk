VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FirmService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.00.0318 (jjm)
Option Explicit
'
'Private Const ServiceName = "FirmService"
'
'Private Client As SoapClient30
Private mDataSet As Dataset
Private mFirmView As Dataset
Private mPolicyDataset As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
'  Set Client = New SoapClient30
'  Client.MSSoapInit HttpServer &  WebComponent & ServiceName & ".jws?wsdl"
'  Client.ConnectorProperty("EndPointURL") = HttpServer &  WebService & ServiceName & ".jws" & LawSession.Session
'

  Set mDataSet = NewGenericDataset(DatabaseName.FIRM_TABLE)
  Set mFirmView = NewGenericDataset(DatabaseName.FIRM_VIEW)
  Set mPolicyDataset = NewGenericDataset(DatabaseName.POLICY_TABLE)
  
End Sub


''
'' Interface to server
''
'
''
'' Create an application.
''
'Public Function CreateFirm( _
'    FirmId As String, FirmLocator As String, FirmName As String, _
'    StateBarId As String, FederalTaxId As String, OrganizationType As String, _
'    EstablishDate As Date, NextEffectiveDate As Date, _
'    LocationId As String, PersonId As String, _
'    eventNote As String) As Variant
'
'On Error GoTo ErrorHandler
'
'  CreateFirm = Client.Create( _
'      FirmId, FirmLocator, FirmName, _
'      StateBarId, FederalTaxId, OrganizationType, _
'      EstablishDate, NextEffectiveDate, _
'      LocationId, PersonId, _
'      eventNote)
'  Exit Function
'
'ErrorHandler:
'  ErrorUtil.DisplaySoapError Err, Client
''  MsgBox "Fault: " & Client.FaultString, vbExclamation
'
'End Function
'
''
'' Write an application.
''
'Public Function WriteFirm( _
'    firmHandle As String, FirmLocator As String, FirmName As String, _
'    StateBarId As String, FederalTaxId As String, OrganizationType As String, _
'    EstablishDate As Date, NextEffectiveDate As Date, _
'    LocationId As String, PersonId As String, _
'    eventNote As String) As Boolean
'
'On Error GoTo ErrorHandler
'
'  Call Client.Write( _
'      firmHandle, FirmLocator, FirmName, _
'      StateBarId, FederalTaxId, OrganizationType, _
'      EstablishDate, NextEffectiveDate, _
'      LocationId, PersonId, _
'      eventNote)
'  WriteFirm = True
'  Exit Function
'
'ErrorHandler:
'  ErrorUtil.DisplaySoapError Err, Client
'  'MsgBox "Fault: " & Client.FaultString, vbExclamation
'  WriteFirm = False
'End Function


'
' Delete an Firm
'
'
'Public Function DeleteFirm( _
'    firmHandle As String, _
'    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean
'
'On Error GoTo ErrorHandler
'
'  Call Client.Delete(firmHandle, ParameterNames, ParameterValues)
'  DeleteFirm = True
'  Exit Function
'
'ErrorHandler:
'  'MsgBox "Fault: " & Client.FaultString, vbExclamation
'  ErrorUtil.DisplaySoapError Err, Client
'  DeleteFirm = False
'End Function


Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.FIRM_KEY, Handle, Column)
End Function

Public Function FindView(Handle As String, Optional Column As String) As Collection
  Set FindView = mFirmView.DataProvider.Find(DatabaseName.FIRM_KEY, Handle, Column)
End Function

'Helper function to get the Firm Table information by the organizationId
Public Function GetFirmByOrganizationId(organizationId As String, Optional Column As String) As Collection
  Set GetFirmByOrganizationId = mDataSet.DataProvider.Find(DatabaseName.ORGANIZATION_KEY, organizationId, Column)
End Function


'ver 2.00.0318 (jjm)
Public Function GetFirmLocator(Handle As String) As String
  If Handle = vbNullString Then
    Exit Function
  End If
  GetFirmLocator = mFirmView.DataProvider.Find(DatabaseName.FIRM_KEY, Handle, DatabaseName.FIRMENTITYLOCATOR_FIELD)(DatabaseName.FIRMENTITYLOCATOR_FIELD)
End Function


'Determine if the Firm is currently insured based on firm insurer status.
'@param handle string representing the Firm.firmId.
'@return a boolean which will be true if the firm is currently insured.
Public Function IsCurrentInsured(Handle As String) As Boolean
  mPolicyDataset.DataProvider.filter = _
    DatabaseName.FIRM_KEY & " = '" & Handle & "' and " & _
    DatabaseName.EFFECTIVEDATE_FIELD & " <= " & LawDialect.DateFormat(Today) & " and " & _
    DatabaseName.EXPIRATIONDATE_FIELD & " >= " & LawDialect.DateFormat(Today) & " and " & _
    "empty(" & DatabaseName.CANCELEFFECTIVEDATE_FIELD & ")"
  mPolicyDataset.DataProvider.Column = DatabaseName.POLICY_KEY
  mPolicyDataset.DataProvider.OpenRecordset
  Dim rows As New Collection
  Set rows = mPolicyDataset.DataProvider.Collection
  IsCurrentInsured = rows.Count > 0
End Function


