VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ContactService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const ServiceName = "ContactService"

Private mClient As SoapClient30
Private mDataSet As Dataset

'
' Initialize service and connect via SOAP
'
'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Set mDataSet = NewGenericDataset(DatabaseName.CONTACT_TABLE)
End Sub
'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.CONTACT_KEY, Handle, Column)
End Function

'
' Interface to server
'

'  parameterOrder='ContactId displayName salutation title firstName
'                  middleName lastName suffix nickname jobTitle
'                  socialSecurityNum attributeNames attributeValues parameterNames parameterValues'>

'
' Create an application.
'
Public Function CreateContact( _
    contactTypeId As String, _
    organizationId As String, _
    contactEntityId As String, _
    LocationId As String, _
    instanceId As String, _
    contactSalutation As String) As Variant
    
On Error GoTo ErrorHandler

  CreateContact = mClient.Create( _
      contactTypeId, organizationId, contactEntityId, LocationId, instanceId, _
      contactSalutation)
  
  Exit Function
    
ErrorHandler:
  'MsgBox "Fault: " & mClient.FaultString & vbCrLf & mClient.Detail, vbExclamation
  ErrorUtil.DisplaySoapError Err, mClient
End Function

'
' Write an contact.
'
Public Function WriteContact( _
    contactId As String, contactTypeId As String, _
    organizationId As String, contactEntityId As String, LocationId As String, instanceId As String, _
    contactSalutation As String) As Variant
    
On Error GoTo Exception:
    mClient.Write _
       contactId, _
       contactTypeId, _
       organizationId, _
       contactEntityId, _
       LocationId, _
       instanceId, _
       contactSalutation
    WriteContact = True
    
  Exit Function
    
Exception:

  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Contact Service Error: " & Err.Description, vbExclamation
  WriteContact = False
End Function

'
' Delete an Contact
'
Public Function DeleteContact(ContactHandle As String) As Boolean

On Error GoTo ErrorHandler
  Call mClient.Delete(ContactHandle)
  DeleteContact = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Fault: " & mClient.FaultString, vbExclamation
  DeleteContact = False
End Function

Public Function FindByOrganization(organizationHandle As String, ContactTypeHandle As String) As Collection
  mDataSet.DataProvider.Column = DatabaseName.CONTACT_KEY
  mDataSet.DataProvider.filter = DatabaseName.ORGANIZATION_KEY & " = '" & organizationHandle & "' and " & _
                                 DatabaseName.CONTACTTYPE_KEY & " = '" & ContactTypeHandle & "'"
  mDataSet.DataProvider.Sort = vbNullString
  mDataSet.DataProvider.OpenRecordset
  If (Not mDataSet.DataProvider.IsEmpty) Then
    Set FindByOrganization = mDataSet.DataProvider.Collection
  End If
End Function

Public Function FindByFirm(dud As String, no_good As String) As Collection
  MsgBox "Debug: Please modify the code to use the FindByOrganization function."
End Function

'
' Move a contact to a different person
'
Public Function SetPerson(ContactHandle As String, PersonHandle As String) As Boolean

On Error GoTo ErrorHandler
  Call mClient.SetPerson(ContactHandle, PersonHandle)
  SetPerson = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, mClient
  'MsgBox "Fault: " & mClient.FaultString, vbExclamation
  SetPerson = False
  
End Function
