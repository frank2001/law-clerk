VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCLELawyer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lawyer CLE"
   ClientHeight    =   5292
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   5496
   Icon            =   "frmCLELawyer.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5292
   ScaleWidth      =   5496
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5244
      Left            =   0
      ScaleHeight     =   5244
      ScaleWidth      =   5496
      TabIndex        =   0
      Top             =   0
      Width           =   5496
      Begin VB.CommandButton cmdHelp 
         Caption         =   "Help"
         Height          =   336
         Left            =   4188
         TabIndex        =   3
         Top             =   4872
         Width           =   1200
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   1680
         TabIndex        =   2
         Top             =   4872
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2928
         TabIndex        =   1
         Top             =   4872
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTabCLE 
         Height          =   4692
         Left            =   132
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Width           =   5268
         _ExtentX        =   9292
         _ExtentY        =   8276
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   520
         TabCaption(0)   =   "Lawyer CLE"
         TabPicture(0)   =   "frmCLELawyer.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblEventLocation"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblEVentName"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblEventID"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblEVentDate"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblThru"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblFrom"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblUsedCredit"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblMaxDate"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblCLEEarned"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtpDataField(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtpDataField(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtpDataField(3)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtpDataField(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "fraLawyer"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtDataField(3)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtDataField(2)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtDataField(1)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtDataField(8)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtDataField(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).ControlCount=   19
         Begin VB.TextBox txtDataField 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   1
            EndProperty
            DataMember      =   "CLEEvent"
            Height          =   288
            Index           =   4
            Left            =   2256
            TabIndex        =   14
            Top             =   2688
            Width           =   2904
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "cleCredit"
            DataMember      =   "CLEEvent"
            Height          =   288
            Index           =   8
            Left            =   2256
            TabIndex        =   13
            Top             =   4032
            Width           =   2904
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "cleEventId"
            DataMember      =   "CLEEvent"
            Height          =   288
            Index           =   1
            Left            =   2256
            TabIndex        =   12
            Top             =   1680
            Width           =   2904
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventName"
            DataMember      =   "CLEEvent"
            Height          =   288
            Index           =   2
            Left            =   2256
            TabIndex        =   11
            Top             =   2016
            Width           =   2904
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventLocation"
            DataMember      =   "CLEEvent"
            Height          =   288
            Index           =   3
            Left            =   2256
            TabIndex        =   10
            Top             =   2352
            Width           =   2904
         End
         Begin VB.Frame fraLawyer 
            Caption         =   "Lawyer Info"
            Height          =   852
            Left            =   96
            TabIndex        =   5
            Top             =   432
            Width           =   5064
            Begin VB.Label lblLawyerID 
               Caption         =   "Lawyer ID: "
               Height          =   192
               Left            =   132
               TabIndex        =   9
               Top             =   312
               Width           =   960
            End
            Begin VB.Label lblName 
               Caption         =   "Lawyer Name:"
               Height          =   192
               Left            =   132
               TabIndex        =   8
               Top             =   552
               Width           =   1212
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "lawyerId"
               DataField       =   "lawyerId"
               DataMember      =   "Lawyer"
               Height          =   192
               Index           =   0
               Left            =   2052
               TabIndex        =   7
               Top             =   300
               UseMnemonic     =   0   'False
               Width           =   600
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "lawyerName"
               DataField       =   "lawyerName"
               DataMember      =   "Lawyer"
               Height          =   192
               Index           =   1
               Left            =   2052
               TabIndex        =   6
               Top             =   540
               UseMnemonic     =   0   'False
               Width           =   912
            End
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "beginDate"
            Height          =   288
            Index           =   1
            Left            =   2256
            TabIndex        =   15
            Top             =   3024
            Width           =   2904
            _ExtentX        =   5122
            _ExtentY        =   508
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   47251457
            CurrentDate     =   37111
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "maxDate"
            Height          =   288
            Index           =   3
            Left            =   2256
            TabIndex        =   16
            Top             =   3696
            Width           =   2904
            _ExtentX        =   5122
            _ExtentY        =   508
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   47251457
            CurrentDate     =   37111
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "eventDate"
            Height          =   288
            Index           =   0
            Left            =   2256
            TabIndex        =   17
            Top             =   1344
            Width           =   2904
            _ExtentX        =   5122
            _ExtentY        =   508
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   47251457
            CurrentDate     =   37111
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            DataField       =   "endDate"
            Height          =   288
            Index           =   2
            Left            =   2256
            TabIndex        =   18
            Top             =   3360
            Width           =   2904
            _ExtentX        =   5122
            _ExtentY        =   508
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   47251457
            CurrentDate     =   37111
         End
         Begin VB.Label lblCLEEarned 
            Caption         =   "CLE Earned:"
            Height          =   192
            Left            =   228
            TabIndex        =   27
            Top             =   2724
            Width           =   1224
         End
         Begin VB.Label lblMaxDate 
            Caption         =   "Max Date:"
            Height          =   192
            Left            =   228
            TabIndex        =   26
            Top             =   3732
            Width           =   984
         End
         Begin VB.Label lblUsedCredit 
            Caption         =   "Used Credit:"
            Height          =   192
            Left            =   228
            TabIndex        =   25
            Top             =   4068
            Width           =   2184
         End
         Begin VB.Label lblFrom 
            Caption         =   "From:"
            Height          =   192
            Left            =   228
            TabIndex        =   24
            Top             =   3060
            Width           =   1584
         End
         Begin VB.Label lblThru 
            Caption         =   "Thru:"
            Height          =   192
            Left            =   228
            TabIndex        =   23
            Top             =   3396
            Width           =   1824
         End
         Begin VB.Label lblEVentDate 
            Caption         =   "Event Date:"
            Height          =   192
            Left            =   228
            TabIndex        =   22
            Top             =   1380
            Width           =   1224
         End
         Begin VB.Label lblEventID 
            Caption         =   "Event ID:"
            Height          =   192
            Left            =   228
            TabIndex        =   21
            Top             =   1716
            Width           =   1224
         End
         Begin VB.Label lblEVentName 
            Caption         =   "Event Name:"
            Height          =   192
            Left            =   228
            TabIndex        =   20
            Top             =   2052
            Width           =   1224
         End
         Begin VB.Label lblEventLocation 
            Caption         =   "Event Location:"
            Height          =   192
            Left            =   228
            TabIndex        =   19
            Top             =   2388
            Width           =   1224
         End
      End
      Begin VB.Label Label2 
         Caption         =   "CLE Credits"
         Height          =   252
         Left            =   864
         TabIndex        =   30
         Top             =   3672
         Width           =   1932
      End
      Begin VB.Label Label1 
         Caption         =   "CLE Credits"
         Height          =   252
         Left            =   984
         TabIndex        =   29
         Top             =   3684
         Width           =   1932
      End
      Begin VB.Label Label3 
         Caption         =   "Lawyer ID "
         Height          =   252
         Left            =   2544
         TabIndex        =   28
         Top             =   792
         Width           =   732
      End
   End
End
Attribute VB_Name = "frmCLELawyer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
  
Private mvarPrimaryKey As Collection
Private mvarDataFields As Collection
Private CLEEvent As Dataset
Private LawyerData As Dataset

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub Form_Initialize()
   Set CLEEvent = New CLEEventDataset
   Set CLEEvent.Session = LawSession
   
   Set LawyerData = New LawyerDataset
   Set LawyerData.Session = LawSession
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  FormResize
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me

End Sub

