VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IncidentService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'ver 2.30.1007
'previous ver 2.30.0904 (jjm)
Option Explicit

Private Const ServiceName = "IncidentService"
Private Client As SoapClient30
Private mDataSet As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.INCIDENT_TABLE)
    
  'Setup SOAP
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub

'
' Create an Incident
'
Public Function CreateIncident( _
    IncidentId As String, ClaimId As String, claimant As String, _
    firmId As String, LawyerId As String, PolicyId As String, _
    OccurDateRange As String, OccurDate As Date, _
    KnowDateRange As String, KnowDate As Date, Adjuster As String, _
    FeeDispute As Boolean, ReportDate As Date, _
    OpenDate As Date, CloseDate As Date, lawAreas() As String, lawErrors() As String, _
    eventNote As String) As String


On Error GoTo ErrorHandler
  
  CreateIncident = Client.Create( _
    IncidentId, ClaimId, claimant, _
    firmId, LawyerId, PolicyId, _
    OccurDateRange, OccurDate, _
    KnowDateRange, KnowDate, Adjuster, _
    FeeDispute, ReportDate, _
    OpenDate, CloseDate, lawAreas(), lawErrors(), _
    eventNote)
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Function

'
' Write an Incident
'
Public Function WriteIncident( _
    Handle As String, ClaimId As String, claimant As String, _
    firmId As String, LawyerId As String, PolicyId As String, _
    OccurDateRange As String, OccurDate As Date, _
    KnowDateRange As String, KnowDate As Date, Adjuster As String, _
    FeeDispute As Boolean, ReportDate As Date, _
    OpenDate As Date, CloseDate As Date, lawAreas() As String, lawErrors() As String, _
    eventNote As String) As Boolean

    
On Error GoTo ErrorHandler
  
  Call Client.Write( _
    Handle, ClaimId, claimant, _
    firmId, LawyerId, PolicyId, _
    OccurDateRange, OccurDate, _
    KnowDateRange, KnowDate, Adjuster, _
    FeeDispute, ReportDate, _
    OpenDate, CloseDate, lawAreas(), lawErrors(), eventNote)
  WriteIncident = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  WriteIncident = False
End Function


'
' Delete an Firm
'

Public Function DeleteIncident( _
    IncidentHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler

  Call Client.Delete(IncidentHandle, ParameterNames, ParameterValues)
  DeleteIncident = True
  Exit Function
  
ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  DeleteIncident = False
End Function


Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.INCIDENT_KEY, Handle, Column)
End Function


Public Sub ShowNarrative(Handle As String)
  Dim Path As String
  Path = IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVEPATH_INI) & _
         IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVEPREFIX_INI) & _
         Handle & _
         IniFolder.ReadFile(UrlName.MISC_INI, UrlName.NARRATIVESUFFIX_INI)
         
  ShellExecute 0&, vbNullString, GetRealPath(IniFolder.ReadFile(UrlName.MISC_INI, "incidentcasesht.bat")), _
            Path, vbNullString, SW_SHOWNORMAL
            
'  ErrorUtil.DisplayMessage "DEBUG: See details for shell execute informmation. This message should be removed for the production build.", _
'                           "File: " & GetRealPath(IniFolder.ReadFile(UrlName.MISC_INI, "incidentcasesht.bat")) & vbCrLf & "Parameter: " & Path
End Sub



