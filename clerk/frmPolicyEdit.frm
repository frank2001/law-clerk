VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPolicyEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Policy Edit"
   ClientHeight    =   3600
   ClientLeft      =   120
   ClientTop       =   600
   ClientWidth     =   4212
   Icon            =   "frmPolicyEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   4212
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3156
      Left            =   0
      ScaleHeight     =   3156
      ScaleWidth      =   4212
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   384
      Width           =   4212
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   456
         TabIndex        =   0
         Top             =   2808
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   1692
         TabIndex        =   1
         Top             =   2808
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         CausesValidation=   0   'False
         Height          =   336
         Left            =   2928
         TabIndex        =   2
         Top             =   2808
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab 
         Height          =   2664
         Left            =   96
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   96
         Width           =   4032
         _ExtentX        =   7112
         _ExtentY        =   4699
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "frmPolicyEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label3"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblDataField(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label5"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblDataField(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label7"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblDataField(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Label9"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblDataField(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblDataField(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "Label2"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblDataField(5)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "effectiveDate"
            DataField       =   "effectiveDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   192
            Index           =   5
            Left            =   1860
            TabIndex        =   20
            Top             =   804
            UseMnemonic     =   0   'False
            Width           =   948
         End
         Begin VB.Label Label2 
            Caption         =   "Effective Date"
            Height          =   192
            Left            =   132
            TabIndex        =   19
            Top             =   799
            Width           =   1356
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "issueDate"
            DataField       =   "issueDate"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   192
            Index           =   4
            Left            =   1860
            TabIndex        =   18
            Top             =   492
            UseMnemonic     =   0   'False
            Width           =   732
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "premium"
            DataField       =   "premium"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   192
            Index           =   3
            Left            =   1860
            TabIndex        =   16
            Top             =   2028
            UseMnemonic     =   0   'False
            Width           =   624
         End
         Begin VB.Label Label9 
            Caption         =   "Premium"
            Height          =   192
            Left            =   132
            TabIndex        =   15
            Top             =   2028
            Width           =   1512
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "perClaimDeductible"
            DataField       =   "perClaimDedcutible"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   192
            Index           =   2
            Left            =   1860
            TabIndex        =   14
            Top             =   1716
            UseMnemonic     =   0   'False
            Width           =   1428
         End
         Begin VB.Label Label7 
            Caption         =   "Per Claim Deductible"
            Height          =   192
            Left            =   132
            TabIndex        =   13
            Top             =   1720
            Width           =   1512
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "aggregateLimit"
            DataField       =   "aggreageLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   192
            Index           =   1
            Left            =   1860
            TabIndex        =   12
            Top             =   1416
            UseMnemonic     =   0   'False
            Width           =   1080
         End
         Begin VB.Label Label5 
            Caption         =   "Aggregate Limit"
            Height          =   192
            Left            =   132
            TabIndex        =   11
            Top             =   1413
            Width           =   1356
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "perClaimLimit"
            DataField       =   "perClaimLimit"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$""#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   2
            EndProperty
            Height          =   192
            Index           =   0
            Left            =   1860
            TabIndex        =   10
            Top             =   1104
            UseMnemonic     =   0   'False
            Width           =   972
         End
         Begin VB.Label Label3 
            Caption         =   "Per Claim Limit"
            Height          =   192
            Left            =   132
            TabIndex        =   9
            Top             =   1106
            Width           =   1356
         End
         Begin VB.Label Label1 
            Caption         =   "Issue Date"
            Height          =   192
            Left            =   132
            TabIndex        =   8
            Top             =   492
            Width           =   1356
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   4212
      _ExtentX        =   7430
      _ExtentY        =   677
      BandCount       =   1
      _CBWidth        =   4212
      _CBHeight       =   384
      _Version        =   "6.7.8988"
      Child1          =   "picUpDown"
      MinHeight1      =   336
      Width1          =   1068
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   336
         ScaleWidth      =   4116
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   24
         Width           =   4116
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   72
            TabIndex        =   7
            Top             =   60
            Width           =   732
            _ExtentX        =   1291
            _ExtentY        =   402
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   456
      Top             =   36
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   396
      Left            =   108
      TabIndex        =   17
      Top             =   4176
      Visible         =   0   'False
      Width           =   972
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator342 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Index           =   0
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Index           =   1
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator876 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&davanced Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator986 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsNotepad 
         Caption         =   "&Notepad"
      End
      Begin VB.Menu mnuSeparator3512 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "Wor&kflow..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmPolicyEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mPolicyHandle As String
Private mApplicationHandle As String
Private mEventHandle As String

Private mNew As Boolean
Private mDataSet As Dataset
Private mListView As ListView
Private mListIndex As Integer
Private mListItemType As String

Private mEventTypes As Collection

Private mParameterNames() As String
Private mParameterValues() As String

Private mDirty As Boolean
Public FormMgr As New FormManager

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, vData, mNew
  End If
End Property

Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
'    ' Either select or new firm did not specify any action.
    Case fimNone
      Show
      Exit Sub

    ' Select form selected
    Case fimSelect
      'mPolicyHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.CLAIM_KEY)
      mApplicationHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.APPLICATION_KEY)
      'ClaimRefresh
      'Dirty = True
  End Select
  Show
End Sub

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  If (mNew) Then
'    ClearDataField txtDataField
'    dtpDataField(0).Value = Now
'    cboEventTypeName.ListIndex = IIf(cboEventTypeName.ListCount > 0, 0, -1)
'    mEventHandle = ""
    Caption = "New Policy (" & mPolicyHandle & ")"
  Else
    Dim Datafields As Collection
    Set Datafields = Factory.PolicyService.Find(mPolicyHandle)
    
    If Datafields Is Nothing Then
      Unload Me
      Exit Sub
    End If
'
'    mPolicyHandle = DataFields(DatabaseName.CLAIM_KEY)
'    mEventHandle = DataFields(databasename.EVENT_KEY)
'
'    SetFields txtDataField, DataFields
    SetFields lblDataField, Datafields
'
    Caption = "Edit Policy (" & mPolicyHandle & ")"
  End If
'
'  ' Refresh any claim associated data.
'  ClaimRefresh
'
'  ' Send To
'  mnuFileSendToFolder.Enabled = Not mNew
'
'
'  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
'
  Dirty = False
End Sub

'
' Because form is fixed single, we have a custom FormResize to
' set the size of the form depending on the location of picBody
' as it
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
'  mListIndex = vData
'  mListView.SelectedItem.Selected = False
'  mListView.ListItems(vData).Selected = True
'  Select Case mListItemType
'    Case DatabaseName.DEDUCTIBLEREIMBURSE_TABLE
'      mPolicyHandle = ListItemText(mListView.SelectedItem, databasename.DEDUCTIBLEREIMBURSE_KEY)
'    Case DatabaseName.EVENT_TABLE
'      mPolicyHandle = Factory.ReferenceService.ReferenceId1(ListItemText(mListView.SelectedItem, databasename.EVENT_KEY), DatabaseName.DEDUCTIBLEREIMBURSE_TABLE)
'  End Select
End Property

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
  
  ' Custom Code
  
'  Dim mDataControl As New Collection
'  AddTextControl txtDataField, mDataControl
'  AddValueControl dtpDataField, mDataControl
'
'  If mNew Then
'    mPolicyHandle = Factory.DeductibleReimburseService.CreateDeductibleReimburse( _
'        vbNullString, mPolicyHandle, mDataControl(databasename.EVENTTYPE_KEY), _
'        GetDate(mDataControl("reimburseDate")), GetDouble(mDataControl("deductibleReimburse")), _
'        vbNullStringArray, vbNullStringArray, mParameterNames, mParameterValues)
'    Save = mPolicyHandle <> vbNullString
'    If Save Then
'      mNew = False
'    End If
'  Else
'    Save = Factory.DeductibleReimburseService.WriteDeductibleReimburse( _
'      mPolicyHandle, _
'      mPolicyHandle, mDataControl(databasename.EVENTTYPE_KEY), _
'      GetDate(mDataControl("reimburseDate")), GetDouble(mDataControl("deductibleReimburse")), _
'      vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
'  End If

  If Save Then
    FormRefresh
  Else
    Dirty = True
  End If
End Function
          
Public Sub ShowByListView(View As ListView, Optional ListItemType As String = DatabaseName.DEDUCTIBLEREIMBURSE_TABLE)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no deductibles to view or edit."
    Exit Sub
  End If
  Set mListView = View
  mListItemType = ListItemType
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(PolicyHandle As String)
  mPolicyHandle = PolicyHandle
  Show
End Sub


Public Sub ShowNew(Optional PolicyHandle As String)
  mPolicyHandle = PolicyHandle
  mNew = True
  Show
End Sub
'
'
' End General
'
'
'
'
' Control Methods
'


Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdClaim_Click()
  Dispatcher.ShowSearch DatabaseName.CLAIM_TABLE, FormMgr, True
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
  Set mDataSet = New PolicyDataset
  Set mDataSet.Session = LawSession
  
  'Factory.FolderParameterService.ParameterLoad DatabaseName.POLICY_TABLE, lawActionNew, mParameterNames, mParameterValues
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
 ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
'
'  '
'  ' Populate EventTypeName Dropdown
'  '
'  Set mEventTypes = Factory.EventTypeService.SourceCollection(DatabaseName.DEDUCTIBLEREIMBURSE_TABLE)
'  Factory.EventTypeService.ComboBoxLoad mEventTypes, cboEventTypeName, "eventName"
  
  FormResize
  FormRefresh
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  mNew = True
  mPolicyHandle = vbNullString
  FormRefresh
End Sub

Private Sub mnuFileWorkflow_Click(Index As Integer)
'  FormUtil.ShowEventDialog DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, mPolicyHandle, Index
  FormRefresh
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.POLICY_TABLE, mPolicyHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsNotepad_Click()
  Dim fNotepad As New frmNotepad
'  fNotepad.ShowEvent mEventHandle
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
'  FolderUtil.WorkflowShow DatabaseName.DEDUCTIBLEREIMBURSE_TABLE, lawActionNew, _
'      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub


Private Sub txtDataField_Change(Index As Integer)
'  Dirty = True
'  Select Case Index
'    Case 0
'      cboEventTypeName.ListIndex = CollectionFind(mEventTypes, databasename.EVENTTYPE_KEY, txtDataField(Index).Text) - 1
'  End Select
End Sub

