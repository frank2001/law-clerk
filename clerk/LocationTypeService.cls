VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LocationTypeService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'Private Const ServiceName = "ReferenceService"
'Private mClient As SoapClient30

Private mDataSet As Dataset


Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.LOCATIONTYPE_TABLE)
End Sub


Public Function NameCollection() As Collection
  mDataSet.DataProvider.Column = DatabaseName.LOCATIONTYPE_KEY & "," & DatabaseName.LOCATIONTYPENAME_FIELD
  mDataSet.DataProvider.Filter = vbNullString
  mDataSet.DataProvider.Sort = DatabaseName.LOCATIONTYPENAME_FIELD
  mDataSet.DataProvider.OpenRecordset
  Set NameCollection = mDataSet.DataProvider.Collection
End Function
