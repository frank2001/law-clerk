VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GenericService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const WSDLFile = "GenericService"
Private Const SERVICE = "GenericService"
Private Const SERVICE11 = "GenericService11"
Private Const SERVICE12 = "GenericService12"

Private mServiceName As String
Private mClient As SoapClient30

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mClient = New SoapClient30
  
  Dim URL As String
  URL = HttpServer & WebApplication & WebComponent & WSDLFile & ".wsdl"
  
  Dim ServiceList(3) As String
  ServiceList(0) = SERVICE12
  ServiceList(1) = SERVICE11
  ServiceList(2) = SERVICE

  Dim I As Integer
  For I = 0 To SafeUBound(ServiceList)
    Set mClient = Factory.ServiceMSSoapInit(URL, ServiceList(I))
    If Not mClient Is Nothing Then
      mServiceName = ServiceList(I)
      Exit For
    End If
  Next I
  
  If mClient Is Nothing Then
    MsgBox "GenericService Error:  Unable to connect to the server (" & HttpServer & ").  You will not be able to save changes.", vbCritical, "Error"
    Exit Sub
  End If

  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebApplication & WebService & LawSession.Session
End Sub

'
' Generic Add call for all services
'
Public Function addObject( _
    ClassName As String, Names() As String, Values As Variant, _
    AttributeNames() As String, AttributeValues() As String, _
    ParameterNames() As String, ParameterValues() As String) As Variant
    
On Error GoTo ErrorHandler
  Select Case mServiceName
    Case SERVICE
      addObject = mClient.addObject(ClassName, Names, Values, _
          AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  End Select
  Exit Function
  
ErrorHandler:
  MsgBox mServiceName & " Fault: " & mClient.FaultString, vbExclamation
End Function

Public Function Find(Source As String, HandleName As String, Handle As String, Optional Column As String) As Collection
  Dim DS As Dataset
  Set DS = NewGenericDataset(Source)
  Set Find = DS.DataProvider.Find(HandleName, Handle, Column)
End Function

Public Function FindMulti(Source As String, HandleNames() As String, Handles, Optional Column As String) As Collection
  Dim DS As Dataset
  Set DS = NewGenericDataset(Source)
  Set FindMulti = DS.DataProvider.FindMulti(HandleNames, Handles, Column)
End Function

'
' Generic Replace (update) call for all services
'
Public Function updateObject( _
    ClassName As String, Key As Variant, _
    Names() As String, Values As Variant, _
    AttributeNames() As String, AttributeValues() As String, _
    ParameterNames() As String, ParameterValues() As String) As Boolean
    
On Error GoTo ErrorHandler

  Select Case mServiceName
    Case SERVICE
      Call mClient.updateObject(ClassName, Key, Names, Values, _
          AttributeNames, AttributeValues, ParameterNames, ParameterValues)
      updateObject = True
  End Select
  Exit Function
  
ErrorHandler:
  MsgBox mServiceName & " Fault: " & mClient.FaultString, vbExclamation
End Function

Public Function removeObject( _
   ClassName As String, Key As Variant, _
    ParameterNames() As String, ParameterValues() As String) As Boolean
On Error GoTo ErrorHandler

  Select Case mServiceName
    Case SERVICE
      Call mClient.removeObject(ClassName, Key, _
          ParameterNames, ParameterValues)
      removeObject = True
  End Select
  Exit Function
  
ErrorHandler:
  MsgBox mServiceName & " Fault: " & mClient.FaultString, vbExclamation
End Function


