VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ApplicationLawyerService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const ServiceName = "ApplicationLawyerService"
Private Client As SoapClient30

Private mDataSet As Dataset

Private mHandleNames(1) As String

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.APPLICATIONLAWYER_TABLE)
  mHandleNames(0) = DatabaseName.APPLICATION_KEY
  mHandleNames(1) = DatabaseName.LAWYER_KEY
  
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub


'
' Data Access
'
Public Function Find(ApplicationHandle As String, LawyerHandle As String, Optional Column As String) As Collection
  Dim Handles(1) As String
  Handles(0) = ApplicationHandle
  Handles(1) = LawyerHandle
  Set Find = mDataSet.DataProvider.FindMulti(mHandleNames, Handles)
End Function


'
' Interface to server
'

'
' Create an application.
'
Public Function CreateApplicationLawyer(ApplicationId As String, _
                                        LawyerId As String, _
                                        RetroactiveDate As Date, _
                                        inPracticeDuration As Double, _
                                        PositionId As String, _
                                        CoverageId As String) As Variant
On Error GoTo ErrorHandler
  CreateApplicationLawyer = Client.Create(ApplicationId, _
                                          LawyerId, _
                                          RetroactiveDate, _
                                          inPracticeDuration, _
                                          PositionId, _
                                          CoverageId)
  Exit Function


ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client

End Function

'
' Write an application.
'
Public Function WriteApplicationLawyer(ApplicationId As String, _
                                        LawyerId As String, _
                                        LawyerName As String, _
                                        RetroactiveDate As Date, _
                                        inPracticeDuration As Double, _
                                        PositionId As String, _
                                        CoverageId As String) As Boolean

On Error GoTo ErrorHandler


  Client.Write ApplicationId, _
               LawyerId, _
               LawyerName, _
               RetroactiveDate, _
               inPracticeDuration, _
               PositionId, _
               CoverageId
  WriteApplicationLawyer = True
  Exit Function

ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  WriteApplicationLawyer = False
End Function

'
' Delete an ApplicationLawyer
'
Public Function DeleteApplicationLawyer(ApplicationId As String, LawyerId As String) As Boolean

On Error GoTo ErrorHandler
  Call Client.Delete(ApplicationId, LawyerId)
  DeleteApplicationLawyer = True
  Exit Function

ErrorHandler:
  ErrorUtil.DisplaySoapError Err, Client
  DeleteApplicationLawyer = False
End Function


'
' Prepare
'

Public Function ApplicationLawyerPrepare(ApplicationId As String, LawyerId As String) As Collection
On Error GoTo Exception

  Dim dblArray As Variant
  dblArray = Client.Prepare(ApplicationId, LawyerId)
  Dim Result As New Collection
  'popuplate the collection
  Dim I As Integer
  For I = 0 To SafeUBound(dblArray)
    Result.Add dblArray(I)(1), dblArray(I)(0)
  Next I
  Set ApplicationLawyerPrepare = Result
  Exit Function
    
Exception:
  ErrorUtil.DisplaySoapError Err, Client
  Set ApplicationLawyerPrepare = New Collection
End Function

