Attribute VB_Name = "BrowseForFolderAPI"
Option Explicit

Private Type BrowseInfo
    hWndOwner As Long
    pIDLRoot As Long
    pszDisplayName As Long
    lpszTitle As Long
    ulFlags As Long
    lpfnCallback As Long
    lParam As Long
    iImage As Long
End Type

'Browsing type.
Public Enum BrowseType
    BrowseForFolders = &H1
    BrowseForComputers = &H1000
    BrowseForPrinters = &H2000
    BrowseForEverything = &H4000
End Enum

'Folder Type
Public Enum FolderType
    CSIDL_BITBUCKET = 10
    CSIDL_CONTROLS = 3
    CSIDL_DESKTOP = 0
    CSIDL_DRIVES = 17
    CSIDL_FONTS = 20
    CSIDL_NETHOOD = 18
    CSIDL_NETWORK = 19
    CSIDL_PERSONAL = 5
    CSIDL_PRINTERS = 4
    CSIDL_PROGRAMS = 2
    CSIDL_RECENT = 8
    CSIDL_SENDTO = 9
    CSIDL_STARTMENU = 11
End Enum

''Second Source
'Public Type SHITEMID                        ' Shell item identifier
'     cb As Long
'     abID As Byte
'End Type
'
'Public Type ITEMIDLIST                      ' Shell item identifier list
'     mkid As SHITEMID
'End Type


Private Const MAX_PATH = 260
Private Declare Sub CoTaskMemFree Lib "ole32.dll" (ByVal hMem As Long)
Private Declare Function lstrcat Lib "kernel32.dll" Alias "lstrcatA" (ByVal lpString1 As String, ByVal lpString2 As String) As Long
Private Declare Function SHBrowseForFolder Lib "shell32.dll" (lpbi As BrowseInfo) As Long
Private Declare Function SHGetPathFromIDList Lib "shell32.dll" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
Private Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hWndOwner As Long, ByVal nFolder As Long, ListId As Long) As Long



'=======================================================================
' BrowseFolders hWnd, "Select a Folder", BrowseForFolders, CSIDL_NETHOOD
'
' BrowseFolders hWnd, "Select a Folder for EFT File", BrowseForFolders, CSIDL_NETHOOD
'
'
'=======================================================================

Public Function BrowseFolders(hWndOwner As Long, sMessage As String, Browse As BrowseType, ByVal RootFolder As FolderType) As String
    Dim Nullpos As Integer
    Dim lpIDList As Long
    Dim res As Long
    Dim sPath As String
    Dim BInfo As BrowseInfo
    Dim RootID As Long

    SHGetSpecialFolderLocation hWndOwner, RootFolder, RootID
    BInfo.hWndOwner = hWndOwner
    BInfo.lpszTitle = lstrcat(sMessage, "")
    BInfo.ulFlags = Browse
    If RootID <> 0 Then BInfo.pIDLRoot = RootID
    lpIDList = SHBrowseForFolder(BInfo)
    If lpIDList <> 0 Then
        sPath = String(MAX_PATH, 0)
        res = SHGetPathFromIDList(lpIDList, sPath)
        Call CoTaskMemFree(lpIDList)
        Nullpos = InStr(sPath, vbNullChar)
        If Nullpos <> 0 Then
            sPath = Left(sPath, Nullpos - 1)
        End If
    End If
    BrowseFolders = sPath
End Function
'
'Public Function BrowseForFolder( _
'  ByVal hwnd As Long, _
'  Optional ByVal TopFolder As FolderType = CSIDL_DESKTOP, _
'  Optional ByVal Title As String = "Select folder", _
'  Optional ByVal Flags As BrowseType = BrowseForFolders, _
'  Optional ByVal DefaultFolder As String) As String
''===========================================================================
''   BrowseForFolder - Allows the user to browse for a folder using
''   the shell namespace.
''
''   TopFolder       Identifies the root folder.
''   Title           Attaches a description above the TreeView.
''   Flags           Misc flags defining behavior.
''   DefaultFolder   Reserved.
''===========================================================================
'
'  Dim bi      As BrowseInfo   ' input structure used by Shell API
'  Dim idl     As ITEMIDLIST   ' input structure used by Shell API
'  Dim Result  As Long         ' return code
'  Dim Pidl    As Long         ' pointer to an Item ID List
'  Dim Path    As String       ' resulting path returned
'  Dim Pos     As Integer      ' parsing index
'
'  ' Return default value if canceled
'  BrowseForFolder = DefaultFolder
'
'  With bi
'    .hWndOwner = hwnd
'
'
'    ' Convert virtual folder to pidl
'    Result = SHGetSpecialFolderLocation(hwnd, TopFolder, idl)
'    .pIDLRoot = idl.mkid.cb
'
'    ' set the dialog title
'    .lpszTitle = Title
'    ' input options
'    .ulFlags = Flags
'
'    ' Give callback info about default folder
'    If Len(DefaultFolder) > 0 Then
'
'      ' Set address of callback function
'      .lpfn = ProcAddress(AddressOf BrowseCallbackProc)
'
'      ' Convert to Ansi if not on NT
'      If Not IsWinNt Then
'        DefaultFolder = StrConv(DefaultFolder, vbFromUnicode)
'      End If
'
'      ' Store pointer to default folder
'      .lParam = StrPtr(DefaultFolder)
'
'    End If
'
'  End With
'
'  ' display browse dialog
'  Pidl = SHBrowseForFolder(bi)
'
'  ' Make sure Cancel not clicked
'  If Pidl <> 0 Then
'
'    ' size of the buffer to fit & pad with nulls
'    Path = Space$(519) & vbNullChar
'
'    ' copy the path to local buffer
'    If CBool(SHGetPathFromIDList(Pidl, Path)) Then
'      ' strip off null terminators
'      Pos = InStr(Path, Chr$(0))
'      BrowseForFolder = Left$(Path, Pos - 1)
'    End If
'
'    ' Release the pidl!
'    CoTaskMemFree Pidl
'
'  End If
'
'End Function
