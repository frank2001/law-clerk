VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmIncidentEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Incident Edit"
   ClientHeight    =   6270
   ClientLeft      =   375
   ClientTop       =   1335
   ClientWidth     =   8685
   Icon            =   "frmIncidentEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   6270
   ScaleWidth      =   8685
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6252
      Left            =   0
      ScaleHeight     =   6255
      ScaleWidth      =   8685
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   0
      Width           =   8688
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   6156
         TabIndex        =   51
         Top             =   5880
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   7404
         TabIndex        =   52
         Top             =   5880
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   5700
         Left            =   96
         TabIndex        =   40
         Top             =   84
         Width           =   8508
         _ExtentX        =   15002
         _ExtentY        =   10054
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   5
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmIncidentEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Frame1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraReinsureReport"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Frame5"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Frame4"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Frame3"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Law &Area"
         TabPicture(1)   =   "frmIncidentEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "cmdItemRemove(0)"
         Tab(1).Control(1)=   "cmdItemSelect(0)"
         Tab(1).Control(2)=   "cmdItemDown(0)"
         Tab(1).Control(3)=   "cmdItemUp(0)"
         Tab(1).Control(4)=   "lvItem(0)"
         Tab(1).ControlCount=   5
         TabCaption(2)   =   "La&w Error"
         TabPicture(2)   =   "frmIncidentEdit.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "cmdItemRemove(1)"
         Tab(2).Control(1)=   "cmdItemSelect(1)"
         Tab(2).Control(2)=   "cmdItemDown(1)"
         Tab(2).Control(3)=   "cmdItemUp(1)"
         Tab(2).Control(4)=   "lvItem(1)"
         Tab(2).ControlCount=   5
         TabCaption(3)   =   "N&ote"
         TabPicture(3)   =   "frmIncidentEdit.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "txtNote"
         Tab(3).ControlCount=   1
         Begin VB.Frame Frame3 
            Caption         =   "Firm"
            Height          =   576
            Left            =   96
            TabIndex        =   48
            Top             =   900
            Width           =   5544
            Begin VB.CommandButton cmdFirmSelect 
               Caption         =   "&Firm"
               Height          =   288
               Left            =   4428
               TabIndex        =   2
               Top             =   204
               Width           =   1020
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "firmName"
               DataField       =   "firmName"
               DataMember      =   "Firm"
               Height          =   192
               Index           =   4
               Left            =   132
               TabIndex        =   49
               Top             =   252
               Width           =   696
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Policy"
            Height          =   576
            Left            =   5688
            TabIndex        =   46
            Top             =   300
            Width           =   2688
            Begin VB.CommandButton cmdPolicySelect 
               Caption         =   "&Policy"
               Height          =   288
               Left            =   1572
               TabIndex        =   1
               Top             =   204
               Width           =   1020
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "policyNumber"
               DataField       =   "policyNumber"
               DataMember      =   "Policy"
               Height          =   195
               Index           =   5
               Left            =   135
               TabIndex        =   47
               Top             =   255
               Width           =   960
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Claim"
            Height          =   576
            Left            =   5688
            TabIndex        =   44
            Top             =   900
            Width           =   2688
            Begin VB.CommandButton cmdClaimSelect 
               Caption         =   "&Claim"
               Height          =   288
               Left            =   1572
               TabIndex        =   3
               Top             =   204
               Width           =   1020
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "otherInstanceId"
               DataField       =   "otherInstanceId"
               DataMember      =   "Instance"
               Height          =   195
               Index           =   6
               Left            =   135
               TabIndex        =   45
               Top             =   255
               Width           =   1110
            End
         End
         Begin VB.Frame fraReinsureReport 
            Caption         =   "Tortfeasor"
            Height          =   576
            Left            =   96
            TabIndex        =   42
            Top             =   300
            Width           =   5544
            Begin VB.CommandButton cmdLawyerSelect 
               Caption         =   "&Lawyer"
               Height          =   288
               Left            =   4440
               TabIndex        =   0
               Top             =   204
               Width           =   1020
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "lawyerName"
               DataField       =   "lawyerName"
               DataMember      =   "Lawyer"
               Height          =   192
               Index           =   0
               Left            =   132
               TabIndex        =   43
               Top             =   240
               Width           =   912
            End
         End
         Begin VB.TextBox txtNote 
            Height          =   5244
            Left            =   -74892
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   26
            Top             =   348
            Width           =   8292
         End
         Begin VB.CommandButton cmdItemRemove 
            Caption         =   "&Remove"
            Height          =   336
            Index           =   1
            Left            =   -70260
            TabIndex        =   25
            Top             =   1560
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemSelect 
            Caption         =   "&Select"
            Height          =   336
            Index           =   1
            Left            =   -70260
            TabIndex        =   24
            Top             =   1152
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemDown 
            Caption         =   "Move &Down"
            Height          =   336
            Index           =   1
            Left            =   -70260
            TabIndex        =   23
            Top             =   756
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemUp 
            Caption         =   "Move &Up"
            Height          =   336
            Index           =   1
            Left            =   -70260
            TabIndex        =   22
            Top             =   348
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemRemove 
            Caption         =   "&Remove"
            Height          =   336
            Index           =   0
            Left            =   -70260
            TabIndex        =   20
            Top             =   1560
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemSelect 
            Caption         =   "&Select"
            Height          =   336
            Index           =   0
            Left            =   -70260
            TabIndex        =   19
            Top             =   1152
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemDown 
            Caption         =   "Move &Down"
            Height          =   336
            Index           =   0
            Left            =   -70260
            TabIndex        =   18
            Top             =   756
            Width           =   1200
         End
         Begin VB.CommandButton cmdItemUp 
            Caption         =   "Move &Up"
            Height          =   336
            Index           =   0
            Left            =   -70260
            TabIndex        =   17
            Top             =   348
            Width           =   1200
         End
         Begin VB.Frame Frame1 
            Caption         =   "Incident"
            Height          =   1512
            Left            =   108
            TabIndex        =   32
            Top             =   1524
            Width           =   8280
            Begin VB.TextBox txtDataField 
               DataField       =   "incidentId"
               DataMember      =   "Incident"
               Height          =   288
               Index           =   8
               Left            =   984
               TabIndex        =   4
               Text            =   "incidentId"
               Top             =   504
               Width           =   1500
            End
            Begin VB.CheckBox chkDataField 
               Caption         =   "Is Fee Dispute"
               DataField       =   "isFeeDispute"
               DataMember      =   "Instance"
               Height          =   192
               Index           =   0
               Left            =   2712
               TabIndex        =   5
               Top             =   564
               Width           =   1692
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "claimant"
               DataMember      =   "Instance"
               Height          =   576
               Index           =   2
               Left            =   984
               MultiLine       =   -1  'True
               TabIndex        =   6
               Top             =   828
               Width           =   7200
            End
            Begin VB.Label lblDatafield 
               AutoSize        =   -1  'True
               Caption         =   "firmName (snapshot)"
               DataField       =   "firmName"
               DataMember      =   "Instance"
               Height          =   195
               Index           =   1
               Left            =   990
               TabIndex        =   50
               Top             =   240
               Width           =   1440
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Insured:"
               Height          =   192
               Left            =   132
               TabIndex        =   41
               Top             =   240
               Width           =   564
            End
            Begin VB.Label Label9 
               Caption         =   "Identifier:"
               Height          =   192
               Left            =   132
               TabIndex        =   35
               Top             =   564
               Width           =   924
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "Claimant:"
               Height          =   192
               Left            =   132
               TabIndex        =   33
               Top             =   852
               Width           =   660
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Date:"
            Height          =   1272
            Left            =   84
            TabIndex        =   27
            Top             =   3060
            Width           =   8280
            Begin VB.TextBox txtDataField 
               DataField       =   "knowDateRange"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   3
               Left            =   6636
               TabIndex        =   14
               Text            =   "knowDateRange"
               Top             =   540
               Width           =   1548
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "occurDateRange"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   1
               Left            =   3708
               TabIndex        =   11
               Text            =   "occurDateRange"
               Top             =   540
               Width           =   1548
            End
            Begin VB.ComboBox cboDataField 
               DataField       =   "adjuster"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   0
               ItemData        =   "frmIncidentEdit.frx":007C
               Left            =   3720
               List            =   "frmIncidentEdit.frx":0083
               Style           =   2  'Dropdown List
               TabIndex        =   12
               Top             =   876
               Width           =   1548
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmIncidentEdit.frx":0095
               DataField       =   "occurDate"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   0
               Left            =   3720
               TabIndex        =   10
               Top             =   204
               Width           =   1548
               _ExtentX        =   2725
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   110624769
               CurrentDate     =   37109
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmIncidentEdit.frx":00B1
               DataField       =   "knowDate"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   2
               Left            =   6636
               TabIndex        =   13
               Top             =   204
               Width           =   1548
               _ExtentX        =   2725
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   110624769
               CurrentDate     =   37109
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmIncidentEdit.frx":00C4
               DataField       =   "openDate"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   4
               Left            =   828
               TabIndex        =   7
               Top             =   204
               Width           =   1548
               _ExtentX        =   2725
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   110624769
               CurrentDate     =   37109
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmIncidentEdit.frx":00D7
               DataField       =   "closeDate"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   5
               Left            =   828
               TabIndex        =   8
               Top             =   540
               Width           =   1548
               _ExtentX        =   2725
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   110624769
               CurrentDate     =   37109
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmIncidentEdit.frx":00EA
               DataField       =   "reportDate"
               DataMember      =   "Instance"
               Height          =   288
               Index           =   1
               Left            =   816
               TabIndex        =   9
               Top             =   876
               Width           =   1548
               _ExtentX        =   2725
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   110624769
               CurrentDate     =   37109
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "Open:"
               Height          =   192
               Left            =   132
               TabIndex        =   39
               Top             =   252
               Width           =   432
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               Caption         =   "Close:"
               Height          =   192
               Left            =   120
               TabIndex        =   38
               Top             =   588
               Width           =   456
            End
            Begin VB.Label Label13 
               AutoSize        =   -1  'True
               Caption         =   "Adjuster:"
               Height          =   192
               Left            =   2604
               TabIndex        =   37
               Top             =   924
               Width           =   624
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Report:"
               Height          =   192
               Left            =   132
               TabIndex        =   36
               Top             =   924
               Width           =   528
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Occur Range:"
               Height          =   192
               Left            =   2604
               TabIndex        =   31
               Top             =   588
               Width           =   984
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "Know:"
               Height          =   192
               Left            =   5520
               TabIndex        =   30
               Top             =   252
               Width           =   420
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               Caption         =   "Know  Range:"
               Height          =   192
               Left            =   5520
               TabIndex        =   29
               Top             =   588
               Width           =   984
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Occur:"
               Height          =   192
               Left            =   2604
               TabIndex        =   28
               Top             =   252
               Width           =   456
            End
         End
         Begin MSComctlLib.ListView lvItem 
            Height          =   4992
            Index           =   0
            Left            =   -74904
            TabIndex        =   16
            Top             =   336
            Width           =   4548
            _ExtentX        =   8017
            _ExtentY        =   8811
            SortKey         =   1
            View            =   3
            LabelEdit       =   1
            Sorted          =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            Checkboxes      =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   10583
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   0
            EndProperty
         End
         Begin MSComctlLib.ListView lvItem 
            Height          =   4992
            Index           =   1
            Left            =   -74904
            TabIndex        =   21
            Top             =   336
            Width           =   4548
            _ExtentX        =   8017
            _ExtentY        =   8811
            SortKey         =   1
            View            =   3
            LabelEdit       =   1
            Sorted          =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            Checkboxes      =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   10583
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   0
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   348
      TabIndex        =   34
      Top             =   816
      Width           =   1056
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Incident"
         End
         Begin VB.Menu mnuSeperator429 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Claim"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Firm"
            Index           =   2
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Lawyer"
            Index           =   3
         End
         Begin VB.Menu mnuFileOpenOther 
            Caption         =   "&Policy"
            Index           =   4
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d to"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeperator665 
         Caption         =   "-"
      End
      Begin VB.Menu mnuClearSelection 
         Caption         =   "&Clear Selections"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator996 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator945 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmIncidentEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.70.00 (jjm) 2004 09 23
'ver 2.50.00 (jjm) 2004 02 19
'ver 2.50.00 (jjm) 2004 02 10
'ver 2.40.0107 (jjm)
'ver 2.20.0611 (jjm)

Option Explicit

Private mIncidentHandle As String

Private mClaimHandle As String
Private mFirmHandle As String
Private mLawyerHandle As String
Private mEventHandle As String
Private mPolicyHandle As String


Private mDataSet As Dataset

'Private mListIndex As Integer
'Private mListView As ListView

Private mNew As Boolean
Private mDirty As Boolean
Private mCancel As Boolean

Private mLawArea As Collection
Private mLawError As Collection

Private mTab As Integer

'ver 2.72.00 (jjm) 2005 05 06 - removed claimant search
'ver 2.72.00 (jjm) 2005 04 21
'Private IsNullClaimant As Boolean

Private FormMgr As New FormManager

Private Const TXT_CLAIMANT_INDEX = 2

Private Const LBL_CLAIMNUMBER_INDEX = 6
Private Const LBL_FIRMNAME_INDEX = 4
Private Const LBL_INSTNACEFIRMNAME_INDEX = 1
Private Const LBL_LAWYERNAME_INDEX = 0
Private Const LBL_POLICYNUMBER_INDEX = 5



Public Sub AddItem(View As ListView, Optional Key As String, Optional Text As String, Optional Checked As Boolean, Optional Tag As String)
  Dim SortKey As String
  SortKey = Format(View.ListItems.Count + 1, "0000")
  
  If Key = vbNullString Then
    Key = "A" & SortKey
  End If
  
  With View.ListItems.Add(, Key, Text)
    .Checked = Checked
    .Tag = Tag
    .ListSubItems.Add , "SortKey", SortKey
  End With
End Sub

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, False
  End If
End Property

Public Function ItemArray(View As ListView) As String()
  Dim item As ListItem, List As String
  For Each item In View.ListItems
    If item.Checked Then
      List = List & IIf(List = vbNullString, "", "<!-- -->") & item.Tag
    End If
  Next item

  If List <> vbNullString Then
    ItemArray = Split(List, "<!-- -->")
  End If
End Function

Public Sub ButtonRefresh(Index As Integer)
  With lvItem(Index).SelectedItem
    cmdItemUp(Index).Enabled = (.Index > 1)
    cmdItemDown(Index).Enabled = (.Index < lvItem(Index).ListItems.Count)
    cmdItemSelect(Index).Enabled = Not .Checked
    cmdItemRemove(Index).Enabled = .Checked
  End With
End Sub

Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Show
  Refresh
  Select Case fInterface.Message
    Case fimNone
      Show
      Exit Sub
    Case fimNew
      'Code not to get caught up in some other forms unload
      Show
      Exit Sub
    Case fimSelect
      Select Case fInterface.Key
        Case DatabaseName.FIRM_TABLE
          mFirmHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRM_KEY)
          lbldatafield(LBL_INSTNACEFIRMNAME_INDEX).Caption = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.FIRMENTITYNAME_FIELD)
          lbldatafield(LBL_FIRMNAME_INDEX).Caption = lbldatafield(LBL_INSTNACEFIRMNAME_INDEX).Caption
        Case DatabaseName.LAWYER_TABLE
          mLawyerHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LAWYER_KEY)
          mFirmHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LATESTFIRMID_FIELD)
          'display lawyer name. Pulls data from search. The search columns must contain the lawyerEntityName to function
          lbldatafield(LBL_LAWYERNAME_INDEX).Caption = _
                ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LAWYERENTITYNAME_FIELD)
          'auto grab firm
          If mFirmHandle <> vbNullString Then
            Dim FirmCol As New Collection
            Set FirmCol = Factory.FirmService.FindView(mFirmHandle, _
                    DatabaseName.LATESTPOLICYID_FIELD & ", " & DatabaseName.FIRMENTITYNAME_FIELD)
            lbldatafield(LBL_INSTNACEFIRMNAME_INDEX).Caption = _
                    GetDataFromCollection(FirmCol, DatabaseName.FIRMENTITYNAME_FIELD)
            lbldatafield(LBL_FIRMNAME_INDEX).Caption = GetDataFromCollection(FirmCol, DatabaseName.FIRMENTITYNAME_FIELD)
            
            mPolicyHandle = GetDataFromCollection(FirmCol, DatabaseName.LATESTPOLICYID_FIELD)
            'If there is a policyId in the firm table set the incidentName
            'by the firmName in the Policy table
            If mPolicyHandle <> vbNullString Then
              Dim PolicyCol As New Collection
              Set PolicyCol = Factory.PolicyService.Find(mPolicyHandle, DatabaseName.FIRMNAME_FIELD & "," & DatabaseName.POLICYNUMBER_FIELD)
              lbldatafield(LBL_FIRMNAME_INDEX).Caption = GetDataFromCollection(PolicyCol, DatabaseName.FIRMNAME_FIELD)
              lbldatafield(LBL_POLICYNUMBER_INDEX).Caption = GetDataFromCollection(PolicyCol, DatabaseName.POLICYNUMBER_FIELD)
            Else
              lbldatafield(LBL_FIRMNAME_INDEX).Caption = FirmCol(DatabaseName.FIRMENTITYNAME_FIELD)
            End If
          
          End If
        Case DatabaseName.POLICY_TABLE
          If fInterface.Child.ReturnNull Then
            lbldatafield(LBL_POLICYNUMBER_INDEX).Caption = vbNullString
            lbldatafield(1).Caption = vbNullString
            mPolicyHandle = vbNullString
          Else
            mPolicyHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.POLICY_KEY)
            Dim PolData As Collection
            Set PolData = Factory.PolicyService.Find(mPolicyHandle)
            lbldatafield(LBL_POLICYNUMBER_INDEX).Caption = _
                    GetDataFromCollection(PolData, DatabaseName.POLICYNUMBER_FIELD)
            lbldatafield(1).Caption = _
                    GetDataFromCollection(PolData, DatabaseName.FIRMNAME_FIELD)

          End If
        Case DatabaseName.CLAIM_TABLE
          mClaimHandle = IIf(fInterface.Child.ReturnNull, vbNullString, _
                              ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.CLAIM_KEY))
          lbldatafield(LBL_CLAIMNUMBER_INDEX).Caption = DataUtil.FindHandle(DatabaseName.CLAIM_TABLE, DatabaseName.CLAIM_KEY, mClaimHandle, DatabaseName.CLAIMNUMBER_FIELD)
      End Select
    Case fimSave
      Show
      Exit Sub
  End Select
    
  Dirty = True
  MenuRefresh
  Show
End Sub

Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub

Private Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  'Clear the fields
  ClearDataField txtDataField
  ClearDataField dtpDataField
  ClearDataField lbldatafield
  ClearDataField cboDataField
  ClearDataField chkDatafield
  
  'Clear the handles
  mFirmHandle = vbNullString
  mLawyerHandle = vbNullString
  mPolicyHandle = vbNullString
  
  
  'ver 2.70 jjm 2004 11 22
  Dim DateControl As DTPicker
  For Each DateControl In dtpDataField
    DateControl.Value = Today
  Next DateControl
    
  If (mNew) Then
    Me.Caption = "New Incident"
    mIncidentHandle = vbNullString
    
    dtpDataField(0).Value = vbNullString
    dtpDataField(2).Value = vbNullString
    dtpDataField(5).Value = vbNullString
    'ver 2.72.00 (jjm) 2005 05 06 - removed claimant search
    'IsNullClaimant = True
  Else
    Dim Row As Collection
    Set Row = Factory.IncidentService.Find(mIncidentHandle)
    Caption = "Edit Incident " & mIncidentHandle
    SetFields txtDataField, Row, DatabaseName.INCIDENT_TABLE
    
    'Set Instance data
    Dim InstanceData As Collection
    Set InstanceData = Factory.InstanceService.Find(mIncidentHandle)
    SetFields dtpDataField, InstanceData, "Instance"
    SetFields cboDataField, InstanceData, "Instance"
    SetFields chkDatafield, InstanceData, "Instance"
    SetFields lbldatafield, InstanceData, "Instance"
    SetFields txtDataField, InstanceData, "Instance"
    'Get the handles from instance
    mClaimHandle = GetDataFromCollection(InstanceData, DatabaseName.OTHERINSTANCEID_FIELD)
    mFirmHandle = GetDataFromCollection(InstanceData, DatabaseName.FIRM_KEY)
    mLawyerHandle = GetDataFromCollection(InstanceData, DatabaseName.MAINLAWYERID_FIELD)
    mPolicyHandle = GetDataFromCollection(InstanceData, DatabaseName.POLICY_KEY)
    'set policy data
    Dim PolicyData As Collection
    Set PolicyData = Factory.PolicyService.Find(mPolicyHandle, DatabaseName.POLICYNUMBER_FIELD)
    SetFields lbldatafield, PolicyData, "Policy"
    
    'Firm / Lawyer data
    'Manually set the Firm and Lawyer name fields
    lbldatafield(LBL_LAWYERNAME_INDEX).Caption = GetDataFromCollection(Factory.EntityService.Find(mLawyerHandle, DatabaseName.ENTITYNAME_FIELD), DatabaseName.ENTITYNAME_FIELD)
    lbldatafield(LBL_FIRMNAME_INDEX).Caption = Factory.EntityService.Find(mFirmHandle, DatabaseName.ENTITYNAME_FIELD)(DatabaseName.ENTITYNAME_FIELD)
    
    mEventHandle = InstanceData(DatabaseName.EVENT_KEY)
    
    With txtDataField(8)
      .Locked = True
      .BackColor = picBody.BackColor
    End With
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
    'ver 2.72.00 (jjm) 2005 05 06 - removed claimant search
    'ver 2.72.00 (jjm) 2005 04 21
    'If the Claimant is null then add the claimant search
    'IsNullClaimant = (Row(DatabaseName.CLAIMANT_FIELD) = vbNullString)
    
  End If
  
  LawAreaRefresh
  LawErrorRefresh
  
  MenuRefresh
  
  
  mnuFileOpen.Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  
  ' Refresh navigation controls
'  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' Set change status to false
  Dirty = False
End Sub

Private Sub FirmRefresh()
  If mFirmHandle = vbNullString Then
    ClearDataField lbldatafield, DatabaseName.FIRM_TABLE
  Else
    SetFields lbldatafield, Factory.FirmService.Find(mFirmHandle), DatabaseName.FIRM_TABLE
  End If
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub


Private Sub LawAreaRefresh()
  lvItem(0).ListItems.Clear
  
  Dim Row As Collection
  For Each Row In mLawArea
    AddItem lvItem(0), "Key:" & Row(DatabaseName.LAWAREA_KEY), Row("lawAreaName"), , Row(DatabaseName.LAWAREA_KEY)
  Next Row
  
  Dim Items As Collection, I As Integer
  Set Items = Factory.InstanceService.LawAreaCollection(mIncidentHandle)
  If Items Is Nothing Then
    Exit Sub
  End If
  
  For Each Row In Items
    I = I + 1
    lvItem(0).ListItems.item("Key:" & Row(DatabaseName.LAWAREA_KEY)).Selected = True
    lvItem(0).ListItems.item("Key:" & Row(DatabaseName.LAWAREA_KEY)).Checked = True
    Swap lvItem(0), I
  Next Row
End Sub

Private Sub LawErrorRefresh()
  lvItem(1).ListItems.Clear
  
  Dim Row As Collection
  For Each Row In mLawError
    AddItem lvItem(1), "Key:" & Row(DatabaseName.LAWERROR_KEY), Row("lawErrorName"), , Row(DatabaseName.LAWERROR_KEY)
  Next Row

  Dim Items As Collection, I As Integer
  Set Items = Factory.InstanceService.LawErrorCollection(mIncidentHandle)
  If Items Is Nothing Then
    Exit Sub
  End If
  
  For Each Row In Items
    I = I + 1
    lvItem(1).ListItems.item("Key:" & Row(DatabaseName.LAWERROR_KEY)).Selected = True
    lvItem(1).ListItems.item("Key:" & Row(DatabaseName.LAWERROR_KEY)).Checked = True
    Swap lvItem(1), I
  Next Row
End Sub

Private Sub MenuRefresh()
  mnuFileOpenOther(1).Enabled = (lbldatafield(LBL_CLAIMNUMBER_INDEX).Caption <> vbNullString)
  mnuFileOpenOther(2).Enabled = (mFirmHandle <> vbNullString)
  mnuFileOpenOther(3).Enabled = (mLawyerHandle <> vbNullString)
  mnuFileOpenOther(LBL_FIRMNAME_INDEX).Enabled = (lbldatafield(LBL_POLICYNUMBER_INDEX).Caption <> vbNullString)
End Sub

Public Sub ShowByPrimaryKey(IncidentId As String, Optional ShowTab As Integer)
  mIncidentHandle = IncidentId
  mTab = ShowTab
  Show
End Sub

Public Sub ShowNew()
  mNew = True
  Show
End Sub
'
' Swap simply swaps the SortKey of the selected item with the
' desired index
'
Private Sub Swap(View As ListView, Index As Integer)
  View.Sorted = False
  
  Dim Selected As ListItem
  Set Selected = View.SelectedItem
  
  Dim item As ListItem
  Set item = View.ListItems(Index)
  
  Dim SortKey As String
  SortKey = Selected.ListSubItems("SortKey")
  Selected.ListSubItems("SortKey") = item.ListSubItems("SortKey")
  item.ListSubItems("SortKey") = SortKey
  
  View.Sorted = True
  View.ListItems(Index).Selected = True
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub chkDatafield_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub


'ver 2.50.00 (jjm) 2004 02 19
Private Sub cmdClaimSelect_Click()
  Dispatcher.ShowSearch DatabaseName.CLAIM_TABLE, FormMgr, True, , , , , True
End Sub

Private Sub cmdFirmSelect_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True
End Sub

Private Sub cmdLawyerSelect_Click()
  Dispatcher.ShowSearch DatabaseName.LAWYER_TABLE, FormMgr, True
End Sub

Private Sub cmdPolicySelect_Click()
 'ver 2.50.00 (jjm) 2004 02 17 - call it to allow a select none
  Dispatcher.ShowSearch DatabaseName.POLICY_TABLE, FormMgr, True, , , , , True
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdItemDown_Click(Index As Integer)
  Swap lvItem(Index), lvItem(Index).SelectedItem.Index + 1
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdItemRemove_Click(Index As Integer)
  lvItem(Index).SelectedItem.Checked = False
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdItemSelect_Click(Index As Integer)
  lvItem(Index).SelectedItem.Checked = True
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdItemUp_Click(Index As Integer)
  Swap lvItem(Index), lvItem(Index).SelectedItem.Index - 1
  ButtonRefresh Index
  lvItem(Index).SetFocus
  Dirty = True
End Sub

Private Sub cmdSave_Click()
  If Save Then Unload Me
End Sub


Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub Form_Initialize()
   Set mDataSet = New IncidentDataset
   Set mDataSet.Session = LawSession
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  FormUtil.EditLoad Me
  FormMgr.Load Me
  
  Set mLawArea = Factory.LawAreaService.NameCollection
  Set mLawError = Factory.LawErrorService.NameCollection
  
  'Ver 2.40.0107 (jjm)
  'Load the AdjusterNames ComboBox
  FormUtil.ComboBoxRegistryLoad cboDataField(0), "adjusternames"

  'Ensure first tab is open.
  If SSTab1.Tab <> mTab Then
    SSTab1.Tab = mTab
  End If
  
  FormRefresh
  FormResize
  
  FormUtil.TabControlEnable SSTab1
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub


Private Sub lvItem_Click(Index As Integer)
  ButtonRefresh Index
  Dirty = True
End Sub

Private Sub mnuClearSelection_Click()
  mLawyerHandle = vbNullString
  mFirmHandle = vbNullString
  mPolicyHandle = vbNullString
  lbldatafield(LBL_POLICYNUMBER_INDEX).Caption = vbNullString
  lbldatafield(LBL_CLAIMNUMBER_INDEX).Caption = vbNullString
  lbldatafield(1).Caption = vbNullString
  lbldatafield(LBL_LAWYERNAME_INDEX).Caption = vbNullString
  lbldatafield(LBL_FIRMNAME_INDEX).Caption = vbNullString
  Dirty = True
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileOpen_Click()
  FormUtil.ShowByPrimaryKey New frmIncidentOpen, mIncidentHandle, FormMgr
End Sub

Private Sub mnuFileOpenOther_Click(Index As Integer)
  Select Case Index
    Case 1 'claim 6
      FormUtil.ShowByPrimaryKey New frmClaimOpen, lbldatafield(LBL_CLAIMNUMBER_INDEX).Caption, FormMgr
    Case 2 'firm
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mFirmHandle, FormMgr
    Case 3 'lawyer
      FormUtil.ShowByPrimaryKey New frmLawyerOpen, mLawyerHandle, FormMgr
    Case 4 'policy 5
      FormUtil.ShowByPrimaryKey New frmPolicyOpen, lbldatafield(5).Caption, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.INCIDENT_TABLE, mIncidentHandle
  End If
End Sub

Private Sub mnuFileSendToFolder_Click()
'  FormUtil.SendToFolder DatabaseName.CLAIM_REF, DatabaseName.CLAIM_TABLE, mClaimHandle, mClaimHandle & " " & lblDataField(1)
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuFileNew_Click()
  If Not Save(True) Then Exit Sub
  ClearDataField txtDataField
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.INCIDENT_TABLE, FormMgr
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code - Updated to reflect no mNew
  
  ' If there have been changes, and
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And Verify Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Begin custom code
  
  'ver 2.72.00 (jjm) 2005 05 06 - removed claimant search
  'ver 2.72.00 (jjm) 2005 04 21
  'Check for a duplicate claimant
  'If IsNullClaimant Then
  '  If ClaimUtil.isDuplicateClaimantMSG(mIncidentHandle, txtDataField(TXT_CLAIMANT_INDEX).Text) Then
  '    Exit Function
  '  End If
  'End If
  
  Dim mDataControls As New Collection
  AddTextControl txtDataField, mDataControls
  'AddCaptionControl lblDatafield, mDataControls
  AddValueControl dtpDataField, mDataControls
  AddValueControl chkDatafield, mDataControls
  AddTextControl cboDataField, mDataControls
  
  ' Claim Edit does not support adding of new claims.  New claims
  ' created via browser based interface.

  Dim lawAreas() As String
  Dim lawErrors() As String
  lawAreas = ItemArray(lvItem(0))
  lawErrors = ItemArray(lvItem(1))
    
  If (mNew) Then
    mIncidentHandle = Factory.IncidentService.CreateIncident( _
        GetDataFromCollection(mDataControls, DatabaseName.INCIDENT_KEY), _
        mClaimHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.CLAIMANT_FIELD), _
        mFirmHandle, mLawyerHandle, mPolicyHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.OCCURDATERANGE_FIELD), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.OCCURDATE_FIELD)), _
        GetDataFromCollection(mDataControls, DatabaseName.KNOWDATERANGE_FIELD), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.KNOWDATE_FIELD)), _
        GetDataFromCollection(mDataControls, DatabaseName.ADJUSTER_FIELD), _
        IIf(chkDatafield(0).Value, True, False), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.REPORTDATE_FIELD)), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.OPENDATE_FIELD)), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.CLOSEDATE_FIELD)), _
        lawAreas, lawErrors, txtNote.Text)
    Save = mIncidentHandle <> vbNullString
    
    'ver 2.70.00 (jjm) 2004 09 23
    'If successfull, complete the Incident Event
    If Save Then
      'Get EventId
      Dim EventHandles(0) As String
      EventHandles(0) = Factory.InstanceService.Find(mIncidentHandle, DatabaseName.EVENT_KEY)(DatabaseName.EVENT_KEY)
      If Not Factory.EventService.SetComplete(EventHandles, vbNullStringArray, vbNullStringArray) Then
        MsgBox "The Save was successfull, however the Incident Event was unable to be completed.", vbCritical
      End If
    End If
  Else
    Save = Factory.IncidentService.WriteIncident( _
        mIncidentHandle, mClaimHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.CLAIMANT_FIELD, True), _
        mFirmHandle, mLawyerHandle, mPolicyHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.OCCURDATERANGE_FIELD, True), _
        GetDate(mDataControls(DatabaseName.OCCURDATE_FIELD)), _
        GetDataFromCollection(mDataControls, DatabaseName.KNOWDATERANGE_FIELD), _
        GetDate(GetDataFromCollection(mDataControls, DatabaseName.KNOWDATE_FIELD, True)), _
        GetDataFromCollection(mDataControls, DatabaseName.ADJUSTER_FIELD, True), _
        chkDatafield(0).Value, _
        GetDate(mDataControls(DatabaseName.REPORTDATE_FIELD)), _
        GetDate(mDataControls(DatabaseName.OPENDATE_FIELD)), _
        GetDate(mDataControls(DatabaseName.CLOSEDATE_FIELD)), _
        lawAreas, lawErrors, txtNote.Text)
  End If

  If Save Then
    FormMgr.Message = fimSave
    Dirty = False
  End If
End Function

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
