VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmArrangeCustom 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Arrange Custom Icons"
   ClientHeight    =   3900
   ClientLeft      =   36
   ClientTop       =   312
   ClientWidth     =   5196
   Icon            =   "frmArrangeCustom.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3900
   ScaleWidth      =   5196
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   3840
      TabIndex        =   7
      Top             =   3468
      Width           =   1200
   End
   Begin VB.Frame fraLine 
      Height          =   36
      Left            =   132
      TabIndex        =   9
      Top             =   3276
      Width           =   4932
   End
   Begin VB.CommandButton cmdExclude 
      Caption         =   "&Exclude"
      Height          =   336
      Left            =   3672
      TabIndex        =   4
      Top             =   1992
      Width           =   1200
   End
   Begin VB.CommandButton cmdInclude 
      Caption         =   "&Include"
      Height          =   336
      Left            =   3672
      TabIndex        =   3
      Top             =   1596
      Width           =   1200
   End
   Begin MSComctlLib.ListView lvView 
      Height          =   2412
      Left            =   168
      TabIndex        =   0
      Top             =   792
      Width           =   3348
      _ExtentX        =   5906
      _ExtentY        =   4255
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   336
      Left            =   2556
      TabIndex        =   6
      Top             =   3468
      Width           =   1200
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   336
      Left            =   1284
      TabIndex        =   5
      Top             =   3468
      Width           =   1200
   End
   Begin VB.CommandButton cmdItemDown 
      Caption         =   "Move &Down"
      Height          =   336
      Left            =   3672
      TabIndex        =   2
      Top             =   1200
      Width           =   1200
   End
   Begin VB.CommandButton cmdItemUp 
      Caption         =   "Move &Up"
      Height          =   336
      Left            =   3672
      TabIndex        =   1
      Top             =   792
      Width           =   1200
   End
   Begin VB.Label lblInstructions 
      Caption         =   "Check the icons that you would like to make visible in this menu. Use the Move Up and Move Down buttons to reorder the options."
      Height          =   588
      Index           =   0
      Left            =   204
      TabIndex        =   8
      Top             =   108
      Width           =   4680
   End
End
Attribute VB_Name = "frmArrangeCustom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public OK As Boolean

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Sub cmdDefault_Click()
  '
  'Need to add code for a reset to default settings
  '
End Sub

Private Sub cmdExclude_Click()
  lvView.SelectedItem.Checked = False
  RefreshButtons
  lvView.SetFocus
End Sub

'
' Swap simply swaps the SortKey of the selected item with the
' desired index
'
Private Sub Swap(Index As Integer)
  lvView.Sorted = False
  
  Dim Selected As ListItem
  Set Selected = lvView.SelectedItem
  
  Dim Item As ListItem
  Set Item = lvView.ListItems(Index)
  
  Dim SortKey As String
  SortKey = Selected.ListSubItems("SortKey")
  Selected.ListSubItems("SortKey") = Item.ListSubItems("SortKey")
  Item.ListSubItems("SortKey") = SortKey
  
  lvView.Sorted = True
  lvView.ListItems(Index).Selected = True
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub cmdItemDown_Click()
  Swap lvView.SelectedItem.Index + 1
  RefreshButtons
  lvView.SetFocus
End Sub

Private Sub cmdItemUp_Click()
  Swap lvView.SelectedItem.Index - 1
  RefreshButtons
  lvView.SetFocus
End Sub

Private Sub cmdOK_Click()
  OK = True
  Me.Hide
End Sub


Private Sub cmdInclude_Click()
  lvView.SelectedItem.Checked = True
  RefreshButtons
  lvView.SetFocus
End Sub

Public Sub AddItem(Index As Integer, Key As String, Text As String, Alignment As Integer, Width As Integer, Position As Integer, Tag As String, Required As Boolean)
  'cboItem.AddItem Text
  
  Dim Item As ListItem
  Set Item = lvView.ListItems.Add(, Key, Text)
  
  Item.Checked = (Position > 0)
  Item.Tag = Tag
  
  Dim SortKey As String
  SortKey = IIf(Position = 0, "B" & Text, "A" & Format(Position, "000"))
  Item.ListSubItems.Add , "SortKey", SortKey
  Item.ListSubItems.Add , "Index", Index
  Item.ListSubItems.Add , "Alignment", Alignment
  Item.ListSubItems.Add , "Width", Width
  Item.ListSubItems.Add , "Required", Required
End Sub

'
' Method requires that the section have at least one column.
'
Public Sub LoadItems(Ini As IniFile, Section As String)
  ListViewModule.DeserializeColumnSettings Me, Ini, Section
  
  lvView.SortKey = 1
  lvView.Sorted = True
  lvView.ListItems(1).Selected = True

  RefreshButtons
  lvView_ItemClick Nothing
'  cboItem.ListIndex = 0
End Sub

Public Sub RefreshButtons()
  Dim Item As ListItem
  Set Item = lvView.SelectedItem
  cmdItemUp.Enabled = (Item.Index > 1)
  cmdItemDown.Enabled = (Item.Index < lvView.ListItems.Count)
  cmdInclude.Enabled = Not Item.Checked
  cmdExclude.Enabled = Item.Checked And Not Item.ListSubItems("Required")
End Sub

' Dialogs are centered.

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If

End Sub

Private Sub lvView_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  If Not Item.Checked And Item.ListSubItems("Required") Then
    Beep
    MsgBox "The column you have selected is required. To hide the column set the column width to 0 under properties.", vbOKOnly, "Required Column"
    Item.Checked = True
  End If
  RefreshButtons
End Sub

Private Sub lvView_ItemClick(ByVal Item As MSComctlLib.ListItem)
  RefreshButtons
End Sub

Public Property Get Column() As ListView
  Set Column = lvView
End Property

