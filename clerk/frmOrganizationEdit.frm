VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmOrganizationEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   7575
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   10740
   Icon            =   "frmOrganizationEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7575
   ScaleWidth      =   10740
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   7125
      Left            =   0
      ScaleHeight     =   7125
      ScaleWidth      =   10740
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   390
      Width           =   10740
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   9405
         TabIndex        =   22
         Top             =   6615
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   336
         Left            =   8145
         TabIndex        =   21
         Top             =   6615
         Width           =   1200
      End
      Begin TabDlg.SSTab sstDetail 
         Height          =   6345
         Left            =   60
         TabIndex        =   23
         Top             =   120
         Width           =   10560
         _ExtentX        =   18627
         _ExtentY        =   11192
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "&General"
         TabPicture(0)   =   "frmOrganizationEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraOrg"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "frmLocation"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraFirm"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "N&ote"
         TabPicture(1)   =   "frmOrganizationEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtNote"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "cmdClearNotepad"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).ControlCount=   2
         Begin VB.Frame fraFirm 
            Caption         =   "Firm"
            Height          =   720
            Left            =   105
            TabIndex        =   52
            Top             =   2220
            Width           =   10335
            Begin VB.TextBox txtDatafield 
               DataField       =   "stateBarNumber"
               DataMember      =   "Firm"
               Height          =   288
               Index           =   2
               Left            =   1065
               TabIndex        =   8
               Top             =   298
               Width           =   1095
            End
            Begin MSComCtl2.DTPicker dtpDataField 
               Bindings        =   "frmOrganizationEdit.frx":0044
               DataField       =   "nextEffectiveDate"
               DataMember      =   "Firm"
               Height          =   285
               Index           =   0
               Left            =   3915
               TabIndex        =   9
               Top             =   300
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   122093569
               CurrentDate     =   37225
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Next Effective Date:"
               Height          =   195
               Left            =   2400
               TabIndex        =   54
               Top             =   345
               Width           =   1440
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               Caption         =   "State Bar Id:"
               Height          =   195
               Left            =   90
               TabIndex        =   53
               Top             =   345
               Width           =   870
            End
         End
         Begin VB.Frame frmLocation 
            Caption         =   "Location"
            Height          =   3225
            Left            =   105
            TabIndex        =   26
            Top             =   3000
            Width           =   10335
            Begin VB.CommandButton cmdLocation 
               Caption         =   "Select Lo&cation"
               Height          =   288
               Left            =   4320
               TabIndex        =   55
               Top             =   330
               Width           =   1365
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine2"
               DataMember      =   "Location"
               Height          =   288
               Index           =   23
               Left            =   996
               TabIndex        =   12
               Top             =   1005
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine3"
               DataMember      =   "Location"
               Height          =   288
               Index           =   22
               Left            =   996
               TabIndex        =   13
               Top             =   1350
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "zip"
               DataMember      =   "Location"
               Height          =   288
               Index           =   19
               Left            =   4272
               TabIndex        =   16
               Top             =   1695
               Width           =   1056
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "addressLine1"
               DataMember      =   "Location"
               Height          =   288
               Index           =   4
               Left            =   996
               TabIndex        =   11
               Top             =   660
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "county"
               DataMember      =   "Location"
               Height          =   288
               Index           =   13
               Left            =   996
               TabIndex        =   17
               Text            =   " "
               Top             =   2040
               Width           =   1554
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "country"
               DataMember      =   "Location"
               Height          =   288
               Index           =   12
               Left            =   3372
               TabIndex        =   1
               Text            =   " "
               Top             =   2055
               Visible         =   0   'False
               Width           =   1554
            End
            Begin VB.ComboBox cboLocationTypeName 
               DataField       =   "locationTypeId"
               DataMember      =   "Location"
               Height          =   315
               ItemData        =   "frmOrganizationEdit.frx":0053
               Left            =   996
               List            =   "frmOrganizationEdit.frx":0055
               Style           =   2  'Dropdown List
               TabIndex        =   10
               Tag             =   "LocationEditLocationType"
               Top             =   315
               Width           =   3192
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "email"
               DataMember      =   "Location"
               Height          =   288
               Index           =   3
               Left            =   996
               TabIndex        =   20
               Top             =   2760
               Width           =   3192
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "state"
               DataMember      =   "Location"
               Height          =   315
               Index           =   2
               Left            =   996
               Style           =   2  'Dropdown List
               TabIndex        =   14
               TabStop         =   0   'False
               Top             =   1695
               Width           =   648
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "city"
               DataMember      =   "Location"
               Height          =   315
               Index           =   0
               Left            =   2124
               TabIndex        =   15
               Text            =   "cboDatafield"
               Top             =   1695
               Width           =   1752
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmOrganizationEdit.frx":0057
               DataField       =   "fax"
               DataMember      =   "Location"
               Height          =   285
               Index           =   2
               Left            =   3900
               TabIndex        =   19
               Top             =   2415
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskDataField 
               Bindings        =   "frmOrganizationEdit.frx":006A
               DataField       =   "telephone"
               DataMember      =   "Location"
               Height          =   285
               Index           =   1
               Left            =   1425
               TabIndex        =   18
               Top             =   2415
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   12
               Format          =   "###.###.####"
               Mask            =   "###.###.####"
               PromptChar      =   "_"
            End
            Begin MSComctlLib.ListView lvReference 
               DragMode        =   1  'Automatic
               Height          =   2520
               Left            =   5970
               TabIndex        =   49
               TabStop         =   0   'False
               Top             =   570
               Width           =   4230
               _ExtentX        =   7461
               _ExtentY        =   4445
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   0
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "(street address)"
               Height          =   195
               Left            =   4320
               TabIndex        =   58
               Top             =   720
               Width           =   1080
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "(Attn, C/O, Suite #)"
               Height          =   195
               Left            =   4320
               TabIndex        =   57
               Top             =   1065
               Width           =   1365
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               Caption         =   "(P.O. Box)"
               Height          =   195
               Left            =   4320
               TabIndex        =   56
               Top             =   1425
               Width           =   720
            End
            Begin VB.Label Label28 
               AutoSize        =   -1  'True
               Caption         =   "This ia a list of items that use this location."
               Height          =   195
               Left            =   6000
               TabIndex        =   50
               Top             =   345
               Width           =   2955
            End
            Begin VB.Label Label27 
               AutoSize        =   -1  'True
               Caption         =   "Country:"
               DataField       =   "County"
               Height          =   195
               Left            =   2745
               TabIndex        =   48
               Top             =   2100
               Visible         =   0   'False
               Width           =   570
            End
            Begin VB.Label Label26 
               AutoSize        =   -1  'True
               Caption         =   "City:"
               Height          =   195
               Left            =   1785
               TabIndex        =   47
               Top             =   1740
               Width           =   300
            End
            Begin VB.Label Label25 
               AutoSize        =   -1  'True
               Caption         =   "Type:"
               Height          =   195
               Left            =   90
               TabIndex        =   46
               Top             =   345
               Width           =   420
            End
            Begin VB.Label Label24 
               AutoSize        =   -1  'True
               Caption         =   "Address:"
               Height          =   195
               Left            =   90
               TabIndex        =   45
               Top             =   705
               Width           =   645
            End
            Begin VB.Label Label23 
               AutoSize        =   -1  'True
               Caption         =   "State:"
               Height          =   195
               Left            =   90
               TabIndex        =   44
               Top             =   1740
               Width           =   405
            End
            Begin VB.Label Label22 
               AutoSize        =   -1  'True
               Caption         =   "County:"
               DataField       =   "County"
               Height          =   195
               Left            =   90
               TabIndex        =   43
               Top             =   2100
               Width           =   525
            End
            Begin VB.Label Label21 
               AutoSize        =   -1  'True
               Caption         =   "Zip:"
               Height          =   195
               Left            =   3990
               TabIndex        =   42
               Top             =   1725
               Width           =   270
            End
            Begin VB.Label Label20 
               AutoSize        =   -1  'True
               Caption         =   "Main Telephone:"
               Height          =   195
               Left            =   90
               TabIndex        =   41
               Top             =   2430
               Width           =   1215
            End
            Begin VB.Label Label19 
               AutoSize        =   -1  'True
               Caption         =   "Main Fax:"
               Height          =   195
               Left            =   3090
               TabIndex        =   40
               Top             =   2460
               Width           =   690
            End
            Begin VB.Label Label18 
               AutoSize        =   -1  'True
               Caption         =   "Email:"
               Height          =   195
               Left            =   90
               TabIndex        =   39
               Top             =   2805
               Width           =   450
            End
         End
         Begin VB.CommandButton cmdClearNotepad 
            Caption         =   "&Clear"
            Height          =   288
            Left            =   -74892
            TabIndex        =   25
            Top             =   5172
            Width           =   1028
         End
         Begin VB.TextBox txtNote 
            Height          =   4425
            Left            =   -74904
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   24
            Top             =   384
            Width           =   7035
         End
         Begin VB.Frame fraOrg 
            Caption         =   "Organization"
            Height          =   1785
            Left            =   105
            TabIndex        =   34
            Top             =   390
            Width           =   10335
            Begin VB.TextBox txtDatafield 
               DataField       =   "entityURL"
               DataMember      =   "Entity"
               Height          =   288
               Index           =   0
               Left            =   780
               TabIndex        =   7
               Top             =   1365
               Width           =   4740
            End
            Begin VB.CheckBox chkIsVendor 
               Caption         =   "Make Vendor"
               Height          =   315
               Left            =   3555
               TabIndex        =   2
               Top             =   293
               Width           =   1320
            End
            Begin VB.ComboBox cboOrganizationType 
               Height          =   315
               ItemData        =   "frmOrganizationEdit.frx":007D
               Left            =   780
               List            =   "frmOrganizationEdit.frx":0090
               Style           =   2  'Dropdown List
               TabIndex        =   0
               Top             =   293
               Width           =   2610
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "entityLocator"
               DataMember      =   "Entity"
               Height          =   288
               Index           =   1
               Left            =   780
               TabIndex        =   3
               Top             =   675
               Width           =   960
            End
            Begin VB.ComboBox cboDatafield 
               DataField       =   "organizationLegalForm"
               DataMember      =   "Organization"
               Height          =   315
               Index           =   1
               ItemData        =   "frmOrganizationEdit.frx":00BD
               Left            =   2820
               List            =   "frmOrganizationEdit.frx":00BF
               Style           =   2  'Dropdown List
               TabIndex        =   4
               Top             =   662
               Width           =   816
            End
            Begin VB.TextBox txtDatafield 
               DataField       =   "entityName"
               DataMember      =   "Entity"
               Height          =   288
               Index           =   5
               Left            =   780
               TabIndex        =   6
               Top             =   1020
               Width           =   4740
            End
            Begin MSMask.MaskEdBox mskEstablishYear 
               Height          =   285
               Left            =   4965
               TabIndex        =   5
               Top             =   675
               Width           =   570
               _ExtentX        =   1005
               _ExtentY        =   503
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   4
               Mask            =   "####"
               PromptChar      =   "_"
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Website:"
               Height          =   195
               Left            =   90
               TabIndex        =   59
               Top             =   1410
               Width           =   630
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Type:"
               Height          =   210
               Left            =   90
               TabIndex        =   51
               Top             =   345
               Width           =   420
            End
            Begin VB.Label Label13 
               Caption         =   "Establish Year:"
               Height          =   195
               Left            =   3840
               TabIndex        =   38
               Top             =   720
               Width           =   1110
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "Legal Form:"
               Height          =   195
               Left            =   1920
               TabIndex        =   37
               Top             =   720
               Width           =   825
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               Height          =   195
               Left            =   90
               TabIndex        =   36
               Top             =   1067
               Width           =   480
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Locator:"
               Height          =   195
               Left            =   90
               TabIndex        =   35
               Top             =   722
               Width           =   570
            End
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   2010
         Top             =   6510
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   2565
         Top             =   6510
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "cmdEnter"
      Height          =   336
      Left            =   3816
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   684
      Width           =   1308
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   10740
      _ExtentX        =   18944
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   10740
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   10620
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   30
         Width           =   10620
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   72
            TabIndex        =   33
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Label Label14 
      Caption         =   "Label14"
      Height          =   12
      Left            =   3252
      TabIndex        =   29
      Top             =   3096
      Width           =   972
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   28
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Firm"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuseperator9879 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewLocation 
            Caption         =   "L&ocation (Primary)"
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   0
            Shortcut        =   ^O
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Diary"
            Index           =   2
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "L&ocation (Primary)"
            Index           =   3
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditMove 
         Caption         =   "&Move to Folder..."
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSeparator28162 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Contact (Firm)"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "L&ocation (Primary)"
         Index           =   1
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator693 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmOrganizationEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.20.0611 (jjm)
'
' frmUnderwritingContact
'
' This form provides the user interface to the ContactFirmDataset class.
'

'
' Future Development
'
' -Options Form that has Options for a Save and Add command button.

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mOrganizationHandle As String
Private mFirmHandle As String
Private mLocationHandle As String
Private mLocationTypeHandle As String
Private mEventHandle As String
Private mPersonHandle As String

Private mNew As Boolean
Private mNewType As String                  'stores an optional organization type for when the form is loaded for a new entry

Private mDatafields As Collection

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private SaveToPath As String
Private ApplicationFolder As String

' Form Manager
Private FormMgr As New FormManager

' Signal that a refresh is being called.
Private mRefresh As Boolean


'ver 2.71.00 (jjm) 2005 01 31 - for city dropdown
'  http://www.freevbcode.com/ShowCode.Asp?ID=2266
Private miSelStart As Integer


'Constants
Private Const MSK_TELEPHONE_INDEX = 1
Private Const MSK_FAX_INDEX = 2

Private Const TXT_COUNTY_INDEX = 13
Private Const TXT_ENTITYLOCATOR_INDEX = 1
Private Const TXT_ZIP_INDEX = 19

Private Const CBO_CITY_INDEX = 0
Private Const CBO_STATE_INDEX = 2

Private Const TELEPHONE_MASK = "___.___.____"

'To be centralized
Private mLocationType As Collection


Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Public Property Get organizationHandle() As String
  organizationHandle = mOrganizationHandle
End Property

'
' FirmHandle needed when linking/loading a new firm.  Used in
' application edit when selecting a new firm for the application.
'
'Public Property Get FirmHandle() As String
'  FirmHandle = mOrganizationHandle
'End Property

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Dim Handle As String
  
  Select Case fInterface.Message
    Case fimNone
      Show
    Case fimNew
      If fInterface.Key = DatabaseName.LOCATION_TABLE Then
        mnuFileNewLocation_Click
      End If
    Case fimSelect
      If fInterface.Key = DatabaseName.LOCATION_TABLE Then
        Handle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
        If mLocationHandle <> Handle Then
          mLocationHandle = Handle
          Dirty = True
        End If
        LocationRefresh
      End If
    Case fimSave
      If fInterface.Key = DatabaseName.LOCATION_TABLE & "Edit" Then
        Handle = fInterface.Child.LocationHandle
        If mLocationHandle <> Handle Then
          mLocationHandle = Handle
          Dirty = True
        End If
        LocationRefresh
      End If
  End Select
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
End Sub

Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  MaskEditClear mskEstablishYear
  
  ClearDataField txtDatafield
  ClearDataField mskDataField
  ClearDataField dtpDataField
  'ClearDataField lblDataField
  
  mFirmHandle = vbNullString
  
  If (mNew) Then
    Me.Caption = "New Organization"                                  'Set the window caption
    'Blank the handles
    mOrganizationHandle = vbNullString
    mPersonHandle = vbNullString
    mLocationHandle = vbNullString
    dtpDataField(0).Value = Today                                    'Set default for established year
  
    Util.SetComboValue cboOrganizationType, IIf(mNewType = vbNullString, DatabaseName.FIRM_TABLE, mNewType)  'Set default Organization type to Firm
  Else
    Dim OrgData As Collection
    Set OrgData = Factory.OrganizationService.Find(mOrganizationHandle)
    
    If OrgData Is Nothing Then
      MsgBox "organizationId: " & mOrganizationHandle & " no longer exists."
      mNew = True
      FormRefresh
      Exit Sub
    End If
    
    'Get Entity data
    Dim EntityData As Collection
    Set EntityData = Factory.EntityService.Find(mOrganizationHandle)
    
        
    'display organization data
    If Not GetDataFromCollection(OrgData, DatabaseName.ESTABLISHDATE_FIELD) = vbNullString Then
      mskEstablishYear = Year(GetDataFromCollection(OrgData, DatabaseName.ESTABLISHDATE_FIELD))
    End If
    SetFields txtDatafield, OrgData, "Organization"
    SetFields cboDatafield, OrgData, "Organization"
    SetFields txtDatafield, EntityData, "Entity"
    
    Caption = "Edit Organization - " & GetDataFromCollection(OrgData, DatabaseName.ENTITYLOCATOR_FIELD) & " (" & mOrganizationHandle & ")"
    


    mLocationHandle = GetDataFromCollection(EntityData, DatabaseName.MAINLOCATIONID_FIELD)
    mPersonHandle = GetDataFromCollection(OrgData, DatabaseName.MAINPERSONID_FIELD)
    
    'Handle Sub Types (Firm, Insurer, Vendor, etc...)
    Dim subType As String          'Variable to populate the cboOrganizationType combo box
    Dim mSubCol As Collection      'Collection to be used for temp information storage
  
    'Handle Firm
    Set mSubCol = Factory.FirmService.Find(mOrganizationHandle)
    Dim isFirm As Boolean
    isFirm = (Not mSubCol Is Nothing)
    'Handle the firm display block
    fraFirm.Visible = isFirm
    fraFirm.Enabled = isFirm
    'Handle firm data
    If isFirm Then
      mFirmHandle = GetDataFromCollection(mSubCol, DatabaseName.FIRM_KEY)
      SetFields dtpDataField, mSubCol, DatabaseName.FIRM_TABLE
      SetFields txtDatafield, mSubCol, DatabaseName.FIRM_TABLE
      subType = DatabaseName.FIRM_TABLE
    End If
    
    'Handle Vendor
    Set mSubCol = Factory.VendorService.Find(mOrganizationHandle)
    If (mSubCol Is Nothing) Then
      chkIsVendor.Value = vbUnchecked
    Else
      chkIsVendor.Value = vbChecked
      chkIsVendor.Enabled = False
    End If
    
    'Set subtype to other if no other subtype is found.
    If subType = vbNullString Then subType = "Other"
    
    Util.SetComboValue cboOrganizationType, subType      'Set the organization type combo
    cboOrganizationType.Enabled = False                  'disable the organization type combo


    'EVENT INFO
    mEventHandle = GetDataFromCollection(EntityData, DatabaseName.EVENT_KEY)
    If (mEventHandle <> vbNullString) Then
      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
    End If
  
  End If
  
  LocationRefresh
  
  mnuFileOpen(0).Enabled = Not mNew
  mnuFileOpen(2).Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  
  ' Refresh navigation controls
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' Set change status to false
  Dirty = False
End Sub
    
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub
    
Public Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mOrganizationHandle = ListItemText(mListView.SelectedItem, DatabaseName.ORGANIZATION_KEY)
End Property

Public Sub LocationRefresh()
  If mLocationHandle = vbNullString Then
    'set default location type as primary (Firm)
    cboLocationTypeName.ListIndex = CollectionFind(mLocationType, DatabaseName.LOCATIONTYPE_KEY, DatabaseName.PRIMARY_LOCATIONTYPE) - 1
    Exit Sub
  End If
  
  'LOCATION INFO
  Dim LocationData As Collection
  Set LocationData = Factory.LocationService.Find(mLocationHandle)
  SetFields cboDatafield, LocationData, DatabaseName.LOCATION_TABLE
  SetFields txtDatafield, LocationData, DatabaseName.LOCATION_TABLE
  SetFields mskDataField, LocationData, DatabaseName.LOCATION_TABLE
  'handle location type
  mLocationTypeHandle = GetDataFromCollection(LocationData, DatabaseName.LOCATIONTYPE_KEY)
  cboLocationTypeName.ListIndex = CollectionFind(mLocationType, DatabaseName.LOCATIONTYPE_KEY, mLocationTypeHandle) - 1
  'Refresh reference list
  LocationReferenceUtil.ListRefresh lvReference, mLocationHandle
  
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
'  ' See if we really should be adding a new location.
'  If mNew And mLocationHandle = vbNullString Then
'    Select Case MsgBox("You can not save a new firm without creating a location.  Would you like to create a location now?", vbYesNo)
'      Case vbYes
'        mnuFileNewLocation_Click
'        Exit Function
'      Case Else
'        Exit Function
'    End Select
'  End If
  
  'Mantis issue 2481 - duplicate location edit validation
  If txtDatafield(TXT_COUNTY_INDEX).Text = vbNullString Or _
     cboDatafield(CBO_STATE_INDEX).Text = vbNullString Or _
     txtDatafield(TXT_ZIP_INDEX).Text = vbNullString Or _
     cboDatafield(CBO_CITY_INDEX).Text = vbNullString Then
    MsgBox "State, County, City, State and Zip fields are required location data"
    sstDetail.Tab = 0
    txtDatafield(TXT_ZIP_INDEX).SetFocus
    Exit Function
  End If
  
  
  
  
  'Check for safe locator
  'ver 2.75.00 (jjm)
  'Make sure that the locator will not be the same as an exisiting firmId
  If Not Factory.EntityService.verifyEntityLocator(txtDatafield(TXT_ENTITYLOCATOR_INDEX).Text, mOrganizationHandle) Then
    Exit Function
  End If
  
  ' Custom Code
  Dim mDataControls As New Collection
  AddTextControl txtDatafield, mDataControls
  AddTextControl cboDatafield, mDataControls
  AddTextControl mskDataField, mDataControls
  AddValueControl dtpDataField, mDataControls

  Dim mEstablishDate As Date
  If mskEstablishYear.ClipText <> vbNullString Then
    mEstablishDate = CDate("1/1/" & mskEstablishYear)
  End If
  
  'Handle Organization Type


  If (mNew) Then
    mOrganizationHandle = Factory.OrganizationService.CreateOrganization( _
        IIf(cboOrganizationType.Text = "Other", vbNullString, cboOrganizationType.Text), _
        (chkIsVendor.Value = vbChecked), _
        GetDataFromCollection(mDataControls, DatabaseName.ENTITYLOCATOR_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ENTITYNAME_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ENTITYURL_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONLEGALFORM_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.STATEBARNUMBER_FIELD), _
        mEstablishDate, _
        GetDate(GetDataFromCollection(mDataControls, NEXTEFFECTIVEDATE_FIELD)), _
        mLocationHandle, mLocationTypeHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE1_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE2_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE3_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.STATE_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.CITY_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ZIP_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.COUNTY_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.COUNTRY_FIELD), _
        GetMask(GetDataFromCollection(mDataControls, DatabaseName.TELEPHONE_FIELD), TELEPHONE_MASK), _
        GetMask(GetDataFromCollection(mDataControls, DatabaseName.FAX_FIELD), TELEPHONE_MASK), _
        GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
        mPersonHandle, txtNote.Text)
    Save = mOrganizationHandle <> vbNullString
  Else
    Save = Factory.OrganizationService.WriteOrganization( _
         mOrganizationHandle, _
        (chkIsVendor.Value = vbChecked And chkIsVendor.Enabled), _
        GetDataFromCollection(mDataControls, DatabaseName.ENTITYLOCATOR_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ENTITYNAME_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ENTITYURL_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ORGANIZATIONLEGALFORM_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.STATEBARNUMBER_FIELD), _
        mEstablishDate, _
        GetDate(GetDataFromCollection(mDataControls, NEXTEFFECTIVEDATE_FIELD)), _
        mLocationHandle, mLocationTypeHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE1_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE2_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ADDRESSLINE3_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.STATE_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.CITY_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.ZIP_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.COUNTY_FIELD), _
        GetDataFromCollection(mDataControls, DatabaseName.COUNTRY_FIELD), _
        GetMask(GetDataFromCollection(mDataControls, DatabaseName.TELEPHONE_FIELD), TELEPHONE_MASK), _
        GetMask(GetDataFromCollection(mDataControls, DatabaseName.FAX_FIELD), TELEPHONE_MASK), _
        GetDataFromCollection(mDataControls, DatabaseName.EMAIL_FIELD), _
        mPersonHandle, txtNote.Text, mFirmHandle)

  End If
  
  Dirty = Not Save
End Function

Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no organizations to view or edit." ' To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(firmHandle As String)
  mOrganizationHandle = firmHandle
  Show
End Sub


Public Sub ShowNew(Optional defaultType As String)
  mNew = True
  mNewType = defaultType
  Show

End Sub
'
' End General Methods
'
'
'
'
' Begin Control Methods
'

Private Sub cboDataField_Click(Index As Integer)
  Select Case Index
    Case CBO_STATE_INDEX
      'Update City Selection
      Factory.LocationService.LoadCitiesByState cboDatafield(CBO_CITY_INDEX), cboDatafield(CBO_STATE_INDEX).Text
    Case CBO_CITY_INDEX
      'Update county
      txtDatafield(TXT_COUNTY_INDEX).Text = Factory.LocationService.GetCountyByStateCity(cboDatafield(CBO_STATE_INDEX).Text, cboDatafield(CBO_CITY_INDEX).Text)
  End Select
  
  Dirty = True
End Sub

Private Sub cboDatafield_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
  Select Case Index
    'ver 2.70.00 (jjm) 2005 01 31
    Case CBO_CITY_INDEX
      '<Delete>
      If KeyCode = 46 Then KeyCode = 0 'Disable the delete key
      
      'Change backspace to left arrow
      If KeyCode = vbKeyBack Then KeyCode = vbKeyLeft
      
      'ver 2.70.00 (jjm) 2005 02 09 - tweak
      'When the left key is pressed have the curser move left one
      'position from the selection start instead of the end of
      'the selection. This section will blank the keycode and
      'manually set the cursor position.
      If KeyCode = vbKeyLeft Then
        With cboDatafield(Index)
          miSelStart = IIf(.SelStart > 0, .SelStart - 1, 0)
          cboDatafield(Index).SelStart = miSelStart
          .SelLength = Len(.Text) - .SelStart
        End With
        
        KeyCode = 0
        Exit Sub
      End If
      
      miSelStart = cboDatafield(Index).SelStart
  End Select
End Sub

Private Sub cboDataField_KeyPress(Index As Integer, KeyAscii As Integer)
  Select Case Index
    'ver 2.70.00 (jjm) 2005 01 31
    Case CBO_CITY_INDEX
      Dim lCnt       As Long 'Generic long counter
      Dim lMax       As Long
      Dim sComboItem As String
      Dim sComboText As String 'Text currently in combobox
      Dim sText      As String 'Text after keypressed
  
      
      With cboDatafield(Index)
        lMax = .ListCount - 1
        sComboText = .Text
        sText = Left(sComboText, miSelStart) & Chr(KeyAscii)
        
        KeyAscii = 0 'Reset key pressed
        
        For lCnt = 0 To lMax
          sComboItem = .List(lCnt)
          
          If UCase(sText) = UCase(Left(sComboItem, _
                                     Len(sText))) Then
            .ListIndex = lCnt
            .Text = sComboItem
            .SelStart = Len(sText)
            .SelLength = Len(sComboItem) - (Len(sText))
            
            Exit For
          End If
        Next 'lCnt
      End With
  
  End Select
End Sub

' Handle Location Type combo
Private Sub cboLocationTypeName_Click()
  If cboLocationTypeName.ListIndex < 0 Then
    mLocationTypeHandle = vbNullString
  Else
    mLocationTypeHandle = mLocationType(cboLocationTypeName.ListIndex + 1)(DatabaseName.LOCATIONTYPE_KEY)
  End If
  Dirty = True
End Sub

Private Sub cboOrganizationType_Click()
   fraFirm.Visible = cboOrganizationType.Text = DatabaseName.FIRM_TABLE
End Sub

Private Sub chkIsVendor_Click()
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdClearNotepad_Click()
  txtNote.Text = vbNullString
  txtNote.SetFocus
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Load selection of location.
'
Private Sub cmdLocation_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr, True, , UrlName.LOCATIONSEARCH_ENTITYID_INDEX, mOrganizationHandle
End Sub

Private Sub cmdSave_Click()
  Dim WasNew As Boolean
  WasNew = mNew
  If Save Then
    FormMgr.Message = fimSave
    If TypeOf FormMgr.Parent Is frmFirmOpen Then
      Unload Me
      Exit Sub
    End If
    
    Unload Me
    
    If WasNew Then
      ' do not use form manager since we unloaded.
      ' Test for Firm
      Dim firmHandle As String
      firmHandle = DataUtil.FindHandle(FIRM_TABLE, FIRM_KEY, mOrganizationHandle, FIRM_KEY)
      If firmHandle = vbNullString Then
        FormUtil.ShowByPrimaryKey New frmOrganizationOpen, mOrganizationHandle
      Else
        FormUtil.ShowByPrimaryKey New frmFirmOpen, firmHandle
      End If
    End If
  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0) Then
    If KeyCode = vbKeyTab Then
      sstDetail.SetFocus
    End If
  End If
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyO
        mnuFileNewLocation_Click
        KeyCode = 0
    End Select
  End If
End Sub

'
' Load Controls
'
Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  mnuFileNewLocation.Caption = mnuFileNewLocation.Caption & vbTab + "Ctrl+Shift+O"
  
  'Organization Legal Form dropdown
  FormUtil.ComboBoxRegistryLoad cboDatafield(1), "organizationType"
  
  'LOCATION SETUP
  Set mLocationType = Factory.LocationTypeService.NameCollection()
  FormUtil.ComboBoxLoad mLocationType, cboLocationTypeName, DatabaseName.LOCATIONTYPENAME_FIELD
  'ver 2.70.00 (jjm) 2004 09 23
  Factory.LocationService.LoadStates cboDatafield(CBO_STATE_INDEX), "WI"
  'setup reference view
  LocationReferenceUtil.ListInit lvReference
  
  FormResize
  TabControlEnable sstDetail
  FormRefresh
End Sub

'
' Terminate Used for memory audit
'
Private Sub Form_Terminate()
  'MsgBox "frmApplicationEdit.Terminate"
End Sub

'
' See if form can unload.
'
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = 1
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub lvReference_DblClick()
  LocationReferenceUtil.ListDblClick lvReference, FormMgr
End Sub

Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0
      Dim fContact As New frmContactEdit
      FormMgr.Add fContact
      fContact.ShowByFirm mOrganizationHandle, DatabaseName.FIRM_TABLE
    Case 1
      If mLocationHandle = vbNullString Then
        MsgBox "No default location has yet to be assigned to this firm."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmLocationEdit, mLocationHandle, FormMgr, , DatabaseName.LOCATION_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuEditMove_Click()
  Factory.EventService.ShowMove mEventHandle
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Save(True) Then
    mNew = True
    FormRefresh
  End If
End Sub

Private Sub mnuFileNewLocation_Click()
  Dim fForm As New frmLocationEdit
  FormMgr.Add fForm, DatabaseName.LOCATION_TABLE & "Edit"
  fForm.ShowNew mOrganizationHandle
End Sub


Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      FormUtil.ShowByPrimaryKey New frmFirmOpen, mOrganizationHandle, FormMgr
    'Case 1 ' menu separator
    Case 2
      FormUtil.ShowByPrimaryKey New frmFirmDiaryOpen, mOrganizationHandle, FormMgr
    Case 3
      If mLocationHandle = vbNullString Then
        MsgBox "No default location has yet to be assigned to this person."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmLocationOpen, mLocationHandle, FormMgr, , DatabaseName.LOCATION_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.FIRM_TABLE, mOrganizationHandle
  End If
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuFileSendToFolder_Click()
  FormUtil.SendToFolder DatabaseName.FIRM_REF, DatabaseName.FIRM_TABLE, mOrganizationHandle, txtDatafield(5)
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.APPLICATION_TABLE, FormMgr
End Sub

Private Sub mnuToolsNotepad_Click()
  FormUtil.ShowByPrimaryKey New frmNotepad, mEventHandle, FormMgr
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub

Private Sub mskDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub mskDataField_Validate(Index As Integer, Cancel As Boolean)
  Select Case Index
    Case 0
      If mNew Then
        If Not Factory.FirmService.Find(txtDatafield(1).Text, """firmId""") Is Nothing Then
          MsgBox "Firm locator already exists."
          Cancel = True
        End If
      End If
  End Select
End Sub

Private Sub mskEstablishYear_Change()
  Dirty = True
End Sub

Private Sub mskEstablishYear_GotFocus()
  FormUtil.EditTextSelect mskEstablishYear
End Sub

Private Sub mskEstablishYear_Validate(Cancel As Boolean)
  If mskEstablishYear = "____" Then Exit Sub
  Dim mYear As Integer
  mYear = Year(Now) + 1
  If mskEstablishYear.ClipText < 1700 Or mskEstablishYear.ClipText > (Year(Now) + 2) Then
    MsgBox "Establish Year must be between 1700 and " & (Year(Now) + 2)
    Cancel = True
  End If
End Sub


Private Sub sstDetail_Click(PreviousTab As Integer)
  TabControlEnable sstDetail
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDatafield(Index)
  If Not mNew Or txtDatafield(Index).Text = "" Then
    Exit Sub
  End If
  Select Case Index
    Case 15
      'txtDataField(15).Text = lblDataField(5).Caption
    Case 14
      'txtDataField(14).Text = lblDataField(6).Caption
  End Select
End Sub

Private Sub txtDataField_KeyPress(Index As Integer, KeyAscii As Integer)
  Select Case Index
    Case 1
      If Len(txtDatafield(Index).Text) >= 6 And KeyAscii <> 8 Then
        If txtDatafield(Index).SelLength = 0 Then
          KeyAscii = 0
          Exit Sub
        End If
      End If
            
      Select Case KeyAscii
        Case 32         'Converts space to '_'
          KeyAscii = 95
        Case 48 To 57   'Accept Digits
        Case 65 To 90   'Accept Upper Case letters
        Case 97 To 122  'Convert lower case to upper
          KeyAscii = KeyAscii - 32
        Case 95         'Accept the '_'
        Case 8          'Accept the backspace to delete
        Case Else       'Ignore all other key stokes
          KeyAscii = 0
      End Select
  End Select

End Sub

'Private Sub txtDataField_LostFocus(Index As Integer)
'  Select Case Index
'    Case 1 ' Middle Initial
'      txtDataField(1).Text = UCase(txtDataField(1).Text) + "."
'  End Select
'End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub




