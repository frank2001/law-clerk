VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PersonDataset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'ver 2.10.0421 (jjm)
'PersonDataset

Option Explicit
Implements Dataset

Private mvarSession As Session
Private mDataProvider As DataProvider
Private Words As String
Private ListIndex As Integer

Private Keys As Collection
Private KeyIndex As Integer


Private Sub Class_Initialize()
  ListIndex = UrlName.PERSONSEARCH_PERSONENTITYNAME
End Sub

Private Property Set Dataset_Session(ByVal vData As Session)
  Set mvarSession = vData
  Set mDataProvider = vData.NewDataProvider
  mDataProvider.Source = DatabaseName.PERSON_VIEW '"Person"
End Property


Private Property Get Dataset_Session() As Session
  Set Dataset_Session = mvarSession
End Property


Private Property Get Dataset_DataProvider() As DataProvider
  Set Dataset_DataProvider = mDataProvider
End Property



Private Sub Dataset_LoadSearch(frm As Form)
  frm.txtWords = Words
  frm.cboIn.Clear
  frm.cboIn.AddItem "in last name field only"
  frm.cboIn.AddItem "in lawyer entity locator field only"
  frm.cboIn.AddItem "in organization identifier field only"
  frm.cboIn.AddItem "in person entity name field only"
  frm.cboIn.AddItem "in person identifier field only"
  frm.cboIn.ListIndex = IIf(LocaleRememberLastSearch, ListIndex, UrlName.PERSONSEARCH_PERSONENTITYNAME)
End Sub

' Sets lvView.  Returns a collection of key fields.
Private Function Dataset_FindByForm(frm As Form) As Collection
  Words = frm.txtWords
  ListIndex = frm.cboIn.ListIndex
  Dim newFilter As New Filter
  
  Words = Replace(Words, "'", "''") 'Translate to SQL
  
  If Not Words = "" Then
    Select Case ListIndex
      Case UrlName.PERSONSEARCH_LAWYERLOCATOR_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.LAWYERENTITYLOCATOR_FIELD, Words)
      Case UrlName.PERSONSEARCH_PERSONENTITYNAME
        newFilter.Add LawDialect.ContainsAndArray(DatabaseName.PERSONENTITYNAME_FIELD, Words)
      Case UrlName.PERSONSEARCH_LASTNAME_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.LASTNAME_FIELD, Words)
      Case UrlName.PERSONSEARCH_ORGANIZATIONID_INDEX
        newFilter.Add LawDialect.StartsWithUpperCase(DatabaseName.ORGANIZATION_KEY, Words)
      Case UrlName.PERSONSEARCH_PERSONID_INDEX
        newFilter.Add LawDialect.ContainsUpperCase(DatabaseName.PERSON_KEY, Words)
    End Select
  End If

  mDataProvider.Filter = newFilter
  mDataProvider.OpenRecordset
  mDataProvider.SetListView frm.lvView
End Function
