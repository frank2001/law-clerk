Attribute VB_Name = "TimeUtil"
Option Explicit

Private Const CP_ACP = 0

Private Declare Function GetTimeZoneInformation Lib _
    "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Long
Private Declare Function lstrlenW Lib "kernel32" (lpString As Any) As Long
Private Declare Function WideCharToMultiByte Lib "kernel32" _
    (ByVal codepage As Long, ByVal dwFlags As Long, _
    lpWideCharStr As Any, ByVal cchWideChar As Long, _
    lpMultiByteStr As Any, ByVal cchMultiByte As Long, _
    ByVal lpDefaultChar As String, ByVal lpUsedDefaultChar As Long) As Long

Private Type SYSTEMTIME
        wYear As Integer
        wMonth As Integer
        wDayOfWeek As Integer
        wDay As Integer
        wHour As Integer
        wMinute As Integer
        wSecond As Integer
        wMilliseconds As Integer
End Type

Private Type TIME_ZONE_INFORMATION
        Bias As Long
        StandardName(32) As Integer
        StandardDate As SYSTEMTIME
        StandardBias As Long
        DaylightName(32) As Integer
        DaylightDate As SYSTEMTIME
        DaylightBias As Long
End Type


Public Function GetUTCTime() As Date
    Dim tz As TIME_ZONE_INFORMATION
    Dim lRV As Long
    Dim dRV As Date
    
    lRV = GetTimeZoneInformation(tz)
    
    dRV = DateAdd("n", CDbl(tz.Bias), Now)
    
    GetUTCTime = dRV
End Function

Public Function GetTimeZoneOffset() As Integer
    Dim tz As TIME_ZONE_INFORMATION
    Dim lRV As Long
    Dim iRV As Integer
    
    lRV = GetTimeZoneInformation(tz)
    
    ' offset is indicated in minutes
    iRV = tz.Bias
    
    GetTimeZoneOffset = iRV
End Function

Public Function GetTimeZoneName() As String
    Dim tz As TIME_ZONE_INFORMATION
    Dim lRV As Long
    Dim sRV As String
    Dim lCounter As Long
    
    Dim wchar(1) As Byte
    
    lRV = GetTimeZoneInformation(tz)
    
    ' the TZ Name is stored in Wide Chars, we will extract by just grabbing first byte of each digit and mapping
    ' to ASCII. Should be safe for most of the time.
    
    sRV = GetStrFromPtrW(VarPtr(tz.StandardName(0)))
    
    GetTimeZoneName = sRV
End Function

' Returns the string up to the first Null Char
Public Function GetStrFromANSIBuffer(sBuf As String) As String
    If InStr(sBuf, &H0) Then
        GetStrFromANSIBuffer = Left(sBuf, InStr(sBuf, &H0) - 1)
    Else
        GetStrFromANSIBuffer = sBuf ' no null, so don't worry about it
    End If
End Function

' Return ANSI string from a pointer to a Unicode string.
Public Function GetStrFromPtrW(lpszW As Long) As String
    Dim sRV As String
    
    sRV = String$(lstrlenW(ByVal lpszW) * 2, &H0) ' 2 bytes/char
    
    ' copy from Unicode string into new buffer
    WideCharToMultiByte CP_ACP, &H0, ByVal lpszW, -1, ByVal sRV, Len(sRV), &H0, &H0
    GetStrFromPtrW = GetStrFromANSIBuffer(sRV)
End Function

