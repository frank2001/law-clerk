VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QuoteService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "QuoteService"
Private mLawyerSet As Dataset
Private mDataSet As Dataset


'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.QUOTE_TABLE)
  Set mLawyerSet = NewGenericDataset(DatabaseName.QUOTELAWYER_VIEW)
  
  'Setup SOAP
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub


'
' Interface to server
'


' parameterOrder='ApplicationRatingId claimId eventTypeId reimburseDate ApplicationRating attributeNames attributeValues parameterNames parameterValues'>




'
' Write a Quote
'
Public Function WriteQuote( _
    QuoteHandle As String, _
    QuoteName As String, _
    eventNote As String) As Boolean
    
On Error GoTo Exception

  WriteQuote = Client.Write(QuoteHandle, _
                            QuoteName, _
                            eventNote)
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  WriteQuote = False
End Function


Public Function Find(QuoteHandle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.QUOTE_KEY, QuoteHandle, Column)
End Function

Public Function FindByMainQuoteBook(QuoteBookHandle As String, Optional Column As String) As Collection
  Set FindByMainQuoteBook = mDataSet.DataProvider.Find(DatabaseName.MAINQUOTEBOOKID_FIELD, QuoteBookHandle, Column)
End Function

Public Function Lawyers(QuoteHandle As String) As Collection
  mLawyerSet.DataProvider.Filter = LawDialect.EqualString(DatabaseName.QUOTE_KEY, QuoteHandle)
  mLawyerSet.DataProvider.Sort = DatabaseName.LAWYERNAME_FIELD
  mLawyerSet.DataProvider.OpenRecordset
  Set Lawyers = mLawyerSet.DataProvider.Collection
End Function
