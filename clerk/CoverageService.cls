VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CoverageService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const ServiceName = "CoverageService"

Private mDataSet As Dataset
Private mLawyerSet As Dataset
Private Client As SoapClient30


Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.COVERAGE_TABLE)
  Set mLawyerSet = NewGenericDataset(DatabaseName.COVERAGELAWYER_VIEW)
  
  
  'Setup SOAP
On Error GoTo Exception
  
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Sub


Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.COVERAGE_KEY, Handle, Column)
End Function

Public Function FindByEvent(EventHandle As String, Optional Column As String) As Collection
  Set FindByEvent = mDataSet.DataProvider.Find(DatabaseName.EVENT_KEY, EventHandle, Column)
End Function

Public Function Lawyers(Handle As String) As Collection
  mLawyerSet.DataProvider.filter = LawDialect.EqualString(DatabaseName.COVERAGE_KEY, Handle)
  mLawyerSet.DataProvider.Sort = DatabaseName.LAWYER_KEY
  mLawyerSet.DataProvider.OpenRecordset
  Set Lawyers = mLawyerSet.DataProvider.Collection
End Function



'
'
' Interface to server
'
'


'    parameterOrder =  coverageId issueDate firmId
'                      policyId applicationId applicationRatingId
'                      perClaimLimit aggregateLimit perClaimDeductible aggregateDeductible coverageNote
'                      applicationLimitId attributeNames attributeValues parameterNames parameterValues'>

'
' Write an application.
'
Public Function WriteCoverage( _
    coverageHandle As String, _
    eventTypeId As String, _
    IssueDate As Date, _
    QuoteCoverageId As String, _
    eventNote As String, _
    TermAttribute As Integer, _
    ExpirationDateAttribute As Date) As Boolean
    
On Error GoTo Exception:
  
  Call Client.Write( _
      coverageHandle, _
      eventTypeId, _
      IssueDate, _
      QuoteCoverageId, _
      eventNote, _
      TermAttribute, _
      ExpirationDateAttribute)
  WriteCoverage = True
  Exit Function
  
Exception:
  'MsgBox "Coverage Service Error: " & Err.Description, vbExclamation
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  WriteCoverage = False
End Function

'
' Delete an Coverage
'
Public Function DeleteCoverage( _
    coverageHandle As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo ErrorHandler
  Call Client.Delete(coverageHandle, ParameterNames, ParameterValues)
  DeleteCoverage = True
  Exit Function
  
ErrorHandler:
  MsgBox "Coverage Service Error: " & Err.Description, vbExclamation
  DeleteCoverage = False
End Function


Public Function SetNote(Handle As String, ModifyHandle As String, Note As String) As Boolean
  
On Error GoTo Exception
  
  Call Client.SetNote(Handle, ModifyHandle, Note)
  SetNote = True
  Exit Function

Exception:
  MsgBox "Coverage Set Note Service Error: " & Err.Description, vbExclamation
End Function


