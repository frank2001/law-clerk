Attribute VB_Name = "ClaimUtil"
Option Explicit

'ver 2.72.00 (jjm) 2005 04 21 - pulled from claim edit and added incident search
Public Function isDuplicateClaimantMSG(Handle As String, claimant As String) As Boolean
    Dim ClaimSearch As Dataset
    Dim IncidentSearch As Dataset

    Dim SearchString As String     'String to build the SQL section for the claimant value including wildcards
    
    'Build the search string
    SearchString = UCase(claimant)
    'ver 2.70.00 (jjm) 2004 11 22 - remove all funky characters
    Dim Invalid As String
    Dim n As Integer
    Invalid = ".,' &"""
    For n = 1 To Len(Invalid)
      SearchString = Replace(SearchString, Mid$(Invalid, n, 1), "%") 'Remove each invalid character
    Next n
    SearchString = "%" & UCase(SearchString) & "%" 'Set the string to all upper case
    SearchString = Replace(SearchString, "%%", "%") 'Change all double wild cards to single wild cards
    
    
    'Search the data
    'Claim search
    Set ClaimSearch = NewGenericDataset(DatabaseName.CLAIM_VIEW)
    ClaimSearch.DataProvider.Column = DatabaseName.CLAIM_KEY & "," & _
                                      DatabaseName.FIRMENTITYNAME_FIELD & "," & _
                                      DatabaseName.CLAIMANT_FIELD
    ClaimSearch.DataProvider.Filter = _
        "not(" & DatabaseName.CLAIM_KEY & " = '" & Handle & "') and " & _
        LawDialect.StartsWithUpperCase(DatabaseName.CLAIMANT_FIELD, SearchString)
    ClaimSearch.DataProvider.OpenRecordset
             

    
    'Incident Search
    Set IncidentSearch = NewGenericDataset(DatabaseName.INCIDENT_VIEW)
    IncidentSearch.DataProvider.Column = _
               DatabaseName.INCIDENT_KEY & "," & _
               DatabaseName.FIRMENTITYNAME_FIELD & "," & _
               DatabaseName.CLAIMANT_FIELD
    IncidentSearch.DataProvider.Filter = _
               "not(" & DatabaseName.INCIDENT_KEY & " = '" & Handle & "') and " & _
               LawDialect.StartsWithUpperCase(DatabaseName.CLAIMANT_FIELD, SearchString)
    IncidentSearch.DataProvider.OpenRecordset
    
    
    If IncidentSearch.DataProvider.IsEmpty Or ClaimSearch.DataProvider.IsEmpty Then
      isDuplicateClaimantMSG = False
    Else
      'show dialog
      Dim fDub As New frmDuplicateClaimantDialog
      
      fDub.setClaimData ClaimSearch.DataProvider
      fDub.setIncidentData IncidentSearch.DataProvider
            
      fDub.Show vbModal
      
      isDuplicateClaimantMSG = Not fDub.Continue
      Unload fDub
    End If
    

End Function

    'parse the claim results
    'If Not ClaimResults Is Nothing Then
      
      'Dim i As Integer
      'ResultString = "Claim:" & vbTab & vbTab & "Claimant:" & vbTab & vbTab & "Firm Name:" & vbCrLf
      
      
      'ver 2.72.00 (jjm) 2005 05 02
      'added firm name to dialog box
      
      'Due to vb restrictions, there is a need to create a dummy variable
      'Dim tmpFirmHandle As String
      'For i = 1 To ClaimResults.Count
      '  tmpFirmHandle = ClaimResults(i)(DatabaseName.FIRM_KEY)
      '  ResultString = ResultString & _
      '                 ClaimResults(i)(DatabaseName.CLAIM_KEY) & vbTab & vbTab & _
      '                 ClaimResults(i)(DatabaseName.CLAIMANT_FIELD) & vbTab & vbTab & _
      '                 Factory.FirmService.Find(tmpFirmHandle, """" & DatabaseName.FIRMENTITYNAME_FIELD & """")(DatabaseName.FIRMENTITYNAME_FIELD) & vbCrLf
      'Next i
    'End If

'    'parse the claim results
'    If Not IncidentResults Is Nothing Then
'
'      Dim j As Integer
'      ResultString = ResultString & vbCrLf & "Incident:" & vbTab & vbTab & "Claimant:" & vbTab & vbTab & "Firm Name:" & vbCrLf
'
'      Dim TailString As String
'      For j = 1 To IncidentResults.Count
'        tmpFirmHandle = IncidentResults(j)(DatabaseName.FIRM_KEY)
'        ResultString = ResultString & _
'                       IncidentResults(j)(DatabaseName.INCIDENT_KEY) & vbTab & vbTab & _
'                       IncidentResults(j)(DatabaseName.CLAIMANT_FIELD) & vbTab & vbTab
'        'Due to limitations of VB, the next statment must be written out as a full if then else statement
'        If tmpFirmHandle = vbNullString Then
'          ResultString = ResultString & vbCrLf
'        Else
'          ResultString = ResultString & Factory.FirmService.Find(tmpFirmHandle, """" & DatabaseName.FIRMENTITYNAME_FIELD & """")(DatabaseName.FIRMENTITYNAME_FIELD) & vbCrLf
'        End If
'      Next j
'    End If
    
'    'only display the message if an item has been found
'    If ResultString <> vbNullString Then
'      ResultString = "A similar claimant has been found in the following instances:" & vbCrLf & vbCrLf & _
'                     ResultString & vbCrLf & _
'                     "Would you like to continue with the save?"
'
'      isDuplicateClaimantMSG = IIf(MsgBox(ResultString, vbYesNo, "Continue with Save?") = vbYes, False, True)
'    Else
'      'Nothing has been found so it is ok to continue
'      isDuplicateClaimantMSG = False
'    End If


