VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClaimPaymentService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Client As SoapClient30
Private Const ServiceName = "ClaimPaymentService"
Private mDataSet As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = DataUtil.NewGenericDataset(DatabaseName.CLAIMPAYMENT_TABLE)
  
On Error GoTo Exception
  Set Client = New SoapClient30
  Client.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  Client.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
  
End Sub


Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.CLAIMPAYMENT_KEY, Handle, Column)
End Function


'
' Interface to server
'

'
' Create.
'
Public Function CreateClaimPayment( _
    ClaimPaymentId As String, _
    ClaimId As String, EventTypeId As String, _
    PaymentDate As Date, VendorId As String, _
    InvoiceId As String, CheckNum As String, PAYEE As String, _
    PaymentALAE As Double, PaymentIndemnity As Double, PaymentAmount As Double, _
    DeductibleALAE As Double, DeductibleIndemnity As Double, _
    DeductibleAdvance As Double, deductibleDirect As Double, _
    eventNote As String) As Variant
    
On Error GoTo Exception
  
  CreateClaimPayment = Client.Create( _
      ClaimPaymentId, _
      ClaimId, EventTypeId, _
      PaymentDate, VendorId, _
      InvoiceId, CheckNum, PAYEE, _
      PaymentALAE, PaymentIndemnity, PaymentAmount, _
      DeductibleALAE, DeductibleIndemnity, _
      DeductibleAdvance, deductibleDirect, _
      eventNote)
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Function


'
' Write an application.
'
Public Function WriteClaimPayment( _
    ClaimPaymentHandle As String, _
    ClaimId As String, EventTypeId As String, _
    PaymentDate As Date, VendorId As String, _
    InvoiceId As String, CheckNum As String, PAYEE As String, _
    PaymentALAE As Double, PaymentIndemnity As Double, PaymentAmount As Double, _
    DeductibleALAE As Double, DeductibleIndemnity As Double, _
    DeductibleAdvance As Double, deductibleDirect As Double, _
    eventNote As String) As Boolean
    
On Error GoTo Exception
  
  Call Client.Write( _
      ClaimPaymentHandle, _
      ClaimId, EventTypeId, _
      PaymentDate, VendorId, _
      InvoiceId, CheckNum, PAYEE, _
      PaymentALAE, PaymentIndemnity, PaymentAmount, _
      DeductibleALAE, DeductibleIndemnity, _
      DeductibleAdvance, deductibleDirect, _
      eventNote)
  WriteClaimPayment = True
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, Client, ServiceName
End Function
