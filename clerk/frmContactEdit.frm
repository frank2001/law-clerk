VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "ComCt332.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmContactEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Contact"
   ClientHeight    =   7275
   ClientLeft      =   2775
   ClientTop       =   2760
   ClientWidth     =   7470
   Icon            =   "frmContactEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7275
   ScaleWidth      =   7470
   Begin VB.TextBox txtDataField 
      DataField       =   "contactTypeId"
      BeginProperty DataFormat 
         Type            =   0
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   0
      EndProperty
      DataMember      =   "Contact"
      Enabled         =   0   'False
      Height          =   288
      Index           =   0
      Left            =   6420
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   396
      Visible         =   0   'False
      Width           =   936
   End
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   6840
      Left            =   0
      ScaleHeight     =   6840
      ScaleWidth      =   7470
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   390
      Width           =   7470
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   6132
         TabIndex        =   7
         Top             =   6300
         Width           =   1200
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   336
         Left            =   4836
         TabIndex        =   6
         Top             =   6300
         Width           =   1200
      End
      Begin TabDlg.SSTab sstDetail 
         Height          =   6075
         Left            =   60
         TabIndex        =   44
         Top             =   90
         Width           =   7290
         _ExtentX        =   12859
         _ExtentY        =   10716
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   5
         TabHeight       =   420
         TabCaption(0)   =   "&Contact"
         TabPicture(0)   =   "frmContactEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraEntity"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraLocation"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraContact"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "A&dvanced"
         TabPicture(1)   =   "frmContactEdit.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame1"
         Tab(1).Control(1)=   "Frame2"
         Tab(1).ControlCount=   2
         Begin VB.Frame Frame2 
            Caption         =   "Claim / Incident Reference"
            Height          =   1380
            Left            =   -74892
            TabIndex        =   54
            Top             =   1425
            Width           =   7032
            Begin VB.CommandButton cmdInstance 
               Caption         =   "Instance"
               Height          =   288
               Left            =   5676
               TabIndex        =   55
               Top             =   204
               Width           =   1028
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "firmName"
               DataField       =   "firmName"
               DataMember      =   "Instance"
               Height          =   195
               Index           =   4
               Left            =   930
               TabIndex        =   63
               Top             =   795
               UseMnemonic     =   0   'False
               Width           =   660
            End
            Begin VB.Label Label17 
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               Height          =   195
               Left            =   135
               TabIndex        =   62
               Top             =   795
               Width           =   465
            End
            Begin VB.Label Label18 
               AutoSize        =   -1  'True
               Caption         =   "Claimant:"
               Height          =   195
               Left            =   135
               TabIndex        =   61
               Top             =   1050
               Width           =   660
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "claimant"
               DataField       =   "claimant"
               DataMember      =   "Instance"
               Height          =   195
               Index           =   19
               Left            =   930
               TabIndex        =   60
               Top             =   1050
               UseMnemonic     =   0   'False
               Width           =   5805
               WordWrap        =   -1  'True
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "claimNumber"
               DataField       =   "claimNumber"
               DataMember      =   "Claim"
               Height          =   195
               Index           =   18
               Left            =   930
               TabIndex        =   59
               Top             =   285
               UseMnemonic     =   0   'False
               Width           =   915
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Incident:"
               Height          =   195
               Left            =   135
               TabIndex        =   58
               Top             =   540
               Width           =   615
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "Claim:"
               Height          =   195
               Left            =   135
               TabIndex        =   57
               Top             =   285
               Width           =   420
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "incidentNumber"
               DataField       =   "incidentNumber"
               DataMember      =   "Incident"
               Height          =   195
               Index           =   0
               Left            =   930
               TabIndex        =   56
               Top             =   540
               UseMnemonic     =   0   'False
               Width           =   1110
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "Organization Reference"
            Height          =   915
            Left            =   -74892
            TabIndex        =   49
            Top             =   420
            Width           =   7032
            Begin VB.CommandButton cmdOrganization 
               Caption         =   "Organization"
               Height          =   288
               Left            =   5676
               TabIndex        =   53
               Top             =   204
               Width           =   1028
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               Height          =   195
               Index           =   5
               Left            =   135
               TabIndex        =   52
               Top             =   585
               Width           =   480
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "entityName"
               DataField       =   "entityName"
               DataMember      =   "OrganizationEntity"
               Height          =   195
               Index           =   20
               Left            =   795
               TabIndex        =   51
               Top             =   585
               UseMnemonic     =   0   'False
               Width           =   795
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "entityLocator"
               DataField       =   "entityLocator"
               DataMember      =   "OrganizationEntity"
               Height          =   195
               Index           =   24
               Left            =   135
               TabIndex        =   50
               Top             =   285
               UseMnemonic     =   0   'False
               Width           =   915
            End
         End
         Begin VB.Frame fraContact 
            Caption         =   "Contact"
            Height          =   960
            Left            =   108
            TabIndex        =   26
            Top             =   468
            Width           =   7032
            Begin VB.ComboBox cboContactTypeId 
               Height          =   315
               ItemData        =   "frmContactEdit.frx":0044
               Left            =   900
               List            =   "frmContactEdit.frx":0046
               Style           =   2  'Dropdown List
               TabIndex        =   1
               Tag             =   "ContactEditContactType"
               Top             =   576
               Width           =   2340
            End
            Begin VB.TextBox txtDataField 
               DataField       =   "contactSalutation"
               DataMember      =   "Contact"
               Height          =   288
               Index           =   14
               Left            =   900
               TabIndex        =   0
               Top             =   192
               Width           =   2292
            End
            Begin VB.Label Label19 
               AutoSize        =   -1  'True
               Caption         =   "Type:"
               Height          =   192
               Left            =   84
               TabIndex        =   41
               Top             =   624
               Width           =   420
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Salutation:"
               Height          =   192
               Index           =   0
               Left            =   60
               TabIndex        =   27
               Top             =   264
               Width           =   744
            End
         End
         Begin VB.Frame fraLocation 
            Caption         =   "Location"
            Height          =   2556
            Left            =   108
            TabIndex        =   13
            Top             =   3405
            Width           =   7032
            Begin VB.CommandButton cmdLocationOrganization 
               Caption         =   "O&rganization"
               Height          =   288
               Left            =   5676
               TabIndex        =   5
               TabStop         =   0   'False
               Top             =   1260
               Width           =   1028
            End
            Begin VB.CommandButton cmdLocationPerson 
               Caption         =   "Pe&rson"
               Height          =   288
               Left            =   5676
               TabIndex        =   4
               TabStop         =   0   'False
               Top             =   912
               Width           =   1028
            End
            Begin VB.CommandButton cmdLocation 
               Caption         =   "&All"
               Height          =   288
               Left            =   5676
               TabIndex        =   3
               TabStop         =   0   'False
               Top             =   552
               Width           =   1028
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Entity Name:"
               Height          =   195
               Left            =   150
               TabIndex        =   48
               Top             =   375
               Width           =   900
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "entityName"
               DataField       =   "entityName"
               DataMember      =   "LocationEntity"
               Height          =   195
               Index           =   1
               Left            =   1095
               TabIndex        =   47
               Top             =   375
               UseMnemonic     =   0   'False
               Width           =   795
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "addressLine1"
               DataField       =   "addressLine1"
               DataMember      =   "Location"
               Height          =   195
               Index           =   8
               Left            =   1095
               TabIndex        =   46
               Top             =   630
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Address:"
               Height          =   192
               Index           =   0
               Left            =   144
               TabIndex        =   45
               Top             =   624
               Width           =   648
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Select location by:"
               Height          =   192
               Left            =   5496
               TabIndex        =   43
               Top             =   240
               Width           =   1308
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "email"
               DataField       =   "email"
               DataMember      =   "Location"
               Height          =   195
               Index           =   13
               Left            =   1095
               TabIndex        =   39
               Top             =   2235
               UseMnemonic     =   0   'False
               Width           =   390
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "county"
               DataField       =   "county"
               DataMember      =   "Location"
               Height          =   195
               Index           =   12
               Left            =   1095
               TabIndex        =   38
               Top             =   1965
               UseMnemonic     =   0   'False
               Width           =   465
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "telephone"
               DataField       =   "telephone"
               DataMember      =   "Location"
               Height          =   195
               Index           =   16
               Left            =   1095
               TabIndex        =   37
               Top             =   1695
               UseMnemonic     =   0   'False
               Width           =   720
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "fax"
               DataField       =   "fax"
               DataMember      =   "Location"
               Height          =   192
               Index           =   17
               Left            =   2844
               TabIndex        =   36
               Top             =   1692
               UseMnemonic     =   0   'False
               Width           =   204
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "zip"
               DataField       =   "zip"
               DataMember      =   "Location"
               Height          =   192
               Index           =   15
               Left            =   3900
               TabIndex        =   35
               Top             =   1404
               UseMnemonic     =   0   'False
               Width           =   204
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "state"
               DataField       =   "state"
               DataMember      =   "Location"
               Height          =   192
               Index           =   14
               Left            =   2916
               TabIndex        =   34
               Top             =   1404
               UseMnemonic     =   0   'False
               Width           =   348
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "city"
               DataField       =   "city"
               DataMember      =   "Location"
               Height          =   195
               Index           =   11
               Left            =   1095
               TabIndex        =   33
               Top             =   1410
               UseMnemonic     =   0   'False
               Width           =   240
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "addressLine3"
               DataField       =   "addressLine3"
               DataMember      =   "Location"
               Height          =   195
               Index           =   10
               Left            =   1095
               TabIndex        =   32
               Top             =   1110
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "addressLine2"
               DataField       =   "addressLine2"
               DataMember      =   "Location"
               Height          =   195
               Index           =   9
               Left            =   1095
               TabIndex        =   31
               Top             =   870
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label lblCounty 
               Caption         =   "County:"
               Height          =   192
               Left            =   144
               TabIndex        =   21
               Top             =   1968
               Width           =   492
            End
            Begin VB.Label Label10 
               AutoSize        =   -1  'True
               Caption         =   "Telephone:"
               Height          =   192
               Left            =   132
               TabIndex        =   20
               Top             =   1692
               Width           =   828
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               Caption         =   "E-Mail:"
               Height          =   192
               Left            =   156
               TabIndex        =   18
               Top             =   2232
               Width           =   492
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "FAX:"
               Height          =   192
               Left            =   2424
               TabIndex        =   17
               Top             =   1692
               Width           =   336
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "Zip:"
               Height          =   192
               Left            =   3552
               TabIndex        =   15
               Top             =   1404
               Width           =   264
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               Caption         =   "State:"
               Height          =   192
               Index           =   0
               Left            =   2412
               TabIndex        =   14
               Top             =   1404
               Width           =   408
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               Caption         =   "City:"
               Height          =   192
               Index           =   0
               Left            =   144
               TabIndex        =   10
               Top             =   1404
               Width           =   300
            End
         End
         Begin VB.Frame fraEntity 
            Caption         =   "Person / Organization"
            Height          =   1875
            Left            =   108
            TabIndex        =   12
            Top             =   1470
            Width           =   7032
            Begin VB.CommandButton cmdContactOrganization 
               Caption         =   "Or&ganization"
               Height          =   288
               Left            =   5670
               TabIndex        =   64
               TabStop         =   0   'False
               Top             =   555
               Width           =   1028
            End
            Begin VB.CommandButton cmdPerson 
               Caption         =   "&Person"
               Height          =   288
               Left            =   5676
               TabIndex        =   2
               TabStop         =   0   'False
               Top             =   204
               Width           =   1028
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Home Email:"
               Height          =   195
               Index           =   9
               Left            =   90
               TabIndex        =   74
               Top             =   1290
               Width           =   885
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "homeEmail"
               DataField       =   "homeEmail"
               DataMember      =   "Person"
               Height          =   195
               Index           =   22
               Left            =   1185
               TabIndex        =   73
               Top             =   1290
               UseMnemonic     =   0   'False
               Width           =   765
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Work Email:"
               Height          =   195
               Index           =   8
               Left            =   90
               TabIndex        =   72
               Top             =   1035
               Width           =   855
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "email"
               DataField       =   "email"
               DataMember      =   "Person"
               Height          =   195
               Index           =   21
               Left            =   1185
               TabIndex        =   71
               Top             =   1035
               UseMnemonic     =   0   'False
               Width           =   360
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Home:"
               Height          =   195
               Index           =   7
               Left            =   4275
               TabIndex        =   70
               Top             =   1545
               Width           =   465
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "999.888.7777"
               DataField       =   "homeTelephone"
               DataMember      =   "Person"
               Height          =   195
               Index           =   7
               Left            =   4800
               TabIndex        =   69
               Top             =   1545
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Mobile:"
               Height          =   195
               Index           =   6
               Left            =   2412
               TabIndex        =   68
               Top             =   1545
               Width           =   510
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "999.888.7777"
               DataField       =   "mobileTelephone"
               DataMember      =   "Person"
               Height          =   195
               Index           =   3
               Left            =   2985
               TabIndex        =   67
               Top             =   1545
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Work Phone:"
               Height          =   195
               Index           =   3
               Left            =   90
               TabIndex        =   66
               Top             =   1545
               Width           =   945
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "999.888.7777"
               DataField       =   "workTelephone"
               DataMember      =   "Person"
               Height          =   195
               Index           =   2
               Left            =   1185
               TabIndex        =   65
               Top             =   1545
               UseMnemonic     =   0   'False
               Width           =   990
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Full Name:"
               Height          =   195
               Index           =   4
               Left            =   90
               TabIndex        =   40
               Top             =   525
               Width           =   750
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "entityName"
               DataField       =   "entityName"
               DataMember      =   "Entity"
               Height          =   195
               Index           =   5
               Left            =   1185
               TabIndex        =   30
               Top             =   270
               UseMnemonic     =   0   'False
               Width           =   795
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "salutation"
               DataField       =   "salutation"
               DataMember      =   "Person"
               Height          =   195
               Index           =   6
               Left            =   1185
               TabIndex        =   29
               Top             =   780
               UseMnemonic     =   0   'False
               Width           =   690
            End
            Begin VB.Label lblFullNameDisplay 
               AutoSize        =   -1  'True
               Caption         =   "title firstName M lastName suffix"
               DataField       =   "title"
               DataMember      =   "Person"
               Height          =   195
               Left            =   1185
               TabIndex        =   28
               Top             =   525
               UseMnemonic     =   0   'False
               Width           =   2235
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Salutation:"
               Height          =   195
               Index           =   2
               Left            =   90
               TabIndex        =   9
               Top             =   780
               Width           =   750
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Display Name:"
               Height          =   192
               Index           =   1
               Left            =   96
               TabIndex        =   8
               Top             =   264
               Width           =   1068
            End
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   390
         Top             =   6270
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   945
         Top             =   6180
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "cmdEnter"
      Height          =   336
      Left            =   3816
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   684
      Width           =   1308
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   7470
      _ExtentX        =   13176
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   7470
      _CBHeight       =   390
      _Version        =   "6.7.9816"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   7350
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   30
         Width           =   7350
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   25
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Label Label14 
      Caption         =   "Label14"
      Height          =   12
      Left            =   3252
      TabIndex        =   19
      Top             =   3096
      Width           =   972
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Height          =   192
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   300
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Contact"
            Index           =   0
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Organization"
            Index           =   2
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Location"
            Index           =   3
         End
         Begin VB.Menu mnuFileNew 
            Caption         =   "&Person"
            Index           =   4
         End
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Claim"
            Index           =   0
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Firm"
            Index           =   1
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Location"
            Index           =   2
         End
         Begin VB.Menu mnuFileOpen 
            Caption         =   "&Person"
            Index           =   3
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEditMenu 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEdit 
         Caption         =   "&Organization"
         Index           =   0
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Location"
         Index           =   1
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Person"
         Index           =   2
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSeperator654 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator693 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsEnvelope 
         Caption         =   "&Envelope..."
      End
      Begin VB.Menu mnuSeparator3812 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator444 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmContactEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ver 2.70.00 (jjm) 2004 10 19 - fixed bug when selecting a person for a contact
' ver 2.70.00 (jjm) 2004 09 29
' ver 2.70.00 (jjm) 2004 09 24
' ver 2.00.0317 core
'
' frmUnderwritingContact
'
' This form provides the user interface to the ContactFirmDataset class.
'

'
' Future Development
'
' -Options Form that has Options for a Save and Add command button.

Option Explicit

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private mOrganizationHandle As String
Private mEntityHandle As String
Private mLocationHandle As String
Private mLawyerHandle As String
Private mContactHandle As String
Private mEventHandle As String
Private mInstanceHandle As String

Private mNew As Boolean
Private mDatafields As Collection
Private mContactType As Collection
Private mIsOrganization As Boolean


'Private mFirmNew As Boolean


Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private SaveToPath As String
Private ApplicationFolder As String

' Form Manager
Private FormMgr As New FormManager

' Signal that a refresh is being called.
Private mRefresh As Boolean


Private Const CONTACTORG_SEARCH_FORMKEY = "ContactOrganizationSearch"
Private Const CONTACTORG_EDIT_FORMKEY = "ContactOrganizationEdit"

Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Public Property Get ContactHandle()
  ContactHandle = mContactHandle
End Property


'
' FirmHandle needed when linking/loading a new firm.  Used in
' application edit when selecting a new firm for the application.
'
'Public Property Get firmHandle()
'  firmHandle = mFirmHandle
'End Property


'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  Select Case fInterface.Message
    Case fimNone
      Show
    Case fimNew
      Select Case fInterface.Key
        Case CONTACTORG_SEARCH_FORMKEY
          mnuFileNew_Click 2
        Case DatabaseName.LOCATION_TABLE
          mnuFileNew_Click 3
        Case DatabaseName.PERSON_TABLE
          mnuFileNew_Click 4
        'New organization reference
        Case DatabaseName.ORGANIZATION_TABLE
          Dim fFirm As New frmOrganizationEdit
          FormMgr.Add fFirm, DatabaseName.ORGANIZATION_TABLE & "Edit"
          fFirm.ShowNew
        Case Else
          MsgBox "Insert not supported for '" & fInterface.Key & "' records."
      End Select
    Case fimSelect
      Dirty = True
      Select Case fInterface.Key
        Case DatabaseName.INSTANCE_TABLE
          mInstanceHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INSTANCE_KEY)
          InstanceRefresh
        Case DatabaseName.ORGANIZATION_TABLE
          mOrganizationHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.ORGANIZATION_KEY)
          OrganizationRefresh
        Case DatabaseName.LOCATION_TABLE
          mLocationHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.LOCATION_KEY)
          LocationRefresh
        Case CONTACTORG_SEARCH_FORMKEY
          mEntityHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.ORGANIZATION_KEY)
          EntityRefresh True
        Case DatabaseName.PERSON_TABLE
          mEntityHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.PERSON_KEY)
          EntityRefresh True
'        Case DatabaseName.INCIDENT_TABLE
'          mIncidentHandle = ListItemText(fInterface.Child.lvView.SelectedItem, DatabaseName.INCIDENT_KEY)
'          Dim Col As New Collection
'          Set Col = Factory.IncidentService.Find(mIncidentHandle, """" & DatabaseName.EVENT_KEY & """")
'          If Not Col Is Nothing Then
'            mEventHandle = Col(DatabaseName.EVENT_KEY)
'          End If
'          IncidentRefresh
      End Select
    Case fimSave
      Dirty = True
      Select Case fInterface.Key
        Case DatabaseName.PERSON_TABLE & "Edit"
          mEntityHandle = fInterface.Child.PersonHandle
          EntityRefresh True
        Case DatabaseName.LOCATION_TABLE & "Edit"
          mLocationHandle = fInterface.Child.LocationHandle
          LocationRefresh
        'Organization Contact
        Case CONTACTORG_EDIT_FORMKEY
          mEntityHandle = fInterface.Child.organizationHandle
          EntityRefresh True
        'Reference Organization
        Case DatabaseName.ORGANIZATION_TABLE & "Edit"
          mOrganizationHandle = fInterface.Child.organizationHandle
          OrganizationRefresh
        Case DatabaseName.INSTANCE_TABLE & "Edit"
          mInstanceHandle = fInterface.Child.InstanceHandle
          InstanceRefresh
      End Select
      
  End Select
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
End Sub


Public Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  ' Load all data fields with nothing
  ClearDataField txtDataField
  ClearDataField lblDataField
  
  If (mNew) Then
    Set mDatafields = New Collection
    Me.Caption = "New Contact"
    mContactHandle = vbNullString
    
    'ver 2.00.0106 (jjm)
    If mIsOrganization Then
      txtDataField(0).Text = DatabaseName.FIRMPOLICYCONTACT_TYPE
    Else
      'Default entry based on last contact type
      txtDataField(0).Text = IniMain.ReadFile(UrlName.MISC_INI, "lastContactType")
    End If

  Else
    Set mDatafields = Factory.ContactService.Find(mContactHandle)
    Me.Caption = "Edit Contact (" & mContactHandle & ")"
    'Get Handles
    mOrganizationHandle = GetDataFromCollection(mDatafields, DatabaseName.ORGANIZATION_KEY)
    mLocationHandle = GetDataFromCollection(mDatafields, DatabaseName.LOCATION_KEY)
    mEntityHandle = GetDataFromCollection(mDatafields, DatabaseName.CONTACTENTITY_KEY)
    mInstanceHandle = GetDataFromCollection(mDatafields, DatabaseName.INSTANCE_KEY)
    mEventHandle = GetDataFromCollection(mDatafields, DatabaseName.EVENT_KEY)
    
    'set contact fields
    SetFields txtDataField, mDatafields, "Contact"
    

  
'    If (mEventHandle <> vbNullString) Then
'      txtNote.Text = Factory.EventService.GetNote(mEventHandle)
'    End If
  End If
  
  
  ' Get Detail Info
  OrganizationRefresh
  LocationRefresh
  EntityRefresh
  InstanceRefresh
  
  ' Refresh navigation controls
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  mnuFileProperties.Enabled = Not mNew
  
  ' Set change status to false
  Dirty = False
End Sub
    
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub
    
Private Sub InstanceRefresh()
  SetFields lblDataField, Factory.InstanceService.Find(mInstanceHandle), DatabaseName.INSTANCE_TABLE
  SetFields lblDataField, Factory.ClaimService.Find(mInstanceHandle), DatabaseName.CLAIM_TABLE
  SetFields lblDataField, Factory.IncidentService.Find(mInstanceHandle), DatabaseName.INCIDENT_TABLE
  
End Sub


Public Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mContactHandle = ListItemText(mListView.SelectedItem, DatabaseName.CONTACT_KEY)
End Property

'
' Refresh displayName and salutation from the Location table.
'
Private Sub LocationRefresh()
  ClearDataField lblDataField, DatabaseName.LOCATION_TABLE
  ClearDataField lblDataField, "LocationEntity"

  Dim LocationData As Collection
  Set LocationData = Factory.LocationService.Find(mLocationHandle)
  SetFields lblDataField, LocationData, DatabaseName.LOCATION_TABLE
  
  Dim entityId As String
  entityId = GetDataFromCollection(LocationData, DatabaseName.MAINENTITYID_FIELD)
  If entityId <> vbNullString Then
    SetFields lblDataField, _
              Factory.EntityService.Find(entityId, DatabaseName.ENTITYNAME_FIELD), _
              "LocationEntity"
  End If
            
End Sub

'
' Refresh Reference Organization information.
'
Private Sub OrganizationRefresh()
  Dim EntCol As Collection
  Set EntCol = Factory.EntityService.Find(mOrganizationHandle)
  cmdLocationOrganization.Enabled = Not EntCol Is Nothing
  If EntCol Is Nothing Then
    ClearDataField lblDataField, "OrganizationEntity"
    Exit Sub
  End If
  SetFields lblDataField, EntCol, "OrganizationEntity"
  'If location information isn't provided by the person, get the location
  'information from the organization instead.
  If mLocationHandle = vbNullString And mNew Then
    mLocationHandle = GetDataFromCollection(EntCol, DatabaseName.MAINLOCATIONID_FIELD)
    LocationRefresh
  End If
End Sub


'
' Refresh contact entity information
'
Private Sub EntityRefresh(Optional Update As Boolean)

  Dim Row As Collection
  Set Row = Factory.PersonService.Find(mEntityHandle)
  SetFields lblDataField, Row, DatabaseName.PERSON_TABLE
  
  Dim isPerson As Boolean
  isPerson = Not Row Is Nothing
  
  'custom display name - ver 3.00.00 Beta 3.2
  lblFullNameDisplay.Caption = vbNullString
  If isPerson Then
    lblFullNameDisplay.Caption = _
        IIf(Contains(Row, DatabaseName.TITLE_FIELD), GetDataFromCollection(Row, DatabaseName.TITLE_FIELD) & " ", vbNullString) & _
        IIf(Contains(Row, DatabaseName.FIRSTNAME_FIELD), GetDataFromCollection(Row, DatabaseName.FIRSTNAME_FIELD) & " ", vbNullString) & _
        IIf(Contains(Row, DatabaseName.MIDDLENAME_FIELD), GetDataFromCollection(Row, DatabaseName.MIDDLENAME_FIELD) & " ", vbNullString) & _
        IIf(Contains(Row, DatabaseName.LASTNAME_FIELD), GetDataFromCollection(Row, DatabaseName.LASTNAME_FIELD) & " ", vbNullString) & _
        IIf(Contains(Row, DatabaseName.SUFFIX_FIELD), GetDataFromCollection(Row, DatabaseName.SUFFIX_FIELD) & " ", vbNullString)
  End If
  
  'get Entity data (mainly entityName for person)
  Dim entData As New Collection
  Set entData = Factory.EntityService.Find(mEntityHandle, DatabaseName.ENTITYNAME_FIELD & "," & DatabaseName.MAINLOCATIONID_FIELD)
  
  SetFields lblDataField, entData, DatabaseName.ENTITY_TABLE
  
  
  ' If Update = true then location data need to be refreshed based on person
  If Update Then
    Dim LocationHandle As String
    LocationHandle = GetDataFromCollection(entData, DatabaseName.MAINLOCATIONID_FIELD)
    If mLocationHandle <> LocationHandle Then
      If mLocationHandle <> vbNullString And isPerson Then
        If MsgBox("Should the location for this Contact be the same as the Firm's primary location?", vbYesNo) = vbYes Then
          Update = False
        End If
      End If
      'if an organization was selected, auto update the location
      If Update Or Not (isPerson) Then
        mLocationHandle = LocationHandle
        LocationRefresh
      End If
    End If
    
    
    txtDataField(14) = IIf(isPerson, GetDataFromCollection(Row, DatabaseName.SALUTATION_FIELD), GetDataFromCollection(entData, DatabaseName.ENTITYNAME_FIELD))
    
  End If
End Sub



Private Function Save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  ' End Standard Code
  
  
  ' Custom Code
  
  Dim mDataControls As New Collection
  AddTextControl txtDataField, mDataControls

  Dim ParameterNames() As String
  Dim ParameterValues() As String
   
  ' Save new contact at end in case we have a new location, etc.
  'ver 2.70.00 (jjm) 2004 09 24 - Removed contactName
  If mNew Then
    ' Only on new do we create a new contact record.
    mContactHandle = Factory.ContactService.CreateContact( _
        GetDataFromCollection(mDataControls, DatabaseName.CONTACTTYPE_KEY), _
        mOrganizationHandle, mEntityHandle, mLocationHandle, mInstanceHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.CONTACTSALUTATION_FIELD))
    Save = mContactHandle <> vbNullString
  Else
    Save = Factory.ContactService.WriteContact(mContactHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.CONTACTTYPE_KEY), _
        mOrganizationHandle, mEntityHandle, mLocationHandle, mInstanceHandle, _
        GetDataFromCollection(mDataControls, DatabaseName.CONTACTSALUTATION_FIELD))
  End If
  
  'Save last contact type
  IniMain.WriteFile UrlName.MISC_INI, "lastContactType", txtDataField(0)
  
  If Save Then
    mNew = False
    FormMgr.Message = fimSave
  End If
  
  Dirty = Not Save
End Function

Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no contacts to view or edit." ' To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByFirm(firmHandle As String, ContactTypeHandle As String, Optional LocationHandle As String)

  Dim Contacts As Collection
  Set Contacts = Factory.ContactService.FindByOrganization(firmHandle, ContactTypeHandle)
  If Contacts Is Nothing Then
    If MsgBox("No 'Firm' contact record was found for this firm.  Would you like to insert a new 'Firm' contact?", vbYesNo) = vbYes Then
      ShowNewByOrganization firmHandle, LocationHandle
    End If
  Else
    ShowByPrimaryKey CStr(Contacts(1)(DatabaseName.CONTACT_KEY))
  End If
End Sub

Public Sub ShowByPrimaryKey(ContactHandle As String)
  mContactHandle = ContactHandle
  Show
End Sub


Public Sub ShowNew(Optional contactTypeId As String)
  mNew = True
  Show
  
  If Not contactTypeId = vbNullString And txtDataField(0) = vbNullString Then
    txtDataField(0) = contactTypeId
  End If
  
  If mEntityHandle = vbNullString Then
    cmdPerson_Click
  End If
End Sub


Public Sub ShowNewByOrganization(organizationHandle As String, Optional LocationHandle As String)
  mOrganizationHandle = organizationHandle
  mLocationHandle = LocationHandle
  mIsOrganization = True
  ShowNew
End Sub

Public Sub ShowNewByClaim(ClaimHandle As String)
  'Optional FirmHandle as String
  mInstanceHandle = ClaimHandle
  'mOrganizationHandle = GetDataFromCollection(Factory.FirmService.Find(firmHandle, DatabaseName.ORGANIZATION_KEY), DatabaseName.ORGANIZATION_KEY)

  'mFirmHandle = firmHandle
  'mClaimHandle = ClaimHandle
  ShowNew
End Sub

Public Sub ShowNewByIncident(IncidentHandle As String)
  'EventHandle As String, Optional firmHandle As String
  mInstanceHandle = IncidentHandle
  'mEventHandle = EventHandle
  'mFirmHandle = firmHandle
  ShowNew
End Sub


Public Sub ShowNewByLocation(LocationHandle As String, Optional firmHandle As String)
  mLocationHandle = LocationHandle
  If firmHandle <> vbNullString Then
    mOrganizationHandle = GetDataFromCollection(Factory.FirmService.Find(firmHandle, DatabaseName.ORGANIZATION_KEY), DatabaseName.ORGANIZATION_KEY)
  End If
  ShowNew
End Sub

Public Sub ShowNewByPerson(ContactEntityHandle As String, Optional LocationHandle As String)
  mEntityHandle = ContactEntityHandle
  mLocationHandle = LocationHandle
  ShowNew
End Sub

'
' End General Methods
'
'
'
'
' Begin Control Methods
'


Private Sub cboContactTypeId_Click()
  If cboContactTypeId.ListIndex < 0 Then
    txtDataField(0).Text = ""
  Else
    txtDataField(0).Text = mContactType(cboContactTypeId.ListIndex + 1)(DatabaseName.CONTACTTYPE_KEY)
  End If
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

'selecting an organization as a contact
Private Sub cmdContactOrganization_Click()
  Dispatcher.ShowSearch DatabaseName.ORGANIZATION_TABLE, FormMgr, True, , , , "Select an organization to be the contact.", , CONTACTORG_SEARCH_FORMKEY
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'Selecting the claim /incident a contact references
Private Sub cmdInstance_Click()
  Dispatcher.ShowSearch DatabaseName.INSTANCE_TABLE, FormMgr, True
End Sub

'
' Load selection of location.
'
Private Sub cmdLocation_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr, True
End Sub
'Search for location by organization
Private Sub cmdLocationOrganization_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr, True, , UrlName.LOCATIONSEARCH_ENTITYID_INDEX, mOrganizationHandle
End Sub
'Search for location by person
Private Sub cmdLocationPerson_Click()
  Dispatcher.ShowSearch DatabaseName.LOCATION_TABLE, FormMgr, True, , UrlName.LOCATIONSEARCH_ENTITYID_INDEX, mEntityHandle
End Sub

'Search for the organization a contact references. Either an organization / entity or instance must be referenced.
Private Sub cmdOrganization_Click()
  Dispatcher.ShowSearch DatabaseName.ORGANIZATION_TABLE, FormMgr, True
End Sub

'
' Load selection of person
'
Private Sub cmdPerson_Click()
  Dispatcher.ShowSearch DatabaseName.PERSON_TABLE, _
                        FormMgr, _
                        True, , _
                        UrlName.PERSONSEARCH_PERSONENTITYNAME, , _
                        "Select a person for a contact"
End Sub

Private Sub cmdSave_Click()
  If Save Then
    FormMgr.Message = fimSave
    FormUtil.SetComboDefault cboContactTypeId
    Unload Me
  End If
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) = 0) Then
    If KeyCode = vbKeyTab Then
      sstDetail.SetFocus
    End If
  End If
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyO
        mnuFileNew_Click 3
        KeyCode = 0
      Case vbKeyP
        mnuFileNew_Click 4
        KeyCode = 0
    End Select
  End If

End Sub


'
' Load Controls
'
Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  mnuFileNew(3).Caption = mnuFileNew(3).Caption & vbTab + "Ctrl+Shift+O"
  mnuFileNew(4).Caption = mnuFileNew(4).Caption & vbTab + "Ctrl+Shift+P"
  
  Set mContactType = Factory.ContactTypeService.NameCollection()
  FormUtil.ComboBoxLoad mContactType, cboContactTypeId, "contactTypeName"
  
  FormRefresh
  FormResize
  TabControlEnable sstDetail
End Sub

'
' Terminate Used for memory audit
'
Private Sub Form_Terminate()
  'MsgBox "frmApplicationEdit.Terminate"
End Sub

'
' See if form can unload.
'
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = 1
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not Save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub


Private Sub mnuEdit_Click(Index As Integer)
  Select Case Index
    Case 0
      If mOrganizationHandle = vbNullString Then
        MsgBox "No organization has yet to be associated with this contact."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmOrganizationEdit, mOrganizationHandle, FormMgr, , DatabaseName.ORGANIZATION_TABLE & "Edit"
    Case 1
      If mLocationHandle = vbNullString Then
        MsgBox "No location has yet to be associated with this contact."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmLocationEdit, mLocationHandle, FormMgr, , DatabaseName.LOCATION_TABLE & "Edit"
    Case 2
      If mEntityHandle = vbNullString Then
        MsgBox "No person has yet to be associated with this contact."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmPersonEdit, mEntityHandle, FormMgr, , DatabaseName.PERSON_TABLE & "Edit"
  End Select
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click(Index As Integer)
  Select Case Index
    Case 0
      If Save(True) Then
        mNew = True
        FormRefresh
      End If
      
    Case 2  ' Organization
      Dim fFirm As New frmOrganizationEdit
      FormMgr.Add fFirm, CONTACTORG_EDIT_FORMKEY
      fFirm.ShowNew
  
    Case 3  ' Location
      Dim fForm As New frmLocationEdit
      FormMgr.Add fForm, DatabaseName.LOCATION_TABLE & "Edit"
      fForm.ShowNew mOrganizationHandle
    
    Case 4  ' Person
      Dim fPerson As New frmPersonEdit
      FormMgr.Add fPerson, DatabaseName.PERSON_TABLE & "Edit"
      fPerson.ShowNew False, mOrganizationHandle
    
  End Select
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  Select Case Index
    Case 0
      If mInstanceHandle = vbNullString Then
        MsgBox "No instance has yet to be associated with this contact."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmClaimOpen, mInstanceHandle, FormMgr
    Case 1
      If mOrganizationHandle = vbNullString Then
        MsgBox "No organization has yet to be associated with this contact."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmOrganizationOpen, mOrganizationHandle, FormMgr
    Case 2
      If mLocationHandle = vbNullString Then
        MsgBox "No location has yet to be associated with this contact."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmLocationOpen, mLocationHandle, FormMgr
    Case 3
      If mEntityHandle = vbNullString Then
        MsgBox "No person has yet to be associated with this contact."
        Exit Sub
      End If
      FormUtil.ShowByPrimaryKey New frmPersonOpen, mEntityHandle, FormMgr
  End Select
End Sub

Private Sub mnuFileProperties_Click()
  If Not FormUtil.ShowEventProperties(mEventHandle) Then
    FormUtil.ShowProperties DatabaseName.CONTACT_TABLE, mContactHandle
  End If

End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsAdvancedFind_Click()
  Dispatcher.ShowSearch DatabaseName.APPLICATION_TABLE, FormMgr
End Sub

Private Sub mnuToolsEnvelope_Click()
  Dim addressText As String
  addressText = IIf(lblDataField(5).Caption = vbNullString, vbNullString, UCase(lblDataField(5)) & vbCrLf) & _
                IIf(lblDataField(1).Caption = lblDataField(5).Caption Or lblDataField(1).Caption = vbNullString, vbNullString, Replace(Replace(UCase(lblDataField(1).Caption), ",", vbNullString), ".", vbNullString) & vbCrLf) & _
                IIf(lblDataField(8).Caption = vbNullString, vbNullString, UCase(lblDataField(8)) & vbCrLf) & _
                IIf(lblDataField(9).Caption = vbNullString, vbNullString, UCase(lblDataField(9)) & vbCrLf) & _
                IIf(lblDataField(10).Caption = vbNullString, vbNullString, UCase(lblDataField(10)) & vbCrLf) & _
                UCase(lblDataField(11).Caption) & " " & UCase(lblDataField(14)) & "  " & lblDataField(15)
  FormUtil.PrintEnvelope addressText, ""
End Sub

Private Sub mnuViewNext_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  FormRefresh
End Sub

Private Sub mnuViewPrevious_Click()
  If Not Save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  FormRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub


Private Sub sstDetail_Click(PreviousTab As Integer)
  TabControlEnable sstDetail
End Sub


Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Select Case Index
    Case 0
      cboContactTypeId.ListIndex = CollectionFind(mContactType, DatabaseName.CONTACTTYPE_KEY, txtDataField(Index).Text) - 1
  End Select
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
  If Not mNew Or txtDataField(Index).Text <> "" Then
    Exit Sub
  End If
  Select Case Index
    Case 14
      txtDataField(14).Text = lblDataField(6).Caption
  End Select
  FormUtil.EditTextSelect txtDataField(Index)
End Sub

Private Sub txtDataField_LostFocus(Index As Integer)
  Select Case Index
    Case 1 ' Middle Initial
      txtDataField(1).Text = UCase(txtDataField(1).Text) + "."
  End Select
End Sub

Private Sub txtNote_Change()
  Dirty = True
End Sub

Private Sub txtNote_GotFocus()
  FormUtil.NoteGotFocus cmdSave, cmdEnter
End Sub

Private Sub txtNote_LostFocus()
  FormUtil.NoteLostFocus cmdSave, cmdEnter
End Sub
