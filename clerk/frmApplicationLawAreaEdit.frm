VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmApplicationLawAreaEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Title"
   ClientHeight    =   2340
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   5160
   Icon            =   "frmApplicationLawAreaEdit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   2340
   ScaleWidth      =   5160
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   1956
      Left            =   0
      ScaleHeight     =   1950
      ScaleWidth      =   5160
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   384
      Width           =   5160
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1428
         TabIndex        =   3
         Top             =   1536
         Width           =   1200
      End
      Begin VB.CommandButton cmdSaveNew 
         Caption         =   "Save && &New"
         Height          =   336
         Left            =   2688
         TabIndex        =   4
         Top             =   1548
         Width           =   1188
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   3924
         TabIndex        =   5
         Top             =   1548
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   1380
         Left            =   84
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   84
         Width           =   5040
         _ExtentX        =   8890
         _ExtentY        =   2434
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Law Area"
         TabPicture(0)   =   "frmApplicationLawAreaEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "mskDataField(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cboDescription"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "mskDataField(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         Begin MSMask.MaskEdBox mskDataField 
            DataField       =   "lawAreaPercent"
            Height          =   288
            Index           =   1
            Left            =   1032
            TabIndex        =   2
            Top             =   972
            Width           =   552
            _ExtentX        =   979
            _ExtentY        =   503
            _Version        =   393216
            ClipMode        =   1
            MaxLength       =   3
            Mask            =   "999"
            PromptChar      =   "_"
         End
         Begin VB.ComboBox cboDescription 
            DataField       =   "description"
            Height          =   288
            ItemData        =   "frmApplicationLawAreaEdit.frx":0028
            Left            =   972
            List            =   "frmApplicationLawAreaEdit.frx":002A
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Tag             =   "ApplicationLawAreaEditLawArea"
            Top             =   468
            Width           =   3840
         End
         Begin MSMask.MaskEdBox mskDataField 
            Bindings        =   "frmApplicationLawAreaEdit.frx":002C
            DataField       =   "lawAreaId"
            Height          =   288
            Index           =   0
            Left            =   108
            TabIndex        =   0
            Top             =   468
            Width           =   720
            _ExtentX        =   1270
            _ExtentY        =   503
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   2
            Mask            =   ">AA"
            PromptChar      =   "_"
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Percentage:"
            Height          =   192
            Index           =   2
            Left            =   96
            TabIndex        =   8
            Top             =   1020
            Width           =   876
         End
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   5160
      _ExtentX        =   9102
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   5160
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   5040
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   30
         Width           =   5040
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   228
            Left            =   24
            TabIndex        =   11
            Top             =   24
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   397
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   1572
      TabIndex        =   12
      Top             =   456
      Width           =   1056
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   16777215
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   624
      Top             =   96
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator998 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmApplicationLawAreaEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mApplicationHandle As String
Private mLawAreaHandle As String

Private mNew As Boolean
Private mDataSet As Dataset
Private mDatafields As Collection
Private mListView As ListView
Private mListIndex As Integer

' Get law areas for drop down.
Private mLawAreaDataset As Dataset
Private mLawAreaCollection As Collection

Private mDirty As Boolean

Public FormMgr As New FormManager

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

Private Function DescriptionRefresh() As Boolean
  If mskDataField(0).Text = "__" Then
    cboDescription.ListIndex = -1
    DescriptionRefresh = True
    Exit Function
  End If
 
  Dim I, ListIndex As Integer
  For I = 1 To mLawAreaCollection.Count
    If mLawAreaCollection(I)(DatabaseName.LAWAREA_KEY) = mskDataField(0).Text Then
      ListIndex = I
      Exit For
    End If
  Next I
  
  If ListIndex = 0 Then
    MsgBox "Invalid law area id"
    Exit Function
  End If
  
  cboDescription.ListIndex = ListIndex - 1
  DescriptionRefresh = True
  
End Function

'
' FormInterface Interface
'

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

Private Sub FormRefresh()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  If (mNew) Then
    ClearDataField mskDataField
    Caption = "New Application Law Area"
    FormUtil.GetComboDefault cboDescription
  Else
    Set mDatafields = Factory.ApplicationLawAreaService.Find(mApplicationHandle, mLawAreaHandle)
    Caption = "Edit Application Law Area"
    SetFields mskDataField, mDatafields
  End If
  
  mnuFileProperties.Enabled = Not mNew

  DescriptionRefresh
  
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  ' Record is no longer dirty.
  Dirty = False
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Public Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mApplicationHandle = ListItemText(mListView.SelectedItem, DatabaseName.APPLICATION_KEY)
  mLawAreaHandle = ListItemText(mListView.SelectedItem, DatabaseName.LAWAREA_KEY)
End Property

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  Dim mDataControls As New Collection
  AddMaskControl mskDataField, mDataControls
  
  If mNew Then
    Dim Handle As Variant
    Handle = Factory.ApplicationLawAreaService.CreateApplicationLawArea( _
                mApplicationHandle, _
                GetDataFromCollection(mDataControls, DatabaseName.LAWAREA_KEY), _
                GetDouble(GetDataFromCollection(mDataControls, DatabaseName.LAWAREAPERCENT_FIELD)))
    save = Not VarType(Handle) = vbEmpty
    If save Then
      mApplicationHandle = Handle(0)
      mLawAreaHandle = Handle(1)
      mNew = False
    End If
  Else
    save = Factory.ApplicationLawAreaService.WriteApplicationLawArea( _
                mApplicationHandle, _
                mLawAreaHandle, _
                GetDataFromCollection(mDataControls, DatabaseName.LAWAREA_KEY), _
                GetDouble(GetDataFromCollection(mDataControls, DatabaseName.LAWAREAPERCENT_FIELD)))
    If save Then
      mLawAreaHandle = mskDataField(0).Text
    End If
  End If
  
  If save Then
    FormRefresh
  Else
    Dirty = False
  End If
End Function
          
Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no law areas to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(ApplicationId As String, LawAreaId As String)
  mApplicationHandle = ApplicationId
  mLawAreaHandle = LawAreaId
  Show
End Sub


Public Sub ShowNew(ApplicationId As String)
  mApplicationHandle = ApplicationId
  mNew = True
  Show
End Sub

'Private Function Validate() As Boolean
'  If Not mDirty Then
'    Validate = True
'    Exit Function
'  End If
'  Dim Result As Integer
'  Select Case MsgBox("Save " + IIf(mNew, "new", "changes to the") + " application law area?", vbYesNoCancel)
'    Case vbYes
'      Validate = Save
'    Case vbNo
'      Validate = True
'    Case vbNo
'      Validate = False
'  End Select
'End Function


'
' End General
'


'
' Control Methods
'

Private Sub cboDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub cboDataField_Scroll(Index As Integer)
  Dirty = True
End Sub

Private Sub cboDescription_Click()
  If cboDescription.ListIndex < 0 Then
    mskDataField(0).Text = "__"
  Else
    mskDataField(0).Text = GetDataFromCollection(mLawAreaCollection(cboDescription.ListIndex + 1), DatabaseName.LAWAREA_KEY)
  End If
End Sub

'
' Standard Cancel Command Button.
'
Private Sub cmdCancel_Click()
  mDirty = False
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

Private Sub cmdSave_Click()
  If save Then
    FormUtil.SetComboDefault cboDescription
    Unload Me
  End If
End Sub

Private Sub cmdSaveNew_Click()
  If save Then mnuFileNew_Click
End Sub

Private Sub Form_Initialize()
   'Set the Datasets Here
   Set mDataSet = New GenericDataset
   Set mDataSet.Session = LawSession
   mDataSet.DataProvider.Source = DatabaseName.APPLICATIONLAWAREA_TABLE
   
   Set mLawAreaDataset = New LawAreaDataset
   Set mLawAreaDataset.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  ' Standard navigation
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        If Not mnuViewNext.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewNext_Click
      Case 188
        If Not mnuViewPrevious.Enabled Then
          Beep
          Exit Sub
        End If
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If

End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  
  ' Setup screen.
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
    
  ' Scale toolbars to button height.
  tbrUpDown.Height = tbrUpDown.ButtonHeight
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.Height + 48
  
  '
  ' Populate Description Dropdown
  '
  mLawAreaDataset.DataProvider.OpenRecordset
  Set mLawAreaCollection = mLawAreaDataset.DataProvider.Collection
  If Not mLawAreaCollection Is Nothing Then
    Dim I As Integer
    Dim item As ComboItem
    For I = 1 To mLawAreaCollection.Count
      cboDescription.AddItem mLawAreaCollection(I)("lawAreaName")
    Next I
  End If
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not save(True) Then Exit Sub
  mNew = True
  mLawAreaHandle = vbNullString
  FormRefresh
  mskDataField(0).SetFocus
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowPropertiesMulti DatabaseName.APPLICATIONLAWAREA_TABLE, _
      DatabaseName.APPLICATION_KEY, DatabaseName.LAWAREA_KEY, mApplicationHandle, mLawAreaHandle
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub


Private Sub mskDataField_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub mskDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect mskDataField(Index)
End Sub

Private Sub mskDataField_Validate(Index As Integer, Cancel As Boolean)
  If mskDataField(0) = "__" Then
    Exit Sub
  End If
  Cancel = Not DescriptionRefresh
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub


Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

