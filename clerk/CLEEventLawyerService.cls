VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLEEventLawyerService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const ServiceName = "CLEEventLawyerService"

Private mClient As SoapClient30
Private mDataSet As Dataset
Private mDataProvider As DataProvider
Private mHandleNames(1) As String

 'Initialize Web Service
Public Sub Init(old_WSDLFile As String, EndPointURL As String, Session As Session)
  Set mDataProvider = Session.NewDataProvider(DatabaseName.CLEEVENTLAWYER_TABLE)
  mHandleNames(0) = DatabaseName.CLEEVENT_KEY
  mHandleNames(1) = DatabaseName.LAWYER_KEY
  
'Setup SOAP
On Error GoTo Exception
  Set mClient = New SoapClient30
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Sub

'
' Data Access
'
Public Function Find(CLEEventHandle As String, LawyerHandle As String, Optional Column As String) As Collection
  Dim Handles(1) As Variant
  Handles(0) = CLEEventHandle
  Handles(1) = LawyerHandle
  Set Find = mDataProvider.FindMulti(mHandleNames, Handles)
End Function


'
' Interface to server
'

'
' Create a CLEEventLawyer
'
Public Function CreateCLEEventLawyer( _
    CLEEventId As String, LawyerId As String, _
    BeginDate As Date, EndDate As Date, MaxDate As Date, Used As Boolean, _
    AttributeNames() As String, AttributeValues() As String, _
    ParameterNames() As String, ParameterValues() As String) As Variant

On Error GoTo Exception

  CreateCLEEventLawyer = mClient.Create( _
      CLEEventId, LawyerId, BeginDate, EndDate, MaxDate, Used, _
      AttributeNames, AttributeValues, ParameterNames, ParameterValues)
  Exit Function

Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Function


'
' Write a CLEEventLawyer
'
Public Function WriteCLEEventLawyer( _
    CLEEventHandle As String, LawyerHandle As String, _
    CLEEventId As String, LawyerId As String, _
    BeginDate As Date, EndDate As Date, MaxDate As Date, Used As Boolean, _
    AttributeNames() As String, AttributeValues() As String, _
    ParameterNames() As String, ParameterValues() As String) As Variant
    
On Error GoTo Exception

  mClient.Write _
    CLEEventHandle, LawyerHandle, _
    CLEEventId, LawyerId, BeginDate, EndDate, MaxDate, Used, _
    AttributeNames, AttributeValues, ParameterNames, ParameterValues
  WriteCLEEventLawyer = True
  
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Function


'
' Delete an CLEEventLawyer
'
Public Function DeleteCLEEventLawyer(CLEEventId As String, LawyerId As String, _
    ByRef ParameterNames() As String, ByRef ParameterValues() As String) As Boolean

On Error GoTo Exception
  mClient.Delete CLEEventId, LawyerId, ParameterNames, ParameterValues
  DeleteCLEEventLawyer = True
  Exit Function
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Function

'Private Sub Class_Initialize()
'  Set mClient = New SoapClient30
'  Dim URL As String
'  URL = HttpServer &  "component/" & WSDLFile & ".wsdl"
'
'  Dim ServiceList(2) As String
'  ServiceList(0) = SERVICE11
'  ServiceList(1) = SERVICE
'
'  Dim I As Integer
'  For I = 0 To SafeUBound(ServiceList)
'    Set mClient = Factory.ServiceMSSoapInit(URL, ServiceList(I))
'    If Not mClient Is Nothing Then
'        mServiceName = ServiceList(I)
'      Exit For
'    End If
'  Next I
'
'  If mClient Is Nothing Then
'    MsgBox "CLEEventLawyerService Error:  Unable to connect to the server (" & HttpServer & ").  You will not be able to save changes.", vbCritical, "Error"
'    Exit Sub
'  End If
'
'  Set mDataSet = NewGenericDataset(DatabaseName.CLEEVENTLAWYER_TABLE)
'  mHandleNames(0) = DatabaseName.CLEEVENT_KEY
'  mHandleNames(1) = DatabaseName.LAWYER_KEY
'
'End Sub

