VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmListSelectDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Dialog"
   ClientHeight    =   4464
   ClientLeft      =   36
   ClientTop       =   276
   ClientWidth     =   4188
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4464
   ScaleWidth      =   4188
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   336
      Left            =   1500
      TabIndex        =   3
      Top             =   3984
      Width           =   1200
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   336
      Left            =   2820
      TabIndex        =   2
      Top             =   3984
      Width           =   1200
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3732
      Left            =   96
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   3948
      _ExtentX        =   6964
      _ExtentY        =   6583
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   420
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmListSelectDialog.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lvView"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin MSComctlLib.ListView lvView 
         Height          =   3264
         Left            =   72
         TabIndex        =   1
         Top             =   360
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   5757
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
End
Attribute VB_Name = "frmListSelectDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOk As Boolean

Public Property Get OK() As Boolean
  OK = mOk
End Property

Private Sub cmdCancel_Click()
  mOk = False
  Me.Hide
End Sub

Private Sub cmdOK_Click()
  mOk = True
  Me.Hide
End Sub

Private Sub lvview_DblClick()
  lvView.SelectedItem.Checked = Not lvView.SelectedItem.Checked
End Sub
