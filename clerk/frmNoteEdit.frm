VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmNotebook 
   Caption         =   "Edit Note"
   ClientHeight    =   4956
   ClientLeft      =   48
   ClientTop       =   276
   ClientWidth     =   6156
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4956
   ScaleWidth      =   6156
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picButtons 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   3564
      ScaleHeight     =   420
      ScaleWidth      =   2568
      TabIndex        =   1
      Top             =   4464
      Width           =   2568
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   1272
         TabIndex        =   3
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   12
         TabIndex        =   2
         Top             =   60
         Width           =   1200
      End
   End
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4488
      Left            =   0
      ScaleHeight     =   4488
      ScaleWidth      =   6156
      TabIndex        =   0
      Top             =   0
      Width           =   6156
      Begin VB.PictureBox Picture1 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   276
         Left            =   300
         ScaleHeight     =   276
         ScaleWidth      =   3876
         TabIndex        =   7
         Top             =   708
         Width           =   3876
         Begin VB.Label lblNote 
            AutoSize        =   -1  'True
            Caption         =   "lblNote"
            Height          =   192
            Left            =   0
            TabIndex        =   8
            Top             =   0
            Width           =   516
         End
      End
      Begin VB.CommandButton cmdEdit 
         Caption         =   "&Edit"
         Height          =   288
         Left            =   5148
         TabIndex        =   6
         Top             =   708
         Width           =   756
      End
      Begin VB.TextBox txtNote 
         Height          =   3048
         Left            =   276
         TabIndex        =   5
         Text            =   "Text1"
         Top             =   1152
         Width           =   5652
      End
      Begin MSComctlLib.TabStrip tabNote 
         Height          =   4164
         Left            =   144
         TabIndex        =   4
         Top             =   192
         Width           =   5916
         _ExtentX        =   10435
         _ExtentY        =   7345
         _Version        =   393216
         BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
            NumTabs         =   2
            BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Note1"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Note2"
               ImageVarType    =   2
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmNotebook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOk As Boolean

Private Sub cmdCancel_Click()
  mOk = False
  Hide
End Sub

Private Sub cmdOK_Click()
  mOk = True
  Hide
End Sub

Private Sub Form_Resize()
  picBody.Height = Me.ScaleHeight
  tabNote.Width = Me.ScaleWidth - 96
  tabNote.Height = Me.ScaleHeight - 496
  txtNote.Width = tabNote.Width - 196
  txtNote.Height = tabNote.Height - 432
  picButtons.Left = tabNote.Left + tabNote.Width - picButtons.Width + 96
  picButtons.Top = tabNote.Top + tabNote.Height
End Sub


Private Sub txtNote_DblClick()
  MsgBox cmdOK.Left
  MsgBox cmdOK.Top
End Sub

Private Sub txtNote_GotFocus()
  cmdOK.Default = False
End Sub

Public Function ShowNote(Note As String) As Boolean
  mOk = False
  txtNote.Text = Note
  Show vbModal
  ShowNote = mOk
End Function

Public Property Get Note() As String
  Note = txtNote.Text
End Property

