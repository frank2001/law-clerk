VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ServiceFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"ApplicationLawAreaService"
Attribute VB_Ext_KEY = "Member1" ,"ApplicationLawyerService"
Attribute VB_Ext_KEY = "Member2" ,"ApplicationLimitService"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member3" ,"ApplicationQuestionService"
Attribute VB_Ext_KEY = "Member4" ,"ApplicationRatingService"
Attribute VB_Ext_KEY = "Member5" ,"ApplicationService"
Attribute VB_Ext_KEY = "Member6" ,"AttributeService"
Attribute VB_Ext_KEY = "Member7" ,"AutoKeyService"
Attribute VB_Ext_KEY = "Member8" ,"ClaimPaymentService"
Attribute VB_Ext_KEY = "Member9" ,"ClaimService"
Attribute VB_Ext_KEY = "Member10" ,"ContactService"
Attribute VB_Ext_KEY = "Member11" ,"ContactTypeService"
Attribute VB_Ext_KEY = "Member12" ,"CoverageService"
Attribute VB_Ext_KEY = "Member13" ,"DeductibleReimburseService"
Attribute VB_Ext_KEY = "Member14" ,"EventRecipientService"
Attribute VB_Ext_KEY = "Member15" ,"EventService"
Attribute VB_Ext_KEY = "Member16" ,"EventTypeService"
Attribute VB_Ext_KEY = "Member17" ,"FirmInsurerService"
Attribute VB_Ext_KEY = "Member18" ,"FirmService"
Attribute VB_Ext_KEY = "Member19" ,"FolderParameterService"
Attribute VB_Ext_KEY = "Member20" ,"FolderService"
Attribute VB_Ext_KEY = "Member21" ,"GLAccountService"
Attribute VB_Ext_KEY = "Member22" ,"LawAreaService"
Attribute VB_Ext_KEY = "Member23" ,"LawErrorService"
Attribute VB_Ext_KEY = "Member24" ,"LawyerService"
Attribute VB_Ext_KEY = "Member25" ,"LocationService"
Attribute VB_Ext_KEY = "Member26" ,"LocationTypeService"
Attribute VB_Ext_KEY = "Member27" ,"PersonService"
Attribute VB_Ext_KEY = "Member28" ,"PolicyAccountingService"
Attribute VB_Ext_KEY = "Member29" ,"PolicyService"
Attribute VB_Ext_KEY = "Member30" ,"ReferenceService"
'ver 2.10.0417 (jjm)
Option Explicit

'Private Const ApplicationLawyerServiceWSDL = "component/ApplicationLawyerService.wsdl"
Private Const ApplicationLawAreaServiceWSDL = "component/ApplicationLawAreaService.wsdl"
Private Const ApplicationQuestionServiceWSDL = "component/ApplicationQuestionService.wsdl"
Private Const CLEEventServiceWSDL = "component/CLEEventService.wsdl"
Private Const CLEEventLawyerServiceWSDL = "component/CLEEventLawyerService.wsdl"
Private Const EndorsementServiceWSDL = "component/EndorsementService.wsdl"
Private Const FirmInsurerServiceWSDL = "component/FirmInsurerService.wsdl"
'Private Const FirmPolicyServiceWSDL = "component/FirmPolicyService.wsdl"
Private Const FolderServiceWSDL = "component/FolderService.wsdl"
'Private Const ServiceServiceWSDL = "component/ServiceService.wsdl"

Private mEventService As EventService
Private mApplicationService As ApplicationService
Private mApplicationLawAreaService As ApplicationLawAreaService
Private mApplicationLawyerService As ApplicationLawyerService
Private mApplicationLimitService As ApplicationLimitService
Private mApplicationQuestionService As ApplicationQuestionService
Private mAttributeService As AttributeService
Private mAutoKeyService As AutoKeyService
Private mClaimService As ClaimService
Private mClaimCoverageService As ClaimCoverageService
Private mClaimPaymentService As ClaimPaymentService
Private mCLEEventService As CLEEventService
Private mCLEEventLawyerService As CLEEventLawyerService
Private mContactService As ContactService
Private mContactTypeService As ContactTypeService
Private mCoverageService As CoverageService
Private mDeductibleReimburseService As DeductibleReimburseService
Private mEFTIndividualService As EFTIndividualService
Private mEndorsementService As EndorsementService
Private mEntityService As EntityService
Private mEventTypeService As EventTypeService
Private mEventRecipientService As EventRecipientService
Private mPersistenceManagerService As PersistenceManagerService
Private mFirmService As FirmService
Private mFirmInsurerService As FirmInsurerService
Private mFirmPolicyService As FirmPolicyService
'User preference service instead
'Private mFolderService As FolderService
Private mFolderService As PreferenceService
Private mFolderParameterService As FolderParameterService
Private mGLAccountService As GLAccountService
Private mIncidentService As IncidentService
Private mInstanceService As InstanceService
Private mInstancePaymentService As InstancePaymentService
Private mLawAreaService As LawAreaService
Private mLawErrorService As LawErrorService
Private mLawyerService As LawyerService
Private mLocationService As LocationService
Private mLocationTypeService As LocationTypeService
Private mOrganizationService As OrganizationService
Private mPersonService As PersonService
Private mPolicyService As PolicyService
Private mPolicyAccountingService As PolicyAccountingService
Private mPreferenceService As PreferenceService
Private mQuoteService As QuoteService
Private mReferenceService As ReferenceService
Private mServiceService As ServiceService
Private mTimeService As TimeService
Private mVendorService As VendorService

Public Function ServiceMSSoapInit(ConnectionPath As String, ServiceName As String) As SoapClient30
On Error GoTo Exception
  Dim SoapClient30 As New SoapClient30
  
  ' Attempt to initialize with a version
  SoapClient30.MSSoapInit ConnectionPath, ServiceName
  Set ServiceMSSoapInit = SoapClient30
  Exit Function
  
Exception:
  Set ServiceMSSoapInit = Nothing
  ErrorUtil.DisplaySoapError Err, SoapClient30
End Function
Public Property Get EventService() As EventService
  If mEventService Is Nothing Then
    Set mEventService = New EventService
  End If
  Set EventService = mEventService
End Property

Public Property Get ApplicationService() As ApplicationService
  If mApplicationService Is Nothing Then
    Set mApplicationService = New ApplicationService
  End If
  Set ApplicationService = mApplicationService
End Property


Public Property Get ApplicationLawAreaService() As ApplicationLawAreaService
  If mApplicationLawAreaService Is Nothing Then
    Set mApplicationLawAreaService = New ApplicationLawAreaService
    mApplicationLawAreaService.Init _
        HttpServer & ApplicationLawAreaServiceWSDL, _
        HttpServer & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set ApplicationLawAreaService = mApplicationLawAreaService
End Property


Public Property Get ApplicationLawyerService() As ApplicationLawyerService
  If mApplicationLawyerService Is Nothing Then
    Set mApplicationLawyerService = New ApplicationLawyerService
    'mApplicationLawyerService.Init _
    '    HttpServer &  ApplicationLawyerServiceWSDL, _
    '    HttpServer &  WebService & "rpcrouter" & LawSession.Session, _
    '    LawSession
  End If
  Set ApplicationLawyerService = mApplicationLawyerService
End Property

Public Property Get ApplicationLimitService() As ApplicationLimitService
  If mApplicationLimitService Is Nothing Then
    Set mApplicationLimitService = New ApplicationLimitService
  End If
  Set ApplicationLimitService = mApplicationLimitService
End Property

Public Property Get ApplicationQuestionService() As ApplicationQuestionService
  If mApplicationQuestionService Is Nothing Then
    Set mApplicationQuestionService = New ApplicationQuestionService
    mApplicationQuestionService.Init _
        HttpServer & ApplicationQuestionServiceWSDL, _
        HttpServer & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set ApplicationQuestionService = mApplicationQuestionService
End Property

Public Property Get QuoteService() As QuoteService
  If mQuoteService Is Nothing Then
    Set mQuoteService = New QuoteService
  End If
  Set QuoteService = mQuoteService
End Property

Public Property Get AttributeService() As AttributeService
  If mAttributeService Is Nothing Then
    Set mAttributeService = New AttributeService
  End If
  Set AttributeService = mAttributeService
End Property

Public Property Get AutoKeyService() As AutoKeyService
  If mAutoKeyService Is Nothing Then
    Set mAutoKeyService = New AutoKeyService
  End If
  Set AutoKeyService = mAutoKeyService
End Property

Public Property Get ClaimService() As ClaimService
  If mClaimService Is Nothing Then
    Set mClaimService = New ClaimService
  End If
  Set ClaimService = mClaimService
End Property

Public Property Get ClaimCoverageService() As ClaimCoverageService
  If mClaimCoverageService Is Nothing Then
    Set mClaimCoverageService = New ClaimCoverageService
  End If
  Set ClaimCoverageService = mClaimCoverageService
End Property

Public Property Get ClaimPaymentService() As ClaimPaymentService
  If mClaimPaymentService Is Nothing Then
    Set mClaimPaymentService = New ClaimPaymentService
  End If
  Set ClaimPaymentService = mClaimPaymentService
End Property

Public Property Get CLEEventService() As CLEEventService
  If mCLEEventService Is Nothing Then
    Set mCLEEventService = New CLEEventService
    mCLEEventService.Init _
        HttpServer & CLEEventServiceWSDL, _
        HttpServer & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set CLEEventService = mCLEEventService
End Property

Public Property Get CLEEventLawyerService() As CLEEventLawyerService
  If mCLEEventLawyerService Is Nothing Then
    Set mCLEEventLawyerService = New CLEEventLawyerService
    mCLEEventLawyerService.Init _
        HttpServer & CLEEventLawyerServiceWSDL, _
        HttpServer & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set CLEEventLawyerService = mCLEEventLawyerService
End Property

Public Property Get ContactService() As ContactService
  If mContactService Is Nothing Then
    Set mContactService = New ContactService
  End If
  Set ContactService = mContactService
End Property

Public Property Get ContactTypeService() As ContactTypeService
  If mContactTypeService Is Nothing Then
    Set mContactTypeService = New ContactTypeService
  End If
  Set ContactTypeService = mContactTypeService
End Property

Public Property Get CoverageService() As CoverageService
  If mCoverageService Is Nothing Then
    Set mCoverageService = New CoverageService
  End If
  Set CoverageService = mCoverageService
End Property

Public Property Get DeductibleReimburseService() As DeductibleReimburseService
  If mDeductibleReimburseService Is Nothing Then
    Set mDeductibleReimburseService = New DeductibleReimburseService
  End If
  Set DeductibleReimburseService = mDeductibleReimburseService
End Property

Public Property Get EFTIndividualService() As EFTIndividualService
  If mEFTIndividualService Is Nothing Then
    Set mEFTIndividualService = New EFTIndividualService
  End If
  Set EFTIndividualService = mEFTIndividualService
End Property

Public Property Get EndorsementService() As EndorsementService
  If mEndorsementService Is Nothing Then
    Set mEndorsementService = New EndorsementService
    mEndorsementService.Init _
        HttpServer & EndorsementServiceWSDL, _
        HttpServer & WebService & "rpcrouter" & LawSession.Session, _
        LawSession
  End If
  Set EndorsementService = mEndorsementService
End Property

Public Property Get EntityService() As EntityService
  If mEntityService Is Nothing Then
    Set mEntityService = New EntityService
  End If
  Set EntityService = mEntityService
End Property

Public Property Get EventTypeService() As EventTypeService
  If mEventTypeService Is Nothing Then
    Set mEventTypeService = New EventTypeService
  End If
  Set EventTypeService = mEventTypeService
End Property

Public Property Get EventRecipientService() As EventRecipientService
  If mEventRecipientService Is Nothing Then
    Set mEventRecipientService = New EventRecipientService
  End If
  Set EventRecipientService = mEventRecipientService
End Property

Public Property Get FirmService() As FirmService
  If mFirmService Is Nothing Then
    Set mFirmService = New FirmService
  End If
  Set FirmService = mFirmService
End Property

Public Property Get FirmInsurerService() As FirmInsurerService
  If mFirmInsurerService Is Nothing Then
    Set mFirmInsurerService = New FirmInsurerService
'    mFirmInsurerService.Init _
'        HttpServer &  FirmInsurerServiceWSDL, _
'        HttpServer &  WebService & "rpcrouter" & LawSession.Session, _
'        LawSession
  End If
  Set FirmInsurerService = mFirmInsurerService
End Property

Public Property Get FirmPolicyService() As FirmPolicyService
  If mFirmPolicyService Is Nothing Then
    Set mFirmPolicyService = New FirmPolicyService
  End If
  Set FirmPolicyService = mFirmPolicyService
End Property

'Public Property Get FolderService() As FolderService
'  If mFolderService Is Nothing Then
'    Set mFolderService = New FolderService
'    mFolderService.Init _
'        HttpServer &  FolderServiceWSDL, _
'        HttpServer &  WebService & "rpcrouter" & LawSession.Session, _
'        LawSession
'  End If
'  Set FolderService = mFolderService
'End Property
'
'Public Property Get FolderParameterService() As FolderParameterService
'  If mFolderParameterService Is Nothing Then
'    Set mFolderParameterService = New FolderParameterService
'  End If
'  Set FolderParameterService = mFolderParameterService
'End Property

Public Property Get GLAccountService() As GLAccountService
  If mGLAccountService Is Nothing Then
    Set mGLAccountService = New GLAccountService
  End If
  Set GLAccountService = mGLAccountService
End Property

Public Property Get IncidentService() As IncidentService
  If mIncidentService Is Nothing Then
    Set mIncidentService = New IncidentService
  End If
  Set IncidentService = mIncidentService
End Property

Public Property Get InstanceService() As InstanceService
  If mInstanceService Is Nothing Then
    Set mInstanceService = New InstanceService
  End If
  Set InstanceService = mInstanceService
End Property

Public Property Get InstancePaymentService() As InstancePaymentService
  If mInstancePaymentService Is Nothing Then
    Set mInstancePaymentService = New InstancePaymentService
  End If
  Set InstancePaymentService = mInstancePaymentService
End Property

Public Property Get LawyerService() As LawyerService
  If mLawyerService Is Nothing Then
    Set mLawyerService = New LawyerService
  End If
  Set LawyerService = mLawyerService
End Property

Public Property Get LawAreaService() As LawAreaService
  If mLawAreaService Is Nothing Then
    Set mLawAreaService = New LawAreaService
  End If
  Set LawAreaService = mLawAreaService
End Property

Public Property Get LawErrorService() As LawErrorService
  If mLawErrorService Is Nothing Then
    Set mLawErrorService = New LawErrorService
  End If
  Set LawErrorService = mLawErrorService
End Property

Public Property Get LocationService() As LocationService
  If mLocationService Is Nothing Then
    Set mLocationService = New LocationService
  End If
  Set LocationService = mLocationService
End Property

Public Property Get LocationTypeService() As LocationTypeService
  If mLocationTypeService Is Nothing Then
    Set mLocationTypeService = New LocationTypeService
  End If
  Set LocationTypeService = mLocationTypeService
End Property

Public Property Get OrganizationService() As OrganizationService
  If mOrganizationService Is Nothing Then
    Set mOrganizationService = New OrganizationService
  End If
  Set OrganizationService = mOrganizationService
End Property

Public Property Get PersistenceManagerService() As PersistenceManagerService
  If mPersistenceManagerService Is Nothing Then
    Set mPersistenceManagerService = New PersistenceManagerService
  End If
  Set PersistenceManagerService = mPersistenceManagerService
End Property

Public Property Get PersonService() As PersonService
  If mPersonService Is Nothing Then
    Set mPersonService = New PersonService
  End If
  Set PersonService = mPersonService
End Property

Public Property Get PolicyService() As PolicyService
  If mPolicyService Is Nothing Then
    Set mPolicyService = New PolicyService
  End If
  Set PolicyService = mPolicyService
End Property

Public Property Get PolicyAccountingService() As PolicyAccountingService
  If mPolicyAccountingService Is Nothing Then
    Set mPolicyAccountingService = New PolicyAccountingService
  End If
  Set PolicyAccountingService = mPolicyAccountingService
End Property

Public Property Get PreferenceService() As PreferenceService
  If mPreferenceService Is Nothing Then
    Set mPreferenceService = New PreferenceService
  End If
  Set PreferenceService = mPreferenceService
End Property

Public Property Get ReferenceService() As ReferenceService
  If mReferenceService Is Nothing Then
    Set mReferenceService = New ReferenceService
  End If
  Set ReferenceService = mReferenceService
End Property

Public Property Get ServiceService() As ServiceService
  If mServiceService Is Nothing Then
    Set mServiceService = New ServiceService
  End If
  Set ServiceService = mServiceService
End Property

Public Property Get TimeService() As TimeService
  If mTimeService Is Nothing Then
    Set mTimeService = New TimeService
  End If
  Set TimeService = mTimeService
End Property

Public Property Get VendorService() As VendorService
  If mVendorService Is Nothing Then
    Set mVendorService = New VendorService
  End If
  Set VendorService = mVendorService
End Property


