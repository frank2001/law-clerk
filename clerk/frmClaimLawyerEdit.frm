VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmClaimLawyerEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lawyer"
   ClientHeight    =   2355
   ClientLeft      =   120
   ClientTop       =   -14460
   ClientWidth     =   4320
   Icon            =   "frmClaimLawyerEdit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   4320
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   1944
      Left            =   0
      ScaleHeight     =   1950
      ScaleWidth      =   4320
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   384
      Width           =   4320
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   3108
         TabIndex        =   3
         Top             =   1548
         Width           =   1176
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1860
         TabIndex        =   2
         Top             =   1548
         Width           =   1176
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   1392
         Left            =   72
         TabIndex        =   4
         Top             =   96
         Width           =   4176
         _ExtentX        =   7355
         _ExtentY        =   2461
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   420
         TabCaption(0)   =   "Properties"
         TabPicture(0)   =   "frmClaimLawyerEdit.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label3"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblDataField(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label4"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtpDataField(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtpDataField(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmClaimLawyerEdit.frx":0028
            DataField       =   "firmLawyerDate"
            DataMember      =   "FirmLawyer"
            Height          =   288
            Index           =   0
            Left            =   816
            TabIndex        =   0
            Top             =   612
            Width           =   1416
            _ExtentX        =   2487
            _ExtentY        =   503
            _Version        =   393216
            Format          =   84738049
            CurrentDate     =   36892
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmClaimLawyerEdit.frx":003D
            DataField       =   "departDate"
            DataMember      =   "firmLawyer"
            Height          =   288
            Index           =   1
            Left            =   828
            TabIndex        =   1
            Top             =   984
            Width           =   1416
            _ExtentX        =   2487
            _ExtentY        =   503
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   84738049
            CurrentDate     =   37085
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Lawyer:"
            Height          =   192
            Left            =   108
            TabIndex        =   12
            Top             =   372
            Width           =   552
         End
         Begin VB.Label lblDataField 
            AutoSize        =   -1  'True
            Caption         =   "Lawyer:"
            DataField       =   "lawyerName"
            DataMember      =   "FirmLawyer"
            Height          =   192
            Index           =   0
            Left            =   816
            TabIndex        =   11
            Top             =   372
            Width           =   3216
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Depart:"
            Height          =   192
            Left            =   108
            TabIndex        =   10
            Top             =   1020
            Width           =   528
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Date:"
            Height          =   192
            Left            =   108
            TabIndex        =   9
            Top             =   660
            Width           =   384
         End
      End
      Begin MSComctlLib.ImageList imlUpDown 
         Left            =   264
         Top             =   1416
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   16777215
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList imlUpDownDisabled 
         Left            =   888
         Top             =   1512
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   4320
      _ExtentX        =   7620
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   4320
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   336
         Left            =   24
         ScaleHeight     =   330
         ScaleWidth      =   4230
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   24
         Width           =   4224
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   84
            Left            =   24
            TabIndex        =   7
            Top             =   24
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   159
            ButtonWidth     =   487
            ButtonHeight    =   466
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl +<)"
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   312
      Left            =   3036
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   528
      Width           =   876
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties..."
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeperator504 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Pre&vious"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Ne&xt"
      End
      Begin VB.Menu mnuSeperator503 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolBar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator993 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmClaimLawyerEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mClaimId As String
Private mLawyerId As String
Private mNew As Boolean
Private mDatafields As Collection
Private mDataSet As Dataset

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean

Private FormMgr As New FormManager

Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property

 
Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Sub cmdApply_Click()
  Dim Datafields As New Collection
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOk_Click()
  cmdApply_Click
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  PostMessage hWnd, WM_KEYDOWN, vbKeyTab, 0
End Sub

Private Sub cmdSave_Click()
  If save Then Unload Me
End Sub

Private Sub Form_Initialize()
   Set mDataSet = NewGenericDataset(DatabaseName.INSTANCELAWYER_TABLE)
   Set mDataSet.Session = LawSession
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0) Then
    Select Case KeyCode
      Case 190
        mnuViewNext_Click
      Case 188
        mnuViewPrevious_Click
    End Select
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  ' Custom Menu Shortcut Adjustments
  Me.KeyPreview = True
  
  mnuFileExit.Caption = mnuFileExit.Caption + Chr(9) + "Alt+F4"
  mnuViewPrevious.Caption = mnuViewPrevious.Caption + Chr(9) + "Ctrl+<"
  mnuViewNext.Caption = mnuViewNext.Caption + Chr(9) + "Ctrl+>"

  FormMgr.Load Me
  
  cbrCoolBar.Bands(1).MinHeight = tbrUpDown.ButtonHeight + 24
  
  FormUtil.EditLoad Me
  FormUtil.EditLoadImages Me
  FormMgr.Load Me
  
  FormResize
  
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

'
' FormInterface Interface
'

'
' FormLoad is called after Form_Initialize and before Form_Load
'
Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when parent form unloaded
'
Public Sub FormParentUnload()
  Set mListView = Nothing
End Sub

'
' Notify when a child form is unloaded.  Only child is lawyer select.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
  If Not fInterface.Child.Ok Then Exit Sub
  
  lbldatafield(0) = fInterface.Child.lvView.SelectedItem
  mLawyerId = fInterface.Child.lvView.SelectedItem.ListSubItems(DatabaseName.LAWYER_KEY)
  Show
End Sub

'
' End FormInterface
'

Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  With mListView.ListItems(vData)
    .Selected = True
'    mClaimId = .ListSubItems(databasename.CLAIM_KEY)
    mLawyerId = .ListSubItems(DatabaseName.LAWYER_KEY)
  End With
End Property


Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no lawyers to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
  mnuViewRefresh_Click
End Sub

Private Sub mnuFileClose_Click()

End Sub


Private Sub mnuFileProperties_Click()
  FormUtil.ShowProperties DatabaseName.INSTANCELAWYER_TABLE, mClaimId
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuView_Click()
  If (mListView Is Nothing) Then
    mnuViewPrevious.Enabled = False
    mnuViewNext.Enabled = False
    Exit Sub
  End If
  mnuViewPrevious.Enabled = mListView.Enabled And mListIndex > 1
  mnuViewNext.Enabled = mListIndex < mListView.ListItems.Count
End Sub

Private Sub mnuViewNext_Click()
  mnuView_Click
  If (Not mnuViewNext.Enabled) Then
    Exit Sub
  End If
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  mnuView_Click
  If (Not mnuViewPrevious.Enabled) Then
    Exit Sub
  End If
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  Dim mPrimaryKey As New Collection
'  mPrimaryKey.Add CreateDataField(mDataset.DataProvider.Source, databasename.CLAIM_KEY, mClaimId), databasename.CLAIM_KEY
  mPrimaryKey.Add CreateDataField(mDataSet.DataProvider.Source, DatabaseName.LAWYER_KEY, mLawyerId), DatabaseName.LAWYER_KEY
  
  If (mNew) Then
    Set mDatafields = mPrimaryKey
    Me.Caption = "New Lawyer"
  Else
    'Set mDatafields = mDataSet.FindByPrimaryKey(mPrimaryKey)
    Me.Caption = "Edit Lawyer (" & mDatafields.item(DatabaseName.LAWYER_KEY) & ")"
  End If
  
  Call SetFields(Me.lbldatafield, mDatafields)
  Call SetFields(Me.dtpDataField, mDatafields)
  
  'To Do later: Get the barAdmittedDate from the lawyer table.
  
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
 ' IIf mnuViewToolbar.Checked = True, Me.Height = Me.Height + cbrCoolBar.Height, Me.Height = Me.Height - cbrCoolBar.Height
  cbrCoolBar.Visible = mnuViewToolbar.Checked
  FormResize
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Next"
      mnuViewNext_Click
    Case "Previous"
      mnuViewPrevious_Click
  End Select
End Sub

'ToDo: Enable save function
Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Start custom Code
  
'  If mNew Then
'    Dim Handle As Variant
'    Handle = Factory.ApplicationLawyerService.CreateApplicationLawyer(mClaimId, _
'      mLawyerId, lblDataField(0).Caption, GetDate(dtpDataField(0).Value), GetDouble(txtDataField(2)), _
'      CDbl(0), CDbl(0), CDbl(0), txtDataField(1).Text, _
'      vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
'    Save = Not VarType(Handle) = vbEmpty
'  Else
'    Save = Factory.ApplicationLawyerService.WriteApplicationLawyer(mApplicationId, _
'      mLawyerId, lblDataField(0).Caption, GetDate(dtpDataField(0).Value), CDbl(txtDataField(2)), _
'      CDbl(0), CDbl(0), CDbl(0), txtDataField(1).Text, _
'      vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
'  End If
'
'  If Save Then
'    mDirty = False
'  End If
End Function


