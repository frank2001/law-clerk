VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Begin VB.Form frmGenericSearch 
   ClientHeight    =   7035
   ClientLeft      =   225
   ClientTop       =   855
   ClientWidth     =   8595
   Icon            =   "frmGenericSearch.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7035
   ScaleWidth      =   8595
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picSelectNone 
      BorderStyle     =   0  'None
      Height          =   456
      Left            =   5496
      ScaleHeight     =   450
      ScaleWidth      =   1485
      TabIndex        =   46
      Top             =   5124
      Width           =   1488
      Begin VB.CommandButton cmdSelectNone 
         Caption         =   "Select None"
         Height          =   336
         Left            =   108
         TabIndex        =   47
         Top             =   60
         Width           =   1200
      End
   End
   Begin VB.PictureBox picButtons 
      BorderStyle     =   0  'None
      Height          =   456
      Left            =   1272
      ScaleHeight     =   450
      ScaleWidth      =   4065
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   5124
      Width           =   4068
      Begin VB.CommandButton cmdNew 
         Caption         =   "&New"
         Height          =   336
         Left            =   1380
         TabIndex        =   26
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2640
         TabIndex        =   27
         Top             =   60
         Width           =   1200
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Enabled         =   0   'False
         Height          =   336
         Left            =   96
         TabIndex        =   25
         Top             =   60
         Width           =   1200
      End
   End
   Begin VB.PictureBox picSelect 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   492
      Left            =   0
      ScaleHeight     =   492
      ScaleMode       =   0  'User
      ScaleWidth      =   6909.528
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   4905
      Width           =   8595
   End
   Begin VB.Timer tmrSearch 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   6660
      Top             =   5712
   End
   Begin VB.PictureBox picListView 
      Align           =   1  'Align Top
      BackColor       =   &H80000005&
      Height          =   1392
      Left            =   0
      ScaleHeight     =   1335
      ScaleWidth      =   8535
      TabIndex        =   24
      TabStop         =   0   'False
      Tag             =   "Firm"
      Top             =   3510
      Width           =   8595
      Begin VB.PictureBox picSearchStatus 
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         Height          =   240
         Left            =   1704
         ScaleHeight     =   240
         ScaleWidth      =   4995
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   4992
         Begin VB.Label lblSearchStatus 
            AutoSize        =   -1  'True
            BackColor       =   &H80000009&
            BackStyle       =   0  'Transparent
            Caption         =   "Search Status"
            Height          =   192
            Left            =   0
            TabIndex        =   35
            Top             =   0
            Width           =   996
         End
      End
      Begin MSComctlLib.ListView lvView 
         Height          =   4152
         Left            =   1704
         TabIndex        =   28
         Top             =   12
         Width           =   5856
         _ExtentX        =   10319
         _ExtentY        =   7329
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         _Version        =   393217
         Icons           =   "imlIcons"
         SmallIcons      =   "imlSmallIcons"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   0
      End
      Begin VB.Label lblListView 
         BackColor       =   &H80000005&
         Caption         =   "Double click or press Enter on an item to view or edit the information. "
         Height          =   1572
         Index           =   1
         Left            =   84
         TabIndex        =   33
         Top             =   732
         Width           =   1212
      End
      Begin VB.Label lblListView 
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "Table Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   84
         TabIndex        =   32
         Top             =   0
         Width           =   1524
         WordWrap        =   -1  'True
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   29
      Top             =   6765
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7488
            Text            =   "0 object(s)"
            TextSave        =   "0 object(s)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "10/20/2010"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            TextSave        =   "2:14 PM"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   7344
      Top             =   5100
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":000C
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":011E
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0230
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0342
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0454
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0566
            Key             =   "View Large Icons"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0678
            Key             =   "View Small Icons"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":078A
            Key             =   "View List"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":089C
            Key             =   "View Details"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":09AE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlIcons 
      Left            =   7260
      Top             =   5676
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":0D0A
            Key             =   "book"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlSmallIcons 
      Left            =   10836
      Top             =   8364
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   13
      ImageHeight     =   13
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":1024
            Key             =   "closed"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":1196
            Key             =   "cylinder"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":1308
            Key             =   "leaf"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":147A
            Key             =   "open"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":15EC
            Key             =   "smlBook"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGenericSearch.frx":189E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picSearchTab 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3144
      Left            =   0
      ScaleHeight     =   3150
      ScaleWidth      =   8595
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   360
      Width           =   8595
      Begin VB.PictureBox picSearchButtons 
         BorderStyle     =   0  'None
         Height          =   2244
         Left            =   6864
         ScaleHeight     =   2250
         ScaleWidth      =   1380
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   312
         Width           =   1380
         Begin VB.CommandButton cmdSearchNow 
            Caption         =   "&Search Now"
            Default         =   -1  'True
            Height          =   336
            Left            =   96
            TabIndex        =   22
            Top             =   36
            Width           =   1080
         End
         Begin VB.CommandButton cmdNewSearch 
            Caption         =   "New Search"
            Height          =   336
            Left            =   96
            TabIndex        =   23
            Top             =   444
            Width           =   1080
         End
      End
      Begin TabDlg.SSTab sstabSearch 
         Height          =   2976
         Left            =   72
         TabIndex        =   43
         Top             =   84
         Width           =   6348
         _ExtentX        =   11192
         _ExtentY        =   5239
         _Version        =   393216
         Style           =   1
         Tabs            =   7
         TabsPerRow      =   7
         TabHeight       =   420
         TabCaption(0)   =   "Generic"
         TabPicture(0)   =   "frmGenericSearch.frx":1B50
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label3"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtWords"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "cboIn"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Advanced"
         TabPicture(1)   =   "frmGenericSearch.frx":1B6C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "chkPolicyIncludeVoid"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Advanced"
         TabPicture(2)   =   "frmGenericSearch.frx":1B88
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "chkFirmPolicyContact"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Advanced"
         TabPicture(3)   =   "frmGenericSearch.frx":1BA4
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "cboYearsOperator"
         Tab(3).Control(1)=   "txtCounty"
         Tab(3).Control(2)=   "chkCounty"
         Tab(3).Control(3)=   "chkRetroactiveDate"
         Tab(3).Control(4)=   "txtYearsInPractice"
         Tab(3).Control(5)=   "chkYearsInPractice"
         Tab(3).ControlCount=   6
         TabCaption(4)   =   "Advanced"
         TabPicture(4)   =   "frmGenericSearch.frx":1BC0
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "chkBeginDate"
         Tab(4).Control(1)=   "dtpToBeginDate"
         Tab(4).Control(2)=   "dtpFromBeginDate"
         Tab(4).Control(3)=   "Label4"
         Tab(4).Control(4)=   "Label5"
         Tab(4).ControlCount=   5
         TabCaption(5)   =   "Advanced"
         TabPicture(5)   =   "frmGenericSearch.frx":1BDC
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "chkCLEEvent(3)"
         Tab(5).Control(1)=   "chkCLEEvent(2)"
         Tab(5).Control(2)=   "txtCLEEvent(2)"
         Tab(5).Control(3)=   "chkCLEEvent(0)"
         Tab(5).Control(4)=   "chkCLEEvent(1)"
         Tab(5).Control(5)=   "txtCLEEvent(0)"
         Tab(5).Control(6)=   "txtCLEEvent(1)"
         Tab(5).Control(7)=   "dtpToEventDate"
         Tab(5).Control(8)=   "dtpFromEventDate"
         Tab(5).Control(9)=   "Label6"
         Tab(5).Control(10)=   "Label2"
         Tab(5).ControlCount=   11
         TabCaption(6)   =   "Advanced"
         TabPicture(6)   =   "frmGenericSearch.frx":1BF8
         Tab(6).ControlEnabled=   0   'False
         Tab(6).Control(0)=   "cboAdjuster"
         Tab(6).Control(1)=   "chkACA"
         Tab(6).Control(2)=   "Frame4"
         Tab(6).ControlCount=   3
         Begin VB.ComboBox cboAdjuster 
            Height          =   315
            ItemData        =   "frmGenericSearch.frx":1C14
            Left            =   -73860
            List            =   "frmGenericSearch.frx":1C16
            Style           =   2  'Dropdown List
            TabIndex        =   54
            Top             =   407
            Width           =   1776
         End
         Begin VB.CheckBox chkACA 
            Caption         =   "Adjuster:"
            Height          =   264
            Left            =   -74832
            TabIndex        =   53
            Top             =   432
            Width           =   984
         End
         Begin VB.Frame Frame4 
            Height          =   1224
            Left            =   -74832
            TabIndex        =   48
            Top             =   765
            Width           =   2010
            Begin VB.CheckBox chkDiaryDate 
               Caption         =   "Diary Date:"
               Height          =   264
               Left            =   96
               TabIndex        =   55
               Top             =   150
               Width           =   1320
            End
            Begin MSComCtl2.DTPicker dtpDiaryToDate 
               Height          =   285
               Left            =   600
               TabIndex        =   49
               Top             =   750
               Width           =   1275
               _ExtentX        =   2249
               _ExtentY        =   503
               _Version        =   393216
               Enabled         =   0   'False
               Format          =   120193025
               CurrentDate     =   37550
            End
            Begin MSComCtl2.DTPicker dtpDiaryFromDate 
               Height          =   285
               Left            =   600
               TabIndex        =   50
               Top             =   420
               Width           =   1275
               _ExtentX        =   2249
               _ExtentY        =   503
               _Version        =   393216
               Enabled         =   0   'False
               Format          =   120193025
               CurrentDate     =   37550
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "From:"
               Height          =   192
               Left            =   96
               TabIndex        =   52
               Top             =   468
               Width           =   408
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "To:"
               Height          =   192
               Left            =   96
               TabIndex        =   51
               Top             =   792
               Width           =   240
            End
         End
         Begin VB.CheckBox chkCLEEvent 
            Caption         =   "Event Date:"
            Height          =   192
            Index           =   3
            Left            =   -74832
            TabIndex        =   19
            Top             =   1440
            Width           =   1272
         End
         Begin VB.CheckBox chkCLEEvent 
            Caption         =   "Location:"
            Height          =   192
            Index           =   2
            Left            =   -74832
            TabIndex        =   17
            Top             =   1092
            Width           =   1272
         End
         Begin VB.TextBox txtCLEEvent 
            Height          =   288
            Index           =   2
            Left            =   -73524
            TabIndex        =   18
            Top             =   1044
            Width           =   2412
         End
         Begin VB.CheckBox chkCLEEvent 
            Caption         =   "Event Name:"
            Height          =   192
            Index           =   0
            Left            =   -74832
            TabIndex        =   13
            Top             =   420
            Width           =   1272
         End
         Begin VB.CheckBox chkCLEEvent 
            Caption         =   "Topic:"
            Height          =   192
            Index           =   1
            Left            =   -74832
            TabIndex        =   15
            Top             =   756
            Width           =   1272
         End
         Begin VB.TextBox txtCLEEvent 
            Height          =   288
            Index           =   0
            Left            =   -73524
            TabIndex        =   14
            Top             =   372
            Width           =   2412
         End
         Begin VB.TextBox txtCLEEvent 
            Height          =   288
            Index           =   1
            Left            =   -73524
            TabIndex        =   16
            Top             =   708
            Width           =   2412
         End
         Begin VB.CheckBox chkBeginDate 
            Caption         =   "Begin Date:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   10
            Top             =   432
            Width           =   1332
         End
         Begin VB.ComboBox cboYearsOperator 
            Height          =   315
            ItemData        =   "frmGenericSearch.frx":1C18
            Left            =   -73212
            List            =   "frmGenericSearch.frx":1C25
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   396
            Width           =   1344
         End
         Begin VB.TextBox txtCounty 
            Height          =   288
            Left            =   -73212
            TabIndex        =   8
            Top             =   708
            Width           =   2808
         End
         Begin VB.CheckBox chkCounty 
            Caption         =   "County:"
            Height          =   192
            Left            =   -74832
            TabIndex        =   7
            Top             =   756
            Width           =   1392
         End
         Begin VB.CheckBox chkRetroactiveDate 
            Caption         =   "Only show items with retroactive dates"
            Height          =   204
            Left            =   -74832
            TabIndex        =   9
            Top             =   1092
            Width           =   3492
         End
         Begin VB.TextBox txtYearsInPractice 
            Height          =   288
            Left            =   -71820
            TabIndex        =   6
            Top             =   396
            Width           =   504
         End
         Begin VB.CheckBox chkYearsInPractice 
            Caption         =   "Years in Practice:"
            Height          =   204
            Left            =   -74832
            TabIndex        =   4
            Top             =   432
            Width           =   1596
         End
         Begin VB.CheckBox chkFirmPolicyContact 
            Caption         =   "Firm Policy Contact"
            Height          =   192
            Left            =   -74832
            TabIndex        =   3
            Top             =   432
            Width           =   2136
         End
         Begin VB.CheckBox chkPolicyIncludeVoid 
            Caption         =   "Include Void"
            Height          =   192
            Left            =   -74832
            TabIndex        =   2
            Top             =   432
            Value           =   1  'Checked
            Width           =   1308
         End
         Begin VB.ComboBox cboIn 
            Height          =   315
            ItemData        =   "frmGenericSearch.frx":1C55
            Left            =   1572
            List            =   "frmGenericSearch.frx":1C57
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Tag             =   "Firm"
            Top             =   720
            Width           =   2160
         End
         Begin VB.TextBox txtWords 
            Height          =   300
            Left            =   1572
            TabIndex        =   0
            Top             =   372
            Width           =   2148
         End
         Begin MSComCtl2.DTPicker dtpToBeginDate 
            Height          =   288
            Left            =   -71520
            TabIndex        =   12
            Top             =   384
            Width           =   1224
            _ExtentX        =   2170
            _ExtentY        =   503
            _Version        =   393216
            Format          =   120586241
            CurrentDate     =   37532
         End
         Begin MSComCtl2.DTPicker dtpFromBeginDate 
            Height          =   288
            Left            =   -73032
            TabIndex        =   11
            Top             =   384
            Width           =   1176
            _ExtentX        =   2064
            _ExtentY        =   503
            _Version        =   393216
            Format          =   120586241
            CurrentDate     =   37524
         End
         Begin MSComCtl2.DTPicker dtpToEventDate 
            Height          =   288
            Left            =   -71532
            TabIndex        =   21
            Top             =   1380
            Width           =   1224
            _ExtentX        =   2170
            _ExtentY        =   503
            _Version        =   393216
            Format          =   120586241
            CurrentDate     =   37532
         End
         Begin MSComCtl2.DTPicker dtpFromEventDate 
            Height          =   288
            Left            =   -73056
            TabIndex        =   20
            Top             =   1380
            Width           =   1176
            _ExtentX        =   2064
            _ExtentY        =   503
            _Version        =   393216
            Format          =   119537665
            CurrentDate     =   37524
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "To:"
            Height          =   192
            Left            =   -71832
            TabIndex        =   45
            Top             =   1428
            Width           =   228
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "From:"
            Height          =   192
            Left            =   -73524
            TabIndex        =   44
            Top             =   1428
            Width           =   408
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "From:"
            Height          =   192
            Left            =   -73488
            TabIndex        =   42
            Top             =   432
            Width           =   408
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "To:"
            Height          =   192
            Left            =   -71808
            TabIndex        =   41
            Top             =   432
            Width           =   228
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Search f&or:"
            Height          =   192
            Left            =   168
            TabIndex        =   40
            Top             =   468
            Width           =   768
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Search in:"
            Height          =   192
            Left            =   168
            TabIndex        =   39
            Top             =   768
            Width           =   708
         End
      End
   End
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            Object.ToolTipText     =   "New"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Open"
            Object.ToolTipText     =   "Open"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Print"
            Object.ToolTipText     =   "Print"
            ImageKey        =   "Print"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Move"
            Object.ToolTipText     =   "Move to Folder"
            ImageKey        =   "Copy"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Delete"
            Object.ToolTipText     =   "Delete"
            ImageKey        =   "Delete"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode0"
            Object.ToolTipText     =   "View Large Icons"
            ImageKey        =   "View Large Icons"
            Style           =   2
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode1"
            Object.ToolTipText     =   "View Small Icons"
            ImageKey        =   "View Small Icons"
            Style           =   2
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode2"
            Object.ToolTipText     =   "View List"
            ImageKey        =   "View List"
            Style           =   2
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "lvMode3"
            Object.ToolTipText     =   "View Details"
            ImageKey        =   "View Details"
            Style           =   2
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpenMenu 
         Caption         =   "&Open"
         Begin VB.Menu mnuFileOpen 
            Caption         =   "mnuFileOpen"
            Index           =   0
         End
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFilePrintSetup 
         Caption         =   "Print Set&up"
      End
      Begin VB.Menu mnuFileBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder"
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "Propert&ies..."
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditCut 
         Caption         =   "Cu&t"
         Enabled         =   0   'False
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "&Paste"
         Enabled         =   0   'False
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEditBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditRotate 
         Caption         =   "Rotate"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSeparator8261829 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditSelectAll 
         Caption         =   "Select &All"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuEditInvertSelection 
         Caption         =   "&Invert Selection"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "Status &Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "Lar&ge Icons"
         Index           =   0
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "S&mall Icons"
         Index           =   1
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&List"
         Index           =   2
      End
      Begin VB.Menu mnuListViewMode 
         Caption         =   "&Details"
         Index           =   3
      End
      Begin VB.Menu mnuViewBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewArrangeIcons 
         Caption         =   "Arrange &Icons"
         Begin VB.Menu mnuArrangeIcon 
            Caption         =   "Columns"
            Index           =   0
         End
      End
      Begin VB.Menu mnuViewChooseColumns 
         Caption         =   "Choose Colum&ns..."
      End
      Begin VB.Menu mnuViewSaveSettings 
         Caption         =   "Sa&ve Settings"
      End
      Begin VB.Menu mnuViewBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsOptions 
         Caption         =   "&Options..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "&Search For Help On..."
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
      Begin VB.Menu mnuTestControl 
         Caption         =   "Test Control"
         Shortcut        =   ^T
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "&Popups"
      Begin VB.Menu mnuListItem 
         Caption         =   "mnuListItem"
         Begin VB.Menu mnuListItemSelect 
            Caption         =   "Select"
         End
         Begin VB.Menu mnuListItemOpen 
            Caption         =   "mnuListItemOpen"
            Index           =   0
         End
         Begin VB.Menu mnuSeparator3421 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupSendTo 
            Caption         =   "Sen&d To"
            Begin VB.Menu mnuListItemSendToFolder 
               Caption         =   "&Folder..."
            End
         End
         Begin VB.Menu mnuSeparator783 
            Caption         =   "-"
         End
         Begin VB.Menu mnuListItemProperties 
            Caption         =   "P&roperties"
         End
      End
      Begin VB.Menu mnuSubItem 
         Caption         =   "mnuSubItem"
         Begin VB.Menu mnuSubItemView 
            Caption         =   "&View"
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "Lar&ge Icons"
               Index           =   0
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "S&mall Icons"
               Index           =   1
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "&List"
               Index           =   2
            End
            Begin VB.Menu mnuSubItemViewIcon 
               Caption         =   "&Details"
               Index           =   3
            End
         End
         Begin VB.Menu mnuSeparator33 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemArrangeIcons 
            Caption         =   "Arrange &Icons"
            Begin VB.Menu mnuSubItemArrangeIcon 
               Caption         =   "mnuListViewArrangeIcon"
               Index           =   0
            End
         End
         Begin VB.Menu mnuSubItemRefresh 
            Caption         =   "R&efresh"
         End
         Begin VB.Menu mnuSeparator95 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemChooseColumns 
            Caption         =   "Choose Colum&ns..."
         End
         Begin VB.Menu mnuSeparator592 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemNew 
            Caption         =   "Ne&w"
         End
         Begin VB.Menu mnuSeparator343 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSubItemProperties 
            Caption         =   "P&roperties"
         End
      End
   End
End
Attribute VB_Name = "frmGenericSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ver 2.00.0303 (jjm) Core
'
' Main Form
'
'
' The main form can be one of 3 form types set by the the following
' global constants from MainModule:
'
' ftView - Folder list and main window
' ftAdvancedSearch - Look in list, advanced search and a list view
' ftSelect - Advanced search and list view pinned to a folder
'
'
'

Option Explicit

' Public manager is needed by popup menus.
Public FormMgr As New FormManager

Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

'local variable(s) to hold property value(s)
Private mCaptionText As String
Private mFolderKey As String
'Private mFolderKey As String
Private mView As View
Private mIsSearchOnly As Boolean

'ver 2.50.00 (jjm) 2004 02 17
Private mEnableSelectNone As Boolean
Public ReturnNull As Boolean

Private lastSearch As String
Private lastCount As Integer

Private mbMoving As Boolean
Const sglSplitLimit = 1000
Private isLoaded As Boolean

Public TxtWordsIndex As Integer

Private mPopup(1) As Form
Private mDefaultMenu As Menu
Private mUnloaded As Boolean

Private mPolicyHandle As String
Public Property Get PolicyHandle() As String
'Possibly create a function to return a handle by type passed in
'Public Function GetHandle(HandleType as String) as String
  PolicyHandle = mPolicyHandle
End Property

'
' Start Generic Section
'
' Generic code is copied from frmGenericSearch.  In an object
' oriented world, this would be included in an ancestor.
'
'

'
' If a child is unloaded, find last form of same name.  Load that first, then
' load the main form.
'
Public Sub FormChildUnload(ByRef vData As FormInterface)
  Dim LastForm As Form
  Set LastForm = FindLastForm(vData.Child)
  If LastForm Is Nothing Then
    Show
    Refresh
  Else
    LastForm.Show
  End If
End Sub

'
' Called when this form is loaded from a Form Manager
'
Public Sub FormInterfaceLoad(ByRef ParentInterface As FormInterface)
  Set FormMgr.ParentInterface = ParentInterface
End Sub


'
' Do nothing if a parent form is unloaded
'
Public Sub FormParentUnload()
End Sub


' Refresh folder

Public Sub MenuListItem(Optional X, Optional Y)
  MenuViewRefresh
  
  On Error Resume Next
  PopupMenu mnuListItem, , X, Y, mDefaultMenu
  On Error GoTo 0
  
  ' Either default menu doesn't exist or control array doesn't exist, or the default
  ' menu is a sub menu.
  If Err = 340 Or Err = 427 Or Err = 490 Then
    PopupMenu mnuListItem, , X, Y
  End If
End Sub

Public Sub MenuSubItem(Optional X, Optional Y)
  ' Eventually add Paste functionality here.
  PopupMenu mnuSubItem, , X, Y
End Sub


Public Sub MenuViewRefresh()
  Dim Count As Integer, IsCount, Selected As Integer, IsSelected, Workflow As Boolean
  Count = lvView.ListItems.Count
  IsCount = Count > 0
  Selected = ListViewModule.SelectedCount(lvView.hWnd)
  IsSelected = Selected > 0
  Workflow = mView.IsWorkflow
  
  
  ' set arrange check
  If (lvView.ColumnHeaders.Count > 0) Then
'    mnuArrangeIcon(lvView.SortKey + 1).Checked = True
'    Popup.mnuListViewArrangeIcon(lvView.SortKey + 1).Checked = True
  End If
  
  ' If an item selected, copy over popup open and explore.
  If IsSelected Then
    sbStatusBar.Panels(1).Text = Selected & " of " & Count & " object(s) selected"
    
    ' Copy over Open and Explore
    FormUtil.MenuCopy mView.OpenMenu, mnuFileOpen
    FormUtil.MenuCopy mView.OpenMenu, mnuListItemOpen
  Else
    sbStatusBar.Panels(1).Text = Count & " object(s)"
  End If
  
  mnuFileOpenMenu.Enabled = IsSelected And Not mnuFileOpen(0).Visible
'  mnuFilePrint.Enabled = IsCount
'  mnuFileExport.Enabled = IsSelected
  mnuFileSendToFolder.Enabled = IsSelected And Not Workflow
    
  mnuEditCut.Enabled = IsSelected And Workflow
  mnuEditPaste.Enabled = Not EventBuffer Is Nothing And Workflow
  
  mnuEditInvertSelection.Enabled = IsCount
  mnuEditSelectAll.Enabled = IsCount
  
  cmdOK.Enabled = IsSelected
  
  'ver 2.00.0303 (jjm)
  'cmdNew.Enabled = IsSelected
  
  If IsSelected Then
    cmdOK.Default = True
  Else
    cmdSearchNow.Default = True
  End If
  
  If (mIsSearchOnly) Then
    Set mDefaultMenu = mnuListItemOpen(mView.DefaultMenuIndex)
  Else
    Set mDefaultMenu = mnuListItemSelect
  End If
End Sub


Private Sub SetArrangeMenu()
End Sub


Public Sub LoadView()
  ' Setup list view for searching.
  lvView.ListItems.Clear
  picListView_Resize
  lblSearchStatus.Caption = "Enter your search criteria to begin."
  picSearchStatus.Visible = True
End Sub


Public Sub SearchNow()
  ' Show hourglass
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  lvView.ListItems.Clear
  lblSearchStatus.Caption = "Searching..."
  picSearchStatus.Visible = True
  lblSearchStatus.Refresh
  
  'lastSearch is used for tmrSearch
  lastSearch = txtWords.Text
  
  'Setup column, sort and find
  If lvView.ColumnHeaders.Count > 0 Then
    mView.Dataset.DataProvider.Column = LawDialect.Column(lvView.ColumnHeaders)
    mView.Dataset.DataProvider.Sort = LawDialect.Sort(lvView)
  End If
  
  mView.Dataset.FindByForm Me
  
  'SQL Debug - This is turned on and off in the Main Options
  If LocaleDebugAdvancedSearch Then
    DebugWhereClause mView.Dataset.DataProvider
  End If
  
  SearchResult
  MenuViewRefresh
End Sub

Public Sub SearchResult()
  If lvView.ListItems.Count = 0 Then
    lblSearchStatus.Caption = "Search is complete.  There are no results to display."
    picSearchStatus.Visible = True
  Else
    picSearchStatus.Visible = False
  End If
  MenuViewRefresh
  SetArrangeMenu
End Sub
'ver 2.50.00 2004 02 17
Public Sub ShowSearch(FolderKey As String, Optional IsSelect As Boolean, Optional TabIndex As Integer, Optional SearchInIndex As Integer, Optional SearchText As String, Optional CaptionText As String, Optional EnableSelectNone As Boolean)
  mFolderKey = FolderKey
  mIsSearchOnly = Not IsSelect
  mCaptionText = CaptionText
  mEnableSelectNone = EnableSelectNone
  
  Show
  sstabSearch.Tab = TabIndex
  
  If Not SearchInIndex = -1 Then
    If Not cboIn.ListCount < SearchInIndex - 1 Then
      cboIn.ListIndex = SearchInIndex
    End If
    'if it is a contact search and FirmId is the default search,
    'then show only firm policy contacts
    If sstabSearch.TabVisible(2) And SearchInIndex = UrlName.CONTACTSEARCH_ORGANIZATIONID_INDEX Then
      chkFirmPolicyContact.Value = vbChecked
    End If
  End If
  txtWords = SearchText
End Sub

Public Sub ShowAdvancedSearch(FolderKey As String)
  mFolderKey = FolderKey
  mIsSearchOnly = True
  Show
End Sub

Public Sub ShowSelect(FolderKey As String)
  mFolderKey = FolderKey
  Show
End Sub
'
' End General Methods
'
'
'
'
' Control Methods
'

Private Sub chkDiaryDate_Click()
  dtpDiaryToDate.Enabled = (chkDiaryDate.Value = vbChecked)
  dtpDiaryFromDate.Enabled = (chkDiaryDate.Value = vbChecked)
End Sub


Private Sub cmdClose_Click()
  mnuFileExit_Click
End Sub

Private Sub cmdNew_Click()
  mnuFileNew_Click
End Sub

Private Sub cmdNewSearch_Click()
  txtWords.Text = ""
  lvView.ListItems.Clear
  lblSearchStatus.Caption = "Enter your search criteria to begin."
  
  chkYearsInPractice.Value = vbUnchecked
  txtYearsInPractice.Text = vbNullString
  chkRetroactiveDate.Value = vbUnchecked
  chkBeginDate.Value = vbUnchecked
  
  dtpDiaryFromDate.Value = Today
  dtpDiaryToDate.Value = Today
    
  Dim I As Integer
  For I = 0 To 3
    chkCLEEvent(I).Value = vbUnchecked
  Next I
  For I = 0 To 2
    txtCLEEvent(I).Text = vbNullString
  Next I
    
  MenuViewRefresh
End Sub

Private Sub cmdOk_Click()
  If mIsSearchOnly Then
    MenuViewRefresh
    CallByName Me, mDefaultMenu.Name & "_Click", VbMethod, mDefaultMenu.Index
  Else
    FormMgr.Message = fimSelect
    Unload Me
  End If
End Sub

Public Sub cmdSearchNow_Click()
  mnuViewRefresh_Click
End Sub

'ver 2.50.00 (jjm) 2004 02 17
Private Sub cmdSelectNone_Click()
  ReturnNull = True
  FormMgr.Message = fimSelect
  Unload Me
End Sub

Private Sub dtpFromBeginDate_Change()
  chkBeginDate.Value = vbChecked
End Sub
Private Sub dtpToBeginDate_Change()
  chkBeginDate.Value = vbChecked
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyInsert Then
    mnuFileNew_Click
  End If
End Sub

Private Sub Form_Load()
  If mUnloaded Then
    MsgBox Name & ": Illegal Load Attempt.  Please report."
    Unload Me
    Exit Sub
  End If
  
  ' Load form manager.
  FormMgr.Load Me
      
  ' Setup child forms.
  Set mPopup(0) = New frmPopup
  Load mPopup(0)
  FormMgr.Add mPopup(0)
  
  Set mPopup(1) = New frmPopup2
  Load mPopup(1)
  FormMgr.Add mPopup(1)
  
  Dim isVowel As Boolean
  isVowel = InStr("aeio", LCase(Left(mFolderKey, 1)))
  
  Caption = IIf(mCaptionText = vbNullString, _
                IIf(mIsSearchOnly, "Search for ", "Select ") & IIf(isVowel, "an ", "a ") & mFolderKey, _
                mCaptionText)
  sstabSearch.Caption = mFolderKey
  
   
  ' Setup view.
  Set mView = New View
  FolderUtil.ViewLoad mView, mFolderKey, mPopup
  
  
  'Check to ensure the view was loaded correctly. If there is no folder
  'with a folderId same as the key, then the key is assumed to be the
  'datatype.
  If mView.Dataset.DataProvider.Source = vbNullString Then
    Set mView.Dataset = NewDataset(mFolderKey)
  End If
  
  
  ' Open dataset if necessary.
  sbStatusBar.Panels(2).Text = mView.Dataset.DataProvider.Source
    
  lvView.Tag = mFolderKey & IIf(mIsSearchOnly, "Search", "Select")
  lvView.ColumnHeaders.Clear
  lvView.ListItems.Clear
  ListViewModule.DeserializeColumnHeaders lvView, lvView.Tag
  mView.Dataset.LoadSearch Me
  
  LoadView

  ' Set toolbar
  tbToolBar.Buttons("lvMode" & lvView.View).Value = tbrPressed
    
  ' Setup form
  sstabSearch.TabVisible(1) = (mFolderKey = DatabaseName.POLICY_TABLE)
  sstabSearch.TabVisible(2) = (mFolderKey = DatabaseName.CONTACT_TABLE)
  sstabSearch.TabVisible(3) = (mFolderKey = DatabaseName.LAWYER_TABLE)
  sstabSearch.TabVisible(4) = (mFolderKey = DatabaseName.CLEEVENTLAWYER_TABLE)
  sstabSearch.TabVisible(5) = (mFolderKey = DatabaseName.CLEEVENT_TABLE)
  sstabSearch.TabVisible(6) = (mFolderKey = DatabaseName.INSTANCE_TABLE)
  
  'Contact
  chkFirmPolicyContact.Value = IIf(LocaleDefaultToFirmPolicyContact, 1, 0)
  
  'CLE Event Lawyer
  dtpFromBeginDate.Value = DateAdd("yyyy", -1, Today)
  dtpToBeginDate.Value = Today
  cboYearsOperator.ListIndex = 0
  'CLE
  dtpFromEventDate.Value = Today
  dtpToEventDate.Value = Today
  
  'Instance
  dtpDiaryFromDate.Value = Today
  dtpDiaryToDate.Value = Today
  FormUtil.ComboBoxRegistryLoad cboAdjuster, "adjusternames"
  If cboAdjuster.ListCount > 0 Then cboAdjuster.ListIndex = 0
  
  
  
  If TypeOf FormMgr.Parent Is frmApplicationLawyerEdit Then
    mPolicyHandle = FormMgr.Parent.PolicyHandle
    cmdSearchNow_Click
  End If
      
  If mIsSearchOnly Then
    picSelect.Visible = False
    picSelect.Enabled = False
    mnuListItemSelect.Visible = False
    cmdClose.Enabled = False
    picButtons.Left = -20000
  End If
  
  'Get view settings from ini file.
  mnuViewToolbar.Checked = IniMain.ReadFile(UrlName.MAIN_INI, "ViewToolbar")
  mnuViewStatusBar.Checked = IniMain.ReadFile(UrlName.MAIN_INI, "ViewStatusBar")
  
  ' Hide popup.
  mnuPopup.Visible = False
   
  ' SSTab Fix
  FormUtil.TabControlEnable sstabSearch
     
  ' position settings
  ' set defaults first
  Top = 0
  Left = 0
  Height = 7200
  Width = 9600
  FormUtil.DeserializePosition Me
  
  FormLoad
End Sub

Private Sub Form_Resize()
  ' Do not resize when minimized.
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  tbToolBar.Visible = mnuViewToolbar.Checked
  sbStatusBar.Visible = mnuViewStatusBar.Checked
  
  Dim BottomHeight As Integer
  BottomHeight = IIf(sbStatusBar.Visible, sbStatusBar.Height, 0)
  BottomHeight = BottomHeight + IIf(picSelect.Visible, picSelect.Height, 0)
  
  picListView.Height = Max(Me.ScaleHeight - BottomHeight - picListView.Top, 0)
  
  'ver 2.50.00 (jjm) 2004 02 18
  picSelectNone.Visible = mEnableSelectNone
  
  If mIsSearchOnly = False Then
    picButtons.Top = picSelect.Top
    'ver 2.50.00 (jjm) 2004 02 17
    If picSelectNone.Visible Then picSelectNone.Top = picSelect.Top
  End If
  
End Sub

'
' Uncomment MsgBox to test for proper form termination.
'
Private Sub Form_Terminate()
  'MsgBox "frmMain Terminate"
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  
  ' Close down form manager
  FormMgr.Unload
  
  ' Unload child forms
  Unload mPopup(0)
  Unload mPopup(1)
  
  mUnloaded = True
End Sub

Private Sub lblListView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  picListView_MouseUp Button, Shift, X, Y
End Sub

Public Sub lvView_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  ListViewSort lvView, ColumnHeader, mView.Dataset.DataProvider, True
End Sub

Private Sub lvview_DblClick()
  If cmdOK.Enabled Then cmdOk_Click
End Sub

Private Sub lvView_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF10 And (Shift And vbShiftMask) > 0 Then
    If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
      MenuSubItem picListView.Left + 24, picListView.Top + 24
    Else
      MenuListItem picListView.Left + 24, picListView.Top + 24
    End If
    KeyCode = 0
    Exit Sub
  End If
End Sub

Private Sub lvView_KeyUp(KeyCode As Integer, Shift As Integer)
  MenuViewRefresh
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If ListViewModule.SubItemClick(lvView.hWnd, X, Y) And ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    lvView.SelectedItem.Selected = False
  End If
   If Button = vbRightButton Then
     ' Trap for click on subitem or click on main item whitespace.
     If ListViewModule.SelectedCount(lvView.hWnd) = 0 Then
       MenuSubItem
     Else
       MenuListItem
     End If
  End If
  ' Don't call anything here in case form was unloaded in MenuSubItem.
End Sub

Private Sub lvView_GotFocus()
  If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
    cmdOK.Default = True
  End If
End Sub

Private Sub mnuArrangeIcon_Click(Index As Integer)
  Call lvView_ColumnClick(lvView.ColumnHeaders(Index))
End Sub

Private Sub mnuControl_Click()
  MsgBox Me.ActiveControl.Name & " TabIndex: " & Me.ActiveControl.TabIndex
End Sub

Private Sub mnuEditInvertSelection_Click()
  Dim n, lvCount As Integer
  lvCount = lvView.ListItems.Count
  
  For n = 1 To lvCount
    If lvView.ListItems(n).Selected = True Then
      lvView.ListItems(n).Selected = False
    Else
      lvView.ListItems(n).Selected = True
    End If
  Next n
  MenuViewRefresh
End Sub

Private Sub mnuEditRotate_Click()
  cboIn.ListIndex = (cboIn.ListIndex + 1) Mod cboIn.ListCount
  txtWords.SetFocus
End Sub

Private Sub mnuEditSelectAll_Click()
  ListViewModule.SelectAll lvView.hWnd
  MenuViewRefresh
End Sub

Private Sub mnuFile_Click()
  mnuFilePrint.Enabled = (lvView.ListItems.Count > 0)
  If ActiveControl Is lvView Then
    mnuFileProperties.Tag = vbNullString
    If ListViewModule.SelectedCount(lvView.hWnd) > 0 Then
      mnuFileProperties.Tag = ListViewModule.ListItemText(lvView.SelectedItem, DatabaseName.EVENT_KEY)
    End If
    mnuFileProperties.Enabled = mnuFileProperties.Tag <> vbNullString
  End If
  MenuViewRefresh
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileNew_Click()
  ' The parent form should use the FormInterface.Message property
  ' to see if there is a request for a new record (fimNew).  See
  ' frmUnderwritingComment.FormChildUnload method.
  If mIsSearchOnly Then
    Dispatcher.ShowNew mFolderKey, , FormMgr
    Exit Sub
  End If
  
  FormMgr.Message = fimNew
  Unload Me
End Sub

Private Sub mnuFileOpen_Click(Index As Integer)
  CallByName mView.PopupMenuForm, mView.OpenMenuName & "_Click", VbMethod, Index
End Sub

Private Sub mnuFilePrint_Click()
  PrintUtil.PrintDialog Me, lvView
End Sub

Private Sub mnuFilePrintSetup_Click()
  PrintUtil.PrintSetupDialog Me
End Sub

Private Sub mnuFileSendToFolder_Click()
  Dim FolderKey As String, ruleId As String, ReferenceId As String, EventName As String
  FolderKey = DatabaseName.FIRM_TABLE
  ruleId = DatabaseName.FIRM_REF
  ReferenceId = DatabaseName.FIRM_KEY
  EventName = "firmName"
  
  ' Create form and show.
  Dim Form As New frmFolderBrowseDialog
  If Not Form.ShowSendTo(ruleId) Then
    Unload Form
    Exit Sub
  End If
  
  '
  ' Form is OK
  '
  Refresh
  Dim pointer As New WaitPointer
  pointer.Default
  
  MsgBox "ToDo: enable Send To Folder"
'  Call Factory.EventService.Create( _
'      Form.EventTypeId, _
'      Group, _
'      Form.FolderKey, _
'      Source, _
'      ListViewText(lvView, ReferenceId), _
'      ListViewText(lvView, EventName))
  
  Unload Form
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp HelpName.ADVANCEDFIND_TOPIC
End Sub

Public Sub mnuListItemOpen_Click(Index As Integer)
  mnuFileOpen_Click Index
End Sub

Private Sub mnuListItemSelect_Click()
  cmdOk_Click
End Sub

Private Sub mnuListItemSendToFolder_Click()
  mnuFileSendToFolder_Click
End Sub

Private Sub mnuListViewMode_Click(Index As Integer)
  lvView.View = Index
  tbToolBar.Buttons("lvMode" & Index).Value = tbrPressed
End Sub

Private Sub mnuTestControl_Click()
  MsgBox ActiveControl.Name
End Sub

Private Sub mnuToolsOptions_Click()
  Dim fOptions As New frmMainOptionsDialog
  fOptions.Show vbModal, Me
  Unload fOptions
  picListView_Resize
End Sub

'
' Choose Columns.  Public because it is called from mPopup menu.
'
Public Sub mnuViewChooseColumns_Click()
  If ListViewModule.ColumnSettings(lvView, mView.Dataset.DataProvider.Source, lvView.Tag) Then
    LoadView
    lvView.Visible = True
  End If
End Sub

'
' Refresh - Public because called from mPopup
'
Public Sub mnuViewRefresh_Click()
  SearchNow
End Sub

'
' Serialize column headers
'
Private Sub mnuViewSaveSettings_Click()
  ListViewModule.SaveSettings lvView, lvView.Tag
End Sub

Private Sub mnuViewStatusBar_Click()
  mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
  Form_Resize
  IniMain.WriteFile UrlName.MAIN_INI, "ViewStatusbar", mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
  mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
  Form_Resize
  IniMain.WriteFile UrlName.MAIN_INI, "ViewToolbar", mnuViewToolbar.Checked
End Sub

Private Sub picListView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  lvView_MouseUp Button, Shift, X, Y
End Sub

Private Sub picListView_Resize()
  ' Do not resize when minimized
  If Me.WindowState = vbMinimized Then
    Exit Sub
  End If

  lvView.Left = IIf(LocaleEnableWebContent = True, 1716, 0)
  lvView.Height = Max(picListView.ScaleHeight - lvView.Top, 0)
  lvView.Width = Max(picListView.ScaleWidth - lvView.Left, 0)
  
  picSearchStatus.Top = lvView.Top + IIf(lvView.View = lvwReport, 240, 0)
  picSearchStatus.Left = lvView.Left
  picSearchStatus.Width = lvView.Width
  
  If Me.Width < 7180 Then
    Me.Width = 7180
    sstabSearch.Width = Me.Width - picSearchButtons.Width
    Exit Sub
   End If
    
   sstabSearch.Width = Me.Width - picSearchButtons.Width
   picSearchButtons.Left = sstabSearch.Left + sstabSearch.Width
   txtWords.Width = sstabSearch.Width - 1850
   cboIn.Width = txtWords.Width
End Sub

Private Sub picSelect_Resize()
  If Not mIsSearchOnly Then
    'ver 2.50.00 (jjm) 2004 02 17
     If mEnableSelectNone Then
       picSelectNone.Left = Max(ScaleWidth - picSelectNone.Width, 0)
       picButtons.Left = Max(ScaleWidth - (picButtons.Width + picSelectNone.Width), 0)
     Else
       picButtons.Left = Max(ScaleWidth - picButtons.Width, 0)
     End If
  End If
End Sub

Private Sub sstabSearch_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable sstabSearch
End Sub

'
' This is a rare case where the ToolBar is the master for functionality.
' Usually, menus contain the functionality
'
Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
      Case "New"
        mnuFileNew_Click
      Case "Open"
        cmdOk_Click
      Case "Print"
         mnuFilePrint_Click
      Case "Move"
          
      Case "Delete"
          MsgBox "Add delete code" 'ToDo:
      Case "View Large Icons"
          lvView.View = lvwIcon
          picListView_Resize
      Case "View Small Icons"
          lvView.View = lvwSmallIcon
          picListView_Resize
      Case "View List"
          lvView.View = lvwList
          picListView_Resize
      Case "View Details"
          lvView.View = lvwReport
          picListView_Resize
    End Select
End Sub

Private Sub tmrSearch_Timer()
  tmrSearch.Enabled = False
  If Len(txtWords.Text) > 2 And lastSearch <> txtWords.Text Then
    cmdSearchNow_Click
  End If
End Sub

Private Sub txtCLEEvent_Change(Index As Integer)
  chkCLEEvent(Index).Value = vbChecked
End Sub

Private Sub txtCounty_Change()
  chkCounty.Value = vbChecked
End Sub

Private Sub txtWords_GotFocus()
  FormUtil.SelAll txtWords
End Sub

Private Sub txtWords_KeyDown(KeyCode As Integer, Shift As Integer)
  If (KeyCode = vbKeyDown Or KeyCode = vbKeyUp) And (Shift And vbShiftMask) = 0 Then
    SendMessage lvView.hWnd, WM_KEYDOWN, KeyCode, 0
    KeyCode = 0
    cmdOK.Default = True
    Exit Sub
  End If
  If (KeyCode = vbKeyL) And (Shift And vbCtrlMask) > 0 Then
    cboIn.ListIndex = IIf(cboIn.ListIndex = cboIn.ListCount - 1, _
      0, cboIn.ListIndex + 1)
    txtWords_KeyPress 0
    KeyCode = 0
  End If
End Sub

Private Sub txtWords_KeyPress(KeyAscii As Integer)
  tmrSearch.Enabled = False
  cmdSearchNow.Default = True
  tmrSearch.Enabled = True
End Sub
'
'
'
' End Generic Section
'
'
'
'
' Start Custom Section
'
'


'
' Hook for inherited searches (frmFirmSearch, )
'
Public Sub FormLoad()
End Sub

Private Sub RefreshContactView()
  If Not sstabSearch.TabVisible(2) Then
    Exit Sub
  End If
  
End Sub

Private Sub txtYearsInPractice_Change()
  chkYearsInPractice.Value = IIf(txtYearsInPractice.Text = vbNullString, vbUnchecked, vbChecked)
End Sub
