VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEventDialog 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4095
   ClientLeft      =   30
   ClientTop       =   315
   ClientWidth     =   8430
   Icon            =   "frmEventView.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4095
   ScaleWidth      =   8430
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      Height          =   336
      Left            =   7164
      TabIndex        =   8
      Top             =   3636
      Width           =   1200
   End
   Begin VB.CommandButton cmdOptions 
      Caption         =   "&Options"
      Enabled         =   0   'False
      Height          =   336
      Left            =   7140
      TabIndex        =   2
      Top             =   1668
      Width           =   1200
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "&Apply"
      Height          =   336
      Left            =   5868
      TabIndex        =   7
      Top             =   3636
      Width           =   1200
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "&Deselect"
      Height          =   336
      Index           =   1
      Left            =   7140
      TabIndex        =   1
      Top             =   1244
      Width           =   1200
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "&Select"
      Height          =   336
      Index           =   0
      Left            =   7140
      TabIndex        =   0
      Top             =   816
      Width           =   1200
   End
   Begin VB.Frame fraLine 
      Height          =   36
      Left            =   132
      TabIndex        =   10
      Top             =   3432
      Width           =   8208
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "&Refresh"
      Height          =   336
      Left            =   7152
      TabIndex        =   3
      Top             =   2436
      Width           =   1200
   End
   Begin MSComctlLib.ListView lvView 
      Height          =   2412
      Left            =   144
      TabIndex        =   4
      Tag             =   "EventDialog"
      Top             =   792
      Width           =   6900
      _ExtentX        =   12171
      _ExtentY        =   4260
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Key             =   "eventTypeName"
         Text            =   "Type"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Key             =   "eventDesc"
         Text            =   "Description"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "~folderPath"
         Text            =   "In Folder"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Key             =   "eventId"
         Text            =   "eventId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Key             =   "folderId"
         Text            =   "folderId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Key             =   "eventTypeId"
         Text            =   "eventTypeId"
         Object.Width           =   0
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   336
      Left            =   4572
      TabIndex        =   6
      Top             =   3636
      Width           =   1200
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   336
      Left            =   3300
      TabIndex        =   5
      Top             =   3636
      Width           =   1200
   End
   Begin VB.Label lblInstructions 
      Caption         =   "Click on the box next to the events you would like selected."
      Height          =   588
      Index           =   0
      Left            =   204
      TabIndex        =   9
      Top             =   108
      Width           =   5232
   End
End
Attribute VB_Name = "frmEventDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Ok As Boolean
Private mReference As Dataset
Private mHandle As String
Private mActionType As Integer
Private mCheckedCount As Integer

'
' Refresh command buttons
'
Public Sub ButtonRefresh()
  If lvView.SelectedItem Is Nothing Then
    cmdSelect(0).Enabled = False
    cmdSelect(1).Enabled = False
    cmdOptions.Enabled = False
  Else
    With lvView.SelectedItem
      cmdSelect(0).Enabled = Not .Checked
      cmdSelect(1).Enabled = .Checked
    End With
    cmdOptions.Enabled = True
  End If
  cmdApply.Enabled = (mCheckedCount > 0)
End Sub

'
' Refresh form from references.
'
Public Sub FormRefresh()
  mReference.DataProvider.Filter = LawDialect.EqualString(DatabaseName.REFERENCE_KEY, mHandle) & " and " & _
                                   LawDialect.ColumnIsEmpty(DatabaseName.COMPLETEDATE_FIELD) & " and " & _
                                   LawDialect.ColumnIsEmpty(DatabaseName.VOIDDATE_FIELD)
  ListViewLoad lvView, mReference.DataProvider, True
  
  'manually add a row for the item being referenced
  Dim eventRow As Collection
  Set eventRow = Factory.EventService.Find(mHandle)
  If GetDataFromCollection(eventRow, DatabaseName.EVENTSTATUS_FIELD) = DatabaseName.PENDING_EVENTSTATUS Then
    Dim refRow As ListItem
    Set refRow = ListViewModule.AddListItem(lvView)
    ListViewModule.SetItemCollection refRow, eventRow
  End If
  
  Dim item As Variant
  For Each item In lvView.ListItems
    item.ListSubItems("~folderPath").Text = Factory.PreferenceService.GetPath(SystemFolders_TreeName, item.ListSubItems(DatabaseName.FOLDER_KEY))
  Next item
  
  If lvView.ListItems.Count > 0 Then
    ListViewModule.AutosizeWidth lvView
  End If
  
  mCheckedCount = 0
  ButtonRefresh
End Sub

'
' Process checked references.
'
Public Function Process() As Boolean
  Dim item As ListItem, Count As Integer
  
  Dim Handles(0) As String
  Dim ParameterNames() As String
  Dim ParameterValues() As String
  
  For Each item In lvView.ListItems
    With item
      If .Checked Then
        Handles(0) = .ListSubItems(DatabaseName.EVENT_KEY).Text
        'Old SysFolder config no longer exists
        'Factory.FolderParameterService.ParameterLoad .ListSubItems(DatabaseName.FOLDER_KEY).Text, mActionType, ParameterNames, ParameterValues, True
        If mActionType = lawActionComplete Then
          Factory.EventService.SetComplete Handles, ParameterNames, ParameterValues
        ElseIf mActionType = lawActionVoid Then
          Factory.EventService.SetVoid Handles, ParameterNames, ParameterValues
        End If
      End If
    End With
  Next item
  Process = True
End Function

'
' Load references.
'
Public Sub ReferenceLoad(eventId As String, ActionType As Integer)
  mHandle = eventId
  mActionType = ActionType
  Caption = ActionText(ActionType) & " Pending Event(s)"
End Sub
'
' End General
'
'
'
' Begin Control
'

Private Sub cmdApply_Click()
  If Process Then FormRefresh
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdColumn_Click()
  ChooseColumns lvView, mReference.DataProvider, False
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub cmdOk_Click()
  If Process Then Unload Me
End Sub

Private Sub cmdOptions_Click()
  If Not ListItemText(lvView.SelectedItem, DatabaseName.FOLDER_KEY) = vbNullString Then
    FolderUtil.WorkflowShow ListItemText(lvView.SelectedItem, DatabaseName.FOLDER_KEY), mActionType
  Else
    MsgBox "There is not a folderId associated with this event. The workflow options can not be displayed."
  End If
End Sub

Private Sub cmdRefresh_Click()
  FormRefresh
End Sub

Private Sub cmdSelect_Click(Index As Integer)
  lvView.SelectedItem.Checked = (Index = 0)
  mCheckedCount = mCheckedCount + IIf(lvView.SelectedItem.Checked, 1, -1)
  ButtonRefresh
  lvView.SetFocus
End Sub

Private Sub Form_Initialize()
  Set mReference = NewGenericDataset(DatabaseName.REFERENCEVIEW_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  FormRefresh
End Sub

Private Sub lvview_DblClick()
  If cmdOptions.Enabled Then
    cmdOptions_Click
  End If
End Sub

Private Sub lvView_ItemCheck(ByVal item As MSComctlLib.ListItem)
  mCheckedCount = mCheckedCount + IIf(item.Checked, 1, -1)
  ButtonRefresh
End Sub

Private Sub lvView_ItemClick(ByVal item As MSComctlLib.ListItem)
  ButtonRefresh
End Sub
