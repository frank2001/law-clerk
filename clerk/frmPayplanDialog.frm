VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmPayplanDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Quarterly Payment Plan Cycle"
   ClientHeight    =   2040
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   7155
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2040
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   2796
      Left            =   0
      ScaleHeight     =   2790
      ScaleWidth      =   7155
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   0
      Width           =   7152
      Begin VB.CommandButton cmdOptions 
         Caption         =   "&Options"
         Height          =   336
         Left            =   5820
         TabIndex        =   4
         Top             =   1104
         Width           =   1200
      End
      Begin VB.CommandButton cmdFolder 
         Caption         =   "&Folder"
         Height          =   336
         Left            =   132
         TabIndex        =   1
         Top             =   1104
         Width           =   1212
      End
      Begin VB.CommandButton CancelButton 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   5820
         TabIndex        =   3
         Top             =   528
         Width           =   1200
      End
      Begin VB.CommandButton OKButton 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   5820
         TabIndex        =   2
         Top             =   120
         Width           =   1200
      End
      Begin MSComCtl2.DTPicker dtpDate 
         Height          =   288
         Left            =   132
         TabIndex        =   0
         Top             =   684
         Width           =   1368
         _ExtentX        =   2408
         _ExtentY        =   503
         _Version        =   393216
         Format          =   20643841
         CurrentDate     =   37439
      End
      Begin VB.Label lblFolder 
         Height          =   192
         Left            =   156
         TabIndex        =   6
         Top             =   1596
         Width           =   6876
      End
      Begin VB.Label Label1 
         Caption         =   "To run the quarterly payment plan cycle, select a date, select a folder and press OK."
         Height          =   384
         Left            =   108
         TabIndex        =   5
         Top             =   168
         Width           =   5208
      End
   End
End
Attribute VB_Name = "frmPayplanDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public mOk As Boolean

Private Sub CancelButton_Click()
  Hide
End Sub

Private Sub cmdFolder_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowSelect(, lblFolder.Caption) Then
    lblFolder.Caption = fForm.FolderPath
  End If
  Unload fForm
End Sub


Private Sub Form_Load()
  dtpDate.Value = Today
  lblFolder.Caption = IniMain.ReadFile(UrlName.MISC_INI, "PayPlanFolder")
End Sub

Private Sub OKButton_Click()
  If lblFolder.Caption = vbNullString Then
    MsgBox "You must select a folder to continue."
    Exit Sub
  End If
  
  'Write the folder name to the ini
  IniMain.WriteFile UrlName.MISC_INI, "PayPlanFolder", lblFolder.Caption

  mOk = True
  Hide
End Sub

