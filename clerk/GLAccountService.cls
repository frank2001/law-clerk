VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GLAccountService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const ServiceName = "ReferenceService"

Private mClient As SoapClient30
Private mDataSet As Dataset
Private mGLGroup As Dataset

'
' Initialize service and connect via SOAP
'
Private Sub Class_Initialize()
  Set mDataSet = NewGenericDataset(DatabaseName.GLACCOUNT_TABLE)
  Set mGLGroup = NewGenericDataset(DatabaseName.GLACCOUNTGROUP_VIEW)
End Sub

'
' Default finder for single key tables.
'
Public Function Find(Handle As String, Optional Column As String) As Collection
  Set Find = mDataSet.DataProvider.Find(DatabaseName.GLACCOUNT_KEY, Handle, Column)
End Function

'
' Interface to server
'
Public Function GroupCollection(groupId As String) As Collection
  mGLGroup.DataProvider.Column = DatabaseName.GLACCOUNT_KEY & ", " & DatabaseName.ACCOUNTNAME_FIELD
  mGLGroup.DataProvider.Filter = LawDialect.EqualString(DatabaseName.EVENTGROUPID_FIELD, groupId)
  mGLGroup.DataProvider.Sort = DatabaseName.GLACCOUNT_KEY
  mGLGroup.DataProvider.OpenRecordset
  Set GroupCollection = mGLGroup.DataProvider.Collection
End Function

