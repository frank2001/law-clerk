VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrintDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print"
   ClientHeight    =   4224
   ClientLeft      =   48
   ClientTop       =   432
   ClientWidth     =   6084
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4224
   ScaleWidth      =   6084
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   5415
      Left            =   0
      ScaleHeight     =   5412
      ScaleWidth      =   6084
      TabIndex        =   0
      Top             =   0
      Width           =   6084
      Begin VB.Frame fraLine 
         Height          =   135
         Left            =   150
         TabIndex        =   10
         Top             =   3240
         Width           =   5775
      End
      Begin MSComctlLib.ListView lvReport 
         Height          =   1812
         Left            =   144
         TabIndex        =   9
         Top             =   1320
         Width           =   4332
         _ExtentX        =   7641
         _ExtentY        =   3196
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         Height          =   336
         Left            =   4680
         TabIndex        =   8
         Top             =   3720
         Width           =   1200
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "&Print"
         Default         =   -1  'True
         Height          =   336
         Left            =   3360
         TabIndex        =   7
         Top             =   3720
         Width           =   1200
      End
      Begin VB.CommandButton cmdPrintPreview 
         Caption         =   "Print Pre&view"
         Height          =   336
         Left            =   4680
         TabIndex        =   6
         Top             =   720
         Width           =   1200
      End
      Begin VB.CommandButton cmdPrintSetup 
         Caption         =   "Printer &Setup"
         Height          =   336
         Left            =   4680
         TabIndex        =   5
         Top             =   270
         Width           =   1200
      End
      Begin VB.ComboBox cboGroup 
         Height          =   315
         ItemData        =   "frmPrintDialog.frx":0000
         Left            =   1320
         List            =   "frmPrintDialog.frx":000A
         TabIndex        =   4
         Text            =   "Combo1"
         Top             =   735
         Width           =   3135
      End
      Begin VB.Label Label3 
         Caption         =   "Report Group:"
         Height          =   195
         Left            =   150
         TabIndex        =   3
         Top             =   795
         Width           =   1095
      End
      Begin VB.Label lblCurrentPrinter 
         Caption         =   "CurrentPrinter"
         Height          =   195
         Left            =   1320
         TabIndex        =   2
         Top             =   330
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "Current Printer:"
         Height          =   195
         Left            =   150
         TabIndex        =   1
         Top             =   330
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmPrintDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public mOk As Boolean

Private Sub cboGroup_Click()
  If cboGroup.ListIndex = 0 Then
    RestoreList
  Else
    ClearList
  End If
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdPrint_Click()
  mOk = True
  Unload Me
End Sub


Private Sub Form_Load()
  mOk = False
  cboGroup.ListIndex = 0
  RestoreList
End Sub

Private Sub ClearList()
  lvReport.ListItems.Clear
End Sub

Private Sub RestoreList()
  lvReport.ListItems.Clear
  lvReport.ListItems.Add , , "Quote Form" & vbTab & "(AE)"
  lvReport.ListItems.Add , , "Quote Form" & vbTab & "(AO)"
  lvReport.ListItems.Add , , "Quote Form" & vbTab & "(AQ)"
End Sub

' Overview:
'
' This form was to be used to prepare a report to be printed.
' Currently this form is only being used as a yes / no dialog box.
'
' How it was supposed to work:
' Ideally the user would select which "Report Group" they wished to
' print a report for. After selecting the group, a list of optional
' reports would be listed in lvReport. From there the methods for that
' report would be launched by using Crystal Report objects.
'
' Currently this is shown only on frmMain through the tools report menu
' item. Only one type of report is created through law being "AE".
'
