VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PrintPageLayout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'
' Encapsulate Page Layout
'

Option Explicit

Private m_LeftMargin As Integer
Private m_RightMargin As Integer
Private m_TopMargin As Integer
Private m_BottomMargin As Integer
Private m_PaperSize As Integer
Private m_Orientation As Integer
Private m_Header As PrintText
Private m_Footer As PrintText

' The Footer property

Public Property Get Footer() As PrintText
    Set Footer = m_Footer
End Property

Public Property Set Footer(Footer As PrintText)
    Set m_Footer = Footer
End Property

' The Header property

Public Property Get Header() As PrintText
    Set Header = m_Header
End Property

Public Property Set Header(Header As PrintText)
    Set m_Header = Header
End Property

' The Orientation property

Public Property Get Orientation() As Integer
    Orientation = m_Orientation
End Property

Public Property Let Orientation(ByVal newValue As Integer)
    m_Orientation = newValue
End Property

Private Sub Class_Initialize()
    m_LeftMargin = 15
    m_RightMargin = 15
    m_TopMargin = 15
    m_BottomMargin = 15
    If Not Printer Is Nothing Then
        m_PaperSize = Printer.PaperSize
        m_Orientation = Printer.Orientation
    Else
        m_PaperSize = vbPRPSLetter
        m_Orientation = vbPRORPortrait
    End If
    Set m_Header = New PrintText
    Set m_Footer = New PrintText
End Sub

' The PaperSize property

Public Property Get PaperSize() As Integer
    PaperSize = m_PaperSize
End Property

Public Property Let PaperSize(ByVal newValue As Integer)
    m_PaperSize = newValue
End Property

' The BottomMargin property

Public Property Get BottomMargin() As Integer
    BottomMargin = m_BottomMargin
End Property

Public Property Let BottomMargin(ByVal newValue As Integer)
    m_BottomMargin = newValue
End Property

' The TopMargin property

Public Property Get TopMargin() As Integer
    TopMargin = m_TopMargin
End Property

Public Property Let TopMargin(ByVal newValue As Integer)
    m_TopMargin = newValue
End Property

' The RightMargin property

Public Property Get RightMargin() As Integer
    RightMargin = m_RightMargin
End Property

Public Property Let RightMargin(ByVal newValue As Integer)
    m_RightMargin = newValue
End Property

' The LeftMargin property

Public Property Get LeftMargin() As Integer
    LeftMargin = m_LeftMargin
End Property

Public Property Let LeftMargin(ByVal newValue As Integer)
    m_LeftMargin = newValue
End Property

Private Sub Class_Terminate()
    Set m_Header = Nothing
    Set m_Footer = Nothing
End Sub
