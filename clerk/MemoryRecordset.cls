VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MemoryDataProvider"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
' ver 2.00.0219 (jjm)
Option Explicit
Implements DataProvider

Private mvarPrimaryKey As Collection
Private mvarColumn As String
Private mvarSource As String
Private mvarFilter As String
Private mvarSort As String
Private mvarIsNew As Boolean
Private mvarBaseFilter As String

Private Sub Create(Datafields As Collection)
  Dim CreateSQL As String
  Dim ParameterSQL As String
  Dim I As Integer
  For I = 1 To Datafields.Count
    If I = 1 Then
      ParameterSQL = "?"
    Else
      ParameterSQL = ParameterSQL + ", ?"
    End If
  Next
  CreateSQL = "insert into " + mvarSource + " columns (" + ParameterSQL + ") values (" + ParameterSQL
  MsgBox CreateSQL
  Dim item As Variant
  For Each item In Datafields
    MsgBox item.Source + ", " + item.Name
  Next
End Sub


Private Property Get DataProvider_BaseFilter() As String
  DataProvider_BaseFilter = mvarBaseFilter
End Property

Private Property Let DataProvider_BaseFilter(ByVal vData As String)
  mvarBaseFilter = vData
End Property

Public Property Set DataProvider_PrimaryKey(ByVal vData As Collection)
  Set mvarPrimaryKey = vData
End Property


Public Property Get DataProvider_PrimaryKey() As Collection
  Set DataProvider_PrimaryKey = mvarPrimaryKey
End Property


Public Property Set DataProvider_DataFields(ByVal vData As Collection)
  If mvarIsNew Then
    Create vData
  End If
End Property


Public Property Get DataProvider_DataFields() As Collection
End Property


Private Property Get DataProvider_IsOpen() As Boolean
  DataProvider_IsOpen = False
End Property

Private Property Get DataProvider_IsEmpty() As Boolean
  DataProvider_IsEmpty = True
End Property

Private Property Let DataProvider_IsNew(ByVal vData As Boolean)
  mvarIsNew = vData
End Property


Private Property Get DataProvider_IsNew() As Boolean
  DataProvider_IsNew = mvarIsNew
End Property


Private Property Get DataProvider_Params() As String
  DataProvider_Params = "select "
  If mvarColumn = "" Then
    DataProvider_Params = DataProvider_Params & "*"
  Else
    DataProvider_Params = DataProvider_Params & mvarColumn
  End If
  DataProvider_Params = DataProvider_Params & " from " & mvarSource
  'ver 2.00.0219 (jjm)
  If Not (mvarFilter = "" And mvarBaseFilter = "") Then
    DataProvider_Params = DataProvider_Params & " where " & mvarFilter & _
      IIf(mvarBaseFilter = vbNullString, vbNullString, " and ") & mvarBaseFilter
  End If
  If Not mvarSort = "" Then
    DataProvider_Params = DataProvider_Params & " order by " & mvarSort
  End If
End Property


Private Sub DataProvider_OpenRecordset(Optional Count As Integer)
  MsgBox DataProvider_Params
End Sub


Private Property Let DataProvider_Filter(ByVal vData As String)
  mvarFilter = vData
End Property


Private Property Get DataProvider_Filter() As String
  DataProvider_Filter = mvarFilter
End Property


Private Property Set DataProvider_Recordset(ByVal vData As Variant)
End Property


Private Property Get DataProvider_Recordset() As Variant
End Property


Private Property Let DataProvider_Source(ByVal vData As String)
  mvarSource = vData
End Property


Private Property Get DataProvider_Source() As String
  DataProvider_Source = mvarSource
End Property


Private Property Let DataProvider_Column(ByVal vData As String)
  mvarColumn = vData
End Property


Private Property Get DataProvider_Column() As String
  DataProvider_Column = mvarColumn
End Property


Private Property Let DataProvider_Sort(ByVal vData As String)
  mvarSort = vData
End Property


Private Property Get DataProvider_Sort() As String
  DataProvider_Sort = mvarSort
End Property


Private Property Get DataProvider_Collection() As Collection
End Property

Private Sub DataProvider_SetListView(lvView As Object, Optional isIcon As Boolean = True)
  Dim xItem As ListItem
  
  lvView.ColumnHeaders.Clear
  lvView.ListItems.Clear
  
  ' Add a ListItem setting its text, icon and small icon. Then
  ' add three ListSubItems setting the Key and Text of each.
  Set xItem = lvView.ListItems.Add(Key:="Add", _
  Text:="Add Record", Icon:="book", SmallIcon:="closed")

  lvView.ColumnHeaders.Add , , "Key Field"
  lvView.ColumnHeaders.Add , , "Field Name"

  ' Add a ListItem setting its text, icon and small icon. Then
  ' add three ListSubItems setting the Key and Text of each.
  Set xItem = lvView.ListItems.Add(Key:="Key Value", _
  Text:="Key Value", Icon:="book", SmallIcon:="closed")

  xItem.ListSubItems.Add Key:="Field Name", Text:="Field Value"
End Sub


Public Function DataProvider_Find(HandleName As String, Handle, Optional Column As String) As Collection
End Function

Public Function DataProvider_FindMulti(HandleNames() As String, Handles, Optional Column As String) As Collection
End Function

