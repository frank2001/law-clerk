VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCLEEventEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CLE Event Edit"
   ClientHeight    =   3900
   ClientLeft      =   4545
   ClientTop       =   1905
   ClientWidth     =   4245
   Icon            =   "frmCLE.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3900
   ScaleWidth      =   4245
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3492
      Left            =   0
      ScaleHeight     =   3495
      ScaleWidth      =   4245
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   384
      Width           =   4248
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         Height          =   336
         Left            =   1656
         TabIndex        =   9
         Top             =   3048
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   2964
         TabIndex        =   11
         Top             =   3048
         Width           =   1200
      End
      Begin TabDlg.SSTab SSTabCLE 
         Height          =   2868
         Left            =   72
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   96
         Width           =   4092
         _ExtentX        =   7223
         _ExtentY        =   5054
         _Version        =   393216
         Style           =   1
         Tabs            =   1
         TabHeight       =   520
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "frmCLE.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLocation"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblTopic"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblCLECredits"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblDate"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblAttg"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblEventName"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblEventID"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtpDataField(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtDataField(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtDataField(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtDataField(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtDataField(5)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtDataField(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtDataField(3)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         Begin VB.TextBox txtDataField 
            DataField       =   "eventLocation"
            DataMember      =   "CLE"
            Height          =   288
            Index           =   3
            Left            =   1344
            TabIndex        =   3
            Text            =   "eventLocation"
            Top             =   1428
            Width           =   2640
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventTopic"
            DataMember      =   "CLE"
            Height          =   288
            Index           =   2
            Left            =   1344
            TabIndex        =   2
            Text            =   "eventTopic"
            Top             =   1092
            Width           =   2640
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "cleCredit"
            DataMember      =   "CLE"
            Height          =   288
            Index           =   5
            Left            =   1344
            TabIndex        =   6
            Text            =   "cleCredit"
            Top             =   2436
            Width           =   516
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "attendingStaff"
            DataMember      =   "CLE"
            Height          =   288
            Index           =   4
            Left            =   1344
            TabIndex        =   4
            Text            =   "attendingStaff"
            Top             =   1764
            Width           =   2640
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "eventName"
            DataMember      =   "CLE"
            Height          =   288
            Index           =   1
            Left            =   1344
            TabIndex        =   1
            Text            =   "eventName"
            Top             =   768
            Width           =   2640
         End
         Begin VB.TextBox txtDataField 
            DataField       =   "cleEventId"
            DataMember      =   "CLE"
            Height          =   288
            Index           =   0
            Left            =   1344
            TabIndex        =   0
            Text            =   "cleEventId"
            Top             =   432
            Width           =   1920
         End
         Begin MSComCtl2.DTPicker dtpDataField 
            Bindings        =   "frmCLE.frx":0028
            DataField       =   "eventDate"
            DataMember      =   "CLE"
            Height          =   288
            Index           =   0
            Left            =   1356
            TabIndex        =   5
            Top             =   2100
            Width           =   1440
            _ExtentX        =   2540
            _ExtentY        =   503
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   93519873
            CurrentDate     =   37523
         End
         Begin VB.Label lblEventID 
            AutoSize        =   -1  'True
            Caption         =   "Event ID"
            Height          =   192
            Left            =   132
            TabIndex        =   18
            Top             =   504
            Width           =   636
         End
         Begin VB.Label lblEventName 
            AutoSize        =   -1  'True
            Caption         =   "Event Name"
            Height          =   192
            Left            =   132
            TabIndex        =   17
            Top             =   840
            Width           =   888
         End
         Begin VB.Label lblAttg 
            AutoSize        =   -1  'True
            Caption         =   "Attg"
            Height          =   192
            Left            =   132
            TabIndex        =   16
            Top             =   1824
            Width           =   276
         End
         Begin VB.Label lblDate 
            AutoSize        =   -1  'True
            Caption         =   "Date"
            Height          =   192
            Left            =   132
            TabIndex        =   15
            Top             =   2160
            Width           =   348
         End
         Begin VB.Label lblCLECredits 
            AutoSize        =   -1  'True
            Caption         =   "CLE Credits"
            Height          =   192
            Left            =   132
            TabIndex        =   14
            Top             =   2484
            Width           =   840
         End
         Begin VB.Label lblTopic 
            AutoSize        =   -1  'True
            Caption         =   "Topic"
            Height          =   192
            Left            =   132
            TabIndex        =   13
            Top             =   1164
            Width           =   420
         End
         Begin VB.Label lblLocation 
            AutoSize        =   -1  'True
            Caption         =   "Location"
            Height          =   192
            Left            =   132
            TabIndex        =   12
            Top             =   1500
            Width           =   612
         End
      End
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Height          =   324
      Left            =   276
      TabIndex        =   8
      Top             =   780
      Width           =   1056
   End
   Begin ComCtl3.CoolBar cbrCoolBar 
      Align           =   1  'Align Top
      Height          =   384
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   4248
      _ExtentX        =   7488
      _ExtentY        =   688
      BandCount       =   1
      _CBWidth        =   4245
      _CBHeight       =   390
      _Version        =   "6.7.9782"
      Child1          =   "picUpDown"
      MinHeight1      =   330
      Width1          =   1065
      NewRow1         =   0   'False
      Begin VB.PictureBox picUpDown 
         BorderStyle     =   0  'None
         Height          =   330
         Left            =   30
         ScaleHeight     =   330
         ScaleWidth      =   4125
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   30
         Width           =   4125
         Begin MSComctlLib.Toolbar tbrUpDown 
            Height          =   264
            Left            =   72
            TabIndex        =   21
            Top             =   60
            Width           =   732
            _ExtentX        =   1296
            _ExtentY        =   476
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Next"
                  Object.ToolTipText     =   "Next Record (Ctrl + >)"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Previous"
                  Object.ToolTipText     =   "Previous Record (Ctrl + <)"
                  ImageIndex      =   1
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList imlUpDown 
      Left            =   0
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlUpDownDisabled 
      Left            =   2760
      Top             =   0
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuFileNew 
            Caption         =   "&CLE Event"
            Shortcut        =   ^N
         End
         Begin VB.Menu mnuSeparator3872 
            Caption         =   "-"
         End
         Begin VB.Menu mnuFileNewFirm 
            Caption         =   "&Firm"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSeparator873 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Complete..."
         Enabled         =   0   'False
         Index           =   1
      End
      Begin VB.Menu mnuFileWorkflow 
         Caption         =   "&Void..."
         Enabled         =   0   'False
         Index           =   2
      End
      Begin VB.Menu mnuSeparatro2823 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSendTo 
         Caption         =   "Sen&d To"
         Begin VB.Menu mnuFileSendToFolder 
            Caption         =   "&Folder..."
         End
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "P&roperties"
      End
      Begin VB.Menu mnuSeparator712 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Undo/Refresh"
      End
      Begin VB.Menu mnuSeparator86732 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewNext 
         Caption         =   "Next"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPrevious 
         Caption         =   "Previous"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeperator969 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsAdvancedFind 
         Caption         =   "A&dvanced Find..."
         Enabled         =   0   'False
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSeperator999 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsWorkflowOptions 
         Caption         =   "&Workflow Options..."
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
      End
      Begin VB.Menu mnuSeperator321 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
End
Attribute VB_Name = "frmCLEEventEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.72.00 (jjm) 2005 04 21 - bug fix and new feature
'ver 2.50.00 (jjm) 2004 02 17
Option Explicit

Private mCLEEventHandle As String
Private mNew As Boolean
Private mData As DataProvider

Private mListIndex As Integer
Private mListView As ListView
Private mDirty As Boolean
Private mCancel As Boolean

' Form Manager
Private FormMgr As New FormManager

' Constants
Private Const TXT_CLEEVENTID_INDEX = 0
Private Const TXT_CLECREDIT_INDEX = 5
Private Const DTP_EVENTDATE_INDEX = 0


Public Property Get CLEEventHandle() As String
  CLEEventHandle = mCLEEventHandle
End Property

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    FormUtil.Dirty Me, mDirty, mNew
  End If
End Property


Public Sub FormInterfaceLoad(ByRef vData As FormInterface)
  Set FormMgr.ParentInterface = vData
End Sub

'
' Notify when a child form is unloaded.
'
Public Sub FormChildUnload(ByRef fInterface As FormInterface)
End Sub

'
' Notify when parent form unloaded.  If form supports scrolling, then
' scrolling should be disabled.
'
Public Sub FormParentUnload()
End Sub


Private Sub FormRefresh()

  ClearDataField txtDataField
  ClearDataField dtpDataField
  
  If (mNew) Then
     Caption = "New CLEEvent"
     'ver 2.50.00 (jjm) 2004 02 17
     dtpDataField(0).Value = Today
  Else
    Dim Datafields As New Collection
    Set Datafields = mData.Find(DatabaseName.CLEEVENT_KEY, mCLEEventHandle)
    SetFields txtDataField, Datafields
    SetFields dtpDataField, Datafields
    Caption = "CLE Edit (" & mCLEEventHandle & ")"
    txtDataField(TXT_CLEEVENTID_INDEX).Enabled = False
    txtDataField(TXT_CLEEVENTID_INDEX).BackColor = cmdSave.BackColor
    txtDataField(TXT_CLECREDIT_INDEX).Enabled = False
    txtDataField(TXT_CLECREDIT_INDEX).BackColor = cmdSave.BackColor
    dtpDataField(DTP_EVENTDATE_INDEX).Enabled = False
  End If

  FormUtil.NavigationRefresh Me, mListView, mListIndex
  
  Dirty = False
         
  mnuFileOpen.Enabled = Not mNew
  mnuFileSendTo.Enabled = Not mNew
  mnuFileProperties.Enabled = Not mNew
  
  mnuFileWorkflow(1).Enabled = False
  mnuFileWorkflow(2).Enabled = False
  
End Sub

Private Sub FormResize()
  Height = picBody.Top + picBody.Height + (Height - ScaleHeight)
End Sub

Private Property Let ListIndex(vData As Integer)
  mListView.SelectedItem.Selected = False
  mListIndex = vData
  mListView.ListItems(vData).Selected = True
  mCLEEventHandle = ListItemText(mListView.SelectedItem, DatabaseName.CLEEVENT_KEY)
End Property

Private Function save(Optional Verify As Boolean) As Boolean
  ' Start Standard Code
  
  ' If there have been changes, and we are not on a new record or
  ' we were just calling to verify, then we can consider the record saved.
  If Not mDirty And (Not mNew Or Verify) Then
    save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        save = False
        Exit Function
    End Select
  End If
  
  ' End Standard Code
  
  ' Start custom Code
  
  '
  ' Save Data
  '
  Dim mDataControls As New Collection
  AddTextControl txtDataField, mDataControls
  AddValueControl dtpDataField, mDataControls


  If (mNew) Then
    mCLEEventHandle = Factory.CLEEventService.CreateCLEEvent( _
                mDataControls(DatabaseName.CLEEVENT_KEY), _
                mDataControls("eventName"), mDataControls("eventTopic"), _
                mDataControls("eventLocation"), mDataControls("attendingStaff"), _
                GetDouble(mDataControls("cleCredit")), GetDate(mDataControls("eventDate")), _
                vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
    save = Len(mCLEEventHandle) > 0
    mNew = Not save
  Else
    'ver 2.72.00 (jjm) 2005 04 21
    If MsgBox("You are attempting to change CLE credit information. This could impact underwriting. Would you like to continue?", vbYesNo, "Continue?") = vbYes Then
      save = Factory.CLEEventService.WriteCLEEvent( _
                  mCLEEventHandle, mDataControls(DatabaseName.CLEEVENT_KEY), _
                  mDataControls("eventName"), mDataControls("eventTopic"), _
                  mDataControls("eventLocation"), mDataControls("attendingStaff"), _
                  GetDouble(mDataControls("cleCredit")), GetDate(mDataControls("eventDate")), _
                  vbNullStringArray, vbNullStringArray, vbNullStringArray, vbNullStringArray)
    End If

  End If
  
  Dirty = Not save
  
  'End custom code
End Function

Public Sub ShowNew(Optional CLEEventHandle As String)
  mCLEEventHandle = CLEEventHandle
  mNew = True
  Show
  
End Sub

Public Sub ShowByListView(View As ListView)
  If View.ListItems.Count = 0 Then
    MsgBox "There are no events to view or edit.  To insert, press [Insert]."
    Exit Sub
  End If
  Set mListView = View
  ListIndex = View.SelectedItem.Index
  Show
End Sub

Public Sub ShowByPrimaryKey(CLEEventHandle As String)
  mCLEEventHandle = CLEEventHandle
  Show
End Sub

Private Sub cboDataField_Click(Index As Integer)
  Dirty = True
End Sub

Private Sub cmdCancel_Click()
  mCancel = True
  Unload Me
End Sub

Private Sub cmdEnter_Click()
  FormUtil.PostTab Me.hWnd
End Sub

'
' Open up a select form for firm.  The response to the select
' is processed under FormChildUnload.
'
Public Sub cmdFirm_Click()
  Dispatcher.ShowSearch DatabaseName.FIRM_TABLE, FormMgr, True
End Sub

Private Sub cmdSave_Click()
  Dim WasNew As Boolean
  WasNew = mNew
  If save Then
    FormMgr.Message = fimSave
    If TypeOf FormMgr.Parent Is frmCLEEventOpen Then
      Unload Me
      Exit Sub
    End If
    
    Unload Me
    
    If WasNew Then
      ' DON'T register FormMgr.
      FormUtil.ShowByPrimaryKey New frmCLEEventOpen, mCLEEventHandle
    End If
  End If
End Sub

Private Sub dtpDataField_Change(Index As Integer)
  Dirty = True
End Sub

'
' Initailize non-visible properties
'
Private Sub Form_Initialize()
  Set mData = LawSession.NewDataProvider(DatabaseName.CLEEVENT_TABLE)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If ((Shift And vbCtrlMask) > 0 And (Shift And vbShiftMask) > 0) Then
    Select Case KeyCode
      Case vbKeyF
        mnuFileNewFirm_Click
        KeyCode = 0
    End Select
  End If
    If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , Me.hWnd
  End If

End Sub

Private Sub Form_Load()
  FormUtil.DeserializePosition Me
  'Custom Accelerators
  KeyPreview = True
  mnuFileNewFirm.Caption = mnuFileNewFirm.Caption + vbTab + "Ctrl+Shift+F"
  
  FormUtil.EditLoadImages Me
  FormUtil.EditLoad Me
  FormMgr.Load Me
 
'  Factory.FolderParameterService.ParameterLoad DatabaseName.CLEEVENT_TABLE, lawActionNew
  
  FormRefresh
End Sub

Private Sub Form_Paint()
  FormResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If Not FormMgr("Firm") Is Nothing Then
    MsgBox "Can not unload form until firm selection is complete."
    FormMgr("Firm").Child.Show
    Cancel = True
    Exit Sub
  End If
  
  If UnloadMode <> vbFormCode Then
    Cancel = Not save(True)
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  FormUtil.SerializePosition Me
  FormMgr.Unload
End Sub

Private Sub mnuFileExit_Click()
  If save(True) Then Unload Me
End Sub

Private Sub mnuFileNew_Click()
  If Not save(True) Then
    Exit Sub
  End If
  mNew = True
  mCLEEventHandle = vbNullString
  FormRefresh
End Sub

Private Sub mnuFileNewFirm_Click()
  'Dim fForm As New frmFirmContactEdit
  'FormMgr.Add fForm, "NewFirm"
  'fForm.ShowNew
End Sub

'
' Check for changes and open CLE
'
Private Sub mnuFileOpen_Click()
  If Not save(True) Then Exit Sub
  
  If TypeOf FormMgr.Parent Is frmCLEEventOpen Then
    Unload Me
    Exit Sub
  End If
  
  Unload Me
  
  Dim fCLEOpen As New frmCLEEventOpen
  fCLEOpen.ShowByPrimaryKey mCLEEventHandle
End Sub

Private Sub mnuFileProperties_Click()
  FormUtil.ShowProperties DatabaseName.CLEEVENT_TABLE, mCLEEventHandle
End Sub

Private Sub mnuFileSave_Click()
  save
End Sub

'Private Sub mnuFileWorkflow_Click(Index As Integer)
'  'MANTIS ISSUE 1459
'  'FormUtil.ShowEventDialog mEventHandle, Index
'  MsgBox "Functionality pending Mantis Issue 1459"
'
'  FormRefresh
'End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpContents_Click()
  FormUtil.ShowHelp , Me.hWnd
End Sub

Private Sub mnuToolsWorkflowOptions_Click()
'  FolderUtil.WorkflowShow DatabaseName.CLEEVENT_TABLE, lawActionNew, _
'      mParameterNames, mParameterValues
End Sub

Private Sub mnuViewNext_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex + 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewPrevious_Click()
  If Not save(True) Then Exit Sub
  ListIndex = mListIndex - 1
  mnuViewRefresh_Click
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub tbrUpDown_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Index
    Case 1
      Call mnuViewNext_Click
    Case 2
      Call mnuViewPrevious_Click
  End Select
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  'ver 2.72.00 (jjm) 2005 04 21 - bug fix
  If Index = 0 And mNew Then
    mCLEEventHandle = txtDataField(Index).Text
  End If
  Dirty = True
End Sub

Private Sub txtDataField_GotFocus(Index As Integer)
  FormUtil.EditTextSelect txtDataField(Index)
End Sub
