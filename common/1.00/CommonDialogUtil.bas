Attribute VB_Name = "CommonDialogUtil"
Option Explicit

Public Const FW_NORMAL = 400
Public Const DEFAULT_CHARSET = 1
Public Const OUT_DEFAULT_PRECIS = 0
Public Const CLIP_DEFAULT_PRECIS = 0
Public Const DEFAULT_QUALITY = 0
Public Const DEFAULT_PITCH = 0
Public Const FF_ROMAN = 16
Public Const CF_PRINTERFONTS = &H2
Public Const CF_SCREENFONTS = &H1
Public Const CF_BOTH = (CF_SCREENFONTS Or CF_PRINTERFONTS)
Public Const CF_EFFECTS = &H100&
Public Const CF_FORCEFONTEXIST = &H10000
Public Const CF_INITTOLOGFONTSTRUCT = &H40&
Public Const CF_LIMITSIZE = &H2000&
Public Const REGULAR_FONTTYPE = &H400
Public Const LF_FACESIZE = 32
Public Const CCHDEVICENAME = 32
Public Const CCHFORMNAME = 32
Public Const GMEM_MOVEABLE = &H2
Public Const GMEM_ZEROINIT = &H40
Public Const DM_DUPLEX = &H1000
Public Const DM_ORIENTATION = &H1
Public Const PD_PRINTSETUP = &H40
Public Const PD_DISABLEPRINTTOFILE = &H80000

Public Type PAGESETUPDLG_TYPE
  lStructSize As Long
  hwndOwner As Long
  hDevMode As Long
  hDevNames As Long
  flags As Long
  ptPaperSize As POINTAPI
  rtMinMargin As RECT
  rtMargin As RECT
  hInstance As Long
  lCustData As Long
  lpfnPageSetupHook As Long
  lpfnPagePaintHook As Long
  lpPageSetupTemplateName As String
  hPageSetupTemplate As Long
End Type

Public Type PRINTDLG_TYPE
  lStructSize As Long
  hwndOwner As Long
  hDevMode As Long
  hDevNames As Long
  hDC As Long
  flags As Long
  nFromPage As Integer
  nToPage As Integer
  nMinPage As Integer
  nMaxPage As Integer
  nCopies As Integer
  hInstance As Long
  lCustData As Long
  lpfnPrintHook As Long
  lpfnSetupHook As Long
  lpPrintTemplateName As String
  lpSetupTemplateName As String
  hPrintTemplate As Long
  hSetupTemplate As Long
End Type

Public Type DEVNAMES
    wDriverOffset As Integer
    wDeviceOffset As Integer
    wOutputOffset As Integer
    wDefault As Integer
    extra As String * 100
End Type

Public Type DEVMODE
    dmDeviceName As String * CCHDEVICENAME
    dmSpecVersion As Integer
    dmDriverVersion As Integer
    dmSize As Integer
    dmDriverExtra As Integer
    dmFields As Long
    dmOrientation As Integer
    dmPaperSize As Integer
    dmPaperLength As Integer
    dmPaperWidth As Integer
    dmScale As Integer
    dmCopies As Integer
    dmDefaultSource As Integer
    dmPrintQuality As Integer
    dmColor As Integer
    dmDuplex As Integer
    dmYResolution As Integer
    dmTTOption As Integer
    dmCollate As Integer
    dmFormName As String * CCHFORMNAME
    dmUnusedPadding As Integer
    dmBitsPerPel As Integer
    dmPelsWidth As Long
    dmPelsHeight As Long
    dmDisplayFlags As Long
    dmDisplayFrequency As Long
End Type

Public Declare Function PrintDlg Lib "comdlg32.dll" Alias "PrintDlgA" (pPrintdlg As PRINTDLG_TYPE) As Long

Public Declare Function PageSetupDlg Lib "comdlg32.dll" Alias "PageSetupDlgA" (pPagesetupdlg As PAGESETUPDLG_TYPE) As Long

Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)
Public Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Public Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long
Public Declare Function GlobalAlloc Lib "kernel32" (ByVal wFlags As Long, ByVal dwBytes As Long) As Long
Public Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long
'
' Use API to show page setup dialog box.
'
Public Function ShowPageSetup(frmOwner As Form, Margin As RECT) As Boolean
    Dim m_PSD As PAGESETUPDLG_TYPE
    'Set the structure size
    m_PSD.lStructSize = Len(m_PSD)
    'Set the owner window
    m_PSD.hwndOwner = frmOwner.hWnd
    'Set the application instance
    m_PSD.hInstance = App.hInstance
    'no extra flags
    m_PSD.flags = 0
    
    m_PSD.ptPaperSize.X = 8500
    m_PSD.ptPaperSize.Y = 11000
    m_PSD.rtMargin.Top = 2000
    

    'Show the pagesetup dialog
    ShowPageSetup = PageSetupDlg(m_PSD)
    If Not ShowPageSetup Then Exit Function
  
    Margin.Top = m_PSD.rtMargin.Top
End Function


'
' Use API to show printer dialog.
'
Public Function ShowPrinter(frmOwner As Form, Optional PrintFlags As Long) As Boolean
    Dim pd As PRINTDLG_TYPE
    Dim Dm As DEVMODE
    Dim Dn As DEVNAMES

    Dim lpDm As Long, lpDn As Long
    Dim bReturn As Integer
    Dim objPrinter As Printer, NewPrinterName As String

    ' Use PrintDialog to get the handle to a memory
    ' block with a Dm and Dn structures
    pd.lStructSize = Len(pd)
    pd.hwndOwner = frmOwner.hWnd
    pd.flags = PrintFlags
    
    On Error Resume Next
    'Set the current orientation and duplex setting
    Dm.dmDeviceName = Printer.DeviceName
    Dm.dmSize = Len(Dm)
    Dm.dmFields = DM_ORIENTATION Or DM_DUPLEX
    Dm.dmPaperWidth = Printer.Width
    Dm.dmOrientation = Printer.Orientation
    Dm.dmPaperSize = Printer.PaperSize
    Dm.dmDuplex = Printer.Duplex
    On Error GoTo 0

    'Allocate memory for the initialization hDm structure
    'and copy the settings gathered above into this memory
    pd.hDevMode = GlobalAlloc(GMEM_MOVEABLE Or GMEM_ZEROINIT, Len(Dm))
    lpDm = GlobalLock(pd.hDevMode)
    If lpDm > 0 Then
        CopyMemory ByVal lpDm, Dm, Len(Dm)
        bReturn = GlobalUnlock(pd.hDevMode)
    End If

    'Set the current driver, device, and port name strings
    With Dn
        .wDriverOffset = 8
        .wDeviceOffset = .wDriverOffset + 1 + Len(Printer.DriverName)
        .wOutputOffset = .wDeviceOffset + 1 + Len(Printer.Port)
        .wDefault = 0
    End With

    With Printer
        Dn.extra = .DriverName & Chr(0) & .DeviceName & Chr(0) & .Port & Chr(0)
    End With

    'Allocate memory for the initial hDn structure
    'and copy the settings gathered above into this memory
    pd.hDevNames = GlobalAlloc(GMEM_MOVEABLE Or GMEM_ZEROINIT, Len(Dn))
    lpDn = GlobalLock(pd.hDevNames)
    If lpDn > 0 Then
        CopyMemory ByVal lpDn, Dn, Len(Dn)
        bReturn = GlobalUnlock(lpDn)
    End If

    ' Call the print dialog.  Exit false up and let the user make changes
    ShowPrinter = PrintDlg(pd)
    If Not ShowPrinter Then
      Exit Function
    End If
    
    'First get the DevNames structure.
    lpDn = GlobalLock(pd.hDevNames)
    CopyMemory Dn, ByVal lpDn, 45
    bReturn = GlobalUnlock(lpDn)
    GlobalFree pd.hDevNames

    'Next get the DevMode structure and set the printer
    'properties appropriately
    lpDm = GlobalLock(pd.hDevMode)
    CopyMemory Dm, ByVal lpDm, Len(Dm)
    bReturn = GlobalUnlock(pd.hDevMode)
    GlobalFree pd.hDevMode
        
    NewPrinterName = UCase$(Left(Dm.dmDeviceName, InStr(Dm.dmDeviceName, Chr$(0)) - 1))
    If UCase$(Printer.DeviceName) <> NewPrinterName Then
      For Each objPrinter In Printers
        If UCase$(objPrinter.DeviceName) = NewPrinterName Then
          Set Printer = objPrinter
          Exit For
        End If
      Next
    End If

    'Set printer object properties according to selections made
    'by user
    
    'Some printers don't support the selected values.
    On Error Resume Next
    Printer.Copies = Dm.dmCopies
    Printer.Duplex = Dm.dmDuplex
    
    ' Workaroung for PrintDlgA not defaulting to appropriate orientation.
    If PrintFlags And cdlPDPrintSetup Then
      Printer.Orientation = Dm.dmOrientation
    End If
    
    Printer.PaperSize = Dm.dmPaperSize
    Printer.PrintQuality = Dm.dmPrintQuality
    Printer.ColorMode = Dm.dmColor
    Printer.PaperBin = Dm.dmDefaultSource
    PrintFlags = pd.flags
    On Error GoTo 0
End Function

'
' Helper Method
'
Public Sub PrintSetupDialog(formOwner As Form)
  Dim PrintFlags As Long
  PrintFlags = cdlPDPrintSetup
  ShowPrinter formOwner, PrintFlags
End Sub

Public Function GetFont(Dialog As CommonDialog) As StdFont
  Dim Font As New StdFont
  Font.Name = Dialog.FontName
  Font.Size = Dialog.FontSize
  Font.Bold = Dialog.FontBold
  Font.Italic = Dialog.FontItalic
  Font.Underline = Dialog.FontUnderline
  Font.Strikethrough = Dialog.FontStrikethru
  Set GetFont = Font
End Function

Public Sub SetFont(Dialog As CommonDialog, Font As StdFont)
  Dialog.FontName = Font.Name
  Dialog.FontSize = Font.Size
  Dialog.FontBold = Font.Bold
  Dialog.FontItalic = Font.Italic
  Dialog.FontUnderline = Font.Underline
  Dialog.FontStrikethru = Font.Strikethrough
End Sub

Public Function HttpSave(Dialog As Object, URL As String, Optional Params As String, Optional FileName As String) As Boolean
  Dim xmlHttp As xmlHttp
  Set xmlHttp = HttpPost(URL, Params)
  
On Error GoTo Exception
  
  With Dialog
    .FileName = FileName
    .Filter = "All files (*.*)|*.*|Text files (*.txt)|*.txt"
    .DefaultExt = "txt"
    .flags = cdlOFNOverwritePrompt Or cdlOFNHideReadOnly Or cdlOFNPathMustExist
    .DialogTitle = "Save As"
    .CancelError = True
    .ShowSave
    FileName = .FileName
    WriteTextFile xmlHttp.responseText, FileName
    HttpSave = True
  End With

Exception:
  Err.Clear
End Function

