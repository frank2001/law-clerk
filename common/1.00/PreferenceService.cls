VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PreferenceService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'References: Micrsoft XML, v4.0
'References: Micrsoft Soap Type Library v3.0

Private mClient As SoapClient30
Private Const ServiceName = "FolderService"
Private mPreferenceDataset As Dataset
Private mEntryDataset As Dataset

'
' Initialize Web Service
'

Private Sub Class_Initialize()
  Set mEntryDataset = NewGenericDataset(DatabaseName.PREFERENCEENTRY_TABLE)
  Set mPreferenceDataset = NewGenericDataset(DatabaseName.PREFERENCE_TABLE)

  'Setup SOAP
On Error GoTo Exception
  Set mClient = New SoapClient30
  'ver  (jjm) 2005-08-11
  mClient.MSSoapInit HttpServer & WebComponent & ServiceName & ".jws?wsdl"  '".wsdl"
  mClient.ConnectorProperty("EndPointURL") = HttpServer & WebService & ServiceName & ".jws" & LawSession.Session
  Exit Sub
  
Exception:
  ErrorUtil.DisplaySoapError Err, mClient, ServiceName
End Sub


'
' Interface to server
'


Public Function CreateFolder( _
    Treename As String, _
    ParentFolderHandle As String, ChildFolderName As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String) As String

On Error GoTo Exception

  CreateFolder = mClient.Create( _
      Treename, ParentFolderHandle, ChildFolderName, _
      AttributeNames, AttributeValues)
  Exit Function

Exception:
  MsgBox "Folder Create Service Error: " & Err.Description, vbExclamation
End Function


'
' Path.
'
Public Function FindFolder(Treename As String, Path As String) As String

On Error GoTo Exception

  ' If no handle, return null back.
  If Path = vbNullString Then Exit Function

  ' Get path for handle.
  FindFolder = mClient.Find(Treename, Path)
  Exit Function

Exception:
  MsgBox "Folder GetPath Service Error: " & Err.Description, vbExclamation
End Function


'
' Path.
'
Public Function GetPath(Treename As String, FolderHandle As String) As String

On Error GoTo Exception

  ' If no handle, return null back.
  If FolderHandle = vbNullString Then Exit Function

  ' Get path for handle.
  GetPath = mClient.GetPath(Treename, FolderHandle)
  Exit Function

Exception:
  MsgBox "Folder GetPath Service Error: " & Err.Description, vbExclamation
End Function


'
' Move Folder
'
Public Function MoveFolder(FolderHandle As String, parentFolderId As String) As Boolean

On Error GoTo Exception

  Call mClient.Move(FolderHandle, parentFolderId)
  MoveFolder = True
  Exit Function

Exception:
  MsgBox "Folder Move Service Error: " & Err.Description, vbExclamation
End Function


'
' Rename Folder
'
Public Function RenameFolder(Treename As String, FolderHandle As String, FolderName As String) As Boolean

On Error GoTo Exception

  Call mClient.Rename(Treename, FolderHandle, FolderName)
  RenameFolder = True
  Exit Function

Exception:
  MsgBox "Folder Rename Service Error: " & Err.Description, vbExclamation
End Function


'
' Swap Folder Positions
'
Public Function SwapFolder(FolderHandle1 As String, FolderHandle2 As String) As Boolean

On Error GoTo Exception

  Call mClient.Swap(FolderHandle1, FolderHandle2)
  SwapFolder = True
  Exit Function

Exception:
  MsgBox "Folder Swap Service Error: " & Err.Description, vbExclamation
End Function

Public Function Sync(Treename As String) As Boolean
On Error GoTo Exception
  Dim pointer As New WaitPointer
  pointer.pointer = vbHourglass
  
  mClient.Sync Treename
  Sync = True
  Exit Function

Exception:
  MsgBox "Folder Sync Service Error: " & Err.Description, vbExclamation
End Function
  


'
' Write a fodler
'
Public Function WriteFolder( _
    Treename As String, _
    FolderHandle As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String) As String

On Error GoTo Exception

  WriteFolder = mClient.Write( _
      Treename, FolderHandle, _
      AttributeNames, AttributeValues)
  Exit Function

Exception:
  MsgBox "Folder Write Service Error: " & Err.Description, vbExclamation
End Function

'
' Delete an Folder
'
Public Function DeleteFolder( _
    Treename As String, FolderHandle As String) As Boolean

On Error GoTo Exception

  Call mClient.Delete(Treename, FolderHandle)

  DeleteFolder = True
  Exit Function

Exception:
  MsgBox "Folder Service Error: " & Err.Description, vbExclamation
End Function

Public Function getPreferenceEntryValue(preferenceId As String, entryId As String)
  mEntryDataset.DataProvider.Filter = LawDialect.EqualString(DatabaseName.PREFERENCE_KEY, preferenceId) & " and " & _
                                       LawDialect.EqualString(DatabaseName.ENTRYID_FIELD, entryId)
  mEntryDataset.DataProvider.Column = DatabaseName.ENTRYVALUE_FIELD
  mEntryDataset.DataProvider.OpenRecordset
  
  'exit if no data exits
  If mEntryDataset.DataProvider.IsEmpty Then
    getPreferenceEntryValue = vbNullString
    Exit Function
  End If
  
  getPreferenceEntryValue = GetDataFromCollection(mEntryDataset.DataProvider.Collection(1), _
                                                  DatabaseName.ENTRYVALUE_FIELD)
End Function

Public Function getNodeName(preferenceId As String) As String
  mPreferenceDataset.DataProvider.Filter = LawDialect.EqualString(DatabaseName.PREFERENCE_KEY, preferenceId)
  mPreferenceDataset.DataProvider.Column = DatabaseName.NODENAME_FIELD
  mPreferenceDataset.DataProvider.OpenRecordset
  
  'exit if no data exits
  If mPreferenceDataset.DataProvider.IsEmpty Then
    getNodeName = vbNullString
    Exit Function
  End If
  
  getNodeName = GetDataFromCollection(mPreferenceDataset.DataProvider.Collection(1), _
                                                  DatabaseName.NODENAME_FIELD)

End Function
