Attribute VB_Name = "CrytsalUtil"
Option Explicit

'Private Const DRIVER_NAME = "Oracle"
'Private Const DATABASE_NAME = ""

Public Function GetCrystalReport(ReportPath As String, ApplicationId As String, _
    ApplicationRatingID As String, _
    PaymentOptionsCode As String, _
    APR As Double _
    ) As CRAXDRT.Report
  
On Error GoTo ErrHandler
        
    Const PROC = "GetCrystalReport"
    
    Dim CR          As CRAXDRT.Application
    Dim CRParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim CRParamDef  As CRAXDRT.ParameterFieldDefinition
    Dim CRReport    As CRAXDRT.Report
    Dim DBTable     As CRAXDRT.DatabaseTable
    
    Dim CRXSection      As CRAXDRT.Section
    Dim CRXSubreport    As CRAXDRT.Report
    Dim CRXSubreportObj As CRAXDRT.SubreportObject
    Dim CRXReportObject As Object
    
    Dim sReportFilepath As String
    
    Dim driverName As String
    Dim databaseName As String
    Dim user As String
    Dim password As String
    driverName = IniFolder.ReadFile(UrlName.CRYSTALREPORTS_INI, UrlName.CRYSTAL_DRIVERNAME_INI)
    databaseName = IniFolder.ReadFile(UrlName.CRYSTALREPORTS_INI, UrlName.CRYSTAL_DATABASENAME_INI)
    user = IniFolder.ReadFile(UrlName.CRYSTALREPORTS_INI, UrlName.CRYSTAL_USER_INI)
    password = IniFolder.ReadFile(UrlName.CRYSTALREPORTS_INI, UrlName.CRYSTAL_PASSWORD_INI)
   
    Set CR = New CRAXDRT.Application
    
    sReportFilepath = ReportPath
    
    Set CRReport = CR.OpenReport(sReportFilepath, CROpenReportMethod.crOpenReportByTempCopy) ' do not open exclusively
      
    For Each DBTable In CRReport.Database.Tables
        'Debug.Print DBTable.Name
        DBTable.SetLogOnInfo driverName, databaseName, user, password
    Next
    
    CRReport.UseIndexForSpeed = True
    
    ' Cycle through each section in the main report.
    For Each CRXSection In CRReport.Sections

        'Get all the objects in the section.
        For Each CRXReportObject In CRXSection.ReportObjects

            'Test the objects to see if they're subreports.
            If CRXReportObject.Kind = crSubreportObject Then

                Set CRXSubreportObj = CRXReportObject
                Set CRXSubreport = CRXSubreportObj.OpenSubreport
                
                ' Set database connection for each subreport
                For Each DBTable In CRXSubreport.Database.Tables
                    'Debug.Print DBTable.Name
                    DBTable.SetLogOnInfo driverName, databaseName, user, password
                Next
                
            End If
        Next
    Next
    

    With CRReport.ParameterFields
    
        .GetItemByName("ApplicationID").SetCurrentValue ApplicationId
        .GetItemByName("ApplicationRatingID").SetCurrentValue ApplicationRatingID
        .GetItemByName("APR").SetCurrentValue APR
        .GetItemByName("PaymentOptionsCode").SetCurrentValue PaymentOptionsCode
 
    End With
      
    Set GetCrystalReport = CRReport
          
    Set CR = Nothing
    
    Exit Function
    
ErrHandler:
  MsgBox Err.Description

  Dim errorInfo As String
  errorInfo = errorInfo & "LastDllError: " & Err.LastDllError & vbCrLf
  errorInfo = errorInfo & "Error Source: " & Err.Source & vbCrLf & vbCrLf
  
'  If Not CRReport Is Nothing Then
'    errorInfo = errorInfo & " === CRReport ===================" & vbCrLf
'    errorInfo = errorInfo & "DriverName: " & CRReport.driverName & vbCrLf
'    errorInfo = errorInfo & "PrinterName: " & CRReport.PrinterName & vbCrLf
'    errorInfo = errorInfo & "SQLQueryString: " & CRReport.SQLQueryString & vbCrLf
'
''    If Not CRReport.Database Is Nothing Then
''      For Each DBTable In CRReport.Database.Tables
''        If Not DBTable Is Nothing Then
''          errorInfo = errorInfo & " === DBTable ====================" & vbCrLf
''          errorInfo = errorInfo & "DatabaseType: " & DBTable.DatabaseType & vbCrLf
''          errorInfo = errorInfo & "Name: " & DBTable.Name & vbCrLf
''          Dim tempErrorProperty As CRAXDRT.ConnectionProperty
''          For Each tempErrorProperty In DBTable.ConnectionProperties
''            errorInfo = errorInfo & "ConnectionProperties." & tempErrorProperty.Name & ": " & tempErrorProperty.Value & vbCrLf
''          Next
''        End If
''      Next
''    End If
'  End If
    
  
  ErrorUtil.DisplayMessage "Error: " & Err.Number & " " & Err.Description, errorInfo, "Crystal Report Error" & LAWVersion, False
  Err.Clear
End Function

