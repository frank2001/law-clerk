Attribute VB_Name = "Util"
Option Explicit

'
' This module contains generic utilities that are based
' solely on the visual basic language and not on any
' custom objects or controls.
'

Const URL_UNSAFE = """,<>%\^[]`;/?:@=&#"
Const DIR_UNSAFE = "\/:*?""<>|&"


' Used internally by EncordURL
Private Function IsUnsafe(compareChar) As Boolean
  IsUnsafe = (InStr(URL_UNSAFE, compareChar) Or Asc(compareChar) < 33 Or Asc(compareChar) > 122)
End Function

' Encode a url.
Public Function EncodeURL(url As String) As String
  Dim I As Long, inC As String, outC As String
  For I = 1 To Len(url)
    inC = Mid(url, I, 1)
    If IsUnsafe(inC) Then
       EncodeURL = EncodeURL & "%" & Right("00" + Hex(Asc(inC)), 2)
    Else
       EncodeURL = EncodeURL & inC
    End If
  Next I
End Function


' Encode a url.
Public Function DirNameSafe(Name As String) As Boolean
  Dim I As Long, Length As Integer
  Length = Len(Name)
  For I = 1 To Length
    If InStr(DIR_UNSAFE, Mid(Name, I, 1)) Then
      MsgBox "A directory name cannot contain any of the following characters: " & DIR_UNSAFE, vbCritical
      Exit Function
    End If
  Next I
  DirNameSafe = True
End Function


'
' Return maximimum integers.
'
Public Function Max(Num1 As Long, Num2 As Long) As Long
  Max = IIf(Num1 > Num2, Num1, Num2)
End Function

'
' Return minimum integers.
'
Public Function Min(Num1 As Integer, Num2 As Integer) As Integer
  Min = IIf(Num1 < Num2, Num1, Num2)
End Function

'
' See if a collection contains the index given
'
Public Function Contains(collection As Variant, Index As Variant) As Boolean
On Error GoTo Exception
  IsObject collection(Index)
  Contains = True
  Exit Function
Exception:
  Err.Clear
End Function


Public Function ArrayContains(SourceArray, Value) As Boolean
On Error GoTo Exception
  Dim item As Variant
  For Each item In SourceArray
    If item = Value Then
      ArrayContains = True
      Exit Function
    End If
  Next item
  Exit Function
Exception:
  Err.Clear
End Function


'
' See if a collection contains the index given
'
Public Function SafeIndex(collection As Variant, Index As Variant) As Variant
On Error Resume Next
  SafeIndex = collection(Index)
End Function

'
' See if a collection contains the index given
'
Public Function ArrayFind(Ary As Variant, Value As Variant) As Integer
On Error GoTo Exception
  Dim LB, UB, I As Integer
  UB = SafeUBound(Ary)
  If UB = -1 Then
    ArrayFind = -1
    Exit Function
  End If
  LB = LBound(Ary)
  For I = LB To UB
    If Ary(I) = Value Then
      ArrayFind = I
      Exit Function
    End If
  Next I
Exception:
  ArrayFind = -1
  Err.Clear
End Function

'
' Check for bounds on an array.  Return -1 if no array.
'
Public Function SafeUBound(Ary As Variant) As Integer
On Error GoTo Exception
  SafeUBound = UBound(Ary)
  Exit Function
Exception:
  Err.Clear
  SafeUBound = -1
End Function

Public Function SafeLBound(Ary As Variant) As Integer
On Error GoTo Exception
  SafeLBound = LBound(Ary)
  Exit Function
Exception:
  Err.Clear
  SafeLBound = -1
End Function

'
' Find index in a two dimension collection.
'
Public Function CollectionFind(Items As collection, Index As Variant, Value As Variant) As Integer
  If Items Is Nothing Then Exit Function
  Dim I, Count As Integer
  Count = Items.Count
  For I = 1 To Count
    If Items(I)(Index) = Value Then
      CollectionFind = I
      Exit For
    End If
  Next I
End Function

' Function used to determine if string boolean data from the database is
' true of false. In the database true boolean data is stored as a
' "Yes" string value.
'
Public Function GetBoolean(BoolVariant As Variant) As Boolean
  On Error Resume Next
  GetBoolean = BoolVariant = DB_TRUE_VALUE Or BoolVariant = "Y"
End Function

Public Function GetDate(DateVariant As Variant) As Date
  On Error Resume Next
  GetDate = CDate(DateVariant)
End Function

Public Function GetDouble(DoubleVariant As Variant) As Double
On Error GoTo Exception
  GetDouble = CDbl(DoubleVariant)
  Exit Function
Exception:
  GetDouble = 0
  Err.Clear
End Function

Public Function GetInteger(IntegerVariant As Variant, Optional Default As Integer) As Integer
On Error GoTo Exception
  GetInteger = CInt(IntegerVariant)
  Exit Function
Exception:
  GetInteger = Default
  Err.Clear
End Function


Public Function GetLong(LongVariant As Variant) As Long
On Error GoTo Exception
  GetLong = CLng(LongVariant)
  Exit Function
Exception:
  GetLong = 0
  Err.Clear
End Function


Public Function GetMask(MaskVariant As Variant, Mask As String) As String
On Error GoTo Exception
  Dim Value As String
  Value = MaskVariant
  If Value = Mask Then
    GetMask = vbNullString
  Else
    GetMask = Value
  End If
  Exit Function
Exception:
  Err.Clear
End Function

Public Function GetRealPath(Path As String) As String
  GetRealPath = IIf(Left(Path, 1) = "\" Or InStr(Path, ":"), _
      Path, App.Path & IIf(Path = "", "", "\" & Path))
End Function

'Function to safely get data from a collection when the index may not exist
Public Function GetDataFromCollection(Col As collection, Key As String, Optional DebugMsg As Boolean = False) As Variant
  'vb6 doesn't short circut so there is a need for two if statements
  If Col Is Nothing Then
    GetDataFromCollection = vbNullString
    If DebugMsg Then
      MsgBox "Data Collection is empty. Looking for index: " & Key
    End If
    
    Exit Function
  End If
  
  If Contains(Col, Key) Then
    GetDataFromCollection = Col(Key)
  Else
    If DebugMsg Then
      MsgBox "Index not found in Collection: " & Key
    End If
    GetDataFromCollection = vbNullString
  End If
End Function

'Function to set the value of a combo box
Public Sub SetComboValue(cbx As ComboBox, Value As Variant)
  With cbx
     If Value = vbNullString Then
       If .ListCount > 0 Then
         .ListIndex = 0
       End If
     Else
       Dim Text As String
       Text = Format(Value, .DataFormat.Format)
       If .Style = ComboBoxConstants.vbComboDropdownList Then
         Dim I As Integer
         For I = .ListCount - 1 To 0 Step -1
           If .List(I) = Text Then
             .ListIndex = I
             Exit For
           End If
         Next I
       Else
         .Text = Text
       End If
     End If
   End With
End Sub

'Simple bubble sort on a String collection
Public Sub BubbleSortStringCollection(collection As collection)
  'no collection, nothing to do
  If collection Is Nothing Then Exit Sub
  'a min of 2 is required to sort. This has it's own if statement as VB6 does not short circuit
  If collection.Count < 2 Then Exit Sub
  


  Dim I As Integer
  Dim pos As Integer
  Dim prior As Integer
  Dim tmpString As String
  Dim isSwapped As Boolean
  
  For I = 1 To collection.Count
    isSwapped = False
    
    For pos = 2 To collection.Count - I + 1
      prior = pos - 1
      If StrComp(collection.item(pos), collection.item(prior), vbTextCompare) < 0 Then
        tmpString = collection.item(prior)
        'remove the prior position.
        collection.Remove prior
        'The current position content bubbles up to the priors spot as it is now empty.
        'Reinsert the prior value after it's old position
        collection.Add tmpString, tmpString, , prior
        isSwapped = True
      End If
      
    Next pos
    
    If Not isSwapped Then
      Exit For
    End If
    
   Next I

End Sub



