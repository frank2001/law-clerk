Attribute VB_Name = "ShellExecuteModule"
Option Explicit

Declare Function ShellExecute Lib _
     "shell32.dll" Alias "ShellExecuteA" _
     (ByVal hWnd As Long, ByVal lpOperation _
     As String, ByVal lpFile As String, ByVal _
     lpParameters As String, ByVal lpDirectory _
     As String, ByVal nShowCmd As Long) As Long

Global Const SW_SHOWNORMAL = &H1

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Copyright �1996-2002 VBnet, Randy Birch, All Rights Reserved.
' Some pages may also contain other copyrights by the author.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Distribution: You can freely use this code in your own
'               applications, but you may not reproduce
'               or publish this code on any web site,
'               online service, or distribute as source
'               on any media without express permission.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Const CREATE_NEW_CONSOLE As Long = &H10
Public Const NORMAL_PRIORITY_CLASS As Long = &H20
Public Const INFINITE As Long = -1
Public Const STARTF_USESHOWWINDOW As Long = &H1

Public Const MAX_PATH As Long = 260
Public Const ERROR_FILE_NO_ASSOCIATION As Long = 31
Public Const ERROR_FILE_NOT_FOUND As Long = 2
Public Const ERROR_PATH_NOT_FOUND As Long = 3
Public Const ERROR_FILE_SUCCESS As Long = 32 'my constant
Public Const ERROR_BAD_FORMAT As Long = 11

Public Type STARTUPINFO
  cb As Long
  lpReserved As String
  lpDesktop As String
  lpTitle As String
  dwX As Long
  dwY As Long
  dwXSize As Long
  dwYSize As Long
  dwXCountChars As Long
  dwYCountChars As Long
  dwFillAttribute As Long
  dwFlags As Long
  wShowWindow As Integer
  cbReserved2 As Integer
  lpReserved2 As Long
  hStdInput As Long
  hStdOutput As Long
  hStdError As Long
End Type

Public Type PROCESS_INFORMATION
  hProcess As Long
  hThread As Long
  dwProcessId As Long
  dwThreadID As Long
End Type

Public Declare Function CreateProcess Lib "kernel32" _
   Alias "CreateProcessA" _
  (ByVal lpAppName As String, _
   ByVal lpCommandLine As String, _
   ByVal lpProcessAttributes As Long, _
   ByVal lpThreadAttributes As Long, _
   ByVal bInheritHandles As Long, _
   ByVal dwCreationFlags As Long, _
   ByVal lpEnvironment As Long, _
   ByVal lpCurrentDirectory As Long, _
   lpStartupInfo As STARTUPINFO, _
   lpProcessInformation As PROCESS_INFORMATION) As Long
     
Public Declare Function CloseHandle Lib "kernel32" _
  (ByVal hObject As Long) As Long

Public Declare Function FindExecutable Lib "shell32" _
   Alias "FindExecutableA" _
  (ByVal lpFile As String, _
   ByVal lpDirectory As String, _
   ByVal sResult As String) As Long

Public Declare Function GetTempPath Lib "kernel32" _
   Alias "GetTempPathA" _
  (ByVal nSize As Long, _
   ByVal lpBuffer As String) As Long
'--end block--'
 
 


Private Function GetBrowserName(dwFlagReturned As Long) As String

  'find the full path and name of the user's
  'associated browser
   Dim hFile As Long
   Dim sResult As String
   Dim sTempFolder As String
        
  'get the user's temp folder
   sTempFolder = GetTempDir()
   
  'create a dummy html file in the temp dir
   hFile = FreeFile
      Open sTempFolder & "dummy.html" For Output As #hFile
   Close #hFile

  'get the file path & name associated with the file
   sResult = Space$(MAX_PATH)
   dwFlagReturned = FindExecutable("dummy.html", sTempFolder, sResult)
  
  'clean up
   Kill sTempFolder & "dummy.html"
   
  'return result
   GetBrowserName = TrimNull(sResult)
   
End Function


Private Function TrimNull(item As String)

  'remove string before the terminating null(s)
   Dim Pos As Integer
   
   Pos = InStr(item, Chr$(0))
   
   If Pos Then
         TrimNull = Left$(item, Pos - 1)
   Else: TrimNull = item
   End If
   
End Function


Public Function GetTempDir() As String

  'retrieve the user's system temp folder
   Dim tmp As String
   
   tmp = Space$(256)
   Call GetTempPath(Len(tmp), tmp)
   
   GetTempDir = TrimNull(tmp)
    
End Function

Public Sub Browse(URL As String, Optional UseSameWindow As Boolean)
'   If UseSameWindow Then
    ShellExecute 0&, vbNullString, URL, vbNullString, _
        vbNullString, SW_SHOWNORMAL
'   End If
'
'   'Code for opening a browser in its own window
'
'   Dim success As Long
'   Dim hProcess As Long
'   Dim sBrowser As String
'   Dim Start As STARTUPINFO
'   Dim PROC As PROCESS_INFORMATION
'
'   sBrowser = GetBrowserName(success)
'
'  'did sBrowser get correctly filled?
'   If success >= ERROR_FILE_SUCCESS Then
'
'     'prepare STARTUPINFO members
'      With Start
'         .cb = Len(Start)
'         .dwFlags = STARTF_USESHOWWINDOW
'         .wShowWindow = SW_SHOWNORMAL
'      End With
'
'     'start a new instance of the default
'     'browser at the specified URL. The
'     'lpCommandLine member (second parameter)
'     'requires a leading space or the call
'     'will fail to open the specified page.
'      Call CreateProcess(sBrowser, _
'                              " " & URL, _
'                              0&, 0&, 0&, _
'                              NORMAL_PRIORITY_CLASS, _
'                              0&, 0&, Start, PROC)
'
'     'don't need the process
'     'handle anymore, so close it
'      Call CloseHandle(PROC.hProcess)
'
'     'and close the handle to the thread created
'      Call CloseHandle(PROC.hThread)
'   End If
End Sub

