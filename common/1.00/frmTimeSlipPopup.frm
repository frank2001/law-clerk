VERSION 5.00
Begin VB.Form frmTimeSlipPopup 
   Caption         =   "Form1"
   ClientHeight    =   2472
   ClientLeft      =   156
   ClientTop       =   696
   ClientWidth     =   3696
   LinkTopic       =   "Form1"
   ScaleHeight     =   2472
   ScaleWidth      =   3696
   StartUpPosition =   3  'Windows Default
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Edit"
         Index           =   1
      End
   End
End
Attribute VB_Name = "frmTimeSlipPopup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mFormMgr As Object
Private mView As Object
Private mPlugIn As TimeSlipPlugIn

Public Property Set PlugIn(vData As Object)
  Set mPlugIn = vData
End Property

Public Sub Init(FormMgr As Object, View As Object)
  Set mFormMgr = FormMgr
  Set mView = View
End Sub

Public Sub mnuOpen_Click(Index As Integer)
  mPlugIn.ShowByListView mFormMgr, mView
End Sub
