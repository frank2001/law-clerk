VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Sequence"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mIndex As Integer
Private mTop As Integer
Private mBottom As Integer

Private mList() As Variant
Private mSize As Integer


Public Sub AddItem(Item, Optional Index)
  If mSize = 0 Then
    Err.Raise 1, "Item", "Must set sequence size first."
  End If
  
  Dim CurrentIndex As Integer
  If IsMissing(Index) Then
    CurrentIndex = ArrayIndex(mIndex)
  Else
    CurrentIndex = ArrayIndex(CInt(Index))
  End If
  
  If CurrentIndex = mTop Then
    If (mTop + 1) Mod mSize = mBottom Then
      mBottom = (mBottom + 1) Mod mSize
    End If
  Else
    ' Clear all items until the top.
    Dim I As Integer
    I = CurrentIndex
    While I <> mTop
      I = (I + 1) Mod mSize
      mList(I) = vbEmpty
    Wend
  End If
  
  CurrentIndex = (CurrentIndex + 1) Mod mSize
  mTop = CurrentIndex
  
  If IsObject(Item) Then
    Set mList(CurrentIndex) = Item
  Else
    mList(CurrentIndex) = Item
  End If
  
  mIndex = 0
End Sub


Public Sub Clear()
  Dim I As Integer
  For I = 0 To mSize - 1
    mList(I) = vbEmpty
  Next I
  mIndex = -1
  mTop = 0
  mBottom = 0
End Sub


Public Property Get List(Index As Integer) As Variant
  If mSize = 0 Then
    Err.Raise 1, "Item", "Sequence is empty."
  End If
  
  Dim StackIndex As Integer
  StackIndex = ArrayIndex(CInt(Index))
  If IsObject(mList(StackIndex)) Then
    Set List = mList(StackIndex)
  Else
    List = mList(StackIndex)
  End If
End Property


Public Property Get ListCount() As Integer
  If mIndex < 0 Then
    ListCount = 0
    Exit Sub
  End If
  
  Dim Count As Integer
  Count = mTop - mBottom
  If Count < 0 Then
    Count = mSize + Count
  End If
  ListCount = Count + 1
End Property


Public Property Get ListIndex() As Integer
  ListIndex = mIndex
End Property


Public Property Let ListIndex(vData As Integer)
  Dim Count As Integer
  Count = ListCount
  If vData < 0 Or vData > Count - 1 Then
    Err.Raise 1, "item", "Index " & vData & " is out of range.  Must be between 0 and " & Count - 1
  End If
  mIndex = vData
End Property


Public Property Get Selected() As Variant
  If mIndex > -1 Then
    Selected = List(mIndex)
  End If
End Property


Public Property Let Size(ByVal vData As Integer)
  mSize = vData
  ReDim mList(mSize - 1)
  mIndex = -1
  mTop = 0
  mBottom = 0
End Property


Public Property Get Size() As Integer
  Size = mSize
End Property


Private Function ArrayIndex(RelativeIndex As Integer) As Integer
  If RelativeIndex < 0 Then
    ArrayIndex = -1
    Exit Function
  End If
  
  Dim Index As Integer
  Index = mTop - RelativeIndex
  If Index < 0 Then
    Index = mSize + Index
  End If
  ArrayIndex = Index
End Function

