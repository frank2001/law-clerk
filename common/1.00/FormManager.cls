VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FormManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Description = "Form Interface Collection"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"FormInterface"
Attribute VB_Ext_KEY = "Member0" ,"FormInterface"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member1" ,"InnerClass"
Option Explicit

'local variable to hold collection
Private mCol As Collection
Private mForm As Form

Private mParentInterface As FormInterface


Public Function Add(ChildForm As Object, Optional Key As String) As FormInterface
  'Test for existing form with the same key
  If Contains(mCol, Key) Then
    MsgBox "Form Manager Error: A window is already open with this key."
    Exit Function
  End If

  'create a new object
  Dim objNewMember As New FormInterface
  objNewMember.Load Me, mForm, ChildForm, Key
  
  If Len(Key) = 0 Then
    mCol.Add objNewMember
  Else
    mCol.Add objNewMember, Key
  End If
    
  'return the object created
  Set Add = objNewMember
End Function

'
' Get Item.  If item not found, returns nothing.
'
Public Property Get item(vntIndexKey As Variant) As FormInterface
Attribute item.VB_UserMemId = 0
On Error Resume Next
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set item = mCol(vntIndexKey)
End Property


Public Sub Load(MasterForm As Object)
  Set mForm = MasterForm
End Sub

'
' Signal to the parent that the child is unloading and go through each
' child and signal that the parent is unloading.
'
Public Sub Unload()
  Set mForm = Nothing
  If Not mParentInterface Is Nothing Then
    mParentInterface.ChildUnload
  End If
  Dim fInterface As Variant
  For Each fInterface In mCol
    fInterface.ParentUnload
  Next fInterface
End Sub


Public Function ContainsKey(Key As String) As Boolean
  ContainsKey = Not item(Key) Is Nothing
End Function


Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)

    mCol.Remove vntIndexKey
End Sub


Public Sub RemoveItem(item As FormInterface)
  Dim I As Integer
  For I = 1 To mCol.Count
    If mCol(I) Is item Then
      Remove I
      Exit Sub
    End If
  Next I
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Public Property Set ParentInterface(vData As FormInterface)
  Set mParentInterface = vData
End Property

Public Property Get Parent() As Object
  If Not mParentInterface Is Nothing Then
    Set Parent = mParentInterface.Parent
  End If
End Property

Public Property Let Message(vData As Integer)
  If Not mParentInterface Is Nothing Then
    mParentInterface.Message = vData
  End If
End Property

Private Sub Class_Initialize()
  'creates the collection when this class is created
  Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
  'destroys collection when this class is terminated
  Set mCol = Nothing
End Sub
