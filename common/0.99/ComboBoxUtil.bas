Attribute VB_Name = "ComboBoxUtil"
Option Explicit

'Uses Module MessageUtil
'Uses Module TypeUtil
'Uses Module Util
'Uses Class KeyItem

Public Declare Function MoveWindow Lib "user32" _
  (ByVal hWnd As Long, _
   ByVal X As Long, ByVal Y As Long, _
   ByVal nWidth As Long, _
   ByVal nHeight As Long, _
   ByVal bRepaint As Long) As Long

Public Declare Function GetWindowRect Lib "user32" _
  (ByVal hWnd As Long, _
   lpRect As RECT) As Long

Public Declare Function ScreenToClient Lib "user32" _
  (ByVal hWnd As Long, _
   lpPoint As POINTAPI) As Long

Public Const CB_SHOWDROPDOWN = &H14F
Public Const CB_GETITEMHEIGHT = &H154
Public Const CB_GETDROPPEDSTATE = &H157



Private Sub AddItem(Combo As ComboBox, Text As String, First As Boolean)
  Dim SelStart, SelLength As Integer
  SelStart = Combo.SelStart
  SelLength = Combo.SelLength
  If First Then
    Combo.AddItem Text, 0
    Combo.ListIndex = 0
  Else
    Combo.AddItem Text
    Combo.ListIndex = Combo.ListCount - 1
  End If
  Combo.SelStart = SelStart
  Combo.SelLength = SelLength
End Sub


' Add current text of Combo to list.  Optionally place the text at the top of the
' list.  Otherwise added to end.
Public Sub AddText(Combo As ComboBox, Optional First As Boolean, Optional Limit As Integer)
  Dim I As Integer
  
  For I = 0 To Combo.ListCount - 1
    If (Combo.List(I) = Combo.Text) Then
      AddItem Combo, Combo.Text, First
      Combo.RemoveItem I + IIf(First, 1, 0)
      Exit Sub
    End If
  Next I
  
  AddItem Combo, Combo.Text, First
  
  If Limit > 0 Then
    While Combo.ListCount > Limit
      If First Then
        Combo.RemoveItem Combo.ListCount - 1
      Else
        Combo.RemoveItem 0
      End If
    Wend
  End If
End Sub


Private Function NewKeyItem(Key As Variant, Item As Variant) As KeyItem
  Dim KeyItem As New KeyItem
  If IsObject(Key) Then
    Set KeyItem.Key = Key
  Else
    KeyItem.Key = Key
  End If
  If IsObject(Item) Then
    Set KeyItem.Item = Item
  Else
    KeyItem.Item = Item
  End If
  Set NewKeyItem = KeyItem
End Function


Public Sub Deserialize(Combo As ComboBox, Section As String, Ini As IniFile, Optional Cache As Collection)
  Combo.Clear
  
  Dim List As Collection
  If Not Cache Is Nothing Then
    If Contains(Cache, Section) Then
      DeserializeCache Combo, Cache(Section).Item
      Exit Sub
    End If
    Set List = New Collection
  End If
  
  Dim I As Integer, Index As String, Item As String
  For I = 0 To 1000
    Index = "list." & I & ".item"
    Item = Ini.ReadFile(Section, Index)
    If (Item = "") Then
      Exit For
    End If
    Combo.AddItem Item
    If Not List Is Nothing Then
      List.Add NewKeyItem(Index, Item), Index
    End If
  Next I
  
  ' If anything existed in ini
  If I > 0 Then
    Combo.ListIndex = Ini.ReadFile(Section, "listindex")
    If Not List Is Nothing Then
      List.Add NewKeyItem("listindex", Combo.ListIndex), "listindex"
      Cache.Add NewKeyItem(Section, List), Section
    End If
  End If
End Sub


Public Sub Serialize(Combo As ComboBox, Section As String, Optional Ini As IniFile, Optional Cache As Collection)
  If Not Cache Is Nothing Then
    If Contains(Cache, Section) Then
      Cache.Remove Section
    End If
    Dim List As New Collection
    Cache.Add NewKeyItem(Section, List), Section
    SerializeCache Combo, List
    Exit Sub
  End If
  
  Ini.RemoveSection Section
  Ini.WriteFile Section, "listindex", Combo.ListIndex
  Dim I As Integer
  For I = 0 To Combo.ListCount - 1
    Ini.WriteFile Section, "list." & I & ".item", Combo.List(I)
  Next I
End Sub


Private Sub DeserializeCache(Combo As ComboBox, List As Collection)
  Dim Item As Variant, ListIndex As Integer
  ListIndex = -1
  For Each Item In List
    If Item.Key = "listindex" Then
      ListIndex = Item.Item
    Else
      Combo.AddItem Item.Item
    End If
  Next Item
  
  ' Restore ListIndex last so that all list items will be there
  Combo.ListIndex = ListIndex
End Sub


Private Sub SerializeCache(Combo As ComboBox, List As Collection)
  List.Add NewKeyItem("listindex", Combo.ListIndex), "listindex"
  Dim I As Integer, Index As String
  For I = 0 To Combo.ListCount - 1
    Index = "list." & I & ".item"
    List.Add NewKeyItem(Index, Combo.List(I)), Index
  Next I
End Sub


Public Sub Flush(Ini As IniFile, Cache As Collection)
  Dim List As Variant, Section As String, Item As Variant
  For Each List In Cache
    Section = List.Key
    Ini.RemoveSection Section
    For Each Item In List.Item
      Ini.WriteFile Section, Item.Key, Item.Item
    Next Item
  Next List
End Sub


Public Function IsDropped(Combo As ComboBox) As Boolean
  IsDropped = SendMessage(Combo.hWnd, CB_GETDROPPEDSTATE, 0, 0)
End Function


Public Sub SetDropDownHeight(Container As Object, Combo As ComboBox, ItemCount As Integer)
   Dim NewHeight As Long
   
  'Calculate the new height of the combo Combo. This
  'is the number of items times the item height
  'plus two.
   NewHeight = SendMessage(Combo.hWnd, CB_GETITEMHEIGHT, 0, ByVal 0) * (ItemCount + 2)
   
  'Using the values returned and set above,
  'call MoveWindow to reposition the combo Combo
   Call MoveWindow(Combo.hWnd, _
       Container.ScaleX(Combo.Left, Container.ScaleMode, vbPixels), _
       Container.ScaleY(Combo.Top, Container.ScaleMode, vbPixels), _
       Container.ScaleX(Combo.Width, Container.ScaleMode, vbPixels), _
       NewHeight, True)
End Sub
