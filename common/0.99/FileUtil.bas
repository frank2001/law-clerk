Attribute VB_Name = "FileUtil"
Option Explicit


Public Function IsFile(FileName As String) As Boolean
On Error GoTo Exception
  
  FileSystem.FileDateTime FileName
  IsFile = True
  Exit Function
  
Exception:
  Err.Clear
End Function


Public Function ReadTextFile(FileName As String) As String
On Error GoTo Exception
  Dim fNum As Integer, isOpen As Boolean
  fNum = FreeFile()
  Open FileName For Input As #fNum
  isOpen = True
  
  Dim Line As String
  While Not EOF(fNum)
    Input #fNum, Line
    ReadTextFile = ReadTextFile & IIf(ReadTextFile = vbNullString, "", vbNewLine) & Line
  Wend
  
Exception:
  If isOpen Then Close #fNum
  If Err Then Err.Raise Err.Number, , Err.Description
End Function


Public Sub WriteTextFile(Text As String, FileName As String, Optional AppendMode As Boolean)
On Error GoTo Exception
  Dim fNum As Integer, isOpen As Boolean
  fNum = FreeFile()
  If AppendMode Then
    Open FileName For Append As #fNum
  Else
    Open FileName For Output As #fNum
  End If
  isOpen = True
  Print #fNum, Text
Exception:
  If isOpen Then Close #fNum
  If Err Then Err.Raise Err.Number, , Err.Description
End Sub
