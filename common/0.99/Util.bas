Attribute VB_Name = "Util"
Option Explicit

'
' This module contains generic utilities that are based
' solely on the visual basic language and not on any
' custom objects or controls.
'

Const URL_UNSAFE = """,<>%\^[]`;/?:@=&#"
Const DIR_UNSAFE = "\/:*?""<>|&"


' Used internally by EncordURL
Private Function IsUnsafe(compareChar) As Boolean
  IsUnsafe = (InStr(URL_UNSAFE, compareChar) Or Asc(compareChar) < 33 Or Asc(compareChar) > 122)
End Function

' Encode a url.
Public Function EncodeURL(URL As String) As String
  Dim I As Long, inC As String, outC As String
  For I = 1 To Len(URL)
    inC = Mid(URL, I, 1)
    If IsUnsafe(inC) Then
       EncodeURL = EncodeURL & "%" & Right("00" + Hex(Asc(inC)), 2)
    Else
       EncodeURL = EncodeURL & inC
    End If
  Next I
End Function


' Encode a url.
Public Function DirNameSafe(Name As String) As Boolean
  Dim I As Long, Length As Integer
  Length = Len(Name)
  For I = 1 To Length
    If InStr(DIR_UNSAFE, Mid(Name, I, 1)) Then
      MsgBox "A directory name cannot contain any of the following characters: " & DIR_UNSAFE, vbCritical
      Exit Function
    End If
  Next I
  DirNameSafe = True
End Function


'
' Return maximimum integers.
'
Public Function Max(Num1 As Integer, Num2 As Integer) As Integer
  Max = IIf(Num1 > Num2, Num1, Num2)
End Function

'
' Return minimum integers.
'
Public Function Min(Num1 As Integer, Num2 As Integer) As Integer
  Min = IIf(Num1 < Num2, Num1, Num2)
End Function

'
' See if a collection contains the index given
'
Public Function Contains(Collection As Variant, Index As Variant) As Boolean
On Error GoTo Exception
  IsObject Collection(Index)
  Contains = True
  Exit Function
Exception:
  Err.Clear
End Function


Public Function ArrayContains(SourceArray, Value) As Boolean
On Error GoTo Exception
  Dim Item As Variant
  For Each Item In SourceArray
    If Item = Value Then
      ArrayContains = True
      Exit Function
    End If
  Next Item
  Exit Function
Exception:
  Err.Clear
End Function


'
' See if a collection contains the index given
'
Public Function SafeIndex(Collection As Variant, Index As Variant) As Variant
On Error Resume Next
  SafeIndex = Collection(Index)
End Function

'
' See if a collection contains the index given
'
Public Function ArrayFind(Ary As Variant, Value As Variant) As Integer
On Error GoTo Exception
  Dim LB, UB, I As Integer
  UB = SafeUBound(Ary)
  If UB = -1 Then
    ArrayFind = -1
    Exit Function
  End If
  LB = LBound(Ary)
  For I = LB To UB
    If Ary(I) = Value Then
      ArrayFind = I
      Exit Function
    End If
  Next I
Exception:
  ArrayFind = -1
  Err.Clear
End Function

'
' Check four bounds on an array.  Return -1 if no array.
'
Public Function SafeUBound(Ary As Variant) As Integer
On Error GoTo Exception
  SafeUBound = UBound(Ary)
  Exit Function
Exception:
  Err.Clear
  SafeUBound = -1
End Function

'
' Find index in a two dimension collection.
'
Public Function CollectionFind(Items As Collection, Index As Variant, Value As Variant) As Integer
  If Items Is Nothing Then Exit Function
  Dim I, Count As Integer
  Count = Items.Count
  For I = 1 To Count
    If Items(I)(Index) = Value Then
      CollectionFind = I
      Exit For
    End If
  Next I
End Function

Public Function GetBoolean(BoolVariant As Variant) As Date
  On Error Resume Next
  GetBoolean = BoolVariant = "Yes" Or BoolVariant = "Y"
End Function

Public Function GetDate(DateVariant As Variant) As Date
  On Error Resume Next
  GetDate = CDate(DateVariant)
End Function

Public Function GetDouble(DoubleVariant As Variant) As Double
On Error GoTo Exception
  GetDouble = CDbl(DoubleVariant)
  Exit Function
Exception:
  GetDouble = 0
  Err.Clear
End Function

Public Function GetInteger(IntegerVariant As Variant, Optional Default As Integer) As Integer
On Error GoTo Exception
  GetInteger = CDbl(IntegerVariant)
  Exit Function
Exception:
  GetInteger = Default
  Err.Clear
End Function

Public Function GetMask(MaskVariant As Variant, Mask As String) As String
On Error GoTo Exception
  Dim Value As String
  Value = MaskVariant
  If Value = Mask Then
    GetMask = vbNullString
  Else
    GetMask = Value
  End If
  Exit Function
Exception:
  Err.Clear
End Function

Public Function GetRealPath(Path As String) As String
  GetRealPath = IIf(Left(Path, 1) = "\" Or InStr(Path, ":"), _
      Path, App.Path & IIf(Path = "", "", "\" & Path))
End Function

