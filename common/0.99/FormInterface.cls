VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FormInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mManager As FormManager
Private mParentForm As Form
Private mChildForm As Form
Private mKey As String
Private mMessage As Integer



'
' FormInterface is loaded exclusively through a FormManager.
'
' FormInterface defines the relationship between a parent form and
' a child form.  The interface is loaded before the child forms Load
' method is called.  The interface is unloaded when either the parent
' or the child form is unloaded.
'
' Parent forms that use this interface must implement the
' FormChildUnload method. Child forms that use this interface
' must support FormInterfaceLoad and FormParentUnload methods.
'
' Example Methods:
'
'
Public Sub Class_Initialize()
End Sub

'
' Load Interface.  Signal the Child that the form is loaded
'
Public Sub Load(Manager As FormManager, Parent As Object, Child As Object, Optional Key As String)
  Set mManager = Manager
  Set mParentForm = Parent
  Set mChildForm = Child
  mKey = Key
  
  ' Load the child form.
  mChildForm.FormInterfaceLoad Me
End Sub

'
' Unload the interface.  Remove references to forms so that the
' forms can terminate.
'
Private Sub Unload()
  mManager.RemoveItem Me
  Set mParentForm = Nothing
  Set mChildForm = Nothing
End Sub

'
' Return the parent form in the interface
'
Public Property Get Parent() As Object
  Set Parent = mParentForm
End Property

'
' Signal to child form that the parent form is
' being unloaded.
'
Public Sub ParentUnload()
  If (Not mChildForm Is Nothing) Then
    mChildForm.FormParentUnload
  End If
  Unload
End Sub

'
' Return the child form in the interface.
'
Public Property Get Child() As Object
  Set Child = mChildForm
End Property

'
' Unload teh child form.  Signal the parent form that the child
' form is about to be unloaded.
'
Public Sub ChildUnload()
  If (Not mParentForm Is Nothing) Then
    mParentForm.FormChildUnload Me
  End If
  Unload
End Sub

'
' Return the Key
'
Public Property Get Key() As String
  Key = mKey
End Property

'
' Message
'
Public Property Let Message(vData As Integer)
  mMessage = vData
End Property

Public Property Get Message() As Integer
  Message = mMessage
End Property
