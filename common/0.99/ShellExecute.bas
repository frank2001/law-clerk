Attribute VB_Name = "ShellExecuteModule"
Option Explicit

Declare Function ShellExecute Lib _
     "shell32.dll" Alias "ShellExecuteA" _
     (ByVal hWnd As Long, ByVal lpOperation _
     As String, ByVal lpFile As String, ByVal _
     lpParameters As String, ByVal lpDirectory _
     As String, ByVal nShowCmd As Long) As Long

Global Const SW_SHOWNORMAL = &H1


Public Sub Browse(URL As String)
  ShellExecute 0&, vbNullString, URL, vbNullString, _
      vbNullString, SW_SHOWNORMAL
End Sub



