Attribute VB_Name = "CrytsalUtil"
Option Explicit

Public Function GetCrystalReport(ReportPath As String, ApplicationID As String, _
    ApplicationRatingID As String, _
    PaymentOptionsCode As String, _
    APR As Double _
    ) As CRAXDRT.Report
  
    'On Error GoTo ErrHandler
    Const PROC = "GetCrystalReport"
    
    Dim CR          As CRAXDRT.Application
    Dim CRParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim CRParamDef  As CRAXDRT.ParameterFieldDefinition
    Dim CRReport    As CRAXDRT.Report
    Dim DBTable     As CRAXDRT.DatabaseTable
    
    Dim CRXSection      As CRAXDRT.Section
    Dim CRXSubreport    As CRAXDRT.Report
    Dim CRXSubreportObj As CRAXDRT.SubreportObject
    Dim CRXReportObject As Object
    
    'Dim fs              As Scripting.FileSystemObject
    Dim sReportFilepath As String
    
   
    Set CR = New CRAXDRT.Application
    
    sReportFilepath = ReportPath
     
    'check to see if report file exists, before the check 'hardware failure error' would be raised
'    Set fs = New Scripting.FileSystemObject
'    If fs.FileExists(sReportFilepath) = False Then
'        Err.Raise 1002, , "Report file '" & sReportFilepath & "' does not exist. Please contact the System Administrator."
'        Exit Function
'    End If
'    Set fs = Nothing
 
    
    Set CRReport = CR.OpenReport(sReportFilepath, CROpenReportMethod.crOpenReportByTempCopy) ' do not open exclusively
   ' Set DBInfo = New CDatabaseInformation
    
    For Each DBTable In CRReport.Database.Tables
        Debug.Print DBTable.Name
        DBTable.SetLogOnInfo "Oracle", , "WILMICRD", "wilmic"
    Next
   'Set DBInfo = Nothing
   
    CRReport.UseIndexForSpeed = True
    
    ' Cycle through each section in the main report.
    For Each CRXSection In CRReport.Sections

        'Get all the objects in the section.
        For Each CRXReportObject In CRXSection.ReportObjects

            'Test the objects to see if they're subreports.
            If CRXReportObject.Kind = crSubreportObject Then

                Set CRXSubreportObj = CRXReportObject
                Set CRXSubreport = CRXSubreportObj.OpenSubreport
                
                ' Set database connection for each subreport
                For Each DBTable In CRXSubreport.Database.Tables
                    Debug.Print DBTable.Name
                    DBTable.SetLogOnInfo "Oracle", , "WILMICRD", "wilmic"
                Next

            End If
        Next
    Next
    

    With CRReport.ParameterFields
    
        .GetItemByName("ApplicationID").SetCurrentValue ApplicationID
        .GetItemByName("ApplicationRatingID").SetCurrentValue ApplicationRatingID
        .GetItemByName("APR").SetCurrentValue APR
        .GetItemByName("PaymentOptionsCode").SetCurrentValue PaymentOptionsCode
 
    End With
      
    Set GetCrystalReport = CRReport
    
    'GetCrystalReport.Database.LogOnServer
      
    Set CR = Nothing
    
    Exit Function
    
'ErrHandler:
      'Sean: put your error handler code here
End Function

