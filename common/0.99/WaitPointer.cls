VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "WaitPointer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarPointer As MousePointerConstants

Private Sub Class_Initialize()
  mvarPointer = Screen.MousePointer
End Sub

Private Sub Class_Terminate()
  Restore
End Sub

Public Property Let Pointer(vData As MousePointerConstants)
  Screen.MousePointer = vData
End Property

Public Sub Default()
  Screen.MousePointer = vbHourglass
End Sub

Private Sub Restore()
  Screen.MousePointer = mvarPointer
End Sub

