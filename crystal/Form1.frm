VERSION 5.00
Object = "{C4847593-972C-11D0-9567-00A0C9273C2A}#8.0#0"; "CRVIEWER.DLL"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   8076
   ClientLeft      =   60
   ClientTop       =   456
   ClientWidth     =   9660
   LinkTopic       =   "Form1"
   ScaleHeight     =   8076
   ScaleWidth      =   9660
   StartUpPosition =   3  'Windows Default
   Begin CRVIEWERLibCtl.CRViewer CRViewer1 
      Height          =   7815
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9375
      DisplayGroupTree=   -1  'True
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   -1  'True
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   -1  'True
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   0   'False
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Public Function GetCrystalReport(ReportPath As String, ApplicationID As String, _
    ApplicationRatingID As String, _
    PaymentOptionsCode As String, _
    APR As Double _
    ) As CRAXDRT.Report
  
    'On Error GoTo ErrHandler
    Const PROC = "GetCrystalReport"
    
    Dim CR          As CRAXDRT.Application
    Dim CRParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim CRParamDef  As CRAXDRT.ParameterFieldDefinition
    Dim CRReport    As CRAXDRT.Report
    Dim DBTable     As CRAXDRT.DatabaseTable
    
    Dim CRXSection      As CRAXDRT.Section
    Dim CRXSubreport    As CRAXDRT.Report
    Dim CRXSubreportObj As CRAXDRT.SubreportObject
    Dim CRXReportObject As Object
    
    Dim fs              As Scripting.FileSystemObject
    Dim sReportFilepath As String
    
   
    Set CR = New CRAXDRT.Application
    
    sReportFilepath = ReportPath
     
    'check to see if report file exists, before the check 'hardware failure error' would be raised
    Set fs = New Scripting.FileSystemObject
    If fs.FileExists(sReportFilepath) = False Then
        Err.Raise 1002, , "Report file '" & sReportFilepath & "' does not exist. Please contact the System Administrator."
        Exit Function
    End If
    Set fs = Nothing
 
    
    Set CRReport = CR.OpenReport(sReportFilepath, CROpenReportMethod.crOpenReportByTempCopy) ' do not open exclusively
   ' Set DBInfo = New CDatabaseInformation
    
    For Each DBTable In CRReport.Database.Tables
        Debug.Print DBTable.Name
        DBTable.SetLogOnInfo "Oracle", , "WILMICRD", "wilmic"
    Next
   'Set DBInfo = Nothing
   
    CRReport.UseIndexForSpeed = True
    
    ' Cycle through each section in the main report.
    For Each CRXSection In CRReport.Sections

        'Get all the objects in the section.
        For Each CRXReportObject In CRXSection.ReportObjects

            'Test the objects to see if they're subreports.
            If CRXReportObject.Kind = crSubreportObject Then

                Set CRXSubreportObj = CRXReportObject
                Set CRXSubreport = CRXSubreportObj.OpenSubreport
                
                ' Set database connection for each subreport
                For Each DBTable In CRXSubreport.Database.Tables
                    Debug.Print DBTable.Name
                    DBTable.SetLogOnInfo "Oracle", , "WILMICRD", "wilmic"
                Next

            End If
        Next
    Next
    

    With CRReport.ParameterFields
    
        .GetItemByName("ApplicationID").SetCurrentValue ApplicationID
        .GetItemByName("ApplicationRatingID").SetCurrentValue ApplicationRatingID
        .GetItemByName("APR").SetCurrentValue APR
        .GetItemByName("PaymentOptionsCode").SetCurrentValue PaymentOptionsCode
 
    End With
      
    Set GetCrystalReport = CRReport
    
    'GetCrystalReport.Database.LogOnServer
      
    Set CR = Nothing
    
    Exit Function
    
'ErrHandler:
      'Sean: put your error handler code here
End Function



Private Sub Form_Load()
   Dim Report As CRAXDRT.Report
   Set Report = GetCrystalReport( _
        "M:\Und\PolForms\PremiumQuotation.rpt", _
        "AP1791300", "AR10022980", "AE", 7)
   Report.PrinterSetup Me.hWnd
   
     

   'Set CRViewer1.ReportSource = Report
    'CRViewer1.PrintReport
   ' CRViewer1.ViewReport
   ' CRViewer1.PrintReport
    
End Sub


