=========================
Admin tools release notes
=========================

Last Modified: 04/30/2004

==========
Build 1.01
==========

preferences.exe and folders.exe:
-When on the list view section, the enter key now modifys the data
-And hotkeys for Edit->New->Key (ctrl + Y) and Edit->New->Value (ctrl + U)
-After adding a new key or value, that value is selected and the rename is automatically called.
