====================================
frameworkPLUS Admin Tools readme.txt
====================================

Modified 05/03/2004

========
Overview
========
The Admin Tools are a collection of administrative utilities used to configure a frameworkPLUS implementation.

Below is a list of the individual tools:
-Folders.exe
-Preferences.exe


===============================
Folders.exe and Preferences.exe
===============================
The Folders.exe and Preferences.exe are used to modify the System Folders tree and the System Preferences tree. These trees are stored in data.

