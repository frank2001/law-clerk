Attribute VB_Name = "XMLUtil"
Option Explicit


Public Function HttpPost(URL As String, Params As String) As xmlHttp
  Dim xmlHttp As New xmlHttp
  xmlHttp.open "POST", URL, False
  xmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
  xmlHttp.send Params
  Set HttpPost = xmlHttp
  'Open "c:\xml.txt" For Append As #1
  'Write #1, xmlHttp.responseText
  'Close #1
End Function


Public Function HttpGet(URL As String) As xmlHttp
  Dim xmlHttp As New xmlHttp
  xmlHttp.open "GET", URL, False
  xmlHttp.send
  Set HttpGet = xmlHttp
End Function

'
'Public Function Rowset(Element As MSXML2.IXMLDOMNode, Source As String) As Collection
'  Dim Node, RowsetNode As MSXML2.IXMLDOMNode, Result As New Collection
'
'  If Element Is Nothing Then
'    Exit Function
'  End If
'
'  Set RowsetNode = Element.selectSingleNode("data/" & LCase(Source))
'  If Not RowsetNode.hasChildNodes Then
'    Exit Function
'  End If
'
'  For Each Node In RowsetNode.childNodes
'    Result.Add Row(Node.childNodes, Source)
'  Next Node
'
'  Set Rowset = Result
'End Function
'
'
'Public Function Row(Nodes As MSXML2.IXMLDOMNodeList, Source As String) As Collection
'  Dim Node As MSXML2.IXMLDOMNode, Result As New Collection, NodeName As String
'
'  For Each Node In Nodes
'    NodeName = Node.NodeName
'    Result.Add CreateDataField(Source, NodeName, _
'          DataValue(NodeName, Node.Text)), NodeName
'  Next Node
'
'  Set Row = Result
'End Function

  
'
' Currently, the only formatting needed is for dates and conversion to
' newline.  The XML is delivered
' in SQL date format and with a single return chr(10).
' The date will change shortly to the SOAP date format.
'
'Public Function DataValue(NodeName As String, Text As String) As String
'  If (Right(NodeName, 4) <> "Date" And Right(NodeName, 4) <> "Time") Or Len(Text) < 10 Then
'    DataValue = Replace(Text, Chr(10), vbNewLine)
'    Exit Function
'  End If
'
'  Dim DateVal As Date
'  DateVal = DateSerial(Left(Text, 4), Mid(Text, 6, 2), Mid(Text, 9, 2))
'  DataValue = Format(DateVal, LocaleShortDate)
'  If Len(Text) > 10 Then
'    DataValue = DataValue & " " & Format(TimeSerial(CInt(Mid(Text, 12, 2)), CInt(Mid(Text, 15, 2)), CInt(Mid(Text, 18, 2))), "h:mm:ss AMPM")
'  End If
'End Function


Public Function DisplayFormat(DataField As String, DataValue As String, Optional DataFormat As String, Optional DateFormat As String) As String
  If DataFormat <> vbNullString Then
    DisplayFormat = Format(DataValue, DataFormat)
    Exit Function
  End If
  If (Right(DataField, 4) <> "Date" And Right(DataField, 4) <> "Time") Or Len(DataValue) < 10 Or DateFormat = vbNullString Then
    DisplayFormat = DataValue
    Exit Function
  End If
  DisplayFormat = Format(DataValue, DateFormat)
End Function



