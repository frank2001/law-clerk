VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmFolderOrganizeDialog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Organize Folders"
   ClientHeight    =   4080
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5160
   Icon            =   "frmFolderOrganizeDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   5160
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picFolder 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3540
      Left            =   0
      ScaleHeight     =   3540
      ScaleWidth      =   5160
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   0
      Width           =   5160
      Begin VB.CommandButton cmdSync 
         Caption         =   "Sync Folders"
         Height          =   336
         Left            =   3865
         TabIndex        =   12
         Top             =   3096
         Width           =   1200
      End
      Begin VB.CommandButton cmdMoveUp 
         Caption         =   "Move &Up"
         Height          =   336
         Left            =   3876
         TabIndex        =   1
         Top             =   84
         Width           =   1200
      End
      Begin VB.CommandButton cmdMoveDown 
         Caption         =   "Move &Down"
         Height          =   336
         Left            =   3864
         TabIndex        =   2
         Top             =   480
         Width           =   1200
      End
      Begin VB.CommandButton cmdCreateFolder 
         Caption         =   "&New Folder"
         Height          =   336
         Left            =   3876
         TabIndex        =   3
         Top             =   1044
         Width           =   1200
      End
      Begin VB.CommandButton cmdMoveToFolder 
         Caption         =   "Mo&ve to Folder"
         Height          =   336
         Left            =   3876
         TabIndex        =   4
         Top             =   1428
         Width           =   1200
      End
      Begin VB.CommandButton cmdRename 
         Caption         =   "&Rename"
         Height          =   336
         Left            =   3876
         TabIndex        =   5
         Top             =   2016
         Width           =   1200
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "D&elete"
         Height          =   336
         Left            =   3876
         TabIndex        =   6
         Top             =   2388
         Width           =   1200
      End
      Begin MSComctlLib.TreeView tvFolder 
         Height          =   3384
         Left            =   60
         TabIndex        =   0
         Top             =   48
         Width           =   3708
         _ExtentX        =   6535
         _ExtentY        =   5980
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   494
         LabelEdit       =   1
         PathSeparator   =   "/"
         Style           =   7
         BorderStyle     =   1
         Appearance      =   1
      End
   End
   Begin VB.PictureBox picOkCancel 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   528
      Left            =   0
      ScaleHeight     =   525
      ScaleWidth      =   5160
      TabIndex        =   7
      Top             =   3540
      Width           =   5160
      Begin VB.CommandButton cmdHelp 
         Caption         =   "&Help"
         Height          =   336
         Left            =   3888
         TabIndex        =   11
         Top             =   120
         Width           =   1200
      End
      Begin VB.Frame fraLine 
         Height          =   36
         Left            =   12
         TabIndex        =   10
         Top             =   0
         Width           =   5076
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Close"
         Default         =   -1  'True
         Height          =   336
         Left            =   2592
         TabIndex        =   8
         Top             =   120
         Width           =   1200
      End
      Begin MSComctlLib.ImageList imlTVFolders 
         Left            =   612
         Top             =   96
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   255
         _Version        =   393216
      End
   End
End
Attribute VB_Name = "frmFolderOrganizeDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'
' General Methods
'

Public Sub ButtonRefresh()
  With tvFolder.SelectedItem
    cmdMoveUp.Enabled = Not .Previous Is Nothing
    cmdMoveDown.Enabled = Not .Next Is Nothing
  End With
  tvFolder.SetFocus
End Sub

Public Sub ShowFolder(Key As String)
  Load Me
  tvFolder.Nodes(Key).Selected = True
  Show vbModal

End Sub

Private Sub Swap(Node As Node, Relative As Node, Relationship As Integer)
  If Relationship = tvwChild Then
    If Not Factory.FolderService.MoveFolder(Node.Key, Relative.Key) Then
      Exit Sub
    End If
  ElseIf Not Factory.FolderService.SwapFolder(Node.Key, Relative.Key) Then
    Exit Sub
  End If
  
  ' Remove node and add back in and then add children.
  tvFolder.Nodes.Remove Node.Key
  Dim ExpandedImage As String
  ExpandedImage = Node.ExpandedImage
  Set Node = tvFolder.Nodes.Add(Relative, Relationship, Node.Key, Node.Text, Node.Image, Node.SelectedImage)
  Node.ExpandedImage = ExpandedImage
  FolderUtil.TreeViewLoadNodes tvFolder, Node, FolderUtil.FolderNodes(Node.Key)
  
  Node.Selected = True
  ButtonRefresh
End Sub
'
' End General Methods
'
'
' Control Methods
'

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Sub cmdCreateFolder_Click()
  PreferenceUtil.AddFolder tvFolder
  ButtonRefresh
End Sub

Private Sub cmdDelete_Click()
  PreferenceUtil.RemoveFolder tvFolder
  ButtonRefresh
End Sub

Private Sub cmdHelp_Click()
  FormUtil.ShowHelp HelpName.ORGANIZINGFOLDERS_TOPIC, Me.hWnd
End Sub

Private Sub cmdMoveDown_Click()
  Swap tvFolder.SelectedItem, tvFolder.SelectedItem.Next, tvwNext
End Sub

Private Sub cmdMoveToFolder_Click()
  Dim fForm As New frmFolderBrowseDialog
  If fForm.ShowMove(tvFolder.SelectedItem.Key) Then
    Swap tvFolder.SelectedItem, tvFolder.Nodes(fForm.FolderKey), tvwChild
  End If
  Unload fForm
  ButtonRefresh
End Sub

Private Sub cmdMoveUp_Click()
  Swap tvFolder.SelectedItem, tvFolder.SelectedItem.Previous, tvwPrevious
End Sub

Private Sub cmdRename_Click()
  tvFolder.StartLabelEdit
End Sub

Private Sub cmdSync_Click()
  If Factory.PreferenceService.Sync(SystemFolders_TreeName) Then
    Me.Hide
  End If
End Sub

Private Sub Form_Activate()
  ButtonRefresh
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    cmdHelp_Click
  End If
End Sub

Private Sub Form_Load()
  'load images for Tree View
  imlTVFolders.ListImages.Add 1, "TVFolderImage", PicDisplay.GetPicture(UrlName.TVFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 2, "TVOpenFolderImage", PicDisplay.GetPicture(UrlName.TVOPENFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 3, "LAWFolder", PicDisplay.GetPicture(UrlName.LAWFOLDER_IMAGE)
  
  tvFolder.ImageList = imlTVFolders
  
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolder
  'FolderUtil.TreeViewLoadFolder tvFolder, systemfolders_treename
  
End Sub

Private Sub tvFolder_AfterLabelEdit(Cancel As Integer, NewString As String)
  If Not DirNameSafe(NewString) Then
    Cancel = True
    Exit Sub
  End If
  If tvFolder.SelectedItem.Key = SystemFolders_RootKey Then
    SystemFolders_RootName = NewString
    PreferenceUtil.renameIniNode SystemFolders_TreeName, SystemPreferences_RootName
  Else
    Cancel = Not Factory.PreferenceService.RenameFolder(SystemFolders_TreeName, tvFolder.SelectedItem.Key, NewString)
  End If

  
End Sub

Private Sub tvFolder_NodeClick(ByVal Node As MSComctlLib.Node)
  ButtonRefresh
End Sub
