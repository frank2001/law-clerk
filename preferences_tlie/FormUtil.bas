Attribute VB_Name = "FormUtil"
Option Explicit

Public Const lawActionComplete = 0
Public Const lawActionVoid = 1
Public Const lawActionReopen = 2
Public Const lawActionNew = 3

Public Declare Function LaunchHtmlHelp Lib "Hhctrl.ocx" _
    Alias "HtmlHelpA" (ByVal hWndCaller As Long, _
    ByVal pszFile As String, ByVal uCommand As Long, _
    ByVal dwData As Long) As Long

' Constants for HTML help
Public Const HH_DISPLAY_TOPIC = &H0
Public Const HH_HELP_CONTEXT = &HF



Public Sub ShowHelp(Optional Topic As Long, Optional WindowHandle As Long)
  Dim HelpFilePath As String
  Dim lngReturn As Long
  
  HelpFilePath = App.Path & "\" & "LawSystemHelp.chm"
  
  ' Check to see if file exists
  If Len(Dir(HelpFilePath)) > 0 Then
    If Topic = 0 Then
      ' Load default help
      lngReturn = LaunchHtmlHelp(WindowHandle, HelpFilePath, HH_DISPLAY_TOPIC, 0)
    Else
      ' Load by topic
      lngReturn = LaunchHtmlHelp(WindowHandle, HelpFilePath, HH_HELP_CONTEXT, Topic)
    End If
    If CBool(lngReturn) Then
      'Help was successfully loaded using the topic
      Exit Sub
    End If
    'Load the default help if the topic fails
    MsgBox "Unable to display associated Help Contents. The default help will be displayed.", vbInformation, "Help"
    lngReturn = LaunchHtmlHelp(WindowHandle, HelpFilePath, HH_DISPLAY_TOPIC, 0)
  Else
    
    ' Displays if help file not found
    MsgBox "Unable to display Help Contents. Help file not found.", vbInformation, "Help"
  End If
  
End Sub

Public Function ActionText(ActionType As Integer, Optional IsDatabaseName As Boolean) As String
  Select Case ActionType
    Case 0
      ActionText = "Complete"
    Case 1
      ActionText = "Void"
    Case 2
      ActionText = "Reopen"
    Case 3
      ActionText = IIf(IsDatabaseName, "Create", "New")
  End Select

End Function

Public Sub EditTextSelect(Text As Control)
'  If LocaleTextSelectOnFocus = True Then
    SelAll Text
'  End If
End Sub

Public Sub SelAll(Text As Control)
  Text.SelStart = 0
  Text.SelLength = Len(Text)
End Sub


'Public Sub PostTab(hWnd As Long)
'  PostMessage hWnd, WM_KEYDOWN, vbKeyTab, 0
'End Sub


'
' Load up open menu.
'
Public Sub MenuUnload(Menu As Object)
  Dim item As Menu, Index As Integer
  
  ' Ensure first is visible before removing everything.
  Menu(0).Visible = True
  
  ' Clear destination.
  For Each item In Menu
    If item.Index <> 0 Then Unload item
  Next item
End Sub

'
' Load up open menu.
'
Public Sub MenuCopy(Source As Object, Destination As Object)
  Dim item As Menu, Index As Integer
  
  ' Unload any destination.
  MenuUnload Destination
  
  ' If no source, then just exit
  If Source Is Nothing Then Exit Sub
  
  ' Copy source to destination.
  For Each item In Source
    Index = item.Index
    If Index <> 0 Then
      Load Destination(Index)
      With Destination(Index)
        .Caption = item.Caption
        .Enabled = item.Enabled
        .Visible = item.Visible
      End With
    End If
  Next item
  
  ' Hide holding menu if something has been copied.
  If Destination.Count > 1 Then
    Destination(0).Visible = False
  End If
End Sub


Public Sub MenuLoad(frm As Form)
  ' Custom Menu Shortcut Adjustments
  frm.KeyPreview = True
  
  frm.mnuViewPrevious.Caption = frm.mnuViewPrevious.Caption + vbTab + "Ctrl+<"
  frm.mnuViewNext.Caption = frm.mnuViewNext.Caption + vbTab + "Ctrl+>"
End Sub

'
' Requires standard "Edit" menus and command buttons.
'
'Public Sub EditLoad(frm As Form)
'  On Error Resume Next
'
'  MenuLoad frm
'
'  frm.cmdEnter.Default = LocaleEnterAsTab
'  frm.cmdEnter.TabStop = False
'
'  frm.cmdCancel.Cancel = True
'  frm.cmdCancel.CausesValidation = False
'End Sub


Public Sub EditLoadImages(frm As Form)
  'add the images
  ' fill image list
  frm.imlUpDown.ListImages.Add 1, "Next", PicDisplay.GetPicture(UrlName.DOWN_IMAGE)
  frm.imlUpDown.ListImages.Add 2, "Previous", PicDisplay.GetPicture(UrlName.UP_IMAGE)
  frm.imlUpDown.MaskColor = &HC0C0C0
  frm.imlUpDown.UseMaskColor = True
  
  frm.imlUpDownDisabled.ListImages.Add 1, "DisNext", PicDisplay.GetPicture(UrlName.DOWNDISABLED_IMAGE)
  frm.imlUpDownDisabled.ListImages.Add 2, "DisPrevious", PicDisplay.GetPicture(UrlName.UPDISABLED_IMAGE)
  frm.imlUpDownDisabled.MaskColor = &HC0C0C0
  frm.imlUpDownDisabled.UseMaskColor = True
  
  ' set the image list to the toolbar
  frm.tbrUpDown.ImageList = frm.imlUpDown
  frm.tbrUpDown.DisabledImageList = frm.imlUpDownDisabled
  frm.tbrUpDown.Buttons(1).Image = "Next"
  frm.tbrUpDown.Buttons(2).Image = "Previous"
End Sub

Public Function ValidateControls(Form As Form) As Boolean
  On Error Resume Next
  Form.ValidateControls
  ValidateControls = Not Err
End Function


'
' Navigation Refresh
'
Public Sub NavigationRefresh(Form As Form, View As ListView, Index As Integer)
  '
  ' Standard Navigation Update for all Edit form mnuViewRefresh.
  '
  If (View Is Nothing) Then
    Form.mnuViewPrevious.Enabled = False
    Form.mnuViewNext.Enabled = False
    Form.tbrUpDown.Buttons(1).Enabled = False
    Form.tbrUpDown.Buttons(2).Enabled = False
  Else
    Form.mnuViewNext.Enabled = View.Enabled And Index < View.ListItems.Count
    Form.mnuViewPrevious.Enabled = View.Enabled And Index > 1
    Form.tbrUpDown.Buttons(1).Enabled = Form.mnuViewNext.Enabled
    Form.tbrUpDown.Buttons(2).Enabled = Form.mnuViewPrevious.Enabled
  End If
End Sub
'
' When the text note gets focus disable the default
' buttons to allow the enter key to act as a carriage
' return while editing in the note
'
Public Sub NoteGotFocus(DefaultControl As CommandButton, Optional EnterControl As CommandButton)
  DefaultControl.Default = False
  If Not EnterControl Is Nothing Then
    EnterControl.Default = False
  End If
End Sub
'
' Reset the default buttons
'
'Public Sub NoteLostFocus(DefaultControl As CommandButton, Optional EnterControl As CommandButton)
'  DefaultControl.Default = Not LocaleEnterAsTab
'  If Not EnterControl Is Nothing Then
'    EnterControl.Default = LocaleEnterAsTab
'  End If
'End Sub

'
' Standardize call to write to form's ini file.
'
Public Sub WriteIniFile(FormName As String, Name As String, Value As String)
  IniForm.WriteFile FormName, Name, Value
End Sub

'
' Save form position.
'
'Public Sub SerializePosition(Form As Form)
'  If Form.WindowState = vbNormal Then
'    WriteIniFile Form.Name, "Left", Form.Left
'    WriteIniFile Form.Name, "Top", Form.Top
'    If Form.BorderStyle = vbSizable Then
'      WriteIniFile Form.Name, "Height", Form.Height
'      WriteIniFile Form.Name, "Width", Form.Width
'    End If
'  End If
'  WriteIniFile Form.Name, "WindowState", Form.WindowState
'End Sub

'
' Standardize call to read from forms ini file.
'
Public Function ReadIniFile(FormName As String, Name As String) As String
  ReadIniFile = IniForm.ReadFile(FormName, Name)
End Function

'
' Load form position.  See if other forms of the same name are
' open and offset from those.
'
'Public Sub DeserializePosition(Form As Form)
'  Dim mLeft, mTop As String
'  mLeft = ReadIniFile(Form.Name, "Left")
'  mTop = ReadIniFile(Form.Name, "Top")
'
'  Dim Window As Form
'  For Each Window In Forms
'    If Window.Name = Form.Name And Window.hWnd <> Form.hWnd Then
'      If Window.WindowState = vbMinimized Then
'        Window.WindowState = vbNormal
'      End If
'      Window.Show
'      Window.Refresh
'      mLeft = Window.Left + 288
'      mTop = Window.Top + 336
'    End If
'  Next Window
'
'  If mLeft <> vbNullString Then Form.Left = mLeft
'  If mTop <> vbNullString Then Form.Top = mTop
'
'  Dim Value As String
'  Value = ReadIniFile(Form.Name, "WindowState")
'  If Value <> vbNullString Then
'    If Value = vbMaximized Then
'      Form.WindowState = vbMaximized
'      Exit Sub
'    End If
'  End If
'  Form.WindowState = vbNormal
'
'  If Form.BorderStyle = vbSizable Then
'    Value = ReadIniFile(Form.Name, "Height")
'    If Value <> vbNullString Then Form.Height = Value
'    Value = ReadIniFile(Form.Name, "Width")
'    If Value <> vbNullString Then Form.Width = Value
'  End If
'End Sub

'
' Used by form main when a child is unloaded.
'
Public Function FindLastForm(Form As Form) As Form
  Dim Window As Variant
  For Each Window In Forms
    If Window.Name = Form.Name And Window.hWnd <> Form.hWnd Then
      Set FindLastForm = Window
    End If
  Next Window
End Function

Public Sub SizeSingleToolbar(Bar As Toolbar, Left As Integer)
  Bar.Height = Bar.ButtonHeight
  Bar.Width = Bar.Buttons(1).Width
  Bar.Left = Left
End Sub

'Public Sub ShowByEventListView(Parent As Form, Child As Form)
'  Parent.FormMgr.Add Child
'  Child.ShowByListView Parent.lvView, DatabaseName.EVENT_TABLE
'End Sub

'Public Sub ShowByListView(Form As Form, FormMgr As FormManager, View As ListView, Optional FormMgrKey)
'  If View.ListItems.Count = 0 Then
'    MsgBox "There are no items to view or edit."
'    Unload Form
'    Exit Sub
'  End If
'
'  If IsMissing(FormMgrKey) Then
'    FormMgr.Add Form
'  Else
'    FormMgr.Add Form, CStr(FormMgrKey)
'  End If
'
'  Form.ShowByListView View
'End Sub

'Public Sub ShowByPrimaryKey(Form As Form, Handle As String, Optional FormMgr As FormManager, Optional Index, Optional FormMgrKey)
'  If Not FormMgr Is Nothing Then
'    If IsMissing(FormMgrKey) Then
'      FormMgr.Add Form
'    Else
'      FormMgr.Add Form, CStr(FormMgrKey)
'    End If
'  End If
'  If IsMissing(Index) Then
'    Form.ShowByPrimaryKey Handle
'  Else
'    Form.ShowByPrimaryKey Handle, CInt(Index)
'  End If
'End Sub
'
'Public Sub ShowByPrimaryKeys(Form As Form, FormMgr As FormManager, Handle1 As String, Optional Handle2, Optional Index)
'  FormMgr.Add Form
'  If IsMissing(Index) Then
'    Form.ShowByPrimaryKey Handle1, Handle2
'  Else
'    Form.ShowByPrimaryKey Handle1, Handle2, Index
'  End If
'End Sub

'Public Sub ShowEventDialog(Table As String, Handle As String, ActionType As Integer)
'  Dim fForm As New frmEventDialog
'  fForm.ReferenceLoad Table, Handle, ActionType
'  fForm.Show vbModal
'  Unload fForm
'End Sub
'
'Public Function ShowEventProperties(EventHandle As String) As Boolean
'  If EventHandle = vbNullString Then
'    Exit Function
'  End If
'
'  Dim DataField As Collection
'  Set DataField = Factory.EventService.Find(EventHandle)
'
'  If DataField Is Nothing Then
'    Exit Function
'  End If
'
'  Dim fEvent As New frmEventPropertiesEdit
'  fEvent.ShowEvent DataField
'  ShowEventProperties = True
'End Function

'Public Sub ShowNew(Form As Form, FormMgr As FormManager, Optional Handle, Optional FormMgrKey)
'  If IsMissing(FormMgrKey) Then
'    FormMgr.Add Form
'  Else
'    FormMgr.Add Form, CStr(FormMgrKey)
'  End If
'  If IsMissing(Handle) Then
'    Form.ShowNew
'  Else
'    Form.ShowNew Handle
'  End If
'End Sub
'
'Public Function ShowProperties(Source As String, Handle As String) As Boolean
'  Dim SourceId As String
'  SourceId = Replace(Source, "View", vbNullString)
'  SourceId = LCase(Left(SourceId, 1)) & Mid(SourceId, 2) & "Id"
'
'  Dim DataProvider As DataProvider, DataField As Collection
'  Set DataProvider = LawSession.NewDataProvider
'  DataProvider.Source = Source
'
'  Set DataField = DataProvider.Find(SourceId, Handle)
'  If DataField Is Nothing Then
'    Exit Function
'  End If
'
'  Dim fProps As New frmPropertiesEdit
'  fProps.ShowProperties Source, DataField
'  ShowProperties = True
'End Function
'
'Public Function ShowPropertiesMulti(Source As String, HandleName1 As String, HandleName2 As String, HandleValue1 As String, HandleValue2 As String) As Boolean
'  Dim DataProvider As DataProvider, DataField As Collection
'  Set DataProvider = LawSession.NewDataProvider
'  DataProvider.Source = Source
'  DataProvider.Filter = """" & HandleName1 & """ = '" & HandleValue1 & "' AND " & _
'      """" & HandleName2 & """ = '" & HandleValue2 & "'"
'
'  DataProvider.OpenRecordset 1
'  If DataProvider.IsEmpty Then
'    Exit Function
'  End If
'
'  Dim fProps As New frmPropertiesEdit
'  fProps.ShowProperties Source, DataProvider.Collection(1)
'  ShowPropertiesMulti = True
'End Function
''
'Public Sub SendToFolder(Group As String, Source As String, Handle As String, Name As String)
'  ' Create form and show.
'  Dim Form As New frmFolderBrowseDialog
'  If Not Form.ShowSendTo(Group) Then
'    Unload Form
'    Exit Sub
'  End If
'
'  Dim Handles(0) As String, Names(0) As String
'  Handles(0) = Handle
'  Names(0) = Name
'
'  Call Factory.EventService.Create( _
'      Form.EventTypeId, _
'      Group, _
'      Form.FolderKey, _
'      Source, _
'      Handles, _
'      Names)
'
'  Unload Form
'End Sub

'
' Used by SStabs to fix the tab order by enabling the control.
' Just call this code form the tab click routine and in Form_Load
'
Public Sub TabControlEnable(mSSTab As ssTab)
  Dim c As Control
  For Each c In mSSTab.Parent.Controls
    If Not (TypeOf c Is ImageList Or _
            TypeOf c Is Menu Or _
            TypeOf c Is CommonDialog Or _
            TypeOf c Is Timer Or _
            TypeOf c Is PictureBox) Then
      If c.Container Is mSSTab Then
        c.Enabled = c.Left > 0
      End If
    End If
  Next
End Sub



'
'Public Sub MenuEventLoad(Popup As Form, Handle As String)
'  With Popup
'    ' Unload current menu.
'    FormUtil.MenuUnload .mnuEventOpen
'
'    Dim Refs As Collection, I, j As Integer, Source As String
'    'ToDo: Sort refs by caption
'    Set Refs = Factory.ReferenceService.References(Handle, DatabaseName.REFERENCETABLE_FIELD)
'    If Refs Is Nothing Then
'      .mnuEventOpen(0).Visible = False
'      Exit Sub
'    End If
'
'    For I = 1 To Refs.Count
'      Source = Refs.item(I).item("referenceTable")
'      Load .mnuEventOpen(I)
'      .mnuEventOpen(I).Tag = Source
'      .mnuEventOpen(I).Caption = "Open " & Source
'    Next I
'
'    Load .mnuEventOpen(I)
'    .mnuEventOpen(I).Tag = "Notepad"
'    .mnuEventOpen(I).Caption = "Open Notepad"
'    I = I + 1
'
'    'Hack
'    If Source = "Claim" Then
'      Load .mnuEventOpen(I)
'      .mnuEventOpen(I).Tag = "BrowseClaim"
'      .mnuEventOpen(I).Caption = "Browse Claim"
'      I = I + 1
'    End If
'
'    'ver 2.21.0708 jjm
'    'Hack
'    'eventTypeId = worksheet claim then...
'    If Factory.EventService.Find(Handle, """" & DatabaseName.EVENTTYPE_KEY & """")(DatabaseName.EVENTTYPE_KEY) = "WorksheetClaim" Then
'      Load .mnuEventOpen(I)
'      .mnuEventOpen(I).Tag = "BrowseWorksheet"
'      .mnuEventOpen(I).Caption = "Browse Worksheet"
'      I = I + 1
'    End If
'
'    Dim Row As Collection
'    Set Row = Factory.EventService.Find(Handle, """sourceGroupId""")
'    If Not Row Is Nothing Then
'      If Row("sourceGroupId") = DatabaseName.FIRM_DOCUMENT Or _
'         Row("sourceGroupId") = DatabaseName.CLAIM_DOCUMENT Or _
'         Row("sourceGroupId") = DatabaseName.ENDORSEMENT_TABLE _
'      Then
'        Load .mnuEventOpen(I + 1)
'        .mnuEventOpen(I + 1).Tag = Row("sourceGroupId")
'        .mnuEventOpen(I + 1).Caption = "Edit Document"
'      End If
'    End If
'    .mnuEventOpen(0).Visible = False
'    .mnuEventPaste.Enabled = (Not EventBuffer Is Nothing)
'  End With
'End Sub

Public Sub Dirty(Form As Form, IsDirty As Boolean, Optional IsNew As Boolean)
  On Error Resume Next
  
  Dim isOpen As Boolean
  isOpen = IsDirty Or IsNew
  
  Form.mnuFileSave.Enabled = isOpen
  Form.cmdSave.Enabled = isOpen
  Form.cmdSaveNew.Enabled = isOpen
  Form.cmdCancel.Caption = IIf(IsDirty, "Cancel", "Close")
  
  Form.mnuFileWorkflow(0).Enabled = Not isOpen
  Form.mnuFileWorkflow(1).Enabled = Not isOpen
  
  ' Clear any error.
  Err.Clear
End Sub


'Public Sub GLTransactionLoad(View As ListView, _
'    GLAccountIds() As String, GLTransactionAmounts() As Double)
'
'  Dim Count As Integer, I As Integer
'  Count = View.ListItems.Count
'
'  If Count = 0 Then Exit Sub
'
'  ReDim GLAccountIds(Count - 1)
'  ReDim GLTransactionAmounts(Count - 1)
'  For I = 1 To Count
'    GLAccountIds(I - 1) = View.ListItems(I)
'    GLTransactionAmounts(I - 1) = View.ListItems(I).ListSubItems("transactionAmount")
'  Next I
'
'End Sub

'Public Sub LoadListViewCollection(LV As ListView, mCollection As Collection)
'  Dim n As Integer
'  Dim xItem As ListItem
'
'  Set xItem = LV.ListItems.Add(, , mCollection(LV.ColumnHeaders(1).Key))
'  For n = 2 To LV.ColumnHeaders.Count
'    xItem.ListSubItems.Add , LV.ColumnHeaders(n).Key, mCollection(LV.ColumnHeaders(n).Key)
'  Next n
'End Sub

'Public Sub ComboBoxLoad(Items As Collection, Combo As ComboBox, Index As Variant)
'  If Items Is Nothing Then Exit Sub
'  Dim I, Count As Integer
'  Count = Items.Count
'  For I = 1 To Count
'    Combo.AddItem Items(I)(Index)
'  Next I
'End Sub
'
'Public Sub ComboBoxRegistryLoad(Combo As ComboBox, Key As String)
'  Dim List As String, Items() As String
'  Combo.Clear
'  List = IniMain.ReadFile("Dropdown", Key)
'  If List <> vbNullString Then
'    ComboBoxArrayLoad Combo, Split(List, "|")
'  End If
'End Sub
'
'Public Sub ComboBoxArrayLoad(Combo As ComboBox, List As Variant)
'  Dim I, Count As Integer
'  Count = UBound(List)
'  For I = 0 To Count
'    Combo.AddItem List(I)
'  Next I
'End Sub
'
'Public Sub ShowLawyerByPrimaryKey(Form As Form, FormMgr As FormManager, LawyerHandle As String, Optional LawyerTab As Boolean, Optional FormMgrKey)
'  Dim PersonId As String
'  PersonId = Factory.LawyerService.Find(LawyerHandle, """" + DatabaseName.PERSON_KEY + """")(DatabaseName.PERSON_KEY)
'  If (PersonId = vbNullString) Then
'    MsgBox "Data corruption problem.  The lawer selected has lost its reference to a person."
'    Exit Sub
'  End If
'  Dim fPerson As New frmPersonEdit
'  SetFormMgr fPerson, FormMgr, FormMgrKey
'  fPerson.ShowByPrimaryKey PersonId, LawyerTab
'End Sub

'Private Sub SetFormMgr(Form As Form, FormMgr As FormManager, Optional FormMgrKey)
'  If IsMissing(FormMgrKey) Then
'    FormMgr.Add Form
'  Else
'    FormMgr.Add Form, CStr(FormMgrKey)
'  End If
'End Sub
'
'Public Function PrintEnvelope(Optional DeliveryAddress As String, Optional DeliveryPoint As String) As Boolean
'  Dim fEnvelope As New frmEnvelopeLabel
'  fEnvelope.DeliveryAddress = DeliveryAddress
'  fEnvelope.DeliveryPoint = DeliveryPoint
'  fEnvelope.PrintBarCode = 1
'  fEnvelope.Show vbModal
'  PrintEnvelope = fEnvelope.OK
'  Unload fEnvelope
'End Function

Public Function Today(Optional DateTime As Date) As Date
  If DateTime = vbNullDate Then
    DateTime = Now
  End If
  Today = DateSerial(Year(DateTime), Month(DateTime), Day(DateTime))
End Function
Public Function GLTransactionAdd(View As ListView, _
    GLAccountId As String, Name As String, Reference As String, Amount As Double) As ListItem
  
  Dim xItem As ListItem
  Set xItem = View.ListItems.Add(, , GLAccountId)
  xItem.ListSubItems.Add , "transactionName", Name
  xItem.ListSubItems.Add , "transactionReference", Reference
  xItem.ListSubItems.Add , "transactionAmount", Amount
  xItem.ListSubItems.Add , "~debit"
  xItem.ListSubItems.Add , "~credit"
  Set GLTransactionAdd = xItem
End Function

Public Sub GLTransactionEdit(item As ListItem, _
    GLAccountId As String, Name As String, Reference As String, Amount As Double)
    
  item.Text = GLAccountId
  item.ListSubItems("transactionName").Text = Name
  item.ListSubItems("transactionReference").Text = Reference
  item.ListSubItems("transactionAmount").Text = Amount
End Sub
    

Public Sub GLTransactionSummary(Items As ListItems, _
    Optional lblDebit As Label, Optional lblCredit As Label, Optional lblBalance As Label, Optional Add As Boolean)
  
  Dim item As ListItem
  Dim Debits As Double, Credits As Double
  If Add Then
    Debits = GetDouble(lblDebit)
    Credits = GetDouble(lblCredit)
  End If
  For Each item In Items
    If item.ListSubItems("transactionAmount") < 0 Then
      item.ListSubItems("~debit") = ""
      item.ListSubItems("~credit") = Format(0 - item.ListSubItems("transactionAmount"), "#,##0.00;(#,##0.00)")
      Credits = Credits + item.ListSubItems("~credit")
    Else
      item.ListSubItems("~debit") = Format(item.ListSubItems("transactionAmount"), "#,##0.00;(#,##0.00)")
      item.ListSubItems("~credit") = ""
      Debits = Debits + item.ListSubItems("~debit")
    End If
  Next item
  
  If Not lblDebit Is Nothing Then
    lblDebit.Caption = Format(Debits, "#,##0.00;(#,##0.00)")
  End If
  
  If Not lblCredit Is Nothing Then
    lblCredit.Caption = Format(Credits, "#,##0.00;(#,##0.00)")
  End If
  
  If Not lblBalance Is Nothing Then
    lblBalance.Caption = Format(Debits - Credits, "#,##0.00;(#,##0.00)")
  End If

End Sub
'Public Function ExportItem(View As ListView, ExportIndex As Integer, SaveDialog As CommonDialog, Optional FileName As String, Optional FolderKey As String) As Boolean
'  Dim Pointer As New WaitPointer
'  Pointer.Default
'
'  Dim ReferenceTable As String, ServiceName As String, Selected() As String
'  Select Case ExportIndex
'    Case 0
'      ReferenceTable = DatabaseName.APPLICATION_TABLE
'      ServiceName = "applicationprint"
'      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
'    Case 1
'      ReferenceTable = DatabaseName.APPLICATION_TABLE
'      ServiceName = "applicationpartIIprint"
'      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
'    Case 2
'      ReferenceTable = DatabaseName.APPLICATION_TABLE
'      ServiceName = "applicationpartIIIprint"
'      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
'    Case 3
'      ServiceName = "LAWBatchExport.txt"
'      Selected = ListViewModule.ListItemArray(View, DatabaseName.GLBATCH_KEY)
'    Case 4
'      ServiceName = "eventprint"
'      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
'    Case 5
'      ServiceName = "financeprint"
'      Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
'    Case 6
'      ServiceName = "policyprint"
'      If FolderKey = DatabaseName.POLICY_TABLE Then
'        Selected = ListViewModule.ListItemArray(View, DatabaseName.POLICY_KEY)
'      Else
'        ReferenceTable = DatabaseName.POLICY_TABLE
'        Selected = ListViewModule.ListItemArray(View, DatabaseName.EVENT_KEY)
'      End If
'  End Select
'
'  Dim Key As String, Params As String, I, Count As Integer
'
'  For I = 0 To SafeUBound(Selected)
'    If ReferenceTable = vbNullString Then
'      Key = Selected(I)
'    Else
'      Key = Factory.ReferenceService.ReferenceId1(Selected(I), ReferenceTable)
'    End If
'    If Not Key = vbNullString Then
'      Params = Params & IIf(Params = "", "", "&") & "key=" & EncodeURL(Key)
'    End If
'  Next I
'
'  If Params = vbNullString Then
'    MsgBox "Nothing was found to export."
'    Exit Function
'  End If
'
'  ExportItem = CommonDialogUtil.HttpSave(SaveDialog, HttpServer & WebApplication & WebService & ServiceName & "?content=text/plain", Params, FileName)
'End Function

Public Sub SetTextControlFont(TextControl As Control, vFont As StdFont)
  TextControl.Font = vFont.Name
  TextControl.FontStrikethru = vFont.Strikethrough
  TextControl.FontUnderline = vFont.Underline
  TextControl.FontItalic = vFont.Italic
  TextControl.FontBold = vFont.Bold
  TextControl.FontSize = vFont.Size
End Sub


Public Sub SetComboDefault(Combo As Control, Optional Ref As String)
  IniMain.WriteFile "defaults", Combo.Tag & Ref, Combo.Text
End Sub

Public Sub GetComboDefault(Combo As Control, Optional Ref As String)
  Dim I As Integer, Key As String
  
  Combo.ListIndex = 0
  Key = IniMain.ReadFile("defaults", Combo.Tag & Ref)
  
  For I = 0 To Combo.ListCount - 1
    If Combo.List(I) = Key Then
      Combo.ListIndex = I
      Exit For
    End If
  Next I
End Sub
