VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "XMLSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Implements Session

'
' Session
'
' The Session class establishes a connection with a data provider.
'
' This session object establishes XML connections.
'

Private mConnection As String
Private mSession As String
Private mSQLDialect As New SQLDialect

'Private Function Session_NewDataProvider(Optional Source As String) As DataProvider
'  Dim xmlDS As New XMLDataProvider, ds As DataProvider
'  xmlDS.Connection = mConnection
'  xmlDS.Session = mSession
'  Set ds = xmlDS
'  ds.Source = Source
'  Set Session_NewDataProvider = ds
'End Function


Private Function Session_Authenticate(Form As Object) As Boolean
  Dim xmlHttp As New xmlHttp
  Dim Request As String
  Dim xmlDoc As DOMDocument

  'Request = Form.cboServer & "malcolm/servlet/session?j_username=" & Form.txtUserName & "&j_password=" & Form.txtPassword & "&" & NoCache
  Request = Form.txtServerAddress & "malcolm/servlet/session?j_username=" & Form.txtUserName & "&j_password=" & Form.txtPassword & "&" & NoCache
  xmlHttp.open "GET", Request, False
  
  On Error GoTo xmlHttpError
  xmlHttp.send
  
  'If we don't get a good return code.
  If xmlHttp.Status <> 200 Then
    Session_Authenticate = False
    MsgBox "Username and password are invalid"
    Exit Function
  End If
  
  'UserName = Form.txtUserName
  mConnection = Form.txtServerAddress & "malcolm/"
  mSession = ";jsessionid=" & xmlHttp.responseText
  Session_Authenticate = True
  Exit Function

xmlHttpError:
  MsgBox "Unable to reach server '" & Form.txtServerAddress.Text & "': " & Err.Description & ".  To change the destination server, click the Advanced button."
  Err.Clear
End Function


Private Property Get Session_Connection() As String
  Session_Connection = mConnection
End Property


Private Property Get Session_Session() As String
  Session_Session = mSession
End Property

Private Property Get Session_SQLDialect() As SQLDialect
  Set Session_SQLDialect = mSQLDialect
End Property
