VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DataProvider"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Filter"
' ver 2.00.0219 (jjm)
' DataProvider Interface

Option Explicit

Public Property Get BaseFilter() As String
End Property

Public Property Let BaseFilter(ByVal vData As String)
End Property

Public Property Get IsEmpty() As Boolean
End Property


Public Property Get isOpen() As Boolean
End Property


Public Property Get Params() As String
End Property


Public Property Let Source(ByVal vData As String)
End Property


Public Property Get Source() As String
End Property


Public Property Let Column(ByVal vData As String)
End Property


Public Property Get Column() As String
End Property


Public Property Let Filter(ByVal vData As String)
End Property


Public Property Get Filter() As String
End Property


Public Property Set Recordset(ByVal vData As Variant)
End Property


Public Property Get Recordset() As Variant
End Property


Public Property Let Sort(ByVal vData As String)
End Property


Public Property Get Sort() As String
End Property


Public Sub OpenRecordset(Optional Count As Integer)
End Sub


Public Property Get Collection() As Collection
End Property


Public Sub SetListView(lvView As Object, Optional isIcon As Boolean = True)
End Sub


Public Function Find(HandleName As String, Handle, Optional Column As String) As Collection
End Function


Public Function FindMulti(HandleNames() As String, Handles, Optional Column As String) As Collection
End Function
