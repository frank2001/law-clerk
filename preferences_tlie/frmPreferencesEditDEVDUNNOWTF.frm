VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmPreferencesEdit 
   Caption         =   "Preferences Edit"
   ClientHeight    =   6204
   ClientLeft      =   132
   ClientTop       =   708
   ClientWidth     =   9084
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6204
   ScaleWidth      =   9084
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picBody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   3360
      Left            =   0
      ScaleHeight     =   3360
      ScaleWidth      =   9084
      TabIndex        =   1
      Top             =   0
      Width           =   9084
      Begin VB.PictureBox picSplitter 
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         FillColor       =   &H00808080&
         Height          =   4800
         Left            =   3036
         ScaleHeight     =   2084.849
         ScaleMode       =   0  'User
         ScaleWidth      =   312
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   12
         Visible         =   0   'False
         Width           =   48
      End
      Begin MSComctlLib.TreeView tvFolders 
         Height          =   2868
         Left            =   24
         TabIndex        =   2
         Top             =   0
         Width           =   2796
         _ExtentX        =   4932
         _ExtentY        =   5059
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   127
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin MSComctlLib.ListView lvView 
         Height          =   2892
         Left            =   3312
         TabIndex        =   3
         Top             =   0
         Width           =   4560
         _ExtentX        =   8043
         _ExtentY        =   5101
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Image imgSplitter 
         Height          =   4608
         Left            =   2940
         MousePointer    =   9  'Size W E
         Top             =   0
         Width           =   48
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   264
      Left            =   0
      TabIndex        =   0
      Top             =   5940
      Width           =   9084
      _ExtentX        =   16023
      _ExtentY        =   466
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15536
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlTVFolders 
      Left            =   804
      Top             =   3828
      _ExtentX        =   804
      _ExtentY        =   804
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   255
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save Values"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeperator001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditNewMenu 
         Caption         =   "&New"
         Begin VB.Menu mnuEditNewKey 
            Caption         =   "&Key"
         End
         Begin VB.Menu mnuSeperator109 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEditNewValue 
            Caption         =   "&Value"
         End
      End
      Begin VB.Menu mnuSeperator100 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditDelete 
         Caption         =   "&Delete "
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuEditRename 
         Caption         =   "&Rename"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuSeperator101 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditModifyValueData 
         Caption         =   "&Modify Value Data"
      End
      Begin VB.Menu mnuSeperator102 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditFind 
         Caption         =   "&Find"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuEditFindNext 
         Caption         =   "Find Ne&xt"
         Enabled         =   0   'False
         Shortcut        =   {F3}
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuSynchronize 
         Caption         =   "&Synchronize"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator201 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupNewMenu 
         Caption         =   "New"
         Begin VB.Menu mnuPopupNewKey 
            Caption         =   "&Key"
         End
         Begin VB.Menu mnuSeperator300 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupNewValue 
            Caption         =   "&Value"
         End
      End
      Begin VB.Menu mnuPopupFind 
         Caption         =   "&Find"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeperator301 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupModify 
         Caption         =   "&Modify"
      End
      Begin VB.Menu mnuSeperator302 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuPopupRename 
         Caption         =   "&Rename"
      End
   End
End
Attribute VB_Name = "frmPreferencesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit

'Compontents: Microsoft Windows Common Controls 6.0 (SP 6)
'References: Micrsoft XML, v4.0

Private mDirty As Boolean
Private mNodeKey As String
Private mbMoving As Boolean
Const sglSplitLimit = 1000

Private Function AddKey() As Node
  Dim FolderHandle As String
  'Get the new folder from the server
  FolderHandle = Factory.PreferenceService.CreateFolder(SystemPreferences_TreeName, _
        IIf(tvFolders.SelectedItem.Key = SystemPreferences_RootKey, SystemPreferences_TreeName, tvFolders.SelectedItem.Key), _
        "New Folder", vbNullStringArray, vbNullStringArray)
  If FolderHandle = vbNullString Then
    Exit Function
  End If
  
  TreeRefresh
  Set AddKey = tvFolders.Nodes(FolderHandle)
End Function

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    mnuFileSave.Enabled = mDirty
  End If
End Property

'
' Enables and disables the menus
'
Private Sub MenuRefresh()
  Dim ItemsExist, IsLV As Boolean
  Select Case ActiveControl.Name
    Case tvFolders.Name
      ItemsExist = tvFolders.Nodes.Count > 0
    Case lvView.Name
      ItemsExist = lvView.ListItems.Count > 0
      IsLV = True
  End Select

  mnuEditModifyValueData.Enabled = ItemsExist
  mnuPopupModify.Enabled = ItemsExist And IsLV
  mnuEditRename.Enabled = ItemsExist
  mnuPopupRename.Enabled = ItemsExist
  mnuEditDelete.Enabled = ItemsExist
  mnuPopupDelete.Enabled = ItemsExist
End Sub

Private Sub FormRefresh()
  TreeRefresh
  NodeRefresh
  Dirty = False
End Sub

'
' Gets / sets the node key. This also loads the node's information
'

Private Property Get NodeKey() As String
  NodeKey = mNodeKey
End Property

Private Property Let NodeKey(vData As String)
  'check to insure that the node key is valid
  If tvFolders.Nodes(vData) Is Nothing Then
    Exit Property
  End If

  'set current node
  mNodeKey = vData
  sbStatusBar.Panels(1).Text = tvFolders.Nodes(NodeKey).FullPath
    
  NodeRefresh
End Property

Private Sub NodeRefresh()
  If NodeKey = vbNullString Then
    Exit Sub
  End If

  'Clear list
  lvView.ListItems.Clear
 
  If NodeKey = SystemPreferences_RootKey Then
    'ToDo: 2.30.0828-B
    Dim Col As New Collection
    Set Col = PreferenceUtil.getIniNodeCollection(SystemPreferences_TreeName)
    If Not Col Is Nothing Then
      Dim DataItem As DataField
      For Each DataItem In Col
        With lvView.ListItems.Add(, , DataItem.Name)
         .ListSubItems.Add , "data", DataItem.Value
        End With
      Next DataItem
    End If
  Else
    'Get values
    Dim map As MSXML2.IXMLDOMNode
    Set map = PreferenceUtil.GetNodeMap("systempreferences", NodeKey)
    If map Is Nothing Then
      Exit Sub
    End If
    'Load list
    Dim EntryList As IXMLDOMNodeList
    Dim Entry As IXMLDOMNode
    Set EntryList = map.selectNodes("entry")
    For Each Entry In EntryList
      With lvView.ListItems.Add(, , Entry.Attributes.getNamedItem("key").Text)
        .ListSubItems.Add , "data", Entry.Attributes.getNamedItem("value").Text
      End With
    Next
  End If
  Dirty = False
End Sub

Private Function Save(Optional Verify As Boolean) As Boolean
  If Not mDirty And Verify Then
    Save = True
    Exit Function
  End If
  
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  '
  'Save Stuff
  '
  ' Load items into an array
  Dim AttributeNames() As String
  Dim AttributeValues() As String
  
  Dim mItem As ListItem
  For Each mItem In lvView.ListItems
    AddParameter mItem.Text, mItem.ListSubItems("data"), AttributeNames, AttributeValues
  Next mItem
  
  ' call write
  If NodeKey = SystemPreferences_RootKey Then
    PreferenceUtil.writeIniNodeEntries SystemPreferences_TreeName, NodeKey, tvFolders.Nodes(NodeKey).Text, AttributeNames, AttributeValues
    Save = True
  Else
    Save = (Factory.PreferenceService.WriteFolder("systempreferences", NodeKey, AttributeNames, AttributeValues) <> vbNullString)
  End If
  
  Dirty = Not Save
  
End Function

Private Sub TreeRefresh()
  FolderUtil.TreeViewLoadSystemPreferences tvFolders
End Sub

Private Sub cboDataType_Change()
  Dirty = True
End Sub

Private Sub chkSearch_Click()
  Dirty = True
End Sub

Private Sub cmdRename_Click()
  Dim Original As String
  Original = lvView.SelectedItem.Text
  lvView.SetFocus
  lvView.StartLabelEdit
End Sub

Private Sub Form_Load()
  lvView.ColumnHeaders.Clear
  lvView.ColumnHeaders.Add , "name", "Name"
  lvView.ColumnHeaders.Add , "data", "Data"
  lvView.ColumnHeaders("data").Width = lvView.Width - lvView.ColumnHeaders("name").Width
  
  'load images for Tree View
  imlTVFolders.ListImages.Add 1, "TVFolderImage", PicDisplay.GetPicture(UrlName.TVFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 2, "TVOpenFolderImage", PicDisplay.GetPicture(UrlName.TVOPENFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 3, "LAWFolder", PicDisplay.GetPicture(UrlName.LAWFOLDER_IMAGE)
  tvFolders.ImageList = imlTVFolders
  
  TreeRefresh
  
  If Not tvFolders.Nodes.item(1) Is Nothing Then
    tvFolders_NodeClick tvFolders.Nodes.item(1)
    tvFolders.Nodes.item(1).Expanded = True
  End If
  
  picSplitter.Left = 2796
End Sub

Private Sub Form_Resize()
  picbody.Height = Max(ScaleHeight - picbody.Top - sbStatusBar.Height, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If Save(True) Then Unload Me
End Sub

'
' split resizer
'
Private Sub imgSplitter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  With imgSplitter
    picSplitter.Move .Left, .Top + picbody.Top, .Width, .Height - 20
  End With
  picSplitter.Visible = True
  mbMoving = True
End Sub

Private Sub imgSplitter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim sglPos As Single

  If mbMoving Then
    sglPos = X + imgSplitter.Left
    If sglPos < sglSplitLimit Then
      picSplitter.Left = sglSplitLimit
    ElseIf sglPos > Me.Width - sglSplitLimit Then
      picSplitter.Left = Me.Width - sglSplitLimit
    Else
      picSplitter.Left = sglPos
    End If
  End If
End Sub

Private Sub imgSplitter_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  imgSplitter.Left = picSplitter.Left
  picBody_Resize
End Sub

Private Sub lvview_DblClick()
  mnuEditModifyValueData_Click
End Sub

Private Sub lvView_GotFocus()
  MenuRefresh
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    PopupMenu mnuPopup, , , , mnuPopupModify
  End If
End Sub

Private Sub mnuEditDelete_Click()
  If ActiveControl Is Nothing Then Exit Sub
  Select Case ActiveControl.Name
    Case lvView.Name
      If Not lvView.SelectedItem Is Nothing Then
        lvView.ListItems.Remove lvView.SelectedItem.Index
        Dirty = True
        If lvView.ListItems.Count > 0 Then
          lvView.SelectedItem.Selected = True
        End If
      End If

    Case tvFolders.Name
      If MsgBox("Are you sure you want to delete this key?", vbYesNo) = vbYes Then
        Factory.PreferenceService.DeleteFolder "systemtree", tvFolders.SelectedItem.Key
        tvFolders.Nodes.Remove tvFolders.SelectedItem.Key
        NodeKey = tvFolders.SelectedItem.Key
      End If
  End Select

End Sub

Private Sub mnuEditModifyValueData_Click()
  If Not lvView.ListItems.Count > 0 Then
    Exit Sub
  End If

  Dim Temp As String
  Temp = InputBox("Value Name:  " & lvView.SelectedItem.Text & vbCrLf & vbCrLf & _
      "Value Data:", "Modify Value Data", lvView.SelectedItem.ListSubItems("data"))
  
  If Temp = "" Then
    Exit Sub
  End If
  
  lvView.SelectedItem.ListSubItems("data") = Temp
  Dirty = True
End Sub


Private Sub mnuEditNewKey_Click()
  AddKey
  MenuRefresh
End Sub

Private Sub mnuEditNewValue_Click()
  With lvView.ListItems.Add(, , "New Value")
    .ListSubItems.Add , "data", vbNullString
  End With
  Dirty = True
  MenuRefresh
End Sub

Private Sub mnuEditRename_Click()
  If ActiveControl Is Nothing Then Exit Sub
  Select Case ActiveControl.Name
    Case tvFolders.Name
      tvFolders.SetFocus
      tvFolders.StartLabelEdit
    Case lvView.Name
      lvView.SetFocus
      lvView.StartLabelEdit
  End Select
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDelete_Click
End Sub

Private Sub mnuPopupModify_Click()
  mnuEditModifyValueData_Click
End Sub

Private Sub mnuPopupNewKey_Click()
  mnuEditNewKey_Click
End Sub

Private Sub mnuPopupNewValue_Click()
  mnuEditNewValue_Click
End Sub

Private Sub mnuPopupRename_Click()
  mnuEditRename_Click
End Sub

Private Sub mnuSynchronize_Click()
  Save True
  If Factory.PreferenceService.Sync(SystemPreferences_TreeName) Then
    FormRefresh
  End If
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub picBody_Resize()
  imgSplitter.Height = picbody.Height
  
  tvFolders.Height = picbody.Height
  tvFolders.Left = 24
  tvFolders.Width = Max(imgSplitter.Left - 24, 0)
  
  lvView.Height = picbody.Height
  lvView.Left = imgSplitter.Left + imgSplitter.Width
  lvView.Width = Max(picbody.Width - lvView.Left - 24, 0)
  
  picSplitter.Visible = False
  mbMoving = False

End Sub

Private Sub tvFolders_AfterLabelEdit(Cancel As Integer, NewString As String)
  'If lvView.SelectedItem.Text <> NewString Then
  If Not DirNameSafe(NewString) Then
    Cancel = True
    Exit Sub
  End If
  
  If tvFolders.SelectedItem.Key = SystemPreferences_RootKey Then
    SystemPreferences_RootName = NewString
    PreferenceUtil.renameIniNode SystemPreferences_TreeName, SystemPreferences_RootName
  Else
    Cancel = Not Factory.FolderService.RenameFolder(tvFolders.SelectedItem.Key, NewString)
  End If
End Sub

Private Sub tvFolders_GotFocus()
  MenuRefresh
End Sub

Private Sub tvFolders_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    PopupMenu mnuPopup, , , , mnuPopupRename
  End If
End Sub

Private Sub tvFolders_NodeClick(ByVal Node As MSComctlLib.Node)
  If Node.Key = NodeKey Then
    Exit Sub
  End If

  If Not Save(True) Then
    'make no changes, leave as is
    tvFolders.Nodes(mNodeKey).Selected = True
    Exit Sub
  End If
  
  NodeKey = Node.Key
  
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dirty = True
End Sub

Private Sub txtFolderName_Change()
  Dirty = True
End Sub
