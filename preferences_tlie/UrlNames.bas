Attribute VB_Name = "UrlName"
' ver 2.30.0828-B (jjm)
' ver 2.30.0622 (jjm)
' ver 2.10.0421 (jjm)

'
' url Names
'

'
' This is a constant library for strings and other items
' use in some form of location identifing. Ex: file name,
' a section name in an ini file, ect...
'


'
' Servlet Parameters.
' used with http://localhost:8080/malcolm/servlet/%SERVLET%&%PARAMETER%
' example: http://localhost:8080/malcolm/servlet/claimopen?key=01-0001

'ToDo: Not for sure if Application open is functioning...
Public Const APPLICATIONOPEN_SERVLET = "applicationopen?key="

Public Const CLAIMOPEN_SERVLET = "claimopen?key=" '[claimId] ex: 01-0001
Public Const CLAIMRESERVEOPEN_SERVLET = "claimreserveopen?key=" '[claimReserveId] ex:CR1001808

Public Const LAWYEROPEN_SERVLET = "lawyeropen?key=" '[lawyerId] ex:BUNNBUG_
Public Const LAWYERPOLICYOPEN_SERVLET = "lawyerpolicyopen?key=" '[lawyerId]&policyid=[policyId]   ex:BUNNBUG_&policyid=0303001

Public Const FIRMOPEN_SERVLET = "firmopen?key=" '[firmId] ex: DOEJOH

Public Const POLICYOPEN_SERVLET = "policyopen?key="



'
' INI File Sections
'
Public Const BARCODE_INI = "BarCode"

Public Const CONFDEFAULTPATH = "ConfDefaultPath"
Public Const CONFPERSONALPATH = "ConfPersonalPath"
Public Const CURRENTX_INI = "CurrentX"
Public Const CURRENTY_INI = "CurrentY"

Public Const DEFAULTTOFIRMPOLICYCONTACT_INI = "DefaultToFirmPolicyContact"

Public Const ENTERASTAB_INI = "EnterAsTab"
Public Const ENABLEWEBCONTENT_INI = "EnableWebContent"
Public Const ENVELOPE_INI = "Envelope"
Public Const ENVELOPEPRINTER_INI = "EnvelopePrinter"

Public Const FOLDER_INI = "Folder"
Public Const FOLDERKEY_INI = "FolderKey"
Public Const FONTNAME_INI = "FontName"
Public Const FONTSIZE_INI = "FontSize"
Public Const FONTBOLD_INI = "FontBold"
Public Const FONTITALIC_INI = "FontItalic"
Public Const FONTUNDERLINE_INI = "FontUnderline"
Public Const FONTSTRIKETHROUGH_INI = "FontStrikethrough"
Public Const FORECOLOR_INI = "ForeColor"
Public Const FRMMAIN_INI = "frmMain"

Public Const HOME_INI = "home"

Public Const IMAGEFOLDERPATH_INI = "ImageFolderPath"
Public Const INIFOLDER_INI = "IniFolder"
Public Const INIFORM_INI = "IniForm"
Public Const ITEMSPERVIEW_INI = "ItemsPerView"

Public Const MAIN_INI = "Main"
Public Const MISC_INI = "Misc"

Public Const ORGANIZATIONLONG_INI = "OrganizationLong"
Public Const ORGANIZATIONSHORT_INI = "OrganizationShort"

Public Const PAPERSIZE_INI = "PaperSize"
Public Const PROFILELIST_INI = "ProfileList"

Public Const REMEMBERLASTSEARCH_INI = "RememberLastSearch"

Public Const SERVERLIST_INI = "ServerList"
Public Const SYSTEMFOLDERS_TREENAME_INI = "SystemFoldersTreeName"
Public Const SYSTEMPREFERENCES_TREENAME_INI = "SystemPreferencesTreeName"

Public Const TEXTSELECTONFOCUS_INI = "TextSelectOnFocus"
Public Const TREE_INI = "Tree"

'
' Image file names
'

Public Const BACK_IMAGE = "Back.bmp"
Public Const BACKDISABLED_IMAGE = "BackDisabled.bmp"

Public Const DOWN_IMAGE = "Down.bmp"
Public Const DOWNDISABLED_IMAGE = "DownDisabled.bmp"

Public Const FORWARD_IMAGE = "Forward.bmp"
Public Const FORWARDDISABLED_IMAGE = "ForwardDisabled.bmp"

Public Const LISTVIEW_IMAGE = "ListView.bmp"

Public Const TVFOLDER_IMAGE = "TVFolder.bmp"
Public Const TVOPENFOLDER_IMAGE = "TVOpenFolder.bmp"

Public Const UP_IMAGE = "Up.bmp"
Public Const UPDISABLED_IMAGE = "UpDisabled.bmp"
Public Const UPONELEVEL_IMAGE = "UpOneLevel.bmp"
Public Const UPONELEVELDISABLED_IMAGE = "UpOneLevelDisabled.bmp"




  
