VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "XQueryDialect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function Column(Args As Variant) As String
  If TypeOf Args Is ColumnHeaders Then
    Dim List As String
    Dim Header As Variant
    For Each Header In Args
      If Header.Key <> "" And Left(Header.Key, 1) <> "~" Then
        List = List & IIf(List = vbNullString, vbNullString, ",") & Header.Key
      End If
    Next Header
    Column = List
    Exit Function
  End If
  MsgBox "Column does not know how to handle: " & Args
End Function

'ToDo: Update to XQuery syntax
Public Function Sort(Args As Variant) As String
  If TypeOf Args Is ListView Then
    If Args.ColumnHeaders.Count > 0 Then
      Sort = """" & Args.ColumnHeaders(Args.SortKey + 1).Key & _
          """" & IIf(Args.SortOrder = 1, " DESC", "")
    End If
    Exit Function
  End If
  MsgBox "Sort does not know how to handle: " & Args
End Function


'
' Return two digit month of field in select clause
'
Public Function Month(Column As String) As String
  Month = "{fn month(""" & Column & """)}"
End Function


Public Function Concat(Param1 As String, Param2 As String) As String
  Concat = "concat('" & Param1 & "','" & Param2 & "')"
End Function

Public Function Contains(Column As String, SubStr As String) As String
  Contains = "contains(" & Column & ",'" & SubStr & "')"
End Function

'Creates the xquery filter for a contains function using the upper case function
Public Function ContainsUpperCase(Words As String, Field As String)
   ContainsUpperCase = LawDialect.Contains(LawDialect.UpperCase(Field), UCase(Words))
End Function

' The following two function takes a string, splits it to an array,
' and creates a contains string for each item in the array. The String
' is split by spaces
Public Function ContainsAndArray(Words As String, Field As String)
  Dim ArrStr() As String, Result As String
  ArrStr = Split(Words, " ")
  Dim I As Integer
  For I = 0 To SafeUBound(ArrStr)
    Result = IIf(Result = vbNullString, vbNullString, Result & " and ") & _
      LawDialect.ContainsUpperCase(ArrStr(I), Field)
  Next I
End Function

Public Function ContainsOrArray(Words As String, Field As String)
  Dim ArrStr() As String, Result As String
  ArrStr = Split(Words, " ")
  Dim I As Integer
  For I = 0 To SafeUBound(ArrStr)
    Result = IIf(Result = vbNullString, vbNullString, Result & " or ") & _
      LawDialect.ContainsUpperCase(ArrStr(I), Field)
  Next I
End Function

Public Function StartsWith(Column As String, SubStr As String) As String
  StartsWith = "starts-with(" & Column & ",'" & SubStr & "')"
End Function


Public Function UpperCase(Column As String) As String
  UpperCase = "upper-case(" & Column & ")"
End Function


Public Function DateFormat(DateVal As Date) As String
  DateFormat = "date(" & Format(DateVal, "yyyy-mm-dd") & ")"
End Function


