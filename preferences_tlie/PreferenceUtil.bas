Attribute VB_Name = "PreferenceUtil"
Option Explicit

'Util Module to help the PreferenceService
'This util relies on an existing Factory.PreferenceService object existing

'------------------------------------------'
'  This is the xml format for the map node '
'------------------------------------------'
'
'  <node name="folderId">
'    <map>
'      <entry key="url" value=""/>
'    </map>
'  </node>


'
' Service Helpers
'
'
'
' Add a new workflow folder
'
Public Function AddFolder(Tree As TreeView) As Node
  Dim FolderHandle As String

  FolderHandle = Factory.PreferenceService.CreateFolder(SystemFolders_TreeName, _
        IIf(Tree.SelectedItem.Key = SystemFolders_RootKey, SystemFolders_TreeName, Tree.SelectedItem.Key), _
        "New Folder", vbNullStringArray, vbNullStringArray)
  If FolderHandle = vbNullString Then
    Exit Function
  End If


  Dim AttributeNames(0) As String
  Dim AttributeValues(0) As String
  AttributeNames(0) = "url"
  AttributeValues(0) = "../module/modulecheckevent.list?qFor=EventView[folderId='" & _
                       FolderHandle & "' and  eventStatus='Pending']&max=10"

  If (Factory.PreferenceService.WriteFolder(SystemFolders_TreeName, FolderHandle, AttributeNames, AttributeValues) = vbNullString) Then
    'Alert the user if default settings failed
    MsgBox "Folder created but setting the default setting failed.", vbCritical, "Error"
  End If

  Dim nodeX As Node

  Set nodeX = Tree.Nodes.Add(Tree.SelectedItem, tvwChild, FolderHandle, "New Folder", _
      "TVFolderImage", "TVOpenFolderImage")
  nodeX.ExpandedImage = "TVOpenFolderImage"

  nodeX.Selected = True
  Set AddFolder = nodeX
End Function

'
' Remove the selected folder.
'
Public Sub RemoveFolder(Tree As TreeView)
  If Not MsgBox("Are you sure you want to remove the folder '" & Tree.SelectedItem.FullPath & "'?", vbYesNo + vbQuestion, "Confirm Folder Delete") = vbYes Then
    Exit Sub
  End If
  If Factory.PreferenceService.DeleteFolder(SystemFolders_TreeName, Tree.SelectedItem.Key) Then
    Tree.Nodes.Remove Tree.SelectedItem.Index
  End If
End Sub


Public Function GetNodeMap(Treename As String, NodeKey As String) As MSXML2.IXMLDOMNode
  
  Dim OpenURL As String
  Select Case Treename
    Case SystemFolders_TreeName
      OpenURL = "folderopen"
    Case SystemPreferences_TreeName
      OpenURL = "preferenceopen"
    Case Else
      MsgBox "Error:Unknown tree name: " & Treename
      Exit Function
  End Select
  
  Set GetNodeMap = XMLUtil.HttpGet(HttpServer & WebApplication & WebUtil & OpenURL & "?" & "node=" & NodeKey & "&source=text/xml" & "&" & NoCache).responseXML.selectSingleNode("map")
  'Browse HttpServer & WebApplication & WebUtil & "folderopen?key=" & TreeName & "&node=" & NodeKey & "&source=text/xml"
End Function


'Plan to change to fill in a data type collection, then can use the set fields
Public Function getMapEntryCollection(Treename As String, FolderKey As String) As Collection

  Dim map As MSXML2.IXMLDOMNode
  Dim Entries As MSXML2.IXMLDOMNodeList
  Dim EntriesCollection As New Collection
  
  Set map = GetNodeMap(Treename, FolderKey)
  If map Is Nothing Then
    Exit Function
  End If
  Set Entries = map.selectNodes("entry")

  Dim Entry As MSXML2.IXMLDOMNode
  For Each Entry In Entries
    EntriesCollection.Add CreateDataField("entry", Entry.Attributes.getNamedItem("key").Text, Entry.Attributes.getNamedItem("value").Text), Entry.Attributes.getNamedItem("key").Text
  Next Entry

  Set getMapEntryCollection = EntriesCollection
End Function

Public Function getMapEntryValue(Node As IXMLDOMNode, EntryKey As String) As String
  If Node Is Nothing Then
    Exit Function
  End If
  
  Dim EntryList As IXMLDOMNodeList
  Dim Entry As IXMLDOMNode
  Set EntryList = Node.selectNodes("entry")
  For Each Entry In EntryList
    If Entry.Attributes.getNamedItem("key").Text = EntryKey Then
      getMapEntryValue = Entry.Attributes.getNamedItem("value").Text
      Exit Function
    End If
  Next
End Function

'
' These functions deal with saving and retrieving data for the root
' folders that are saved locally
'

'---------------------------------------'
' LAW.ini  (Move to folder.ini)         '
'---------------------------------------'
'[main]
'SystemFoldersTreeName = systemtreename
'[systemtreename]
'handle = "something"
'name = "somethingelse"
'entry.0.key="name"
'entry.0.value="value"
'entry.1.key="name"
'entry.1.value="value"

Public Function getIniNodeCollection(Treename As String) As Collection
  Dim Col As New Collection
  Dim I As Integer
  Dim Key As String
  For I = 0 To 100
    Key = IniFolder.ReadFile(Treename, "entry." & I & ".key")
    If Key = vbNullString Then
      Exit For
    End If
    Col.Add CreateDataField("entry", Key, IniFolder.ReadFile(Treename, "entry." & I & ".value")), Key
  Next I
  Set getIniNodeCollection = Col
End Function


'
' ToDo: Change the ini used from law.ini to folder.ini
'
Public Function writeIniNodeEntries(Treename As String, NodeHandle As String, NodeName As String, _
    ByRef AttributeNames() As String, ByRef AttributeValues() As String) As Boolean
  IniFolder.RemoveSection Treename
  IniFolder.WriteFile Treename, "handle", NodeHandle
  IniFolder.WriteFile Treename, "name", NodeName
  Dim I As Integer
  For I = 0 To SafeUBound(AttributeValues)
    IniFolder.WriteFile Treename, "entry." & I & ".key", AttributeNames(I)
    IniFolder.WriteFile Treename, "entry." & I & ".value", AttributeValues(I)
  Next I
  writeIniNodeEntries = True
End Function

Public Sub renameIniNode(Treename As String, NodeName As String)
  IniFolder.WriteFile Treename, "name", NodeName
End Sub
