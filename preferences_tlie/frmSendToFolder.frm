VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmFolderBrowseDialog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Browse For Folder"
   ClientHeight    =   5328
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   4992
   Icon            =   "frmSendToFolder.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5328
   ScaleWidth      =   4992
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picFolder 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4356
      Left            =   0
      ScaleHeight     =   4356
      ScaleWidth      =   4992
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   0
      Width           =   4992
      Begin MSComctlLib.TreeView tvFolder 
         Height          =   3732
         Left            =   60
         TabIndex        =   0
         Top             =   612
         Width           =   4884
         _ExtentX        =   8615
         _ExtentY        =   6583
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   494
         LabelEdit       =   1
         PathSeparator   =   "/"
         Style           =   7
         BorderStyle     =   1
         Appearance      =   1
      End
      Begin VB.Label lblDescription 
         AutoSize        =   -1  'True
         Caption         =   "Select the desired folder:"
         Height          =   192
         Left            =   156
         TabIndex        =   8
         Top             =   144
         Width           =   1776
      End
   End
   Begin VB.PictureBox picEvent 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   492
      Left            =   0
      ScaleHeight     =   492
      ScaleWidth      =   4992
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   4356
      Visible         =   0   'False
      Width           =   4992
      Begin VB.ComboBox cboEvent 
         Height          =   288
         Left            =   1008
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Tag             =   "FolderBrowseDialogEvent"
         Top             =   60
         Width           =   3636
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Event"
         Height          =   192
         Left            =   144
         TabIndex        =   6
         Top             =   120
         Width           =   408
      End
   End
   Begin VB.PictureBox picOkCancel 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   468
      Left            =   0
      ScaleHeight     =   468
      ScaleWidth      =   4992
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   4848
      Width           =   4992
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   336
         Left            =   2220
         TabIndex        =   2
         Top             =   36
         Width           =   1200
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   336
         Left            =   3480
         TabIndex        =   4
         Top             =   36
         Width           =   1200
      End
      Begin MSComctlLib.ImageList imlTVFolders 
         Left            =   192
         Top             =   0
         _ExtentX        =   804
         _ExtentY        =   804
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   255
         _Version        =   393216
      End
   End
End
Attribute VB_Name = "frmFolderBrowseDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'ver 2.30-B 2.30.1017 (jjm) core
'Previous ver 1.93.0221 (jjm) Core
Option Explicit

Private mEventTypes As New Collection
Private EventType As Integer


Private mOk As Boolean
Private mGroup As String
Private mKey As String
Private mPath As String
Private mEventName As String


'
' General Methods
'

Public Property Get EventTypeId() As String
  EventTypeId = mEventTypes(cboEvent.ListIndex + 1)(DatabaseName.EVENTTYPE_KEY)
End Property

Public Property Get FolderKey() As String
  FolderKey = tvFolder.SelectedItem.Key
End Property

Public Property Get FolderPath() As String
  Dim Path As String
  Path = tvFolder.SelectedItem.FullPath
  FolderPath = Mid$(Path, InStr(Path, "/") + 1)
End Property

Public Function ShowGoTo(Optional Key As String, Optional Path As String) As Boolean
  mKey = Key
  mPath = Path
  Load Me
  lblDescription = "Go to the folder:"
  Show vbModal
  ShowGoTo = mOk
End Function

Public Function ShowMove(Optional Key As String, Optional Path As String) As Boolean
  mKey = Key
  mPath = Path
  Load Me
  lblDescription = "Move the selected item(s) to the folder:"
  Show vbModal
  ShowMove = mOk
End Function

Public Function ShowSelect(Optional Key As String, Optional Path As String) As Boolean
  mKey = Key
  mPath = Path
  Load Me
  lblDescription = "Select the desired folder:"
  Show vbModal
  ShowSelect = mOk
End Function

Public Function ShowSendTo(Group As String, Optional Key As String, Optional Path As String) As Boolean
  mKey = Key
  mPath = Path
  mGroup = Group
  Load Me
  lblDescription = "Send the selected item(s) to the folder:"
  Show vbModal
  ShowSendTo = mOk
  If mOk Then
    WriteIniFile Name, "Path", FolderPath
  End If
End Function
'
' End General Methods
'
'
'
' Begin Control Methods
'


Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Sub cmdOk_Click()
  'ToDo: Beta 2.30B fix - modify the statement below to get the map value
  ' instead of using a tag

  'ver 1.93.0221 (jjm)
  If mGroup <> vbNullString And _
      PreferenceUtil.getMapEntryValue(PreferenceUtil.GetNodeMap(SystemFolders_TreeName, tvFolder.SelectedItem.Key), "datatype") <> DatabaseName.EVENT_TABLE Then
      
    MsgBox "Can only send to workflow folders", vbInformation
    Exit Sub
  End If
  mOk = True
  'ver 2.30.1027 (jjm)
  FormUtil.SetComboDefault cboEvent, mGroup
  
  Me.Hide
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
    FormUtil.ShowHelp , Me.hWnd
  End If
End Sub

Private Sub Form_Load()
  ' A group is sent when we wish to send with events.
  'If mGroup <> vbNullString Then
    'Set mEventTypes = Factory.EventTypeService.GroupCollection(mGroup)
   ' FormUtil.ComboBoxLoad mEventTypes, cboEvent, "eventTypeName"
    'picEvent.Visible = True
    'If cboEvent.ListCount > 0 Then
    '  cboEvent.ListIndex = 0
    'End If
  'End If
  'FormUtil.GetComboDefault cboEvent, mGroup

  'load images for Tree View
  imlTVFolders.ListImages.Add 1, "TVFolderImage", PicDisplay.GetPicture(UrlName.TVFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 2, "TVOpenFolderImage", PicDisplay.GetPicture(UrlName.TVOPENFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 3, "LAWFolder", PicDisplay.GetPicture(UrlName.LAWFOLDER_IMAGE)

  tvFolder.ImageList = imlTVFolders
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolder
  'FolderUtil.TreeViewLoadFolder tvFolder, systemfolders_treename
  
  ' Try to assign mKey from path if no mKey defined.
  If mKey = vbNullString Then
    If mPath = vbNullString Then
      mPath = ReadIniFile(Name, "Path")
    End If
    mKey = Factory.FolderService.FindFolder(SystemFolders_TreeName, mPath)
  End If
  
  ' If mkey, then open.
  If mKey <> vbNullString And Contains(tvFolder.Nodes, mKey) Then
    tvFolder.Nodes(mKey).Selected = True
    tvFolder.SelectedItem.Expanded = True
  End If
  
  Height = picOkCancel.Top + picOkCancel.Height + (Height - ScaleHeight)
End Sub

