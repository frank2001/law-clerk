VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFolderPropertiesEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Folder Properties Edit"
   ClientHeight    =   7560
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   9345
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   9345
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picbody 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   7560
      Left            =   0
      ScaleHeight     =   7560
      ScaleWidth      =   9345
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   0
      Width           =   9345
      Begin TabDlg.SSTab SSTab1 
         Height          =   6900
         Left            =   3120
         TabIndex        =   1
         Top             =   105
         Width           =   6090
         _ExtentX        =   10742
         _ExtentY        =   12171
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   420
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "frmFolderPropertiesEdit.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFolder"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraNote"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Advanced"
         TabPicture(1)   =   "frmFolderPropertiesEdit.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvView"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.Frame fraNote 
            Caption         =   "Note"
            Height          =   2025
            Left            =   120
            TabIndex        =   14
            Top             =   2190
            Width           =   5850
            Begin VB.TextBox txtDataField 
               DataField       =   "note"
               Height          =   1620
               Index           =   7
               Left            =   120
               MultiLine       =   -1  'True
               TabIndex        =   2
               Top             =   264
               Width           =   5610
            End
         End
         Begin VB.Frame fraFolder 
            Caption         =   "Folder"
            Height          =   1785
            Left            =   120
            TabIndex        =   7
            Top             =   312
            Width           =   5850
            Begin VB.TextBox txtDataField 
               DataField       =   "url"
               Height          =   288
               Index           =   3
               Left            =   144
               TabIndex        =   15
               Text            =   "data"
               Top             =   1365
               Width           =   5595
            End
            Begin VB.TextBox txtLocation 
               BorderStyle     =   0  'None
               Height          =   390
               Left            =   984
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   8
               Text            =   "frmFolderPropertiesEdit.frx":0038
               Top             =   696
               Width           =   4770
            End
            Begin VB.Label lblURL 
               AutoSize        =   -1  'True
               Caption         =   "URL:"
               Height          =   195
               Left            =   150
               TabIndex        =   16
               Top             =   1110
               Width           =   375
            End
            Begin VB.Label lblLocation 
               AutoSize        =   -1  'True
               Caption         =   "Location:"
               Height          =   192
               Left            =   144
               TabIndex        =   13
               Top             =   696
               Width           =   648
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "Id"
               DataField       =   "folderId"
               Height          =   192
               Index           =   0
               Left            =   984
               TabIndex        =   12
               Top             =   228
               Width           =   4176
               WordWrap        =   -1  'True
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               Height          =   192
               Index           =   1
               Left            =   144
               TabIndex        =   11
               Top             =   456
               Width           =   480
            End
            Begin VB.Label lblLabel 
               AutoSize        =   -1  'True
               Caption         =   "Id:"
               Height          =   192
               Index           =   5
               Left            =   132
               TabIndex        =   10
               Top             =   228
               Width           =   168
            End
            Begin VB.Label lblDataField 
               AutoSize        =   -1  'True
               Caption         =   "Name"
               DataField       =   "name"
               Height          =   192
               Index           =   1
               Left            =   996
               TabIndex        =   9
               Top             =   456
               Width           =   4176
               WordWrap        =   -1  'True
            End
         End
         Begin MSComctlLib.ListView lvView 
            Height          =   6405
            Left            =   -74910
            TabIndex        =   3
            Top             =   375
            Width           =   5895
            _ExtentX        =   10398
            _ExtentY        =   11298
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "Apply"
         Height          =   336
         Left            =   6750
         TabIndex        =   4
         Top             =   7095
         Width           =   1200
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "&Close"
         Height          =   336
         Left            =   8010
         TabIndex        =   5
         Top             =   7095
         Width           =   1200
      End
      Begin MSComctlLib.TreeView tvFolders 
         Height          =   6975
         Left            =   105
         TabIndex        =   0
         Top             =   135
         Width           =   2925
         _ExtentX        =   5159
         _ExtentY        =   12303
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   127
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList imlTVFolders 
         Left            =   1830
         Top             =   6705
         _ExtentX        =   794
         _ExtentY        =   794
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   255
         _Version        =   393216
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save Values"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSeperator001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditNewMenu 
         Caption         =   "Ne&w"
         Begin VB.Menu mnuEditNewKey 
            Caption         =   "&Folder"
            Shortcut        =   ^Y
         End
         Begin VB.Menu mnuSeperator109 
            Caption         =   "-"
         End
         Begin VB.Menu mnuEditNewValue 
            Caption         =   "&Value"
            Shortcut        =   ^U
         End
      End
      Begin VB.Menu mnuSeperator100 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditDelete 
         Caption         =   "&Delete "
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuEditRename 
         Caption         =   "&Rename"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuSeperator101 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditModifyValueData 
         Caption         =   "&Modify Value Data"
      End
      Begin VB.Menu mnuSeperator102 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditFind 
         Caption         =   "&Find"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuEditFindNext 
         Caption         =   "Find Ne&xt"
         Enabled         =   0   'False
         Shortcut        =   {F3}
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsOrganizeFolders 
         Caption         =   "&Organize Folders..."
      End
      Begin VB.Menu mnuSeperator069 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSynchronize 
         Caption         =   "&Synchronize"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuSeperator201 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About LAW Clerk..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupNewMenu 
         Caption         =   "Ne&w"
         Begin VB.Menu mnuPopupNewKey 
            Caption         =   "&Folder"
         End
         Begin VB.Menu mnuSeperator300 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupNewValue 
            Caption         =   "&Value"
         End
      End
      Begin VB.Menu mnuPopupFind 
         Caption         =   "&Find"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSeperator301 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupModify 
         Caption         =   "&Modify"
      End
      Begin VB.Menu mnuSeperator302 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuPopupRename 
         Caption         =   "&Rename"
      End
      Begin VB.Menu mnuSeperator070 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopupOrganizeFolders 
         Caption         =   "&Organize Folders"
      End
   End
End
Attribute VB_Name = "frmFolderPropertiesEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Compontents: Microsoft Windows Common Controls 6.0 (SP 6)
'References: Micrsoft XML, v4.0

Private mDirty As Boolean
Private mNodeKey As String

Private Function GetListIndex(DataField As String, Items As ListItems) As Integer
  GetListIndex = -1
  Dim item As ListItem
  For Each item In Items
    If item.Text = DataField Then
      GetListIndex = item.Index
      Exit Function
    End If
  Next item
End Function

Private Function AddKey() As Node
  Set AddKey = PreferenceUtil.AddFolder(tvFolders)
  
  'Dim FolderHandle As String
  'Get the new folder from the server
  'FolderHandle = Factory.PreferenceService.CreateFolder(SystemFolders_TreeName, _
  '      IIf(tvFolders.SelectedItem.Key = SystemFolders_RootKey, SystemFolders_TreeName, tvFolders.SelectedItem.Key), _
  '      "New Folder", vbNullStringArray, vbNullStringArray)
  'If FolderHandle = vbNullString Then
  '  Exit Function
  'End If
  
  'TreeRefresh
  'Set AddKey = tvFolders.Nodes(FolderHandle)
End Function

'
' Track whether or not data has been changed
'
Private Property Let Dirty(vData As Boolean)
  If mDirty <> vData Then
    mDirty = vData
    cmdApply.Enabled = mDirty
    FormUtil.Dirty Me, mDirty
  End If
End Property

Private Sub FormRefresh()
  TreeRefresh
  NodeRefresh
  Dirty = False
End Sub

'
' Enables and disables the menus
'
Private Sub MenuRefresh()
  Dim ItemsExist, IsLV As Boolean
  Select Case ActiveControl.Name
    Case tvFolders.Name
      ItemsExist = tvFolders.Nodes.Count > 0
    Case lvView.Name
      ItemsExist = lvView.ListItems.Count > 0
      IsLV = True
  End Select

  mnuEditModifyValueData.Enabled = ItemsExist
  mnuPopupModify.Enabled = ItemsExist And IsLV
  mnuEditRename.Enabled = ItemsExist
  mnuPopupRename.Enabled = ItemsExist
  mnuEditDelete.Enabled = ItemsExist
  mnuPopupDelete.Enabled = ItemsExist
End Sub


'
' Gets / sets the node key. This also loads the node's information
'
Private Property Get NodeKey() As String
  NodeKey = mNodeKey
End Property

Private Property Let NodeKey(vData As String)
  'check to insure that the node key is valid
  If tvFolders.Nodes(vData) Is Nothing Then
    Exit Property
  End If

  'set current node
  mNodeKey = vData
  tvFolders.SelectedItem = tvFolders.Nodes(NodeKey)
    
  NodeRefresh
End Property

Private Function Save(Optional Verify As Boolean) As Boolean
  If Not mDirty And Verify Then
    Save = True
    Exit Function
  End If
    
  ' If last control doesn't validate.
  If Not FormUtil.ValidateControls(Me) Then
    Save = False
    Exit Function
  End If
  
  ' If we were just calling to verify.
  If Verify Then
    Select Case MsgBox("Do you want to save the changes you made?", vbYesNoCancel + vbExclamation)
      ' Consider saved if we do not want to save changes.
      Case vbNo
        Save = True
        Exit Function
      ' Not saved if user cancels.
      Case vbCancel
        Save = False
        Exit Function
    End Select
  End If

  '
  'Save Stuff
  '
  ' Load items into an array
  Dim AttributeNames() As String
  Dim AttributeValues() As String

  Dim mItem As ListItem
  For Each mItem In lvView.ListItems
    AddParameter mItem.Text, mItem.ListSubItems("data"), AttributeNames, AttributeValues
  Next mItem

  ' call write
  If NodeKey = SystemFolders_RootKey Then
    PreferenceUtil.writeIniNodeEntries SystemFolders_TreeName, NodeKey, tvFolders.Nodes(NodeKey).Text, AttributeNames, AttributeValues
    Save = True
  Else
    Save = (Factory.PreferenceService.WriteFolder(SystemFolders_TreeName, NodeKey, AttributeNames, AttributeValues) <> vbNullString)
  End If
  
  Dirty = Not Save

End Function

Private Sub NodeRefresh()
  If NodeKey = vbNullString Then
    Exit Sub
  End If

  'Clear list
  lvView.ListItems.Clear
 
  If NodeKey = SystemFolders_RootKey Then
    'ToDo: 2.30.0828-B
    txtLocation.Text = "/" & SystemFolders_RootName
    Dim Col As New Collection
    Set Col = PreferenceUtil.getIniNodeCollection(SystemFolders_TreeName)
    If Not Col Is Nothing Then
      Dim DataItem As DataField
      For Each DataItem In Col
        With lvView.ListItems.Add(, , DataItem.Name)
         .ListSubItems.Add , "data", DataItem.Value
        End With
      Next DataItem
    End If
  Else
    txtLocation.Text = Factory.PreferenceService.GetPath(SystemFolders_TreeName, NodeKey)
    'Get values
    Dim map As MSXML2.IXMLDOMNode
    Set map = PreferenceUtil.GetNodeMap(SystemFolders_TreeName, NodeKey)
    If map Is Nothing Then
      Exit Sub
    End If
    'Load list
    Dim EntryList As IXMLDOMNodeList
    Dim Entry As IXMLDOMNode
    Set EntryList = map.selectNodes("entry")
    For Each Entry In EntryList
      With lvView.ListItems.Add(, , Entry.Attributes.getNamedItem("key").Text)
        .ListSubItems.Add , "data", Entry.Attributes.getNamedItem("value").Text
      End With
    Next
  End If
  
  'Build a collection of values from the list view
  Dim Datafields As New Collection
  Dim mItem As ListItem
  For Each mItem In lvView.ListItems
    Datafields.Add CreateDataField("entry", mItem.Text, mItem.ListSubItems("data")), mItem.Text
  Next mItem
  
  'Load the values
  lblDataField(0).Caption = NodeKey
  lblDataField(1).Caption = tvFolders.Nodes(NodeKey).Text
  SetFields txtDataField, Datafields
  
  Dirty = False
End Sub

Public Sub ShowFolder(FolderKey As String)
  If Contains(tvFolders.Nodes, FolderKey) Then
    NodeKey = FolderKey
  End If
  Show
End Sub

Private Sub TreeRefresh()
  
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  'ver 2.30.0826-B (jjm)
  If Contains(tvFolders.Nodes, NodeKey) Then
    tvFolders.SelectedItem = tvFolders.Nodes(NodeKey)
  End If

End Sub

Private Sub cboDataType_Change()
  Dirty = True
End Sub

Private Sub chkSearch_Click()
  Dirty = True
End Sub

Private Sub cmdApply_Click()
  Save
End Sub

Private Sub cmdClose_Click()
  mnuFileExit_Click
End Sub


Private Sub Form_Load()
  
  lvView.ColumnHeaders.Clear
  lvView.ColumnHeaders.Add , "name", "Name"
  lvView.ColumnHeaders.Add , "data", "Data"
  lvView.ColumnHeaders("data").Width = lvView.Width - lvView.ColumnHeaders("name").Width

  'load images for Tree View
  imlTVFolders.ListImages.Add 1, "TVFolderImage", PicDisplay.GetPicture(UrlName.TVFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 2, "TVOpenFolderImage", PicDisplay.GetPicture(UrlName.TVOPENFOLDER_IMAGE)
  imlTVFolders.ListImages.Add 3, "LAWFolder", PicDisplay.GetPicture(UrlName.LAWFOLDER_IMAGE)
  tvFolders.ImageList = imlTVFolders
    
  txtLocation.BackColor = lblLocation.BackColor
  FormUtil.TabControlEnable SSTab1
    
  TreeRefresh
  
  'Set a nodekey if there is none
  If Not tvFolders.Nodes.item(1) Is Nothing And NodeKey = vbNullString Then
    NodeKey = tvFolders.Nodes.item(1).Key
    tvFolders.Nodes.item(1).Expanded = True
  End If

End Sub

Private Sub lvview_DblClick()
  mnuEditModifyValueData_Click
End Sub

Private Sub lvView_GotFocus()
  MenuRefresh
End Sub

Private Sub lvView_KeyDown(KeyCode As Integer, Shift As Integer)
  'ver 1.01 (jjm) 2004-04-30
  If KeyCode = vbKeyReturn Then
    mnuEditModifyValueData_Click
  End If
End Sub

Private Sub lvView_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    PopupMenu mnuPopup, , , , mnuPopupModify
  End If
End Sub

Private Sub mnuEditDelete_Click()
  If ActiveControl Is Nothing Then Exit Sub
  Select Case ActiveControl.Name
    Case lvView.Name
      If Not lvView.SelectedItem Is Nothing Then
        lvView.ListItems.Remove lvView.SelectedItem.Index
        Dirty = True
        If lvView.ListItems.Count > 0 Then
          lvView.SelectedItem.Selected = True
        End If
      End If

    Case tvFolders.Name
      If MsgBox("Are you sure you want to delete this key?", vbYesNo) = vbYes Then
        If Factory.PreferenceService.DeleteFolder(SystemFolders_TreeName, tvFolders.SelectedItem.Key) Then
          tvFolders.Nodes.Remove tvFolders.SelectedItem.Key
          NodeKey = tvFolders.SelectedItem.Key
        End If
      End If
  End Select

End Sub

Private Sub mnuEditModifyValueData_Click()
  If Not lvView.ListItems.Count > 0 Then
    Exit Sub
  End If

  Dim Temp As String
  Temp = InputBox("Value Name:  " & lvView.SelectedItem.Text & vbCrLf & vbCrLf & _
      "Value Data:", "Modify Value Data", lvView.SelectedItem.ListSubItems("data"))
  
  If Temp = "" Then
    Exit Sub
  End If
  
  lvView.SelectedItem.ListSubItems("data") = Temp
  Dirty = True
End Sub

Private Sub mnuEditNewKey_Click()
  'ver 1.01 (jjm) 2004-04-30
  Dim NewNode As Node
  Set NewNode = AddKey
  MenuRefresh

  tvFolders.SetFocus
  If Not NewNode Is Nothing Then
    NewNode.Selected = True
    mnuEditRename_Click
  End If
  
End Sub

Private Sub mnuEditNewValue_Click()
  With lvView.ListItems.Add(, , "New Value")
    .ListSubItems.Add , "data", vbNullString
    .Selected = True 'ver 1.01 (jjm) 2004-04-30
  End With
  
  Dirty = True
  MenuRefresh
  
  'ver 1.01 (jjm) 2004-04-30
  'Set the focus to the recently added item along with the rename
  SSTab1.Tab = 1
  lvView.SetFocus
  mnuEditRename_Click
  
End Sub

Private Sub mnuEditRename_Click()
  If ActiveControl Is Nothing Then Exit Sub
  Select Case ActiveControl.Name
    Case tvFolders.Name
      tvFolders.SetFocus
      tvFolders.StartLabelEdit
    Case lvView.Name
      lvView.SetFocus
      lvView.StartLabelEdit
  End Select
End Sub

Private Sub mnuFileExit_Click()
  If Save(True) Then Unload Me
End Sub

Private Sub mnuFileSave_Click()
  Save
End Sub

Private Sub mnuPopupDelete_Click()
  mnuEditDelete_Click
End Sub

Private Sub mnuPopupModify_Click()
  mnuEditModifyValueData_Click
End Sub

Private Sub mnuPopupNewKey_Click()
  mnuEditNewKey_Click
End Sub

Private Sub mnuPopupNewValue_Click()
  mnuEditNewValue_Click
End Sub

Private Sub mnuPopupOrganizeFolders_Click()
  mnuToolsOrganizeFolders_Click
End Sub

Private Sub mnuPopupRename_Click()
  mnuEditRename_Click
End Sub

Private Sub mnuSynchronize_Click()
  Save True
  If Factory.PreferenceService.Sync(SystemFolders_TreeName) Then
    FormRefresh
  End If
End Sub

Private Sub mnuToolsOrganizeFolders_Click()
  Dim fForm As New frmFolderOrganizeDialog
  fForm.ShowFolder NodeKey
  Unload fForm
  'ver 2.30.0825-B (jjm)
  FolderUtil.TreeViewLoadSystemFolders tvFolders
  'FolderUtil.TreeViewLoadFolder tvFolders, systemfolders_treename
  If Contains(tvFolders.Nodes, NodeKey) Then
    tvFolders.Nodes(NodeKey).Selected = True
  End If
  TreeRefresh
End Sub

Private Sub mnuViewRefresh_Click()
  FormRefresh
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  FormUtil.TabControlEnable SSTab1
End Sub

Private Sub tvFolders_AfterLabelEdit(Cancel As Integer, NewString As String)
  'If lvView.SelectedItem.Text <> NewString Then
  If Not DirNameSafe(NewString) Then
    Cancel = True
    Exit Sub
  End If
  
  If tvFolders.SelectedItem.Key = SystemFolders_RootKey Then
    SystemFolders_RootName = NewString
    PreferenceUtil.renameIniNode SystemFolders_TreeName, SystemFolders_RootName
  Else
    Cancel = Not Factory.FolderService.RenameFolder(SystemFolders_TreeName, tvFolders.SelectedItem.Key, NewString)
  End If

End Sub

Private Sub tvFolders_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    PopupMenu mnuPopup, , , , mnuPopupRename
  End If
End Sub

Private Sub tvFolders_NodeClick(ByVal Node As MSComctlLib.Node)
  If Node.Key = NodeKey Then
    Exit Sub
  End If

  If Not Save(True) Then
    'make no changes, leave as is
    tvFolders.Nodes(NodeKey).Selected = True
    Exit Sub
  End If

  NodeKey = Node.Key
End Sub

Private Sub txtDatafield_Change(Index As Integer)
  Dim strDatafield As String
  Dim ListIndex As Integer
  
  strDatafield = txtDataField(Index).DataField
  ListIndex = GetListIndex(strDatafield, lvView.ListItems)
  
  'remove from list if null
  If txtDataField(Index).Text = vbNullString Then
    If ListIndex > -1 Then
      lvView.ListItems.Remove ListIndex
    End If
    Dirty = True
    Exit Sub
  End If
    
  'write
  If ListIndex > -1 Then
    lvView.ListItems(ListIndex).ListSubItems("data").Text = txtDataField(Index).Text
  Else
    With lvView.ListItems.Add(, , strDatafield)
      .ListSubItems.Add , "data", txtDataField(Index).Text
    End With
  End If
  
  Dirty = True
End Sub

